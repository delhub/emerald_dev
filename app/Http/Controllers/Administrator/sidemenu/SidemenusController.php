<?php

namespace App\Http\Controllers\Administrator\sidemenu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sidemenus\Sidemenu;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use DB;

class SidemenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sidemenus = Sidemenu::orderBy('created_at', 'desc')->paginate(10);
        return view('administrator.sidemenus.index', compact('sidemenus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sidemenus = sidemenu::all();
        return view('administrator.sidemenus.create', compact('sidemenus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'menu_title' => 'required',
            'menu_group' => 'required',
            'country_id' => 'required',
            'menu_link' => 'required',
            'menu_arrangement' => 'required',
            'menu_icon' => 'image|nullable|max:1999'
        ]);

        // Create New Menu
        $sidemenus = new Sidemenu;
        $sidemenus->menu_title = $request->input('menu_title');
        $sidemenus->menu_group = $request->input('menu_group');
        $sidemenus->menu_parent = $request->input('menu_parent');
        $sidemenus->country_id = $request->input('country_id');
        $sidemenus->menu_link = $request->input('menu_link');
        
        // $sidemenus->menu_icon = $fileNameToStore;
        $sidemenus->menu_arrangement = $request->input('menu_arrangement');
        $sidemenus->menu_show = ($request->input('menu_show') != 1) ? 0 : 1;

        if($request->hasFile('menu_icon')){

            $file = $request->file('menu_icon');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $sidemenus->menu_title . '-' . $sidemenus->menu_group . '.' . $fileExtension;
            $destinationPath =

                public_path('/storage/customer/sidebar-icons/');

                if (!File::isDirectory($destinationPath)) {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $fileName);

                $sidemenus->menu_icon = $fileName;
                
        } else {
            $sidemenus->menu_icon = 'noimage.jpg';
        }
        
        $sidemenus->save();

        return redirect('/administrator/sidemenus/')->with('success', 'submit done.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sidemenu = Sidemenu::find($id);
        return view('administrator.sidemenus.show')->with('sidemenu', $sidemenu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sidemenuParent = Sidemenu::all();
        $sidemenu = Sidemenu::find($id);
        return view('administrator.sidemenus.edit')
        ->with('sidemenu', $sidemenu)
        ->with('sidemenuParent', $sidemenuParent);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'menu_title' => 'required',
            'menu_group' => 'required',
            'country_id' => 'required',
            'menu_link' => 'required',
            'menu_icon' => 'image|nullable|max:1999'
        ]);

        // Create New Menu
        $sidemenus = Sidemenu::find($id);
        $sidemenus->menu_title = $request->input('menu_title');
        $sidemenus->menu_group = $request->input('menu_group');
        $sidemenus->menu_parent = $request->input('menu_parent');
        $sidemenus->country_id = $request->input('country_id');
        $sidemenus->menu_link = $request->input('menu_link');
        $sidemenus->menu_arrangement = $request->input('menu_arrangement');
        $sidemenus->menu_show = ($request->input('menu_show') != 1) ? 0 : 1;
       
        if($request->hasFile('menu_icon') && request('menu_icon') != ''){

            $file = $request->file('menu_icon');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $sidemenus->menu_title . '-' . $sidemenus->menu_group . '.' . $fileExtension;
            $destinationPath =

            public_path('/storage/customer/sidebar-icons/');

                if (!File::isDirectory($destinationPath)) {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $fileName);

                $sidemenus->menu_icon = $fileName;
                
        } else {
            // $sidemenus->menu_icon = 'noimage.jpg';
        }

        $sidemenus->save();

        return redirect('/administrator/sidemenus/')->with('success', 'submit Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sidemenu = Sidemenu::find($id);
        $sidemenu->delete();
        return redirect('/administrator/sidemenus/')->with('success', 'Menu Remove');
    }
}
