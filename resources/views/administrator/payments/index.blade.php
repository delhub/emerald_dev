@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="#" style="color: white; font-style: normal; border-radius: 5px;"
                        class="btn btn-dark">Coming Soon</a>
                </div>
                <div class="col-md-5 ml-auto">
                    <form action="{{ route('administrator.payment') }}" method="GET">
                        <div class="row">
                            <div class="input-group col-md-9 p-0 ml-auto mx-4">
                                <input type="hidden" id="paymentStatus" name="paymentStatus"
                                    value="{{ $request->query('paymentStatus') }}">
                                {{-- <div class="col-md-5"> --}}
                                    <select class="custom-select" id="selectSearch" name="selectSearch">
                                        <option value="ALL">Choose...</option>
                                        <option value="inv_number" {{ ($request->selectSearch == null || $request->selectSearch == 'inv_number')  ? 'selected' : '' }}>
                                            Invoice Number
                                        </option>
                                        <option value="purchase_number" {{ ($request->selectSearch == 'purchase_number')  ? 'selected' : '' }}>
                                            Order Number
                                        </option>
                                        <option value="PT" {{ ($request->selectSearch == 'PT')  ? 'selected' : '' }}>
                                            Purchase Type
                                        </option>
                                        {{-- <option value="PO" {{ $request->selectSearch == 'PO' ? 'selected' : '' }}>
                                            PO Number
                                        </option> --}}
                                        <option value="BDO" {{ $request->selectSearch == 'BDO' ? 'selected' : '' }}>
                                            DO Number
                                        </option>
                                        <option value="NAME" {{ $request->selectSearch == 'NAME' ? 'selected' : '' }}>
                                            Name
                                        </option>
                                        <option value="RM" {{ $request->selectSearch == 'RM' ? 'selected' : '' }}>
                                            Price
                                        </option>
                                        <option value="REFERENCE" {{ $request->selectSearch == 'REFERENCE' ? 'selected' : '' }}>
                                            Reference Number
                                        </option>
                                    </select>
                                {{-- </div> --}}


                                {{-- <div class="input-group-append col-md-5 p-0">
                                    <input type="text" class="form-control" id="searchBox" name="searchBox"
                                        value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                                </div> --}}
                                <div class="input-group-append col-md-4 p-0" id="searchPT" @if($request->selectSearch !== 'PT') style="display: none" @endif>
                                    <select class="custom-select" name="searchBox" id="searchBox"  @if($request->selectSearch !== 'PT') disabled @endif>
                                        @foreach ($purchase_types as $purchase_type)
                                            <option value="{{ $purchase_type }}" {{ ($request->searchBox == $purchase_type) ? 'selected' : '' }}>
                                                {{ $purchase_type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group-append col-md-4 p-0" id="searchInput" @if($request->selectSearch == 'PT') style="display: none" @endif>
                                    <input type="text" class="form-control" id="searchBox" name="searchBox"
                                        value="{{ ($request->searchBox) ? $request->searchBox : '' }}" @if($request->selectSearch == 'PT') disabled @endif>
                                </div>
                                <div class="input-group-append {{-- col-md-3 --}} p-0">
                                    <button type="submit" class="btn btn-outline-warning"
                                        style="color:black;">Search</button>
                                </div>
                                <div class="input-group-append {{-- col-md-3 --}} p-0">
                                    <button type="button" class="btn btn-outline-warning" style="color:black;" {{-- @@click="resetFilter" --}}onclick="resetFilter()">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            <table id="payments-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td style="width: 5%;">No.</td>
                        <td style="width: 65%;">Details</td>
                        <td style="width: 30%;">Documents</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($purchases as $purchase)
                    <tr>
                        <td>
                            {{-- {{ $loop->index }} --}}
                            {{ ($purchases->currentpage()-1) * $purchases ->perpage() + $loop->index + 1 }}
                        </td>
                        <td class="px-md-4">
                            <div class="row border-bottom mb-md-3 pb-md-3">
                                <div class="col-md-12 ">
                                    <p class="mb-0">
                                        Invoice Number: <span
                                        class="text-capitalize font-weight-bold">
                                        @if (isset($purchase->inv_number))
                                            {{ $purchase->inv_number }}
                                        @else
                                            N/A
                                        @endif

                                    </span>
                                </p>

                                <p class="mb-0">
                                    Order Number: <span
                                            class="text-capitalize font-weight-bold">{{ $purchase->getFormattedNumber() }}
                                        </span>
                                    </p>
                                    <p class="mb-0">
                                        Purchase Amount: <span class="text-capitalize font-weight-bold">
                                            {{ number_format(($purchase->purchase_amount / 100), 2) }}</span>
                                    </p>
                                    <p class="mb-0">
                                        Currency: <span class="text-capitalize font-weight-bold">
                                            {{ $purchase->countryId->country_currency }}</span>
                                    </p>
                                    <p class="mb-0">
                                        Purchase Type: <span
                                            class="text-capitalize font-weight-bold">
                                            {{ ($purchase->purchase_type != null) ? ($purchase->purchase_type == 'card_cybersource' ? 'Card' : $purchase->purchase_type) : 'Not Available' }}{{ ($purchase->installment) ? '(DC Credit)' : '' }} {{-- $purchase->payment_type>7000&&$purchase->purchase_type!='Voucher'?'('.$statuses->where('id',$purchase->payment_type)->first()->name.')':'' --}}</span>

                                    </p>
                                    <p class="mb-0">
                                        Purchased At: <span
                                            class="text-capitalize font-weight-bold text-muted">{{ $purchase->created_at->format('d/m/Y@g:iA') }}</span>
                                    </p>
                                    @if (in_array($purchase->purchase_status,[4005,4008,4009]))
                                    <hr>
                                    <p class="mb-0">
                                        Refund Remark: <span
                                            class="text-capitalize font-weight-bold text-muted">{{ $purchase->purchases_notes['admin_notes'] }}</span>
                                    </p>
                                    @endif
                                </div>
                            </div>
                            <div class="row border-bottom mb-md-3 pb-md-3">
                                <div class="col-md-6 pr-0">
                                    <form action="{{ route('administrator.payment.update', ['id' => $purchase->id]) }}" method="POST" target="_blank" >
                                        @csrf
                                        @method('PUT')
                                        <div class="form-row">
                                            <div class="col-12 col-md-12 input-group">
                                                <select name="purchase_status" id="purchase_status" class="form-control disabledFirst" onchange="refundNotes(this,{{ $purchase->id }})" disabled>
                                                    @foreach($statuses as $status)
                                                    <option value="{{ $status->id }}"
                                                        {{ ($purchase->purchase_status  == $status->id) ? 'selected' : '' }}>
                                                        {{ $status->name }}</option>
                                                    @endforeach
                                                    @foreach($purchase->orders as $order)
                                                    <input type="hidden" name="do_number[]"
                                                        value={{  $order->delivery_order }} />
                                                    <input type="hidden" name="po_number[]" value={{  $order->order_number }} />
                                                    @endforeach

                                                </select>
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-primary" type="submit" id="normalUpdate{{ $purchase->id }}" >Update</button>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVoucher" data-do-number='@json($purchase->orders->pluck('delivery_order'))' data-po-number='@json($purchase->orders->pluck('order_number'))' data-purchase-id="{{ $purchase->id }}"  data-refund-type="" data-old-status="{{ $purchase->orders->pluck('order_status') }}" id="voucherUpdate{{ $purchase->id }}" style="display: none;"> Voucher Notes </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6 pl-1">
                                    @if (isset($purchase->payments->first()['gateway_response_code']) && $purchase->payments->first()['gateway_response_code'] == '99')
                                    <form action="{{ route('payment.request', [$purchase->purchase_number]) }}" method="GET">
                                        @csrf
                                        @method('GET')
                                        <div class="form-row">
                                            <div class="col-12 col-md-12 input-group">
                                                <button type="submit" class="btn btn-primary" type="submit">Update FPX Status</button>
                                            </div>
                                        </div>
                                    </form>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="row mt-1">
                                        <div class="col-12 col-md-6">
                                            <p class="mb-0">
                                                Purchased By:
                                                <br>
                                                {{ ($purchase->user && $purchase->user->userInfo) ? $purchase->user->userInfo->full_name : 'Not Available' }}
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            Purchased For:
                                            <br>
                                            {{ ($purchase->ship_full_name != null) ? $purchase->ship_full_name : 'Not Available' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="mb-1">
                                <p class="mb-0">
                                    Invoice
                                </p>
                                <a target="_blank" href="{{ route('administrator.view-pdf', ['purchase' => $purchase]) }}">
                                    <button class="btn btn-primary">View</button>
                                </a>

                                @if (in_array($purchase->purchase_status, [4002, 4007]))
                                <a target="_blank" href="{{ route('global.view.pdf',['pdf_type'=>'invoice','purchase_number'=>$purchase->getFormattedNumber()]) }}">
                                    <button class="btn btn-danger">Download PDF</button>
                                </a>
                                @endif
                            </div>
                            <div class="mb-1">
                                <p class="mb-0">
                                    Receipt
                                </p>
                                <a target="_blank"
                                    href="{{ route('global.view.pdf',['pdf_type'=>'receipt','purchase_number'=>$purchase->getFormattedNumber()]) }}">
                                    {{ $purchase->receipt_number }}</a>
                            </div>
                            {{-- <div class="mb-1">
                                <p class="mb-0">
                                    Order Number
                                </p>
                                @foreach($purchase->orders as $order)
                                <a href="#">{{ $order->order_number }}</a>
                                @endforeach
                            </div> --}}
                            <div class="mb-1">
                                <p class="mb-0">
                                    Delivery Orders
                                </p>
                                @foreach($purchase->orders as $order)
                                <a target="_blank"
                                    href="{{ route('global.view.pdf',['pdf_type'=>'delivery-order','purchase_number'=>$purchase->getFormattedNumber(),'order_number'=>$order->delivery_order]) }}">{{ $order->delivery_order }}</a>
                                @endforeach
                            </div>
                            @if(!in_array($purchase->purchase_type,['Bluepoint','Card','Fpx']))
                            <div class="mb-1">
                                <p class="mb-0">
                                    Payment Proof
                                </p>
                                <a target="_blank"
                                    href="{{ URL::asset($purchase->payment_proof) }}">{{ $purchase->getFormattedNumber() }}</a>
                            </div>
                            @endif
                            <div class="my-3">
                                <p class="mb-0">
                                    <a target="_blank"
                                        href="/administrator/regenerate-invoice/{{$purchase->purchase_number}}"
                                        class="btn btn-primary text-light">Regenerate Inv., Recpt., PO. PDF</a>
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-auto ml-auto">
                {{ $purchases->appends($_GET)->links() }}
                {{-- {!! $purchases->render() !!} --}}
            </div>
        </div>


    </div>
</div>
@endsection

    <!-- Modal Refunded -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" id="modalVoucher" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-lg-centered modal-lg">
            <form action="{{ route('administrator.payment.update', ['id' => 'refund']) }}"
                method="POST" target="_blank">
                @csrf
                @method('PUT')
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="modalVoucherTitle">Refund Remarks</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body row">
                        <div class="col-md-12">
                            <div class="col-md-12 bg-danger my-md-3 py-md-3 deliveredStatus" style="display: none;">
                                <h4>Reminder: This order is already delivered.</h4>
                                <br>
                            </div>
                            <h5 class="checkboxLabel" style="display:none;">Choose order that will be refund</h5>
                            <div class="checkboxDiv" style="display:none;">
                            </div>
                            <hr class="checkboxLabel" style="display:none;">
                            <input type="hidden" name="purchase_id" id="purchase_id" value="">
                            <input type="hidden" name="refund_type" id="refund_type" value="">
                            <input type="hidden" name="order_before" id="order_before" value="">

                            <div class="form-group col-md-12">
                                <label for="paymentNotes">Refund Remarks <span class="text-danger">*</span></label>
                                <input type="text" name="payment_notes" id="paymentNotes" style="width: 100%" required>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
                    <button type="submit" class="btn btn-primary" >Update</button>
                    </div>
                </div>
              </form>
        </div>
    </div>

@push('style')
<style>
    /*  */
</style>
@endpush

@push('script')
<script>
    function refundNotes(obj,id){
        var normalUpdate = $('#normalUpdate'+id);
        var voucherUpdate = $('#voucherUpdate'+id);
        if (obj.value == 4005 || obj.value == 4008 || obj.value == 4009) {
            $(normalUpdate).hide();
            $(voucherUpdate).show();
        } else {
            $(voucherUpdate).hide();
            $(normalUpdate).show();
        }
        $(voucherUpdate).attr('data-refund-type',obj.value);
    }

    $('#modalVoucher').on('hide.bs.modal', function(e) {
        $('#modalVoucher .form-group [name*=do_number],[name*=po_number]').remove()
    })

    //triggered when modal is about to be shown
    $('#modalVoucher').on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var purchaseID = $(e.relatedTarget).data('purchase-id');
        var refundType = $(e.relatedTarget).data('refund-type');
        var oldStatus = $(e.relatedTarget).data('old-status');
        var doNumbers = $(e.relatedTarget).data('do-number');
        var poNumbers = $(e.relatedTarget).data('po-number');

        doNumbers.forEach(doNumber => {
            $('#modalVoucher .form-group').append(`<input type="hidden" name="do_number[]" value="${doNumber}">`)
        })

        poNumbers.forEach(poNumber => {
            $('#modalVoucher .form-group').append(`<input type="hidden" name="po_number[]" value="${poNumber}">`)
        })

        if (refundType === 4009) {
            $(e.currentTarget).find('.checkboxDiv').show();
            $(e.currentTarget).find('.checkboxLabel').show();

            $.ajax({
            url: "/administrator/payments/order-list/",
            type: "GET",
            data: {
                    id: purchaseID
                },
            success: function(result) {
                console.log(result);
                $(e.currentTarget).find('.checkboxDiv').html(result);
            },
            error: function(result) {
                // console.log(result.status + ' ' + result.statusText);
            }
            });
        }

        //populate the textbox
        $(e.currentTarget).find('#purchase_id').val(purchaseID);
        $(e.currentTarget).find('#refund_type').val(refundType);
        $(e.currentTarget).find('#order_before').val(oldStatus);
    });

    $(document).ready(function() {

        $('.disabledFirst').removeAttr('disabled');

        // purchase_status

        // $('#payments-table').DataTable({
        //     'autoWidth': false,
        //     'paging': false
        // });
    });

    function resetFilter() {
        url = new URL(window.location.href)

        url.searchParams.delete('selectSearch')
        url.searchParams.delete('searchBox')

        window.location.href = url.toString()
    }
    $(document).ready(function() {
        $('#selectSearch').on('change', function () {
            if ($(this).val() == 'PT') {
                $('#searchPT').show()
                $('#searchInput').hide()
                $('select[name=searchBox]').prop('disabled', false)
                $('input[name=searchBox]').prop('disabled', true)
            } else {
                $('input[name=searchBox]').val('')
                $('#searchPT').hide()
                $('#searchInput').show()
                $('select[name=searchBox]').prop('disabled', true)
                $('input[name=searchBox]').prop('disabled', false)
            }
        })
    });
</script>
@endpush
