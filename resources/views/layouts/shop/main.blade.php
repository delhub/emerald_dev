<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Formula') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- JQUERY for datepicker -->
    {{-- <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}"> --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.1/css/bootstrap.min.css" integrity="sha512-Ez0cGzNzHR1tYAv56860NLspgUGuQw16GiOOp/I2LuTmpSK9xDXlgJz3XN4cnpXWDmkNBKXR/VDMTCnAaEooxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    {{-- slick slider and OwlCarousel slider --}}
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owlslider.css') }}?v=2.4">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}?v=3.6">
    <link rel="stylesheet" href="{{ asset('css/style2.css') }}?v=3.7">
    <link rel="stylesheet" href="{{ asset('css/style3.css') }}?v=3.3">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}?v=3.9">
    <link rel="stylesheet" href="{{ asset('css/responsive2.css') }}?v=3.3">
    <link href="{{ asset('css/app.css') }}?v=1.7" rel="stylesheet">
    <!-- Custom Scrollbar CDN -->

    <!-- TODO: Import using mix. -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />


    @stack('style')

</head>

<body class="home-bg">
    <!-- Navigation bar -->
    @if (!in_array(\Request::route()->getName(), ['agent.direct.pay', 'gateway.responses', 'direct.pay.success']))
        @include('layouts.management.navigations.navigation-bar-customer')

    @endif

    <!-- Side bar -->
    {{-- @include('layouts.shop.navigation.sidebar') --}}



    <main class="no-margin" id="body-content-collapse-sidebar">
        <div id="app" class="___class_+?2___">
            <div id="coverBg"></div>
            @yield('content')
        </div>
    </main>

    <!-- Footer -->
    @include('layouts.shop.footer.footer')


    <!--Insert JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.1/js/bootstrap.bundle.min.js" integrity="sha512-sH8JPhKJUeA9PWk3eOcOl8U+lfZTgtBXD41q6cO/slwxGHCxKcW45K4oPCUhHG7NMB4mbKEddVmPuTXtpbCbFA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js">
    </script>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.zoom.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/slideronly.js') }}?v=1.4"></script>
    <script src="{{ asset('js/emerald.js') }}?v=1.8"></script>


    <!-------Show  perfect list and Add card quantity-------->
    @if (!in_array(\Request::route()->getName(), ['agent.direct.pay', 'gateway.responses', 'direct.pay.success']))

        <script type="text/javascript">
            $(document).ready(function() {

                //ajax data
                function onPageLoadGetQuantity() {
                    @if (Auth::check())
                        let getQuantityURL = "{{ route('web.shop.cart.quantity', ['id' => Auth::user()->id]) }}";
                    @else
                        let getQuantityURL = null;
                    @endif
                    const cartQuantityElement = $('#cart-quantity');
                    const cartQuantityElementMobile = $('#cart-quantity-mobile');

                    $.ajax({
                        async: true,
                        beforeSend: function() {
                            console.log('Request starting.');
                        },
                        complete: function() {
                            console.log('Request complete.');
                        },
                        url: getQuantityURL,
                        type: 'GET',
                        success: function(response) {
                            cartQuantityElement.text(response.data);
                            cartQuantityElementMobile.text(response.data);
                        },
                        error: function(response) {
                            console.log('Error');
                            console.log(response);
                        }
                    });
                }

                // function to get perfect list quantity
                function onPageLoadGetPerfectListQuantity() {
                    @if (Auth::check())
                        let getQuantityURL = "{{ route('shop.perfect-list.quantity', ['id' => Auth::user()->id]) }}";
                    @else
                        let getQuantityURL = null;
                    @endif
                    const perfectListQuantityElement = $('#wishlist-quantity');

                    $.ajax({
                        async: true,
                        beforeSend: function() {
                            console.log('Request starting.');
                        },
                        complete: function() {
                            console.log('Request complete.');
                        },
                        url: getQuantityURL,
                        type: 'GET',
                        success: function(response) {
                            perfectListQuantityElement.text(response.data);
                        },
                        error: function(response) {
                            console.log('Error');
                            console.log(response);
                        }
                    });
                }
                $("a").tooltip();

            });
        </script>
    @endif
    <!--Validate register fields -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    @stack('script')
</body>

</html>
