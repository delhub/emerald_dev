<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('warehouse_batch_item')->insert([
            'item_id' => time(),
            'batch_id' => self::generateCode(10),
            'detail_id' => rand(1,9999),
            'location_id' => rand(1,20),
            'shelf_id' => rand(1,9),
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }

    public static function generateCode($length = 8)
    {
        if (function_exists('random_bytes')) {
            $bytes = random_bytes($length / 2);
        } else {
            $bytes = openssl_random_pseudo_bytes($length / 2);
        }

        $code = bin2hex($bytes);

        $code = str_replace(str_split('oO01l'), '', $code);
        while (strlen($code) < $length) $code .= self::generateCode(22);

        $code = strtoupper($code);

        return substr($code, 0, $length);
    }

}
