<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class UpdateViewInventoryStockBundle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW view_inventories_stock_bundle
            AS
            select `wh_inventories`.`product_code` AS `product_code`,`wh_inventories`.`outlet_id` AS `outlet_id`, `wh_order`.`delivery_order` AS whDO,
            (case when `panel_attribute_bundle`.`active` IS NULL then 0 ELSE 1 END ) AS `bundle`,
            sum((case when ((`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8010') or (`wh_inventories`.`inv_type` = '8012')or (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014') or (`wh_inventories`.`inv_type` = '8017') or (`wh_inventories`.`inv_type` = '8018') or (`wh_inventories`.`inv_type` = '8019') or (`wh_inventories`.`inv_type` = '8020')) then `wh_inventories`.`inv_amount` else 0 end)) AS `physical_stock`,
            sum((case when (`wh_inventories`.`inv_type` = '8001') then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock_reserved`,
            sum((case when (`wh_inventories`.`inv_type` = '8010') OR (`wh_inventories`.`inv_type` = '8013') OR (`wh_inventories`.`inv_type` = '8014') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockIn`,
            sum((case when (`wh_inventories`.`inv_type` = '8011') then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock_reserved_offline`,
            sum((case when (`wh_inventories`.`inv_type` = '8012') then `wh_inventories`.`inv_amount` else 0 end)) AS `pos_completed`,
            sum((case when (`wh_inventories`.`inv_type` = '8013') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockTransferIn`,
            sum((case when (`wh_inventories`.`inv_type` = '8014') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockTransferOut`,
            sum((case when (`wh_inventories`.`inv_type` = '8017') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_adjustOut`,
            sum((case when (`wh_inventories`.`inv_type` = '8018') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_adjustIn`,
            sum((case when (`wh_inventories`.`inv_type` = '8019') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_b_bundle_add`,
            sum((case when (`wh_inventories`.`inv_type` = '8020') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_b_bundle_minus`,
            sum((case when (`wh_inventories`.`inv_type` = '8000') then `wh_inventories`.`inv_amount` else 0 end)) AS `online_sold`,
            sum((case when ((`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8012')) then `wh_inventories`.`inv_amount` else 0 end)) AS `total_sold`,
            sum((case when ((`wh_inventories`.`inv_type` = '8000')  or (`wh_inventories`.`inv_type` = '8010')  or  (`wh_inventories`.`inv_type` = '8012') or (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014') or (`wh_inventories`.`inv_type` = '8017') or (`wh_inventories`.`inv_type` = '8018') or (`wh_inventories`.`inv_type` = '8019') or (`wh_inventories`.`inv_type` = '8020')OR (`wh_inventories`.`inv_type` = '8001')OR (`wh_inventories`.`inv_type` = '8011')) then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock`

            from `wh_inventories`
            JOIN `wh_order` ON `wh_order`.`id` = `wh_inventories`.`model_id`
            LEFT JOIN `panel_attribute_bundle` ON `panel_attribute_bundle`.`product_code` = `wh_inventories`.`product_code` AND `panel_attribute_bundle`.`active` IN  (1,2)
            WHERE ((`wh_inventories`.`outlet_id` != '999' ) OR (`wh_inventories`.`outlet_id` = '999' AND `wh_inventories`.`model_type` IN ('App\\Models\\Warehouse\\Order\\WarehouseOrder')))
            AND  `wh_inventories`.`created_at` > '2021-10-01'
            group by `wh_inventories`.`product_code`,`wh_inventories`.`outlet_id`, `panel_attribute_bundle`.`active`, `wh_order`.`delivery_order`, `wh_order`.`id`
            order by `wh_inventories`.`outlet_id`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
