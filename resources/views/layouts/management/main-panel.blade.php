<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    {{-- <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
    <!--Bootstrap style for table form -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}?v=2.4" rel="stylesheet">
    <link href="http://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine&display=swap" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- JQUERY for datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


</head>

<body class="app header-fixed aside-menu-fixed sidebar-lg-show">
    @include('layouts.management.navigations.navigation-bar-customer')
    <div class="">
        @include('layouts.management.navigations.side-bar-panel')
        <main class="main">

            @yield('breadcrumbs')
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <!-- Content Here -->
                    @yield('content')
                </div>
            </div>
        </main>
        {{-- @include('layouts.management.components.aside') --}}
    </div>
    @include('layouts.shop.footer.footer')
    <div id="coverBg"></div>

    <script type="text/javascript">
        function onPageLoadGetQuantity() {
            let getQuantityURL = "{{ route('web.shop.cart.quantity', ['id' => Auth::user()->id]) }}";
            const cartQuantityElement = $('#cart-quantity');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    cartQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        // function to get perfect list quantity
        function onPageLoadGetPerfectListQuantity() {
            let getQuantityURL = "{{ route('shop.perfect-list.quantity', ['id' => Auth::user()->id]) }}";
            const perfectListQuantityElement = $('#wishlist-quantity');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    perfectListQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        // $(document).ready(function() {
        //     onPageLoadGetQuantity();
        //     onPageLoadGetPerfectListQuantity();
        // });
    </script>

    @stack('script')

</body>

</html>
