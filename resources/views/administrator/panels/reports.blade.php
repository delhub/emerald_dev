@inject('controller', 'App\Http\Controllers\Administrator\Panels\PanelsController')

@extends('layouts.administrator.main')

@section('content')
<br>

<h2>Summary of brands and products</h2>
<br>
<div class="card shadow-sm">
  <div class="card-body">
      <div class="table-responsive m-2">
        <table class="container table table-striped table-bordered">
          <thead>
            <tr>
              <th rowspan="2" scope="col"></th>
              <th rowspan="2" scope="col">Brands</th>
              <th colspan="3" scope="col">Products</th>
              <th rowspan="2" scope="col">Total Products</th>
            </tr>
            <tr>
              <th scope="col">Active</th>
              <th scope="col">Suspended</th>
              <th scope="col">Terminated</th>
            </tr>
          </thead>
          <tbody>

            @php
                $sumBrand = $sumTerminate = $sumSuspend = $sumActive = 0;
            @endphp

              @foreach ($qualityReports as $key => $qualityReport)
              @php
              // dd($qualityReport['product']);
              // $qualityReport['product']['display_only']['product_status']
              $active =  count($qualityReport['product'][0][1]);
              $suspend = count($qualityReport['product'][1][1]);
              $terminate = count($qualityReport['product'][0][2]) + count($qualityReport['product'][1][2]);
              $brand = count($qualityReport['brand']);

              $sumActive += $active;
              $sumSuspend += $suspend;
              $sumTerminate += $terminate;
              $sumBrand += $brand;

              @endphp
              <tr>
                  <th scope="row">{{ $quality->where('id',$key)->first()->name }}</th>
                  <td>{{ $brand }}</td>
                  <td>{{ $active }}</td>
                  <td>{{ $suspend }}</td>
                  <td>{{ $terminate }}</td>
                  <td>{{ $active+$terminate+$suspend }}</td>
                </tr>
              @endforeach
              <tr>
               <td><b>Total</b></td>
               <td>{{ $sumBrand }}</td>
               <td>{{ $sumActive }}</td>
               <td>{{ $sumSuspend }}</td>
               <td>{{ $sumTerminate }}</td>
               <td>
                {{ $sumTerminate + $sumSuspend + $sumActive }} <small><b>(domestic + import + exclusive)</b></small></td>
              </tr>
          </tbody>
        </table>
      </div>
      {{-- <div class="row">asda</div> --}}
      <small class=""><span class="text-danger">*</span><b>Remark</b>
        <br> 1. Data is based on brand which has product.
        <br> 2. If same brand but different category, will count separately.
        <br> For example: Blackmore has product in domestic and import category, so domestic brand count 1 and import brand count 1, total brand = 2. </small>
  </div>
</div>
<div class="card shadow-sm">
  <div class="card-body">
      <table class="w-25 table table-striped table-bordered">
        <thead>
          <tr>
            <th scope="col"></th>
            @foreach ($monthlyReports as $monthlyReport)
            <th scope="col">{{ $monthlyReport['tr'] }}</th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>All Brands</td>
            @foreach ($monthlyReports as $monthlyReport)
            <td>{{ $monthlyReport['td']['brand_count'] }}</td>
            @endforeach
          </tr>
          <tr>
            <td>All Products</td>
            @foreach ($monthlyReports as $monthlyReport)
            <td>{{ $monthlyReport['td']['product_count'] }}</td>
            @endforeach
          </tr>
          <tr>
            <td>New Brands</td>
            @foreach ($monthlyReports as $monthlyReport)
            <td>{{ $monthlyReport['td']['new_brand_count'] }}</td>
            @endforeach
          </tr>
          <tr>
            <td>New Products</td>
            @foreach ($monthlyReports as $monthlyReport)
            <td>{{ $monthlyReport['td']['new_product_count'] }}</td>
            @endforeach
          </tr>
          <tr>
            <td>Product Suspended</td>
            @foreach ($monthlyReports as $monthlyReport)
            <td>{{ $monthlyReport['td']['product_suspended'] }}</td>
            @endforeach
          </tr>
          <tr>
            <td>Product Terminated</td>
            @foreach ($monthlyReports as $monthlyReport)
            <td>{{ $monthlyReport['td']['product_terminate'] }}</td>
            @endforeach
          </tr>

        </tbody>
      </table>
  </div>
  <div class="pb-md-3 px-md-4">
    <small class=""><span class="text-danger">*</span><b>Remark</b>
      <br> 1. Data is based on brands have and dont have products.
      <br> 2. If same brand but different category, will count as 1 brand.
      <br> For example: Blackmore has product in domestic and import category, total brand = 1.
      </small>
  </div>

</div>


@endsection
