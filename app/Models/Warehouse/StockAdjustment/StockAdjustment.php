<?php

namespace App\Models\Warehouse\StockAdjustment;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;


class StockAdjustment extends Model
{
    use TableLogs;

    protected $table = 'wh_stock_adjustment';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    public function theUser()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'user');
    }
    public function adjustType()
    {
        return $this->hasone('App\Models\Globals\Status', 'id', 'adjust_type');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Warehouse\Location\Wh_location', 'location_id', 'id');
    }
}
