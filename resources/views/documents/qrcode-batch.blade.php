<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $pageTitle }}</title>
     @include('layouts.favicon')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
    body {
        width: 100%;
        height: 100%;
        margin: 0 auto;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 210mm;
        min-height: 297mm;
        padding: 0mm 10.5mm ;
        /* margin: 10mm auto; */
        /* border: 1px #D3D3D3 solid;
        border-radius: 5px; */
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    .my-style{
        background: rgb(255 255 255);
        height: 18mm;
        width: 27mm;
        /* border: 1px dotted #eaeaea;
        border-radius: 20px; */
        float: left;
    }
    .itemid {
        display: block;
        font-size: 7px;
        position: relative;
        top: -12px;
    }
    .start {
        display: block;
        font-size: 9px;
        position: relative;
        top: 12px;
    }
    @page {
        size: A4;
        padding-top: 0mm;

    }

    @page :first {
        padding-top: 0mm
    }

    @media print {
        body {
            width: 210mm;
            height: 297mm;
        }
        .page {
            /* margin: 0; */
/*             padding: 5.8mm 10.5mm ; */
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }

    }
    </style>
</head>
<body>
    <div class="page">
        @php
            $size = 60;
            $row = $request->row * 7;
            $start = $request->start * 1;
        @endphp
        <div>
            @if(isset($row))
            @for ($i = 1; $i <= $row; $i++)
            <div class="my-style text-center">
                <img width="{{$size}}px" width="{{$size}}px" src="data:images/png;base64, R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAICTAEAOw==">
                <span class="itemid"></span>
            </div>
            @endfor
            @endif
            @if(isset($start))
            @for ($i = 1; $i <= $start; $i++)
            <div class="my-style text-center">
                <img width="{{$size}}px" width="{{$size}}px" src="data:images/png;base64, R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAICTAEAOw==">
                <span class="itemid"></span>
            </div>
            @endfor
            @endif

            @foreach ($batch as $val)
                @if ($loop->index == 0)
                @php
                    $detail = $val->detail_id;
                @endphp
                <div class="my-style text-center">
                    <span class="start">
                        {{$val->batchDetail->product_code}}
                        {{$val->batchDetail->product_code}}
                        {{$val->batchDetail->product_code}}
                        {{$val->batchDetail->product_code}}
                    </span>
                </div>
                @elseif ($val->detail_id != $detail)
                @php
                    $detail = $val->detail_id;
                @endphp
                <div class="my-style text-center">
                    <span class="start">
                        {{$val->batchDetail->product_code}}
                        {{$val->batchDetail->product_code}}
                        {{$val->batchDetail->product_code}}
                        {{$val->batchDetail->product_code}}
                    </span>
                </div>
                @else
                @endif
                <div class="my-style text-center">
                    <img src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size($size)->generate( Request::getHost() . "/qr/item/". $val->items_id )) }} ">
                    <span class="itemid">{{$val->items_id}}</span>
                </div>
            @endforeach
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.1/jspdf.umd.min.js"></script>

    <script>
        // window.print();
    </script>
</body>
</html>
