<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Purchases\Order;
use App\Models\Purchases\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class UpdateOrdersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('courier_name')->after('claim_status')->nullable();
            $table->string('tracking_number')->after('courier_name')->nullable();
            $table->string('shipment_key')->after('tracking_number')->nullable();
            $table->string('order_date')->after('shipment_key')->nullable()->default('Pending');
            $table->string('est_shipping_date')->after('order_date')->nullable()->default('Pending');
            $table->string('shipping_date')->after('est_shipping_date')->nullable()->default('Pending');
            $table->string('collected_date')->after('shipping_date')->nullable()->default('Pending');
            $table->string('admin_remarks')->after('collected_date')->nullable();
        });

        $orders = Order::get();
        foreach ($orders as $order) {
            $item = Item::where('order_number', $order->order_number)->first();

            if (isset($item->actual_ship_date) || isset($item->created_at)) {
                DB::table('orders')
                    ->where('order_number', $order->order_number)
                    ->update([
                        "courier_name" => $item->courier_name,
                        "tracking_number" => $item->tracking_number,
                        "shipment_key" => $item->shipment_key,
                        "order_date" =>  Carbon::parse($item->created_at)->format('d/m/Y'),
                        "est_shipping_date" => $item->ship_date,
                        "shipping_date" => $item->actual_ship_date,
                        "collected_date" => $item->collected_date,
                        "admin_remarks" => $item->admin_remarks,
                    ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('courier_name');
            $table->dropColumn('tracking_number');
            $table->dropColumn('shipment_key');
            $table->dropColumn('order_date');
            $table->dropColumn('est_shipping_date');
            $table->dropColumn('shipping_date');
            $table->dropColumn('collected_date');
            $table->dropColumn('admin_remarks');
        });
    }
}
