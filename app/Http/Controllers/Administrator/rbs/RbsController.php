<?php

namespace App\Http\Controllers\Administrator\rbs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rbs\RbsOptions;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;

class RbsController extends Controller
{
    public function index() {

        $rbsReminder = RbsOptions::all();
        $rbsFrequency = RbsOptions::where('option_type','frequency')->get();
        $rbsTimeToSends = RbsOptions::where('option_type','times')->get();

        return view('administrator.rbs.index')
        ->with('rbsReminder', $rbsReminder)
        ->with('rbsFrequency', $rbsFrequency)
        ->with('rbsTimeToSends', $rbsTimeToSends)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function createEdit(Request $request) {
        $rbsReminder = null;
        if (isset($request->id)) {
            $rbsReminder = RbsOptions::where('id',$request->id)->first();
        }

        return view('administrator.rbs.create')
            ->with('rbsReminder', $rbsReminder);

    }

    // public function edit(Request $request) {

    //     $rbsReminder = RbsOptions::where('id',$request->id)->first();

    //     return view('administrator.rbs.create')
    //     ->with('rbsReminder', $rbsReminder);

    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'rbs_label' => 'required',
            'rbs_month' => 'required|integer',
            // 'rbs_status' => 'required'
        ]);

        $rbsReminder = RbsOptions::firstOrNew(
            ['id' => $request->input('rbs_id')]
        );

        $rbsReminder->rbs_label = $request->input('rbs_label');
        $rbsReminder->rbs_data = $request->input('rbs_month');
        $rbsReminder->option_type = $request->input('option_type');
        $rbsReminder->option_status = $request->input('rbs_status');
        $rbsReminder->created_at = Carbon::now();
        $rbsReminder->updated_at = Carbon::now();
        $rbsReminder->save();

        return redirect('/administrator/rbs/')->with('success', 'submit done.');
    }

    public function disableRbs($id)
    {
        $rbsReminder = RbsOptions::where('id', $id)->get()->first();
        $rbsReminder->option_status = 'inactive';
        $rbsReminder->save();

        return redirect('/administrator/rbs/')->with('success', 'Rbs had been disabled');
    }

    public function enableRbs($id)
    {
        $rbsReminder = RbsOptions::where('id', $id)->get()->first();
        $rbsReminder->option_status = 'active';
        $rbsReminder->save();

        return redirect('/administrator/rbs/')->with('success', 'Rbs had been enabled');

    }
    public function sendEmail()
    {
        $rbsPostpaid = Artisan::call('rbs:postpaid');
        $output = Artisan::output();

        if($output) {
            $error_message = "Error";
        } else {
            $error_message = "RBS Email Postpaid run.";
        }

        return back()->with(['error_message' => $error_message]);

    }

}