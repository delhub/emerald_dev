<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.favicon')
    <title>{{ config('app.name', 'Formula') }}</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.1/css/bootstrap.min.css" integrity="sha512-Ez0cGzNzHR1tYAv56860NLspgUGuQw16GiOOp/I2LuTmpSK9xDXlgJz3XN4cnpXWDmkNBKXR/VDMTCnAaEooxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}?v=3.6">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}?v=3.9">
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet"> --}}
    {{-- @stack('style') --}}

    <style>
        .edit-popup-bg .modal-content {
            background: url(../images/formula/edit-details-bg.png) no-repeat 0% 100%;
            background-color: #fff;
            border-radius: 2.3em;
        }

        button.close {
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .close {
            float: right;
            font-size: 1.35rem;
            font-weight: 700;
            line-height: 1;
            color: black;
            text-shadow: 0 1px 0 #fff;
            opacity: 0.5;
        }

    </style>
    {{-- /* Make sure the background image covers the screen */
        html {
            height: 100%;
        }

        body {
            height: 100%;
        }

        .remember-check {
            width: 15px;
            height: 15px;
            cursor: pointer;
        }
        .mylogo{
            margin-top: 10%;
            width: 30%;
        }
        .bg-main{
            background: url({{ asset("/images/landing-bg.jpg") }})no-repeat 61% 30%;
            background-size: cover;
        }
        @media (max-width: 1024px) {
            .bg-main{
                background: url({{ asset("/images/landing-bg.jpg") }})no-repeat 51% 17%;
                background-size: 315%;
            }
            .mylogo{
                margin-top: 40%;
                width: 40%;
            }
            .my-t{
                margin-top:100px;
            }
        }
        @media (max-width: 768px) {
            .mylogo{
                width: 50%;
                margin-top: 20%;
            }
            .my-t{
                margin-top:100px;
            }
            /* .bg-md {

                background-image: url(/images/bujishu-home-lg-{{$country}}.jpg);

            } */


        }

        @media (max-width: 414px) {
            .mylogo{
                width: 60%;
                margin-top: 22%;
            }
            .bg-main{
                background: url({{ asset("/images/landing-bg.jpg") }})no-repeat 61% 28%;
                background-size: 373%;
            }
            /* .bg-sm {
                background-image: url(/images/bujishu-home-sm-{{$country}}.jpg);
            } */
        }

        @media (max-width: 400px) {
            .mylogo{
                width: 50%;
                margin-top: 5%;
            }
            .bg-main{
                background: url({{ asset("/images/landing-bg.jpg") }})no-repeat 61% 28%;
                background-size: 373%;
            }
            /* .bg-sm {
                background-image: url(/images/bujishu-home-sm-{{$country}}.jpg);
            } */
        }



    </style> --}}
</head>

<body class="d-flex h-100 text-center mybg">

    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

        <main class="px-3 top-mg">
            <div class="row">
                <div class="col-md-8 mx-auto signin-bg">
                    <div class="col-md-10 mx-auto mb-3">
                        <img class="fmllogo mb-1" src="{{ asset('images/logo/formula-logo.png') }}" alt="Formula">
                        <p>FORMULA HEALTHCARE</p>
                    </div>
                    <div class="col-md-10 mx-auto">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="row mb-3">
                                <label for="email" class="col-2 col-sm-2 col-form-label font25 login-ico"><i
                                        class="fa fa-user"></i></label>
                                <div class="col-10 col-sm-10">
                                    <input class="form-control my @error('email') is-invalid @enderror" type="email"
                                        name="email" id="email" placeholder="Your email address">
                                </div>
                                <div class="col-12">
                                    @error('email')
                                        <span class="my-text-danger mt-1" role="alert">
                                            {{ $message }}
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="colFormLabel" class="col-2 col-sm-2 col-form-label font25 login-ico"><i
                                        class="fa fa-lock"></i></label>
                                <div class="col-10 col-sm-10">
                                    <input type="password" class="form-control my" name="password" id="password"
                                        placeholder="Your password">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6 mb-1">
                                    <div class="form-check">
                                        <input class="form-check-input remember-check" type="checkbox" name="remember"
                                            id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label text-white" style="padding-top: 1px;"
                                            for="remember">
                                            <small>Remember me</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-6 text-right mb-1">
                                    <a class="text-white" href="{{ route('password.request') }}">
                                        <small>Forgot Password?</small>
                                    </a>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <button type="submit" class="btn btn-primary-my">
                                    Login
                                    {{-- <div class="btn-right-arrow"></div> --}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>

    </div>


    {{-- <div class="bg-md bg-sm bg-main">
        <div class="row">
            <div class="col-12 col-md-8 text-center mx-auto">
                <img class="mylogo" src="{{ asset("storage/logo/formula-logo.png") }}" alt="Formula">
            </div>
        </div>
        <div class="">
            <div class="card border-rounded-0 bg-bujishu-gold mt-4 guests-card" style="border-radius: 10px;">
                <h5 class="text-center bujishu-gold form-card-title " style="border-radius: 10px;"><b>SIGN IN</b></h5>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-row mb-2">
                            <div class="input-group col-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user bujishu-gold"></i></span>
                                </div>
                                <input class="form-control @error('email') is-invalid @enderror" type="email"
                                    name="email" id="email" placeholder="Your email address">
                            </div>
                            <div class="col-12">
                                @error('email')
                                <span class="text-danger mt-1" role="alert">
                                    {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row mb-2">
                            <div class="input-group col-12">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-key bujishu-gold"></i></span>
                                </div>
                                <input class="form-control" type="password" name="password" id="password"
                                    placeholder="Your password">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 mb-1">
                                <div class="form-check">
                                    <input class="form-check-input remember-check" type="checkbox" name="remember"
                                        id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label text-dark" style="padding-top: 1px;" for="remember">
                                        Remember me
                                    </label>
                                </div>
                            </div>
                            <div class="col-6 text-right mb-1">
                                <a class="text-dark" href="{{ route('password.request') }}">Forgot Password?</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-center mb-1">
                                <button type="submit" class="btn" style="background-color: #ffffff;">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- Modal -->
    <div class="modal fade" id="model-notification" tabindex="-1" aria-labelledby="accountCreatedNotificationLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md edit-popup-bg">
            <div class="modal-content edit-info-modal-box confirm-info">
                <div class="modal-header pb-0"
                    style="border-bottom: none;padding-top: 40px;padding-left: 50px;padding-right: 50px;">
                    <h4 class="modal-title edit-tittle" id="accountCreatedNotificationLabel">Login Expired!</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <p id="vouchermsg" class="fs-6 text-danger mb-2"
                                style="text-align: left; padding-left: 35px;padding-right: 30px;">Your login session has
                                expired or your authentication is invalid. Please login again.</p>
                        </div>
                        <div class="modal-footer justify-content-center log-exp-btn" style="border-top: 0;">
                            <a class="le-btn" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="mt-auto text-black-50 myfooter">
        <p>© 2021 Copyrighted by <span class="text-black">FORMULA HEALTHCARE SDN. BHD. (1318429-V)</span></p>
    </footer>

    <script>
        $(document).ready(function() {
            @if ($redirectFromRegister == 'true')

                $('.modal-body .message').html=`<h4 class="mb-1 font-weight-bold">Congratulations!</h4>
                <p style="font-size: 1.1rem;" class="message-body-text">Account Successfully created. Please inform your referral
                    agent to verify your account.</p>`;
                $('#model-notification').modal('show');

            @endif

            @if ($sessionExpired == 'true')
                $('.modal-body .message').html = `<p style="font-size: 1.1rem;" class="message-body-text"> Your login session has
                    expired or your authentication is invalid. Please login again.</p>`;
                $('#model-notification').modal('show');
            @endif


            @if ($emailVerified == 'true')
                $('.modal-body .message').html= `<h4 class="mb-1 font-weight-bold">Email Verified!</h4>
                <p style="font-size: 1.1rem;" class="message-body-text">Your customer email address is verified.</p>`;
                $('#model-notification').modal('show');

            @endif
        });
    </script>

</body>
{{-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" --}}
{{-- integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" --}}
{{-- crossorigin="anonymous"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" --}}
{{-- integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" --}}
{{-- crossorigin="anonymous"></script> --}}

</html>
