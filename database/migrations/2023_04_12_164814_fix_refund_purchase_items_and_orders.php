<?php

use App\Http\Controllers\Administrator\Payment\PaymentController;
use App\Models\Purchases\Purchase;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class FixRefundPurchaseItemsAndOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $purchases = Purchase::where('purchase_status', 4005)->whereHas('orders', function ($q) {
            $q->whereNotIn('order_status', [1011, 1012])
                ->orWhereHas('items', function ($q) {
                    $q->whereNotIn('item_order_status', [1011, 1012]);
                });
        })->get();

        foreach ($purchases as $purchase) {
            $req = new Request();

            $req->merge([
                'purchase_status' => 4005,
                'do_number' => $purchase->orders->pluck('delivery_order'),
                'po_number' => $purchase->orders->pluck('order_number')
            ]);

            (new PaymentController)->update($req, $purchase->id);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
