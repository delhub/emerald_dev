<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateShippingRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('shipping_rates')
            ->where('zone_id', '1')
            ->where('category_id', '1')
            ->where('min_rates', '0.10')
            ->where('max_rates', '3.00')
            ->where('price', '600')
            ->update(
                ['min_rates' => '0.00']
            );

        DB::table('shipping_rates')
            ->where('zone_id', '2')
            ->where('category_id', '1')
            ->where('min_rates', '0.10')
            ->where('max_rates', '1.00')
            ->where('price', '1700')
            ->update(
                ['min_rates' => '0.00']
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
