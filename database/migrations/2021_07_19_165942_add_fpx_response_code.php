<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFpxResponseCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_payment_responses', function (Blueprint $table) {
            $table->string('type')->default('card')->after('description');
        });

        DB::table('global_payment_responses')->insert( array(
            array('response_code' => '00','description' => 'Approved','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '03','description' => 'Invalid Merchant','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '05','description' => 'Invalid Seller Or Acquiring Bank Code','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '09','description' => 'Transaction Pending','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '12','description' => 'Invalid Transaction','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '13','description' => 'Invalid Amount','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '14','description' => 'Invalid Buyer Account','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '20','description' => 'Invalid Response','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '30','description' => 'Format Error','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '31','description' => 'Invalid Bank','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '39','description' => 'No Credit Account','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '45','description' => 'Duplicate Seller Order Number','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '46','description' => 'Invalid Seller Exchange Or Seller','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '47','description' => 'Invalid Currency','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '48','description' => 'Maximum Transaction Limit Exceeded','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '49','description' => 'Merchant Specific Limit Exceeded','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '50','description' => 'Invalid Seller for Merchant Specific Limit','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '51','description' => 'Insufficient Funds','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '53','description' => 'No Buyer Account Number','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '57','description' => 'Transaction Not Permitted','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '58','description' => 'Transaction To Merchant Not Permitted','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '70','description' => 'Invalid Serial Number','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '76','description' => 'Transaction Not Found','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '77','description' => 'Invalid Buyer Name Or Buyer ID','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '78','description' => 'Decryption Failed','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '79','description' => 'Host Decline When Down','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '80','description' => 'Buyer Cancel Transaction','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '83','description' => 'Invalid Transaction Model','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '84','description' => 'Invalid Transaction Type','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '85','description' => 'Internal Error At Bank System','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '87','description' => 'Debit Failed Exception Handling','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '88','description' => 'Credit Failed Exception Handling','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '89','description' => 'Transaction Not Received Exception Handling','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '90','description' => 'Bank Internet Banking Unavailable','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '92','description' => 'Invalid Buyer Bank','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '96','description' => 'System Malfunction','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '98','description' => 'MAC Error','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '99','description' => 'Pending Authorization (Applicable for B2B model)','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'BB','description' => 'Blocked Bank','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'BC','description' => 'Transaction Cancelled By Customer','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'DA','description' => 'Invalid Application Type','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'DB','description' => 'Invalid Email Format','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'DC','description' => 'Invalid Maximum Frequency','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'DD','description' => 'Invalid Frequency Mode','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'DE','description' => 'Invalid Expiry Date','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'DF','description' => 'Invalid e-Mandate Buyer Bank ID','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'FE','description' => 'Internal Error','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'OE','description' => 'Transaction Rejected As Not In FPX Operating Hours','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'OF','description' => 'Transaction Timeout','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'SB','description' => 'Invalid Acquiring Bank Code','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XA','description' => 'Invalid Source IP Address (Applicable for B2B2 model)','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XB','description' => 'Invalid Seller Exchange IP','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XC','description' => 'Seller Exchange Encryption Error','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XE','description' => 'Invalid Message','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XF','description' => 'Invalid Number Of Orders','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XI','description' => 'Invalid Seller Exchange','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XM','description' => 'Invalid FPX Transaction Model','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XN','description' => 'Transaction Rejected Due To Duplicate Seller Exchange Order Number','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XO','description' => 'Duplicate Exchange Order Number','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XS','description' => 'Seller Does Not Belong To Exchange','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XT','description' => 'Invalid Transaction Type','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => 'XW','description' => 'Seller Exchange Date Difference Exceeded','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1A','description' => 'Buyer Session Timeout At Internet Banking Login Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1B','description' => 'Buyer Failed To Provide The Necessary Info To Login To Internet Banking Login Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1C','description' => 'Buyer Choose Cancel At Login Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1D','description' => 'Buyer Session Timeout At Account Selection Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1E','description' => 'Buyer Failed To Provide The Necessary Info At Account Selection Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1F','description' => 'Buyer Choose Cancel At Account Selection Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1G','description' => 'Buyer Session Timeout At TAC Request Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1H','description' => 'Buyer Failed To Provide The Necessary Info At TAC Request Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1I','description' => 'Buyer Choose Cancel At TAC Request Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1J','description' => 'Buyer Session Timeout At Confirmation Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1K','description' => 'Buyer Failed To Provide The Necessary Info At Confirmation Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1L','description' => 'Buyer Choose Cancel At Confirmation Page','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '1M','description' => 'Internet Banking Session Timeout.','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '2A','description' => 'Transaction Amount Is Lower Than Minimum Limit','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('response_code' => '2X','description' => 'Transaction Is Canceled By Merchant','type' => 'fpx','created_at' => Carbon::now(),'updated_at' => Carbon::now())
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_payment_responses', function (Blueprint $table) {
            //
        });
    }
}
