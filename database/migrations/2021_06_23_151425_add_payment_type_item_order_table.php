<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentTypeItemOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->integer('payment_type')->default('7000')->after('panel_remarks');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('payment_type')->default('7000')->after('country_id');
        });
        Schema::table('purchases', function (Blueprint $table) {
            $table->removeColumn('payment_type');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
