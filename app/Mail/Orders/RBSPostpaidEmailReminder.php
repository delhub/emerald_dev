<?php

namespace App\Mail\Orders;

use App\Models\Dealers\DealerDirectPay;
use App\Models\Globals\Products\Product;
use App\Models\Rbs\RbsPostpaid;
use App\Models\Users\Customers\Cart;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class RBSPostpaidEmailReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $directPay;
    public $productName;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(DealerDirectPay $directPay, string $current_reminder = null)
    {
        $this->directPay = $directPay;
        $this->current_reminder = $current_reminder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $pdf = PDF::loadView('documents.invoice')->setPaper('a4');
        // dd(json_decode($this->productName));
        // dd($this->directPay->user->userInfo->full_name);
        $products = array();
        $directPayCartIDs = json_decode($this->directPay->cart_id);
        foreach ($directPayCartIDs as $key => $directPayCartID) {
            // dd($directPayCartID);
            $cart = Cart::where('id',$directPayCartID)->first();
            if ($cart) {
                if (array_key_exists('product_rbs', $cart->product_information)) {
                    $productInformation = $cart->product_information['product_rbs'];
                } else {
                    $productInformation = $cart->product_information['product_size'];
                }
                if (array_key_exists('product_rbs_frequency', $cart->product_information)) {
                    $deliveryFrequency = 'Delivery every ' . $cart->product_information['product_rbs_frequency'] . ' month(s) for ' . $cart->product_information['product_rbs_times'] . ' times.';
                }
                $products[$key] = $cart->product->parentProduct;
                $products[$key]->productInformation = $productInformation;
                $products[$key]->deliveryFrequency = $deliveryFrequency ?? null;
                $products[$key]->subTotalPrice = $cart->subtotal_price;
            }
        }
        $webLink = route('agent.direct.pay', ['directID' => $this->directPay->uuid_link, 'type' => 'rbs']);

        $reminder = $this->current_reminder ?? 'Reminder';

        return $this->subject('Formula Healthcare ' . $reminder . ' - Recurring Booking System (RBS) Postpaid')
            ->markdown('emails.orders.rbs-postpaid-email')
            ->with('webLink' , $webLink)
            ->with('directPay', $this->directPay)
            ->with('products', $products);

        // return $this->subject('Formula Healthcare Reminder - Recurring Booking System (RBS) Postpaid')
        //     ->view('emails.orders.rbs-postpaid-email')
        //     ->with('directPay', $this->directPay)
        //     ->with('webLink', $webLink)
        //     ->with('productNames', $productNames);
    }
}
