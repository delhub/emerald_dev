<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertPriceInAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->bigInteger('active_price')->default(0)->after('offer_price');
            $table->bigInteger('promo_price')->default(0)->after('active_price');
            $table->bigInteger('default_price')->default(0)->after('promo_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('active_price');
            $table->dropColumn('promo_price');
            $table->dropColumn('default_price');
        });
    }
}
