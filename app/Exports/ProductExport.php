<?php

namespace App\Exports;

use App\Models\Globals\Products\Product;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class ProductExport implements FromArray
{
    protected $product;

    public function __construct(array $product)
    {
        $this->product = $product;
    }

    public function array(): array
    {
        return $this->product;
    }
}
