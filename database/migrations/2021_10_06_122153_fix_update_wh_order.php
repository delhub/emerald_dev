<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;
use App\Models\Products\WarehouseInventories;

class FixUpdateWhOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $beforeFirstOctober = Carbon::now()->subDays(5)->startOfDay(); // 1st Octber 2021

        $inventories = WarehouseInventories::where('model_type', 'App\Models\Products\WarehouseBatchDetail')
            ->whereDate('created_at', '<', $beforeFirstOctober)
            ->get();
        foreach ($inventories as $key => $inventory) {
            $inventory->outlet_id = 999;
            $inventory->save();
        }

        $inventories2 = WarehouseInventories::where('model_type', 'App\Models\Warehouse\StockTransfer\StockTransfer')
            ->where('inv_type', 8013)
            ->get();
        foreach ($inventories2 as $key => $inventory) {
            $inventory->outlet_id = 2;
            $inventory->save();
        }

        $inventories999 = WarehouseInventories::where('model_type', 'App\Models\Warehouse\StockTransfer\StockTransfer')
            ->where('inv_type', 8014)
            ->get();
        foreach ($inventories999 as $key => $inventory) {
            $inventory->outlet_id = 999;
            $inventory->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
