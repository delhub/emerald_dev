<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Log;

use Closure;
use Illuminate\Support\Facades\Cookie;
use App\Models\Users\User;
use App\Models\Globals\State;
use App\Models\Globals\City;


class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */

    public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($request, $guards);
        $user = Auth::user();
        if (!request()->is('api*')) {
            if (!isset($user->userInfo) || $user->notes == 'incomplete') {

                return redirect(route('account.error'));
            }
            $addressCookie = json_decode(Cookie::get('addressCookie'));

            if ($addressCookie == NULL) {
                $this->createCookie();
            }
        }

        return $next($request);
    }

    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        }
    }

    private function createCookie()
    {
        $user = User::find(Auth::user()->id);
        $userState = State::where('id', $user->userInfo->shippingAddress->state_id)->first();
        $currentCountry = country()->country_id;

        if ($userState->country_id == $currentCountry) {
            $data = array(
                'name' => $user->userInfo->full_name,
                'mobile' => $user->userInfo->mobileContact->contact_num,
                'address_1' => $user->userInfo->shippingAddress->address_1,
                'address_2' => $user->userInfo->shippingAddress->address_2,
                'address_3' => $user->userInfo->shippingAddress->address_3,
                'postcode' => $user->userInfo->shippingAddress->postcode,
                'city' => $user->userInfo->shippingAddress->city,
                'city_key' => $user->userInfo->shippingAddress->city_key,
                'city_name' => $user->userInfo->shippingAddress->city_key != '0' ? City::where('city_key', $user->userInfo->shippingAddress->city_key)->first() : $user->userInfo->shippingAddress->city,
                'state_name' => State::find($user->userInfo->shippingAddress->state_id),
                'state_id' => $user->userInfo->shippingAddress->state_id
            );
        } elseif ($user->userInfo->cartShippingAddress != NULL) {
            $data = array(
                'name' => $user->userInfo->full_name,
                'mobile' => $user->userInfo->mobileContact->contact_num,
                'address_1' => $user->userInfo->cartShippingAddress->address_1,
                'address_2' => $user->userInfo->cartShippingAddress->address_2,
                'address_3' => $user->userInfo->cartShippingAddress->address_3,
                'postcode' => $user->userInfo->cartShippingAddress->postcode,
                'city' => $user->userInfo->cartShippingAddress->city,
                'city_key' => $user->userInfo->cartShippingAddress->city_key,
                'city_name' => $user->userInfo->cartShippingAddress->city_key != '0' ? City::where('city_key', $user->userInfo->cartShippingAddress->city_key)->first() : $user->userInfo->cartShippingAddress->city,
                'state_name' => State::find($user->userInfo->cartShippingAddress->state_id),
                'state_id' => $user->userInfo->cartShippingAddress->state_id
            );
        } else {
            $data = array(
                'name' => $user->userInfo->full_name,
                'mobile' => '',
                'address_1' => '',
                'address_2' => '',
                'address_3' => '',
                'postcode' => '',
                'city' => '',
                'city_key' => City::where('country_id', $currentCountry)->first()->city_key,
                'city_name' => City::where('country_id', $currentCountry)->first(),
                'state_id' => State::where('country_id', $currentCountry)->first()->id,
                'state_name' => State::where('country_id', $currentCountry)->first()
            );
        }

        Cookie::queue('addressCookie', json_encode($data), 3600);
    }
}
