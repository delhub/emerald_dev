<!-- Footer -->
<!DOCTYPE html>
<html>

<head>
     <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@200;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/monthly-statement.css') }}">
     @include('layouts.favicon')
</head>

<body>
    <table class="footer">
                <tbody>
                    <tr>
                        <td>
                            <hr class="dbl-line">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                This is computer generated statement no signature required. The company must be notified
                                of any discrepanct from the statement within 7 days upon received, otherwise the
                                information of the statement will be considered accurate.
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
</body>

</html>
<!-- End Footer -->
