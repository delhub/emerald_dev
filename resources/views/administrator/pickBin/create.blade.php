@inject('controller', 'App\Http\Controllers\Administrator\PickBin\PickBinController')

@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{route($controller::$home)}}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Create {{$controller::$header}}
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{route($controller::$store)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Bin number <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="bin_number" name="bin_number" class="form-control" value="{{ old('bin_number') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Location <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="location_id" class="form-control select2" required>
                                    <option value="">Please choose a location.</option>
                                    @foreach($locations as $location)
                                    <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


