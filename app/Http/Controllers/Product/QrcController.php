<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products\WarehouseBatch;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Products\ProductAttribute;
use Auth;
use App\Models\Users\User;
use Illuminate\Support\Facades\Session;
use App\Models\Pick\PickBatch;
use App\Models\Pick\PickBatchDetail;
use App\Http\Controllers\Administrator\Pick\PickBatchController;
use App\Models\Warehouse\StockTransfer\StockTransfer;
use App\Models\Warehouse\StockTransfer\StockTransferItem;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundle;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundleItem;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Warehouse\StockTransfer\StockTransferController;

class QrcController extends Controller
{

    public function qrItemDetail(Request $request, $id)
    {
        $data = explode("-", $id);
        $detail_id = (int) $data[1];

        $batch = WarehouseBatchDetail::where([['batch_id', $data[0]], ['id', $detail_id]])->first();

        if ($user = Auth::user()) {
            $sortData = Session::get('sortingProcess');

            if ($user->hasRole('wh_sorter') && $sortData != NULL) {

                return $this->sorter($request, $sortData, $batch, $id);
            } elseif ($user->hasRole('wh_transferrer')) {

                return $this->transferrer($batch, $id);
            } elseif ($user->hasRole('wh_receiver')) {

                return $this->receiver($batch, $id, $user);
            } elseif ($user->hasRole('wh_b_unbundle') || $user->hasRole('wh_supervisor')) {

                return $this->bUnbundle($batch, $id);
            }
        }

        if ($batch) {
            $attribute = ProductAttribute::Where('product_code', $batch->product_code)->first();
            $warehouseBatchItem = WarehouseBatchItem::where('items_id', $id)->first();

            return view('documents.item-info')
                ->with('pageTitle', $batch->product_code)
                ->with('attribute', $attribute)
                ->with('batch', $batch)
                ->with('warehouseBatchItem', $warehouseBatchItem);
        }
    }

    private function sorter($request, $sortData, $batch, $id)
    {
        //will compare different for next clean code
        foreach ($sortData['sortingReadyDO']->items as $firstKey => $item) {

            $warehouseBatchItem = WarehouseBatchItem::where('items_id', $id)->first();
            $whPickingOrder = PickBatchDetail::where('delivery_order', $item->delivery_order)->first();

            // ********************compare start
            //different
            if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2) {
                //this is for bundle product

                //different
                $go = 0;
                foreach ($item->bundleChild as $key => $bundle) {

                    if ($go == 1) continue;

                    if ($bundle->sorted_qty == ($bundle->primary_quantity * $item->quantity)) {
                        $go = 0;
                    } else {
                        $go = 1;
                    }
                }

                if ($go == 1) {
                    //differnet
                    foreach ($item->bundleChild as $key => $bundle) {
                        //differnet
                        if (($bundle->primary_product_code == $batch->product_code)) {
                            if (isset($warehouseBatchItem) && isset($whPickingOrder) && $warehouseBatchItem->picking_order_id == NULL) {

                                //differnet
                                $bundle->sorted_qty += 1;

                                //differnet
                                if ($bundle->sorted_qty > ($bundle->primary_quantity * $item->quantity)) {
                                    return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))
                                        ->with('error', 'This product done, scan another product');
                                }
                                //differnet
                                $bundle->save();

                                $warehouseBatchItem->picking_order_id = $whPickingOrder->id;
                                $warehouseBatchItem->type = 'pick';

                                $warehouseBatchItem->save();
                                //differnet
                                if (($bundle->sorted_qty != ($bundle->primary_quantity * $item->quantity)) && $item->item_order_status != 1011 && $item->product->parentProduct->no_stockProduct != 1) {
                                    //this item not yet complete
                                    return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('success', 'Continue scan for product');
                                }

                                //Declare as completed
                                $allDone = 1;

                                $pickBatch = PickBatch::where('wh_picking.id', $sortData['sortingReadyDO']->key)
                                    ->where('wh_picking.pick_pack_status', 3)
                                    ->first();

                                foreach ($pickBatch->batchDetail as $key1 => $batchDO) {
                                    $detailDO = PickBatchDetail::where('wh_picking_order.delivery_order', $item->delivery_order)
                                        ->first();

                                    if ($detailDO != NULL) {
                                        foreach ($detailDO->items as $key2 => $doItem) {
                                            if ($doItem->item_order_status != 1011 && $doItem->product->parentProduct->no_stockProduct != 1) {

                                                //differnet if else
                                                if (isset($doItem->bundleChild) && ($doItem->bundleChild->first() != NULL) && $doItem->bundleChild->first()->active == 2) {

                                                    foreach ($doItem->bundleChild as $key => $bundle) {
                                                        if (($bundle->sorted_qty != ($bundle->primary_quantity * $doItem->quantity))) {
                                                            $allDone = 2;
                                                        }
                                                    }
                                                } else {
                                                    if (($doItem->sorted_qty != $doItem->quantity)) {
                                                        $allDone = 2;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($allDone == 2) continue;

                                    foreach ($batchDO->items as $key2 => $otherItem) {
                                        if ($otherItem->item_order_status != 1011 && $otherItem->product->parentProduct->no_stockProduct != 1) {

                                            //differnet if else
                                            if (isset($otherItem->bundleChild) && ($otherItem->bundleChild->first() != NULL) && $otherItem->bundleChild->first()->active == 2) {

                                                foreach ($otherItem->bundleChild as $key => $bundle) {
                                                    if (($bundle->sorted_qty != ($bundle->primary_quantity * $otherItem->quantity))) {
                                                        $allDone = 3;
                                                        continue;
                                                    }
                                                }
                                            } else {
                                                if (($otherItem->sorted_qty != $otherItem->quantity)) {
                                                    $allDone = 3;
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }

                                if ($allDone == 2) {
                                    return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('success', 'Continue scan for product');
                                } elseif ($allDone == 3) {
                                    return redirect(route('administrator.pickBatch.sortEnd-toScanDo-redirect', ['again' => 1, 'batch_id' => $whPickingOrder->id]))->with('success', 'Next DO / Product');
                                } else {

                                    //Done sorting
                                    $batch = PickBatchController::updateBinPickBatchDetailOrders($request, $pickBatch->bin->id, 3, 4, 'sort');
                                    if ($batch == 'error') return back()->with('error', 'Please refresh this page');

                                    return redirect(route('administrator.pickBatch.sortEnd'))->with('success', 'Completed Sort for Batch Number :' . $batch->batch_number . ' ');
                                }
                            } else {
                                return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('error', 'Product repeated scanned! Use another product!');
                            }
                        }
                    }
                }

                //******************** compare end
            } else {

                //this is for normal product
                if (($item->product_code == $batch->product_code)) {
                    if (isset($warehouseBatchItem) && isset($whPickingOrder) && $warehouseBatchItem->picking_order_id == NULL) {

                        $item->sorted_qty += 1;
                        if ($item->sorted_qty > $item->quantity) {
                            return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))
                                ->with('error', 'This product done, scan another product');
                        }
                        $item->save();

                        $warehouseBatchItem->picking_order_id = $whPickingOrder->id;
                        $warehouseBatchItem->type = 'pick';

                        $warehouseBatchItem->save();
                        if (($item->sorted_qty != $item->quantity) && $item->item_order_status != 1011 && $item->product->parentProduct->no_stockProduct != 1) {
                            //this item not yet complete
                            return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('success', 'Continue scan for product');
                        }

                        //Declare as completed
                        $allDone = 1;

                        $pickBatch = PickBatch::where('wh_picking.id', $sortData['sortingReadyDO']->key)
                            ->where('wh_picking.pick_pack_status', 3)
                            ->first();

                        foreach ($pickBatch->batchDetail as $key1 => $batchDO) {
                            $detailDO = PickBatchDetail::where('wh_picking_order.delivery_order', $item->delivery_order)
                                ->first();

                            if ($detailDO != NULL) {
                                foreach ($detailDO->items as $key2 => $doItem) {
                                    if ($doItem->item_order_status != 1011 && $doItem->product->parentProduct->no_stockProduct != 1) {

                                        //differnet if else
                                        if (isset($doItem->bundleChild) && ($doItem->bundleChild->first() != NULL) && $doItem->bundleChild->first()->active == 2) {

                                            foreach ($item->bundleChild as $key => $bundle) {
                                                if (($bundle->sorted_qty != ($bundle->primary_quantity * $doItem->quantity))) {
                                                    $allDone = 2;
                                                }
                                            }
                                        } else {
                                            if (($doItem->sorted_qty != $doItem->quantity)) {
                                                $allDone = 2;
                                            }
                                        }
                                    }
                                }
                            }

                            if ($allDone == 2) continue;

                            foreach ($batchDO->items as $key2 => $otherItem) {
                                if ($otherItem->item_order_status != 1011 && $otherItem->product->parentProduct->no_stockProduct != 1) {
                                    if (isset($otherItem->bundleChild) && ($otherItem->bundleChild->first() != NULL) && $otherItem->bundleChild->first()->active == 2) {
                                        foreach ($otherItem->bundleChild as $key => $bundle) {

                                            if (($bundle->sorted_qty != ($bundle->primary_quantity * $otherItem->quantity))) {
                                                $allDone = 3;
                                                continue;
                                            }
                                        }
                                    } else {
                                        if (($otherItem->sorted_qty != $otherItem->quantity)) {
                                            $allDone = 3;
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        // dd($allDone);
                        if ($allDone == 2) {
                            return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('success', 'Continue scan for product');
                        } elseif ($allDone == 3) {
                            return redirect(route('administrator.pickBatch.sortEnd-toScanDo-redirect', ['again' => 1, 'batch_id' => $whPickingOrder->id]))->with('success', 'Next DO / Product');
                        } else {

                            //Done sorting
                            $batch = PickBatchController::updateBinPickBatchDetailOrders($request, $pickBatch->bin->id, 3, 4, 'sort');
                            if ($batch == 'error') return back()->with('error', 'Please refresh this page');

                            return redirect(route('administrator.pickBatch.sortEnd'))->with('success', 'Completed Sort for Batch Number :' . $batch->batch_number . ' ');
                        }
                    } else {
                        return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('error', 'Product repeated scanned! Use another product!');
                    }
                }
            }
        }
        return redirect(route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $whPickingOrder->id]))->with('error', 'Wrong product Scanned! Please make sure correct DO and Product scanned!!');
    }

    private function transferrer($batch, $id)
    {
        $stockTransfer = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user', Auth::user()->id)
            ->whereNULL('session_end_time')
            ->where('status', 1)
            ->first();

        if ($stockTransfer != NULL) {
            $warehouseBatchItem = WarehouseBatchItem::where('items_id', $id)->whereNULL('picking_order_id')->where('location_id', $stockTransfer->from_location_id)->first();

            if ($warehouseBatchItem != NULL) {

                $transferItem = StockTransferItem::where('key', $stockTransfer->id)
                    ->where('batch_item_id', $warehouseBatchItem->id)
                    ->first();

                if ($transferItem == NULL) {
                    $transferItem = new StockTransferItem;

                    $transferItem->key = $stockTransfer->id;
                    $transferItem->batch_item_id = $warehouseBatchItem->id;
                    $transferItem->save();

                    return redirect(route('warehouse.StockTransfer.index'))
                        ->with('success', 'Continue scan for product or close session');
                } else {
                    return redirect(route('warehouse.StockTransfer.index'))
                        ->with('error', 'Product repeated');
                }
            } else {
                return redirect(route('warehouse.StockTransfer.index'))->with('error', 'Wrong batch item (' . $id . ') - used in other process or wrong location');
            }
        } else {
            return redirect(route('warehouse.StockTransfer.index'))->with('error', 'please start stock transfer!');
        }
    }

    private function receiver($batch, $id, $user)
    {
        $stockTransfer = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user_receive', $user->id)
            ->where('status', 4)
            ->first();

        if ($stockTransfer != NULL) {

            $warehouseBatchItem = WarehouseBatchItem::where('items_id', $id)
                ->whereNULL('picking_order_id')
                ->where('location_id', $stockTransfer->to_location_id)
                ->first();

            if ($warehouseBatchItem != NULL) {

                $transferItem = StockTransferItem::where('key', $stockTransfer->id)
                    ->where('batch_item_id', $warehouseBatchItem->id)
                    ->first();

                if ($transferItem != NULL) {

                    if ($transferItem->received == 1) {
                        return redirect(route('warehouse.StockTransfer.receiveScanProduct'))
                            ->with('error', 'Product repeated');
                    }

                    if ($transferItem->batchItem->batchDetail->product_code == $batch->product_code) {
                        if ($warehouseBatchItem->location_id == $stockTransfer->to_location_id) {

                            $transferItem->received = 1;
                            $transferItem->save();

                            foreach ($stockTransfer->stockTransferItems as $key1 => $tItem) {
                                if ($tItem->received == 0) {
                                    return redirect(route('warehouse.StockTransfer.receiveScanProduct', ['pickingOrderId' => $transferItem->id]))->with('success', 'Continue scan for product');
                                }
                            }

                            //update stock transfer status, session_end_time
                            $stockTransfer->status = 5;
                            $stockTransfer->session_end_time = Carbon::now();
                            $stockTransfer->save();

                            StockTransferController::receiveDone($stockTransfer->id);

                            return redirect(route('warehouse.StockTransfer.receive'))->with('success', 'Completed receving');
                        } else {
                            return redirect(route('warehouse.StockTransfer.receiveScanProduct'))->with('error', 'Wrong location to recevie');
                        }
                    } else {
                        return redirect(route('warehouse.StockTransfer.receiveScanProduct'))->with('error', 'Wrong product code scanned');
                    }
                } else {
                    return redirect(route('warehouse.StockTransfer.receiveScanProduct'))->with('error', 'Wrong stock transfer item ');
                }
            } else {
                return redirect(route('warehouse.StockTransfer.receive'))->with('error', 'Wrong batch item (' . $id . ') - used in other process or wrong location');
            }
        } else {
            return redirect(route('warehouse.StockTransfer.receive'))->with('error', 'please start stock receive!');
        }
    }

    private function bUnbundle($batch, $id)
    {
        $stockTransfer = WarehouseBunbundle::selectRaw('wh_product_b_unbundle.*')
            ->where('user_id', Auth::user()->id)
            ->where('completed', 0)
            ->first();

        if ($stockTransfer != NULL) {
            $warehouseBatchItem = WarehouseBatchItem::where('items_id', $id)->whereNULL('picking_order_id')->where('location_id', $stockTransfer->location_id)->first();

            if ($warehouseBatchItem != NULL) {

                $transferItem = WarehouseBunbundleItem::where('b_unbundle_key', $stockTransfer->id)
                    ->where('batch_item_id', $warehouseBatchItem->id)
                    ->first();

                if ($transferItem == NULL) {
                    $transferItem = new WarehouseBunbundleItem;

                    $transferItem->b_unbundle_key = $stockTransfer->id;
                    $transferItem->batch_item_id = $warehouseBatchItem->id;

                    $transferItem->save();

                    return redirect(route('warehouse.b_unbundle.index'))
                        ->with('success', 'Continue scan for product or close session');
                } else {
                    return redirect(route('warehouse.b_unbundle.index'))
                        ->with('error', 'Product repeated');
                }
            } else {
                return redirect(route('warehouse.b_unbundle.index'))->with('error', 'Wrong batch item (' . $id . ') - used in other process or wrong location');
            }
        } else {
            return redirect(route('warehouse.b_unbundle.index'))->with('error', 'please start stock transfer!');
        }
    }

    public function qrItemDetailProductCode(Request $request)
    {
        if ($request) {
            $attribute = ProductAttribute::Where('product_code', $request->info)->first();

            return view('documents.item-info-pc')
                ->with('pageTitle', $request->info)
                ->with('attribute', $attribute);
        }
    }
}
