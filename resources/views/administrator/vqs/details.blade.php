@extends('layouts.administrator.main')

@section('content')

@include('agent.vqs-part.summary-part')

@endsection

@push('style')
<style>
    .summary-details {
        background-color: #ececec;
    }

    .table-striped>tbody>tr:nth-child(even)>td,
    .table-striped>tbody>tr:nth-child(even)>th {
        background-color: #fff9e2;
    }

    .table-striped>tbody>tr:nth-child(odd)>td,
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #fff;
    }

    .agent-task::-webkit-scrollbar {
        height: 7px;
    }

    .agent-task::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px #ffcc00;
        border-radius: 10px;
        border: 1px solid #ffcc00;
    }

    .agent-task::-webkit-scrollbar-thumb {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px#000;
    }

    .agent-task td,
    th {
        border: 1px solid #dee2e6;
        text-align: center;
    }

    .archimedes-header {
        border-bottom: 1px solid #ffcc00;
        background: transparent;
    }

    .bjsh-btn-grey {
        color: #000;
        background: lightgrey;

    }

    .my_order_text {
        padding: 10px 20px;
        background: lightgrey;
    }

    .archimedes-title {
        font-size: 26px;
    }

    .tab-content {
        margin-top: -1px;
        background: transparent;
        border: none;
    }

    .nav-tabs .nav-link {
        color: #000;
        background: #b1b1b1;
    }

    .nav-tabs .nav-link.active,
    .nav-tabs .nav-link.active:focus {
        color: #000;
        background: #ffcc00;
    }

    .nav-tabs .nav-link:hover {
        color: #000;
        background: transparent;
        border: 1px solid #ffcc00;
    }

    .archimedesNav {
        min-width: 150px;
        text-align: center;
        font-weight: 700;
    }

    @media (max-width: 575.98px) {
        .summary-title {
            font-size: 12px;
            font-weight: 700;
        }

        .summary-desc {
            font-size: 12px;
        }

        .nav-tabs .nav-link {
            color: #000;
            background: transparent;
        }

        .archimedes-title {
            font-size: 16px;
        }

        .archimedes-orders {
            border-radius: 8px;
            padding: 7px 15px;
        }

        .archimedes-invoice {
            color: #ffcc00;
            font-size: 16px;
            text-align: center;
            font-weight: 600;
            padding: 10px 0px;
        }

        .archimedes-header,
        .summaryProductTitle {
            text-align: center;
        }

        .archimedes-image-title {
            text-align: center;
        }

        .archimedes-btn {
            padding: 10px;

        }

    }
</style>
@endpush

@push('script')
<script>
    $(".dropdown-menu li a").click(function(){
    $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <i class="fas fa-arrow-down"></i>');
    $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
    });

    function updateStatus(obj){
        let button = $(obj);
        let invoice =  button.data('invoice');
        let loader = button.find('.spinner-border');
        // let label = button.find('.fa-heart');
        //let count = button.find('.liked-btn');
        $.ajax({
            async: true,
            beforeSend: function() {
                // Show loading spinner.
                loader.show();

            },
            complete: function() {
                loader.hide();
            },
            url: '{{route("agent.special.task.update")}}',
            type: "POST",
            data: {
                id: invoice,
            },
            success: function(result) {

                console.log(result);

                // label.toggleClass('fas');
                // label.toggleClass('far');
                // console.log(button.data('count'));
                // //button.data('test' ,result.data.likes);
                // button.attr('data-count',result.data.likes);
            },
            error: function(result) {
                label.html( 'error');
            }
        });
    }
</script>
@endpush
