<?php

use App\Models\Globals\Gender;
use App\Models\Globals\Race;
use App\Models\Globals\Marital;
use App\Models\Globals\State;
use App\Models\Globals\Employment;
use App\Models\Purchases\Order;
use App\Models\Purchases\Purchase;
use App\Models\Users\Dealers\DealerAddress;
use App\Jobs\Emails\SendPurchaseOrderEmail;
use App\Jobs\Emails\SendInvoiceAndReceiptEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Users\User;
use App\Models\Globals\Cities;
use App\Models\Globals\Countries;
use App\Models\Products\ProductPrices;
use App\Http\Controllers\Agent\AgentController;
use Illuminate\Support\Facades\URL;
use App\Jobs\Emails\SendMassMail;
use Illuminate\Http\Request;
use App\Models\Purchases\Item;
use setasign\Fpdi\Fpdi;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Warehouse\Order\WarehouseOrder;
use App\Http\Controllers\Warehouse\Order\OrderController;

Route::get('correct-product-code', function (Request $request) {

    if ($request->input('code') != 'hidden-input')  return '500';

    $items = Item::all();
    $data = array();
    foreach ($items as $item) {
        $_data = array(
            'product_code' => $item->product_code,
            'attr_product_code' => $item->product_information['product_code'],
        );

        if ($_data['product_code'] != $_data['attr_product_code']) {
            $item->product_code = $_data['attr_product_code'];
            $item->save();
            $data[] = $_data;
        }
    }
    return $data;
});


Route::get('mass-email-send', function (Request $request) {

    if ($request->input('code') != 'red-alert')  return '500';

    $content = 'We want to apologize for the email sent out by <strong>No-Reply@FORMULA2U</strong> earlier due to system error.  Kindly ignore the email.  We sincerely apologize for any inconvenience we caused.
    <br/><br/>
    We’re aware of what happened and will take extra care in future.
    <br/><br/>
    For the invoice and receipt, you may download from our website (https://www.formula2u.com). <br/>
    <ol><li>Login to https://www.formula2u.com</li>
    <li>Go to "My Dashboard"</li>
    <li>Select "Value Records"</li>
    <li>Click on the "Invoice" and "Receipt" button to open the invoice/receipt and download.</li>
    </ol>

    If you have further enquiry, please do not hesitate to contact us at <br/>formula2u-cs@delhubdigital.com.
    <br/><br/>
    Thank you for your patience, and stay safe.';

    $title = 'Invalid Email Issue';
    $list = array(
        ['email' => 'bucgene@gmail.com', 'name' => 'Phoon Bucgene'],
    );
    foreach ($list as $recipient) {
        $email = $recipient['email'];
        $recipient_name = $recipient['name'];
        // SendMassMail::dispatch($email, $title, $content , $recipient_name);
    }

    // return view('emails.mass-mailing')->with('title', $title)
    // ->with('content', $content)
    // ->with('recipient_name', $recipient_name);


    return 200;
});


Route::get('qr-code-g', function () {
    QrCode::size(500)
        ->format('png')
        ->generate('ItSolutionStuff.com', public_path('images/qrcode.png'));

    return view('qr.qrCode');
});



Route::get('set-13-user', function () {

    $users = UserInfo::whereIn('group_id', array(13))->get();
    foreach ($users  as $user) {
        $dealer = DealerInfo::where('account_id', $user->referrer_id)->first();
        if (!isset($dealer->group_id)) continue;
        $user->group_id = $dealer->group_id;
        $user->save();
    }

    return 'Hello';
});


Route::get('price-list', function () {

    $productPrices = ProductPrices::orderBy('panel_product_prices.product_code', 'DESC')
        ->orderBy('model_type', 'ASC')->get();


    $lists = $item = array();

    foreach ($productPrices as $product) {

        if ($product->model_type == 'App\Models\Products\ProductAttribute') {
            $status = $product->model->product2->parentProduct->product_status;
        } else {

            $status = $product->model->parentProduct->product_status;
        }

        if ($status == 1) {

            $lists[$product->model_id]['product'][] = $product;
        }
    }

    foreach ($lists as $key => &$list) {

        $product = $list['product'][0];
        if ($product->price == 0) {
            unset($lists[$key]);
            continue;
        }
        $list['panelModelID'] = (isset($product->model['id'])) ? $product->model['id'] : '';
        $list['model_type'] = ($product->model_type == 'App\Models\Products\ProductAttribute') ? 'Attribute' : 'Panel Product';
        $list['product_code'] = (isset($product->model->parentProduct->product_code)) ? $product->model->parentProduct->product_code : $product->model['product_code'];
        $list['product_name'] = (isset($product->model->parentProduct->name)) ? $product->model->parentProduct->name : $product->model->product2->parentProduct->name . ' ' . $product->model['attribute_name'];
        $list['product_name'] = str_replace(',', '*',  $list['product_name']);
        $list['product_name'] = str_replace('"', 'in',  $list['product_name']);
        if (isset($product->model['markup_category'])) {
            if (!empty($product->model['markup_category'])) {
                $list['markup_category'] = $product->model->markupCategories[0]->cat_name;
            } else {
                $list['markup_category'] = 'DEFAULT';
            }
        } else {
            $list['markup_category'] = 'FOLLOW PARENT';
        }

        foreach ($list['product'] as $_product) {
            if ($_product->country_id == 'MY') {
                $list[$_product->country_id . '_fixed'] =  $_product->fixed_price / 100;
                $list[$_product->country_id . '_bujishu'] =  $_product->price / 100;
                $list[$_product->country_id . '_dc'] =  $_product->member_price / 100;
                $list[$_product->country_id . '_sv'] =  $_product->product_sv;
            } else {
                $priceModel = $_product->model->getPricingModel($_product->country_id);
                $list[$_product->country_id . '_fixed'] =  $priceModel->fixed_price / 100;
                $list[$_product->country_id . '_bujishu'] =  $priceModel->price / 100;
                $list[$_product->country_id . '_dc'] =  $priceModel->member_price / 100;
                $list[$_product->country_id . '_sv'] =  $priceModel->product_sv;
            }
        }
        unset($list['product']);
    }
    $data = 'id,type,product code,product name, markup category, MY fixed price, MY bujishu price, MY dc price, MY sv,SG fixed price, SG bujishu price, SG dc price, SG sv' . "\r\n";
    foreach ($lists as $list) {
        $data .= implode(',', $list) . "\r\n";
    }
    echo  nl2br($data);
});

Route::get('/correct-and-send-invoice/{purchaseNumber}', function ($purchaseNumber) {
    $purchase = Purchase::where('purchase_number', $purchaseNumber)->first();

    $purchase->purchase_amount = str_replace('.', '', $purchase->offline_payment_amount);
    $purchase->save();

    $user = $purchase->user;

    foreach ($purchase->orders as $order) {
        // Generate PO PDF.
        // Generate PDF.
        $pdf = PDF::loadView('documents.order.dealer-register-purchase-order', compact('order'));
        // Get PDF content.
        $content = $pdf->download()->getOriginalContent();
        // Set path to store PDF file.
        $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v' . $purchase->invoice_version . '/' : '/') . 'purchase-orders/');
        // Set PDF file name.
        $pdfName = $order->order_number;
        // Check if directory exist or not.
        if (!File::isDirectory($pdfDestination)) {
            // If not exist, create the directory.
            File::makeDirectory($pdfDestination, 0777, true);
        }
        // Place the PDF into directory.
        File::put($pdfDestination . $pdfName . '.pdf', $content);

        // Queue PO email to panels.
        SendPurchaseOrderEmail::dispatch($order->panel->company_email, $order);
    }

    // Generate Invoice PDF.
    // Generate PDF.
    $pdf = PDF::loadView('documents.purchase.dealer-register-invoice', compact('purchase'));
    // Get PDF content.
    $content = $pdf->download()->getOriginalContent();
    // Set path to store PDF file.
    $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v' . $purchase->invoice_version . '/' : '/'));
    // Set PDF file name.
    $pdfName = $purchase->getFormattedNumber();
    // Check if directory exist or not.
    if (!File::isDirectory($pdfDestination)) {
        // If not exist, create the directory.
        File::makeDirectory($pdfDestination, 0777, true);
    }
    // Place the PDF into directory.
    File::put($pdfDestination . $pdfName . '.pdf', $content);



    // Generate Receipt PDF.
    // Generate PDF.
    $pdf = PDF::loadView('documents.receipt.dealer-registration-receipt', compact('purchase'));
    // Get PDF content.
    $content = $pdf->download()->getOriginalContent();
    // Set path to store PDF file.
    $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v' . $purchase->invoice_version . '/' : '/'));
    // Set PDF file name.
    $pdfName = $purchase->getFormattedNumber() . '-receipt';
    // Check if directory exist or not.
    if (!File::isDirectory($pdfDestination)) {
        // If not exist, create the directory.
        File::makeDirectory($pdfDestination, 0777, true);
    }
    // Place the PDF into directory.
    File::put($pdfDestination . $pdfName . '.pdf', $content);

    // Queue Invoice email to customer.
    SendInvoiceAndReceiptEmail::dispatch($user->email, $purchase);
});

//Inovice PDF

//old file
// Route::get('/invoice', function () {
//     $purchase = Purchase::find(10993);

//     return $pdf = PDF::loadView('documents.purchase.v2.invoice', compact(['purchase', 'purchasedItemCollection']))
//         ->setPaper('a4')
//         ->stream('invoice');
// });

// Route::get('/invoice-view', function () {
//     $purchase = Purchase::find(10993);

//     return view('documents.purchase.v2.invoice', compact(['purchase', 'purchasedItemCollection']));
// });
//End Inovice PDF
Route::get('/test-hash', function () {
    $hitpay = new \App\Http\Controllers\Purchase\HitpayGatewayController;

    $request =  array(
        'payment_id' => '933c79e4-35ba-4da4-a57e-2fd95a3b3e82',
        'payment_request_id' => '933c79e3-a1cd-49c9-b565-27fe8cb76e5f',
        'phone' => NULL,
        'amount' => '44940.00',
        'currency' => 'SGD',
        'status' => 'completed',
        'reference_number' => '00000SBJN20210000019-607e2422872b7',
        'hmac' => 'a122edf1315ac0a298ff7f0ccace622708a2da99153371a81521d0ce52e2088a',
        'process' => 'hitpay',
    );

    $valuepair = $request;

    unset($valuepair['process']);
    unset($valuepair['hmac']);

    $hash = $hitpay::generateSignatureArray(env('HITPAY_SALT'), $valuepair);
    dd($hash, $request);
});


//tax-invoice PDF
Route::get('/invoice/{purchase_id}', function ($purchase_id) {

    $purchase = Purchase::find($purchase_id);
    // $countries = Countries::getAll();

    // $country = $countries[$purchase->country_id];

    // dd(Countries::getAll());

    // $headerHtml = view()->make('documents.purchase.v2.header-mysg',  compact(['purchase', 'purchasedItemCollection', 'country']))->render();
    // $footerHtml = view()->make('documents.purchase.v2.footer-sg',  compact(['purchase', 'purchasedItemCollection']))->render();
    $fileName = $purchase->getFormattedNumber();

    // $optionsMy = [
    // 'footer-html'   => $footerHtml,
    // 'header-html'      => $headerHtml,
    // 'margin-bottom'   => '12.5mm',
    // 'margin-top'   => '86.5mm'
    // ];
    //
    // $optionsSg = [
    // 'footer-html'   => $footerHtml,
    // 'header-html'      => $headerHtml,
    // 'margin-bottom'   => '12.5mm',
    // 'margin-top'   => '90.5mm'
    // ];

    //Create our PDF with the main view and set the options
    $pdf = PDF::loadView('documents.purchase.v2.invoice-v2',  compact(['purchase']));

    // switch ($purchase->country_id) {

    //     case "MY":
    //         $pdf->setOptions($optionsMy);
    //         break;
    //     case "SG":
    //         $pdf->setOptions($optionsSg);
    //         break;
    // }

    return $pdf->setPaper('a4')->stream($fileName . '.pdf');
});

Route::get('/invoice-view/{purchase_id}', function ($purchase_id) {
    $purchase = Purchase::find($purchase_id);

    return view('documents.purchase.v2.invoice-v2', compact(['purchase']));
});

//End tax-invoice PDF

Route::get('/testing', function () {

    return view('documents.purchase.v2.do-v2-view');
});

//CP cukai PDF
Route::get('/cp-cukai', function () {

    $pdf = PDF::loadView('documents.purchase.v2.cp-cukai');
    return $pdf->setPaper('a4')->stream('CP58_form' . '.pdf');
});

Route::get('/cp-cukai-view', function () {
    return view('documents.purchase.v2.cp-cukai');
});
//End CP cukai PDF

//CP statement PDF
Route::get('/yearly-statement', function () {
});

Route::get('/yearly-statement-view', function () {


    $data['total_amount'] = 1597.77;
    $data['biz_year'] = 2020;
    $data['date_generated'] = '28-04-2021';

    $data['record'] = [
        ['month' => 'JULY 2020', 'item' => 'Sales Revenue', 'amount' => 142.25],
        ['month' => 'JULY 2020', 'item' => 'Bluepoint Redemption', 'amount' => 452.25],
        ['month' => 'AUGUST 2020', 'item' => 'Sales Revenue', 'amount' => 185.50],
        ['month' => 'SEPTEMBER 2020', 'item' => 'Sales Revenue', 'amount' => 452.56],
        ['month' => 'SEPTEMBER 2020 ADJ', 'item' => 'Sales Revenue', 'amount' => -452.56],
        ['month' => 'DECEMBER 2020', 'item' => 'Sales Revenue', 'amount' => 365.21],

    ];
    return view('documents.statement.yearly-statement', compact('data'));
});
//End CP statement PDF

//Receipt PDF
Route::get('/receipt/{purchase_id}', function ($purchase_id) {
    $purchase = Purchase::find($purchase_id);

    $countries = Countries::getAll();

    $country = $countries[$purchase->country_id];

    return $pdf = PDF::loadView('documents.purchase.v2.receipt-v2', compact('purchase', 'country'))->stream('receipt');
});

//testing receipt
Route::get('/receipt-view/{purchase_id}', function ($purchase_id) {
    $purchase = Purchase::find($purchase_id);

    $countries = Countries::getAll();

    $country = $countries[$purchase->country_id];

    return view('documents.purchase.v2.receipt-v2')->with('purchase', $purchase);

    return $pdf = PDF::loadView('documents.purchase.v2.receipt-v2', compact('purchase', 'country'))->stream('receipt');
});


//testing
Route::get('/testing01', function () {

    // return view('documents.purchase.v2.do-v2-view');
    return $pdf = PDF::loadView('documents.purchase.v2.do-v2-view')->stream('receipt');
});


Route::get('/receipt2', function () {
    $purchase = Purchase::find(10993);

    return $pdf = PDF::loadView('documents.purchase.v2.receipt', compact('purchase'))->stream('receipt');
});

Route::get('/receipt-view', function () {
    $purchase = Purchase::find(845);

    return view('documents.purchase.v2.receipt')->with('purchase', $purchase);
});
//End Receipt PDF

//Purchase Order PDF
Route::get('/purchase-order/{purchase_id}', function ($purchase_id) {
    $order = Order::where('purchase_id', $purchase_id)->first();

    $countries = Countries::getAll();

    $country = $countries[$order->country_id];

    return $pdf = PDF::loadView('documents.purchase.v2.purchase-order', compact('order', 'country'))->stream('purchase-order');
});

Route::get('/purchase-order-view', function () {
    $order = Order::where('purchase_id', 845)->first();

    return view('documents.purchase.v2.purchase-order')
        ->with('order', $order);
});
//End Purchase Order PDF

//Delivery Order PDF old code
Route::get('/delivery-order/{purchase_id}', function ($purchase_id) {
    $poslaju = $orderDeliveryInfo  = array();

    $order = Order::where('purchase_id', $purchase_id)->first();

    $orderDeliveryInfo = json_decode($order->delivery_info);

    if ($orderDeliveryInfo != NULL) {
        $poslaju = $orderDeliveryInfo->checkout[0];
        // $poslajuPdf =  Storage::path('public/uploads/poslaju/sp1639966108_consignment_note.pdf');
        $poslajuPdf =  Storage::path('public/uploads/poslaju/' . $orderDeliveryInfo->consignment_note);
    }

    $DOloadPDF = PDF::loadView('documents.purchase.v2.delivery-order', compact('order', 'poslaju', 'orderDeliveryInfo', 'poslajuPdf'))->setpaper('a5');

    $content = $DOloadPDF->output();

    $pdfDestination = public_path('/storage/documents/invoice/' . $order->purchase->getFormattedNumber() . (($order->purchase->invoice_version != 0) ? '/v' . $order->purchase->invoice_version . '/' : '/') . 'delivery-orders/');

    $pdfName = $order->delivery_order;

    File::put($pdfDestination . $pdfName . '.pdf', $content);

    $newpdf = new Fpdi();

    $newpdf->setSourceFile($poslajuPdf);

    $tplIdx = $newpdf->importPage(1);

    $pageCount = $newpdf->setSourceFile($pdfDestination . $pdfName . '.pdf', $content);

    for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++) {

        $pageId = $newpdf->importPage($pageNum);

        $size = $newpdf->getTemplateSize($pageId);

        $newpdf->AddPage($size['orientation'], $size);

        $lastPageNum = $pageCount;

        if ($orderDeliveryInfo != NULL && $pageNum == $lastPageNum) {
            $newpdf->useTemplate($tplIdx, 0, null, 300, 210, false);
        } else {
        }

        $newpdf->useTemplate($pageId);
    }

    if ($pageCount == 3 || $pageCount == 5 || $pageCount == 7 || $pageCount == 9) {
        $newpdf->AddPage($size['orientation'], $size);
    } else {
    }

    $newpdf->Output('I');
});

//Delivery Order PDF testing
// Route::get('/delivery-order/{purchase_id}', function ($purchase_id) {
//     $poslaju = $orderDeliveryInfo  = array();

//     $order = Order::where('purchase_id', $purchase_id)->first();

//     $orderDeliveryInfo = json_decode($order->delivery_info);

//     if ($orderDeliveryInfo != NULL) {
//         $poslaju = $orderDeliveryInfo->checkout[0];
//         $poslajuPdf =  Storage::path('public/uploads/poslaju/' . $orderDeliveryInfo->consignment_note);
//     }

//     $DOloadPDF = PDF::loadView('documents.purchase.v2.delivery-order', compact('order', 'poslaju', 'orderDeliveryInfo', 'poslajuPdf'));
//     $content = $DOloadPDF->output();
//     $pdfDestination = public_path('/storage/documents/invoice/' . $order->purchase->getFormattedNumber() . '/delivery-orders/');
//     $pdfName = $order->delivery_order;

//     File::put($pdfDestination . $pdfName . '.pdf', $content);

//     $file = File::put($pdfDestination . $pdfName . '.pdf', $content);

//     $newpdf = new Fpdi();

//     $halfpage = 1;

//     $newpdf->setSourceFile($poslajuPdf);
//     $tplIdx = $newpdf->importPage(1);

//     $pageCount = $newpdf->setSourceFile($pdfDestination . $pdfName . '.pdf', $content);

//     for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++){

//         $pageId = $newpdf->importPage($pageNum);

//         $size = $newpdf->getTemplateSize($pageId);

//         if ($halfpage % 2 != 0) {

//             $newpdf->AddPage('L', 'A4');
//             $lastPageNum = $pageCount;

//             if($orderDeliveryInfo != NULL && $pageNum == $lastPageNum)
//             {
//                 $newpdf->useTemplate($tplIdx);
//             } else {

//             }
//             $newpdf->useTemplate($pageId);
//             $halfpage = $halfpage+1;

//         } else {

//             $newpdf->useTemplate($pageId, 180, 0);
//             $halfpage = $halfpage+1;

//         }

//     }

//     $newpdf->Output();
// });


Route::get('/delivery-order-view', function () {
    $order = Order::where('purchase_id',  835)->first();

    if ($order != null) {
        $orderDeliveryInfo = json_decode($order->delivery_info);
    } else {
        $orderDeliveryInfo = '';
    }
    //dd($orderDeliveryInfo,$orderDeliveryInfo->consignment_note);

    if ($orderDeliveryInfo != null) {
        $poslaju = $orderDeliveryInfo->checkout[0];
        $ss = $orderDeliveryInfo->consignment_note;
    } else {
        $poslaju = "";
        $ss = "";
    }

    $poslajuPdf =  Storage::path('public/uploads/poslaju/' . $ss);

    return view('documents.purchase.v2.delivery-order')
        ->with('order', $order)
        ->with('poslaju', $poslaju);
});
//End Delivery Order PDF

//monthy statement
Route::get('/monthly-statement', function () {
    $pdf = new Fpdi();
    return $pdf = PDF::loadView('documents.purchase.v2.monthy-statement')->setPaper('a4')->stream('monthy-statement');
});

//monthy statement view
Route::get('/monthly-statement-view', function () {
    return view('documents.purchase.v2.monthy-statement');
});

Route::get('/guzzle', function () {
    $client = new \GuzzleHttp\Client();

    $request = $client->get("https://" . BujishuAPI::apiServer() . "/wp-json/bjs/v1/customer/1913000002/");
    $response = json_decode($request->getBody(), true);

    return $response;
});

Route::get('/check-hash', function () {

    $request =   array(
        'result' => '00T60048000000BJN202100033271965092500000007279302',
        'response' => '00',
        'authCode' => 'T60048',
        'invoiceNo' => '000000BJN20210003327',
        'PAN' => '1965',
        'expiryDate' => '0925',
        'amount' => '000000072793',
        'ECI' => '02',
        'securityKeyRes' => '28j/2KNhis4iYQcuj0/JYf7OeTxzwsGvhk+cbw6wrzo=',
        'hash' => '28j/2KNhis4iYQcuj0/JYf7OeTxzwsGvhk+cbw6wrzo=',
    );

    // $purchase = Purchase::where('purchase_number', $request['invoiceNo'] )->firstOrFail();
    //dd($purchase->purchase_amount, sprintf('%012d', $purchase->purchase_amount), );
    $response = $request['response'];
    $authCode = $request['authCode'];
    $invoiceNo = $request['invoiceNo'];
    $PAN = $request['PAN'];
    $expiryDate = $request['expiryDate'];
    // $amount =  sprintf('%012d', $purchase->purchase_amount);
    $amount = $request['amount'];
    $secretCode = 'DCSGLVP01';
    $checkhash = base64_encode(hash('sha256', $authCode . $response . $invoiceNo . $secretCode . $PAN . $expiryDate . $amount, true));
    $hash = $request['hash'];

    return  dd($checkhash == $hash);
});


//Print QR code layout
Route::get('/qrcode_a4', function () {
    $batch = WarehouseBatchItem::get();
    return view('documents.qrcode-layout-a4')->with('batch', $batch);
});

//create warehouse order (for those missing warehouse order)
Route::get('create-warehouse-order', function (Request $request) {

    $warehouseOrder = WarehouseOrder::where('delivery_order', $request->delivery_order)->first();
    if (!isset($request->delivery_order)) {
        return ('wrong link, please insert delivery order ?delivery_order=');
    } elseif (isset($warehouseOrder)) {
        return ('warehouse order already exist, DO: ' . $request->delivery_order . '. warehouse order ID: ' . $warehouseOrder->id);
    } else {
        $order = Order::where('delivery_order', $request->delivery_order)->first();
        if (in_array($order->order_status, [1010, 1005, 1001])) {
            $data = Order::create_new_warehouse_order($order);
            OrderController::createNew($data);
            return ('created order, please check db. DO:' . $request->delivery_order);
        } else {
            return ('order status doesnt meet the requirement to create warehouse order! Order status: ' . $order->order_status . ' DO:' . $request->delivery_order);
        }
    }
});
