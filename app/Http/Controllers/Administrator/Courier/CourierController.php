<?php

namespace App\Http\Controllers\Administrator\Courier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Courier\Courier;
use App\Models\Purchases\Item;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\PoslajuAPI;
use App\Jobs\PDF\GeneratePdfPoDoEmail;
use App\Models\Purchases\Order;


class CourierController extends Controller
{
    public function index()
    {

        $couriers = Courier::orderBy('created_at', 'desc')->paginate(10);
        return view('administrator.courier.index', compact('couriers'));
    }

    public function create()
    {
        return view('administrator.courier.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'courier_name' => 'required',
        ]);

        $checkcouriername = Courier::where('courier_info.courier_name', $request->input('courier_name'))->first();

        if (!$checkcouriername) {
            $courier = new Courier;
            $courier->courier_name = strtoupper($request->input('courier_name'));
            $courier->ssm_number = $request->input('ssm_number');
            $courier->courier_email = $request->input('courier_email');
            $courier->courier_phone = $request->input('courier_phone');
            $courier->pic_name = $request->input('pic_name');
            $courier->pic_email = $request->input('pic_email');
            $courier->pic_contact = $request->input('pic_contact');

            $courier->save();

            return redirect('/administrator/courier/')->with('success', 'submit done.');
        } else {
            return back()->with(['error' => "Do not add same courier name !!! "]);
        }
    }

    public function edit($id)
    {
        $courier = Courier::find($id);
        return view('administrator.courier.edit')->with('courier', $courier);
    }


    public function update(Request $request, $id)
    {
        $courier = Courier::find($id);
        $courier->courier_name = $request->input('courier_name');
        $courier->ssm_number = $request->input('ssm_number');
        $courier->courier_email = $request->input('courier_email');
        $courier->courier_phone = $request->input('courier_phone');
        $courier->pic_name = $request->input('pic_name');
        $courier->pic_email = $request->input('pic_email');
        $courier->pic_contact = $request->input('pic_contact');

        $courier->save();

        return redirect('/administrator/courier/')->with('success', 'Courier Updates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courier = Courier::find($id);
        $courier->delete();
        return redirect('/administrator/courier/')->with('success', 'Courier Remove');
    }

    public static function createShipment($id)
    {
        $orders = Order::where('id', $id)->first();
        $itemTables = Item::where('order_number', $orders->order_number)->first();

        $order = $itemTables->order->purchase;
        $invoice = ltrim($order->purchase_number, '0');

        $client = new Client();
        $form_args = [

            'form_params' => [
                'api_key' => PoslajuAPI::apiKey(),
                'send_method' => 'pickup',
                'send_date' => Carbon::now()->toDateString(),
                'type' => 'parcel',
                'declared_weight' => '0.5', // 1KG
                'size' => 'box',
                'width' => '10',
                'length' => '10',
                'height' => '10',
                'provider_code' => 'poslaju',
                'content_type' => 'general',
                'content_description' => $itemTables->product->parentProduct ? $itemTables->product->parentProduct->name : 'Product',
                'content_value' => ($itemTables->subtotal_price / 100), // RM 00.00
                'sender_name' => 'FORMULA HEALTHCARE (' . $itemTables->delivery_order . ')',
                'sender_phone' => '0124546955',
                'sender_company_name' => $itemTables->delivery_order,
                'sender_email' => 'formula2u@gmail.com',
                'sender_address_line_1' => 'No 26, Jalan Sierra 10/2',
                'sender_address_line_2' => 'Sierra Zentro I,',
                'sender_address_line_3' => 'Bandar 16 Sierra,',
                'sender_address_line_4' => 'Puchong, Selangor',
                'sender_postcode' => '47110',
                'receiver_name' => $itemTables->order->purchase->ship_full_name . " ($invoice)",
                'receiver_phone' => $itemTables->order->purchase->ship_contact_num,
                'receiver_email' => $itemTables->order->purchase->user->email,
                'receiver_address_line_1' => $itemTables->order->purchase->ship_address_1,
                'receiver_address_line_2' => $itemTables->order->purchase->ship_address_2 ?: '',
                'receiver_address_line_3' => $itemTables->order->purchase->ship_address_3 ?: '',
                'receiver_address_line_4' => '',
                'receiver_postcode' => $itemTables->order->purchase->ship_postcode,
                // 'receiver_postcode' => '55555',
                'receiver_country_code' => 'MY',
            ]
        ];

        // return $form_args;

        $response = $client->post(PoslajuAPI::apiServer() . '/apiv1/create_shipment',    $form_args);
        $responseStatus = $response->getStatusCode();
        $request = json_decode($response->getBody(), true);

        if ($responseStatus == 200 && $request['status'] == 'true') {

            $orders->shipment_key = $request['data']['key'];

            $poslaju_checkout = true;

            if ($poslaju_checkout) {
                $checkout = PoslajuAPI::checkout($orders->shipment_key);

                $dt = Carbon::now();
                $time = $dt->toTimeString(); //14:15:16
                $day = $dt->addDays(1);

                // if ($time > '11:45:00') {
                //     $orders->est_shipping_date = $day->isoFormat('YYYY-MM-DD');
                //     $orders->shipping_date = $day->isoFormat('YYYY-MM-DD');
                // } else {
                //     $orders->est_shipping_date = $checkout[0]->declared_send_at;
                //     $orders->shipping_date = $checkout[0]->declared_send_at;
                // }

                $orders->tracking_number = $checkout[0]->tracking_no;

                $consignment = PoslajuAPI::get_consignment_note([$orders->tracking_number]);

                if (!isset($consignment)) {
                    sleep(5);
                    $consignment = PoslajuAPI::get_consignment_note([$orders->tracking_number]);
                }

                $pl = ['consignment_note' => $consignment, 'shipment' => $request, 'checkout' => $checkout];

                $orders->delivery_info = json_encode($pl);

                $orders->save();

                GeneratePdfPoDoEmail::generatePoDoPdf($orders);
            }

            $message = $request['message'];
        } else {
            $message = $request['message'];
        }

        return response()->json($message);
    }

    public function poslajuCheckout($id)
    {
        // $itemTables = Item::find($id);
        // $orders = Order::where('order_number', $itemTables->order_number)->first();
        $orders = Order::where('id', $id)->first();
        // $itemTables = Item::where('order_number', $orders->order_number)->first();

        $checkout = PoslajuAPI::checkout($orders->shipment_key);

        $dt = Carbon::now();
        $time = $dt->toTimeString(); //14:15:16
        $day = $dt->addDays(1);

        // if ($time > '11:45:00') {
        //     $orders->est_shipping_date = $day->isoFormat('YYYY-MM-DD');
        //     $orders->shipping_date = $day->isoFormat('YYYY-MM-DD');
        // } else {
        //     $orders->est_shipping_date = $checkout[0]->declared_send_at;
        //     $orders->shipping_date = $checkout[0]->declared_send_at;
        // }

        $orders->tracking_number = $checkout[0]->tracking_no;

        $consignment = PoslajuAPI::get_consignment_note([$orders->tracking_number]);

        $pl = ['consignment_note' => $consignment, 'shipment' => $orders, 'checkout' => $checkout];

        $orders->delivery_info = json_encode($pl);

        $orders->save();

        // return response()->json($checkout);
    }

    public function poslajuShip($id)
    {
        $itemTables = Order::where('id', $id)->first();
        // $checkout = PoslajuAPI::checkout($itemTables->shipment_key);

        $dt = Carbon::now();
        $time = $dt->toTimeString(); //14:15:16
        $day = $dt->addDays(1);

        if ($time > '11:45:00') {
            $itemTables->est_shipping_date = $day->isoFormat('YYYY-MM-DD');
            $itemTables->shipping_date = $day->isoFormat('YYYY-MM-DD');
        } else {
            $itemTables->est_shipping_date = $dt->isoFormat('YYYY-MM-DD');
            $itemTables->shipping_date = $dt->isoFormat('YYYY-MM-DD');
        }

        $itemTables->order_status = 1002;

        $itemTables->save();

        return true;
    }
}
