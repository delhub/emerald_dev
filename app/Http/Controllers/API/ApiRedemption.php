<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Dealers\DealerData;
use App\Models\Dealers\DealerStockLedger;
use App\Models\Dealers\DealerPrebuyRedemption;

class ApiRedemption extends Controller
{
    public function getDealerStock(Request $request) {
        $vstock = DealerData::where([['dealer_id',$request->id],['type','vqs']])->get(['id','dealer_id','name','value']);

        if(count($vstock)) {
            return $vstock;
        }
    }

    public function updateDealerStock(Request $request) {

        $vstock = DealerData::where('name', $request->name)->where('dealer_id', $request->dealer_id)->first();

        if($vstock) {
            $vstock->value -= $request->qty;

            if($vstock->save()) {
                if($preBuy = $this->insertDealer_prebuy_redemption($request)) {
                    $this->insertDealerStockledger(json_decode($request->stockLadger),$preBuy->id, $vstock->value);
                }

                return $request->name . " -" . $request->qty;
            }

            \Log::info($request->name. '-' . $request->qty . '--' . date('Y-m-d H:i:s'));
        }
    }

    public function insertDealer_prebuy_redemption($request) {
        $preBuy = new DealerPrebuyRedemption;

        $preBuy->dealer_id = $request->dealer_id;
        $preBuy->channel = $request->channel;
        $preBuy->prebuy_status = $request->prebuy_status;
        $preBuy->total_amount = $request->total_amount;
        $preBuy->redemption_data = $request->redemption_data;

        if($preBuy->save()){
            return $preBuy;
        }
    }

    public function insertDealerStockledger($request, $id, $balance = 0) {

        $stockLadger = new DealerStockLedger;

        $stockLadger->ref_id = $request->ref_id;
        $stockLadger->type = 5001;
        $stockLadger->product_code = $request->product_code;
        $stockLadger->prebuy_redemption_id = $id;
        $stockLadger->quantity = -$request->quantity;
        $stockLadger->unit_price = $request->unit_price;
        $stockLadger->subtotal_price = $request->subtotal_price;
        $stockLadger->balance = $balance;
        $stockLadger->item_id = $request->item_id;
        $stockLadger->agent_id = $request->agent_id;
        $stockLadger->date = Carbon::now();

        $stockLadger->save();
    }

}
