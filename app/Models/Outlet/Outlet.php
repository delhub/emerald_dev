<?php

namespace App\Models\Outlet;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    // Set table
    protected $table = 'outlet_infos';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'outlet_key',
        'outlet_name',
        'country_id',
        'printer_email',
        'contact_number',
        'default_address',
        'postcode',
        'city_key',
        'state_id',
        'operation_time'
    ];

}
