@extends('layouts.administrator.main')

@section('content')

    <h1>Zones</h1>

    <div class="card shadow-sm">
        <div class="card-body">

            <form action="{{ route('administrator.zones.index') }}" method="GET">
                <div class="row">

                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <select class="custom-select" id="selectSearch" name="selectSearch">
                                    <option value="ALL" selected>Choose...</option>
                                    {{-- <option value="ZI" {{ $request->selectSearch == 'ZI' ? 'selected' : '' }}>Zone Id</option> --}}
                                    <option value="NM" {{ $request->selectSearch == 'NM' ? 'selected' : '' }}>Zone Name</option>
                                    <option value="state" {{ $request->selectSearch == 'state' ? 'selected' : '' }}>State</option>
                                    <option value="city" {{ $request->selectSearch == 'city' ? 'selected' : '' }}>City</option>
                                    {{-- <option value="LT" {{ $request->selectSearch == 'LT' ? 'selected' : '' }}>Location Type</option>
                                    <option value="LI" {{ $request->selectSearch == 'LI' ? 'selected' : '' }}>Location Id</option> --}}
                                </select>

                                <input type="text" class="form-control" id="" name="searchBox"
                                    value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <button type="submit" class="btn btn-warning" style="color:black; position:absolute; width:85%;">Search</button>
                    </div>

                    <div class="col-md-1">
                        <button type="submit" class="btn btn-warning" style="color:black; position:absolute; width:85%; margin-left:15px;"><a href={{ route('administrator.zones.index')}} style="color:black;">Reset Search</a></button>
                    </div>

                    <div class="col-md-12">
                        <a href="{{ route('administrator.zones.create') }}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark float-right">Create New Zones</a>
                    </div>

                </div>

                <br>

                    <div class="row float-right">
                        <div class="col-md-12 " >
                            {!! $new_zones->render() !!}
                        </div>
                    </div>
            </form>

            <div class="table-responsive m-2">

                <table id="global-zones-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Country</td>
                            <td>Name</td>
                            <td>Sequence</td>
                            <td>Locations</td>
                            <td>Actions</td>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($new_zones as $new_zone)
                        <tr>
                            <td>{{$new_zone->id}}</td>
                            <td>{{$new_zone->country_id}}</td>
                            <td>{{$new_zone->zone_name}}</td>
                            <td>{{$new_zone->sequence}}</td>

                            @php
                                $zoneLocations = array();
                                    foreach ($new_zone->locations as $location) {
                                        if (($location) && $location->location_type == 'state') {
                                            $zoneLocations[] = $allStates->find($location->location_id)->name;
                                        }
                                        if (($location) && $location->location_type == 'city') {

                                            $zoneLocations[] = $allCities->find($location->location_id) ? $allCities->find($location->location_id)->city_name : '';
                                        }
                                        if (($location) && $location->location_type == 'country') {
                                            $zoneLocations[] = $allCountry->where('country_id',$location->location_id)->first()->country_name;
                                        }
                                    }
                            @endphp
                            <td>{{ implode(",",$zoneLocations) }}</td>

                            <td style="width: 20%;">
                                <a style="color: white; font-style: normal; border-radius: 5px;" href="{{ route('administrator.zones.edit',[$new_zone->id]) }}" class="btn btn-primary shadow-sm">Edit</a>
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
