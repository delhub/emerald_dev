<?php

namespace App\Models\Purchases;

use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Traits\AttributeImage;
use App\Models\Products\ViewInventoriesStock;
use App\Models\Purchases\ItemBundle;

class Item extends Model
{
    use AttributeImage;

    // Set table
    protected $table = 'items';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'id', 'order_number', 'delivery_order', 'item_order_status', 'ship_date', 'actual_ship_date', 'collected_date', 'created_at', 'updated_at', 'product_code', 'bundle_id'
    ];

    // Cast column to another datatype.
    protected $casts = [
        'product_information' => 'array'
    ];

    // Get order of the item.
    public function order()
    {
        return $this->belongsTo('App\Models\Purchases\Order', 'order_number', 'order_number');
    }
    public function childItem()
    {
        return $this->hasMany('App\Models\Purchases\Item', 'bundle_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo('App\Models\Products\Product', 'product_id');
    }

    public function attributes()
    {
        return $this->belongsTo('App\Models\Products\ProductAttribute', 'product_code', 'product_code');
    }

    public function attributeBundle()
    {
        return $this->belongsTo('App\Models\Products\ProductAttributeBundle', 'product_code', 'product_code');
    }
    public function primaryAttributeBundle()
    {
        return $this->hasMany('App\Models\Products\ProductAttributeBundle', 'primary_product_code', 'product_code');
    }
    public function bundleChild()
    {
        return $this->hasMany(ItemBundle::class, 'item_id', 'id');
    }

    /**
     * Get shipping / installation category
     *      */
    public function installationCategories()
    {
        if (array_key_exists('product_installation', $this->product_information)) {
            return Ship_category::where('id', $this->product_information['product_installation'])->first();
        }
    }

    /***Get pending days of the order ***/
    public function getPendingAttribute()
    {
        if (in_array($this->item_order_status, [1001, 1002, 1005, 1010])) {
            $now = Carbon::now();
            $now->endOfDay();
            $date = Carbon::parse($this->order->purchase->created_at ?? '');
            $date->startOfDay();
            return  $date->diffInDays($now);
        } else {
            return 0;
        }
    }

    // Bundle sub item show parentName
    public function getParentName($bundleID)
    {
        $bundleMains = $this::find($bundleID);

        if (isset($bundleMains))  return $bundleMains->product->parentProduct->name;

        return $bundleID;
    }

    // Filter Panel ID
    public function scopepanelFilter($query, $panelFilter)
    {
        if (!is_null($panelFilter)) {

            if ($panelFilter == 'all') {
                return $query->whereRaw('1 = 1');
            } else {
                return $query->where('panel_id', $panelFilter);
            }
        }

        return $query;
    }

    //Stock Filter
    public function scopestockFilter($query, $stock_filter)
    {
        if (!is_null($stock_filter)) {
            if ($stock_filter == 'all') {

                return $query->whereRaw('1 = 1');
            } else {

                $inventory = ViewInventoriesStock::where('physical_stock', 0)->get();

                $productCode = array();
                foreach ($inventory as $key => $attribute) {
                    $productCode[] = $attribute->product_code;
                }

                $order_numbers = $this->whereIn('product_code', $productCode)->pluck('order_number');

                if ($stock_filter == 'has_stock') {
                    return $query->whereNotIn('items.order_number', $order_numbers);
                } elseif ($stock_filter == 'no_stock') {
                    return $query->whereIn('items.order_number', $order_numbers);
                }
            }
        }

        return $query;
    }

    //States
    public function scopestatesFilter($query, $statesFilter)
    {
        if (!is_null($statesFilter)) {
            if ($statesFilter == 'all') {

                return $query->whereRaw('1 = 1');
            } else {

                $purchases = DB::table('purchases')
                    ->where('ship_state_id', $statesFilter)->get();

                $purchases_array = [];
                foreach ($purchases as $purchase) {
                    $purchases_array[] =  $purchase->id;
                }

                // return $query->whereIn('id', $purchases_array);

                return $query->whereIn('orders.purchase_id', $purchases_array);
            }
        }

        return $query;
    }

    // Search Bar
    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if (($selectSearch == 'PODO')) {
                return $query
                    ->where('items.order_number', 'like', '%' . $searchFilter . '%')
                    ->orwhere('items.delivery_order', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'INV') {
                return $query
                    ->join('purchases', 'purchases.id', '=', 'orders.purchase_id')
                    ->where('purchases.inv_number', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'BJN') {
                return $query
                    ->join('purchases', 'purchases.id', '=', 'orders.purchase_id')
                    ->where('purchases.purchase_number', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'NAME') {
                return $query
                    // ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
                    // ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
                    ->where('global_products.name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'CODE') {
                return $query
                    ->where('items.product_code', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }

    // status2
    public function scopestatusFilter($query, $statusFilter, $dateFilter, $periodFilter)
    {
        if (!is_null($statusFilter)) {
            if ($statusFilter == 'all') {

                return $query->whereRaw('1 = 1');
            } elseif ($statusFilter == 'pre-order') {

                if ($dateFilter != '') {

                    if ($periodFilter == 'd') {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereDate('items.created_at', $dateFilter);
                    } elseif ($periodFilter == 'w') {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereBetween('items.created_at', [Carbon::parse($dateFilter)->startOfWeek(), Carbon::parse($dateFilter)->endOfWeek()]);
                    } elseif ($periodFilter == 'm') {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereMonth('items.created_at', date('m', strtotime($dateFilter)));
                    } else {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereDate('items.created_at', $dateFilter);
                    }
                } else {

                    if ($periodFilter == 'd') {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereDate('items.created_at', Carbon::now());
                    } elseif ($periodFilter == 'w') {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereBetween('items.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                    } elseif ($periodFilter == 'm') {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}')
                            ->whereMonth('items.created_at', Carbon::now());
                    } else {
                        return $query->where('items.product_information', 'like', '{%' . "product_preorder_date" .  '%}');
                    }
                }
            } else {
                $arraystatus = explode(",", $statusFilter);

                return $query->whereIn('orders.order_status', $arraystatus);
            }
        }

        return $query;
    }

    // Date
    public function scopedateFilter($query, $dateFilter, $periodFilter)
    {
        if ($periodFilter == 'd') {
            if ($dateFilter != '') {


                return $query->whereDate('items.created_at', $dateFilter);
            } else {

                return $query->whereDate('items.created_at', Carbon::now());
            }
        } elseif ($periodFilter == 'm') {

            if ($dateFilter != '') {


                return $query->whereMonth('items.created_at', date('m', strtotime($dateFilter)));
            } else {

                return $query->whereMonth('items.created_at', Carbon::now());
            }
        } elseif ($periodFilter == 'w') {
            // From a date string

            if ($dateFilter != '') {

                // $datetime =  Carbon::parse($dateFilter);
                // $datetime =  Carbon::create($dateFilter, 'America/Toronto');

                // return $query = \DB::select( \DB::raw("WHERE 'created_at' BETWEEN ('$dateFilter' - INTERVAL 7 DAY) AND '$dateFilter' "));
                return $query->whereBetween('items.created_at', [Carbon::parse($dateFilter)->startOfWeek(), Carbon::parse($dateFilter)->endOfWeek()]);
            } else {

                // return $query = \DB::select( \DB::raw( "WHERE 'created_at' BETWEEN ('$date' - INTERVAL 7 DAY) AND '$date' "));
                return $query->whereBetween('items.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            }
        } elseif ($periodFilter == 'all') {

            if ($dateFilter != '') {


                return $query->whereDate('items.created_at', $dateFilter);
            } else {

                return $query->whereRaw('1 = 1');
            }
        }

        return $query->whereRaw('1 = 1');
        if (!is_null($dateFilter)) {


            return $query->whereDate('items.created_at', $dateFilter);
        }

        return $query->whereRaw('1 = 1');
    }

    //date filter from date to date
    public function scopedatebetweenFilter($query, $datefrom, $dateto)
    {
        $from = Carbon::parse($datefrom);
        $to = Carbon::parse($dateto);

        $from->startOfDay();
        $to->endOfDay();

        if (!is_null($datefrom) && !is_null($dateto)) { //when user entry datefrom & dateto

            return $query
                ->whereBetween('orders.created_at', [$from, $to]);
        } elseif (isset($datefrom)) { //when user entry datefrom only

            $endOfDatefrom = Carbon::parse(' ' . $datefrom . ' 23:55:59');

            return $query
                ->whereBetween('orders.created_at', [$from, $endOfDatefrom]);
        } elseif (isset($dateto)) { //when user entry dateto only

            $startOfDateto = Carbon::parse(' ' . $dateto . ' 00:00:00');

            return $query
                ->whereBetween('orders.created_at', [$startOfDateto, $to]);
        }
    }

    // find items.id
    public function scopefindID($query, $ids)
    {
        $findid = explode(",", $ids);

        if (!is_null($ids)) {

            return $query
                ->whereIn('items.id', $findid);
        }
    }

    public function ProductWarehouseQty()
    {
        $result = 0;

        if (array_key_exists('product_size_id', $this->product_information)) {
            $sizeId = $this->product_information['product_size_id'];

            $result = $this->product->sizeAttributes->where('id', $sizeId)->first();
        }

        return $result;
    }
}
