<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formula - Receipt {{ $purchase->purchase_number }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/a4-receipt-invoice.css') }}">
    @include('layouts.favicon')
</head>

<body>
    <div class="a4">
        <div class="header-invoice">
            <table>
                <tr width="100%">
                    <td width="30%">
                        <img class="logo-head" src="{{ asset('images/formula/post-img/logo.png') }}">
                    </td>
                    <td class="company-name" width="50%">

                        <table width="100%">
                            <tr>
                                <td class="header-add">
                                    <p>Formula Healthcare Sdn. Bhd.<br><span>201901009101 (1318429-V)</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="header-contact">
                                    <p>1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="header-contact">
                                    <p>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                </td>
                            </tr>
                            <tr class="company-details">
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="contact-info">
                                                    <p>
                                                        <img class="ci-width"
                                                            src="{{ asset('images/formula/post-img/do-c-icon1.png') }}">formula2u-cs@delhubdigital.com
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="contact-info">
                                                    <p>
                                                        <img class="ci-width"
                                                            src="{{ asset('images/formula/post-img/do-c-icon3.png') }}">formula2u.com
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%"></td>
                </tr>
            </table>
        </div>
        <div class="receipt-body">
            <div class="receipt-dt">
                <H1>OFFICIAL RECEIPT</H1>
            </div>
            <div class="receipt-dt-box" width="100%">
                <table width="100%">
                    <tr>
                        <td width="60%">
                            <div class="receipt-d-info-left">
                                <table width="100%">
                                    <tr>
                                        <td class="font-bold w-35">
                                                {{ isset($purchase->ship_full_name) ? $purchase->ship_full_name : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold w-35">
                                                {{ isset($purchase->ship_contact_num) ? $purchase->ship_contact_num : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        @php
                                            $lines = [];

                                            // Row 1: Address 1 and Address 2
                                            $line1 = ucwords(rtrim($purchase->ship_address_1, ','));
                                            if (str_replace(',', '', $purchase->ship_address_2)) {
                                                $line1 .= ', ' . ucwords(rtrim($purchase->ship_address_2, ','));
                                            }
                                            $lines[] = $line1;

                                            // Row 2: Address 3
                                            if (str_replace(',', '', $purchase->ship_address_3)) {
                                                $lines[] = ucwords(rtrim($purchase->ship_address_3, ','));
                                            }

                                            // Row 3: Postcode and City
                                            $postcode = $purchase->ship_postcode ?? '';
                                            $city = $purchase->cityKey->city_name ?? $purchase->ship_city ?? '';
                                            if ($postcode || $city) {
                                                $lines[] = trim($postcode . ' ' . ucwords($city), ',');
                                            }

                                            // Row 4: State and "Malaysia"
                                            $state = $purchase->state->name ?? '';
                                            $lines[] = trim(ucwords($state), ',') . ', Malaysia.';
                                        @endphp

                                        <td class="invoice-add2" colspan="3">
                                            @foreach ($lines as $line)
                                                <p>{{ $line }}</p>
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="40%">
                            <div class="receipt-d-info-right">
                                <table>
                                    <tr>
                                        <td class="font-bold">
                                            OFFICIAL RECEIPT NO.
                                        </td>
                                        <td>:</td>
                                        <td>
                                            {{ $purchase->receipt_number }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">
                                            Date
                                        </td>
                                        <td>:</td>
                                        <td>
                                            {{ $purchase->purchase_date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">
                                            Payment Method
                                        </td>
                                        <td>:</td>
                                        <td>
                                            @if ($purchase->purchase_type == 'Metapoint')
                                                MES eVoucher Cash
                                            @else
                                                {{ $purchase->purchase_type }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">
                                            Reference No.
                                        </td>
                                        <td>:</td>
                                        <td>
                                            {{ $purchase->offline_reference }}
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                        <td class="font-bold">
                                            Received in
                                        </td>
                                        <td>:</td>
                                        <td>
                                            PBB
                                        </td>
                                    </tr> --}}
                                    <tr>
                                        <td class="font-bold">
                                            Status
                                        </td>
                                        <td>:</td>
                                        <td>
                                            {{ $purchase->user->userInfo->showUserLevel() }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="receipt-description">
                <table class="table-width">
                    <thead class="dark-clr2">
                        <tr>
                            <th width="10%">NO.</th>
                            <th colspan="2">Invoice Number</th>
                            <th width="20%">AMOUNT <br> (RM)</th>
                        </tr>
                    </thead>
                    <tbody>

                        @php
                            /*
                                                                                $no = 0;

                                                                                $subTotal = 0;
                                                                                $grandTotal = 0;

                                                                                $deliveryFee = ($purchase->getFeeAmount('shipping_fee') != NULL) ? $purchase->getFeeAmount('shipping_fee') : NULL;
                                                                                $rebate_fee = ($purchase->getFeeAmount('rebate_fee') != NULL) ? $purchase->getFeeAmount('rebate_fee') : NULL;

                                                                                foreach ($purchase->items as $item){
                                                                                    $no ++;
                                                                                    $size = NULL;

                                                                                    if(array_key_exists('product_size', $item->product_information)){
                                                                                        $size ='(Size:'.$item->product_information['product_size'].')';
                                                                                    }
                                                                                */
                            echo '
                            <tr>
                                <td class="fb" width="10%" style="font-size:18px;line-height: 20px;font-weight:normal;">1</td>
                                <td colspan="2" width="70%" class="table-line">
                                    <h4 style="font-size:18px;line-height: 20px;font-weight:normal;margin: 0;padding: 0;text-align: left;">
                                        ' .
                                $purchase->inv_number .
                                '
                                    </h4>
                                    ';
                            // if (!empty($item->subtotal_point)) {
                            //     echo'
//         <div class="i-promo-tag">
//             <p style="font-size:16px;line-height: 20px;font-weight:normal;">Point applied: '.number_format(($item->subtotal_point / 100), 2).'</p>
//         </div>
//     ';
                            // }
                            echo '
                                </td>
                                <td class="fb" width="20%" style="font-size:18px;line-height: 20px;font-weight:normal;">' .
                                number_format($purchase->purchase_amount / 100, 2) .
                                '</td>
                            </tr>
                            ';

                            // $subTotal += $item->subtotal_price;

                            $successPayment = isset($purchase->successfulPayment) ? $purchase->successfulPayment->amount : 0;
                            // }

                            // $subTotal = $subTotal + $deliveryFee;
                            //no tax
                            // $grandTotal = $subTotal - $rebate_fee;

                            //calculate balance
                            $balanceDue = $purchase->purchase_amount - $successPayment;
                        @endphp
                        {{-- @if (!empty($deliveryFee))
                        @php $no++ @endphp

                        <tr>
                            <td class="i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">{{$no}}</td>
                            <td colspan="2" width="70%" class="table-line">

                            <h4 style="font-size:18px;line-height: 20px;font-weight:normal;margin: 0;padding: 0;text-align: left;">Delivery Fee</h4>
                            </td>
                            <td class="fb" width="20%" style="font-size:18px;line-height: 20px;font-weight:normal;">
                                {{number_format(($deliveryFee / 100), 2)}}
                            </td>
                        </tr>
                        @endif

                        @if (!empty($rebate_fee))
                        @php $no++ @endphp

                        <tr>
                            <td class="fb" width="10%" style="font-size:18px;line-height: 20px;font-weight:normal;">{{$no}}</td>
                            <td colspan="2" width="70%" class="table-line">

                            <h4 style="font-size:18px;line-height: 20px;font-weight:normal;margin: 0;padding: 0;text-align: left;">Discount Fee</h4>
                            <td class="fb" width="20%" style="font-size:18px;line-height: 20px;font-weight:normal;">
                                - {{number_format(($rebate_fee / 100), 2)}}
                            </td>
                        </tr>
                        @endif --}}

                        <tr class="table-line-top">
                            <td class="tnc-fs" colspan="2" rowspan="2" width="65%">
                                <p width="70%">
                                    The issuance of this Receipt shall be deemed as your acceptance of the terms
                                    and
                                    conditions
                                    of this Disclaimer. The Formula Healthcare Agent Agreement shall only become
                                    effective
                                    after
                                    full
                                    payment is received by the Company.
                                </p>
                            </td>
                            <td class="table-line total-bb r-tot-text font-bold" width="15%">Total Paid</td>
                            <td class="fb total-bb dark-clr2 r-tot-text" width="20%">
                                {{ $purchase->purchase_type != 'Voucher'? number_format($successPayment / 100, 2): number_format($purchase->purchase_amount / 100, 2) }}
                            </td>
                        </tr>
                        <tr class="table-line-top">
                            <td class="table-line total-bb r-tot-text font-bold" width="15%">Balance</td>
                            <td class="fb total-bb r-tot-text" width="20%">
                                {{ $purchase->purchase_type != 'Voucher' ? number_format($balanceDue / 100, 2) : '0.00' }}
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
        <div class="receipt-footer">
            <div class="footer-dp2">
                <p>This is computer generated receipt, no signature is required.</p>
            </div>
        </div>
    </div>
</body>

</html>
