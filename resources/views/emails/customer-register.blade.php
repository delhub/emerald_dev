<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('layouts.favicon')
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>New membership application</title>
</head>

<body>
    <div class="container my-5 text-center">
        <div class="row">
            <div class="col-12">
                <img src="/assets/images/miscellaneous/letterhead/letterhead-logo.jpg" class="img w-25 my-3"
                    alt="Bujishu Logo">
            </div>
        </div>
        <div class="row" style="box-shadow: 0px 0px 9px 0px #cacaca;">
            <div class="col-12">
                <div class="card m-3 border-0">
                    <div class="card-header py-3" style="background-color: #FCCB34">
                        <h5 class="m-0">New membership application</h5>
                    </div>
                    <div class="card-body text-left">
                        {{-- <h5 class="card-title">New membership application</h5> --}}
                        <div class="row">
                            <div class="col-4">Customer status</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->new_or_existing }}</div>
                        </div>
                        <div class="row">
                            <div class="col-4">Name</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->full_name }}</div>
                        </div>
                        <div class="row">
                            <div class="col-4">Contact Number (Mobile)</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->mobile_number }}</div>
                        </div>
                        <div class="row"
                            style="{{ $membership->new_or_existing == 'New' ? 'display:none;' : '' }}">
                            <div class="col-4">Years become DC</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->years_joined }}</div>
                        </div>
                        <div class="row"
                            style="{{ $membership->new_or_existing == 'New' ? 'display:none;' : '' }}">
                            <div class="col-4">Product used</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->product_used }}</div>
                        </div>
                        <div class="row"
                            style="{{ $membership->new_or_existing == 'New' ? 'display:none;' : '' }}">
                            <div class="col-4">Agent name</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->agent_name }}</div>
                        </div>
                        <div class="row"
                            style="{{ $membership->new_or_existing == 'New' ? 'display:none;' : '' }}">
                            <div class="col-4">Agent ID</div>
                            <div class="col-auto">:</div>
                            <div class="col-auto">{{ $membership->contact_number }}</div>
                        </div>
                        {{-- <a href="#" class="btn btn-primary pd">Go somewhere</a> --}}
                    </div>
                </div>
            </div>
        </div>


    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>
