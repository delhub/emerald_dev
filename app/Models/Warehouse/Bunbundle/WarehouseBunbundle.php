<?php

namespace App\Models\Warehouse\Bunbundle;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;


class WarehouseBunbundle extends Model
{
    // use TableLogs;

    protected $table = 'wh_product_b_unbundle';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    /**
     * Get purchase of the WarehouseOrder.
     */
    public function bUnbundleItems()
    {
        return $this->hasMany('App\Models\Warehouse\Bunbundle\WarehouseBunbundleItem', 'b_unbundle_key', 'id');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Warehouse\Location\Wh_location', 'location_id', 'id');
    }

    public function theUser()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'user_id');
    }

    public function panel()
    {
        return $this->belongsTo('App\Models\Users\Panels\PanelInfo', 'panel_id', 'account_id');
    }

    public function productAttribute()
    {
        return $this->hasOne('App\Models\Products\ProductAttribute', 'product_code', 'product_code');
    }

    public function productAttributeFrom()
    {
        return $this->hasOne('App\Models\Products\ProductAttribute', 'product_code', 'from_product_code');
    }
}
