<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bujishu - Receipt {{ $purchase->purchase_number }}</title>
   @include('layouts.favicon')
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

  <style>
    p {
      font-size: 12px;
      font-family: 'Lato', sans-serif;
    }

    #invoice-box {
      background-color: #fef5d6;
      border-radius: 20px;
      padding: 20px 0px 20px 20px;
      font-size: 13px;
      width: 80%;
      height: 100%;
      margin-left: 100px;
    }

    #col-amount {
      background-color: #fef5d6;
      text-align: center;
      width: auto;
      height: auto;
    }

    .vertical {
            border-left: 1px solid gold;
            height: 120px;
            position: fixed;
            left: 240px;
            top: 10px;
        }
  </style>
</head>

<body style="margin: 0;">

  <hr style="height:10px;border:none;background-color:#ffcc00; padding:0;margin:0;" />

  <table cellspacing="0" width="100%" height="1410px" style="padding: 0px 50px 0px 50px;">
    <thead>
      <tr style="padding:10px;">
        <th colspan="2" id="logo" style="padding:10px 10px 10px 0;"><img align="left" width="160px" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-logo.jpg') }}"><div class = "vertical"></div></th>
        <th colspan="5" style="text-align:left;">
          <p style="margin: 0px 0px 0px 10px;font-size:17px;line-height: 0px;padding-top: 20px;"><br>DC SIGNATURE LIVING STYLE SDN BHD</p>
          <p style="margin: 15px 0px 0px 10px;line-height:1px;color:#696969;font-weight: 100;"><sup>Company No:202001002917(1859236-K)</sup></p>
          <p style="margin: 0px 0px 0px 10px;font-size:12px;color:#696969;font-weight: 100;">1-26-15 Menara Bangkok Bank, Berjaya Central Park,
            <br>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
          <p style="margin: 0px 0px 0px 10px;font-size:12px;color:#696969;font-weight: 100;"><img style="height:12px;vertical-align:text-bottom;" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-tel.jpg') }}"> 603-2181 8821&nbsp;&nbsp;&nbsp;&nbsp;<img
               style="height:12px;vertical-align:text-bottom;" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-mail.jpg') }}">
            bujishu-cs@delhubdigital.com&nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-web.jpg') }}" style="height:12px;vertical-align:text-bottom;" > www.bujishu.com</p>
          </p>
        </th>
      </tr>
      <tr>
        <th colspan="4"></th>
        <th colspan="3">
          <p style="color:gold;font-size:35px; text-align:left;padding-top:20px;margin-left:100px;">&nbsp;&nbsp;OFFICIAL RECEIPT</p>
        </th>

      </tr>
      <tr>
        <td colspan="4">
          <p style="font-size:16px;color:#696969;">RECEIVED FROM</p>
          <hr style="border-top: 1px solid gold; width:80%;" align="left">
          <p>
            @if ($purchase->ship_address_1)
            {{ $purchase->ship_address_1 }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->address_1 }}
            @endif
            @if ($purchase->ship_address_2)
            {{ $purchase->ship_address_2 }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->address_2 }}
            @endif
            @if ($purchase->ship_address_2)
            {{ $purchase->ship_address_3 }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->address_3 }}
            @endif
            <br>
            @if ($purchase->ship_postcode)
            {{ $purchase->ship_postcode }},
            @else
            {{ $purchase->user->userInfo->shippingAddress->postcode }}
            @endif
            @if ($purchase->ship_city)
            {{ $purchase->ship_city }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->city }}
            @endif
            <br>@if ($purchase->ship_state_id)
            {{$purchase->state->name }},
            @else
            {{$purchase->user->userInfo->shippingAddress->state->name }}
            @endif
            Malaysia<br Attn : <strong>{{ $purchase->ship_full_name }}</strong>
            <br>
            Tel : <strong>
              {{ $purchase->user->userInfo->mobileContact->contact_num }}</strong>
          </p>
          <hr style="border-top: 1px solid gold; width:80%;" align="left">
        </td>
        <td colspan="3" style="align:right;">
          <table id="invoice-box">
            <tr>
              <td>
                <p>OFFICIAL RECEIPT NO. &nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{$purchase->receipt_number}}</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>INVOICE NO &nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{$purchase->getFormattedNumber()}}</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Date&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{$purchase->purchase_date}}</p>
              </td>

            </tr>
            <tr>
              <td>
                <p>Payment Method&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{$purchase->purchase_type}}</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Reference No&nbsp;&nbsp;</p>
              </td>
              <td>
                <p style="text-transform:capitalize;">: {{$purchase->offline_reference}}</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Received in&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: PBB</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Page&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: 2 / 2</p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="7">&nbsp;</td>
      </tr>
      <tr id="col-amount">
        <td style="font-weight:600;border-top:gold 1px solid;border-bottom:gold 1px solid;line-height: 30px;">
          <p style="font-weight:600;text-align:center;">No.</p>
        </td>
        <td style="font-weight:600;border-top:gold 1px solid;border-bottom:gold 1px solid;line-height: 30px;" colspan="5">
          <p style="font-weight:600;text-align:center;">Description</p>
        </td>
        <td style="font-weight:600;border-top:gold 1px solid;border-bottom:gold 1px solid;line-height: 30px;" id="col-amount">
          <p style="font-weight:600;text-align:center;">Amount (RM)</p>
        </td>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="3" style="font-size:12px;">
          <p><br>The issuance of this invoice shall be deemed as your acceptance of the terms and conditions of this disclaimer.</p>
        </td>
        <td></td>
        <td></td>
        <td style="border-bottom:gold 1px solid;">
          <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px; Color:#ffcc00">Total</p>
        </td>
        <td style="border-bottom:1px gold solid; background-color:#ffcc00;" id="col-amount">
          <p><?php
              $subtotal = 0;
              foreach ($purchase->orders as $order) {
                foreach ($order->items as $item) {
                  $subtotal = $subtotal + $item->subtotal_price;
                }
              }
              ?>
            {{ number_format(($purchase->successfulPayment->amount / 100), 2)}}
            {{-- {{ number_format(($subtotal / 100), 2)}} --}}</p>
        </td>

      </tr>
      <tr>
        <td colspan="7">
          <center style="color:grey;font-size:12px;line-height:25px;">
            <br>
            <p>This is computer generated invoice no signature is required.</p>
          </center>
        </td>
      </tr>

    </tfoot>
    <tbody>
      <tr valign="top">
        <?php
        $iterationNo = 0;
        ?>
        @foreach ($purchase->orders as $order)
        @foreach($order->items as $item)
        <td style="border-bottom:1px gold solid;">
          <br>
          <?php
          $iterationNo = $iterationNo + 1;
          ?>
          <p style="text-align:center;"><br>{{ $iterationNo }}</p>
        </td>
        <td style="width:120px;border-bottom:1px gold solid; align:center;"><br><img src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}"
            alt="{{ $item->product->parentProduct->name }}" style="width:100px;">
        </td>
        <td colspan="3" style="border-bottom:1px gold solid;align:left;padding-left:30px;">
          <p><br>{{ $item->quantity }} x {{ $item->product->parentProduct->name }}
            <br>
            @if($item->product_id != 175 || $item->product_id != 176)
            @if(array_key_exists('product_color_name', $item->product_information))
            Color: {{ $item->product_information['product_color_name'] }}
            @endif
            <br>
            @if(array_key_exists('product_size', $item->product_information))
            Size: {{ $item->product_information['product_size'] }}
            @endif
            <br>
            @if(array_key_exists('product_temperature', $item->product_information))
            Color Temperature: {{ $item->product_information['product_temperature'] }}
            @endif
            @endif
          </p>
        </td>
        <td style="border-bottom:1px gold solid;"></td>
        <td style="border-bottom:1px gold solid;" id="col-amount">
          <p><br>{{ number_format(($purchase->successfulPayment->amount / 100), 2)}}</p>
        </td>
      </tr>

      @endforeach
      @endforeach
    </tbody>
    </tr>
  </table>
  <hr style="height:40px;border:none;background-color:#ffcc00; padding:0;margin:0;" />
</body>

</html>
