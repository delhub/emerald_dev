<?php

namespace App\Http\Controllers\API\Export;

use App\Http\Controllers\API\Export\ExportController;
use App\Models\Purchases\Purchase;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PaymentPerSheets implements FromCollection, WithTitle, WithHeadings,ShouldAutoSize,WithStyles
{
    private $query;
    private $counter;

    public const ONLINE_OFFLINE = 1;
    public const NO_PAYMENT = 2;
    public const CANCEL_REFUND = 3;
    public const CN_DN = 4;
    public const ITEMISED = 5;

    public function __construct($query, $counter)
    {
        $this->query = $query;
        $this->counter  = $counter;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return $this->query['header'];
    }

    public function collection()
    {
        return collect($this->query['data']);
        // return new Collection(  [[1,2,3,4]]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->query['title'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],

        ];
    }
}