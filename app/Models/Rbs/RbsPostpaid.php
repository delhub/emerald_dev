<?php

namespace App\Models\Rbs;

use Illuminate\Database\Eloquent\Model;

class RbsPostpaid extends Model
{
    // Set table
    protected $table = 'rbs_postpaid';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'user_id',
        'attribute_id',
        'rbs_quantity',
        'months_id',
        'start_reminder',
        'last_reminder',
        'active',
        'shipping_address',
    ];

    protected $casts = [
        'shipping_address' => 'array'
    ];

    public function atrributeProduct()
    {
        return $this->belongsTo('App\Models\Products\ProductAttribute', 'attribute_id');
    }

    public function rbsMonth()
    {
        return $this->belongsTo('App\Models\Rbs\RbsOptions', 'months_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id');
    }
}
