<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    // Set table
    protected $table = 'global_cities';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'id',
        'country_id', 
        'state_id',
        'city_key',
        'city_name',
        'city_data',
    ];

    public function state()
    {
        return $this->hasOne('App\Models\Globals\State', 'id', 'state_id');
    }

    // Search
    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if (($selectSearch == 'SI')) {
                return $query
                    ->select('global_states.*', 'global_cities.*')
                    ->join('global_states', 'global_states.id', '=', 'global_cities.state_id')
                    ->where('global_states.name', 'like', '%' . $searchFilter . '%')
                    ->orWhere('global_states.abbreviation', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'CI') {
                return $query
                    ->where('global_cities.city_key', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'CD') {
                return $query
                    ->where('global_cities.city_name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'Country') {
                return $query
                    ->where('global_cities.country_id', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }
}
