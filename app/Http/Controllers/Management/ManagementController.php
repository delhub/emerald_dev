<?php

namespace App\Http\Controllers\Management;

use DB;
use PDF;
use Auth;
use Charts;
use Carbon\Carbon;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\Globals\State;
use App\Models\Globals\BankName;
use App\Models\Globals\Marital;
use App\Models\Purchases\Order;
use App\Models\Purchases\Purchase;
use App\Http\Controllers\Controller;
use App\Models\Users\Dealers\DealerBankDetails;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Users\Dealers\Statement;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\Dealers\DealerSpouse;
use App\Models\Users\Dealers\DealerSales;
use App\Models\Purchases\Item;
use App\Users\Dealers\Statement as DealersStatement;
use App\Models\Sidemenus\Sidemenu;
use App\Models\Courier\Courier;
use App\Models\Globals\Countries;
use App\Models\Globals\City;

class ManagementController extends Controller
{

    // Return Shop -> Panel -> All Orders-> index
    public function index()
    {
        $user = User::find(Auth::user()->id);
        $panel_id = $user->panelInfo->account_id;

        //Get today sales for panel
        $todayOrders = new Order;
        $todayOrders = $todayOrders->where('panel_id', $panel_id)->whereDate('created_at', Carbon::today())->sum('order_amount');

        // Get current month
        $now = Carbon::now();
        $month = $now->format('m');

        //Get monthly sales for panel
        $monthlyOrders = new Order;
        $monthlyOrders = $monthlyOrders->where('panel_id', $panel_id)->whereMonth('created_at', $month)->sum('order_amount');

        // Get total number of pending delivery orders for panel
        $totalPendingDelivery = new Order;
        $totalPendingDelivery = $totalPendingDelivery->where('panel_id', $panel_id)->where('delivery_date', 'Pending')->count();

        //Get total number of delivery orders yet to be delivered for panel
        $status = [1000, 1001, 1002];
        $totalDelivery = new Order;
        $totalDelivery = $totalDelivery->where('panel_id', $panel_id)->whereIn('order_status', $status)->count();

        //Get total number of pending claim
        $totalPendingClaim = new Order;
        $totalPendingClaim = $totalPendingClaim->where('panel_id', $panel_id)->where('claim_status', '0')->count();

        // Dynamic menu
        $menus = Sidemenu::where('menu_parent', '=', 0)->get();
        $menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $allMenus = Sidemenu::pluck('menu_title', 'id')->all();

        //dd($totalPendingClaim);

        return view('management.panel.home')
            ->with('allMenus', $allMenus)
            ->with('menus', $menus)
            ->with('todayOrders', $todayOrders)
            ->with('monthlyOrders', $monthlyOrders)
            ->with('totalPendingDelivery', $totalPendingDelivery)
            ->with('totalDelivery', $totalDelivery)
            ->with('totalPendingClaim', $totalPendingClaim);
    }

    // Return Value Tracking -> Show Customer Orders
    public function valueTracking()
    {
        $status = [1001, 1002, 1003];
        // Get panel

        $panel_id = User::find(Auth::user()->id);
        $couriers = Courier::all();

        $panel_id = $panel_id->panelInfo->account_id;
        $customerOrders = new Order;
        $customerOrders = $customerOrders->where('panel_id', $panel_id)->whereIn('order_status', $status)->orderBy('purchase_id', 'DESC')->get();

        $menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();

        return view("management.panel.index")->with('customerOrders', $customerOrders)->with('couriers', $couriers)->with('menus', $menus);
    }

    /***Return purchase order(pdf) for the order  **/

    public function viewPurchaseOrder($orderNum)
    {

        $order = new Order();
        $order = $order->where('order_number', $orderNum)->first();
        $countries = Countries::getAll();

        $country = $countries[$order->country_id];
        $pdf = PDF::loadView('documents.purchase.v2.purchase-order', compact('order', 'country'))->setPaper('A4');
        return $pdf->stream('purchase-order.' . $orderNum . '.pdf');
    }

    public function viewDeliveryOrder($deliverOrder)
    {
        $order = new Order();
        $order = $order->where('delivery_order', $deliverOrder)->first();
        $countries = Countries::getAll();

        $country = $countries[$order->country_id];
        $pdf = PDF::loadView('documents.purchase.v2.delivery-order', compact('order', 'country'))->setPaper('A5');
        return $pdf->stream('delivery-order.' . $deliverOrder . '.pdf');
    }

    // Update estimate delivery date (orders)
    public function updateOrder(Request $request, $order_num)
    {
        $order = new Order();
        $order = Order::findOrFail($order_num);
        $order->delivery_date = $request->input('delivery_date');
        $order->order_status = 1002;
        // $order->updated_at=
        $order->save();
        if ($order) {
            return back()->with(['successful_message' => 'Order updated successfully']);
        } else {
            return back()->with(['error_message' => 'Failed to update order']);
        }
    }

    /****Return company profile for panel***/

    public function companyProfile()
    {
        $panel_id = User::find(Auth::user()->id);
        $panel_id = $panel_id->panelInfo->account_id;
        $companyProfile = new PanelInfo();
        $companyProfile = $companyProfile->where('account_id', $panel_id)->first();

        $company_city = NULL;
        if (isset($companyProfile->correspondenceAddress->city_key)) {
            if (!empty($companyProfile->correspondenceAddress->city_key)) {
                $company_city = $companyProfile->correspondenceAddress->cityKey->city_name;
            } else {
                $company_city = $companyProfile->correspondenceAddress->city;
            }
        }

        $billing_city = NULL;
        if (isset($companyProfile->billingAddress->city_key)) {
            if (!empty($companyProfile->billingAddress->city_key)) {
                $billing_city = $companyProfile->billingAddress->cityKey->city_name;
            } else {
                $billing_city = $companyProfile->billingAddress->city;
            }
        }

        $companyProfile->correspondenceAddress->city_key;
        $companyProfile->billingAddress->city_key;
        return view("management.panel.company-profile")
            ->with('companyProfile', $companyProfile)
            ->with('company_city', $company_city)
            ->with('billing_city', $billing_city);
    }


    /**Return edit page for company profile */

    public function editProfile()
    {
        $states = State::all();
        $bankNames = BankName::where('type','agent')->where('country_id','MY')->get();

        $panel_id = User::find(Auth::user()->id);
        $panel_id = $panel_id->panelInfo->account_id;
        $companyProfile = new PanelInfo();
        $companyProfile = $companyProfile->where('account_id', $panel_id)->first();
        $citystateId = 0;
        $company_city_panel = 0;
        $company_city = 0;
        $billing_city = 0;
        $billstateId = 0;
        $company_billing_city = 0;

        //city
        if (isset($companyProfile->correspondenceAddress->state_id)) $citystateId = $companyProfile->correspondenceAddress->state_id;
        if (isset($companyProfile->correspondenceAddress->city_key)) $company_city_panel = $companyProfile->correspondenceAddress->city_key;

        //billing city
        if (isset($companyProfile->billingAddress->state_id)) $billstateId = $companyProfile->billingAddress->state_id;
        if (isset($companyProfile->billingAddress->city_key)) $company_billing_city = $companyProfile->billingAddress->city_key;

        $states = State::where('country_id', country()->country_id)->get();

        //city
        if ($citystateId == 0) {
            $cities = City::where('country_id', country()->country_id)->orderBy('city_name', 'asc')->get();
        } else {
            $cities = City::where('country_id', country()->country_id)->where('state_id', $citystateId)->orderBy('city_name', 'asc')->get();
        }

        //billing city
        if ($billstateId == 0) {
            $billcities = City::where('country_id', country()->country_id)->orderBy('city_name', 'asc')->get();
        } else {
            $billcities = City::where('country_id', country()->country_id)->where('state_id', $billstateId)->orderBy('city_name', 'asc')->get();
        }

        return view("management.panel.company-profile-edit")
            ->with('companyProfile', $companyProfile)
            ->with('states', $states)
            ->with('citystateId', $citystateId)
            ->with('billcities', $billcities)
            ->with('cities', $cities)
            ->with('company_city_panel', $company_city_panel)
            ->with('company_city', $company_city)
            ->with('billing_city', $billing_city)
            ->with('billstateId', $billstateId)
            ->with('company_billing_city', $company_billing_city);
    }



    /** Update company profile**/

    public function updateProfile(Request $request, $id)
    {

        $this->validate($request, array(

            'company_billing_address_1' => 'required',
            'company_billing_postcode' => 'required',
            //'company_billing_postcode' => 'required|digits:5',
            'company_billing_city' => 'required',
            'company_address_1' => 'required',
            'postcode' => 'required',
            'company_city' => 'required',
            //'postcode' => 'required|digits:5',
            // 'city' => 'required',
            // 'company_phone_number' => 'required|digits:10',

        ));

        // dd($request->input());
        $companyProfile = new PanelInfo();
        $companyProfile = $companyProfile->where('account_id', $id)->first();
        $correspondence_address = $companyProfile->correspondenceAddress;
        if ($correspondence_address != NULL) {
            $correspondence_address->address_1 = $request->input('company_address_1');
            $correspondence_address->address_2 = $request->input('company_address_2');
            $correspondence_address->address_3 = $request->input('company_address_3');
            $correspondence_address->postcode = $request->input('postcode');
            $correspondence_address->city_key = $request->input('company_city_panel');
            if ($request->input('company_city_panel') == 0) $correspondence_address->city = $request->input('company_city');
            $correspondence_address->state_id = $request->input('state');
            $correspondence_address->save();
        } else {
            $correspondence_address = new PanelInfo;
            $correspondence_address->account_id = $name->account_id;
            $correspondence_address->address_1 = $request->input('company_address_1');
            $correspondence_address->address_2 = $request->input('company_address_2');
            $correspondence_address->address_3 = $request->input('company_address_3');
            $correspondence_address->postcode = $request->input('postcode');
            $correspondence_address->city_key = $request->input('company_city_panel');
            if ($request->input('company_city_panel') == 0) $correspondence_address->city = $request->input('company_city');
            $correspondence_address->state_id = $request->input('state');
            $correspondence_address->is_correspondence_address = 1;
            $correspondence_address->save();
        }

        $billing_address = $companyProfile->billingAddress;
        //dd($request->input());
        if ($billing_address != NULL) {
            $billing_address->address_1 = $request->input('company_billing_address_1');
            $billing_address->address_2 = $request->input('company_billing_address_2');
            $billing_address->address_3 = $request->input('company_billing_address_3');
            $billing_address->postcode = $request->input('company_billing_postcode');
            $billing_address->city_key = $request->input('company_billing_city');
            if ($request->input('company_billing_city') == 0) $billing_address->city = $request->input('billing_city');
            $billing_address->state_id = $request->input('billing_state');
            $billing_address->save();
        } else {
            $billing_address = new PanelInfo;
            $billing_address->account_id = $name->account_id;
            $billing_address->address_1 = $request->input('company_billing_address_1');
            $billing_address->address_2 = $request->input('company_billing_address_2');
            $billing_address->address_3 = $request->input('company_billing_address_3');
            $billing_address->postcode = $request->input('company_billing_postcode');
            $billing_address->city_key = $request->input('company_billing_city');
            if ($request->input('company_billing_city') == 0) $billing_address->city = $request->input('billing_city');
            $billing_address->state_id = $request->input('billing_state');
            $billing_address->is_billing_address = 1;
            $billing_address->save();
        }

        $companyProfile->company_phone = $request->input('company_phone_number');
        $companyProfile->save();

        if ($companyProfile && $correspondence_address && $billing_address) {
            return redirect()->route('management.company.profile')->with('companyProfile', $companyProfile)->with(['successful_message' => 'Profile updated successfully']);
        } else {
            return redirect()->route('management.company.profile')->with('companyProfile', $companyProfile)->with(['error_message' => 'Failed to update profile']);
        }
    }

    // View all orders-panel
    public function allOrders()
    {
        return view('management.orders.allCustOrders');
    }

    // View open orders-panel
    public function openOrders()
    {
        return view('management.orders.open');
    }

    // View in progress orders-panel
    public function inProgressOrders()
    {
        return view('management.orders.in-progress');
    }

    // View completed orders-panel
    public function completedOrders()
    {
        return view('management.orders.completed');
    }

    // View delivered orders-panel
    public function deliveredOrders()
    {
        return view('management.orders.delivered');
    }


    // View all cancelled-panel
    public function cancelledOrders()
    {
        return view('management.orders.cancelled');
    }

    // Home Page-Dealer

    public function homeDealer()
    {
        //Get dealer account id
        $dealer_id = User::find(Auth::user()->id);
        $dealer_id = $dealer_id->dealerInfo->account_id;

        //Get today sales for dealer
        $todaySales = new DealerSales;
        $todaySales = $todaySales->where('account_id', $dealer_id)
            ->whereDate('created_at', Carbon::today())
            ->sum('order_amount');

        // Get current month
        $now = Carbon::now();
        $month = $now->format('m');

        //Get monthly sales for panel
        $monthlySales = new DealerSales;
        $monthlySales = $monthlySales->where('account_id', $dealer_id)
            ->whereMonth('created_at', $month)
            ->sum('order_amount');

        return view('management.dealer.home')
            ->with('todaySales', $todaySales)
            ->with('monthlySales', $monthlySales);
    }

    // Statement Summary Page for Dealer
    public function indexDealer()
    {
        $dealer_id = User::find(Auth::user()->id);
        $dealer_id = $dealer_id->dealerInfo->account_id;
        $dealer_statement = new Statement;
        $dealer_statement = $dealer_statement->where('account_id', $dealer_id)->first();
        return view('management.dealer.index')->with('dealer_statement', $dealer_statement);
    }

    //Sales Summary for Dealer

    public function salesSummary()
    {

        return view('management.dealer.sales-summary');
    }

    // Profile for Dealer
    public function dealerProfile()
    {
        $maritals = Marital::all();

        $user = User::find(Auth::user()->id);
        $dealerProfile = $user->dealerInfo;
        $agent_city = null;

        if (isset($user->dealerInfo->employmentAddress->company_city_key)) {
            if (!empty($user->dealerInfo->employmentAddress->company_city_key)) {
                $agent_city = $dealerProfile->employmentAddress->cityKey->city_name;
            } else {
                $agent_city = $user->dealerInfo->employmentAddress->company_city;
            }
        }
        // $city_key = $user->dealerInfo->employmentAddress->company_city;
        // $city = City::where('city_key',$city_key)->first('city_name');
        // $agent_city = $city->city_name;
        // $user->dealerInfo->employmentAddress->city_key;
        return view("management.dealer.profile")
            ->with('dealerProfile', $dealerProfile)
            ->with('agent_city', $agent_city)
            ->with('maritals', $maritals);

    }


    //Edit Profile for Dealer
    public function editdealerProfile()
    {
        $maritals = Marital::all();
        //        $states = State::all();
       $bankNames = BankName::where('type','agent')->where('country_id','MY')->get();

        $user = User::find(Auth::user()->id);
        $stateId = 0;
        $dealer_company_city = 0;

        if (isset($user->dealerInfo->employmentAddress->company_state_id)) $stateId = $user->dealerInfo->employmentAddress->company_state_id;
        if (isset($user->dealerInfo->employmentAddress->company_city_key)) $dealer_company_city = $user->dealerInfo->employmentAddress->company_city_key;

        $states = State::get();
        $cities = City::get();

        $dealerProfile = $user->dealerInfo;
        return view('management.dealer.edit-profile')
            ->with('dealerProfile', $dealerProfile)
            ->with('maritals', $maritals)
            ->with('states', $states)
            ->with('bankNames', $bankNames)
            ->with('states', $states)
            ->with('stateId', $stateId)
            ->with('cities', $cities)
            ->with('dealer_company_city', $dealer_company_city);
    }

    /** Update dealer profile**/

    public function updateDealerProfile(Request $request, $id)
    {
        $this->validate($request, array(
            'dealer_company_name' => 'required',
            'dealer_company_address_1' => 'required',
            'dealer_company_postcode' => 'required|digits:5',
            //'dealer_company_city' => 'required',
            'spouse_name' =>  'required_if:marital_id,2',
            'spouse_occupation' => 'required_if:marital_id,2',
            'spouse_contact' => 'required_if:marital_id,2|nullable|min:10',
            'spouse_email' => 'required_if:marital_id,2|nullable|email|string',
            'bank_name' => 'required',
            'account_holder_name' => 'required',
            'bank_account_number' => 'required',
            'account_regid' => 'required'

        ));

        $dealerInfo = new DealerInfo();
        $dealerInfo = $dealerInfo->where('account_id', $id)->first();

        //update marital status
        $dealerInfo->marital_id = $request->input('marital_id');
        $dealerInfo->save();

        //update dealer employment address
        $dealer_employment_address = $dealerInfo->employmentAddress;
        if ($dealer_employment_address == null) {
            $dealer_employment_address = new DealerInfo;
            $dealer_employment_address->account_id = $dealerInfo->account_id;
        }
        $dealer_employment_address->company_name = $request->input('dealer_company_name');
        $dealer_employment_address->company_address_1 = $request->input('dealer_company_address_1');
        $dealer_employment_address->company_address_2 = $request->input('dealer_company_address_2');
        $dealer_employment_address->company_address_3 = $request->input('dealer_company_address_3');
        $dealer_employment_address->company_postcode = $request->input('dealer_company_postcode');
        if ($request->input('dealer_company_city') == 0) $dealer_employment_address->company_city  = $request->input('company_city');
        $dealer_employment_address->company_city_key = $request->input('dealer_company_city');
        $dealer_employment_address->company_state_id = $request->input('dealer_company_state');
        $dealer_employment_address->save();

        //update dealer spouse information
        $dealer_spouse_information = $dealerInfo->dealerSpouse;

        if (!$dealer_spouse_information) {
            $dealer_spouse_information = new DealerSpouse();
            $dealer_spouse_information->account_id = $dealerInfo->account_id;
        }


        $dealer_spouse_information->spouse_name = $request->input('spouse_name');
        $dealer_spouse_information->spouse_occupation = $request->input('spouse_occupation');
        $dealer_spouse_information->spouse_contact_mobile = $request->input('spouse_contact');
        $dealer_spouse_information->spouse_email = $request->input('spouse_email');
        $dealer_spouse_information->save();


        // update bank information
        $dealer_bank_details = $dealerInfo->dealerBankDetails;

        if (!$dealer_bank_details) {
            $dealer_bank_details = new DealerBankDetails();
            $dealer_bank_details->account_id = $dealerInfo->account_id;
        }

        $dealer_bank_details->bank_code = ($bank_Code = $request->input('bank_name')) ? $bank_Code : '';
        $dealer_bank_details->bank_acc = ($bank_acc = $request->input('bank_account_number')) ? $bank_acc : 0;
        $dealer_bank_details->bank_acc_name = ($bank_acc_name = $request->input('account_holder_name')) ? $bank_acc_name : "";
        $dealer_bank_details->account_regid = ($account_regid = $request->input('account_regid')) ? $account_regid : "";
        $dealer_bank_details->save();


        if ($dealerInfo && $dealer_employment_address  && $dealer_bank_details) {
            return redirect()->route('shop.dashboard.dealer.profile')->with('dealerProfile', $dealerInfo)->with(['successful_message' => 'Profile updated successfully']);
        } else {
            return redirect()->route('shop.dashboard.dealer.profile')->with('dealerProfile', $dealerInfo)->with(['error_message' => 'Failed to update profile']);
        }
    }




    // Change Password
    public function modifyPassword()
    {
        return view('management.dealer.modifypassword');
    }

    // View Statments for dealers
    public function statements($month, $month_num, $year)
    {

        $dealer_id = Auth::user()->dealerInfo->account_id;

        $dealerProfile = DealerInfo::where('account_id', $dealer_id)->first();

        $userInfo = Auth::user()->userInfo;

        $customerPurchase = Purchase::where('dealer_id', $dealer_id)->get();

        $pdf = PDF::loadView('documents.statement.monthly-statement', compact('dealerProfile', 'customerPurchase', 'userInfo', 'month', 'month_num', 'year'))->setPaper('A4');
        return $pdf->stream('statement.pdf');
    }

    // View Person In Charge for panel
    public function personInCharge()
    {
        return view('management.panel.person-in-charge');
    }

    public function updateTracking(Request $request, $id)
    {
        $item = Item::where('id', $id)->first();
        $item->courier_name = ($request->input('courier_name') != null) ? $request->input('courier_name') : $item->courier_name;
        $item->tracking_number = ($request->input('tracking_number') != null) ? $request->input('tracking_number') : $item->tracking_number;
        $item->save();

        return back()->with(['successful_message' => 'Update Courier name and Tracking number Successfully']);
    }

    public static function editcity(Request $request)
    {
        $state_id = 0;

        if ($request->input('state_id') != NULL) {
            $state_id = $request->input('state_id');
        } elseif ($request->input('state') != NULL) {
            $state_id = $request->input('state');
        }

        $cities = City::where('state_id', $state_id)->get();

        foreach ($cities as  $city) {
            $City[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $City[] = '<option value="0" "selected">Others</option>';

        return $City;
    }
    public static function editbill(Request $request)
    {
        $state_id = 0;

        if ($request->input('state_id') != NULL) {
            $state_id = $request->input('state_id');
        } elseif ($request->input('billing_state') != NULL) {
            $state_id = $request->input('billing_state');
        }

        $billcities = City::where('state_id', $state_id)->get();

        foreach ($billcities as  $city) {
            $City[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $City[] = '<option value="0" "selected">Others</option>';

        return $City;
    }
}
