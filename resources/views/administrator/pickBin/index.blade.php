@inject('controller', 'App\Http\Controllers\Administrator\PickBin\PickBinController')

@extends('layouts.administrator.main')

@section('content')

<h1>{{$controller::$header}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div>
        </div>
        <form action="{{ route('administrator.pickBin.index') }}" method="GET">
            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="col-auto ml-auto">
                    <select class="custom-select" id="location" name="location">

                        <option value="0" selected>All Location...</option>
                        @foreach ($locations as $location)
                        <option value="{{$location->id}}" {{ $request->location == $location->id ? 'selected' : '' }}>{{$location->location_name}}</option>

                        @endforeach
                    </select>
                </div>
            </div>
            <br>
            <br>

            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="col-auto ml-auto">
                    <select class="custom-select" id="selectSearch" name="selectSearch">
                        <option value="ALL" selected>Choose...</option>
                        <option value="name" {{ $request->selectSearch == 'name' ? 'selected' : '' }}>Bin Name</option>
                    </select>

                    <input type="text" class="form-control" name="searchBox"
                    value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                </div>
                <div style="text-align:center;">
                    <div class="col-md-auto px-1">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                        <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.pickBin.index') }} style="color:black;">Reset Filter</a></button>
                    </div>
                </div>
            </div>
        </form>

        <div class="table-responsive m-2">
            @if(count($tables) > 0)
            <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <th>Bin ID</th>
                        <th>Bin Number</th>
                        <th>Using</th>
                        <th>Location</th>
                        <th>Picker Name</th>
                        <th>Sorter Name</th>
                        <th>Packer Name</th>
                        {{-- <th>QR CODE</th> --}}
                        {{-- <th>Action</th> --}}
                    </tr>
                </thead>
                <tbody>

                    @foreach($tables as $row)

                    {{-- @php
                        $url = URL::signedRoute(
                            'administrator.pickBin.qr-code',
                            ['bin_number' => $row->bin_number]
                        );
                    @endphp --}}

                    <tr>
                        <td>{{$row->id }}</td>
                        <td>{{$row->bin_number }}</td>
                        <td>{{$row->using }}</td>
                        <td>{{$row->location->location_name }}</td>
                        <td>{{!empty($row->pick_user) ? $row->pick_user->userInfo->full_name : '' }}</td>
                        <td>{{!empty($row->sort_user) ? $row->sort_user->userInfo->full_name : '' }}</td>
                        <td>{{!empty($row->pack_user) ? $row->pack_user->userInfo->full_name : '' }}</td>

                        {{-- <td>
                            <img src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($url)) }} ">
                        </td> --}}
                            {{-- style="position: absolute;width: 120px;top: 0px;right:0;" --}}



                        {{-- <td>
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left" class="btn btn-primary shadow-sm"
                            href="{{ route($controller::$edit,['id' => $row->id]) }}" >Edit</a>

                            <form action="{{ route($controller::$destroy,['id' => $row->id]) }}" id="delete-form-id" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            @csrf
                                <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>
                            </form>
                        </td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$tables->links()}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p>
            @endif
        </div>
    </div>
</div>
@endsection
