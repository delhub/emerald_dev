<?php

namespace App\Models\Dealers;

use App\Models\Purchases\Item;
use Illuminate\Database\Eloquent\Model;

class DealerPrebuyRedemption extends Model
{
    // Set table
    protected $table = 'dealer_prebuy_redemption';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'dealer_id',
        'channel',
        'prebuy_status',
        'total_amount',
    ];

    // Cast column to another datatype.
    protected $casts = [];

    public function dealerInfo(){
        return $this->hasOne('App\Models\Users\Dealers\DealerInfo','account_id', 'dealer_id');
    }
    public function itemsProductCode(){
        //
        $item_id = json_decode($this->redemption_data);
        $items = array();
        foreach ($item_id as $id) {
            $itemTable = Item::where('id',$id)->first();
            $items[$itemTable->product_code]['product_name'] = $itemTable->product->parentProduct->name;
            $items[$itemTable->product_code]['item'][] = $itemTable;
        }
        return $items;
    }

    public function allDealerData(){

        return $this->hasMany('App\Models\Dealers\DealerData','dealer_id', 'dealer_id');
    }

    public function dealerData($id,$product_code){

        $dealerData = $this->allDealerData->where('dealer_id',$id)->where('name',$product_code)->first();

        return (isset($dealerData)) ? $dealerData->value : null;
    }
    public function dealerTotalStock($id,$product_code){
        $quantity = 0;
        $stockLedger = DealerStockLedger::where('product_code',$product_code)->where('type',5002)->get();
        if ($stockLedger) {
            $quantity = $stockLedger->sum('quantity');
        }
        return $quantity;

        // $dealerData = $this->allDealerData->where('dealer_id',$id)->where('name',$product_code)->first();

        // return (isset($dealerData)) ? $dealerData->value : null;
    }


}
