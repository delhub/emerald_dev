@extends('layouts.management.main-agent')

@section('content')

@section('page_title')
{{ "Welcome to Agent Dashboard" }}
@endsection

<div class="fml-container">
            <div class="row">
                <div class="col-lg-4 dashboard-pt-b">
                    <div class="checkout-box-s checkout-box-height">
                        <div class="dashboard-box">
                            <div class="agent-dashboard-title">
                                <div class="title-width">
                                    <p>Personal Sales</p>
                                </div>
                                <div class="icon-special2">
                                    <div class="icon-viewmore3">
                                        <a href="#" data-original-title="" title="" tabindex="0">
                                            <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="sales-div">
                            <table>
                                <tr>
                                    <td class="crown-img">
                                        <img src={{asset('/images/icons/do-crown-icon.png')}}>

                                    </td>
                                    <td class="sales-amt">
                                        <b>
                                            {{country()->country_currency}}

                                            {{ number_format(($personalSaleD / 100), 2) }}


                                            {{-- @if(count($personalSaleD) > 0)
                                            {{ number_format(($personalSaleD[0]->totAmount / 100), 2) }}
                                            @else
                                            0.00
                                            @endif --}}

                                            {{-- @if(!empty($personalSaleD))
                                            {{ number_format(($personalSaleD / 100), 2) }}
                                            @else
                                            0.00
                                            @endif --}}
                                        </b>
                                        <p>Today: {{$today}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="crown-img"><img
                                            src={{asset('/images/icons/do-crown-icon.png')}}>
                                    </td>
                                    <td class="sales-amt">
                                        <b>
                                            {{country()->country_currency}}

                                            {{ number_format(($personalSaleW / 100), 2) }}

                                            {{-- @if(count($personalSaleW) > 0)
                                            {{ number_format(($personalSaleW[0]->totAmount / 100), 2) }}
                                            @else
                                            0.00
                                            @endif --}}

                                            {{-- @if(!empty($personalSaleW))
                                            {{ number_format(($personalSaleW / 100), 2) }}
                                            @else
                                            0.00
                                            @endif --}}
                                        </b>
                                        <p>Weekly: {{$sevenDaysBefore}} - {{$today}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="crown-img"><img
                                            src={{asset('/images/icons/do-crown-icon.png')}}>
                                    </td>
                                    <td class="sales-amt">
                                        <b>
                                            {{country()->country_currency}}

                                            {{ number_format(($personalSaleM / 100), 2) }}

                                            {{-- @if(count($personalSaleM) > 0)
                                            {{ number_format(($personalSaleM[0]->totAmount / 100), 2) }}
                                            @else
                                            0.00
                                            @endif --}}

                                            {{-- @if(!empty($personalSaleM))
                                            {{ number_format(($personalSaleM / 100), 2) }}
                                            @else
                                            0.00
                                            @endif --}}
                                        </b>
                                        <p>Monthly: {{$month}}</p>
                                    </td>
                                </tr>
                            </table>

                        </div>


                    </div>
                </div>
                <div class="col-lg-4 dashboard-pt-b">
                    <div class="checkout-box-s checkout-box-height">
                        <div class="dashboard-box">
                            <div class="agent-dashboard-title">
                                <div class="title-width">
                                    <p>Group Sales</p>
                                </div>

                                <div class="icon-special2">
                                    <div class="icon-viewmore3">
                                        <a href="#" data-original-title="" title="" tabindex="0">
                                            <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="sales-div">
                            <table>
                                <tr>
                                    <td class="crown-img">
                                        <img src={{asset('/images/icons/do-crown-icon.png')}}>
                                    </td>
                                    <td class="sales-amt">
                                        <b>
                                            {{country()->country_currency}}

                                            {{ number_format(($groupTotalD / 100), 2) }}

                                            {{-- @if(count($groupTotalD) > 0)
                                            {{ number_format(($groupTotalD[0]->grouptot / 100), 2) }}
                                            @else
                                            0
                                            @endif --}}

                                            {{-- @if(!empty($groupTotalD))
                                            {{ number_format(($groupTotalD / 100), 2) }}
                                            @else
                                            0
                                            @endif --}}
                                        </b>
                                        <p>Today: {{$today}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="crown-img">
                                        <img src={{asset('/images/icons/do-crown-icon.png')}}>
                                    </td>
                                    <td class="sales-amt">
                                        <b>
                                            {{country()->country_currency}}

                                            {{ number_format(($groupTotalW / 100), 2) }}

                                            {{-- @if(count($groupTotalW) > 0)
                                            {{ number_format(($groupTotalW[0]->grouptot / 100), 2) }}
                                            @else
                                            0
                                            @endif --}}

                                            {{-- @if(!empty($groupTotalW))
                                            {{ number_format(($groupTotalW / 100), 2) }}
                                            @else
                                            0
                                            @endif --}}
                                        </b>
                                        <p>Weekly: {{$sevenDaysBefore}} - {{$today}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="crown-img"><img src={{asset('/images/icons/do-crown-icon.png')}}>
                                    </td>
                                    <td class="sales-amt">
                                        <b>
                                            {{country()->country_currency}}

                                            {{ number_format(($groupTotalM / 100), 2) }}

                                            {{-- @if(count($groupTotalM) > 0)
                                            {{ number_format(($groupTotalM[0]->grouptot / 100), 2) }}
                                            @else
                                            0
                                            @endif --}}

                                            {{-- @if(!empty($groupTotalM))
                                            {{ number_format(($groupTotalM / 100), 2) }}
                                            @else
                                            0
                                            @endif --}}
                                        </b>
                                        <p>Monthly: {{$month}}</p>
                                    </td>
                                </tr>
                            </table>

                        </div>


                    </div>
                </div>
                <div class="col-lg-4 dashboard-pt-b">
                    <div class="checkout-box-s checkout-box-height">
                        <div class="dashboard-box">
                            <div class="agent-dashboard-title">
                                <div class="title-width">
                                    <p>Announcement</p>
                                </div>

                                <div class="icon-special2">
                                    <div class="icon-viewmore3">
                                        <a href=#" data-original-title="" title="" tabindex="0">
                                            <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                        </a>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="sales-div">
                            <table>
                                <tr>
                                    <td class="logo-img">
                                        <img src={{asset('/images/formula/announcement1.png')}}>
                                    </td>
                                    <td class="announcement-date">
                                        <p>Today: {{$today}}</p>
                                        <div class="announcement-text"><a href="{{route('agent.customerVerification')}}"><b>Customer Verification</b></a></div>
                                    </td>

                                </tr>

                                {{-- <tr>
                                    <td class="logo-img"><img
                                            src={{asset('/images/formula/announcement2.png')}}>
                                    </td>
                                    <td class="announcement-date">
                                        <p>Monthly: {{$month}}</p>
                                        <div class="announcement-text"><b>DuitNow is Available</b></div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="logo-img">
                                        <img src={{asset('/images/formula/announcement1.png')}}>
                                    </td>
                                    <td class="announcement-date">
                                        <p>Monthly: {{$month}}</p>
                                        <div class="announcement-text"><b>New Agent Program</b></div>
                                    </td>

                                </tr> --}}

                            </table>

                        </div>
                    </div>
                </div>

            </div>
            <div class="row my-do-row">
                <div class="col-lg-6 dashboard-pt-b">
                    <div class="checkout-box-s checkout-box-height">
                        <div class="dashboard-box">
                            <div class="agent-dashboard-title">
                                <p>Agent Top 10 Sales</p>
                            </div>
                        </div>
                        <div class="sales-div">
                            <table class="top-10-agent-table">
                                @if(count($agentTop) > 0)
                                    @foreach ($agentTop as $key => $agentTops)
                                        @php if ($key >= 10) continue; @endphp
                                        <tr>
                                            <td class="agent-name" width="70%">{{$agentTops->agent_name}}</td>
                                            <td class="agent-sales" width="10%">{{country()->country_currency}}</td>
                                            <td class="agent-sales" width="20%">{{ number_format(($agentTops->totAmount / 100), 2) }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <span>No Agent</span>
                                @endif
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 dashboard-pt-b">
                    <div class="checkout-box-s checkout-box-height">
                        <div class="dashboard-box">
                            <div class="agent-dashboard-title">
                                <p>Categories Sales Report</p>
                            </div>
                        </div>
                        <div class="sales-div">
                            <div class="chart-area" style="margin: -20px 0px 0px 0px;">
                                <div id="chart"></div>
                            </div>
                            {{-- <div class="col chart-label">
                                <div class="progress-bar-div">
                                    <p>Blood Pressure</p>
                                    <div class="progress my-progress-bar-div">
                                        <div class="progress-bar my-progress-bar" role="progressbar"
                                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"
                                            style="width:75%">RM 75,840.00</div>
                                    </div>
                                </div>
                                <div class="progress-bar-div">
                                    <p>Women</p>
                                    <div class="progress my-progress-bar-div">
                                        <div class="progress-bar my-progress-bar" role="progressbar"
                                            aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"
                                            style="width:70%">RM 70,840.00</div>
                                    </div>
                                </div>
                                <div class="progress-bar-div">
                                    <p>Skin Care</p>
                                    <div class="progress my-progress-bar-div">
                                        <div class="progress-bar my-progress-bar" role="progressbar"
                                            aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"
                                            style="width:55%">RM 55,840.00</div>
                                    </div>
                                </div>
                                <div class="progress-bar-div">
                                    <p>Diabetis</p>
                                    <div class="progress my-progress-bar-div">
                                        <div class="progress-bar my-progress-bar" role="progressbar"
                                            aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"
                                            style="width:35%">RM 35,840.00</div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
    <div class="tab-pane fade" id="value-records" role="tabpanel" aria-labelledby="value-records-tab">
      Coming Soon
    </div>
    <div class="tab-pane fade" id="perfect" role="tabpanel" aria-labelledby="perfect-tab">
      Coming Soon
    </div>
    <div class="tab-pane fade" id="voucher" role="tabpanel" aria-labelledby="voucher-tab">
      Coming Soon
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript">

    window.onload = function() {

        //BAR CHART
        var options = {
        series: [{
            data: [
            @foreach($categoryNames as $categoryName)
            '{{$categoryName['totalSum']}}',
            @endforeach
            ]
        }],
        chart: {
            type: 'bar',
            height: 380
        },
        plotOptions: {
            bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: false,
            dataLabels: {
                position: 'bottom',
                enabled: true,
            },
            }
        },
        colors: ["#2540c3", "#ED0C8C", "#F7931C", "#ED1B27", "#02ABF2", "#990B94","#ab6bff","#ff3ff1","#800000","#57e2e2","#ff9489"],
        dataLabels: {
            enabled: false,
            textAnchor: 'start',
            style: {
            colors: ['#fff']
            },
            formatter: function(val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
            },
            offsetX: 0,
            dropShadow: {
            enabled: true
            }
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        xaxis: {
            categories: [
            @foreach($categoryNames as $categoryName)
            '{{ $categoryName['catName'] }} ({{ $categoryName['totalqlt'] }})',
            @endforeach
            ],
            labels: {
            show: false,
            },

        },
        yaxis: {
            tickAmount: 5,
            min: 0,
            max: function(max) {
            return Math.ceil(max / 10) * 10;
            },
            labels: {
            show: true,
            formatter: function(value) {
                return value;
            }
            },
            title: {
            text: '',
            rotate: 90,
            offsetX: 0,
            offsetY: 0,
            style: {
                cssClass: 'apexcharts-yaxis-title',
            },
            },
        },

        tooltip: {
            theme: 'dark',
            x: {
            show: true
            },
            y: {
            title: {
                formatter: function() {
                return 'RM';
                }
            }
            }
        }
        };

        var barChart = new ApexCharts(document.querySelector("#chart"), options);
        barChart.render();

        //active select side bar and change title
        const pageName = document.querySelectorAll('#home-tab');
            pageName.forEach(navItem =>{
                if(navItem.id !== 'home-tab'){
                    navItem.childNodes[1].classList.remove('active');

                }else {
                    navItem.childNodes[1].classList.add('active');
                }
            })

    }



    // const myslider = document.querySelector('#slideshow');

    // // myslider.style.display="none";

    // //add new slider
    // $(document).ready(() => {

    //     // myslider.style.display="block";

    //     $('#slideshow .slick').slick({
    //         autoplay: true,
    //         fade: false,
    //         autoplaySpeed: 2000,
    //         speed: 1000,
    //         dots: true,
    //         prevArrow: false,
    //         nextArrow: false
    //     });
    // });

    // document.querySelector("#myhomepage").style.display = "block";
    // document.querySelector("#mysubpage").style.display = "none";
    // document.querySelector("#app").classList.remove('app-body');
</script>
@endpush
