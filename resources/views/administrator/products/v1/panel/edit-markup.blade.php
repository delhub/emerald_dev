{{-- <form name="form" method="POST" id="formID"
action="{{ route('administrator.v1.products.panels.price.update-markup', ['id' => $panelProduct->id]) }}"
>
@csrf
@method('POST') --}}

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Panel Product Name / Attribute Name</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>
                {{$panelProduct->parentProduct->name}}

            </p>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Prouct Code</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>
                {{$panelProduct->parentProduct->product_code}}
            </p>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Product Quanlity</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>
                {{$panelProduct->parentProduct->quality->name}}
            </p>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto">
            <p>
                Mark Up Category<small class="text-danger">*</small>
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
            <select name="markup_category[]" id="markup_category"
                class="select2 form-control">
                <option value="0">Choose one markup category</option>
                @foreach($markupCategories as $category)
                <option value="{{ $category->id }}" data-markup-name="{{$category->cat_name}}"
                    {{ $panelProduct->markupCategories && $panelProduct->markupCategories->contains($category->id) ? 'selected' : '' }}
                >Name : {{ $category->cat_name }}, Description : {{ $category->cat_desc}}, Markup Rate {{ $category->markup_rate}}</option>
                @endforeach

            </select>
        </div>
    </div>
    <div class="form-row mt-2">
        <div class="col-12 col-md-6 offset-md-4">
            <button id="modalSubmitBtn" name="modalSubmitBtn" class="btn btn-primary submitBttn modalSubmitBtn" data-item-id="{{ $panelProduct->id }}">
                <i class="fa fa-save"></i>
                <span class="spinner-border spinner-border-sm" style="display: none;" role="status" aria-hidden="true"></span>
                Submit
            </button>

        </div>
    </div>

{{-- </form> --}}

