@extends('layouts.guest.v1.sg-launch-main')

@section('content')


<div class="container-fluid bg-md bg-sm h-auto d-inline-block w-100">

    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 mb-0 pt-2 pb-3">
            <img class="mw-100" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
        </div>
    </div>
    <div class="row w-100 h-auto d-inline-block mt-5 mx-auto" style="border-radius:10px;">

        <div class="row col-md-8 col-11 mx-auto bg-white h-auto px-0" style="border-radius:10px;">
            <!-- Payment column -->
            <div class="order-12 order-md-1  col-12 col-lg-8 col-md-7 mt-lg-3 pt-3 h-auto d-inline-block">
                <h3 class="centerTextMobile">Select Payment Method</h3>

                <!-- Payment options tabs -->
                <ul class="nav nav-tabs m-2" id="myTab" role="tablist">
                    @if (country()->country_id == 'SG')
                    <li class="nav-item col-12 col-md-4 m-md-0 p-md-1 col-xl-4 mx-auto my-2 my-lg-2 my-xl-1">
                        <a class="m-0 pb-0 mx-auto px-2 col-12 px-md-0 p-md-1 payment-method nav-link text-center active" name="sg_cc" id="sg_cc-tab"
                            data-toggle="tab" href="#sg_cc" role="tab" aria-controls="sg_cc" aria-selected="true">
                            <img class="hide-active mx-auto paymentIcon"
                                src="{{ asset('images/flags/card-SG-1.png') }}" alt="">
                            <img class="hide-inactive mx-auto paymentIcon"
                                src="{{ asset('images/flags/card-SG-2.png') }}" alt="">
                            Credit/Debit Card (Singapore Dollar)
                        </a>
                    </li>
                    @endif
                    <li class="nav-item col-12 col-md-4 m-md-0 p-md-1 col-xl-4 mx-auto my-2 my-lg-2 my-xl-1">
                        <a class="m-0 pb-0 px-2 px-md-0 col-12 mx-auto p-md-1 payment-method nav-link text-center" name="home_tab" id="home-tab"
                            data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                            <img class="hide-active mx-auto paymentIcon"
                                src="{{ asset('images/flags/card-MY-1.png') }}" alt="">
                            <img class="hide-inactive mx-auto paymentIcon"
                                src="{{ asset('images/flags/card-MY-2.png') }}" alt="">
                                Credit/Debit Card (Ringgit Malaysia)

                        </a>
                    </li>
                    @if (country()->country_id == 'MY')
                    <li class="nav-item col-12 col-md-4 m-md-0 p-md-1 col-xl-4 mx-auto my-2 my-lg-2 my-xl-1">
                        <a class="m-0 pb-0 px-2 px-md-0 col-12 mx-auto p-md-1 payment-method nav-link text-center disabled" id="fpx" data-toggle="tab"
                            href="#fpx" role="tab" aria-controls="fpx" aria-selected="false"
                            style="background: white; border: 1px solid lightgrey;">
                            <img class="hide-active mx-auto paymentIcon"
                                src="{{ asset('storage/icons/payments/online-banking-02.png') }}" alt="">
                            <img class="hide-inactive mx-auto paymentIcon"
                                src="{{ asset('storage/icons/payments/online-banking-01.png') }}" alt="">FPX Online
                            Banking
                            <p class="m-0">(Coming Soon)</p>
                        </a>
                    </li>
                    {{-- <li class="nav-item col-12 col-md-4 m-md-0 p-md-1 col-xl-4 mx-auto my-lg-2 my-xl-1 ">
                        <a class="m-0 pb-0 px-2 px-md-0 col-12 mx-auto p-md-1 payment-method nav-link text-center  @if((!$bluepointPurchase) || ($rebateAllowed == false) || country()->country_id == 'SG') disabled @endif"
                            id="blue-point-tab" data-toggle="tab" href="#blue-point" role="tab" aria-controls="contact"
                            aria-selected="false" style="background: white; border: 1px solid lightgrey;">
                            <img class="hide-active mx-auto paymentIcon"
                                src="{{ asset('storage/icons/payments/blue-point-02.png') }}" alt="">
                            <img class="hide-inactive mx-auto paymentIcon"
                                src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="">
                            Blue Point
                        </a>
                    </li> --}}
                    @endif
                    <li class="nav-item col-12 col-md-4 m-md-0 p-md-1 col-xl-4 mx-auto my-2 my-lg-2 my-xl-1">
                        <a class="m-0 pb-0 px-2 px-md-0 col-12 mx-auto p-md-1 payment-method nav-link text-center" id="contact-tab" data-toggle="tab"
                            href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                            <img class="hide-active mx-auto paymentIcon"
                                src="{{ asset('storage/icons/payments/offline-payment-02.png') }}" alt="">
                            <img class="hide-inactive mx-auto paymentIcon"
                                src="{{ asset('storage/icons/payments/offline-payment-01.png') }}" alt="">Offline
                            Payment
                        </a>
                    </li>
                </ul>
                <div class="col-12 mx-auto installmentSG">
                    @if($canInstallment)
                        @include('shop.payment.credit-payment')
                    @endif
                </div>
                <!-- Payment form -->
                <div class="tab-content m-3" id="myTabContent">

                    <!-- SG Card Form -->
                    <div class="tab-pane fade {{ country()->country_id == 'SG' ? 'active show' : '' }} " id="sg_cc"
                        role="tabpanel" aria-labelledby="sg_cc-tab">
                        <div class="row">
                            <div class="col-11 p-0 mx-auto">
                                <form id="sg-card-form" action="{{ route('sg.launch.payment')}}" method="POST">
                                    @csrf

                                    <input type="hidden" name="total_price" value="{{ $launchCart->total }}">

                                    <div class="form-row my-2">
                                        <div class="col-12 mx-auto mb-1 form-group">
                                            After clicking "Pay Now", you will be redirected to PayNow Singapore -
                                            Powered by HitPay to complete your purchase securely.
                                        </div>
                                    </div>
                                    <div class="form-row mt-2">
                                        <div class="col-12">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="sg-credit-debit-agree">
                                                <label class="custom-control-label" for="sg-credit-debit-agree">
                                                    I have read, understand and accepted the <a href=""
                                                        data-toggle="modal" data-target="#purchaseAgreementModal">terms
                                                        and
                                                        conditions</a>.
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row mt-2">
                                        <div class="col-auto ml-auto myauto">
                                            {{-- <input type="hidden" name="payment_option" value="card"> --}}
                                            <input type="hidden" name="payment_option" value="Hitpay">
                                            <button id="sg-pay-now-button" type="submit"
                                                class="btn btn-warning btn-block btn-payment-submit"  style="width:auto" type="button"
                                                disabled>Pay Now</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Cybersource Card Form -->
                    <div class="tab-pane fade {{ country()->country_id == 'MY' ? 'active show' : '' }}" id="home"
                        role="tabpanel" aria-labelledby="home-tab">
                        <form id="my-card-form" action="{{ route('sg.launch.payment')}}" method="POST">
                            @csrf
                                <div class="row">
                                    <div class="col-11 p-0 mx-auto">
                                        <div class="form-row my-2">
                                            <div class="col-12 mb-1  form-group">
                                                @include('shop.payment.hidden-address')
                                                @include('shop.payment.hidden-cybersource')

                                                <input type="hidden" name="total_price"
                                                    value="{{ number_format(($launchCart->purchase_amount_myr / 100), 2) }}">

                                                <label class="m-0" for="card_number"><b>Card Number <small
                                                            class="text-danger">*</small></b></label>
                                                <input type="text" class="form-control" name="card_number" id="card_number"
                                                    placeholder="Card Number" autocomplete="off">
                                                <input type="hidden" name="card_type" id="card_type" value="">
                                                <div class="valid-feedback feedback-icon with-cc-icon">
                                                    <i class="fa"></i>
                                                </div>
                                                <div class="invalid-feedback feedback-icon with-helper">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div class="invalid-feedback invalid-helper">
                                                    <small class="text-danger">Card number must be 16.</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row my-2">
                                            <div class="col-12 mb-1 form-group">
                                                <label class="m-0" for="name_on_card"><b>Name On Card <small
                                                            class="text-danger">*</small></b></label>
                                                <input type="text" class="form-control" name="name_on_card"
                                                    id="name_on_card" placeholder="Name On Card" autocomplete="off">
                                                <div class="valid-feedback feedback-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div class="invalid-feedback feedback-icon with-helper">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div class="invalid-feedback invalid-helper">
                                                    <small class="text-danger">Please enter card owner name.</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row my-2">
                                            <div class="col-12 col-lg-6 mb-1 form-group">
                                                <label class="m-0" for="expiry_date"><b>Expiration Date <small
                                                            class="text-danger">*</small></b></label>
                                                <input type="text" class="form-control" name="expiry_date" id="expiry_date"
                                                    placeholder="MMYY" maxlength="4" autocomplete="off">
                                                <div class="valid-feedback feedback-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div class="invalid-feedback feedback-icon">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                            </div>

                                            <div class="col-12 col-lg-6 mb-1 form-group">
                                                <label class="m-0" for="cvv"><b>CVV <small
                                                            class="text-danger">*</small></b></label>
                                                <input type="text" class="form-control" name="cvv" id="cvv"
                                                    placeholder="CVV" autocomplete="off">
                                                <div class="valid-feedback feedback-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div class="invalid-feedback feedback-icon">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row mt-2">
                                            <div class="col-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="my-credit-debit-agree">
                                                    <label class="custom-control-label" for="my-credit-debit-agree">
                                                        I have read, understand and accepted the <a href=""
                                                            data-toggle="modal" data-target="#purchaseAgreementModal">terms
                                                            and
                                                            conditions</a>.
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row mt-2">
                                            <div class="col-auto ml-auto">
                                                <input type="hidden" name="installmentRadio" class="installmentRadio" value="full">

                                                <input type="hidden" name="installmentSG" id="amountSG" class="installmentSG" value="{{ number_format(($launchCart->installmentSG / 100),2) }}">
                                                <input type="hidden" name="installmentMY" id="amountMY" class="installmentMY" value="{{ number_format(($launchCart->installmentMY / 100),2) }}">
                                                <input type="hidden" name="fullSG" id="amountSG" class="fullSG" value="{{ number_format(($launchCart->fullSG / 100),2) }}">
                                                <input type="hidden" name="fullMY" id="amountMY" class="fullMY" value="{{ number_format(($launchCart->fullMY / 100),2) }}">
                                                <input type="hidden" name="totalAmount" id="totalAmount" class="totalAmount" value="{{ number_format(($launchCart->fullMY / 100),2) }}">
                                                <input type="hidden" name="payment_option" value="Cybersource">
                                                <button id="my-pay-now-button" type="submit"
                                                    class="btn btn-warning btn-block btn-payment-submit"  style="width:auto" type="button"
                                                    disabled>Pay Now</button>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                        </form>
                    </div>

                    <!-- Offline Form -->
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div>
                            <div class="row">
                                <div class="col-11 p-0 mx-auto">
                                    <form id="offline-form" action="{{ route('sg.launch.payment')}}" method="GET"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-row my-2">
                                            <div class="col-12 mb-1 form-group">
                                                @include('shop.payment.hidden-address')
                                                <input type="hidden" name="installmentRadio" class="installmentRadio" value="full">
                                                <label class="m-0" for="reference_number"><b> Reference Number <small
                                                            class="text-danger">*</small></b></label>
                                                <input type="text" class="form-control" name="reference_number"
                                                    id="reference_number_offline"
                                                    placeholder="Payment Reference Number">
                                                <div class="valid-feedback feedback-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div class="invalid-feedback feedback-icon with-helper">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div class="invalid-feedback invalid-helper">
                                                    <small class="text-danger">Please enter reference number (digits
                                                        only).</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row my-2">
                                            <div class="col-12 mb-1 form-group">
                                                <label class="m-0" for="payment_proof"><b>Upload Payment Proof <small
                                                            class="text-danger">*</small></b></label>
                                                <div class="custom-file">
                                                    <input type="file" name="payment_proof" class="custom-file-input"
                                                        accept="image/*" id="payment_proof">
                                                    <label class="custom-file-label text-left" for="payment_proof">Choose File
                                                        <small>(.jpg, .png)</small></label>
                                                    <div class="valid-feedback feedback-icon">
                                                        <i class="fa fa-check"></i>
                                                    </div>
                                                    <div class="invalid-feedback feedback-icon with-helper">
                                                        <i class="fa fa-times"></i>
                                                    </div>
                                                    <div class="invalid-feedback invalid-helper">
                                                        <small class="text-danger">Please attach payment
                                                            proof.</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row my-2">
                                            <div class="col-12 mb-1 form-group">
                                                <label class="m-0" for="payment_amount"><b>Payment Amount
                                                        (SGD / RM)<small
                                                            class="text-danger">*</small></b></label>
                                                <input type="text" class="form-control input-mask-price"
                                                    name="payment_amount" id="payment_amount"
                                                    placeholder="Payment amount">
                                                <div class="valid-feedback feedback-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div class="invalid-feedback feedback-icon with-helper">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div class="invalid-feedback invalid-helper">
                                                    <small class="text-danger">Please enter payment amount.</small>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-row mt-2">
                                            <div class="col-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="offline-agree">
                                                    <label class="custom-control-label" for="offline-agree">
                                                        I have read, understand and accepted the <a href=""
                                                            data-toggle="modal"
                                                            data-target="#purchaseAgreementModal">terms
                                                            and
                                                            conditions</a>.
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row mt-2">
                                            <div class="col-auto ml-auto">
                                                <input type="hidden" name="payment_option" value="Offline">
                                                <button href="" id="submit-payment-button"
                                                    class="btn btn-warning btn-block btn-payment-submit" style="width:auto" type="submit"
                                                    disabled>Submit Payment</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Order Summary -->
            <div class="order-1 order-md-12 col-12 col-lg-4 col-md-5 border pt-md-5 pt-lg-5 px-0 h-auto d-inline-block"
                style="background:lightgrey; border-radius:10px;">

                <div class="card border-0" style="background:none;">
                    <div class="card-body">
                        <h4>Order Summary</h4>
                        <div>
                            <p class="text-muted">Subtotal ({{ $launchCart->quantity }} item(s)@if(country()->country_id =="MY") and shipping fee
                                included) @else , shipping fees and 7% GST included) @endif</p>
                        </div>
                        <div class="row">
                            <div class="col-5 pr-0">
                                <h5>Total Amount</h5>
                            </div>
                            <div class="col-7 text-lg-right">
                                <h4 class="text-danger font-weight-bold">
                                    {{country()->countryCurrency}} <span class="convertedPurchaseAmount"> {{ $launchCart->total }}</span>
                                </h4>

                                @if(country()->country_id != 'MY')
                                <p class="text-muted">~ MYR
                                    <span class="purchaseAmount">{{ number_format(($launchCart->fullMY / 100),2) }}</span>
                                </p>
                                @else
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="card border-0" style="background:none;">
                    <div class="card-body">
                        <h5 class="mb-1 font-weight-bold" style="font-size: 1.1rem;">We Accept: </h5>
                        <div class="row">
                            <div class="col-4 col-md-6 my-auto ml-auto">
                                <img src="{{ asset('storage/icons/payments/visa-logo.png') }}" class="w-100 p-2" alt="">
                            </div>
                            <div class="col-4 col-md-6 my-auto mr-auto">
                                <img src="{{ asset('storage/icons/payments/mastercard-logo.png') }}" class="w-100 p-2"
                                    alt="">
                            </div>
                            <!-- <div class="col-4 my-auto">
                                    <img src="{{ asset('storage/icons/payments/fpx-logo.png') }}" class="w-100 p-2" alt="">
                                </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- Spanner Modal -->
<div class="modal fade" id="spannerModel" tabindex="-1" role="dialog" aria-labelledby="spannerModelLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="spanner">
                <div class="loader"></div>
                <p>Preparing your order, please be patient.</p>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
@include('shop.payment.purchase-agreement')


@endsection

<style>
    .mt-3.no-margin {
        margin-top: 0px !important;
    }
</style>
@push('style')
<style>
    .nav-tabs .nav-link {
        /* border-color: #e9ecef #e9ecef #dee2e6; */
        border: 1px solid lightgrey;
        border-radius: 7px;
    }

    .nav-tabs .nav-link:hover,
    .nav-tabs .nav-link:focus,
    .nav-tabs .nav-link.active,
    .nav-tabs .nav-link.active:focus {
        /* border-color: #e9ecef #e9ecef #dee2e6; */
        border: 0.11em solid #ffcc00;
        border-radius: 7px;
    }



    .payment-method {
        background: white;
        margin: 0 5px;
        height: 130px;
        width: 160px;
        /* border: 0.1em lightgrey solid; */
    }

    .spanner {

        text-align: center;
        color: #000;

    }

    .loader,
    .loader:before,
    .loader:after {
        border-radius: 50%;
        width: 2.5em;
        height: 2.5em;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation: load7 1.8s infinite ease-in-out;
        animation: load7 1.8s infinite ease-in-out;
    }

    .loader {
        color: #fccb34;
        font-size: 10px;
        margin: 80px auto;
        position: relative;
        text-indent: -9999em;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }

    .loader:before,
    .loader:after {
        content: '';
        position: absolute;
        top: 0;
    }

    .loader:before {
        left: -3.5em;
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }

    .loader:after {
        left: 3.5em;
    }

    @-webkit-keyframes load7 {

        0%,
        80%,
        100% {
            box-shadow: 0 2.5em 0 -1.3em;
        }

        40% {
            box-shadow: 0 2.5em 0 0;
        }
    }

    @keyframes load7 {

        0%,
        80%,
        100% {
            box-shadow: 0 2.5em 0 -1.3em;
        }

        40% {
            box-shadow: 0 2.5em 0 0;
        }
    }

    .spanner {

        text-align: center;
        color: #000;

    }

    .loader,
    .loader:before,
    .loader:after {
        border-radius: 50%;
        width: 2.5em;
        height: 2.5em;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation: load7 1.8s infinite ease-in-out;
        animation: load7 1.8s infinite ease-in-out;
    }

    .loader {
        color: #fccb34;
        font-size: 10px;
        margin: 80px auto;
        position: relative;
        text-indent: -9999em;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }

    .loader:before,
    .loader:after {
        content: '';
        position: absolute;
        top: 0;
    }

    .loader:before {
        left: -3.5em;
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }

    .loader:after {
        left: 3.5em;
    }

    @-webkit-keyframes load7 {

        0%,
        80%,
        100% {
            box-shadow: 0 2.5em 0 -1.3em;
        }

        40% {
            box-shadow: 0 2.5em 0 0;
        }
    }

    @keyframes load7 {

        0%,
        80%,
        100% {
            box-shadow: 0 2.5em 0 -1.3em;
        }

        40% {
            box-shadow: 0 2.5em 0 0;
        }
    }

    .nav-tabs {
        border-bottom: 0;
    }

    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .nav-link .hide-inactive {
        display: none;
    }

    .nav-link .hide-active {
        display: block;
    }

    .nav-link.active .hide-active {
        display: none;
    }

    .nav-link.active .hide-inactive {
        display: block;
    }

    .profile-menu:active img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    .profile-menu:hover img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    /* .myDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    } */
    .myDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .agentDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    } */
    .agentDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .panelDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    } */
    .panelDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .logout-menu:active, img{
    content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }*/

    .logout-menu:hover img {
        content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }
    @media only screen and (max-width: 600px) {
            .myauto{
            margin:auto;
        }
    }

    /* Small devices (landscape phones, 576px and up) */
    @media (max-width: 576px) {
        /*  */
        .paymentIcon{
            width: 60px;
            height: 60px;
        }
        .centerTextMobile{
            text-align: center;
        }
    }

    /* Medium devices (tablets, 768px and up) */
    @media (min-width: 768px) {
        /*  */
        .paymentIcon{
            width: 64px;
            height: 64px;
        }

    }

    /* Large devices (desktops, 992px and up) */
    @media (min-width: 992px) {
        /*  */
        .paymentIcon{
            width: 55px;
            height: 55px;
        }
        .centerTextMobile{
            padding-left:15px;
        }

    }

    /* X-Large devices (large desktops, 1200px and up) */
    @media (min-width: 1200px) {
        /*  */
        .paymentIcon{
            width: 64px;
            height: 64px;
        }

    }

    /* XX-Large devices (larger desktops, 1400px and up) */
    @media (min-width: 1400px) {
        /*  */
        .paymentIcon{
            width: 64px;
            height: 64px;
        }

    }
</style>
@endpush

@push('script')
<script>

    $("input[name=paymentType]" ).click(function() {
        console.log(this.value);
        $('.installmentRadio').val(this.value);
    });
    $("#home-tab" ).click(function() {
        $('.installmentSG').show();
    });
    $("#sg_cc-tab" ).click(function() {
        $('.installmentSG').hide();
    });
    $("#contact-tab" ).click(function() {
        $('.installmentSG').show();
    });


    let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '9999999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }

        $(document).ready(function() {

            $('.installmentSG').hide();

            $('input:checkbox').prop('checked', false);


            let offlineMessage = true;



            // Pass data to button modal
            $('#addressConfirmationModal').on('show.bs.modal', function(e) {
                var payment_type = $(e.relatedTarget).data('payment-type');
                // get data-payment-type from form submit button
                $("#submitButton").data( "payment-type", payment_type );
                // check which button trigger this popup
                console.log($("#submitButton").data( "payment-type"));
                });


            // Submit button in popup click function
            $("#submitButton").click(function() {

                $("div.spanner").addClass("show");
                $("div.overlayed").addClass("show");
                // $('#spannerModel').modal('show');
                $('#addressConfirmationModal').modal('hide');
                var payment_type = $(this).data('payment-type');
                var address_1 = $('#attention_address_1').val();
                // console.log(address_1);
                // check button type
                console.log('payment_type');
                console.log(payment_type);
                if ((payment_type == 'offline') && (address_1 != '')){
                    $("#offline-form").submit();
                } else if ((payment_type == 'card')&& (address_1 != ''))
                {
                    $("#card-form").submit();
                }  else if ((payment_type == 'bluepoint')&& (address_1 != ''))
                {
                    $('#spannerModel').modal('show');
                    $("#blue-point-form").submit();

                } else {
                    $('#spannerModel').modal('hide');
                }
            });

            /* Author: Wan Shahruddin */
            // Display tab pane when anchor tag is clicked.
            $('.nav-link').on('click', function() {
                $('#myTabContent').css('border', '1px solid #dee2e6');
            });

            // Edit Details JS
            console.log($('#attention_to'));

            $('#attention_to').on('keyup', function() {
                if ($(this).val().length == 0) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#attention_contact').on('keyup', function() {
                if ($(this).val().length > 14 || $(this).val().length < 10) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#attention_address_1').on('keyup', function() {
                if ($(this).val().length == 0) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            // $('#attention_address_2').on('keyup', function() {
            //     if ($(this).val().length == 0) {
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     } else {
            //         $(this).removeClass('is-invalid');
            //         $(this).addClass('is-valid');
            //     }
            // });

            $('#attention_postcode').on('keyup', function() {
                if ($(this).val().length < 5 || $(this).val().length > 5) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#attention_city').on('keyup', function() {
                if ($(this).val().length == 0) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#update-customer-detail').on('submit', function(e) {
                let error = 0;

                if ($('#attention_to').val().length == 0) {
                    error = error + 1;
                }

                if ($('#attention_contact').val().length > 14 || $('#attention_contact').val().length < 10) {
                    error = error + 1;
                }

                if ($('#attention_address_1').val().length == 0) {
                    error = error + 1;
                }

                // if ($('#attention_address_2').val().length == 0) {
                //     error = error + 1;
                // }

                if ($('#attention_postcode').val().length < 5 || $('#attention_postcode').val().length > 5) {
                    error = error + 1;
                }

                if ($('#attention_city').val().length == 0) {
                    error = error + 1;
                }

                if (error == 0) {
                    return true;
                } else {
                    return false;
                }
            });
            // End Edit Details JS

            // Credit/Debit Card JS
            // Credit card pattern recognition algorithm.
            function detectCardType(number) {
                var re = {
                    // electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
                    // maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
                    // dankort: /^(5019)\d+$/,
                    // interpayment: /^(636)\d+$/,
                    // unionpay: /^(62|88)\d+$/,
                    visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
                    mastercard: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/,
                    // amex: /^3[47][0-9]{13}$/,
                    // diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
                    // discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
                    // jcb: /^(?:2131|1800|35\d{3})\d{11}$/
                }

                for (var key in re) {
                    if (re[key].test(number)) {
                        return key
                    }
                }
            }

    //         // card expiry_date
    //         //Put our input DOM element into a jQuery Object
    //             var $jqDate = jQuery('input[name="expiry_date"]');

    //         //Bind keyup/keydown to the input
    //         $jqDate.bind('keyup','keydown', function(e){

    //         //To accomdate for backspacing, we detect which key was pressed - if backspace, do nothing:
    //         console.log(e.which);
    //             if(e.which !== 8 ) {
    // //                 if ( charCode != 45 && charCode > 31
    // // && (charCode < 48 || charCode > 57))
    // //  return false;
    //                 var numChars = $jqDate.val().length;
    //                 if(numChars == 2 ){
    //                     var thisVal = $jqDate.val();
    //                     thisVal += '-';
    //                     $jqDate.val(thisVal);
    //                 }
    //         }
    //         });
            //

            // Form Validation - Credit/Debit Card.
            let cardNumber = null;
            let ccIcon = $('.valid-feedback.feedback-icon.with-cc-icon i');
            // Card Number.
            $('#card_number').on('keyup', function() {
                cardNumber = $(this).val();
                cardType = detectCardType(cardNumber);

                if ($(this).val().length < 7 || cardType == undefined) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                    ccIcon.removeClass();
                    ccIcon.addClass('fa fa-cc-' + cardType).css('font-size', '30px');
                    $('#card_type').val(cardType);
                }
            });

            $('#name_on_card').on('keyup', function() {
                if ($(this).val().length == 0) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#expiry_date').on('keyup', function() {
                if ($(this).val().length != 4) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#cvv').on('keyup', function() {
                if ($(this).val().length < 3 || !$.isNumeric($(this).val())) {
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            $('#card-form').on('submit', function(e) {
                let error = 0;

                if (
                    $('#card_number').val().length < 7 ||
                    cardType == undefined
                ) {
                    error = error + 1;
                    $('#card_number').addClass('is-invalid');
                    $('#card_number').focus();
                }
                if ($('#name_on_card').val().length == 0) {
                    error = error + 1;
                    $('#name_on_card').addClass('is-invalid');
                    $('#name_on_card').focus();
                }
                if ($('#expiry_date').val().length != 4 ) {
                    error = error + 1;
                    $('#expiry_date').addClass('is-invalid');
                    $('#expiry_date').focus();
                }
                if ($('#cvv').val().length < 3 || !$.isNumeric($('#cvv').val())) {
                    error = error + 1;
                    $('#cvv').addClass('is-invalid');
                    $('#cvv').focus();
                }

                if (error == 0) {
                    $('#spannerModel').modal('show');
                    console.log('true');
                    return true;

                } else {
                    $('#spannerModel').modal('hide');
                    console.log('false');
                    return false;


                }
            });

            // End Form Validation - Credit/Debit Card
            // End Credit/Debit Card JS

            // Form Validation - FPX

            // End Form Validation - FPX


            // Offline JS
            let fileName = null;
            let nextSibling = null;

            // Change file input label to filename when a file has been selected.
            $('.custom-file-input').on('change', function(e) {
                fileName = $('#payment_proof').val().split('\\').pop();
                nextSibling = e.target.nextElementSibling;
                nextSibling.innerText = fileName;
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            });

            $('#reference_number_offline').on('keyup', function() {
                if ($(this).val().length < 1 || $(this).val() == '') {

                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            // $('#payment_proof').on('keyup', function() {

            //     if ($(this).get(0).files.length == 0)
            //     {
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     } else {
            //         $(this).removeClass('is-invalid');
            //         $(this).addClass('is-valid');
            //     }
            // });

            $('#payment_amount').on('keyup', function() {
                if ($(this).val().length == 0 || $(this).val() == '') {

                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).addClass('is-valid');
                }
            });

            // Form Validation - Offline
            $('#offline-form').on('submit', function(e) {
                let error = 0;

                if (
                    $('#reference_number_offline').val().length < 1 ||
                    $('#reference_number_offline').val() == '')
                 {
                    error = error + 1;
                    $('#reference_number_offline').addClass('is-invalid');

                }

                var allowedExtensions =
                    /(\.jpg|\.jpeg|\.png|\.gif)$/i;

                if ($('#payment_proof').get(0).files.length == 0 ||
                        !allowedExtensions.exec(fileName))
                 {
                    error = error + 1;
                    $('#payment_proof').addClass('is-invalid');

                }
                if (
                    $('#payment_amount').val().length == 0 ||
                    $('#payment_amount').val() == ''
                ) {
                    error = error + 1;
                    $('#payment_amount').addClass('is-invalid');

                }

                if (error == 0) {
                    $('#spannerModel').modal('show');
                    console.log('true');
                    return true;
                } else {
                    $('#spannerModel').modal('hide');
                    console.log('false');
                    return false;
                }
            });


            // End Offline JS
            // End Form Validation - Offline
            $('#sg-credit-debit-agree').on('change', function() {
                if ($('#sg-credit-debit-agree').prop('checked', true)) {
                    $('#sg-pay-now-button').prop('disabled', false);
                } else {
                    $('#sg-pay-now-button').prop('disabled', true);
                }
            });

            $('#my-credit-debit-agree').on('change', function() {
                if ($('#my-credit-debit-agree').prop('checked', true)) {
                    $('#my-pay-now-button').prop('disabled', false);
                } else {
                    $('#my-pay-now-button').prop('disabled', true);
                }
            });
            $('#offline-agree').on('change', function() {
                if ($('#offline-agree').prop('checked', true)) {
                    $('#submit-payment-button').prop('disabled', false);
                } else {
                    $('#submit-payment-button').prop('disabled', true);
                }
            });


            /* End Author */

        });
</script>
@endpush
