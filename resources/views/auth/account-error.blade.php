@extends('layouts.guest.main')

@section('content')
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('User Registration Error') }}</div>

                <div class="card-body">
                   
                    {{ __('Your user account has not been correctly registered.  Please send an email to') }}

                    <a style="font-size: 1.15rem; font-weight: 600;"
                        href="mailto:formula2u-cs@delhubdigital.com">formula2u-cs@delhubdigital.com</a> for further
                    assistance.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
