<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealerDirectPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_direct_pay', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid_link');
            $table->longText('cart_id');
            $table->integer('user_id');
            $table->integer('purchase_id')->nullable();
            $table->longText('customer_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_direct_pay');
    }
}
