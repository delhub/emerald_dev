
<div class="tab-content border m-lg-3" id='installmentWrapper'>

    <div class="row">
        <div class="col-11 p-4 mx-auto ">
            <div class="form-row my-2">
                <div class="col-auto mr-3 mb-1 form-group" id="fullPaymentDiv">
                    <input type="radio" name="paymentType" class="pointType" id="fullPayment" value="fullcash" checked> <span
                        class="px-2" style="font-size: 0.9rem; font-weight: 400; line-height: 1.6;color: #495057;">Full Cash</span>
                </div>
                <div class="col-auto mb-1 form-group">
                    <input type="radio" name="paymentType" class="pointType" id="halfPoint" value="installment">
                    <span style="font-size: 0.9rem; font-weight: 400; line-height: 1.6;color: #495057;">Half Point</span>
                </div>

            </div>
            <div id="installmentPayBox" class="form-row" style="display:none;">
                <div class="col-md-6 col-12 mb-1 form-group">

                    <div class="row">
                        <div class="col-5 ">
                            <label class="m-0" for="card_number"><b>Available Point</b></label>
                        </div>
                        <div class="col-5 border">
                            {{ $user->userInfo->user_points }}
                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-12 mb-1 form-group">
                    <div class="row">
                        <div class="col-5">
                            <label class="m-0" for="card_number"><b>Point Needed</b></label>
                        </div>
                        <div class="col-6 border">
                            {{ array_sum(array_column($cartsItems,'half_point')) }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script>
//

</script>
@endpush
