@php use Illuminate\Support\Facades\Auth;; @endphp

<div class="container" style="position: relative; min-height: 85vh;margin-top:80px;">
    <div class="card container">
        <div class="card-body">
            <h1 class="text-center">
                Thank you! Your order has been placed.
            </h1>
            <h3 class="text-center">
                Your order number is {{ $purchaseNumberFormatted }}.
            </h3>
            <div class="text-center mt-3">

                @if (Auth::user()->dealerInfo && !Auth::user()->dealerInfo->nric )
                {{-- country()->country_id == 'SG' && () --}}
                <a class="btn bjsh-btn-gradient" href="/shop">Continue Agent Registration</a>
                @else

                @if ($purchase->purchase_status == 4000 && $purchase->purchase_type == 'Hitpay')
                <a class="btn bjsh-btn-gradient" href="{{ route('hitpay.request',[$purchase->purchase_number]) }}">Check
                    Status</a>
                @endif

                @if ($installment)
                <img src="/storage/icons/payments/DC-Credit.png" alt="" class="px-1" style="height: 35px;">
                <a class="btn bjsh-btn-gradient" href="@dc_credit_url($purchase)">Apply DC Credit</a>
                <span class="pl-4"></span>
                @else
                <a class="btn bjsh-btn-gradient" href="/shop">Continue Shopping</a>
                @endif

                <a class="btn bjsh-btn-gradient" href="/shop/dashboard/orders/index">View Orders</a>

                @endif

            </div>

            <div class="mt-3">
                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Items</th>
                    </tr>
                    <?php
                    $iterationNo = 0;
                    ?>

                    @foreach($purchase->sortItems() as $item)

                    <tr class="@if( $item->bundle_id == 0) non-op @else op @endif">
                        <td class="align-middle">
                            @if( $item->bundle_id == 0) {{ $iterationNo = $iterationNo + 1 }} @endif
                        </td>
                        <td style="max-width: 400px;">
                            <div class="row mb-2">
                                <div class="col-4 col-md-2 my-auto">
                                    @if (isset($item->product_information['product_color_img']))

                                    @php
                                    $value =
                                    $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first()
                                    @endphp

                                    <img class="mw-100 rounded d-inline "
                                        style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}}"
                                        src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                    @elseif(isset($item->product_information['product_color_code']))
                                    @php
                                    $value =
                                    $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first()
                                    @endphp

                                    <img class="mw-100 rounded d-inline "
                                        style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};width: 76px;height: 76px;">

                                    @else
                                    {{-- <p class="no-image">no images</p> --}}

                                    <img class="mw-100 rounded d-inline "
                                        src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}"
                                        alt="">

                                    @endif

                                </div>
                                <div class="col-8 col-md-10 my-auto">
                                    <p class="mb-0 font-weight-bold">{{ $item->product->parentProduct->name }} x
                                        {{ $item->quantity }}</p>
                                    @if(array_key_exists('product_color_name', $item->product_information))
                                    <p class="text-capitalize m-0">Color:
                                        {{ $item->product_information['product_color_name'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_size', $item->product_information))
                                    <p class="text-capitalize m-0">Size:
                                        {{ $item->product_information['product_size'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_curtain_size', $item->product_information))
                                    <p class="text-capitalize m-0">Curtain Model:
                                        {{ $item->product_information['product_curtain_size'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_miscellaneous', $item->product_information))
                                    <p class="text-capitalize m-0">
                                        {{ $item->product_information['product_miscellaneous'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('invoice_number', $item->product_information))
                                    <p class="text-capitalize m-0">
                                        Invoice Number: {{ $item->product_information['invoice_number'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_temperature', $item->product_information))
                                    <p class="text-capitalize m-0">Color Temperature:
                                        {{ $item->product_information['product_temperature'] }}</p>
                                    @endif
                                    @if(array_key_exists('product_preorder_date', $item->product_information))
                                    <p class="text-capitalize m-0">Pre Order Delivery:
                                        {{ $item->product_information['product_preorder_date'] }}</p>
                                    @endif
                                    @if(array_key_exists('product_order_remark', $item->product_information))
                                    <p class="text-capitalize m-0">Remarks:
                                        {{ $item->product_information['product_order_remark'] }}</p>
                                    @endif
                                    @if(array_key_exists('product_order_selfcollect', $item->product_information) &&
                                    $item->product_information['product_order_selfcollect'])
                                    <p class="text-capitalize m-0">Self Collect: Yes
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_order_tradein', $item->product_information) &&
                                    $item->product_information['product_order_tradein'])
                                    <p class="text-capitalize m-0">Rebate: - RM
                                        {{ number_format($item->product_information['product_order_tradein'] / 100),2 }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_installation', $item->product_information) &&
                                    $item->product_information['product_installation'])
                                    <p class="text-capitalize m-0">Installation:
                                        {{($item->installationCategories()!=NULL) ? $item->installationCategories()->cat_name : ''}}
                                    </p>
                                    @endif

                                </div>
                            </div>
                        </td>
                    </tr>

                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@push('style')
<style>
    tr.op {}

    .non-op td {
        border-top: 1px solid #dee2e6;
    }

    .op td {
        border: none;
    }

    .op td img {
        width: 60%;
        /* height: 48px; */
        float: right;
        object-fit: contain;
    }

    @media only screen and (max-width: 414px) {
        .op td img {
            width: 100%;
            object-fit: contain;
        }

        .table .op td {
            padding: 0.3rem;
        }

        a.btn.bjsh-btn-gradient {
            width: 47%;
            margin-left: 5px;
            margin-bottom: 10px;
        }
    }
</style>
@endpush
