<?php

namespace App\Console\Commands;

use App\Models\Users\User;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Users\UserPoints;

class UserPointsCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'userpoints:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check User Points';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $userPoints = UserPoints::where('point_type','credit')->where('status','pending')->where('created_at', '<=', Carbon::now()->subDays(7)->toDateTimeString())->get();
        $balance = 0;

        if ($userPoints) {
            foreach ($userPoints as $userPoint) {
                $userPoint->status = 'approved';
                $userPoint->approved_at = Carbon::now();
                $userPoint->save();
                $balance =+ $userPoint->amount;
            }

            $user = User::where('id',$userPoint->user_id)->first();
                $user->userInfo->user_points = $balance;
                $user->userInfo->save();
        }




    }
}
