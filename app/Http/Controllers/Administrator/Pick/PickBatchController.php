<?php

namespace App\Http\Controllers\Administrator\Pick;

use setasign\Fpdi\Fpdi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Warehouse\Bunbundle\BunbundleController;
use App\Http\Controllers\Administrator\Courier\CourierController;
use App\Models\Pick\PickBatch;
use App\Models\Pick\PickBin;
use App\Models\Pick\PickBatchDetail;
use App\Models\Purchases\Order;
use App\Models\Users\User;
use App\Models\Products\WarehouseInventories;
use App\Traits\TableLogs;
use App\Models\Globals\Status;
use App\Models\Globals\State;
use App\Models\Courier\Courier;
use App\Models\Products\ViewInventoriesStock;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Models\Purchases\Counters;
use App\Models\Warehouse\Order\WarehouseOrder;
use PDF;
use Illuminate\Support\Facades\File;
use App\Models\Purchases\Item;
use App\Models\Purchases\ItemBundle;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundle;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundleItem;
use App\Models\Products\WarehouseBatchItem;

class PickBatchController extends Controller
{
    use TableLogs;

    public static $header;
    public static $home;

    public static $startHeader;
    public static $start;
    public static $startWork;

    public static $headerEnd;
    public static $end;
    public static $endWork;

    public static $sortStartHeader;
    public static $sortStart;
    public static $sortStartWork;

    public static $sortHeaderEnd;
    public static $sortEnd;
    public static $sortEndWork;

    public static $packStartHeader;
    public static $packStart;
    public static $packStartWork;

    public static $packHeaderEnd;
    public static $packEnd;
    public static $packEndWork;

    public function __construct()
    {
        self::$header = 'All Batch';
        SELF::$home = 'administrator.pick.index';

        self::$startHeader = 'Pick Start';
        self::$start = 'administrator.pick.start';
        self::$startWork = 'administrator.pickBatch.start-work';

        self::$headerEnd = 'Pick End';
        self::$end = 'administrator.pick.end';
        self::$endWork = 'administrator.pickBatch.end-work';

        self::$sortStartHeader = 'Sort Start';
        self::$sortStart = 'administrator.pick.sortStart';
        self::$sortStartWork = 'administrator.pickBatch.sortStart-work';

        self::$sortHeaderEnd = 'Sort End';
        self::$sortEnd = 'administrator.pick.sortEnd';
        self::$sortEndWork = 'administrator.pickBatch.sortEnd-work';

        self::$packStartHeader = 'Pack Start';
        self::$packStart = 'administrator.pick.packStart';
        self::$packStartWork = 'administrator.pickBatch.pack-start-work';

        self::$packHeaderEnd = 'Pack End';
        self::$packEnd = 'administrator.pick.packEnd';
        self::$packEndWork = 'administrator.pickBatch.packEnd-work';

        // link to view page
        // view('administrator.pick.index');
    }

    public function index()
    {
        $tables = PickBatch::orderBy('created_at', 'ASC');

        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        if (!$user->hasRole('wh_supervisor') && $userLocationId != NULL) $tables = $tables->whereIn('location_id', $userLocationId);

        $tables = $tables->paginate(10);
        return view(self::$home, compact('tables'));
    }

    public function start(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $pickBins = PickBin::where('using', 0)->get();
        $pickBins = $pickBins->whereIn('location_id', $userLocationId);

        return view(self::$start, compact('request', 'pickBins'));
    }

    public function startWork(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $this->validate($request, [
            'selected_bin' => 'required',
        ]);

        $bin_selected = PickBin::where('id', $request->input('selected_bin'))->where('using', 0)->first();
        $data = array(
            'user' => $user,
            'bin_select' => $bin_selected,
        );

        Session::put('pick_data', $data);

        return redirect()->route('administrator.ordertracking.orders-pick')->with('success', 'Sucess - selected ' . $bin_selected->bin_number . '.');
    }

    public function end()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $pickBins = $this->getBin(1, $userLocationId);

        return view(self::$end, compact('user', 'pickBins'));
    }

    public function endWork(Request $request)
    {
        $this->validate($request, [
            'selected_bin' => 'required',
        ]);

        $batch = $this->updateBinPickBatchDetailOrders($request, $request->input('selected_bin'), 1, 2, 'pick');
        if ($batch == 'error') return back()->with('error', 'Please refresh this page');

        return back()->with('success', 'Completed pick for Batch Number :' . $batch->batch_number . ' ');
    }

    public function sortStart(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        // dd($userLocationId);
        $pickBins = $this->getBin(2, $userLocationId);

        return view(self::$sortStart, compact('user', 'pickBins'));
    }

    public function sortStartWork(Request $request)
    {
        $this->validate($request, [
            'selected_bin' => 'required',
        ]);

        $batch = $this->updateBinPickBatchDetailOrders($request, $request->input('selected_bin'), 2, 3, 'sort');
        if ($batch == 'error') return back()->with('error', 'Please refresh this page');

        $batchDetail = PickBatchDetail::where('key', $batch->id)->get('delivery_order');

        $orders = Order::whereIn('delivery_order', $batchDetail)->get();

        //update order's status and shipping date, then combine DO
        $files = $not_combine = array();
        $domain = $request->server->get('DOCUMENT_ROOT');

        foreach ($orders as $key => $order) {

            //if delivery by company will create poslaju shipment
            if ($order->delivery_method != 'Self-pickup' || $order->order_status == 1001) { // 1001 = order placed
                CourierController::createShipment($order->id);
            }

            $invoice_number = $order->purchase->getFormattedNumber('purchase_number');
            $DO_number = $order->delivery_order;

            $array = $this->combinedDO($orders, $domain, $files, $not_combine);
        }

        $this->generateCombinedDO($array['files'], $array['pdf'], $array['fname']);

        dd('not generated, please inform admin', $array['error_message']);
    }

    public function combinedDO($orders, $domain, $files, $not_combine)
    {
        $array = array();
        $error_message = NULL;
        foreach ($orders as $key => $order) {

            //if delivery by company will create poslaju shipment
            // if ($order->delivery_method != 'Self-pickup' || $order->order_status == 1001) { // 1001 = order placed
            //     CourierController::createShipment($order->id);
            // }

            $invoice_number = $order->purchase->getFormattedNumber('purchase_number');
            $DO_number = $order->delivery_order;

            if (file_exists($domain . '/storage/documents/invoice/' . $invoice_number . (($order->purchase->invoice_version != 0) ? '/v' . $order->purchase->invoice_version . '/' : '/') . 'delivery-orders/' . $DO_number . '.pdf')) {

                //file path
                $files[] = $domain . '/storage/documents/invoice/' . $invoice_number . (($order->purchase->invoice_version != 0) ? '/v' . $order->purchase->invoice_version . '/' : '/') . 'delivery-orders/' . $DO_number . '.pdf';
                //file name
                $fname[] = $DO_number;
                if ($order->delivery_method != 'Self-pickup' || $order->order_status == 1001) { // 1001 = order placed
                    $dt = Carbon::now();
                    $time = $dt->toTimeString(); //14:15:16
                    $day = $dt->addDays(1);

                    if ($time > '11:45:00') {
                        $order->est_shipping_date = $day->isoFormat('YYYY-MM-DD');
                        $order->shipping_date = $day->isoFormat('YYYY-MM-DD');
                    } else {
                        $order->est_shipping_date = $dt->isoFormat('YYYY-MM-DD');
                        $order->shipping_date = $dt->isoFormat('YYYY-MM-DD');
                    }

                    $order->courier_name = 'POSLAJU';
                } else {

                    $order->est_shipping_date = Carbon::now()->isoFormat('YYYY-MM-DD');
                    $order->shipping_date = Carbon::now()->isoFormat('YYYY-MM-DD');
                }

                $order->save();
            } else {
                $not_combine[] = $order->delivery_order;
            }

            if (count($not_combine) > 0) {
                $error_message = "These DO are not available - " . implode(', ', $not_combine);
                // return back()->with(['error_message' => $error_message]);
            }

            // initiate FPDI
            if ($order->delivery_method != 'Self-pickup' || $order->order_status == 1001) { // 1001 = order placed
                $pdf = new FPDI('l');
            } else {
                $pdf = new FPDI('p', 'mm', 'A5');
            }
        }
        $array = array(
            'files' => $files,
            'pdf' => $pdf,
            'fname' => $fname,
            'error_message' => $error_message,
        );

        return $array;
    }

    public function generateCombinedDO($files, $pdf, $fname)
    {
        $pageCount = 0;

        // iterate over array of files and merge
        foreach ($files as $file) {
            //if no file, dont combine
            if (!empty($file)) {
                $pageCount = $pdf->setSourceFile($file);
                for ($i = 0; $i < $pageCount; $i++) {
                    $tpl = $pdf->importPage($i + 1, '/MediaBox');
                    $pdf->addPage('P', 'A5');
                    $pdf->useTemplate($tpl);
                }
            }
        }

        // here will output pdf
        if (!empty($files)) {
            $filename = 'combined' . implode(",", $fname) . '.pdf';

            $pdf->Output($filename, 'I');
        }
    }

    public function sortEnd()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $pickBins = $this->getBin(3, $userLocationId);

        return view(self::$sortEnd, compact('user', 'pickBins'));
    }

    public function packStart()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $pickBins = $this->getBin(4, $userLocationId);

        return view(self::$packStart, compact('user', 'pickBins'));
    }

    public function packStartWork(Request $request)
    {
        $this->validate($request, [
            'selected_bin' => 'required',
        ]);

        $batch = $this->updateBinPickBatchDetailOrders($request, $request->input('selected_bin'), 4, 5, 'pack');
        if ($batch == 'error') return back()->with('error', 'Please refresh this page');

        return redirect()->route('administrator.pickBatch.pack-start')->with('success', 'Success, Start pack for ' . $batch->batch_number . '.');
    }

    public function packEnd()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $pickBins = $this->getBin(5, $userLocationId);

        return view(self::$packEnd, compact('user', 'pickBins'));
    }

    public function packEndWork(Request $request)
    {
        $this->validate($request, [
            'selected_bin' => 'required',
        ]);

        $batch = $this->updateBinPickBatchDetailOrders($request, $request->input('selected_bin'), 5, 6, 'pack', true);
        if ($batch == 'error') return back()->with('error', 'Please refresh this page');

        return back()->with('success', 'Completed Pack for Batch Number :' . $batch->batch_number . ' ');
    }

    private function getBin($pick_pack_status, $userLocationId)
    {
        return PickBin::selectRaw('pick_bin.*')
            ->join('wh_picking', 'wh_picking.bin_id', '=', 'pick_bin.id')
            ->whereIn('pick_bin.location_id', $userLocationId)
            ->where('pick_bin.using', 1)
            ->where('wh_picking.pick_pack_status', $pick_pack_status)
            ->get();
    }

    public static function updateBinPickBatchDetailOrders(Request $request, $bin_number, $getBatchPickPackStatus, $update_pick_pack_status, $process, $packEndProccess = false)
    {

        $user = User::find(Auth::user()->id);

        $checkingCorrectPage = PickBatch::where('bin_id', $bin_number)
            ->where('pick_pack_status', $getBatchPickPackStatus)
            ->first();

        if ($checkingCorrectPage == NULL) return 'error';

        //update bin using
        $pickBin = PickBin::where('id', $bin_number)->first();

        switch ($process) {
            case ('pick'):
                $pickBin->picker = $user->id;
                break;
            case ('sort'):
                $pickBin->sorter = $user->id;
                break;
            case ('pack'):
                $pickBin->packer = $user->id;
                break;
        }

        if ($packEndProccess) {
            $pickBin->using = 0;
            $pickBin->picker = 0;
            $pickBin->sorter = 0;
            $pickBin->packer = 0;
        } else {
            $pickBin->using = 1;
        }

        $pickBin->save();

        //update batch
        $batch = PickBatch::where('bin_id', $bin_number)
            ->where('pick_pack_status', $getBatchPickPackStatus)
            ->first();

        $batch->pick_pack_status = $update_pick_pack_status;
        switch ($process) {
            case ('pick'):
                $batch->picker = $user->id;
                break;
            case ('sort'):
                $batch->sorter = $user->id;
                break;
            case ('pack'):
                $batch->packer = $user->id;
                break;
        }

        $batch->save();

        //update orders
        $batchDetail = PickBatchDetail::where('key', $batch->id)->get('delivery_order');

        $orders = Order::whereIn('delivery_order', $batchDetail)->get();

        foreach ($orders as $orderKey => $order) {

            $order->pick_pack_status = $update_pick_pack_status;

            if ($packEndProccess) {
                if ($order->delivery_method == 'Self-pickup') {
                    $order->order_status = 1013;
                } else {
                    $order->order_status = 1002;
                }

                $fromProducts = $toProduct = array();
                foreach ($order->items as $key => $item) {

                    if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2) {
                        foreach ($item->bundleChild as $key => $bundle) {
                            $fromProducts[$bundle->primary_product_code] = ($bundle->primary_quantity * $item->quantity);
                        }
                        $toProduct['product_code'] = $item->product_code;
                        $toProduct['type'] = 'bundle';
                        $toProduct['quantity'] = $item->quantity;
                        $toProduct['remark'] = 'auto' . $item->delivery_order;

                        $batchDetailsss = PickBatchDetail::where('delivery_order', $item->delivery_order)->first();
                    }
                }
                self::minusWarehouseInventory($order->delivery_order);
            }

            $order->save();

            if (isset($fromProducts) && $toProduct && isset($batchDetailsss)) {
                foreach ($fromProducts as $code => $vv) {
                    $bundleProductCodeOnly[] = $code;
                }
                $batchItemss = WarehouseBatchItem::selectRaw('wh_batch_item.*')
                    ->join('wh_batch_detail', 'wh_batch_detail.id', '=', 'wh_batch_item.detail_id')
                    ->where('wh_batch_item.type', 'pick')
                    ->where('wh_batch_item.picking_order_id', $batchDetailsss->id)
                    ->whereIn('wh_batch_detail.product_code', $bundleProductCodeOnly)
                    ->get();

                $warehouseOrders = WarehouseOrder::whereIn('delivery_order', $batchDetail)->first();

                $warehouseBunbundle = new WarehouseBunbundle;
                $warehouseBunbundle->user_id = $user->id;
                $warehouseBunbundle->location_id = $warehouseOrders->location_id;
                $warehouseBunbundle->type = $toProduct['type'];
                $warehouseBunbundle->from_product_code = '';
                $warehouseBunbundle->product_code = $toProduct['product_code'];
                $warehouseBunbundle->quantity = $toProduct['quantity'];
                $warehouseBunbundle->expiry_date = '';
                $warehouseBunbundle->panel_id = 2011000000;
                $warehouseBunbundle->bundle_type = 1;
                $warehouseBunbundle->remark = $toProduct['remark'] . json_encode($fromProducts);

                $warehouseBunbundle->save();

                foreach ($batchItemss as $key => $bItem) {
                    $transferItem = new WarehouseBunbundleItem;

                    $transferItem->b_unbundle_key = $warehouseBunbundle->id;
                    $transferItem->batch_item_id = $bItem->id;

                    $transferItem->save();
                }


                //auto create bundle/batch/mark batch item
                $request->stockTransferId = $warehouseBunbundle->id;
                $request->picking_order_id = $batchDetailsss->id;
                $request->process = 'pick';
                $request->imAuto = 'yes';

                BunbundleController::end($request);
            }
        }

        return $batch;
    }

    public static function minusWarehouseInventory($delivery_order)
    {
        $model_id = WarehouseOrder::where('delivery_order', $delivery_order)->first();

        $model_id->inv_type = 8000;
        $model_id->save();

        $whInventories = WarehouseInventories::where('model_type', 'App\Models\Warehouse\Order\WarehouseOrder')->where('model_id', $model_id->id)->get();

        foreach ($whInventories as $key => $whInventory) {
            //suppose not stock product no inventory
            if ($whInventory->inv_type != 8015 && $whInventory->productAttributes->product2->parentProduct->no_stockProduct != 1) {
                $whInventory->inv_type = 8000;
                $whInventory->save();
            }
        }
    }

    //pick select process here (show ready to pick orders)
    public function ordersPick(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        //check session
        if (Session::get('pick_data') == NULL)  return redirect(route('administrator.pickBatch.start-redirect'))->with('error', 'Please select bin number!');

        $customerOrder = $customerOrderNoStock = $orderBeforeCheckPickPackStatus = $orderNoEnoughStock = array();
        $orderQty = $warehouseQty = $wareHouseStock = $customerOrdera = $customerOrderaNotStock = array();

        //query all search selection
        $allOrders = $this->allOrdersPick($request, $userLocationId);
 	
        // dd($allOrders);
        //get warehouse inventory
        $inventory = ViewInventoriesStock::whereIn('outlet_id', $userLocationId)->whereNotIn('physical_stock', [0])->get();
//	if($user->id == 2) dd($inventory->pluck('physical_stock','product_code'));
        $statuses = Status::whereIn('id', ['1001', '1010', '1012'])->get(); // remove status record created
        // $states = State::all();
        // $couriers = Courier::all();

        foreach ($inventory as $product) {
            $wareHouseStock[$product->product_code] = ($product->physical_stock); //+ $product->pos_completed
        }
	foreach ($allOrders as $orders){

            $can = 1;
	     if (!($orders->pick_pack_status == 0 && ($orders->order_status == 1001 || $orders->order_status == 1010 || $orders->order_status == 1012))) continue;
	
            if ($orders->order_type == 'rbs' && $orders->order_date != 'Pending' && Carbon::createFromFormat('d/m/Y', $orders->order_date)->lt(Carbon::now()) || ($orders->order_type == 'normal')) {

                foreach ($orders->items as $item) {

                    //no checking if refunded(1011) product and no_stockProduct
                    if ($item->item_order_status !=  1011 && $item->product->parentProduct->no_stockProduct != 1) {
                        if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {

                            foreach ($item->attributes->productBundle as $bundleItem) {
                                $productCode =  $bundleItem->primary_product_code;
//if($user->id == 2 && $productCode =='FMLIMX0001') dd( $orderQty, $orders );
                                if (!isset($warehouseQty[$productCode])) {
                                    $warehouseQty[$productCode]['total'] = (isset($wareHouseStock[$productCode]) ? $wareHouseStock[$productCode] : NULL);
                                }
                                if (!isset($orderQty[$productCode])) {
                                    $orderQty[$productCode]['total'] = 0;
                                }

                                $orderQty[$productCode]['total'] += $item->quantity;

                                if ($warehouseQty[$productCode]['total'] < $orderQty[$productCode]['total']) {
                                    $can = 0;
                                }
                            }
                        } else {
                            $productCode = $item->product_code;
			   // if($user->id == 2 && $productCode =='FMLIMX0001') dd( $orderQty, $orders );
                            if (!isset($warehouseQty[$productCode])) {
                                $warehouseQty[$productCode]['total'] = (isset($wareHouseStock[$productCode]) ? $wareHouseStock[$productCode] : NULL);
                            }
                            if (!isset($orderQty[$productCode])) {
                                $orderQty[$productCode]['total'] = 0;
                            }

                            $orderQty[$productCode]['total'] += $item->quantity;

                            if ($warehouseQty[$productCode]['total'] < $orderQty[$productCode]['total']) {
                                $can = 0;
			    }
			   
			}
                    }
                }

                if ($can == 1) {
                    $orderBeforeCheckPickPackStatus[] = $orders;
                } else {
                    foreach ($orders->items as $key => $item2) {
                        if ($item2->item_order_status !=  1011 && $item->product->parentProduct->no_stockProduct != 1) {
                            if (isset($item2->attributes->productBundle) && ($item2->attributes->productBundle->first() != NULL) && $item2->attributes->productBundle->first()->active == 2) {
                                foreach ($item2->attributes->productBundle as $bundleItem) {
                                    $productCode =  $bundleItem->primary_product_code;
                                    if (isset($orderQty[$productCode]['total'])) {
                                        $orderQty[$productCode]['total'] -= $item2->quantity;
                                    }
                                }
                            } else {
                                if (isset($orderQty[$item2->product_code]['total'])) {
                                    $productCode = $item2->product_code;

                                    $orderQty[$productCode]['total'] -= $item2->quantity;
                                }
                            }
                        }
                    }

                    $orderNoEnoughStock[] = $orders;
                    unset($can);
                }
            }
        }
        //if ($userLocationId[0] == 4)  dd($orderBeforeCheckPickPackStatus);

        //got stock
        foreach ($orderBeforeCheckPickPackStatus as $key => $order) {
            if ($order->pick_pack_status == 0 && ($order->order_status == 1001 || $order->order_status == 1010 || $order->order_status == 1012))
                $customerOrdera[] = $order;
        }
        $countTotal = 0;
        foreach ($customerOrdera as $order) {
            if ($order->order_status == 1001 || $order->order_status == 1012) {
                $customerOrder['1'][] = $order;
            } else {
                if ($order->delivery_outlet == 2) {
                    $customerOrder['10'][] = $order;
                } else {
                    $customerOrder['11'][] = $order;
                }
            }
            $countTotal++;
        }
        ksort($customerOrder);

        $customerOrders = $this->paginate($customerOrder, 30);
        //end got stock

        //not stock
        // foreach ($orderNoEnoughStock as $key => $noStockOrder) {
        //     if ($noStockOrder->pick_pack_status == 0 && ($noStockOrder->order_status == 1001 || $noStockOrder->order_status == 1010 || $noStockOrder->order_status == 1012))
        //         $customerOrderaNotStock[] = $noStockOrder;
        // }
        // $countTotalNoStock = 0;
        // foreach ($customerOrderaNotStock as $key => $orderNo) {
        //     if ($orderNo->order_status == 1001 || $orderNo->order_status == 1012) {
        //         $customerOrderNoStock['1'][] = $orderNo;
        //     } else {
        //         $customerOrderNoStock['10'][] = $orderNo;
        //     }
        //     $countTotalNoStock++;
        // }
        // ksort($customerOrderNoStock);

        // $customerOrderNoStocks = $this->paginate($customerOrderNoStock, 30);
        //end no stock

        return view('administrator.ordertracking.orders-pick')
            ->with('user', $user)
            ->with('statuses', $statuses)
            ->with('customerOrders', $customerOrders)
            // ->with('customerOrderNoStocks', $customerOrderNoStocks)
            // ->with('countTotalNoStock', $countTotalNoStock)
            ->with('countTotal', $countTotal);
    }

    public function allOrdersPick($request, $userLocationId)
    {
        return Order::selectRaw('orders.*,orders.created_at, orders.order_number,orders.order_status,orders.pick_pack_status,orders.id,
            global_products.no_stockProduct,
            wh_order.location_id,wh_order.date
            ')
            ->join('wh_order', 'wh_order.delivery_order', '=', 'orders.delivery_order')
            ->join('items', 'orders.order_number', '=', 'items.order_number')
            ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            // ->statusFilter($request->status)
            // ->statesFilter($request->states)
            // ->datebetweenFilter($request->datefrom, $request->dateto)
            // ->searchFilter($request->searchBox, $request->selectSearch)
            // ->where('orders.delivery_order', 'FWD22-004589')

            ->whereIn('wh_order.location_id', [$userLocationId])
            ->whereIn('orders.order_status', ['1001', '1002', '1003', '1010', '1012'])
            // ->stockFilter('has_stock')
            ->whereIn('orders.pick_pack_status', [0, 1, 2, 3, 4, 5])
      	    ->orderBy('wh_order.date', 'ASC')
            ->orderBy('orders.pick_pack_status', 'DESC')
           
            ->orderBy('orders.order_status', 'DESC')
            ->groupBy('orders.id')
            ->get();
    }

    // Update bulk
    public function bulkUpdate(Request $request)
    {
        $action =  $request->input('bulkaction');
        $id = $request->input('action_ids');
        $arrayid = explode(",", $id);
        $success_message = NULL;

        switch ($action) {
            case 'generate_picking_list';
                $sessionData = Session::get('pick_data');

                //check if no select DO
                if ($arrayid[0] == '') return back()->with(['error_message' =>  "Please select DO number"]);

                //check session (main checking)
                if ($sessionData == NULL)  return redirect(route('administrator.pickBatch.start-redirect'))->with('error_message', 'Please select bin number!');

                //double check bin using or not
                $pickBin = PickBin::where('id', $sessionData['bin_select']['id'])->first();
                if ($pickBin->using == 1) return redirect(route('administrator.pickBatch.start-redirect'))->with('error_message', 'Bin number used!');

                //checking duplicate
                foreach ($arrayid as $key => $order_number) {
                    $pickBatch = PickBatchDetail::where('delivery_order', $order_number)->first();
                    if (isset($pickBatch)) return redirect(route('administrator.pickBatch.start-redirect'))->with('error_message', 'Pick Itemize Generated!');
                }

                //check mix order status
                $mixStatusOrders = Order::whereIn('delivery_order', $arrayid)->groupBy('order_status')->get();
                if (count($mixStatusOrders) > 1) return  redirect(route('administrator.ordertracking.orders-pick'))->with('error_message', 'Mixed order placed and self collect, select only one type only!');

                //all checking passed, then generate picking list
                return $this->generate_picking_list($sessionData, $id, $arrayid);

                break;

            default:
                $error_message = "Please choose at least one action";
        }

        if (!empty($result) && $result == "success") {
            return back()->with(['successful_message' => $success_message]);
        } else {
            return back()->with(['error_message' =>  $error_message]);
        }
    }

    public function generate_picking_list($sessionData, $id, $arrayid)
    {
        //create batch
        $batch = new PickBatch;

        $batch->batch_number = Counters::autoIncrement(['pick'])['pick'];
        $batch->location_id =  $sessionData['user']->user_warehouse_location[0];
        $batch->bin_id = $sessionData['bin_select']['id'];
        $batch->picker = $sessionData['user']['id'];
        $batch->pick_pack_status = 1;

        $batch->save();

        //create batch detail
        foreach ($arrayid as $key => $delivery_order) {
            $batchDetail = new PickBatchDetail;

            $batchDetail->key = $batch->id;
            $batchDetail->delivery_order = $delivery_order;
            $batchDetail->save();

            //update order pick pack status
            $order = Order::where('delivery_order', $delivery_order)->first();
            $order->pick_pack_status = 1;

            $order->save();
        }

        //update bin using
        $pickBin = PickBin::where('id', $sessionData['bin_select']['id'])->first();
        $pickBin->using = 1;
        $pickBin->picker = $sessionData['user']['id'];
        $pickBin->save();

        //create ItemBundle
        $items = Item::whereIn('delivery_order', $arrayid)->get();
        foreach ($items as $key => $item) {
            if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {
                foreach ($item->attributes->productBundle as $key => $bundle) {
                    $itemBunlde = ItemBundle::find($item->id);
                    if ($itemBunlde == NULL) {
                        $itemBundle = new ItemBundle;
                    }

                    $itemBundle->item_id = $item->id;
                    $itemBundle->attribute_id = $bundle->attribute_id;
                    $itemBundle->primary_product_code = $bundle->primary_product_code;
                    $itemBundle->primary_attribute_id = $bundle->primary_attribute_id;
                    $itemBundle->primary_quantity = $bundle->primary_quantity;
                    $itemBundle->active = $bundle->active;

                    $itemBundle->save();
                }
            }
        }


        //create PDF
        $batch = PickBatch::find($batch->id);

        $pdf = PDF::loadView('documents.warehouse.pick',  compact('batch'))->setPaper('a4');
        // Get PDF content.
        $contentTax = $pdf->download()->getOriginalContent();
        // Set path to store PDF file.
        $pdfDestination = public_path('/storage/documents/pick/' . $batch->batch_number . '/');
        // Set PDF file name.
        $pdfNameTax = $batch->id;

        // Check if directory exist or not.
        if (!File::isDirectory($pdfDestination)) {
            // If not exist, create the directory.
            File::makeDirectory($pdfDestination, 0777, true);
        }
        // Place the PDF into directory.
        File::put($pdfDestination . $pdfNameTax . '.pdf', $contentTax);

        $generatedPdf =  $pdf->setPaper('a4')->stream($pdfNameTax . '.pdf');
        //end create PDF

        Session::forget('pick_data');

        return redirect(route('administrator.pickBatch.start-redirect', ['batch_number' => $batch->batch_number, 'batch_id' => $batch->id]))
            ->with(['success' => 'Generated pick list! Please print out the picking list']);
    }

    // pagination array
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    //pick select process here

    //sort end process here
    public function sortEndWork(Request $request)
    {
        $user = User::find(Auth::user()->id);

        // $sortEndReady = PickBatch::where('bin_id', $request->selected_bin)
        //     ->where('picker', $user->id)
        //     ->where('pick_pack_status', 3)
        //     ->first();

        return redirect()->route('administrator.pickBatch.sortEnd-toScanDo', ['again' => 0, 'batch_id' => 0])->with('success', 'Please scan DO now');
    }

    public function sortEndToScanDo(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $pickBatchDetail = NULL;
        if (!empty($request->again)) {

            $pickBatchDetail = PickBatchDetail::where('wh_picking_order.id', $request->batch_id)
                ->first();

            return view('administrator.pick.sortEndToScanDo', compact('user', 'pickBatchDetail'));
        } else {
            return view('administrator.pick.sortEndToScanDo', compact('user', 'pickBatchDetail'));
        }
    }

    public function sortEndshowPickingOrder(Request $request)
    {
        $pickBatchDetail = PickBatchDetail::selectRaw('wh_picking_order.*')
            ->join('wh_picking', 'wh_picking_order.key', '=', 'wh_picking.id')
            ->where('wh_picking_order.id', $request->pickingOrderId)
            ->where('wh_picking.pick_pack_status', 3)
            ->first();

        $data = array(
            'sortingReadyDO' => $pickBatchDetail,
        );

        Session::put('sortingProcess', $data);

        return view('administrator.pick.sortEndToScanProduct', compact('pickBatchDetail'));
    }
    //sort end process here (end)

    public function regenerateCombineSort(Request $request, $batchId)
    {
        $batchDetail = PickBatchDetail::where('key', $batchId)->get('delivery_order');

        $orders = Order::whereIn('delivery_order', $batchDetail)->get();

        //update order's status and shipping date, then combine DO
        $files = $not_combine = array();
        $domain = $request->server->get('DOCUMENT_ROOT');

        foreach ($orders as $key => $order) {

            $array = $this->combinedDO($orders, $domain, $files, $not_combine);
        }

        $this->generateCombinedDO($array['files'], $array['pdf'], $array['fname']);
    }
}
