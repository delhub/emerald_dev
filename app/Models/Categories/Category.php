<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Products\ProductArticle;

class Category extends Model
{
    // Set table
    protected $table = 'categories';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'name', 
        'slug',
        'type',
        'parent_category_id'
    ];

    protected $casts = [
        'roles' => 'array',
    ];
    
    /**
     * Get the category's image.
     */
    public function image()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable');
    }

    /**
     * Get top level category.
     */
    public function scopeTopLevelCategory($query)
    {
        return $query->where('parent_category_id', 0)->where('type', 'shop')->get();
    }

    /**
     * Get the category's parent if any.
     */
    public function parentCategory()
    {
        return $this->belongsTo('App\Models\Categories\Category', 'parent_category_id');
    }

    public function getLabelAttribute()
    {
        $currentCategory = $this;
        $label = $currentCategory->name;
        while (isset($currentCategory->parentCategory)){
            $currentCategory = $currentCategory->parentCategory;

            $label = $currentCategory->name . ' > '. $label;
       }
        return $label;
    }

    /**
     * Get all of the category's child if any.
     */
    public function childCategories()
    {
        return $this->hasMany('App\Models\Categories\Category', 'parent_category_id');
    }

    public function getArticle()
    {
        return $this->hasOne('App\Models\Products\ProductArticle', 'cat_id', 'id');
    }

    public function getArticles($id)
    {
        $article = ProductArticle::where('cat_id',$id)->first();

        if($article) {
            return $article;
        }
    }
    

    public function getChildArticle($mainId)
    {
        $child = Category::where('parent_category_id',$mainId)->get('id');

        $collection = collect($child);
        $plucked = $collection->pluck('id');

        $article = ProductArticle::whereIn('cat_id',$plucked->all())->get();

        return $article;

        // return $this->hasMany('App\Models\Products\ProductArticle', 'cat_id', 'id');
    }

    /**
     * Get all products belonging to a category.
     */
    public function products()
    {
        return $this->belongsToMany(
            'App\Models\Globals\Products\Product',
            'piv_category_product',
            'category_id',
            'product_id'
        );
    }

    public function panelProducts()
    {
        return $this->belongsToMany(
            'App\Models\Products\Product',
            'piv_category_product',
            'category_id',
            'product_id'
        );
    }

    public function categoryId()
    {
        return $this->hasMany('App\Models\Categories\Category', 'active');
    }

    public function categoryData()
    {
        return $this->hasMany('App\Models\Categories\CategoryData', 'category_id');
    }

    //Get category URL attribute
    public function getUrlAttribute()
    {
        return "/shop/category/{$this->slug}";
    }

    public function scopetopFilter($query, $topLevelSlug)
    {
        if ($topLevelSlug != null) {
            $query = $query->where('slug', $topLevelSlug);
        }
        return $query;
    }

    public function scopesubFilter($query,$topLevelSlug)
    {
        if ($subLevel != null) {
            // $subLevelID = $query->where('slug',$subLevel)->first();
            $query = $query->where('slug',$subLevel);
        } else {
            $query = $query->where('slug',$topLevelSlug);
        }
        return $query;
    }

    public function scopeQualityFilter($query, $quality)
    {
        if ($quality != null) {
            switch ($quality) {
                case 'standard':
                    $qualityID = 1;
                    break;
                case 'moderate':
                    $qualityID = 2;
                    break;
                case 'premium':
                    $qualityID = 3;
                    break;
            }
            $query = $query
                ->join('piv_category_product', 'piv_category_product.category_id', '=', 'categories.id')
                ->join('global_products', 'global_products.id', '=', 'piv_category_product.product_id')
                ->select('global_products.*')
                ->where('global_products.quality_id',$qualityID);
        }

        return $query;
    }

}
