<?php

namespace App\Models\Warehouse\Location;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;
use App\Models\Globals\State;
use App\Models\Globals\City;

class Wh_location extends Model
{
    protected $table = 'wh_location';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];


    public function city()
    {
        return $this->hasOne('App\Models\Globals\City', 'city_key', 'city_key');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\Globals\State', 'state_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Globals\Countries', 'country_id');
    }

    // public function images()
    // {
    //     return $this->morphMany('App\Models\Globals\Image', 'imageable');
    // }

    // /**
    //  * Get default image of a product.
    //  */
    // public function defaultImage()
    // {
    //     return $this->morphOne('App\Models\Globals\Image', 'imageable')
    //         ->where('default', 1);
    // }

    /**
     * Get largest customer account id.
     */
    public function scopeWarehouseKeyFormat($query, $countryId, $city_key)
    {
        $stateAbbreviation = City::where('city_key', $city_key)->first();

        $middle = strtoupper(str_replace('_', '', $stateAbbreviation->city_key));

        $exist = $this->whereNotIn('id', [999])->get();
        $count = count($exist);

        $result = $countryId . $middle  . str_pad($count + 1, 3, "0", STR_PAD_LEFT);

        return $result;
    }
}
