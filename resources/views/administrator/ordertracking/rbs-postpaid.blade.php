@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">RBS Postpaid</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-bordered text-center table-striped " id="sales-tracking" style="width:100%">
                    <thead style="background-color:	#E8E8E8;">
                        <tr>
                            {{-- <th scope="col"><input type="checkbox" id="checkAll"/>Action</th> --}}
                            <th scope="col">User ID</th>
                            <th scope="col">Product Code</th>
                            <th scope="col">Start Reminder Date</th>
                            <th scope="col">Latest Reminder Date</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($rbsPostpaids as $rbsPostpaid)
                        <tr>
                            <td>{{ $rbsPostpaid->user ? $rbsPostpaid->user->userInfo->account_id : null }}</td>
                            <td>
                                <strong>{{ $rbsPostpaid->atrributeProduct->product_code }}</strong>
                                <br>
                                {{ $rbsPostpaid->atrributeProduct->product2->parentProduct->name }}
                                <br>
                                {{ '('. $rbsPostpaid->atrributeProduct->attribute_name . ')' }}
                                <br>
                                Quantity : {{ $rbsPostpaid->rbs_quantity }}
                            </td>
                            <td>
                                {{ $rbsPostpaid->start_reminder }}
                            </td>
                            <td>
                                {{ $rbsPostpaid->last_reminder }}
                            </td>
                            <td>
                                {{ $rbsPostpaid->active == 1 ? 'Active' : 'Cancelled' }}
                                @if ($rbsPostpaid->active != 1)
                                <br>
                                   on {{ $rbsPostpaid->cancel_date }}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-auto ml-auto py-3">
            {{-- {!! $customerOrders->render() !!} --}}
        </div>

    </div>


</div>

@push('style')
    <style>
        .text-bold {
            font-weight: bold;
        }

        .table .thead-light th {
            font-weight: bold;
            font-family: 'Nunito', sans-serif;
        }

        @media(max-width:767px) {
            .hidden-sm {
                display: none;
            }

        }

        @media(min-width:767px) {
            .hidden-md {
                display: none;
            }
        }


        .text-font-family {
            font-family: 'Nunito', sans-serif;
        }

        /* freeze header  */
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

            table  { border-collapse: collapse; width: 100%; }
            th     { background:#eee; }
    </style>
@endpush

@push('script')
    <script>
        $(function() {
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
                });

                $(".estimatedateonly").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                changeMonth: true,
                changeYear: true
                });

                $(".filterDate").datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                changeMonth: true,
                changeYear: true
            });
        });

        function showButton(itemID) {
            var opt = document.getElementById("courier_name" + itemID).value;
            console.log('opt');
            console.log(opt);
            if(opt == 'POSLAJU'){
                $("#createShipment_" +itemID).show();
            }else{
                $("#createShipment_" +itemID).hide();
            }
        }

        $(document).ready(function () {

            //check all checkbox
            $("#checkAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Get checked id
            $("#bulkactionform").submit(function(){
                var checked = new Array();
                var ids = new Array();

                checked = $('input[name="itemsid[]"]:checked');

                for (i = 0; i < checked.length; i++) {
                    ids.push(checked[i].value);
                    }

                var stringids = ids.toString();
                $("#action_ids").val(stringids);
            });

            //show or hide when selected bulk action
            $("#bulkaction").click(function(){

                if(bulkaction.value == 'combine_do' || bulkaction.value == 'splitPOnDO'){

                document.getElementById("action_date").style.display = "none";
                }else if(bulkaction.value == 'mark_order_date'){

                    document.getElementById("action_date").className = "estimatedateonly form-control";

                    $(".estimatedateonly").datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else if(bulkaction.value == 'mark_actual_ship_date'){

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else{

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }
            });
        });

        function getShipmentData(itemID) {
            console.log(itemID);
            var element = document.getElementById("spinnerSubmit" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/create_shipment/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("createShipment_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Shipment Create";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        alert(result);


                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuCheckout(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/poslaju_checkout/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("checkout_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Checkout";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuShip(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/poslaju_ship/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        // button.disabled = true;
                        // button.innerHTML = "Checkout";
                        // $("#actualSubmit").submit();
                    } else {
                        // element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function exportTasks(_this) {
            let _url = $(_this).data('href');
            window.location.href = _url;
        }

    </script>
@endpush

@endsection
