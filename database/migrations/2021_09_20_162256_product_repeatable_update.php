<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Products\ProductAttribute;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Products\WarehouseInventories;

class ProductRepeatableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $attributes = ProductAttribute::where('repeatable', 1)->get();

        foreach ($attributes as $key => $attribute) {
            $productCode[] = $attribute->product_code;
        }

        $batchItemDetail = WarehouseBatchItem::selectRaw('wh_batch_item.*')
            ->join('wh_batch_detail', 'wh_batch_detail.batch_id', '=', 'wh_batch_item.batch_id')
            ->whereIn('wh_batch_detail.product_code', $productCode)
            ->get();

        foreach ($batchItemDetail as $key => $item) {
            $item->repeatable = 1;
            $item->save();
        }

        $inventories = WarehouseInventories::where('model_type', 'App\Models\Warehouse\Order\WarehouseOrder')
            ->whereIn('product_code', $productCode)
            ->get();

        foreach ($inventories as $key => $inventory) {
            $inventory->repeatable = 1;
            $inventory->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
