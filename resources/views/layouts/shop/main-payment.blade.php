<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- JQUERY for datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet'>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}?v=2.4" rel="stylesheet">
    <!-- Custom Scrollbar CDN -->


    <!-- TODO: Import using mix. -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />





    @stack('style')
    <!-- <style>
        .search-button-updated {
            height: 48px;
            background-color: white;
        }

        @media (max-width: 767px) {
            .search-button-updated {
                height: 37px;
                background-color: white;
            }
        }
    </style> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="{{ asset('js/app.js') }}"></script>
    <!--Validate register fields -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

</head>

<body class="promo-page-background-color">

    <!-- Navigation bar -->
    {{-- @include('layouts.management.navigations.navigation-bar-customer') --}}

    <!-- Side bar -->
    {{-- @include('layouts.shop.navigation.sidebar') --}}

    <main class="mt-3 no-margin" id="body-content-collapse-sidebar">
        <div id="app" class="">
            @yield('content')
        </div>
    </main>

    <!-- Footer -->
    @include('layouts.shop.footer.footer')

    <!-- Custom Scrollbar CDN -->
    <!-- TODO: Import using mix. -->

    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js">
    </script>


    <!-- Scripts -->

    <script type="text/javascript">
        $(document).ready(function() {

            // $("#sidebar").mCustomScrollbar({
            //     theme: "minimal"
            // });

            $('#dismiss, .overlay').on('click', function() {
                $('#sidebar').removeClass('active');
                $('.navbg-protect').addClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').addClass('active').css("overflow", "scroll");
                $('.navbg-protect').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });


            $('#body-content-collapse-sidebar,#footer-content-collapse-sidebar,.nav-content-sidebar-collapse').click(function(event) {
                if ($(event.target).attr('id') !== "sidebar" && $(event.target).attr('id') !== "sidebarCollapse") {
                    $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');
                }
            });

            // date for preorder
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 90,
            });
            //
        });
    </script>

    <!-------Show  perfect list and Add card quantity-------->
    <script type="text/javascript">
        // function onPageLoadGetQuantity() {
        //     let getQuantityURL = "{{-- route('web.shop.cart.quantity', ['id' => $user->id ]) --}}";
        //     const cartQuantityElement = $('#cart-quantity');
        //     const cartQuantityElementMobile = $('#cart-quantity-mobile');

        //     $.ajax({
        //         async: true,
        //         beforeSend: function() {
        //             console.log('Request starting.');
        //         },
        //         complete: function() {
        //             console.log('Request complete.');
        //         },
        //         url: getQuantityURL,
        //         type: 'GET',
        //         success: function(response) {
        //             cartQuantityElement.text(response.data);
        //             cartQuantityElementMobile.text(response.data);
        //         },
        //         error: function(response) {
        //             console.log('Error');
        //             console.log(response);
        //         }
        //     });
        // }

        // function onPageLoadGetPerfectListQuantity() {
        //     let getQuantityURL = "{{-- route('shop.perfect-list.quantity', ['id' => $user->id ]) --}}";
        //     const perfectListQuantityElement = $('#wishlist-quantity');

        //     $.ajax({
        //         async: true,
        //         beforeSend: function() {
        //             console.log('Request starting.');
        //         },
        //         complete: function() {
        //             console.log('Request complete.');
        //         },
        //         url: getQuantityURL,
        //         type: 'GET',
        //         success: function(response) {
        //             perfectListQuantityElement.text(response.data);
        //         },
        //         error: function(response) {
        //             console.log('Error');
        //             console.log(response);
        //         }
        //     });
        // }

        // $(document).ready(function() {
        //     onPageLoadGetQuantity();
        //     onPageLoadGetPerfectListQuantity();
        // });

        // $(document).ready(function() {
        //     $("a").tooltip();
        // });
    </script>

    @stack('script')
</body>

</html>
