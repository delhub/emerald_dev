<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_location', function (Blueprint $table) {
            $table->id();
            $table->string('location_key')->unique();
            $table->string('location_name');
            $table->string('type');
            $table->string('country_id', 2);
            $table->text('printer_email')->nullable();
            $table->string('contact_number')->nullable();
            $table->text('default_address')->nullable();
            $table->string('postcode', 50)->nullable();
            $table->string('city_key');
            $table->integer('state_id');
            $table->string('operation_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_location');
    }
}
