<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CommandHandle;
use Illuminate\Support\Str;
use App\Models\Users\User;
use App\Http\Controllers\Register\CustomerRegisterController;

class CreateUser extends Command
{
    use CommandHandle;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {function?} {--full_name=Name} {--referrer_id=1010} {--nric=1234} {--email=user@email.com} {--password=account123}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create User';

    protected $functions = array(
        'customer',
        'customer_agent'
    );

    public function customer()
    {
        // format: value, default, callback
        $fields = array(
            array('full_name', "Name"),
            array('referrer_id', "1010"),
            array('email', "user@email.com", "validate_email"),
            array('nric', "1234"),
            array('password', "account123"),
        );
        $values = array();
        foreach ($fields as $field) {
            $name = $field[0];
            $value = $field[1];
            $callback = isset($field[2]) ? $field[2] : null;
            $values[$name] = $this->validate($name, $value, $callback);
        }

        extract($values);

        $confirm = $this->choice("Create User {$full_name} - {$referrer_id} - {$email} - {$nric} -{$password}", ['YES', 'NO'], 'NO');

        if ($confirm == 'YES') {
            // create user;
            $user = $this->createCustomer($values);
            $account_id = $user->userInfo->account_id;
            $this->info("Customer {$account_id} created.");

        } else {
            $this->warn('Customer not created.');
        }
    }
    public function createCustomer($values){

        $request = request();
        $request->merge($this->prepareCustomerData($values));
        $user = CustomerRegisterController::createCustomer($request, true, country()->country_id);
        $user->fresh();
        $user->notes = null;
        $user->save();
       
        return $user;
    }
    public function prepareCustomerData($content)
    {
        $data = array(
            'city' => null,
            'date_of_birth' => null,
            'passport_no' => null,
            'country' => null,
            'invoice_number' => null,
            'year_joined' => null,
            'product_used' => null,
            'address_1' => null,
            'address_2' => null,
            'address_3' => null,
            'postcode' => null,
            'city_key' => 0,
            'contact_number_home' => null,
            'referrer_id' => null
        );

        $data = array_merge($data, $content);
        return $data;
    }
  
    public function validate_email($value)
    {
        $result = array();
        $user = User::where('email', $value)->first();
        if ($user) {
            $result = false;
            $this->warn('Email arleady exists.');;
        } else {
            $result = true;
        }

        return $result;
    }

}
