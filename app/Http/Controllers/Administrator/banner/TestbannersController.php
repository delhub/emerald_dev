<?php

namespace App\Http\Controllers\Administrator\banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Testbanners\Testbanner;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Globals\Countries;
use DB;


class TestbannersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$testbanners = Testbanner::all();
        //return Testbanner::where('title', 'agent01')->get();
        //$testbanners = DB::select('SELECT * FROM testbanners');
        //$testbanners = Testbanner::orderBy('title','desc')->get();

        $country = country()->country_id;
        $testbanners = Testbanner::orderBy('created_at','desc')->paginate(10);
        // return view('administrator.testbanners.index', compact('testbanners'));
        return view('administrator.testbanners.index')
        ->with('testbanners',$testbanners)
        ->with('country',$country);

        // return view('sidemenus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.testbanners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'banner_type' => 'required',
            'country_id' => 'required',
            'banner_link' => 'required',
            'banner_web' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'banner_mobile' => 'mimes:jpeg,jpg,png,gif|required|max:10000'
        ]);

        $testbanner = new Testbanner;
        $testbanner->title = $request->input('title');
        $testbanner->banner_type = $request->input('banner_type');
        $testbanner->country_id = $request->input('country_id');
        $testbanner->banner_link = $request->input('banner_link');
        $testbanner->banner_show = ($request->input('banner_show') != 1) ? 0 : 1;
        // $testbanner->banner_web = '/storage/uploads/images/banner/' . $fileNameToStore;
        
        // dd($request);
        if($request->file('banner_web')){

            $file = $request->file('banner_web');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $testbanner->title . '-' . $testbanner->banner_type . '.' . $fileExtension;
            $destinationPath =

                public_path('/storage/uploads/banner/');

                if (!File::isDirectory($destinationPath)) {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $fileName);

                $testbanner->banner_web = $fileName;
                
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        if($request->file('banner_mobile')){

            $file = $request->file('banner_mobile');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $testbanner->title . '-mobile-' . $testbanner->banner_type . '.' . $fileExtension;
            $destinationPath =

                public_path('/storage/uploads/banner/');

                if (!File::isDirectory($destinationPath)) {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $fileName);

                $testbanner->banner_mobile = $fileName;
                
        } else {
            $fileNameToStore = 'noimage.jpg';
        }



        $testbanner->save();

        return redirect('/administrator/testbanners/')->with('success', 'submit done.');
    }

    /**
     * Store Image a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // public function storeImage(Request $request, $id)
    // {
    //     $product = Product::findOrFail($id);

    //     $image = $request->file('file');
    //     $imageDestination = public_path('/storage/uploads/images/products/' . $product->id . '/');
    //     $imageName = $image->getClientOriginalName();
    //     $image->move($imageDestination, $imageName);

    //     $product->images()->create([
    //         'path' => 'uploads/images/products/' . $product->id . '/',
    //         'filename' => $imageName,
    //         'default' => 0
    //     ]);

    //     return response()->json(['success' => $imageName]);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testbanner = Testbanner::find($id);
        return view('administrator.testbanners.show')->with('testbanner', $testbanner);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testbanner = Testbanner::find($id);
        return view('administrator.testbanners.edit')->with('testbanner', $testbanner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testbanner = Testbanner::find($id);
        $testbanner->title = $request->input('title');
        $testbanner->banner_type = $request->input('banner_type');
        $testbanner->country_id = $request->input('country_id');
        $testbanner->banner_link = $request->input('banner_link');
        $testbanner->banner_show = ($request->input('banner_show') != 1) ? 0 : 1;

        if($request->file('banner_web')){

            $file = $request->file('banner_web');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $testbanner->title . '-' . $testbanner->banner_type . '.' . $fileExtension;
            $destinationPath =

                public_path('/storage/uploads/banner/');

                if (!File::isDirectory($destinationPath)) {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $fileName);

                $testbanner->banner_web = $fileName;
                
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        if($request->file('banner_mobile')){

            $file = $request->file('banner_mobile');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $testbanner->title . '-mobile-' . $testbanner->banner_type . '.' . $fileExtension;
            $destinationPath =

                public_path('/storage/uploads/banner/');

                if (!File::isDirectory($destinationPath)) {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $file->move($destinationPath, $fileName);

                $testbanner->banner_mobile = $fileName;
                
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $testbanner->save();

        return redirect('/administrator/testbanners/')->with('success', 'Banner Updates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testbanner = Testbanner::find($id);
        $testbanner->delete();
        return redirect('/administrator/testbanners/')->with('success', 'Banner Remove');
    }
}
