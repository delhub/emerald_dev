<?php

namespace App\Http\Controllers\WEB\Shop\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Cookie;
use Session;
use App\Models\Users\User;
use App\Models\Users\Customers\Cart;
use App\Models\Products\ProductAttribute;
use App\Models\Products\ProductBundle;
use App\Models\Globals\Image;
use App\Models\Globals\State;
use App\Models\Globals\City;
use App\Models\ShippingInstallations\Rates;
use App\Models\ShippingInstallations\Locations;
use App\Models\Globals\Countries;
use App\Models\Tax\Tax;


class CartController extends Controller
{
    /**
     *Get user's cart.
     */
    public function index()
    {
        $cartTotals = array();
        $user = User::find(Auth::user()->id);
        $country_code = country()->country_id;

        $zone = self::maybeCreateCookieAddress();
        $zoneId = self::getZone($zone['state_id'], $zone['city_key']);

        $firstChecks = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('country_id', $country_code)
            ->whereNotIn('disabled', [5])
            ->get();

        self::shippingCalculate($zoneId, $firstChecks);

        $carts = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('selected', 1)
            ->where('country_id', $country_code)
            ->whereIn('disabled', [0, 3])
            ->get();

        $cartTotals = self::shippingCalculate($zoneId, $carts);

        //update cartItems/bundleChildItem
        self::updateCartItems($user, $country_code);
        self::updateBundleChild();

        $cartItems = $user->carts->where('status', 2001)->where('country_id', $country_code);

        $response = array();
        $response['cartTotals'] = $cartTotals;
        $response['cartItems'] = $cartItems;
        $response['zoneId'] = $zoneId;
        // $response = new Response(view('shop.cart.partials.shopping-cart-item', compact(['cartTotals', 'cartItems', 'images', 'zoneId'])));
        // // $response->withCookie(cookie('bjs_cartTotal', json_encode(array('cartTotals' => $cartTotals)), 10));
        // Session::put('bjs_cartTotal', json_encode(array('cartTotals' => $cartTotals)));

        return $response;
    }

    public static function shippingCalculate($zoneId, $carts)
    {
        $cartTotals = array(
            'subtotal' => 0,
            'fee' => array(
                'shipping' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
                'installation' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
                'rebate' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
            ),
            'total' => 0,

            'tax_rate' => 0,
            'tax_name' => 0,

            'shipping_old' => 0,
            'total_old' => 0,
        );
        $country_code = country()->country_id;

        $tax = Tax::where('country_id', $country_code)->get();
        $country = Countries::where('country_id', $country_code)->get();

        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        $currency = $country[0]->country_currency;
        $tax_rate = $tax[0]->tax_percentage;
        $tax_name = $tax[0]->tax_name . " $tax_rate%";

        $moreThanHundred = FALSE;

        //cart separate into category
        $shippingByCategories = $installationByCategories = $disabled_cart_ids = array();
        foreach ($carts as $cart) {

            if ((array_key_exists('product_order_selfcollect', $cart->product_information) && $cart->product_information['product_order_selfcollect']) == 0) {

                //Shipping
                if (!empty($cart->product->shipping_category) && $cart->product->shipping_category != 0) {

                    if (!empty($zoneId)) {
                        foreach ($zoneId as $key => $zone_id) {
                            $zone[] = $zone_id->id;
                        }
                        $Srates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $cart->product->shipping_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                    }

                    if (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-shipping-category')) {

                        if (!isset($shippingByCategories[$cart->product->shipping_category])) {
                            $shippingByCategories[$cart->product->shipping_category]['qty'] = 0;
                            $shippingByCategories[$cart->product->shipping_category]['subtotalPrice'] = 0;
                        }

                        $shippingByCategories[$cart->product->shipping_category]['qty'] += $cart->quantity;
                        $shippingByCategories[$cart->product->shipping_category]['subtotalPrice']  += $cart->subtotal_price;
                        $shippingByCategories[$cart->product->shipping_category]['item'] =  $cart;
                        $shippingByCategories[$cart->product->shipping_category]['items'][] =  $cart;
                    } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-line-quantity')) {

                        if (!isset($shippingByCategories[$cart->product->shipping_category][$cart->product_id])) {
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] = 0;
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] += $cart->quantity;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['item'] =  $cart;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['items'][] =  $cart;
                    } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-category-quantity')) {

                        if (!isset($shippingByCategories[$cart->product->shipping_category][$cart->product_id])) {
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] = 0;
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] += $cart->quantity;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['item'] =  $cart;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['items'][] =  $cart;

                        if (!isset($checkLightSubTotal['light'])) $checkLightSubTotal['light']['subtotalPrice'] = 0;

                        $checkLightSubTotal['light']['subtotalPrice']  += $cart->subtotal_price;
                    }
                }

                //Installation
                if ((array_key_exists('product_installation', $cart->product_information) && $cart->product_information['product_installation']) != 0) {

                    if (!empty($zoneId)) {
                        foreach ($zoneId as $key => $zone_id) {
                            $zone[] = $zone_id->id;
                        }
                        $iRates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $cart->product_information['product_installation'])->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                    }

                    if (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-shipping-category')) {

                        if (!isset($installationByCategories[$cart->product_information['product_installation']])) {
                            $installationByCategories[$cart->product_information['product_installation']]['qty'] = 0;
                            $installationByCategories[$cart->product_information['product_installation']]['subtotalPrice'] = 0;
                        }

                        $installationByCategories[$cart->product_information['product_installation']]['qty'] += $cart->quantity;
                        $installationByCategories[$cart->product_information['product_installation']]['subtotalPrice']  += $cart->subtotal_price;
                        $installationByCategories[$cart->product_information['product_installation']]['item'] =  $cart;
                        $installationByCategories[$cart->product_information['product_installation']]['items'][] =  $cart;
                    } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-line-quantity')) {

                        if (!isset($installationByCategories[$cart->product_information['product_installation']][$cart->product_id])) {
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] = 0;
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] += $cart->quantity;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['item'] =  $cart;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['items'][] =  $cart;
                    } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-category-quantity')) {

                        if (!isset($installationByCategories[$cart->product_information['product_installation']][$cart->product_id])) {
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] = 0;
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] += $cart->quantity;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['item'] =  $cart;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['items'][] =  $cart;

                        if (!isset($checkLightSubTotal['light'])) $checkLightSubTotal['light']['subtotalPrice'] = 0;

                        $checkLightSubTotal['light']['subtotalPrice']  += $cart->subtotal_price;
                    }
                }
            }

            $cartTotals['shipping_old'] += $cart->delivery_fee;
            $cartTotals['subtotal'] += $cart->subtotal_price;

            if (array_key_exists('product_order_tradein', $cart->product_information) && $cart->product_information['product_order_tradein'] != 0) {
                $cartTotals['fee']['rebate']['amount'] += ($cart->product_information['product_order_tradein'] * $cart->quantity);
                $cartTotals['fee']['rebate']['shipping_information'][] += ($cart->product_information['product_order_tradein'] * $cart->quantity);
            }

            //get tax and country currency & rate
            $cartTotals['currency'] = $currency;
            $cartTotals['tax_rate'] = $tax_rate;
            $cartTotals['tax_name'] = $tax_name;

            $cartTotals['unit_price'] = $cart->unit_price * $conversion_rate_markup;
            $cartTotals['subtotal_price'] = $cart->subtotal_price * $conversion_rate_markup;
            //end get tax and country currency & rate

        }

        if (isset($checkLightSubTotal['light'])) {
            if ($checkLightSubTotal['light']['subtotalPrice'] > '10000') {
                $moreThanHundred = True;
            }
        }

        // get array, loop for calculate fee
        foreach ($shippingByCategories as $shipping_category => $shipping_carts) {

            if (!empty($zoneId)) {
                foreach ($zoneId as $key => $zone_id) {
                    $zone[] = $zone_id->id;
                }
                $Srates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $shipping_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
            }

            if (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-shipping-category')) {
                $calculatedFee = self::calculateFee($shipping_carts['item'], $shipping_carts['qty'], $shipping_carts['subtotalPrice'], $zoneId, $shipping_category, $shipping_carts['items'], 'shipping');

                $cartTotals['fee']['shipping']['amount'] +=   $calculatedFee['fee'];
                $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];

                foreach ($shipping_carts['items'] as $groupIitem) {
                    if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                        $disabled_cart_ids[] = $groupIitem->id;
                    }
                }
            } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-line-quantity')) {

                foreach ($shipping_carts as $shipping_cart) {
                    $calculatedFee = self::calculateFee($shipping_cart['item'], $shipping_cart['qty'], $shipping_cart['subtotalPrice'], $zoneId, $shipping_category, $shipping_cart['items'], 'shipping');

                    $cartTotals['fee']['shipping']['amount'] +=   $calculatedFee['fee'];
                    $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];
                    if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                        $disabled_cart_ids[] = $shipping_cart['item']['id'];
                    }
                }
            } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-category-quantity') && $moreThanHundred == FALSE) {

                foreach ($shipping_carts as $shipping_cart) {
                    $calculatedFee = self::calculateFee($shipping_cart['item'], $shipping_cart['qty'], $shipping_cart['subtotalPrice'], $zoneId, $shipping_category, $shipping_cart['items'], 'shipping');

                    $cartTotals['fee']['shipping']['amount'] +=   $calculatedFee['fee'];
                    $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];
                    if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                        $disabled_cart_ids[] = $shipping_cart['item']['id'];
                    }
                }
            }
        }
        if (!empty($installationByCategories)) {
            foreach ($installationByCategories as $installation_category => $installation_carts) {
                if (!empty($zoneId)) {
                    foreach ($zoneId as $key => $zone_id) {
                        $zone[] = $zone_id->id;
                    }
                    $iRates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $installation_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                }

                if (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-shipping-category')) {

                    if (in_array($installation_carts['item']['id'], $disabled_cart_ids)) continue;

                    foreach ($installation_carts['items'] as $key => $i) {
                        if (in_array($i->id, $disabled_cart_ids)) {
                            $installation_carts['qty'] -=  $i->quantity;
                            $installation_carts['subtotalPrice'] -=  $i->subtotal_price;
                            $cartTotals['subtotal'] -= $i->subtotal_price;
                            unset($installation_carts['items'][$key]);
                        }
                    }

                    $calculatedFee = self::calculateFee($installation_carts['item'], $installation_carts['qty'], $installation_carts['subtotalPrice'], $zoneId, $installation_category, $installation_carts['items'], 'installation');
                    $cartTotals['fee']['installation']['amount'] +=   $calculatedFee['fee'];
                    $cartTotals['fee']['installation']['shipping_information'][] = $calculatedFee['shipping_information'];
                } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-line-quantity')) {

                    foreach ($installation_carts as $installation_cart) {
                        if (in_array($installation_cart['item']['id'], $disabled_cart_ids)) continue;

                        $calculatedFee = self::calculateFee($installation_cart['item'], $installation_cart['qty'], $installation_cart['subtotalPrice'], $zoneId, $installation_category, $installation_cart['items'], 'installation');
                        $cartTotals['fee']['installation']['amount'] += $calculatedFee['fee'];
                        $cartTotals['fee']['installation']['shipping_information'][] = $calculatedFee['shipping_information'];
                    }
                } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-category-quantity') && $moreThanHundred == FALSE) {

                    foreach ($installation_carts as $installation_cart) {
                        if (in_array($installation_cart['item']['id'], $disabled_cart_ids)) continue;

                        $calculatedFee = self::calculateFee($installation_cart['item'], $installation_cart['qty'], $installation_cart['subtotalPrice'], $zoneId, $installation_category, $installation_cart['items'], 'installation');
                        $cartTotals['fee']['installation']['amount'] += $calculatedFee['fee'];
                        $cartTotals['fee']['installation']['shipping_information'][] = $calculatedFee['shipping_information'];
                    }
                }
            }
        }

        if ($disabled_cart_ids > 1) {
            $disabledCarts  = Cart::whereIn('id', $disabled_cart_ids)->get();
            foreach ($disabledCarts as $key => $cart) {
                $cartTotals['subtotal'] -= $cart->subtotal_price;
                $cartTotals['fee']['rebate']['amount'] -= $cart->rebate;
            }
        }

        $cartTotals['total'] = ($cartTotals['subtotal'] + $cartTotals['fee']['shipping']['amount'] + $cartTotals['fee']['installation']['amount']) - $cartTotals['fee']['rebate']['amount'];
        $cartTotals['total_old'] = ($cartTotals['subtotal'] + $cartTotals['shipping_old'] + $cartTotals['fee']['installation']['amount']) - $cartTotals['fee']['rebate']['amount'];

        return $cartTotals;
    }

    public static function calculateFee($carts, $qty, $subTotalPrice, $zoneId, $categoryId, $items, $type)
    {
        $fee = 0;
        $cartItems =  $shipping_information = array();

        if (!isset($cartItems[$carts->product_id])) {
            $cartItems[$carts->product_id]['totalQty'] = 0;
            $cartItems[$carts->product_id]['totalPrice'] = 0;
        }

        $cartItems[$carts->product_id]['totalQty'] += $qty;
        $cartItems[$carts->product_id]['totalPrice'] += $subTotalPrice;

        //get rates
        if (!empty($zoneId)) {
            foreach ($zoneId as $key => $zone_id) {
                $zone[] = $zone_id->id;
            }
            $rates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $categoryId)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
        }

        if (isset($rates) && $rates->isNotEmpty()) {

            foreach ($rates as $rate) {
                $minimun = $rate->min_rates;
                $maximum = $rate->max_rates;

                //check condition first, then check TOTAL qty|price between minimun and maximun or not, then check what is the calculation type
                if ($rate->condition == 'quantity') {
                    if ($cartItems[$carts->product_id]['totalQty'] >= $minimun && $cartItems[$carts->product_id]['totalQty'] <= $maximum) {

                        if ($rate->calculation_type == 'per-shipping-category') {

                            if ($rate->set_status != 0) {
                                $carts->disabled = $rate->set_status;
                            } else {
                                $carts->disabled = 0;
                            }

                            $carts->disabled = 0;
                            $fee = $rate->price;
                        } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                            if ($rate->set_status != 0) {
                                $carts->disabled = $rate->set_status;
                            } else {
                                $carts->disabled = 0;
                            }

                            $carts->disabled = 0;
                            $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                        }
                    } else {

                        if ($cartItems[$carts->product_id]['totalQty'] >= $maximum && $minimun == $maximum) {

                            if ($rate->calculation_type == 'per-shipping-category') {

                                if ($rate->set_status != 0) {
                                    $carts->disabled = $rate->set_status;
                                } else {
                                    $carts->disabled = 0;
                                }

                                $carts->disabled = 0;
                                $fee = $rate->price;
                            } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                                if ($rate->set_status != 0) {
                                    $carts->disabled = $rate->set_status;
                                } else {
                                    $carts->disabled = 0;
                                }

                                $carts->disabled = 0;
                                $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                            }
                        } else {

                            if ($type == 'shipping') {

                                if ($cartItems[$carts->product_id]['totalQty'] <= $minimun) {
                                    $carts->disabled = 1;
                                } else {
                                    $carts->disabled = 2;
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalQty'] <= $minimun) {
                                    $carts->disabled = 7;
                                } else {
                                    $carts->disabled = 6;
                                }
                            }

                            $fee = 0;
                        }
                    }
                } elseif ($rate->condition == 'price') {

                    if ($cartItems[$carts->product_id]['totalPrice'] >= $minimun && $cartItems[$carts->product_id]['totalPrice'] <= $maximum) {

                        if ($rate->calculation_type == 'per-shipping-category') {

                            if ($rate->set_status != 0) {
                                $carts->disabled = $rate->set_status;
                            } else {
                                $carts->disabled = 0;
                            }

                            $fee = $rate->price;
                        } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                            if ($rate->set_status != 0) {
                                $carts->disabled = $rate->set_status;
                            } else {
                                $carts->disabled = 0;
                            }

                            $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                        }
                    } else {

                        if ($cartItems[$carts->product_id]['totalPrice'] >= $maximum && $minimun == $maximum) {

                            if ($rate->calculation_type == 'per-shipping-category') {

                                if ($rate->set_status != 0) {
                                    $carts->disabled = $rate->set_status;
                                } else {
                                    $carts->disabled = 0;
                                }

                                $fee = $rate->price;
                            } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                                if ($rate->set_status != 0) {
                                    $carts->disabled = $rate->set_status;
                                } else {
                                    $carts->disabled = 0;
                                }

                                $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                            }
                        } else {

                            if ($type == 'shipping') {
                                if ($cartItems[$carts->product_id]['totalPrice'] <= $minimun) {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 1;
                                    }
                                } else {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 2;
                                    }
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalPrice'] <= $minimun) {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 7;
                                    }
                                } else {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 6;
                                    }
                                }
                            }

                            $fee = 0;
                        }
                    }
                }

                if ($fee != 0 && isset($fee) && $fee > 0) {
                    $qty = $cartItems[$carts->product_id]['totalQty'];
                    $price = $cartItems[$carts->product_id]['totalPrice'];
                    $shipping_information[] = "Fee: {RM" . number_format(($fee / 100), 2) . "} - RateID: {$rate->id}, Condition: {$rate->condition}, Min: {$minimun}, Max: {$maximum}, Type: {$rate->calculation_type}, Qty: {$qty}, Price: {RM" . number_format(($price / 100), 2) . "}";
                    break;
                }
            }
        } else {

            if ($type == 'shipping') {
                $carts->disabled = 2;
            } else {
                $carts->disabled = 6;
            }

            $fee = 0;
        }

        $carts->save();

        //update same catgeory items disabled
        foreach ($items as $item) {
            $id[] = $item->id;
        }

        $updates = Cart::whereIn('id', $id)
            ->where('bundle_id', 0)
            ->where('status', 2001)
            ->where('selected', 1)
            ->get();

        if (isset($updates)) {
            foreach ($updates as $update) {
                if ((array_key_exists('product_order_selfcollect', $update->product_information) && $update->product_information['product_order_selfcollect']) == 0) {
                    $update->disabled = $carts->disabled;
                    $update->save();
                }
            }
        }
        //end update same catgeory items disabled

        //update bundle child item
        self::updateBundleChild();
        // end update bundle child item

        return compact('fee', 'carts', 'shipping_information');
    }

    public static function maybeCreateCookieAddress()
    {
        $zone['city_key'] = 0;
        $zone['state_id'] = 0;

        $cookiesCountryId = country()->country_id;
        $shipCookie = json_decode(Cookie::get('addressCookie'));

        if ($cookiesCountryId == 'MY') {

            $zone['city_key'] = (isset($shipCookie->MY->city_key)) ? $shipCookie->MY->city_key : NULL;
            $zone['state_id'] = (isset($shipCookie->MY->state_id)) ? $shipCookie->MY->state_id : 0;
        } elseif ($cookiesCountryId == 'SG') {

            $zone['city_key'] = (isset($shipCookie->SG->city_key)) ? $shipCookie->SG->city_key : NULL;
            $zone['state_id'] = (isset($shipCookie->SG->state_id)) ? $shipCookie->SG->state_id : 0;
        }

        return $zone;
    }

    public static function getZone($state_id, $city_key)
    {
        //get user profile country, city and state
        $userCity = City::where('city_key', $city_key)->pluck('id')->first();
        $userState = $state_id;
        $userCountry = Countries::currentCountry();

        $userCity = (isset($userCity)) ? $userCity : NULL;
        $userState = (isset($userState)) ? $userState : NULL;
        $locations = Locations::join('shipping_zones', 'shipping_zones.id', '=', 'shipping_locations.zone_id')
            ->whereIn('shipping_locations.location_id', [$userCity, $userState, $userCountry->country_id])
            ->get();

        $array = array();
        foreach ($locations as $location) {
            $array[$location->sequence] = $location;
        }
        if (!empty($array)) {
            ksort($array);
        }

        return $array;
    }

    /**
     * Refresh cart
     */
    public static function updateCartItems(User $user, $country_code)
    {
        $user = User::find(Auth::user()->id);
        $userInfo = $user->userInfo;

        $selectedCartItems = Cart::where('user_id', $user->id)
            // ->where('selected', 1)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('country_id', $country_code)
            ->whereNotIn('disabled', [5])
            ->get();

        //check if user ini not dc customer
        if ($userInfo->account_status == '0') {
            // return;
            return;
        } else {

            // for each selected cart item
            foreach ($selectedCartItems as $selectedCartItem) {

                $img_link = $selectedCartItem->product_information;

                $img_link['product_color_img'] = $selectedCartItem->getImageUrlAttribute($selectedCartItem->product->parentProduct->id);

                $selectedCartItem->product_information = $img_link;

                $selectedCartItem->save();


                /* ---------------------------------- Rebate  ---------------------------------- */

                if (($selectedCartItem->rebate != 0) && ($selectedCartItem->product->parentProduct->can_tradein != 1)) {

                    $selectedCartItem->rebate = 0;

                    $attr = $selectedCartItem->product_information;

                    $attr['product_order_tradein'] = 0;

                    $selectedCartItem->product_information = $attr;

                    $selectedCartItem->save();
                }

                /* ---------------------------------- Disabled Product  ---------------------------------- */
                if ($selectedCartItem->product->parentProduct->product_status == 2) {

                    // disabled & unselected parent bundle
                    $selectedCartItem->selected = 0;
                    $selectedCartItem->disabled = 4;

                    $bundleChilds = Cart::where('bundle_id', $selectedCartItem->id)->where('country_id', $country_code)->get(); // find bundle's child

                    foreach ($bundleChilds as $bundleChild) {
                        // dd($bundleChild->selected);

                        //unselected & unselected bundle child
                        $bundleChild->selected = 0;
                        $selectedCartItem->disabled = 4;
                        $bundleChild->save();
                    }

                    $selectedCartItem->save();
                }

                /* ---------------------------------- size ---------------------------------- */
                if (isset($selectedCartItem->product_information['product_size_id'])) {
                    // return $selectedCartItem;

                    $selectedPrice = $selectedCartItem->product_information['product_size_id'];
                    //  $selectedPrice = $selectedCartItem->product_information['product_size_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);
                    // cart item price = dc customer price
                    // if member price is not 0
                    // if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                    if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                        $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                    } else {
                        $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                    }
                    // }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;

                    /* ------------------------------- temperature ------------------------------ */
                } elseif (isset($selectedCartItem->product_information['product_temperature_id'])) {

                    $selectedPrice = $selectedCartItem->product_information['product_temperature_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);

                    // cart item price = dc customer price
                    // if member price is not 0
                    // if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                    if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                        $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                    } else {
                        $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                    }
                    // }

                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;

                    /* ------------------------------ curtain size ------------------------------ */
                } elseif (isset($selectedCartItem->product_information['product_curtain_size_id'])) {

                    $selectedPrice = $selectedCartItem->product_information['product_curtain_size_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);

                    // cart item price = dc customer price
                    // if member price is not 0
                    // if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                    if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                        $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                    } else {
                        $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                    }
                    // }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;

                    /* ---------------------------------- color --------------------------------- */
                } elseif (isset($selectedCartItem->product_information['product_color_id'])) {

                    $selectedPrice = $selectedCartItem->product_information['product_color_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);

                    // cart item price = dc customer price
                    // if member price is not 0
                    // if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                    if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                        $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                    } else {
                        $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                    }
                    // }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;
                }

                if ($selectedCartItem->product->parentProduct->product_status != 2) {
                    if ($selectedCartItem->disabled == 4) {
                        $selectedCartItem->disabled = 0;
                    }
                } else {
                    $selectedCartItem->disabled = 4;
                    $selectedCartItem->selected = 0;
                }

                $selectedCartItem->save();
            }
        }

        return;
    }

    /**
     * Get total item count in a user's cart.
     */
    public function getTotalCartQuantity($id)
    {
        $country_code = country()->country_id;
        $user = User::find($id);
        $cartQuantity = $user->carts->where('status', 2001)->where('bundle_id', 0)->where('country_id', $country_code)->sum('quantity');

        $data['status'] = 'OK';
        $data['message'] = 'Get request successful.';
        $data['data'] = $cartQuantity;

        return response()->json($data, 200);
    }

    /**
     * Delete a cart item.
     */
    public function remove($id)
    {
        $cartItem = Cart::find($id);
        $cartItem->status = 2002;
        $cartItem->save();

        //bundle
        $cartSubItems = Cart::find($id)->where('bundle_id', $id)->get();

        if (isset($cartSubItems)) {
            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->status = 2002;
                $cartSubItem->save();
            }
        }
        //bundle end

        return $cartItem;
    }

    /**
     * Update cart quantity.
     */
    public function updateQuantity(Request $request, $id)
    {
        //bundle
        $cartSubItems = Cart::where('bundle_id', $id)->get();
        $main = Cart::find($id);

        if (isset($cartSubItems)) {
            $currentValue = $request->input('quantity');

            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->quantity = $cartSubItem->quantity / $main->quantity * $currentValue;

                $cartSubItem->save();
            }
        }
        //bundle end

        $cartItem = Cart::find($id);

        $cartItem->quantity = $request->input('quantity');
        $cartItem->subtotal_price = $cartItem->quantity * $cartItem->unit_price;

        // Trade In
        $cartItem->rebate = ($cartItem->rebate != 0) ? $cartItem->rebate * $request->input('quantity') : 0;
        $this_tradein = (isset($cartItem->product_information['product_order_tradein']) && $cartItem->product_information['product_order_tradein']);
        if ($this_tradein) {
            $cartItem->rebate =  $cartItem->rebate * $request->input('quantity');
        }
        // End trade In

        $cartItem->save();

        return $cartItem;
    }

    /**
     * Update checked status.
     */
    public function toggleSelectItem(Request $request, $id)
    {
        $cartItem = Cart::findOrFail($id);

        if ($cartItem->selected == 0) {
            $cartItem->selected = 1;
        } else {
            $cartItem->selected = 0;
        }
        $cartItem->save();

        //bundle
        $cartSubItems = Cart::where('bundle_id', $id)->get();
        if (isset($cartSubItems)) {
            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->selected = $cartItem->selected;

                $cartSubItem->save();
            }
        }
        //bundle end

        return response()->json($cartItem, 200);
    }

    public function productChangeAddress(Request $request)
    {
        $city = NULL;
        if ($request->input('city') != NULL && $request->input('city_key') == 0) $city = $request->input('city');

        $country_code = country()->country_id;
        $address_cookie = json_decode(request()->cookie('addressCookie'));

        $data = array(
            'address_1' => $request->input('address_1'),
            'address_2' => $request->input('address_2'),
            'address_3' => $request->input('address_3'),
            'postcode' => $request->input('postcode'),
            'city_name' => $city,
            'city_key' => $request->input('city_key'),
            'state_name' => State::find($request->input('state_id')),
            'state_id' => $request->input('state_id'),
            'name' => $request->input('name'),
            'mobile' => $request->input('mobile')
        );

        if ($address_cookie == NULL && empty($address_cookie->$country_code)) {
            $address_cookie[$country_code] = $data;
        } else {
            $address_cookie->$country_code = $data;
        }

        /* Recalculate fee */
        $new_state_id = $request->input('state_id');
        $new_city_key = $request->input('city_key');

        $newzoneId = self::getZone($new_state_id, $new_city_key);

        $user = User::find(Auth::user()->id);

        $getAllCarts = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('country_id', $country_code)
            ->whereNotIn('disabled', [5])
            ->get();

        self::shippingCalculate($newzoneId, $getAllCarts);

        $getCorrectCarts = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('selected', 1)
            ->where('country_id', $country_code)
            ->whereIn('disabled', [0, 3])
            ->get();

        self::shippingCalculate($newzoneId, $getCorrectCarts);
        /* End Recalculate fee */

        // return redirect()->back()->withCookie(Cookie::make('addressCookies', json_encode($address_cookie), 2628000));
    }

    //update bundle child
    public static function updateBundleChild()
    {
        $user = User::find(Auth::user()->id);
        $country_code = country()->country_id;
        $selectedBundleChilds = Cart::where('user_id', $user->id)
            ->where('selected', 1)
            ->where('status', 2001)
            ->where('bundle_id', '!=', 0)
            ->where('country_id', $country_code)
            ->whereNotIn('disabled', [5])
            ->get();

        // check and sync child status with parent status for bundle
        foreach ($selectedBundleChilds as $selectedBundleChild) {
            $parentBundle = Cart::where('user_id', $user->id)->where('id', $selectedBundleChild->bundle_id)->first();
            $selectedBundleChild->selected = $parentBundle->selected;
            $selectedBundleChild->disabled = $parentBundle->disabled;
            $selectedBundleChild->save();
        }
    }

    // backend checking bundle
    public static function doubleCheckingSubitem($panelProductId, $parentqty, $getProductBundleID, $bundleqty)
    {
        $BundleSets = array();
        $UserInputs = array();

        $fromBundleSets = ProductBundle::where('panel_product_id', $panelProductId)->get(); // From Bundle Set
        $fromUserInputs = ProductBundle::whereIn('id', $getProductBundleID)->get(); // From User Input

        foreach ($fromBundleSets as $fromBundleSet) {
            if (!isset($BundleSets[$fromBundleSet->bundle_id])) {
                $BundleSets[$fromBundleSet->bundle_id]['checkingqty'] = $fromBundleSet->bundle_qty * $parentqty;
            }
        }

        foreach ($fromUserInputs as $fromUserInput) {
            if (!isset($UserInputs[$fromUserInput->bundle_id])) {
                $UserInputs[$fromUserInput->bundle_id]['checkingqty'] = 0;
            }
            $UserInputs[$fromUserInput->bundle_id]['checkingqty'] += $bundleqty[$fromUserInput->id];
        }

        //compare 2 array
        if ($BundleSets != $UserInputs) return 1;

        // double check whether sub items are in bundle or not
        $checkingSubItem = ProductBundle::where('panel_product_id', $panelProductId)->whereIn('id', $getProductBundleID)->get();
        if ($checkingSubItem->isEmpty()) return 1;
    }

    public static function stateFilterCity(Request $request)
    {
        $state_id = 0;

        if ($request->input('state_id') != NULL) {
            $state_id = $request->input('state_id');
        } elseif ($request->input('shipping_state') != NULL) {
            $state_id = $request->input('shipping_state');
        }

        $cities = City::where('state_id', $state_id)->orderBy('city_name')->get();

        foreach ($cities as  $city) {
            $City[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $City[] = '<option value="0" "selected">Others</option>';

        return $City;
    }
}
