<?php

use App\Models\Products\ProductAttribute;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLimitTypeIntoPanelProductAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->string('purchase_limit_type');
        });
        $attributes = ProductAttribute::get();
        foreach($attributes as $attr){
            if($attr->productLimit->isNotEmpty()){
                $attr->purchase_limit_type = 'roles';
            }else{
                $attr->purchase_limit_type = 'no';
            }
            $attr->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('purchase_limit_type');
        });
    }
}
