<?php

namespace App\Models\Users\Dealers;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\Dealers\DealerInfo;
class DealerCountry extends Model
{
    // Set table
    protected $table = 'dealer_country';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        // 'account_id',
        // 'full_name',
        // 'nric'
    ];

    /**
     * Get the user info associated with the user.
     */
    public function dealerInfo()
    {
        return $this->belongsTo(DealerInfo::class, 'account_id','account_id');
    }
}
