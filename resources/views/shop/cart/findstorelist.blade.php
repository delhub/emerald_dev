<form action="#" method="get">
    <div class="modal-body find-store-box">

        <div class="store-scroll">

            @foreach ($outlets as $outlet)
                <div class="form-group">

                    <div class="store-details-box">
                        <div class="shoplot">
                            <img class="shop-img1" src="@if ($outlet->defaultImage) {{ asset('storage/' . $outlet->defaultImage->path . $outlet->defaultImage->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif">
                        </div>
                        <div class="shoplot-box shotlot-detail">
                            <ul>
                                <li>
                                    <h5>{{ $outlet->outlet_name }}</h5>
                                </li>
                                <li>
                                    <p>{{ $outlet->default_address }}</p>
                                </li>
                                <li>
                                    <p>{{ $outlet->postcode }} , {{ $outlet->city->city_name }}</p>
                                </li>
                                <li>
                                    <p>{{ $outlet->state->name }}</p>
                                </li>
                                <li class="shoplot-contact">
                                    <p><i class="fa fa-phone" aria-hidden="true"></i>{{ $outlet->contact_number }}</p>

                                    <p><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $outlet->operation_time }}
                                    </p>
                                </li>
                            </ul>

                        </div>
                        <div class="icon-stock">
                            <li class="shoplot-instock">
                                <p><i class="fa fa-check" aria-hidden="true"></i>In Stock
                                </p>
                            </li>
                        </div>
                        <input type="checkbox" class="form-control deliveryOutletId" name="storeLocation"
                            id="{{ $outlet->id }}" value="{{ $outlet->id }}">
                        <label for="{{ $outlet->id }}"></label>
                    </div>

                </div>
            @endforeach


        </div>

    </div>
    <p class="note-delivery">*Please select your location</p>
    <div class="edit-opt-btn modal-footer">
        <button id="findStoreBack" class="cancel-btn btn-primary">Clear</button>
        <button type="submit" id="storeSlc" class="savechg-btn btn btn-primary" data-bs-dismiss="modal"
            aria-label="Close">Select</button>
    </div>

</form>
@push('script')
    <script>
        // Find store select function effect
        let storeNamw = '';
        let deliverySlc = '';
        let selectLength = 0;
        const dlvType = document.querySelectorAll('.delivery-option .nav-item a');
        const slcStore = document.querySelector('#storeSlc');
        const storeInput = document.querySelectorAll('#findastore .find-store-box input[type="checkbox"]');
        const aciveBox = document.querySelectorAll('.find-store-box .store-details-box');
        const clearBtn = document.querySelector('#findStoreBack');
        const addresGetshow = document.querySelector('#showSelectedAddress');
        let updateURL;
        let getOutletInfoURL;
        let finalMethod;

        //get delivery type
        dlvType.forEach(typeItem => {
            // if(typeItem.className === 'nav-link active'){
            //     deliverySlc = typeItem.id;
            //         console.log('top',typeItem.id);
            // }
            typeItem.addEventListener('click', () => {

                method = typeItem.id;
                sessionUpdate(method)
                console.log('method here1method here1', method);
            })
        })

        function sessionUpdate(method){

            updateURl = "{{ route('shop.cart.update-deliveryMethod', ['method' => 'method']) }}";
            updateURl = updateURl.replace('method', method);

            $.ajax({
                    async: true,
                    beforeSend: function() {},
                    complete: function() {},

                    url: updateURl,
                    type: "GET",

                    success: function(result) {
                console.log('method here1', result);

                        if (method != 'delivery') {
                            $('.checkoutselfcollect').addClass('disableClick');
                        }
                        $('.cartShippingPrice').html((result.shippingFee / 100).toFixed(2));
                        $('.finalSubtotalPrice').html('MYR '+(result.finalTotal / 100).toFixed(2));
                        $('.finalSubtotalPrice').attr('data-price',result.finalTotal);
                        $('.finalSubtotalPrice').attr('data-shipping',result.shippingFee);

                        finalMethod = method;
                    },
                    error: function(result) {}
                });

        }

        //function clear all check
        function clearCheckAvtice() {
            aciveBox.forEach(eft => {
                eft.classList.remove('active');
            });
            storeInput.forEach(i => {
                i.checked = false;
            });
            addresGetshow.innerHTML = '';
            selectLength = 0;
        }

        //Unselect and select location
        $(storeInput).click(function() {
            $(storeInput).not(this).prop('checked', false);
        });

        //find store click back will remove all selection
        clearBtn.addEventListener('click', () => {
            clearCheckAvtice();
        })

        //find store get value and active selection effect
        function getAllfunc() {
            storeInput.forEach(item => {
                if (item.checked === true) {
                    storeName = item.value;
                    item.parentElement.classList.add('active');
                    // noteText.style.display = 'none';
                    console.log(storeName, item); // console.log('pls select your location');
                } else {
                    // noteText.style.display = 'block';
                    item.parentElement.classList.remove('active');
                    //console.log(item.checked);
                }
            });

        }

        aciveBox.forEach(box => {
            box.addEventListener('click', () => {
                getAllfunc();
            })
        })

        slcStore.addEventListener('click', (e) => {
            getAllfunc();
            e.preventDefault();

            updateDeliveryMethod();
            //$('#findastore').modal('hide');
        });

        function updateDeliveryMethod() {
            dlvType.forEach(typeItem => {
                if (typeItem.className === 'nav-link active') {
                    method = typeItem.id;

                    updateURl = "{{ route('shop.cart.update-deliveryMethod', ['method' => 'method']) }}";
                    updateURl = updateURl.replace('method', method);

                    $.ajax({
                        async: true,
                        beforeSend: function() {},
                        complete: function() {},

                        url: updateURl,
                        type: "GET",

                        success: function(result) {

                            if (method == 'Self-pickup') {
                                $('.cartShippingPrice').html((result.shippingFee / 100).toFixed(2));
                                $('.subLoader.finalSubtotalPrice').html('MYR '+(result.finalTotal / 100).toFixed(2));
                                $('.subLoader.finalSubtotalPrice').attr('data-price', result.finalTotal);
                            }

                            getOutletInfoURL = "{{ route('shop.cart.get-outletInfo', ['outletId' => 'outletId']) }}";

                            storeInput.forEach(item => {
                                if (item.checked === true) {
                                    outletId = item.value;
                                    getOutletInfoURL = getOutletInfoURL.replace('outletId', outletId);
                                }
                            })

                            showAddress(getOutletInfoURL,outletId,method);
                        },
                        error: function(result) {}
                    });

                    console.log('method here2', method);

                }
            })
        }

        $('.deliveryOutletId').click(function() {
            var outletId = 0;

            if ($('.deliveryOutletId').is(':checked')) {
                outletId = this.value;
            }
            updateOutletID(outletId);
        });

        function updateOutletID(outletId){
            updateURl = "{{ route('shop.cart.update-deliveryOutletId', ['outletId' => 'outletId']) }}";
            updateURl = updateURl.replace('outletId', outletId);

            $.ajax({
                async: true,
                beforeSend: function() {},
                complete: function() {},

                url: updateURl,
                type: "GET",

                success: function(result) {
                    //
                },
                error: function(result) {

                }
            });
        }

        function showAddress(getOutletInfoURL,outletId,method){

            //show address
            $.ajax({
                async: true,
                beforeSend: function() {
                    // Show loading spinner.
                    // loading.show();
                },
                complete: function() {
                    // Hide loading spinner.
                    // loading.hide();
                },
                url: getOutletInfoURL,
                type: "GET",
                data: {
                    // POST data.
                    outletId: outletId,
                },
                success: function(result) {
                    let g = "";
                    if (result !== null) {
                        g = result;
                        addresGetshow.innerHTML = `
                        <div class="d-cl">
                            <div class="sc-add" id="showSelectedAddress">
                            <p>${g}</p>
                            </div>
                        </div>
                        `;

                        $('.checkoutselfcollect').removeClass(
                            'disableClick');
                    }
                    //$('#showSelectedAddress').html(result);
                    console.log('result', result);
                    finalMethod = method;
                },
                error: function(result) {
                    // Log into console if there's an error.
                    console.log(result.status + ' ' + result.statusText);
                }
            });

        }
    </script>
@endpush
