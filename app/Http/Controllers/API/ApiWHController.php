<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use Carbon\Carbon;
use App\Traits\API\ApiWarehouse;

class ApiWHController extends Controller
{
    use ApiWarehouse;

    public function getBatch()
    {
        return $this->getWHbatch();
    }

    public function getBatchDetail()
    {
        return $this->getWHbatchDetail();
    }

    public function getBatchItem()
    {
        return $this->getWHbatchItem();
    }

    public function checkBatchItem($id)
    {
        return $this->checkWHbatchItem($id);
    }

}
