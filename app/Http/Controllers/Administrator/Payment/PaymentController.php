<?php

namespace App\Http\Controllers\Administrator\Payment;

use App\Http\Controllers\Administrator\Archimedes\ArchimedesController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Purchases\Purchase;
use App\Models\Globals\Status;
use App\Models\Purchases\Item;
use App\Models\Purchases\Order;
use App\Http\Controllers\BujishuAPI;
use App\Http\Controllers\Purchase\PaymentGatewayController;
use App\Http\Controllers\XilnexAPI;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Jobs\PDF\GeneratePdfPoDoEmail;
use App\Models\Users\UserPoints;
use App\Traits\Membership;
use Carbon\Carbon;
use App\Traits\Voucher;
use App\Models\Products\WarehouseInventories;
use App\Models\Purchases\Counters;
use App\Models\Users\User;
use App\Models\Warehouse\Order\WarehouseOrder;
use App\Traits\TableLogs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    use Membership;
    use Voucher;
    use TableLogs;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = array();
        switch ($request->query('paymentStatus')) {
            case 'created':
                $status = [3000];
                break;
            case 'pending':
                $status = [4000];
                break;
            case 'on-hold':
                $status = [4001];
                break;
            case 'paid':
                $status = [3001, 3002, 3003, 4002, 4007];
                break;
            case 'comm-cpcb':
                $status = [4003, 4007];
                break;
            case 'cancel':
                $status = [4004];
                break;
            case 'refund':
                $status = [4005, 4008, 4009];
                break;
            case 'failed':
                $status = [4006];
                break;
            default:
                $status = [3000, 3001, 3002, 3003, 4000, 4001, 4002, 4007, 4003, 4004, 4005, 4006, 4008, 4009];
        }

        $statuses = Status::whereIn('id', [3000, 4000, 4001, 4002, 4007, 4003, 4004, 4005, 4008, 4009, 4006])->get();

        $purchases = Purchase::with(['orders.items', 'payments'])
            // ->whereIn('purchases.purchase_status', $status)
            ->where('purchases.created_at', '>=', '2020-06-13')
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->statusFilter($status)
            ->orderBy('purchases.id', 'DESC')
            ->paginate(10);

        $purchase_types = Purchase::when($request->paymentStatus, function ($q) use ($status) {
            $q->whereIn('purchase_status', $status);
        })->pluck('purchase_type')->unique()->map(function ($purchase_type) {
            return ($purchase_type != null) ? (($purchase_type == 'card_cybersource') ? 'Card' : $purchase_type) : 'Not Available';
        });

        return view('administrator.payments.index', compact(['purchases', 'statuses', 'request', 'purchase_types']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messageWH = $message = '';
        $prePurchase = array(
            'normal' => array(),
            'vqs' => array(),
            'buyVqs' => array(),
        );

        if ($id == 'refund') {
            // dd($request->input());
            $purchase = Purchase::find($request->purchase_id);
            $status = Status::findOrFail($request->input('refund_type'));
            $notes = array();
            $notes['order_status_before'] = $request->order_before;
            $notes['purchase_status_before'] = $purchase->purchase_status;
            $notes['refund_type'] = $request->refund_type;
            $notes['admin_notes'] = $request->payment_notes;
            $purchase->purchases_notes = $notes;
        } else {
            $purchase = Purchase::find($id);
            $status = Status::findOrFail($request->input('purchase_status'));
        }

        $original_status = $purchase->purchase_status;

        $purchase->purchase_status = $status->id;
        $purchase->updated_at = Carbon::now();

        $purchase->save();

        if ($original_status != $status->id) $message = 'Not Updated to Server B.';

        //Record Created
        if ($status->id == 3000) {
            $do_numbers =  $request->do_number;
            $items = Item::whereIn('delivery_order', $do_numbers)->get();

            foreach ($items as $item) {
                if ($item->item_order_status != 1003) $item->item_order_status = 1000;
                $item->save();
            }

            $order_numbers =  $request->po_number;

            foreach ($order_numbers as $order_number) {

                $order = Order::where('order_number', $order_number)->first();

                if ($order->order_status != 1003) $order->order_status = 1000;

                $order->save();

                $prePurchase =  ArchimedesController::checkPrePurchaseProduct($order);



                //Purchase Order - Cancel
                if ($order->order_status != 1003) $this->updateWarehouseOrderInventories($order->delivery_order, 8003, $purchase->id, $prePurchase['normal']);
            }
        }

        //Payment Made - Card, Payment Made - FPX, Payment Made - Offline, Payment Made - Offline Approved, Paid, Paid(CP)
        if ($status->id == 3001 || $status->id == 3002 || $status->id == 3003 || $status->id == 3013 || $status->id == 4002 || $status->id == 4007) {

            if (!isset($purchase->inv_number) || !isset($purchase->inv_date)) {
                $counters = Counters::autoIncrementInv(['invoice'], $purchase);
                if (!isset($purchase->inv_number)) $purchase->inv_number =  $counters['invoice'];
                if (!isset($purchase->inv_date)) $purchase->inv_date = Carbon::now();

                $purchase->save();
            }

            $do_numbers =  $request->do_number;
            $items = Item::whereIn('delivery_order', $do_numbers)->get();

            foreach ($items as $item) {
                if ($item->item_order_status != 1003) {
                    if ($item->order->delivery_method == 'Self-pickup') {
                        $item->item_order_status = 1010;
                    } else {
                        $item->item_order_status = 1001;
                    }
                }
                $item->save();
            }

            $order_numbers =  $request->po_number;

            // dd( $request->po_number-);
            $date = $dates = array();
            foreach ($order_numbers as $key => $order_number) {

                $order = Order::where('order_number', $order_number)->first();
                if ($order->order_status != 1003) {
                    if ($item->order->delivery_method == 'Self-pickup') {
                        $order->order_status = 1010;
                    } else {
                        $order->order_status = 1001;
                    }
                }

                if ($order->order_type == 'rbs') {
                    // dd(substr($order->order_number, 13),$order->items->first()->product_information['product_rbs_frequency'],Carbon::now()->addMonth($order->items->first()->product_information['product_rbs_frequency']*substr($order->order_number, 13))->format('d/m/Y'));
                    $order->order_date = Carbon::now()->addMonth($order->items->first()->product_information['product_rbs_frequency'] * $key)->format('d/m/Y');
                    //    $date[] = $order->items->first()->product_information['product_rbs_frequency'];
                    //    $dates[] = $key;
                } else {
                    $order->order_date = Carbon::now()->format('d/m/Y');
                }


                $order->save();

                $prePurchase =  ArchimedesController::checkPrePurchaseProduct($order);
                ArchimedesController::addDealerData($prePurchase['vqs'], 5002);



                //Purchase Order - Stock Hold
                if ($order->order_status != 1003) $this->updateWarehouseOrderInventories($order->delivery_order, 8001, $purchase->id, $prePurchase['normal']);
            }
            // dd($date,$dates);

            if ($purchase->purchase_type == 'Offline') {
                (new PaymentGatewayController())->createOrderStoreRegistration($purchase, $purchase->user,  true, 'Offline', false);
                GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $purchase->user->email);
            }

            $this->voucherOfflinePaymentUpdate($purchase);
        }

        //Cancelled(CP), Cancelled, Refunded, Failed, Payment Made - Offline Rejected
        if ($status->id == 4003 || $status->id == 4004 || $status->id == 4006 || $status->id == 3023) {
            $do_numbers =  $request->do_number;
            $items = Item::whereIn('delivery_order', $do_numbers)->get();

            foreach ($items as $item) {
                if ($item->item_order_status != 1003) $item->item_order_status = 1004;
                $item->courier_name = NULL;
                $item->tracking_number = NULL;
                $item->ship_date = NULL;
                $item->actual_ship_date = NULL;
                $item->collected_date = NULL;
                $item->save();
            }

            $order_numbers =  $request->po_number;

            foreach ($order_numbers as $order_number) {

                $order = Order::where('order_number', $order_number)->first();
                if ($order->order_status != 1003) $order->order_status = 1004;

                $order->save();

                $prePurchase =  ArchimedesController::checkPrePurchaseProduct($order);


                //Purchase Order - Cancel
                if ($order->order_status != 1003) $this->updateWarehouseOrderInventories($order->delivery_order, 8003, $purchase->id, $prePurchase['normal']);
            }

            if ($purchase->credit_point_id) {
                $userPoints = UserPoints::find($purchase->credit_point_id);
                $userPoints->status = 'cancelled';
                $userPoints->save();
            }
        }

        //Pending
        if ($status->id == 4000) {
            $do_numbers =  $request->do_number;
            $items = Item::whereIn('delivery_order', $do_numbers)->get();
            foreach ($items as $item) {
                if ($item->item_order_status != 1003) $item->item_order_status = 1005;
                $item->save();
            }

            $order_numbers =  $request->po_number;

            foreach ($order_numbers as $order_number) {

                $order = Order::where('order_number', $order_number)->first();
                if ($order->order_status != 1003) $order->order_status = 1005;

                $order->save();

                $prePurchase =  ArchimedesController::checkPrePurchaseProduct($order);

                //Purchase Order - Stock Hold (Offline)
                if ($order->order_status != 1003) $this->updateWarehouseOrderInventories($order->delivery_order, 8011, $purchase->id, $prePurchase['normal']);
            }
        }

        // Refund
        if (in_array($status->id, [4005, 4008, 4009])) {

            // if ($original_status == $status->id) {
            //     return 'OK!';
            // }




            foreach ($purchase->orders as $order) {
                $do_numbers[] =  $order->delivery_order;
                $order_numbers[] =  $order->order_number;
            }

            // dd(!in_array($order->order_status,[1003]));

            if ($request->refund_type == 4009) {
                $items = Item::whereIn('id', $request->refund_items)->get();
                $whStatusID = 8015;
                // order_numbers
            } else {
                $items = Item::whereIn('delivery_order', $do_numbers)->get();
                $whStatusID = 8016;
            }

            foreach ($items as $item) {
                $item->item_order_status = 1011;
                $item->save();
            }

            foreach ($order_numbers as $order_number) {

                $order = Order::where('order_number', $order_number)->first();
                $order->order_status =  $request->refund_type == 4009 ? 1012 : 1011;

                $order->save();
            }

            if ($purchase->credit_point_id) {
                $userPoints = UserPoints::find($purchase->credit_point_id);
                $userPoints->status = 'cancelled';
                $userPoints->save();
            }
            // Refund Warehouse Status

            if (!in_array($order->order_status, [1003])) $messageWH = $this->updateWarehouseOrderInventories($order->delivery_order, $whStatusID, $purchase->id, $request->refund_items);

            //generate refund voucher
            if (in_array($status->id, [4008, 4009])) {

                $this->generate_refund_voucher($purchase, $items);
                // generate new invoice
                $generate = new PaymentGatewayController();
                return $generate->regenerateInvoice($request, $purchase->purchase_number);
            }
        }

        return 'OK. ' . $message . $messageWH;
    }

    public function orderList(Request $request)
    {
        //
        // dd($request->id);
        $purchases = Purchase::where('id', $request->id)->first();
        $purchasesItems = $purchases->items;
        $checkboxForm = array();

        foreach ($purchasesItems as $purchasesItem) {
            $checkboxForm[] = "<div class='form-check'><input class='form-check-input' name='refund_items[]' type='checkbox' value='" . $purchasesItem->id . "' id='flexCheckDefault'>"
                . "(" . $purchasesItem->product_code . ") " . $purchasesItem->product->parentProduct->name . "</div>";
        }

        return $checkboxForm;
    }

    public function summary(Request $request)
    {
        $purchase_statuses = [];
        $summary_date = null;
        $parsePurchaseDate = \DB::raw("STR_TO_DATE(`purchase_date`, '%d/%m/%Y')");

        if (isset($request->date_before) || isset($request->date_after)) {

            if (!isset($request->date_before)) {
                $request->date_before = Carbon::createFromFormat('d/m/Y', Purchase::orderBy($parsePurchaseDate)->value('purchase_date'));
            }

            if (!isset($request->date_after)) $request->date_after = today();

            $request->date_before = Carbon::parse($request->date_before)->startOfDay();
            $request->date_after = Carbon::parse($request->date_after)->endOfDay();

            $summary_date = "{$request->date_before->format('d M Y')} - {$request->date_after->format('d M Y')}";
        } else {
            $request->date_before = today()->startOfDay();
            $request->date_after = today()->endOfDay();
            $summary_date = today()->format('d M Y');
        }

        $purchase_types = Purchase::whereNotNull('purchase_type')
            ->whereHas('status')
            ->distinct()
            ->pluck('purchase_type')
            ->map(function ($item) {
                return \Str::title($item);
            })->sort()
            ->toArray();
        Purchase::whereHas('status')->distinct()->get('purchase_status')->each(function ($purchase) use (&$purchase_statuses) {
            $purchase_statuses[] = (['name' => $purchase->status->name, 'value' => $purchase->purchase_status]);
        });

        $purchase_totals = $purchase_cases = array_fill_keys(['paid', 'total'], array_fill_keys($purchase_types, 0));

        $purchases = Purchase::whereNotNull('purchase_type')->whereHas('status')->selectRaw('purchase_status, purchase_type, sum(purchase_amount) as total_purchase_amount, count(*) as total_purchases')
            ->when(isset($request->date_before) || isset($request->date_after), function ($q) use ($request, $parsePurchaseDate) {
                $q->whereBetween($parsePurchaseDate, [Carbon::parse($request->date_before)->startOfDay(), Carbon::parse($request->date_after)->endOfDay()]);
            })
            ->groupBy(['purchase_status', 'purchase_type'])
            ->get()
            ->mapToGroups(function ($purchase) use (&$purchase_cases, &$purchase_totals) {
                $purchase->purchase_type = \Str::title($purchase->purchase_type); //add
                $total_amount = (int) $purchase->total_purchase_amount;

                if (in_array($purchase->purchase_status, [4002, 4007])) {
                    $purchase_totals['paid'][$purchase->purchase_type] += $total_amount;
                    $purchase_cases['paid'][$purchase->purchase_type] += $purchase->total_purchases;
                }

                $purchase_totals['total'][$purchase->purchase_type] += $total_amount;
                $purchase_cases['total'][$purchase->purchase_type] += $purchase->total_purchases;

                return [$purchase->purchase_status => [$purchase->purchase_type => $total_amount]];
            })
            ->map(function ($status_purchases) use ($purchase_types) {

                $purchases_by_type = $status_purchases->mapWithKeys(function ($purchases) {
                    return $purchases;
                })->toArray();
                $default_values = array_fill_keys($purchase_types, 0);
                return array_sort_recursive($purchases_by_type + $default_values);
            });


        $additional_statuses = [
            ['name' => 'Paid/Paid (CB)', 'value' => '4002, 4007'],
            ['name' => 'ALL', 'value' => 'all']
        ];

        $purchase_statuses = array_merge($additional_statuses, $purchase_statuses);

        return view('administrator.payments.summary', compact('purchases', 'purchase_statuses', 'purchase_types', 'purchase_cases', 'purchase_totals', 'summary_date'));
    }

    public function updateWarehouseOrderInventories($do_numbers, $statueId, $purchaseID, $partialItems = null)
    {
        $messageWH = '';

        $whOrder = WarehouseOrder::where('delivery_order', $do_numbers)->first();
        if ($whOrder) {
            $whOrder->inv_type = $statueId;
            $whOrder->save();

            if ($partialItems || !empty($partialItems)) {
                $productCodes = Item::whereIn('id', $partialItems)->pluck('product_code');
            } else {
                $productCodes = Item::where('delivery_order', $do_numbers)->pluck('product_code');
            }

            $whInventories = WarehouseInventories::where('model_type', 'App\Models\Warehouse\Order\WarehouseOrder')->where('model_id', $whOrder->id)->whereIn('product_code', $productCodes)->get();

            foreach ($whInventories as $key => $whInventory) {
                $whInventory->inv_type = in_array($statueId, [8016, 8015]) ? 8015 : $statueId;
                $whInventory->save();
            }
        } else {
            $messageWH = '<p class="text-danger">**Warehouse is not updated because no data in wharehouse order</p>';
            $purchasesData = Purchase::where('id', $purchaseID)->first();
            $whLogs = DB::table('wh_logs')->insert([
                'model_type' => 'App\Models\Purchases\Purchase',
                'model_id' => $purchaseID,
                'log_status' => 'ignored',
                'log_user' => (User::find(Auth::user())) ? User::find(Auth::user()->id)->id : 0,
                'log_data' => json_encode($purchasesData->purchases_notes),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        return $messageWH;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewPDF(Request $request, $purchase)
    {

        $purchase = Purchase::where('id', $purchase)->first();

        $outletInfos = array();
        foreach ($purchase->items as $item) {
            if (array_key_exists('selfcollect_location_id', $item->product_information)) {
                $outletInfos[$item->id] = Outlet::where('id', $item->product_information['selfcollect_location_id'])->first('outlet_name') ?? '';
            }
        }
        // $countries = Countries::getAll();
        // $country = $countries[$purchase->country_id];

        $contentHtml = view()->make('documents.purchase.v2.invoice-v2',  compact('purchase'/* , 'country' */, 'outletInfos'))->render();

        return view('documents.purchase.v2.view-invoice',  compact('contentHtml'));

    }
}
