@extends('layouts.guest.main')

@section('content')
@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp

<div class="r-bg">
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo" src="{{ asset("images/logo/formula-logo.png") }}" alt="">
        </div>
    </div>
    <div>
        <div class="my-register-box guests-card">
            <div class="my-r-title-box">
                <h5>{{$user->notes == 'dealer incomplete' ? 'Update Agent Information' : 'Agent Registration'}}</h5>
            </div>
             <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link register-tab-active active" id="profile-tab" data-toggle="tab"
                        href="#information" role="tab" aria-controls="profile" aria-selected="false">
                        {{$user->notes == 'dealer incomplete' ? 'You are redirected to this page because some required information is missing or incomplete. Please fill in the required fields and click submit.'
                        : 'Congratulations! You have successfully registered as an Agent. Please fill up the following information to confirm your participation.'}}</a>
                </li>
            </ul>
            <div class="card-body">
                <!-- Dealer Registration Form -->
                <form method="POST" action="{{ route('guest.register.dealer.information.post') }}" id="register-form"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <!-- Information Tab -->
                        <div class="r-subt-box tab-pane fade show active" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                            <h5>Account Particulars</h5>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="full_name">Full Name (as per NRIC) <small class="text-danger">*</small></label>
                                    <input type="text" name="full_name"
                                        class="form-control @error('full_name') is-invalid @enderror" id="full_name"
                                        {{-- (($user->dealerInfo) ? $user->dealerInfo->full_name : (($user->userInfo) ? $user->userInfo->full_name : '' ))  --}}

                                        value="{{ old('full_name', $user->userInfo->full_name)}}">
                                    @error('full_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nric">NRIC Number <small>@if ($country =='MY') (Numbers Only) @endif</small> <small class="text-danger">*</small></label>
                                    <input type="text" name="nric"
                                        class="form-control @error('nric') is-invalid @enderror" id="nric"
                                         {{-- value="{{ old('nric', (($user->dealerInfo) ? $user->dealerInfo->nric : '')) }}"> --}}

                                         value="{{ old('nric', $user->dealerInfo == null ? $user->userInfo->nric : $user->dealerInfo->nric)}}">
                                    @error('nric')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="date_of_birth">Date Of Birth <small class="text-danger">*</small></label>
                                    <input type="date"
                                        class="form-control datepicker @error('date_of_birth') is-invalid @enderror"
                                        id="date_of_birth" name="date_of_birth"
                                        {{-- value="{{ old('date_of_birth', $user->dealerInfo->date_of_birth) }}"> --}}

                                        value="{{ old('date_of_birth', $user->dealerInfo == null ? $user->userInfo->date_of_birth : $user->dealerInfo->date_of_birth) }}">
                                    @error('date_of_birth')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="gender">Gender <small class="text-danger">*</small></label>

                                    <select class="form-control text-capitalize" id="gender_id" name="gender_id">
                                        <option disabled selected value="default">Choose your gender..</option>
                                        @foreach($genders as $gender)
                                        <option value="{{ $gender->id }}"
                                            {{-- (old('gender_id',$user->dealerInfo->gender_id)==$gender->id)?'selected':'' --}}
                                            {{ old('gender_id', !$user->dealerInfo ? : $user->dealerInfo->gender_id) == $gender->id ? 'selected' : '' }}>

                                            {{ $gender->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('gender_id')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="race_id">Race <small class="text-danger">*</small></label>
                                    <select name="race_id" id="race_id" class="form-control text-capitalize">
                                        <option disabled selected value="default">Choose your race..</option>
                                        @foreach($races as $race)
                                        <option value="{{ $race->id }}"
                                            {{-- (old('race_id',$user->dealerInfo->race_id)==$race->id)?'selected':'' --}}
                                            {{ old('race_id', !$user->dealerInfo ? : $user->dealerInfo->race_id) == $race->id ? 'selected' : '' }}>
                                            {{$race->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('race_id')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="marital_id">Marital Status <small class="text-danger">*</small></label>
                                    <select name="marital_id" id="marital_id" class="form-control text-capitalize">
                                        <option disabled selected value="default">Choose your marital status..</option>
                                        @foreach($maritals as $marital)
                                        <option value="{{ $marital->id }}"
                                            {{--{{ (old('marital_id',$user->dealerInfo->marital_id)==$marital->id)?'selected':'' }}--}}
                                            {{ old('marital_id', !$user->dealerInfo ? : $user->dealerInfo->marital_id) == $marital->id ? 'selected' : '' }}>

                                            {{ $marital->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('marital_id')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="address_1">Residential Address Line 1 <small class="text-danger">*</small></label>
                                    <input type="text" name="address_1" id="address_1"
                                        class="form-control @error('address_1') is-invalid @enderror"

                                        value="{{ old('address_1', $user->userInfo->shippingAddress->address_1) }}">
                                    @error('address_1')
                                    <small class="form-text text-danger">{{ $message }}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="address_1">Residential Address Line 2</label>
                                    <input type="text" name="address_2" id="address_2"
                                        class="form-control @error('address_2') is-invalid @enderror"

                                        value="{{ old('address_2', $user->userInfo->shippingAddress->address_2) }}">
                                    @error('address_2')
                                    <small class="form-text text-danger">{{ $message}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="postcode">Postcode <span class="form-text text-danger ajaxMessage d-inline" style="display: none"></span><small class="text-danger">*</small></label>
                                    <input type="text" name="postcode" id="postcode"
                                        class="form-control @error('postcode') is-invalid @enderror"

                                        value="{{ old('postcode', $user->userInfo->shippingAddress->postcode) }}">
                                    @error('postcode')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="state">State <small class="text-danger">*</small></label>
                                    <select name="state" id="state" class="form-control">
                                        <option disabled selected>Choose your state..</option>
                                        @foreach($states as $state)
                                        <option class="text-capitalize" value="{{ $state->id }}"
                                            {{ (old('state', $user->userInfo->shippingAddress->state_id) == $state->id) ? 'selected' : '' }}>
                                            {{ $state->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('state')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12" id="class_city_key">
                                    <label for="city_key">City <small class="text-danger">*</small></label>
                                    <select name="city_key" id="city_key" class="form-control @error('city_key') is-invalid @enderror">
                                        <option disabled selected>Choose your city..</option>
                                         @foreach($cities as $city)
                                        <option class="text-capitalize" value="{{ $city->city_key }}"
                                            @if(old('city_key', $user->userInfo->shippingAddress->city_key) == $city->city_key) selected @endif>{{ $city->city_name }}</option>
                                        @endforeach
                                        <option value="0">Others</option>
                                    </select>
                                </div>

                                @error('city_key')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                                <div class="form-group col-md-6" id="city">
                                    <label for="city">Others City</label>
                                    <input type="text" name="city"  class="form-control" value="{{ old('city',$user->userInfo->shippingAddress->city) }}" placeholder="Entry Your City Here">
                                </div>
                            </div>



                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="contact_number_home">Contact Number (Home)</label>
                                    <input type="text" name="contact_number_home" id="contact_number_home"
                                        class="form-control @error('contact_number_home') is-invalid @enderror"

                                        {{-- value="{{ old('contact_number_home', $user->dealerInfo->dealerHomeContact->contact_num) }}"> --}}

                                        value="{{ old('contact_number_home', $user->userInfo->homeContact->contact_num) }}">
                                    @error('contact_number_home')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="contact_number_mobile">Contact Number (Mobile) <small class="text-danger">*</small></label>
                                    <input type="text" name="contact_number_mobile" id="contact_number_mobile"
                                        class="form-control @error('contact_number_mobile') is-invalid @enderror"

                                        {{-- value="{{ old('contact_number_mobile', $user->dealerInfo->dealerMobileContact->contact_num) }}"> --}}

                                        value="{{ old('contact_number_mobile', $user->userInfo->mobileContact->contact_num) }}">
                                    @error('contact_number_mobile')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>



                            <!-- Employment History -->
                        <div class='employment-history'>
                            <h5 class="text-center"
                                style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">
                                Employment History</h5>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="employment_id">Employment Status <small class="text-danger">*</small></label><br/>
                                    @foreach($employments as $employment)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="employment_id"
                                            id="employment_id" value="{{ $employment->id }}"
                                            {{-- ((old('employment_id',$user->dealerInfo->employmentAddress->employment_type))==$employment->id)?'checked':'' --}}
                                            {{ old('employment_id', (!$user->dealerInfo or !$user->dealerInfo->employmentAddress) ? 0 : $user->dealerInfo->employmentAddress->employment_type) == $employment->id ? 'checked' : '' }}>
                                        <label class="form-check-label text-capitalize">{{ $employment->name }}</label>
                                    </div>
                                    @endforeach
                                    @error('employment_id')
                                    <label for="employment_id" class="form-text error">{{ $message }}</label>
                                    @enderror

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="employment_name">Name Of Company <small class="text-danger">*</small></label>
                                    <input type="text" name="employment_name" id="employment_name"
                                        class="form-control @error('employment_name') is-invalid @enderror"

                                        value="{{old('employment_name', (!$user->dealerInfo or !$user->dealerInfo->employmentAddress) ? '' : $user->dealerInfo->employmentAddress->company_name)}}">
                                    @error('employment_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="company_address_1">Company Address Line 1 <small class="text-danger">*</small></label>
                                    <input type="text" name="company_address_1" id="company_address_1"
                                        class="form-control @error('company_address_1') is-invalid @enderror"

                                        value="{{old('company_address_1', (!$user->dealerInfo or !$user->dealerInfo->employmentAddress) ? '' : $user->dealerInfo->employmentAddress->company_address_1)}}">
                                    @error('company_address_1')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="company_address_2">Company Address Line 2</label>
                                    <input type="text" name="company_address_2" id="company_address_2"
                                        class="form-control @error('company_address_2') is-invalid @enderror"

                                        value="{{old('company_address_2', (!$user->dealerInfo or !$user->dealerInfo->employmentAddress) ? '' : $user->dealerInfo->employmentAddress->company_address_2)}}">
                                    @error('company_address_2')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="company_postcode">Postcode <small class="text-danger">*</small></label>
                                    <input type="text" name="company_postcode" id="company_postcode"
                                        class="form-control @error('company_postcode') is-invalid @enderror"

                                         value="{{old('company_postcode', (!$user->dealerInfo or !$user->dealerInfo->employmentAddress) ? '' : $user->dealerInfo->employmentAddress->company_postcode)}}">
                                    @error('company_postcode')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="state">State <small class="text-danger">*</small></label>
                                    <select name="company_state" id="company_state" class="form-control">
                                        <option disabled selected>Choose your state..</option>
                                        @foreach($states as $state)
                                        <option class="text-capitalize" value="{{ $state->id }}"
                                            {{ (old('company_state', $user->dealerInfo->employmentAddress->company_state_id ?? '') == $state->id)?'selected':'' }}>
                                            {{ $state->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('company_state')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                </div>
                            </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12" id="class_company_city_key">
                                    <label for="company_city_key">City <small class="text-danger">*</small></label>
                                    <select name="company_city_key" id="company_city_key" class="form-control @error('company_city_key') is-invalid @enderror">
                                        <option disabled selected>Choose your city..</option>
                                        @foreach($cities as $city)
                                        <option class="text-capitalize" value="{{ $city->city_key }}"
                                            @if(old('company_city_key')==$city->city_key) {{'selected'}} @endif>{{ $city->city_name }}</option>
                                        @endforeach
                                        <option value="0">Others</option>
                                    </select>
                                    </div>


                                    @error('company_city_key')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror

                                    <div class="form-group col-md-6" id="company_city">
                                        <label for="company_city">Others City</label>
                                        <input type="text" name="company_city"  class="form-control" value=" {{old('company_city', (!$user->dealerInfo or !$user->dealerInfo->employmentAddress) ? '' : $user->dealerInfo->employmentAddress->company_city)}} " placeholder="Enter Your City Here">
                                    </div>
                                </div>

                                <!-- Spouse's Particular -->
                            <div class='spouse-particular'>
                                <h5 class="text-center"
                                    style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">
                                    Spouse's Particulars</h5>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="spouse_full_name">Spouse's Full Name <small>(as per NRIC)</small></label>
                                        <input type="text" name="spouse_full_name"
                                            class="form-control @error('spouse_full_name') is-invalid @enderror"
                                            id="spouse_full_name"
                                            value=" {{old('spouse_full_name', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_name)}} " {{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_full_name')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="spouse_nric">Spouse's NRIC Number</label>
                                        <input type="text" name="spouse_nric"
                                            class="form-control @error('spouse') is-invalid @enderror" id="spouse_nric"
                                            value=" {{old('spouse_nric', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_nric)}} " {{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_nric')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="spouse_date_of_birth">Spouse's Date Of Birth</label>
                                        <input type="date" name="spouse_date_of_birth"
                                            class="form-control @error('spouse_date_of_birth') is-invalid @enderror"
                                            id="spouse_date_of_birth"
                                            value=" {{old('spouse_date_of_birth', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_date_of_birth)}} "{{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_date_of_birth')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="spouse_occupation">Spouse's Occupation</label>
                                        <input type="text" name="spouse_occupation"
                                            class="form-control @error('spouse_occupation') is-invalid @enderror"
                                            id="spouse_occupation"
                                            value=" {{old('spouse_occupation', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_occupation)}} "{{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_occupation')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="spouse_contact_office">Contact Number (Office)</label>
                                        <input type="text" name="spouse_contact_office"
                                            class="form-control @error('spouse_contact_office') is-invalid @enderror"
                                            id="spouse_contact_office"
                                            value=" {{old('spouse_contact_office', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_contact_office)}} "{{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_contact_office')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="spouse_contact_mobile">Contact Number (Mobile)</label>
                                        <input type="text" name="spouse_contact_mobile"
                                            class="form-control @error('spouse_contact_mobile') is-invalid @enderror"
                                            id="spouse_contact_mobile"
                                            value=" {{old('spouse_contact_mobile', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_contact_mobile)}} "{{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_contact_mobile')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="spouse_email">Email Address</label>
                                        <input type="text" name="spouse_email" id="spouse_email"
                                            class="form-control @error('spouse_email') is-invalid @enderror"

                                            value=" {{old('spouse_email', (!$user->dealerInfo or !$user->dealerInfo->dealerSpouse) ? '' : $user->dealerInfo->dealerSpouse->spouse_email)}} "{{ $user->dealerInfo && $user->dealerInfo->marital_id == 1 ? 'readonly' : '' }}>
                                        @error('spouse_email')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>


                            <!-- Bank Details -->
                            <div class='bank-details'>
                            <h5 class="text-center"
                                style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">
                                Bank Details</h5>
                                {{-- @php
                                dd(1);
                                 @endphp --}}
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="state">Bank Name <small class="text-danger">*</small></label>
                                    <select name="bank_name" id="bank_name" class="form-control text-capitalize">
                                        @foreach($bankNames as $bankName)
                                        <option value="{{ $bankName->bank_code }}"
                                            {{-- (isset($user->dealerInfo->dealerBankDetails)&&$bankName->bank_code==$user->dealerInfo->dealerBankDetails->bank_code)?'selected':'' --}}
                                            {{ old('bank_name', (!$user->dealerInfo or !$user->dealerInfo->dealerBankDetails) ? '' : $user->dealerInfo->dealerBankDetails->bank_code) == $bankName->bank_code ? 'selected' : '' }}>
                                            {{$bankName->bank_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('bank_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="account_holder_name">Account Holder's Name <small class="text-danger">*</small></label>
                                    <input type="text" name="account_holder_name" id="account_holder_name"
                                        class="form-control @error('account_holder_name') is-invalid @enderror"

                                        value="{{old("account_holder_name", (!$user->dealerInfo or !$user->dealerInfo->dealerBankDetails) ? '' : $user->dealerInfo->dealerBankDetails->bank_acc_name)}}">
                                    @error('account_holder_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="bank_account_number">Bank Account Number <small class="text-danger">*</small></label>
                                    <input type="text" name="bank_account_number" id="bank_account_number"
                                        class="form-control @error('bank_account_number') is-invalid @enderror"

                                        value="{{old('bank_account_number', (!$user->dealerInfo or !$user->dealerInfo->dealerBankDetails) ? '' : $user->dealerInfo->dealerBankDetails->bank_acc)}}">
                                    @error('bank_account_number')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="bank_account_number">Account Holder NRIC/ Company Registration <small class="text-danger">*</small></label>
                                    <input type="text" name="account_regid" id="account_regid"
                                        class="form-control @error('account_regid') is-invalid @enderror"

                                        value="{{old('account_regid', (!$user->dealerInfo or !$user->dealerInfo->dealerBankDetails) ? '' : $user->dealerInfo->dealerBankDetails->account_regid)}}">
                                    @error('account_regid')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                    </div>
                            <!-- Next Button -->
                            <div class="edit-opt-btn" style="text-align: right;">
                                <!-- <a class="btn btn-secondary" id="profile-tab" data-toggle="tab" href="#introducer" role="tab" aria-controls="profile" aria-selected="false">Next</a> -->
                                <button type="submit" id="submit"
                                    class="cancel-btn btn-primary">Save
                                    Information</button>
                            </div>

                            <div id="error-div" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="model-notification" tabindex="-1" aria-labelledby="accountCreatedNotificationLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md edit-popup-bg">
        <div class="modal-content edit-info-modal-box confirm-info">
            <div class="modal-header pb-0"
                style="border-bottom: none;padding-top: 40px;padding-left: 50px;padding-right: 50px;">
                <h4 class="modal-title edit-tittle" id="accountCreatedNotificationLabel">Update Success!</h4>
            </div>
            <div class="modal-body message">
                <div class="row">
                    <div class="col-12">
                        <p id="message" class="fs-6 text-danger mb-2"
                            style="text-align: left; padding-left: 35px;padding-right: 30px;">Your information has been saved.
                            Please click close button to proceed to homepage.</p>
                    </div>
                    <div class="justify-content-center log-exp-btn" style="border-top: 0; color: white;">
                        <a class="le-btn" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<style>
    .error {
        color: red;
    }




</style>

@endpush

@push('script')
<script>
    // Disable spouse information if marital status is single
    $(document).on('change', 'select[name=marital_id]', function(e) {
        var el = $(this);

        if (el.val() === '1' || el.val() === '3') {

            //reset spouse fields to empty if user input changes to 1 or 3
            $('input[name=spouse_full_name]').val('');
            $('input[name=spouse_nric]').val('');
            $('input[name=spouse_full_name]').val('');
            $('input[name=spouse_date_of_birth]').val('');
            $('input[name=spouse_occupation]').val('');
            $('input[name=spouse_contact_office]').val('');
            $('input[name=spouse_contact_mobile]').val('');
            $('input[name=spouse_email]').val('');

            //disable spouse information if single
            $('#spouse_full_name').prop('readonly', true);
            $('#spouse_nric').prop('readonly', true);
            $('#spouse_date_of_birth').prop('readonly', true);
            $('#spouse_occupation').prop('readonly', true);
            $('#spouse_contact_office').prop('readonly', true);
            $('#spouse_contact_mobile').prop('readonly', true);
            $('#spouse_email').prop('readonly', true);
        } else {
            $('#spouse_full_name').prop('readonly', false);
            $('#spouse_nric').prop('readonly', false);
            $('#spouse_date_of_birth').prop('readonly', false);
            $('#spouse_occupation').prop('readonly', false);
            $('#spouse_contact_office').prop('readonly', false);
            $('#spouse_contact_mobile').prop('readonly', false);
            $('#spouse_email').prop('readonly', false);
        }
    });

    // Custom form validation for select.
    $.validator.addMethod("valueNotEquals", function(value, element, arg) {
        return arg !== value;
    }, "Please select an item.");


    // Form Validation
    $("#register-form").validate({
        rules: {
            full_name: {
                required: true,
                minlength: 3
            },

            nric: {
                required: true,
                @if(country()->country_id == "MY")
                minlength: 12,
                maxlength: 12
                @endif

            },

            date_of_birth: {
                required: true,
            },
            gender_id: {
                required: true,
                valueNotEquals: "default"
            },
            race_id: {
                required: true,
                valueNotEquals: "default"
            },
            marital_id: {
                required: true,
                valueNotEquals: "default"
            },
            address_1: {
                required: true,
                minlength: 3
            },

            postcode: {
                required: true,
                minlength: 5,
                maxlength: 6
            },
            city: {
                required: true
            },
            city_key: {
                required: true
            },
            state: {
                required: true
            },

            contact_number_mobile: {
                required: true,
                digits: true,
                minlength: 8,
                maxlength: 20
            },
            existing_customer: {
                required: true
            },

            // employment_id: {
            //     required: true,
            // },
            employment_name: {
                required: true,
                minlength: 3
            },
            company_address_1: {
                required: true
            },
            company_postcode: {
                required: true
            },
            company_city: {
                required: true
            },
            company_city_key: {
                required: true
            },

            company_state: {
                required: true
            },
            bank_account_number : {
                required: true
            },
            account_holder_name : {
                required: true

            },
            account_regid : {
                required: true
            }
        },
        messages: {
            full_name: {
                required: "Please enter your full name.",
                minlength: "Your name must be more than 3 characters."
            },
            nric: {
                required: "Please enter your NRIC number.",
                minlength: "Please enter a valid NRIC number.",
                maxlength: "Please enter a valid NRIC number."
            },
            date_of_birth: {
                required: "Please select your date of birth."
            },
            address_1: {
                required: "Please enter your address"
            },

            postcode: {
                required: "Please enter your postcode"
            },
            city_key: {
                required: "Please enter your city name."
            },
            city: {
                required: "Please enter your city name."
            },
            state: {
                required: "Please select your state"
            },
            // employment_id: {
            //     required: "Please select employment status"
            // },

            contact_number_mobile: {
                required: "Please enter your mobile number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            },
            spouse_full_name: {
                required: "Please enter your spouse's full name.",
                minlength: "Your spouse's name must be more than 3 characters."
            },
            spouse_nric: {
                required: "Please enter your spouse's NRIC number.",
                minlength: "Please enter a valid NRIC number.",
                maxlength: "Please enter a valid NRIC number."
            },
            spouse_date_of_birth: {
                required: "Please select your spouse's date of birth."
            },
            spouse_occupation: {
                required: "Please enter your spouse's occupation.",
                minlenght: "Your spouse's occupation must be more than 3 characters."
            },
            spouse_contact_office: {
                required: "Please enter your spouse's home number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            },
            spouse_contact_mobile: {
                required: "Please enter your spouse's mobile number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            },
            spouse_email: {
                required: "Please enter an email.",
                email: "The email is not valid."
            },
            bank_account_number: {
                required: "Please enter number only."
            }
        }
    });

    // Recheck again before submit.
    $('#submit').click(function(e) {
        if (
            $("#register-form").validate().element('#full_name') &&
            $("#register-form").validate().element('#nric') &&
            $("#register-form").validate().element('#date_of_birth') &&
            $("#register-form").validate().element('#gender_id') &&
            $("#register-form").validate().element('#race_id') &&
            $("#register-form").validate().element('#marital_id') &&
            $("#register-form").validate().element('#address_1') &&
            $("#register-form").validate().element('#postcode') &&
            $("#register-form").validate().element('#city') &&
            $("#register-form").validate().element('#city_key') &&
            $("#register-form").validate().element('#state') &&
            $("#register-form").validate().element('#contact_number_mobile') &&
            // $("#register-form").validate().element('#spouse_full_name') &&
            // $("#register-form").validate().element('#spouse_nric') &&
            // $("#register-form").validate().element('#spouse_date_of_birth') &&
            // $("#register-form").validate().element('#spouse_occupation') &&
            // $("#register-form").validate().element('#spouse_contact_office') &&
            // $("#register-form").validate().element('#spouse_contact_mobile') &&
            // $("#register-form").validate().element('#spouse_email') &&
            $("#register-form").validate().element('#employment_name') &&
            $("#register-form").validate().element('#company_address_1') &&
            $("#register-form").validate().element('#company_address_2') &&
            $("#register-form").validate().element('#company_postcode') &&
            $("#register-form").validate().element('#company_city') &&
            $("#register-form").validate().element('#company_state')
        ) {
            $('#error-div').hide();
            return true;
        } else {
            $('#error-div').show();
            return false;
        }
    });

    $(document).ready(function() {

        @if ($message == 'success')
            $('#model-notification').modal('show');
            $('.le-btn').click(function () {
               window.location.href = '/';
            });
        @endif

        /* City */
        var cityKey = document.getElementById("city_key");
        var city = document.getElementById("city");
        var classCityKey = document.getElementById("class_city_key");

        // console.log(cityKey.value, city.value, classCityKey.value);
        displayCity();

        $('#city_key').on('change', function( ) {
            displayCity();
        });

        $("#state").on("change",function(){
            var variableID = $(this).val();

            if(variableID){
                $.ajax({
                    type:"POST",
                    url:'{{route("guest.register.customer.state-filter-city")}}',
                    data:$("#register-form").serialize(),
                        success:function(toajax){
                            $("#city_key").html(toajax);
                            displayCity();
                        }
                });
            }

        });
        let city_key = $('#city_key').val();
        $("#state").ready(function() {
            let variableID = $("#state").val();

            if(variableID){
                $.ajax({
                    type:"POST",
                    url:'{{route("guest.register.customer.state-filter-city")}}',
                    data:$("#register-form").serialize(),
                    success:function(toajax){
                        $("#city_key").html(toajax);
                        displayCity();
                    }
                });
            }
        });

        function displayCity(){
            if(cityKey.value === '0'){
                city.style.display = "block";
                classCityKey.className ="form-group col-md-6";
            }else {
                city.style.display = "none";
                classCityKey.className ="form-group col-md-12";
            }
        }
        /* End City */

    });

    $(document).ready(function() {

        /* City */
        var empcityKey = document.getElementById("company_city_key");
        var empcity = document.getElementById("company_city");
        var empclassCityKey = document.getElementById("class_company_city_key");

        // console.log(cityKey.value, city.value, classCityKey.value);
        displayempCity();

        $('#company_city_key').on('change', function( ) {
            displayempCity();
        });

        $("#company_state").on("change",function(){
            var variableID = $(this).val();

            if(variableID){
                $.ajax({
                    type:"POST",
                    url:'{{route("guest.register.customer.state-filter-company-city")}}',
                    data:$("#register-form").serialize(),
                        success:function(toajax){
                            $("#company_city_key").html(toajax);
                            displayempCity();
                        }
                });
            }

        });

        function displayempCity(){
            if(empcityKey.value == '0'){
                empcity.style.display = "block";
                empclassCityKey.className ="form-group col-md-6";
            }else {
                empcity.style.display = "none";
                empclassCityKey.className ="form-group col-md-12";
            }
        }
        /* End City */

        });

        $( "#postcode" ).keyup(function() {
        var $postcode = $('#postcode').val();
        if ($postcode.length == 5) {
            $.ajax({
                url: '/get-postcode',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "postcode": $postcode
                },
                beforeSend: function () {},
                success: function (success) {
                    console.log(success.data);
                    if (success.data == false) {
                        // $('#exampleFormControlInput2').attr('style','border:red;')
                        // alert('false');
                        $('.ajaxMessage').show().html('Invalid Postcode');
                        return false;
                    }else{
                        $('.ajaxMessage').hide().html('');
                    $('#cart-form').submit();

                    }

                },
                error: function () {
                    alert("error in loading");
                }

            });
        }
    });

</script>
@endpush
