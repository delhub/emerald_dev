<?php

namespace App\Http\Controllers\Purchase;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Models\Purchases\Payment;
use App\Models\Users\User;
use App\Models\Users\UserConnectedPlatform;

class MetapointGatewayController extends Controller
{
    public function submitForm($request, $purchase, $user, $paymentOption)
    {
        $payment_amount = $purchase->purchase_amount / 100;

        $secret_key = config('app.metapoint_secrect_key');
        $unique_id =  uniqid();

        $metapoint = array();
        $metapoint['redirect_link'] = URL::to('/payment/gateway-response?process=metapoint');
        $metapoint['access_key'] = env('ACCESS_KEY', '38c31ee8397d33a799fef7b79b2f5307');
        // $metapoint['account_id'] = $user->userInfo->account_id;
        $metapoint['requested_uuid'] = $purchase->purchase_number . '-' .  $unique_id;
        $metapoint['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
        $metapoint['transaction_type'] = 'sale';
        $metapoint['reference_number'] = $purchase->purchase_number;
        $metapoint['amount'] = $payment_amount * 100;
        $metapoint['platform'] = config('app.metapoint_platform');
        $metapoint['_token'] = $request->_token;

        $userPlatform = UserConnectedPlatform::where('user_id', $user->id)->where('platform_key', 'META')->first();

        $metapoint['meta_account_id'] = $userPlatform->account_id;
        $metapoint['meta_user_id'] = $request->meta_user_id;

        $metapoint['signature_data'] =  $metapoint['requested_uuid'] . $metapoint['signed_date_time'] . $metapoint['amount'] . $metapoint['meta_account_id'] . $metapoint['meta_user_id'] . $secret_key;
        $metapoint['signature'] = $this->sign($metapoint, $secret_key);

        $payment = new Payment;

        $payment->transaction_id = $unique_id;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->purchase_uuid = $purchase->purchase_number . '-' . $unique_id;
        $payment->gateway_response_code = 'PENDING';
        $payment->amount =  ($purchase->installment_data) ? $payment_amount * 100 : $purchase->purchase_amount;
        $payment->local_amount =  (country()->country_id == 'SG') ? round($payment_amount * 100) : 0;
        $payment->gateway_string_result = '-';
        $payment->auth_code = '-';
        $payment->last_4_card_number = '-';
        $payment->expiry_date = '-';
        $payment->gateway_eci = '-';
        $payment->gateway_security_key_res = '-';
        $payment->gateway_hash = '-';

        $payment->save();

        $logdata = $metapoint;
        Log::channel('metapoint')->info($logdata);

        return view('shop.payment.metapoint.post-to-gateway-metapoint')
            ->with('metapoint', $metapoint);
    }

    public function gatewayResponse($request)
    {
        foreach ($request->request as $name => $value) {
            $params[$name] = $value;
        }

        Log::channel('metapoint')->info($params);
        $paymentGatewayResponse = new PaymentGatewayController;

        $payment = Payment::where('purchase_uuid', $request->input('requested_uuid'))->first();

        $errorMessage = array();
        $errorMessage['id'] = 0;
        $errorMessage['response_code'] = 'FAILED';

        if (!isset($payment)) {

            $errorMessage['description'] = "Payment Not Found.";

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $purchase = $payment->purchase;

        if (in_array($payment->gateway_response_code, ['00', '01', '02', 'FAILED'])) {

            $errorMessage['description'] = "Duplicated Payment Detected.";

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $response = $request->input('response_code');

        $errorMessage = $this->processMetaPointPayment($request, $purchase,  $payment);
        if ($response != '00') {

            $payment->gateway_response_code = $response;
            $payment->save();

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }
        // Hash Checking
        $secret_key = config('app.metapoint_secrect_key');
        $sg['signature_data'] =  $request->input('requested_uuid') . $request->input('signed_date_time') . $request->input('response_code') . $request->input('transaction_id') . $request->input('auth_code') . $request->input('amount') . $secret_key;

        if (strcmp($params["signature"], $this->sign($sg, $secret_key)) != 0) {
            // The password  dont match...
            $errorMessage['description'] = "Hash Validation Failed.";

            $payment->gateway_response_code = 'FAILED';
            $payment->save();

            $purchase->purchase_status = 4006;
            $purchase->save();

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $user = User::find($purchase->user_id);

        $paymentGatewayResponse->updateGroupSales($user, $purchase);

        $check_registration = true;

        $purchase->increment('invoice_version');

        $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration, false, true);

        GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);

        return $paymentGatewayResponse->markOrderComplete($purchase, $user);
    }

    public function processMetaPointPayment($request, $purchase, $payment)
    {
        if (($request->input('response_code')) && ($request->input('response_code') == '00')) {
            $payment->gateway_string_result = $request->input('transaction_id');
            $payment->gateway_response_code = $request->input('response_code');
            $payment->auth_code = $request->input('auth_code');
            $payment->last_4_card_number = substr($request->input('req_card_number'), -4);
            $payment->expiry_date = substr($request->input('req_card_expiry_date'), 0, 2) . substr($request->input('req_card_expiry_date'), -2);
            $payment->amount = $request->input('amount');
            $payment->gateway_eci = '-';
            $payment->gateway_security_key_res = $request->input('signature');
            $payment->gateway_hash = $request->input('signature');

            $payment->save();

            $response['response_code'] =  $request->input('response_code');
            $response['description'] = 'Successful transaction '  . $request->input('signature');

            if (in_array($purchase->payment_type, ['7003', '7004'])) {
                $purchase->purchase_status = 4007;
            } else {
                $purchase->purchase_status = 4002;
            }
        } else {

            $response['response_code'] = $request->input('response_code');
            $response['description'] =  $request->input('response_detail');

            $payment->gateway_response_code = $response['response_code'];
            $payment->save();

            $purchase->purchase_status = 4006;
        }

        $purchase->save();

        Log::channel('transactions')->info($payment);
        return $response;
    }

    public function sign($params, $secret_key)
    {
        return base64_encode(hash_hmac('sha256', $params['signature_data'], $secret_key, true));
    }
}
