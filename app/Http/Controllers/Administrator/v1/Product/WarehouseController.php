<?php

namespace App\Http\Controllers\Administrator\v1\Product;

use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Products\WarehouseBatch;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Products\ProductAttribute;
use App\Models\Users\User;
use Illuminate\Support\Facades\Auth;
use stdClass;
use App\Models\Warehouse\Location\Wh_location;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use PDF;

set_time_limit(300);
class WarehouseController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $batchs = $this->getBatch();
        if (!$user->hasRole('wh_supervisor'))  $batchs = $batchs->whereIn('location_id', $userLocationId);

        $total = COUNT($batchs);
        $batch = $this->paginate($batchs, 10);
        $batch->setPath('');

        return view('administrator.products.v1.warehouse.index')
            ->with('total', $total)
            ->with('batch', $batch);
    }

    public function getBatch()
    {
        // $limit = isset($request->limit) ? $request->limit : 50;
        // $batch = WarehouseBatch::orderBy('id', 'DESC')->limit(50)->get();
        return $batch = WarehouseBatch::orderBy('id', 'DESC')->get();

        // if (count($batch)) {
        //     foreach ($batch as $val) {
        //         $val->dtl = $val->details;
        //         $val->panel = PanelInfo::where('account_id', $val->panel_id)->first();
        //         foreach ($val->dtl as $v) {
        //             $v->item = WarehouseBatchItem::where('detail_id', $v->id)->count();
        //         }

        //         $data[] = $val;
        //     }
        //     return $data;
        // } else {
        //     return [];
        // }
    }

    public function label()
    {
        $panels = PanelInfo::all();
        $attributes = ProductAttribute::orderBy('product_code', 'ASC')->get();

        $batch = WarehouseBatch::orderBy('id', 'DESC')->get();

        return view('administrator.products.v1.warehouse.label')
            ->with('batchs', $batch)
            ->with('products', $attributes)
            ->with('panels', $panels);
    }

    public function reprintQrItem(Request $request)
    {
        if (isset($request->batchItem)) {

            return view('documents.qrcode-reprint')
                ->with('items', $request->batchItem)
                ->with('request', $request)
                ->with('pageTitle', "Reprint QR Item");
        } else {
            $batch = WarehouseBatchItem::all();

            return view('administrator.products.v1.warehouse.reprint-qr-item')
                ->with('batch', $batch);
        }
    }

    public function printLabel(Request $request)
    {

        /* if(count($request->product_code)) {
            foreach ($request->product_code as $k=>$a) {
                $data[$k]['product_code'] = $a;
                foreach ($request->quantity as $k=>$b) {
                    $data[$k]['quantity'] = $b;
                }
            }
        } */

        $collection = collect($request)->forget(['_token', 'submit'])->toArray();

        $detail = base64_encode(\json_encode($collection));

        return view('documents.qrcode-label')
            ->with('detail', $detail)
            ->with('request', $request)
            ->with('products', $request->product_code)
            ->with('quantity', $request->quantity)
            ->with('pageTitle', $request->batch);
    }

    public function labelInfo($data)
    {

        $info = json_decode(base64_decode($data));

        return view('documents.label-info')
            ->with('pageTitle', $info->batch)
            ->with('products', $info->product_code)
            ->with('quantity', $info->quantity)
            ->with('info', $info);
    }

    public function newBatch()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $locations = wh_location::orderBy('location_name', 'desc');

        if (in_array(1, $userLocationId)) {
            $locations = $locations->orWhere('id', 999)->orWhereIn('id', $userLocationId)->get();
        } else {
            $locations = $locations->whereIn('id', $userLocationId)->get();
        }

        $panels = PanelInfo::all();
        $attribute = ProductAttribute::selectRaw('panel_product_attributes.*')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->orderBy('panel_product_attributes.product_code', 'ASC')
            ->where('panel_product_attributes.active', 1)
            ->where('global_products.product_status', 1)
            ->where('global_products.no_stockProduct', 0)
            ->get();

        return view('administrator.products.v1.warehouse.new-batch')
            ->with('panels', $panels)
            ->with('locations', $locations)
            ->with('products', $attribute);
    }

    public static function addBatch(Request $request)
    {
        foreach ($request->product_code as $k => $a) {
            $details[$k]['product_code'] = $a;
        }
        foreach ($request->quantity as $k => $b) {
            $details[$k]['quantity'] = $b;
        }
        foreach ($request->repeatable as $k => $c) {
            $details[$k]['repeatable'] = $c;
        }
        foreach ($request->lot as $k => $e) {
            $details[$k]['lot'] = $e;
        }
        foreach ($request->expiry_date as $k => $d) {
                $details[$k]['expiry_date'] = $d;
        }

        $data = new stdClass;

        $data->panel_id = $request->panel_id;
        $panel = PanelInfo::where('account_id', $data->panel_id)->first();
        $panel_short_id = substr($panel->company_name, 0, 2);

        /* $exBatch_id = WarehouseBatch::where('batch_id', 'like', '%' . $panel_short_id . '%')->orderBy('id', 'DESC')->first();

        if ($exBatch_id) {
            $newNum = substr($exBatch_id->batch_id, -4) * 1;
            $newNum++;

            $data->batch_id = strtoupper($panel_short_id) . sprintf('%04d', $newNum);
        } else {
            $data->batch_id = strtoupper($panel_short_id) . sprintf('%04d', 1);
        } */

        $data->batch_id = str_replace(["-", "_", " "], [".", ".", "."], $request->batch);

        $data->stock_in_date = Carbon::parse($request->stock_in_date)->format('Y-m-d');
        $data->po_number = isset($request->po_number) ? $request->po_number : null;
        $data->inv_number = isset($request->inv_number) ? $request->inv_number : null;
        $data->inv_date = isset($request->inv_date) ? Carbon::parse($request->inv_date)->format('Y-m-d') : null;
        $data->vendor_desc = $request->vendor_desc;
        $data->batch_type  = $request->batch_type;
        $data->notes  = $request->notes;


        if (self::insertWarehouseBatch($data, $request->location_id)) {
            self::addWarehouseBatchDetails($details, $data->batch_id, $request->location_id);
        }

        return redirect()->route('administrator.v1.products.warehouse.newBatch')
            ->with('success', 'Batch data has been successfully added...');
    }

    public static function insertWarehouseBatch($data, $location_id)
    {
        $result = new WarehouseBatch;

        $result->batch_id = $data->batch_id;
        $result->panel_id = $data->panel_id;
        $result->po_number = $data->po_number;
        $result->inv_number = $data->inv_number;
        $result->inv_date = $data->inv_date;
        $result->vendor_desc = $data->vendor_desc;
        $result->batch_type = $data->batch_type;
        $result->location_id = $location_id;
        $result->notes = $data->notes;
        $result->stock_in_date = $data->stock_in_date;

        $result->save();

        return true;
    }

    public static function insertWarehouseBatchDetails($data, $batch_id, $location_id)
    {
        if (Auth::user()) {
            $user_id = User::find(Auth::user()->id)->id;
        } else {
            $user_id = 0;
        }

        $result = new WarehouseBatchDetail;

        $result->batch_id = $batch_id;
        $result->product_code = $data['product_code'];
        $result->lot = $data['lot'];
        $result->expiry_date = $data['expiry_date'];
        $result->save();

        $inventoryRecords[$data['product_code']]['outlet_id'] = $location_id;
        $inventoryRecords[$data['product_code']]['inv_type'] = 8010;
        $inventoryRecords[$data['product_code']]['inv_amount'] =  $data['quantity'];
        $inventoryRecords[$data['product_code']]['inv_user'] = $user_id;
        $inventoryRecords[$data['product_code']]['model_type'] = 'App\Models\Products\WarehouseBatchDetail';
        $inventoryRecords[$data['product_code']]['model_id'] = $result->id;
        $inventoryRecords[$data['product_code']]['product_code'] = $data['product_code'];
        $inventoryRecords[$data['product_code']]['inv_reference'] = null;

        $batch = WarehouseBatch::where('batch_id', $batch_id)->orderBy('created_at', 'DESC')->first();
        if (in_array($batch->batch_type, ['stock_in'])) {
            InventoryController::insertData($inventoryRecords);
        }


        for ($i = 1; $i <= $data['quantity']; $i++) {
            self::insertWarehouseBatchItems($batch_id, $result->id, $i, $data, $location_id);
        }

        return true;
    }

    public static function addWarehouseBatchDetails($data, $batch_id, $location_id)
    {
        if (count($data)) {
            foreach ($data as $key => $val) {
                self::insertWarehouseBatchDetails($val, $batch_id, $location_id);
            }
        }
    }

    public static function insertWarehouseBatchItems($batch_id, $detail_id, $i, $data, $location_id)
    {
        $result = new WarehouseBatchItem;

        $result->items_id = $batch_id . '-' . sprintf('%04d', $detail_id) . '-' . sprintf('%04d', $i);
        $result->batch_id = $batch_id;
        $result->detail_id = $detail_id;
        $result->location_id = $location_id;
        $result->shelf_id = 1;
        $result->repeatable = $data['repeatable'];

        $result->save();
    }

    public function qrBatch($id, Request $request)
    {
        $userLocationId = Auth::user()->user_warehouse_location;
        $batch = WarehouseBatchItem::where('batch_id', $id)->whereIn('location_id', $userLocationId)->get();

        return view('documents.qrcode-batch')
            ->with('batch', $batch)
            ->with('request', $request)
            ->with('pageTitle', $id);
    }

    public function qrBatch_v2($id, Request $request)
    {
        $userLocationId = Auth::user()->user_warehouse_location;
        $batch = WarehouseBatchItem::where('batch_id', $id)->whereIn('location_id', $userLocationId)->get();

        if ($request->html == 1) {
            return view('documents.qrcode-batch-v2')
                ->with('batch', $batch)
                ->with('request', $request)
                ->with('id', $id);
        } elseif ($request->download == 1) {

            $pdf = PDF::loadView('documents.qrcode-batch-v2', compact(['batch', 'request', 'id']))
                ->setOption('margin-top', '20mm')
                ->setOption('margin-bottom', '20mm')
                ->setPaper('A4');/*
                ->stream($id . '.pdf', true); */

            return $pdf->download($id . '.pdf');
        } else {
            return PDF::loadView('documents.qrcode-batch-v2', compact(['batch', 'request', 'id']))
                ->setOption('margin-top', '20mm')
                ->setOption('margin-bottom', '20mm')
                ->setPaper('A4')
                ->stream($id . '.pdf', true);
        }
    }

    public function qrBatch_v3($id, Request $request)
    {
        $userLocationId = Auth::user()->user_warehouse_location;
        $batch = WarehouseBatchItem::where('batch_id', $id)->whereIn('location_id', $userLocationId)->get()->groupBy('detail_id');
        $batchs = [];

        foreach ($batch as $itemBatch) {
            foreach ($itemBatch as $key => $item) {
                if ($key == 0) $batchs[] = $item->batchDetail->product_code;
                $batchs[] = $item;
            }
        }

        return PDF::loadView('documents.qrcode-batch-v3', compact(['batch', 'request', 'id', 'batchs']))
            // ->setOption('margin-top', '8mm')
            // ->setOption('margin-bottom', '8mm')
            ->setPaper('A4')
            ->stream($id . '.pdf', true);
    }

    public function qrBatch_v4($id, Request $request)
    {
        $userLocationId = Auth::user()->user_warehouse_location;
        $batch = WarehouseBatchItem::where('batch_id', $id)->whereIn('location_id', $userLocationId)->get()->groupBy('detail_id');
        $batchs = [];

        foreach ($batch as $itemBatch) {
            foreach ($itemBatch as $key => $item) {
                if ($key == 0) $batchs[] = $item->batchDetail->product_code;
                $batchs[] = $item;
            }
        }

        return PDF::loadView('documents.qrcode-batch-v4', compact(['batch', 'request', 'id', 'batchs']))
            // ->setOption('margin-top', '8mm')
            // ->setOption('margin-bottom', '8mm')
            ->setPaper('A4')
            ->stream($id . '.pdf', true);
    }

    // public function batchItemList(Request $request)
    // {
    //     $user = Auth::user();
    //     $userLocationId = $user->user_warehouse_location;

    //     $table = WarehouseBatchItem::selectRaw('wh_batch_item.*')

    //         ->orderBy('batch_id', 'ASC')
    //         ->orderBy('detail_id', 'ASC')
    //         ->searchFilter($request->searchBox, $request->selectSearch)
    //         ->locationFilter($request->location_id)

    //         ->get();
    //     if (!$user->hasRole('wh_supervisor')) $table = $table->whereIn('location_id', $userLocationId);

    //     $tables = $this->paginate($table, 1000);
    //     $tables->setPath('');
    //     $tables->appends(request()->query());

    //     $locations = wh_location::whereIn('id', $userLocationId)->get();


    //     return view('administrator.products.v1.warehouse.batch-item-list')
    //         ->with('tables', $tables)
    //         ->with('locations', $locations)
    //         ->with('request', $request);
    // }

    public function batchItemList(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $query = WarehouseBatchItem::with([
            'batchDetail.productAttribute.product2.parentProduct',
            'batchDetail.productAttribute',
            'location'
        ])
        ->select('wh_batch_item.*')
        ->orderBy('wh_batch_item.batch_id', 'ASC')
        ->orderBy('wh_batch_item.detail_id', 'ASC')
        ->locationFilter($request->location_id);

        if ($request->selectSearch && $request->searchBox) {
            $searchFilter = $request->searchBox;
            $selectSearch = $request->selectSearch;
            $query->searchFilter($searchFilter, $selectSearch);
        }

        // if (!$user->hasRole('wh_supervisor')) {
        //     $query->whereIn('location_id', $userLocationId);
        // }

        $tables = $query->paginate(100);
        $tables->setPath('');
        $tables->appends(request()->query());

        $locations = wh_location::whereIn('id', $userLocationId)->get();

        return view('administrator.products.v1.warehouse.batch-item-list')
            ->with('tables', $tables)
            ->with('locations', $locations)
            ->with('request', $request);
    }

    // pagination array
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function editNotes(Request $request)
    {
        $batch = WarehouseBatch::where('id', $request->batchID)->first();
        $batch->notes = $request->notes;
        $batch->save();

        return true;
    }

    public function batchExpiry(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $query = WarehouseBatchDetail::with([
            'mainBatch.location',
            'productAttribute.product2.parentProduct',
            'unusedItems'
        ])
        ->select('wh_batch_detail.*')
        ->orderBy('wh_batch_detail.expiry_date', 'ASC')
        ->locationFilter($request->location_id)
        ->whereHas('unusedItems');

        $showAllData = false;

        if (!$request->has('showAllData')) {
            $query->where('wh_batch_detail.expiry_date', '!=', '0000-00-00');
            $showAllData = true;
        }

        if ($request->selectSearch && $request->searchBox) {
            $searchFilter = $request->searchBox;
            $selectSearch = $request->selectSearch;

            $query->searchFilter($searchFilter, $selectSearch);
        }

        if ($request->selectSearch === 'months_left' && $request->monthsLeft !== null) {
            $monthsLeft = intval($request->monthsLeft);

            $query->monthsLeftFilter($monthsLeft);
        }

        $totalItem = count($query->get());
        $tables = $query->paginate(50);
        $tables->setPath('');
        $tables->appends(request()->query());

        $locations = wh_location::get();

        $currentPage = $tables->currentPage();
        $perPage = $tables->perPage();
        $listNumber = ($currentPage - 1) * $perPage + 1;

        foreach ($tables as $row) {
            $expiryDate = Carbon::parse($row->expiry_date);
            $today = Carbon::now();

            $daysDifference = $expiryDate->diffInDays($today);
            $monthsDifference = $today->diffInMonths($expiryDate);

            if ($expiryDate->isPast()) {
                $row->expiryClass = 'expired';
            } else {
                $monthsDifference = $expiryDate->diffInMonths($today);
                if ($monthsDifference < 3) {
                    $row->expiryClass = 'less-than-3';
                } elseif ($monthsDifference < 6) {
                    $row->expiryClass = 'less-than-6';
                } elseif ($monthsDifference < 9) {
                    $row->expiryClass = 'less-than-9';
                } elseif ($monthsDifference < 12) {
                    $row->expiryClass = 'less-than-12';
                } else {
                    $row->expiryClass = '';
                }
            }

            $row->daydiff = $daysDifference;
            $row->mondiff = $monthsDifference;

        }

        return view('administrator.products.v1.warehouse.batch-expiry')
            ->with('tables', $tables)
            ->with('locations', $locations)
            ->with('request', $request)
            ->with('listNumber', $listNumber)
            ->with('showAllData', $showAllData)
            ->with('totalItem', $totalItem);
    }

    public function getBatchInfo($batch_id)
    {
        $batch = WarehouseBatch::where('batch_id', $batch_id)->first();
        if ($batch) {
            return response()->json([
                'batch_type' => $batch->batch_type,
                'panel_name' => $batch->panel->company_name,
                'po_number' => $batch->po_number,
                'invoice_date' => date('d/m/Y', strtotime($batch->inv_date)),
                'invoice_number' => $batch->inv_number,
                'vendor_name' => $batch->vendor_desc,
                'stock_in_date' => date('d/m/Y', strtotime($batch->stock_in_date))
            ]);
        }
        return response()->json(['error' => 'Batch not found'], 404);
    }
}
