<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.favicon')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('style')

    <style>
        /* Make sure the background image covers the screen */
        html {
            height: 100%;
        }

        body .bg-md {
            height: 100%;
        }

        .remember-check {
            width: 15px;
            height: 15px;
            cursor: pointer;
        }
    </style>
</head>

@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp

<body>

    <div class="bg-md bg-sm ">
        <div class="row">
            <div class="col-6 offset-3 col-md-4 offset-md-4 text-center">
                <a href="/">
                    <img class="mw-100 w-50-md" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
                </a>
            </div>
        </div>
        <div class="">
            <div class="card border-rounded-0 bg-white mt-4 guests-card"
                style="border:1px solid #ffcc00;border-radius:5px;">
                <div class="text-center m-4">
                    <button onclick="window.location.href='/shop';" type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <br>
                    <h3><b>Thank you for applying DC membership</b></h3>
                    <p>Our agent will contact you soon</p>
                    <button onclick="window.location.href='/shop';"
                        class="btn next-button bjsh-btn-gradient text-right">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>