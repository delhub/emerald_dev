<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRbsStatusInGlobalRbsReminder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_rbs_reminder', function (Blueprint $table) {
            $table->string('rbs_status')->nullable()->after('rbs_month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_rbs_reminder', function (Blueprint $table) {
            $table->dropColumn('rbs_status');
        });
    }
}
