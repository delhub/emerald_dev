@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<h1>Warehouse Inventory</h1>
<div class="card shadow-sm">
    <div class="card-header">Inventory<small></small>
        {{-- <div class="card-header-actions">
            <button class="btn btn-primary btn-sm" type="button" onclick="window.location.href = '{{ route('administrator.v1.products.warehouse.index')}}';">
        Batch List
        </button>
    </div> --}}
</div>
<div class="card-body">
    <div class="row">
        <div class="col-md-7  mb-3 row">
            <form action="{{ route('administrator.warehouse.inventory.index') }}" method="GET" class="col-md-6">
                <div class="row">
                    <div class="col-10 pr-0">
                        <input type="text" class="form-control d-inline" id="searchBox" name="search_box"
                            placeholder="Product Code / Name"
                            value="{{ ($request->search_box) ?? '' }}">
                    </div>
                    <div class="col-2 px-0">
                        <button type="submit" class="btn btn-outline-primary d-inline"
                            style="color:black;">Search</button>
                    </div>
                    <div class="col-10 pr-0">
                        <select name="location_id" class="form-control d-inline">
                            <option value="all">All Location</option>
                            @foreach ($locations as $location)
                            <option value="{{ $location->id }}" {{ $request->location_id == $location->id ? 'selected' : ''}}>
                                {{ $location->location_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                {{-- <div class="row">
                        <div class="input-group col-md-4 p-0 ml-auto ">
                            <div class="input-group-append col-md-5 p-0">

                            </div>
                            <div class="input-group-append col-md-2 p-0">

                            </div>
                        </div>
                    </div> --}}
            </form>
            <form action="{{ route('administrator.warehouse.inventory.index') }}" method="GET" class="pr-2">
                <button type="submit" class="btn btn-outline-primary" style="color:black;">Reset</button>
            {{-- <form action="{{ route('warehouse-download', ['location_id'=> $request->location_id,'search_box'=> $request->search_box])}}" method="GET" class="pl-0 col-md-2">
                @csrf
                <input type="hidden" name="data" value="{{$productStocks}}"/>
                <input type="submit" class="btn btn-outline-primary" style="color:black;" value ="Download Report"/>
            </form> --}}
            <a href="{{ route('warehouse-download', ['location_id'=> $request->location_id,'search_box'=> $request->search_box])}}"
                class="btn btn-outline-primary" style="color:black;" >Download Report</a>
        </div>
        <div class="col-auto ml-auto">
            {{ $viewProductStocks->links() }}
        </div>
    </div>
    <table class="table table-sm table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col" class="align-middle">#</th>
                <th scope="col" class="align-middle">Location</th>
                <th scope="col" class="align-middle">Product Code</th>
                <th scope="col" class="align-middle">Product Name</th>
                <th scope="col" class="align-middle">Product Attribute</th>
                <th scope="col" class="align-middle">Physical Stock</th>
                <th scope="col" class="align-middle">Virtual Stock (Reserved)</th>
                {{-- <th scope="col" class="align-middle">Virtual Stock (Reserved Offline)</th> --}}
                <th scope="col" class="align-middle">Virtual Stock</th>
                <th scope="col" class="align-middle">Retail Sold</th>
                <th scope="col" class="align-middle">Online Sold</th>
                <th scope="col" class="align-middle">Total Sold</th>
                <th scope="col" class="align-middle">Details</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($viewProductStocks as $key => $viewProductStock)
            <tr>
                <th scope="row" class="text-center">{{ ($viewProductStocks->currentpage()-1)*$viewProductStocks->perpage()+$loop->index+1 }}</th>
                <td>{{ $viewProductStock->warehouseLocation->location_name }}</td>
                <td>{{ $viewProductStock->product_code }}</td>
                <td>{{ $viewProductStock->global_product_name }}</td>
                <td>{{ $viewProductStock->product_name }}</td>
                <td class="text-danger">{{ $viewProductStock->physical_stock }}</td>
                <td class="text-danger">{{ $viewProductStock->virtual_stock_reserved }}</td>
                {{-- <td class="text-danger">{{ $viewProductStock->virtual_stock_reserved_offline }}</td> --}}
                <td class="text-success">{{ $viewProductStock->virtual_stock }}</td>
                <td class="text-danger">{{ $viewProductStock->pos_completed }}</td>
                <td class="text-danger">{{ $viewProductStock->online_sold }}</td>
                <td class="text-danger">{{ $viewProductStock->total_sold }}</td>
                <td class="text-center">
                    {{-- <button type="button" class="btn btn-info">Details</button> --}}
                    <form action="{{ route('administrator.warehouse.inventory.details',['product_code' => $viewProductStock->product_code]) }}">
                        <button type="submit" class="btn btn-primary">Details</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-auto ml-auto">
            {{ $viewProductStocks->appends($_GET)->links() }}
        </div>
    </div>
</div>
</div>
@endsection


