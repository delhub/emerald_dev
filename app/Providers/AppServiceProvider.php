<?php

namespace App\Providers;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use DB;
use Log;
use App;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('dd', function ($expression) {
            return "<?php dd({$expression}); ?>";
        });


        Blade::if('dealershipcheck', function () {
            if (!Auth::check()) {
                return false;
            }

            $user = Auth::user();
            $show = 0;

            if ($user->hasRole(['dealer']) ) {
                $show = ($user->dealerInfo->is_dealership) ? 1 : 0;
            } else {
                $show = 0;
            }


            return $show;
        });

        Blade::directive('enddealershipcheck', function ($expression) {

            return "<?php endif; ?>";
        });


        /**
         * Overrides default email after user registration.
         */
        VerifyEmail::toMailUsing(function ($notifiable) {
            $verifyUrl = URL::temporarySignedRoute(
                'verification.verify',
                Carbon::now()->addMinutes(60),
                ['id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification())
                ]
            );

            return (new MailMessage)
                ->subject('Welcome!')
                ->markdown('emails.registrations.welcome-and-verify', ['url' => $verifyUrl, 'user' => $notifiable]);
        });

        if (!App::environment('prod'))
        DB::listen(function($query) {
            /* Log::channel('sql')->info(
                $query->sql,
                $query->bindings,
                $query->time
            ); */
        });

        Blade::directive('dc_credit_url', function ($expression) {

            return"https://duitpremium.com/apply/?la=<?php echo {$expression}->installment_data->fullamount ?>&ip=<?php echo {$expression}->installment_data->tenure ?>&mp=<?php echo {$expression}->installment_data->monthlypayment ?>&ir=<?php echo {$expression}->installment_data->rate ?>&pn=<?php echo urlencode({$expression}->installment_data->productdescription) ?>+<?php echo {$expression}->getFormattedNumber() ?>&ft=pf";
        });

    }
}
