<?php

namespace App\Http\Controllers\Administrator\ZoneLocations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShippingInstallations\Zones;
use App\Models\Globals\City;
use App\Models\ShippingInstallations\Locations;
use Illuminate\Support\Facades\Validator;
use DB;

class ZonesLocationController extends Controller
{

    public function index(Request $request) {
        $zone_locations = Locations::orderBy('id')
        ->searchFilter($request->searchBox, $request->selectSearch)
        ->paginate(10);

        $cities = City::all();

        return view('administrator.zone_location.index', compact('zones','zone_locations'))
        ->with('request', $request);
    }

    public function edit($id)
    {
        $zone_locations = Locations::find($id);

        $zones = Zones::all();

        $cities = City::all();

        return view('administrator.zone_location.edit')
        ->with('zone_locations', $zone_locations)
        ->with('cities', $cities)
        ->with('zones', $zones);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'zone_id' => 'required',
            'location_type' => 'required',
            'location_id' => 'required'
        ]);

        $zone_locations = Locations::find($id);

        $zone_locations->zone_id = $request->input('zone_id');
        $zone_locations->location_type = $request->input('location_type');
        $zone_locations->location_id = $request->input('location_id');

        $zone_locations->save();

        return redirect()->route('administrator.zone_location.index')->with('success', 'Zones Updates.');
    }
}
