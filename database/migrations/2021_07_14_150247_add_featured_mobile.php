<?php

use App\Models\Categories\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFeaturedMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('featured_mobile')->default(0)->after('featured');
        });
        $categories = Category::all();

        $slug1 = [
            'energy-multi-vitamins-and-nutrition',
            'immunity-care',
            'vision-care',
            'skin-care',
            'stomach-and-guts-care',
            'children-care',
            'infant-care',
            'hair',
            'body-wash',
        ];

        $slug2 = [
            'oral-care',
            'facial',
            'body-protection',
            'intimate-wash',
            'men-care',
            'women-care',
            'cholesterol',
            'blood-pressure',
            'blood-sugar',
        ];

        $slug3 = [
            'mouth-and-throat-care',
            'brain-care',
            'heart-care',
            'lung-and-respiratory-care',
            'bone-and-joint-care',
            'liver-care',
            'kidney-care',
            'muscle-care',
            'anti-oxidant-anti-inflammation',
        ];
        $slug4 = [
            'relaxing-sleeping',
            'weight-management',
            'cosmetic'
        ];

        foreach ($categories as $category) {
            if (in_array($category->slug,$slug1)) {
                $category->featured_mobile = 1;
                $category->save();
            }
            if (in_array($category->slug,$slug2)) {
                $category->featured_mobile = 2;
                $category->save();
            }
            if (in_array($category->slug,$slug3)) {
                $category->featured_mobile = 3;
                $category->save();
            }
            if (in_array($category->slug,$slug4)) {
                $category->featured_mobile = 4;
                $category->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
