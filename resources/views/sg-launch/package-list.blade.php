@extends('layouts.guest.main')

@section('content')


<div class="bg-md mainbg">

    <div class="row">
        <div class="col-6 offset-3 col-lg-2 offset-lg-5 mb-0 pt-2 pb-3">
            <div class="mw-100"></div>
        </div>
    </div>
    <div>

        <div class="my-register-box my-guests-card">
            <div class="my-r-title-box">
                <h5><b>AGENT PACKAGE</b></h5>
            </div>

            <div class="card-body ">
                <!-- Dealer Registration Form -->
                <form method="POST" action="{{ route('sg.launch.buyNow') }}" id="sg-launch-form"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content r-inner-box" id="myTabContent">
                        <!-- Registration  Tab-->
                        <div class="r-subt-box tab-pane fade show active" id="registration" role="tabpanel"
                            aria-labelledby="registration-tab">

                            @foreach ($packages as $package)
                            <div class="form-row border p-lg-2 pt-2 my-1">
                                <div class="col-lg-11 mx-lg-auto col-11 pl-4 mx-auto">
                                    <input class="form-check-input" type="radio" name="panelProduct"
                                        id="package{{ $package->panelProduct->id }}"
                                        value="{{ $package->panelProduct->id }}" style="transform: scale(1.2)" checked>
                                    <label class="form-check-label" for="package{{ $package->panelProduct->id }}">
                                        {{ $package->name }}
                                    </label>

                                </div>
                                <div class="col-lg-3 m-lg-2 pt-lg-2 col-4 p-2">
                                    <label class="form-check-label" for="package{{ $package->panelProduct->id }}">
                                        <img class=" img-thumbnail"
                                            src="{{ asset('storage/' . $package->defaultImage->path. '' . $package->defaultImage->filename) }}"
                                            alt="{{ $package->name }}">
                                    </label>
                                </div>
                                <div class="col-lg-6 col-8 p-2">
                                    <label class="form-check-label" for="package{{ $package->panelProduct->id }}">
                                        @php
                                        \Log::info($package->panelProduct->attributes->first()->getPriceAttributes('price'));
                                        @endphp
                                        <h4>{{ $currency . ' ' . number_format((($userStatus == 'dc' ? $package->panelProduct->attributes->first()->getPriceAttributes('fixed_price') : $attributes->first()->getPriceAttributes('fixed_price')) / 100),2) }}
                                        </h4>
                                    </label>
                                    <br>
                                    {{-- <label class="form-check-label" for="package{{ $package->id }}"> --}}
                                        <h5>{!! $package->panelProduct->product_description !!}</h5>
                                    {{-- </label> --}}

                                </div>
                            </div>

                            @endforeach
                        </div>
                        {{-- <div class="r-btn-opt"> --}}
                        <div class="edit-opt-btn" style="text-align: right;">
                            <button type="submit" class="cancel-btn btn-primary"><b>Buy Package</b></button>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

@endsection

@push('style')
<style>
    .edit-opt-btn .cancel-btn {
        background: linear-gradient(89.82deg, #ff757c 0.13%, #ef3842 99.82%);
        border-radius: 40px;
        width: auto;
        height: auto;
        padding: 16px 16px;
        color: #fff;
        text-align: center;
        line-height: 10px;
        font-weight: 500;
        font-size: 14px;
    }
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .error {
        color: red;
    }
</style>
@endpush
