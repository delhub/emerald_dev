<?php

namespace App\Models\Products;

use App\Models\Purchases\Order;
use App\Traits\TableLogs;
use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse\Order\WarehouseOrder;
class WarehouseInventories extends Model
{
    use TableLogs;
    // Set table
    protected $table = 'wh_inventories';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'order_key',
        'outlet_id',
        'inv_type',
        'inv_amount',
        'inv_user',
        'repeatable',
        'model_type',
        'model_id',
        'product_code',
        'inv_reference'
    ];

    public function scopeinventoryOutletFilter($query, $outletFilter)
    {
        if ($outletFilter != null) {
            $query->where('outlet_id', $outletFilter);
        }
        return $query;
    }

    public function scopeinventoryStatusFilter($query, $statusFilter)
    {
        if ($statusFilter != null) {
            $query->where('inv_type', $statusFilter);
        }
        return $query;
    }

    public function inventoryDetails()
    {
        //dd($this->model_type);
        return $this->belongsTo($this->model_type, 'model_id', 'id');
    }

    public function productAttributes()
    {
        return $this->belongsTo('App\Models\Products\ProductAttribute', 'product_code', 'product_code');
    }

    public function order(){

        return $this->belongsTo(WarehouseOrder::class, 'model_id', 'id');
    }

    // public function inventoryDetailsOrder()
    // {
    //     //dd($this->model_type);
    //     return $this->belongsTo(Order::class, 'model_id', 'id');
    // }
}
