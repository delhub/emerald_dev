<?php

namespace App\Models\Products;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Products\ProductAttribute;

class ProductAttributeLimit extends Model
{
     // Set table
     protected $table = 'panel_attribute_limit';

     // Set timestamps
     public $timestamps = true;

     // Set primary key
     protected $primaryKey = 'id';

     protected $fillable = [
        'attribute_id',
        'product_code',
        'duration',
        'role',
        'amount',
        'active'
     ];

     public function productAttribute()
     {
         return $this->belongsTo(ProductAttribute::class, 'attribute_id');
     }

    public function checkPurchaseLimit($user_id, $product_limit)
    {
        $user = auth()->user();

        $date = \Carbon\Carbon::now();
        $totalPurchase = 0;
        $purchase_limit_left = 100;
        $purchase_limit_eligible = false;

        if ($product_limit) {
            switch ($product_limit->duration) {
                case 'yearly':
                    $dateFrom = $date->copy()->startOfYear();
                    $dateTo = $date->copy()->endOfYear()->endOfDay();
                    $period = 'this year';
                    break;
                case 'monthly':
                    $dateFrom = $date->copy()->startOfMonth();
                    $dateTo = $date->copy()->endOfMonth()->endOfDay();
                    $period = 'this month';
                    break;
                case 'weekly':
                    $dateFrom = $date->copy()->startOfWeek();
                    $dateTo = $date->copy()->endOfWeek()->endOfDay();
                    $period = 'this week';
                    break;
                default:
                    $dateFrom = $date->copy()->startOfDay();
                    $dateTo = $date->copy()->endOfDay();
                    $period = 'today';
            }
            $purchases = $user->purchases()->where('purchase_status', '4002')->whereBetween('created_at', [$dateFrom, $dateTo])->get();
            foreach ($purchases as $purchase) {
                $items = $purchase->items;
                foreach ($items as $item) {
                    if ($item->product_code == $product_limit->product_code) {
                        $totalPurchase += $item->quantity;
                    }
                }
            }
            $userRoles = $user->roles->pluck('name')->toArray();
            $productRoles = json_decode($product_limit->roles);
            switch($product_limit->productAttribute->purchase_limit_type){
                case 'roles':
                    if (collect($userRoles)->diff($productRoles)->isEmpty()) {
                        $purchase_limit_eligible = true;
                        $purchase_limit_left = $product_limit->amount - $totalPurchase;
                    }
                    break;
                case 'agent':
                    if (empty(array_diff($productRoles, $userRoles))) {
                        $purchase_limit_eligible = true;
                        $purchase_limit_left = $product_limit->amount - $totalPurchase;
                    }
                default:
                    # code...
                    break;
            }
            
            $result['purchase_limit_eligible'] = $purchase_limit_eligible;
            $result['purchase_limit_left'] = $purchase_limit_left;
            $result['period'] = $period;
            $result['total_purchase'] = $totalPurchase;
        }

        return $result;
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopesearchFilter($query,  $req)
    {
        if($req->code_filter){
            $query->where('product_code', $req->code_filter);
        }
        if($req->type_filter){
            $query->whereHas('productAttribute', function ($query) use ($req) {
                $query->where('purchase_limit_type', $req->type_filter);
            });
            if($req->roles_filter) $query->whereJsonContains('roles', $req->roles_filter);

            if($req->user_filter)$query->where('user_id', $req->user_filter);
        }
        return $query;
    }
}
