@extends('layouts.administrator.main')

@section('content')

<h1>Tax</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/tax/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Tax</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            <table id="global-tax-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>Country</td>
                        <td>Country ID</td>
                        <td>Tax Name</td>
                        <td>Tax Percentage</td>
                        <td>Tax Registration Number</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(t,i) in tax">
                        <td class="text-center">@{{i+1}}</td>
                        <td>@{{ getCountryName(t.country_id) }}</td>
                        <td>@{{t.country_id}}</td>
                        <td>@{{t.tax_name}}</td>
                        <td>@{{t.tax_percentage}}%</td>
                        <td>@{{t.tax_reg_number}}</td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#taxEdit" @@click="getTaxDataById(t.id)">Edit</button>
                            <button type="button" class="btn btn-danger" @@click="deleteTax(t.id)">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- Start Edit Modal -->
            <form {{-- v-on:submit.prevent --}}>
                @csrf
            <div class="modal fade" id="taxEdit" tabindex="-1" role="dialog" aria-labelledby="taxEditLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                    <h5 class="modal-title" id="taxEditLabel">Edit Tax #@{{tid}} @{{tax_name}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">

                            <div class="form-group">
                              <label for="country">Country</label>
                              <select v-model="country_id" class="form-control">
                                @foreach ($country as $val)
                                <option value="{{$val['country_id']}}">{{$val['country_name']}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                                <label for="tax_name">Tax Name</label>
                                <input type="text" v-model="tax_name" class="form-control" placeholder="Tax Name">
                            </div>
                            <div class="form-group">
                                <label for="tax_percentage">Tax Percentage</label>
                                <input type="text" v-model="tax_percentage" class="form-control" placeholder="Tax Percentage">
                            </div>
                            <div class="form-group">
                                <label for="tax_reg_number">Tax Registration Number</label>
                                <input type="text" v-model="tax_reg_number" class="form-control" placeholder="Tax Registration Number">
                            </div>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" @@click="updateTax(tid)">Save changes</button>
                    </div>

                </div>
                </div>
            </div>
        </form>
        <!-- End Edit Modal -->

        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-tables-2@2.2.1/dist/vue-tables-2.min.js"></script>
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script>
    var app = new Vue({
    el: '#app',
    data: {
        tax: [],
        tid: 0,
        country_id: '',
        tax_name: '',
        tax_percentage: '',
        tax_reg_number: ''
    },
    mounted() { //created // mounted //beforeMount
        axios
            .get('/administrator/tax/getTax')
            .then(response => {
                this.tax = response.data
            })
            .catch(error => console.log(error))
    },
    methods: {
        getTax() {
        axios
            .get('/administrator/tax/getTax')
            .then(response => {
                this.tax = response.data
                })
            .catch(error => console.log(error))
        },
        getTaxDataById(id) {
        axios
            .get('/administrator/tax/getTaxById/' + id)
            .then(response => {
                this.tid = response.data.id
                this.country_id = response.data.country_id
                this.tax_name = response.data.tax_name
                this.tax_percentage = response.data.tax_percentage
                this.tax_reg_number = response.data.tax_reg_number
                })
            .catch(error => console.log(error))
        },
        updateTax(id) {
            axios.post('/administrator/tax/update/' + id, {
                    id: id,
                    country_id: this.country_id,
                    tax_name: this.tax_name,
                    tax_percentage: this.tax_percentage,
                    tax_reg_number: this.tax_reg_number
                })
                .then(function (response) {
                    app.getTax()
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        deleteTax(id) {
            axios.post('/administrator/tax/delete/' + id, {
                //
                })
                .then(function (response) {
                    app.getTax()
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getCountryName($cc) {
            switch($cc) {
            case 'BN':
                return 'Brunei'
                break;
            case 'SG':
                return 'Singapore'
                break;
            default:
                return 'Malaysia'
            }
        }
    }
    });
</script>
@endpush
