<footer id="footer-content-collapse-sidebar" class="fml-footer mt-0">
    <div class="fml-container ">
        <div class="row">
            <div class="col-12 col-md-4 ft-left">
                <div class="d-md-none p-0">
                    <h4>We Care About You</h4>
                </div>
                <a cl ass="fml" href="/shop">
                    <img class="weblogo" src="{{ asset("images/logo/formula-logo.png") }}" alt="Formula" width="150">
                </a>
                <div class="d-none d-lg-block">
                    <div class="row">
                        <div class="col-5 col-md-2 mr-0">
                            <ul class="fml-social">
                                <li>
                                    <div class="fml-social-box">
                                        <a href="">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>

                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="fml-social">
                                <li>
                                    <div class="fml-social-box">
                                        <a href="">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="fml-social">
                                <li>
                                    <div class="fml-social-box">
                                        <a href="">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul class="fml-social">
                                <li>
                                    <div class="fml-social-box">
                                        <a href="">
                                            <i class="fab fa-waze" style="margin-top: 5px"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ft-link-btm">
                    <div class="d-md-none p-0">
                        <div class="ft-link-right w-100">
                            <div class="row">
                                <div class="col-md-4 w-50-md">
                                    <ul>
                                        <li>
                                            <a class="text-font-size" href="/shop/about-us">About Us</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>
                                        <li>
                                            <a class="text-font-size" href="#">Our Vission, Culture, Value</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>
                                        <li>
                                            <a class="text-font-size" href="#">Partner Engagement</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>
                                        <li>
                                            <a class="text-font-size" href="#">Exchange Policy</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ft-link-left w-100 pl-3">
                        <div class="col-md-4 p-0">
                            <ul>
                                <li>
                                    <a class="text-font-size" href="{{route('shop.privacy.policy')}}">Privacy Policy</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 p-0">
                            <ul>
                                <li>
                                    <a class="text-font-size" href="/shop/dashboard/orders/index">Track an Order</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 p-0">
                            <ul>
                                <li>
                                    <a class="text-font-size" href="/shop/faq">FAQ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{-- <h5>©2021 Emerald. All Rights Reserved.</h5> --}}
                {{-- <ul>
                    <li>
                        <a class="text-font-size" href="{{route('shop.bujishu.service')}}">Formula Service</a>
                    </li>
                </ul> --}}

            </div>
            <div class="col-12 col-md-4 ft-right" style="/*top: -30px;*/">
                <div class="footer-centerbg web">
                    <h2>Quality &<span>Trust</span></h2>
                    <p style="color: #ffffff">Keep up with the new products and exclusive product sales from Formula Healthcare Sdn Bhd.</p>
                    <br>
                    <input type="text" name="emailUs" placeholder=" Email" class="checkout-box2 emailUs"
                           style="width: 60%">
                    {{--                    <button class="" type="submit"><i class="fa fa-paper-plane"></i></button>--}}
                    {{--                    <div class="i-social my-pt-1">--}}
                    {{--                        <a href="#">--}}
                    {{--                            <img class="my-mr-1" src="{{ asset("images/icons/icon-social1.png")}}">--}}
                    {{--                        </a>--}}
                    {{--                        <a href="#">--}}
                    {{--                            <img class="my-mr-1" src="{{ asset("images/icons/icon-social2.png") }}">--}}
                    {{--                        </a>--}}
                    {{--                        <a href="#">--}}
                    {{--                            <img src="{{ asset("images/icons/icon-social3.png") }}">--}}
                    {{--                        </a>--}}
                    {{--                    </div>--}}
                </div>
            </div>
            <div class="col-12 col-md-4 ft-right">
                <ul class="list-unstyled">
                    <li class="mb-3">
                        <h2>Contact Us</h2>
                    </li>
                    <li>
                        <p class="mb-3">
                            <a href="mailto:formula2u-cs@delhubdigital.com">
                                formula2u-cs@delhubdigital.com
                            </a>
                        </p>
                    </li>
                    <li>
                        <p class="mb-3">
                            {!! country()->company_address !!}
                        </p>
                    </li>
                    <li>
                        <div class="footer-centerbg mobile">
                            <h2>Quality & Trust</h2>
                            <p>Keep up with the new products and exclusive product sales from Formula Health Care.</p>
                            <div class="i-social">
                                <a href="#">
                                    <img class="my-mr-1" src="{{ asset("images/icons/icon-social1.png")}}">
                                </a>
                                <a href="#">
                                    <img class="my-mr-1" src="{{ asset("images/icons/icon-social2.png") }}">
                                </a>
                                <a href="#">
                                    <img src="{{ asset("images/icons/icon-social3.png") }}">
                                </a>
                            </div>
                        </div>
                    </li>
                    {{-- <li class="ft-title ft-pt">

                    </li> --}}
                </ul>
            </div>
            <div class="ft-title copyright-text">
                {{--                <h5 class="cp-left">©2021 Emerald. All Rights Reserved.</h5>--}}
                <h5 class="cp-right">© 2021 Copyrighted by FORMULA HEALTHCARE SDN. BHD. (1318429-V)</h5>
            </div>
        </div>

        {{-- <div class="col-12 footer-box">
            <div class="row myposr">
                <div class="col-md-3 text-left mb-0 myfooter-x2">
                    <p class="pr-1 pl-1 bujishu-gold mb-1 text-font-size " style="font-size: 0.875rem;"><img width="17"
                            class="mb-1" src="{{asset('/images/footer/icons/home.png')}}" alt="home-icon"> <b>WE ARE Formula</b></p>
                    <ul class="list-unstyled bujishu-white ">
                        <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.about.us')}}">About Us</a>
                        </li>
                        {{-- <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.vision')}}">Our Vision,
                                Culture,
                                Value</a>
                        </li>
                        <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.wip')}}">Partner
                                Engagement</a>
                        </li>
                        <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.workforce')}}">Workforce</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 text-left mb-0 myfooter-x2">

                    <h5 class="pr-1 pl-1 bujishu-gold " style="font-size: 0.875rem;"><img width="17" class="mr-1"
                            src="{{asset('/images/footer/icons/person.png')}}" alt="phone-icon"><b>CUSTOMER
                            SERVICE</b>
                    </h5>
                    <ul class="list-unstyled">
                        <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.bujishu.service')}}">Formula
                                Service</a>
                        </li>
                        <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.privacy.policy')}}">Privacy
                                Policy</a>
                        </li>
                        <li>
                            <!-- <a class="bujishu-white text-font-size" href="/shop/dashboard/orders/order-status"> -->
                            <a class="bujishu-white text-font-size" href="/shop/dashboard/orders/index">Track an
                                Order</a>
                        </li>
                        <li>
                            <a class="bujishu-white text-font-size" href="{{route('shop.faq')}}">FAQ</a>
                        </li>
                        <li>
                        <a class="bujishu-gold " href="">Contact Us</a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-12 text-left mb-0">
                    <ul class="list-unstyled">
                        <li>
                            <p class="bujishu-gold mb-1 text-font-size"><b>OUR CONTACT</b></p>
                            <p class="bujishu-white my mb-1 text-font-size ">
                                <img class="mr-1" src="{{asset('/images/footer/icons/location.png')}}" alt="Home-Icon">
                                <span>
                                {!! country()->company_address !!}
                                </span>
                            </p>
                        </li>
                        <li>
                        <p class="bujishu-white mb-1 text-font-size ">
                            <img class="mr-1" src="{{asset('images/footer/icons/phone.png')}}" alt="phone-icon">
                        <span>
                            03-21818821
                        </span>

                        </p>
                        </li>

                        <li>
                            <a class="bujishu-white mb-1 text-font-size" href="mailto:formula2u-cs@delhubdigital.com">
                                <img class="mr-1 text-font-size" src="{{asset('/images/footer/icons/email.png')}}"
                                    alt="phone-Icon">
                                <span>
                                formula2u-cs@delhubdigital.com
                                    {!! country()->company_email !!}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-3 text-left text-font-size myfooter-x2">
                    <div class="row">
                        <a class="col-md-6 mymb-30 fb01" target="_blank" href="https://facebook.com/DelhubDigital/">
                            <img src="{{asset('/images/footer/icons/fb-01.png')}}" alt="fb-icon">
                            <span class="bujishu-white">Facebook</span>
                        </a>
                        <a class="col-md-6 mymb-30 inst01" href="">
                            <img src="{{asset('/images/footer/icons/IG-01.png')}}" alt="insta-icon">
                            <span class="bujishu-white"> Instagram</span>
                        </a>
                    </div>

                    <!-- <div class="copyrightfont">
                    <h6 class="text-left">@ 2020 Bujishu. All Rights Reserved</h6>
                </div> -->
                </div>
            </div>
        </div> --}}
        {{-- <div class="col-12 footer-box">

            <hr class="myholizontalline" />

            <div class="row copyrightfont" style="color:white;">
                <div class="col-md-6"><h6>© 2021 Copyrighted by {{country()->company_name}} {{country()->company_reg}}</h6> </div>

                <div class="col-md-6 text-right" ><h6>© 2021 Copyrighted by Website powered by FORMULA HEALTHCARE SDN. BHD. (1318429-V)</h6></div>
             </h6>
            </div>
        </div> --}}

    </div>
</footer>

@push('script')
    <!--Start of Tawk.to Script-->
    {{-- <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5f73f535f0e7167d0014e043/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script> --}}
    <!--End of Tawk.to Script-->

@endpush
