<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeRbsGlobal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('global_rbs_reminder');

        Schema::create('global_rbs_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('rbs_label');
            $table->integer('rbs_data');
            $table->text('option_type')->comment('frequency / times');
            $table->text('option_status')->comment('active / inactive');
            $table->timestamps();
        });

        DB::table('global_rbs_options')->insert([
            [
                'rbs_label'=>'Monthly',
                'rbs_data'=>'1',
                'option_type'=>'frequency',
                'option_status'=>'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'rbs_label'=>'Bi Monthly',
                'rbs_data'=>'2',
                'option_type'=>'frequency',
                'option_status'=>'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'rbs_label'=>'3 Times',
                'rbs_data'=>'3',
                'option_type'=>'times',
                'option_status'=>'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'rbs_label'=>'6 Times',
                'rbs_data'=>'6',
                'option_type'=>'times',
                'option_status'=>'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'rbs_label'=>'12 Times',
                'rbs_data'=>'12',
                'option_type'=>'times',
                'option_status'=>'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_rbs_options');
    }
}
