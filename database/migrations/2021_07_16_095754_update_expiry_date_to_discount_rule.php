<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateExpiryDateToDiscountRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_rule', function (Blueprint $table) {
            $table->dateTime('start_date')->nullable()->change();
            $table->dateTime('end_date')->nullable()->change();
            $table->dateTime('expiry_date')->nullable()->change();
            $table->integer('expiry_date_type')->default(0)->after('expiry_date');
            $table->integer('breakdown_exp_date')->default(0)->after('expiry_date_type');

        });

        Schema::table('discount_code', function (Blueprint $table) {
            $table->dateTime('expiry')->nullable()->change();

            $table->dropColumn(['redeemed_by']);
            $table->dropColumn(['purchase_redeemed']);
            $table->dropColumn(['redeem_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_rule', function (Blueprint $table) {
            $table->date('start_date')->nullable()->change();
            $table->date('end_date')->nullable()->change();
            $table->date('expiry_date')->nullable()->change();

            $table->dropColumn(['expiry_date_type']);
            $table->dropColumn(['breakdown_exp_date']);
        });

        Schema::table('discount_code', function (Blueprint $table) {
            $table->date('expiry')->nullable()->change();

            $table->integer('redeemed_by')->nullable()->after('expiry');
            $table->text('purchase_redeemed')->nullable()->after('redeemed_by');
            $table->dateTime('redeem_date')->nullable()->after('purchase_redeemed');
        });
    }
}
