<?php

namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerDirectPay extends Model
{
     // Set table
     protected $table = 'dealer_direct_pay';

     // Set timestamps
     public $timestamps = true;

     // Set primary key
     protected $primaryKey = 'id';

     // Set mass assignable columns
     protected $fillable = [];

     // Cast column to another datatype.
     protected $casts = [
          'direct_pay_id' => 'array'
     ];

     public function user(){
          return $this->belongsTo('App\Models\Users\User','user_id','id');
     }

     public function purchase()
     {
          return $this->belongsTo('App\Models\Purchases\Purchase', 'purchase_id');
     }
}
