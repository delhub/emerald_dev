<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Http\Controllers\WEB\Shop\v1\CartController;
use App\Models\Categories\Category;
use App\Models\Globals\Countries;
use App\Models\Globals\Products\Product;
use App\Models\Products\Product as PanelProduct;
use App\Models\Users\Customers\Cart;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Models\Tax\Tax;
use Illuminate\Http\Response;
use App\Models\Globals\City;
use App\Models\Globals\State;

class SGLaunchPackage extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handles /shop
     */
    public function index()
    {
        // if (Auth::check()) {
        //     Auth::logout();
        // }




        $packageCategory = Category::where('slug', 'agent-registration')->where('type', 'shop')->first();
        $user = User::where('id', Auth::user()->id);
        $userStatus = $user->first()->userInfo->account_status == '0' ? 'bjs' : 'dc';
        $packages = $packageCategory->products->where('product_status', 1)->sortBy('name');

        $country = country()->country_id;
        $currency = Countries::where('country_id', $country)->first()->country_currency;
        // $new_shipCookie = json_encode(array(
        //     'address_1' => 'Address 1',
        //     'address_2' => 'Address 2',
        //     'postcode' => 12000,
        //     'city' => 22,
        //     'city_key' => 'SGP_AMK',
        //     'city_name' => City::where('city_key', 'SGP_AMK')->first(),
        //     'state_name' => State::find(22),
        //     'state_id' => 22,
        //     'name' =>  "Full Name",
        //     'mobile' => '0123456789'
        // ));
        // Cookie::queue('addressCookie', $new_shipCookie, 3600);
        //$registrationRecord = Registration::where('invoice_number', $request->query('invoiceNumber'))->first();

        //if (!$registrationRecord) {
        // return abort(403, 'Invalid URI provided.');
        // } else {
        return view('sg-launch.package-list', compact(['packages', 'country', 'currency', 'userStatus']));
    }

    public function buyNow(Request $request)
    {
        // return $request;
        $country = country()->country_id;
        $countryCurrency = Countries::where('country_id', $country)->first()->country_currency;
        $user = User::where('id', Auth::user()->id)->first();
        $userShipping = $user->userInfo->shippingAddress;

        $panelProduct = PanelProduct::where('id', $request->panelProduct)->first();
        $parentProduct = $panelProduct->parentProduct;

        $product_information = array();
        if ($panelProduct->attributes) {
            $product_information['product_' . $panelProduct->attributes->first()->attribute_type . '_id'] = $panelProduct->attributes->first()->id;
            $product_information['product_' . $panelProduct->attributes->first()->attribute_type] = $panelProduct->attributes->first()->attribute_name;
            $product_information['product_code'] = $panelProduct->attributes->first()->product_code;
            $product_information['product_color_img'] = $panelProduct->attributes->first()->color_images;
        }
        Cart::where('user_id', Auth::user()->id)->where('selected', 1)->update(array('selected' => 0));
        $launchCart = Cart::where('user_id', Auth::user()->id)->where('product_id', $panelProduct->id)->where('status', 2001)->first();

        if ($launchCart == NULL) {
            $launchCart = new Cart;
        }
// dd($panelProduct->attributes->first());
        $launchCart->user_id = Auth::user()->id;
        $launchCart->product_id = $panelProduct->id;
        $launchCart->bundle_id = 0;
        $launchCart->product_code = $parentProduct->product_code;
        $launchCart->product_information = ($product_information);
        $launchCart->quantity = 1;
        $launchCart->unit_price = $user->userInfo->account_status == 1 ? $panelProduct->attributes->first()->getPriceAttributes('web_offer_price') : $panelProduct->attributes->first()->getPriceAttributes('fixed_price');
        $launchCart->subtotal_price = $user->userInfo->account_status == 1 ? $panelProduct->attributes->first()->getPriceAttributes('web_offer_price') : $panelProduct->attributes->first()->getPriceAttributes('fixed_price');
        $launchCart->price_key = $panelProduct->attributes->first()->getPriceAttributes('price_key');
        $launchCart->delivery_fee = 0;
        $launchCart->installation_fee = 0;
        $launchCart->rebate = 0;
        $launchCart->country_id = $country;
        $launchCart->status = 2001;
        $launchCart->selected = 1;
        $launchCart->disabled = 0;
        $launchCart->created_at = Carbon::now();
        $launchCart->updated_at = Carbon::now();
        $launchCart->save(); //------> MOVE DOWN

        $purchase_amount = 0;
        $purchase_amount = $purchase_amount + $launchCart->subtotal_price;

        $discount = $request->coupon_amount;
        $coupon = $request->coupon_code;

        $cartTotals = array();
        $zone = CartController::maybeCreateCookieAddress();
        $zoneId = CartController::getZone($zone['state_id'], $zone['city_key']);
        $cartTotals = CartController::shippingCalculate($zoneId, array($launchCart));

        Session::put('bjs_cartTotal', json_encode(array('cartTotals' => $cartTotals)));

        $cartTotalsSession = json_decode(Session::get('bjs_cartTotal'));
        $cartTotalsSession->cartTotals->tax = $cartTotalsSession->cartTotals->total * $cartTotalsSession->cartTotals->tax_rate / 100;
        $cartTotalsSession->cartTotals->total = $cartTotalsSession->cartTotals->total + $cartTotalsSession->cartTotals->tax;
        $carTotalsFee = $cartTotalsSession->cartTotals->fee;
        $shippingFee = $carTotalsFee->shipping->amount;

        $installationFee = isset($carTotalsFee->installation->amount) ? $carTotalsFee->installation->amount : 0;
        $rebateFee = isset($carTotalsFee->rebate->amount) ? $carTotalsFee->rebate->amount : 0;


        $purchaseCookie = new \stdClass();
        $purchaseCookie->purchase_amount = $purchase_amount + $shippingFee + $installationFee - $rebateFee - ($discount * 100); // add installation into purchase amount
        $purchaseCookie->ship_full_name = $user->userInfo->full_name;
        $purchaseCookie->ship_contact_num = $user->userInfo->mobileContact->contact_num;
        $purchaseCookie->ship_address_1 = $userShipping->address_1;
        $purchaseCookie->ship_address_2 = $userShipping->address_2;
        $purchaseCookie->ship_address_3 = $userShipping->address_3;
        $purchaseCookie->ship_postcode = $userShipping->postcode;
        $purchaseCookie->ship_city =  '';
        $purchaseCookie->ship_city_key =  $userShipping->city_key;
        $purchaseCookie->ship_state_id =  $userShipping->state_id;
        $purchaseCookie->item_count = 1;

        $bluepointPurchase = false;
        $rebateAllowed = false;

        // dd(array($launchCart->id));
        $cookie = (array('cartIds' => array($launchCart->id), 'purchase' => $purchaseCookie, 'bluepointPurchase' => $bluepointPurchase, 'rebateAllowed' => $rebateAllowed, 'carTotalsFee' => $carTotalsFee, 'discount' => $discount, 'coupon' => $coupon));

        //    return ($cookie);
        $country_code = country()->country_id;

        //get tax and country currency & rate
        $tax = Tax::where('country_id', $country_code)->get();
        $tableCountries = Countries::where('country_id', $country_code)->get();



        $conversion_rate = country()->conversion_rate;
        $currency = $tableCountries[0]->country_currency;
        $tax_rate = $tax[0]->tax_percentage;

        $purchase = $cookie['purchase'];
        $purchase->countryCurrency = $currency;
        $purchase->countryCode = $country_code;


        if (in_array($launchCart->product->parentProduct->can_installment, ['0', '1'])) {
            $canInstallment = false;
        } else {
            $installmentDetails = (json_decode($launchCart->product->parentProduct->can_installment));
            $canInstallment = true;
        }
        $canInstallment = false;
        // $launchCart->purchase_amount_myr = $purchaseCookie->purchase_amount / $conversion_rate;
        $launchCart->total = $launchCart->subtotal_price + (($launchCart->subtotal_price * $tax_rate) / 100);
        // return $panelProduct->defaultPrice('member_price', 'MY');
        $priceType = $user->userInfo->account_status == 1 ? 'member_price' : 'price';
        $launchCart->installmentMY = $canInstallment ? ($installmentDetails->firstPayment + $installmentDetails->processingFee) * 100 : 0;
        $launchCart->installmentSG = $launchCart->installmentMY * $conversion_rate;
        $launchCart->fullSG = $launchCart->total;
        $launchCart->fullMY =  $launchCart->fullSG  / $conversion_rate;

        $launchCart->total = number_format(($launchCart->total / 100), 2);


        // $data = [];

        // // foreach ($launchCart as $val) {
        // $dat = [];
        // $dat['eligible_discount'] = $launchCart['product']['parent_product']['eligible_discount'];
        // $dat['generate_discount'] = $launchCart['product']['parent_product']['generate_discount'];
        // $dat['subtotal_price'] = $launchCart['subtotal_price'];
        // $dat['cart_id'] = $launchCart['id'];

        // $data[] = $dat;



        // }

        // dd($data);

        // $data['cart_total'] = $cartTotals['total_new'];

        // Session::put('bjs_cart_discount', $data);
        // return redirect('/payment/cashier');
        return redirect('/shop/cart');

        // $response = new Response(view('sg-launch.package-buy', compact(['packages', 'country', 'user', 'launchCart', 'purchase', 'tax_rate', 'canInstallment', 'installmentDetails'])));

        // $response->withCookie(cookie('bjs_cart', json_encode($cookie), 3600));

        // return $response;

        // return view('sg-launch.package-buy', compact(['packages','country','user','launchCart','countryCurrency','purchase','tax_rate']));


    }
}
