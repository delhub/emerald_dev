<?php

namespace App\Http\Controllers\Administrator\v1\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductAttribute;
use App\Models\Globals\State;
use App\Models\Products\ProductBundle;
use App\Models\Globals\Products\Product as GlobalProduct;
use App\Models\Products\Product;
use App\Models\Products\ProductDelivery;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Globals\Image;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\Users\Panels\PanelCategories;
use Illuminate\Support\Facades\Validator;
use App\Models\Products\ProductPrices;
use App\Models\Products\ProductMarkupCategory;
use Illuminate\Support\Facades\Auth;
use App\Models\Users\User;

class ProductByPanelController extends Controller
{

    public static $category_type;

    public function __construct()
    {

        self::$category_type = array(
            'shipping' => 'Shipping Categories',
            'installation' => 'Installation Categories'
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $globalProduct = GlobalProduct::find($request->input('new_product_id'));
        $panels = PanelInfo::all();
        $states = State::all();
        $curtainIcons = Image::where('imageable_id', '=', '0')->get();
        $firstCategory = $globalProduct->categories->first()->id;
        $deliveries = PanelCategories::where('category_id', $firstCategory)->get();
        $shippingCategory = Ship_category::all();
        $images = $globalProduct->images;
        $products = ProductAttribute::selectRaw('panel_product_attributes.id, panel_product_attributes.product_code, panel_product_attributes.attribute_name, global_products.name')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('panel_product_attributes.product_code', '!=', '')
            ->where('panel_product_attributes.price', '!=', 0)
            ->where('global_products.product_status', '=', 1)
            ->orderBy('panel_product_attributes.product_code', 'DESC')
            ->get();


        return view('administrator.products.v1.panel.create')
            ->with('globalProduct', $globalProduct)
            ->with('panels', $panels)
            ->with('states', $states)
            ->with('curtainIcons', $curtainIcons)
            ->with('images', $images)
            ->with('deliveries', $deliveries)
            ->with('shippingCategory', $shippingCategory)
            ->with('user', $user)
            ->with('products', $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $globalProduct = GlobalProduct::find($request->input('global_product_id'));
        $product = new Product;
        $product->global_product_id = $globalProduct->id;
        $product->panel_account_id = $request->input('panel_id');
        $product->display_panel_name = $request->input('display_panel_name');
        // $product->multichoices = $request->input('multichoices') ? $request->input('multichoices') : '0';

        // $product->price = str_replace('.', '', $request->input('price'));
        // $product->member_price = str_replace('.', '', $request->input('member_price'));
        // $product->has_offer = $request->input('has_offer');
        // $product->offer_price = $request->input('offer_price') ? str_replace('.', '', $request->input('offer_price')) : '0' ;
        $product->origin_state_id = $request->input('ships_from');
        $product->product_description = $request->input('product_description');
        $product->product_content_1 = $request->input('product_material');
        $product->product_content_2 = $request->input('product_consistency');
        $product->product_content_3 = $request->input('product_package');
        // $product->product_sv = $request->input('product_sv');
        $product->estimate_ship_out_date = $request->input('estimate_ship_out_date');
        // $product->shipping_category = $request->input('shippingcategories');

        $product->shipping_category = 0;
        $product->installation_category = 0;

        $installationCategories = array();

        if ($request->input('installationCategories')) {
            foreach ($request->input('installationCategories') as $installationCategory) {
                $installationCategories[] = $installationCategory;
            }

            $product->installation_category = $installationCategories;
        }

        if ($request->input('shippingcategories')) {
            $product->shipping_category = $request->input('shippingcategories');
        }

        // $product->fixed_price = str_replace('.', '', $request->input('fixed_price')) != "" ? str_replace('.', '', $request->input('fixed_price')) : 0;
        // $product->fixed_showitem = ($request->input('fixed_showitem') != 1) ? 0 : 1;
        $product->save();

        /*******      Create price for MY and SG     ******/
        $PanelProductPrice = new ProductPrices;

        $PanelProductPrice->model_type = Product::class;
        $PanelProductPrice->model_id = $product->id;
        $PanelProductPrice->product_code = $product->parentProduct->product_code;
        $PanelProductPrice->country_id = 'MY';
        $PanelProductPrice->fixed_price = 0;
        $PanelProductPrice->price = 0;
        $PanelProductPrice->member_price = 0;
        $PanelProductPrice->offer_price = 0;
        $PanelProductPrice->product_sv = 0;

        $PanelProductPrice->save();

        $PanelProductPriceSG = new ProductPrices;

        $PanelProductPriceSG->model_type = Product::class;
        $PanelProductPriceSG->model_id = $product->id;
        $PanelProductPriceSG->product_code = $product->parentProduct->product_code;
        $PanelProductPriceSG->country_id = 'SG';
        $PanelProductPriceSG->fixed_price = 0;
        $PanelProductPriceSG->price = 0;
        $PanelProductPriceSG->member_price = 0;
        $PanelProductPriceSG->offer_price = 0;
        $PanelProductPriceSG->product_sv = 0;

        $PanelProductPriceSG->save();
        /*******      End create price for MY and SG     ******/



        foreach ($request->input('product_variation') as $key => $attributeType) {
            $attribute = new ProductAttribute;
            $attribute->panel_product_id = $product->id;
            $attribute->attribute_type = $attributeType;
            $attribute->attribute_name = $request->input('product_variation_name')[$key];
            $attribute->fixed_price = 0;
            $attribute->price = 0;
            $attribute->member_price = 0;

            // $attribute->offer_price = ($request->input('product_variation_offer_price')[$key] != null) ? str_replace('.', '', $request->input('product_variation_offer_price')[$key]) : 0;
            $attribute->color_hex = $request->input('product_variation_color_hex')[$key];

            // If there is no product code in variation, use global product code
            if (isset($request->input('product_variation_code')[$key])) {
                $attribute->product_code = $request->input('product_variation_code')[$key];
            } else {
                $attribute->product_code = $globalProduct->product_code;
            }

            // If there is no SV in variation, use main SV
            if (isset($request->input('product_variation_sv')[$key])) {
                $attribute->product_sv = $request->input('product_variation_sv')[$key] ?? '0';
            } else {
                $attribute->product_sv = $request->input('product_sv') ?? '0';
            }

            $attribute->size_icon = $request->input('product_variation_curtain_size_icon')[$key];

            $attribute->color_images = $request->input('productImage')[$key];

            $attribute->save();

            /*******      Create price for MY and SG     ******/
            $PanelProductPrice = new ProductPrices;

            $PanelProductPrice->model_type = ProductAttribute::class;
            $PanelProductPrice->model_id = $attribute->id;
            $PanelProductPrice->product_code = $attribute->product_code;
            $PanelProductPrice->country_id = 'MY';
            $PanelProductPrice->fixed_price = 0;
            $PanelProductPrice->price = 0;
            $PanelProductPrice->member_price = 0;
            $PanelProductPrice->offer_price = 0;
            $PanelProductPrice->product_sv = 0;

            $PanelProductPrice->save();

            $PanelProductPricesSG = new ProductPrices;

            $PanelProductPricesSG->model_type = ProductAttribute::class;
            $PanelProductPricesSG->model_id = $attribute->id;
            $PanelProductPricesSG->product_code = $attribute->product_code;
            $PanelProductPricesSG->country_id = 'SG';
            $PanelProductPricesSG->fixed_price = 0;
            $PanelProductPricesSG->price = 0;
            $PanelProductPricesSG->member_price = 0;
            $PanelProductPricesSG->offer_price = 0;
            $PanelProductPricesSG->product_sv = 0;

            $PanelProductPricesSG->save();
            /*******      End create price for MY and SG     ******/
        }

        if ($request->get('bundle_type')) {
            foreach ($request->input('bundle_type') as $key => $bundleType) {
                $bundle = new ProductBundle;
                $bundle->panel_product_id = $product->id;
                $bundle->bundle_type = $bundleType;
                $bundle->bundle_title = $request->input('bundle_title')[$key];
                $bundle->bundle_id = $request->input('bundle_id')[$key];
                $bundle->bundle_qty = $request->input('bundle_qty')[$key];
                $bundle->product_attribute_id = $request->input('product_attribute_id')[$key];

                $data = array();

                $data['selection'] = $request->input('bundle_variation')[$key];
                $data['name'] = $request->input('bundle_variation_selection')[$key];

                $encode_bundleVariation = json_encode($data);

                $bundle->bundle_variation = $encode_bundleVariation;

                $bundle->save();
            }
        }

        if ($request->get('selectDeliveries')) {
            $parentPanelID = $product->panel->account_id;
            $parentID = $product->parentProduct->categories->first()->id;
            //  $productDeliveryRange = PanelCategories::where('category_id', $parentID)->get();
            foreach ($request->get('selectDeliveries') as $key => $selectDelivery) {
                $saveDeliveryRange = PanelCategories::firstOrNew(
                    [
                        'id' => $request->input('firstCategory')[$key]
                    ]
                );

                $saveDeliveryRange->panel_id  = $parentPanelID;
                $saveDeliveryRange->category_id = $parentID;
                $saveDeliveryRange->delivery_text = $request->input('delivery_text');
                $saveDeliveryRange->free_state = $request->input('freeStates') ? json_encode($request->input('freeStates')) : null;
                $saveDeliveryRange->min_price = str_replace('.', '', $request->input('min_delivery')[$key]);
                $saveDeliveryRange->max_price = str_replace('.', '', $request->input('max_delivery')[$key]);
                $saveDeliveryRange->delivery_fee = str_replace('.', '', $request->input('fixed_delivery')[$key]);
                $saveDeliveryRange->no_purchase = $request->get('no_purchase')[$key];
                $saveDeliveryRange->group_or_individual = $request->get('selectDeliveries')[$key];

                $saveDeliveryRange->save();
            }
        }

        foreach ($request->input('available_in') as $key => $availableIn) {
            $delivery = new ProductDelivery;
            $delivery->state_id = $availableIn;
            $delivery->panel_product_id = $product->id;
            $delivery->delivery_fee = ($request->input('available_in_price')[$key] != null) ? $request->input('available_in_price')[$key] * 100 : 0;
            $delivery->save();
        }

        if ($product->save()) {
            return redirect('/administrator/products/panels')
                ->with('success', 'Product saved successfully');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Something went wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find(Auth::user()->id);
        $product = Product::findOrFail($id);
        $parentProduct = $product->parentProduct;
        $images = $parentProduct->images;
        $panels = PanelInfo::all();
        $states = State::all();
        $curtainIcons = Image::where('imageable_id', '=', '0')->get();
        $firstCategory = $parentProduct->categories->first()->id;
        $shippingCategory = Ship_category::all();
        $deliveries = PanelCategories::where('category_id', $firstCategory)->get();
        $markupCategories = ProductMarkupCategory::get();
        $products = ProductAttribute::selectRaw('panel_product_attributes.id, panel_product_attributes.product_code, panel_product_attributes.attribute_name, global_products.name')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->join('panel_product_prices', 'model_id', '=', 'panel_product_attributes.id')
            ->where('panel_product_attributes.product_code', '!=', '')
            ->where('global_products.product_status', '=', 1)
            ->where('panel_product_prices.price', '!=', 0)
            ->where('panel_product_prices.country_id', 'MY')
            ->orderBy('panel_product_attributes.product_code', 'DESC')
            ->get();

        return view('administrator.products.v1.panel.edit')
            ->with('user', $user)
            ->with('product', $product)
            ->with('parentProduct', $parentProduct)
            ->with('panels', $panels)
            ->with('states', $states)
            ->with('curtainIcons', $curtainIcons)
            ->with('firstCategory', $firstCategory)
            ->with('shippingCategory', $shippingCategory)
            ->with('images', $images)
            ->with('deliveries', $deliveries)
            ->with('markupCategories', $markupCategories)
            ->with('products', $products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'productImage' => 'required'
        ]);
        // $offerPrice = $request->input('offer_price');
        // return $request;
        $product = Product::findOrFail($id);
        $parentProduct = $product->parentProduct;

        $product->panel_account_id = $request->input('panel_id');
        $product->display_panel_name = $request->input('display_panel_name');
        // $product->multichoices = $request->input('multichoices') ? $request->input('multichoices') : '0';
        // $product->price = str_replace('.', '', $request->input('price'));
        // $product->member_price = str_replace('.', '', $request->input('member_price'));
        // $product->has_offer = $request->input('has_offer');
        // $product->offer_price = $request->input('offer_price') ? str_replace('.', '', $request->input('offer_price')) : '0' ;
        $product->origin_state_id = $request->input('ships_from');
        $product->product_description = $request->input('product_description');
        $product->product_content_1 = $request->input('product_material');
        $product->product_content_2 = $request->input('product_consistency');
        $product->product_content_3 = $request->input('product_package');
        $product->product_sv = $request->input('product_sv') ?? '0';
        $product->estimate_ship_out_date = $request->input('estimate_ship_out_date');
        // $product->installation_category = $request->input('installationCategories') ? json_encode($request->input('installationCategories')) : 0;
        // $product->fixed_price = str_replace('.', '', $request->input('fixed_price')) != "" ? str_replace('.', '', $request->input('fixed_price')) : 0;
        // $product->fixed_showitem = ($request->input('fixed_showitem') != 1) ? 0 : 1;
        $product->markup_category = $request->input('markup_category')[0];


        // -------------------------------- Shipping and Installation ---------------------------------------------------//
        $product->shipping_category = 0;
        $product->installation_category = 0;

        $installationCategories = array();

        if ($request->input('installationCategories')) {
            foreach ($request->input('installationCategories') as $installationCategory) {
                $installationCategories[] = $installationCategory;
            }

            $product->installation_category = $installationCategories;
        }
        if ($request->input('shippingcategories')) {
            $product->shipping_category = $request->input('shippingcategories');
        }

        if ($request->input('shippingcategories')) {
            $product->shipping_category = $request->input('shippingcategories');
        }



        $product->fixed_price = str_replace('.', '', $request->input('fixed_price')) != "" ? str_replace('.', '', $request->input('fixed_price')) : 0;

        if (str_replace('.', '', $request->input('fixed_price')) != "") {

            $product->fixed_price = str_replace('.', '', $request->input('fixed_price'));
        } else {
            $product->fixed_price = 0;
        }

        $product->save();

        // return $request->input('product_variation');
        $attributeID = $product->id;
        $updatedAttributes = $request->input('product_variation_id');
        $updatedAttributePrice = $request->input('product_variation_price');

        if ($updatedAttributes != null) {
            $attributesCompares = ProductAttribute::where('panel_product_id', $attributeID)->whereNotIn('id', $updatedAttributes)->get();
            foreach ($attributesCompares as $attributesCompare) {
                $attributesCompare->delete();

                $attributesPriceCompares = ProductPrices::where('model_id', $attributesCompare->id)->get();
                foreach ($attributesPriceCompares as $key => $attributesPriceCompare) {
                    $attributesPriceCompare->delete();
                }
            }
        }

        foreach ($request->input('product_variation') as $key => $attributeType) {

            $findProductAttribute = ProductAttribute::firstOrNew(
                [
                    'id' => $request->input('product_variation_id')[$key]
                ]
            );

            //  $findProductAttribute->attribute_name = $request->input('product_variation_name')[$key] ;
            $findProductAttribute->panel_product_id = $product->id;
            $findProductAttribute->attribute_type = $attributeType;
            $findProductAttribute->attribute_name = $request->input('product_variation_name')[$key];
            $findProductAttribute->fixed_price = 0;
            $findProductAttribute->price = 0;
            $findProductAttribute->member_price = 0;
            $findProductAttribute->color_hex = $request->input('product_variation_color_hex')[$key];
            $findProductAttribute->size_icon = $request->input('product_variation_curtain_size_icon')[$key];

            // If there is no product code in variation, use global product code
            if (isset($request->input('product_variation_code')[$key])) {
                $findProductAttribute->product_code = $request->input('product_variation_code')[$key];
            } else {
                $findProductAttribute->product_code = $parentProduct->product_code;
            }

            // If there is no SV in variation, use main SV
            if (isset($request->input('product_variation_sv')[$key])) {
                $findProductAttribute->product_sv = $request->input('product_variation_sv')[$key] ?? '0';
            } else {
                $findProductAttribute->product_sv = $request->input('product_sv') ?? '0';
            }

            // If there is no color image in variation, use main color main image
            if (isset($request->input('productImage')[$key])) {
                $findProductAttribute->color_images = $request->input('productImage')[$key];
            } else {
                $findProductAttribute->color_images = $request->input('productImage')[0];
            }

            $findProductAttribute->save();

            if ($findProductAttribute->getPrice->isEmpty()) {
                /*******      Create price for MY and SG     ******/
                $PanelProductPrice = new ProductPrices;

                $PanelProductPrice->model_type = ProductAttribute::class;
                $PanelProductPrice->model_id = $findProductAttribute->id;
                $PanelProductPrice->product_code = $findProductAttribute->product_code;
                $PanelProductPrice->country_id = 'MY';
                $PanelProductPrice->fixed_price = 0;
                $PanelProductPrice->price = 0;
                $PanelProductPrice->member_price = 0;
                $PanelProductPrice->offer_price = 0;
                $PanelProductPrice->product_sv = 0;

                $PanelProductPrice->save();

                $PanelProductPricesSG = new ProductPrices;

                $PanelProductPricesSG->model_type = ProductAttribute::class;
                $PanelProductPricesSG->model_id = $findProductAttribute->id;
                $PanelProductPricesSG->product_code = $findProductAttribute->product_code;
                $PanelProductPricesSG->country_id = 'SG';
                $PanelProductPricesSG->fixed_price = 0;
                $PanelProductPricesSG->price = 0;
                $PanelProductPricesSG->member_price = 0;
                $PanelProductPricesSG->offer_price = 0;
                $PanelProductPricesSG->product_sv = 0;

                $PanelProductPricesSG->save();
                /*******      End create price for MY and SG     ******/
            }
        }


        $bundleID = $product->id;
        $updatedBundle = $request->input('product_bundle_id');

        if ($updatedBundle != null) {
            $attributesCompares = ProductBundle::where('panel_product_id', $bundleID)->whereNotIn('id', $updatedBundle)->get();
            foreach ($attributesCompares as $attributesCompare) {
                $attributesCompare->delete();
            }
        }

        if ($request->input('bundle_type')) {
            $data = array();

            foreach ($request->input('bundle_type') as $key => $bundleType) {
                $findProductBundle = ProductBundle::firstOrNew(
                    [
                        'id' => $request->input('product_bundle_id')[$key]
                    ]
                );
                $findProductBundle->panel_product_id = $product->id;
                $findProductBundle->bundle_type = $bundleType;
                $findProductBundle->bundle_title = $request->input('bundle_title')[$key];
                $findProductBundle->bundle_id = $request->input('bundle_id')[$key];
                $findProductBundle->bundle_qty = $request->input('bundle_qty')[$key];
                $findProductBundle->product_attribute_id = $request->input('product_attribute_id')[$key];

                $data['selection'] = $request->input('bundle_variation')[$key];
                $data['name'] = $request->input('bundle_variation_selection')[$key];

                $encode_bundleVariation = json_encode($data);
                $findProductBundle->bundle_variation = $encode_bundleVariation;

                $findProductBundle->save();
            }
        }

        if ($request->get('selectDeliveries')) {

            //    return  ($request->input('freeStates') != null) ? $request->input('freeStates') : 'no';

            $parentPanelID = $product->panel->account_id;
            $parentID = $product->parentProduct->categories->first()->id;

            $updatedCategory = $request->input('firstCategory');

            if ($updatedCategory != null) {
                $compares = PanelCategories::where('category_id', $parentID)->whereNotIn('id', $updatedCategory)->get();
                foreach ($compares as $compare) {
                    $compare->delete();
                }
            }

            foreach ($request->get('selectDeliveries') as $key => $selectDelivery) {
                $saveDeliveryRange = PanelCategories::firstOrNew(
                    [
                        'id' => $request->input('firstCategory')[$key]
                    ]
                );

                $saveDeliveryRange->panel_id  = $parentPanelID;
                $saveDeliveryRange->category_id = $parentID;
                $saveDeliveryRange->delivery_text = $request->input('delivery_text');
                $saveDeliveryRange->free_state = $request->input('freeStates') ? json_encode($request->input('freeStates')) : null;
                // 'category_id' => $request->get('selectDeliveries')[$key]
                $saveDeliveryRange->min_price = str_replace('.', '', $request->input('min_delivery')[$key]);
                $saveDeliveryRange->max_price = str_replace('.', '', $request->input('max_delivery')[$key]);
                $saveDeliveryRange->delivery_fee = str_replace('.', '', $request->input('fixed_delivery')[$key]);
                $saveDeliveryRange->no_purchase = $request->get('no_purchase')[$key];
                $saveDeliveryRange->group_or_individual = $request->get('selectDeliveries')[$key];

                $saveDeliveryRange->save();
            }
        }

        $productDeliveries = $product->deliveries;

        foreach ($productDeliveries as $productDelivery) {
            $productDelivery->delete();
        }

        foreach ($request->input('available_in') as $key => $availableIn) {
            $delivery = new ProductDelivery;
            $delivery->state_id = $availableIn;
            $delivery->panel_product_id = $product->id;
            $delivery->delivery_fee = ($request->input('available_in_price')[$key] != null) ? $request->input('available_in_price')[$key] * 100 : 0;
            $delivery->save();
        }

        if ($request->input('markup_category')[0] != 0) {
            $product->markupCategories()->sync($request->input('markup_category'));
        }

        if ($product->save()) {
            return redirect('/administrator/products/panels')
                ->with('success', 'Product saved successfully');
        } else {
            return redirect()
                ->back()
                ->with('error', 'Something went wrong.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
