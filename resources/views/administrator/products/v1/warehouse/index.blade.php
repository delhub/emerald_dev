@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Warehouse Batch List</h1>
<div class="card shadow-sm">
    <div class="card-header">Warehouse Batch List( total : {{$total}})
        <div class="card-header-actions"><button class="btn btn-primary btn-sm" type="button" onclick="window.location.href = '{{ route('administrator.v1.products.warehouse.newBatch')}}';">Add New Data</button></div>
    </div>

    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{$batch->links()}}
            <div class="col-lg-12" id="whBatch">
                <div class="table-responsive m-2">
                    <div class="col-md-12">
                        <table class="table table-striped mt-3 voucher-tb" id="dtv" {{--style="overflow-x: scroll"--}}>
                            <thead>
                              <tr>
                                {{-- <td><input name='selectAll' type="checkbox"></td> --}}
                                <td>ID</td>
                                <td>Batch ID</td>
                                <td>Batch Type</td>
                                <td>Location</td>
                                <td>Panel</td>
                                <td>Item </td>
                                <td>Stock In Date</td>
                                <td>Notes </td>
                                <td>Action</td>
                              </tr>
                            </thead>
                            <tbody>

                                @foreach($batch as $val)
                                <tr>
                                    {{-- <th width="10px"><input name='data[]' type="checkbox" value="{{$val->id}}"></th> --}}
                                    <th width="20px">{{$loop->iteration}} {{-- {!! QrCode::size(250)->generate("formula2u.com/qr/item/".$val->batch_id); !!} --}}</th>
                                    <th>{{$val->batch_id}}</th>
                                    <th>{{$val->batch_type}}</th>
                                    <th>{{$val->location->location_name}}</th>
                                    <th>{{$val->panel->company_name}}</th>
                                    <td>
                                            {{-- @foreach ($val->details as $detail)
                                                <span>{{$detail->product_code}} &#x2014; x {{$detail->item}} </span> <span class="small text-muted float-right">Expired: {{date("d M Y", strtotime($detail->expiry_date))}}</span><br>
                                            @endforeach --}}

                                        @php
                                            $tranfer = array();
                                            foreach ($val->details as $items){
                                                foreach ($items->items as $item){

                                                    if(!isset($tranfer[$items->product_code])) {
                                                        $tranfer[$items->product_code]['qty'] =0;
                                                        if ( $items->productAttribute ){
                                                              $tranfer[$items->product_code]['productName'] = $items->productAttribute->product2->parentProduct->name;;
                                                        } else {
                                                            $tranfer[$items->product_code]['productName'] = "Item ID#" . $items->id . ', No product2';
                                                            info( $tranfer[$items->product_code]['productName']);
                                                        }

                                                        $tranfer[$items->product_code]['expiredDate'] = $items->expiry_date;
                                                    }
                                                    $tranfer[$items->product_code]['qty'] ++;
                                                }
                                                unset($items);
                                            }
                                        @endphp

                                        @foreach ($tranfer as $productCode => $item)
                                            <span>{{$productCode}} &#x2014; x {{$item['qty']}} </span> <span class="small text-muted float-right">Expired: {{date("d M Y", strtotime($item['expiredDate']))}}</span><br>
                                        @endforeach

                                        @php
                                            unset($tranfer)
                                        @endphp
                                    </td>

                                    <th>{{date("d M Y", strtotime($val->stock_in_date))}}</th>

                                    <th width="300px">
                                        <div>
                                        <textarea type="text" name="notes" id="notes{{$val->id}}" class="form-control" value="{{$val->notes}}">{{$val->notes}}</textarea>
                                            <div class="ml-auto col-2 mr-4 mt-2">
                                                <button type="button" class="btn btn-danger"
                                                    onclick="editNotes({{ $val->id }});">
                                                    Edit
                                                    <div class="spinner-border spinner-border-sm text-dark variationLoader{{ $val->id }}" role="status" style="display:none;">
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                </button>
                                            </div>
                                            <p class="variationThisEmpty{{ $val->id }}" style="color:red;display: none;">Please enter something to add!</p>
                                            <p class="variationRefreshPrice{{ $val->id }}" style="color:red;display: none;">Refresh to edit this notes</p>
                                        </div>
                                    </th>

                                    <td>

                                        <div class="btn-group" role="group" aria-label="print">
                                            <a href="/administrator/warehouse/inventory/qr-batch/{{$val->batch_id}}" target="_blank">
                                                <button type="button" class="btn btn-primary">V1</button>
                                            </a>
                                            <a href="/administrator/warehouse/inventory/qr-batch-v2/{{$val->batch_id}}" target="_blank">
                                                <button type="button" class="btn btn-success ">V2</button>
                                            </a>
                                            <a href="/administrator/warehouse/inventory/qr-batch-v3/{{$val->batch_id}}" target="_blank">
                                                <button type="button" class="btn btn-warning">V3</button>
                                            </a>
                                            <a href="/administrator/warehouse/inventory/qr-batch-v4/{{$val->batch_id}}" target="_blank">
                                                <button type="button" class="btn btn-info">V4</button>
                                            </a>
                                        </div>
                                    </td>
                                  </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{$batch->links()}}

        </div>
    </div>

</div>
@endsection

@push('script')
<script>
    oTable = $('#dtv').DataTable({
        "order": [[ 0, "desc" ]],
        // "paging":   false
        "info":     false
    });
    $('#search').keyup(function(){
        oTable.search($(this).val()).draw();
    });

    $(':checkbox[name=selectAll]').click (function () {
        $(':checkbox[type=checkbox]').prop('checked', this.checked);
    });

    function editNotes(batchID) {
        const notes =$("#notes"+batchID).val();

        if (confirm('Are you sure want to edit this batch notes? Any unsaved changes will be lost')) {
            if (notes=='' ) {
                $(".variationThisEmpty"+batchID).show();
            } else {
                $.ajax({
                    // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
                    url:"{{ route('administrator.v1.products.warehouse.edit-notes', ['batchID' => "+batchID+"]) }}",
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "batchID": batchID,
                        "notes": notes,
                    },
                    beforeSend: function () {
                        $(".variationLoader"+batchID).show();
                    },
                    success: function (success) {
                        $(".variationLoader"+batchID).hide();
                            $(".variationRefreshPrice"+batchID).show();
                    },
                    error: function () {
                        alert("error in loading");
                    }
                });
            }
        }
    }

</script>
@endpush
