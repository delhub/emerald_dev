<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Products\WarehouseInventories;
use App\Models\Warehouse\Order\WarehouseOrder;
use App\Models\Warehouse\Location\Wh_location;
use Illuminate\Support\Facades\DB;

class CreateWhOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $location = new Wh_location;
        $location->location_key = 'MYSGRWH001';
        $location->location_name = 'Puchong warehouse';
        $location->type = 'WH';
        $location->country_id = 'MY';
        $location->city_key = 'SGR_PCG';
        $location->state_id = '12';
        $location->save();

        Schema::create('wh_order', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('location_id');
            $table->bigInteger('inv_key');
            $table->string('type');
            $table->string('date');
            $table->integer('inv_type');
            $table->string('invoice_number');
            $table->string('delivery_order');
            $table->string('inv_reference')->nullable();
            $table->text('order_data')->nullable();
            $table->timestamps();
        });

        $allOrderInventory = WarehouseInventories::where('model_type', 'App\Models\Purchases\Order')->groupBy('model_id')->get();

        foreach ($allOrderInventory as $key => $inventory) {

            $data['location_id'] = 1;
            $data['type'] = ($inventory->inv_type == 8011) ? 'selfpickup' : 'delivery';
            $data['date'] = $inventory->created_at;
            $data['inv_type'] = $inventory->inv_type;
            $data['invoice_number'] = $inventory->inventoryDetails->purchase->purchase_number;
            $data['delivery_order'] = $inventory->inventoryDetails->delivery_order;
            $data['inv_reference'] = 'move wh_inventory to WarehouseOrder';

            foreach ($inventory->inventoryDetails->items as $item) {
                $arryaData[] = [
                    'product_name' => $item->product->parentProduct->name,
                    'product_code' => $item->product_code,
                    'quantity' => - ($item->quantity)
                ];
            }

            $data['order_data'] = json_encode($arryaData);

            $inventoryTables = new WarehouseOrder();
            $whOrderId = $inventoryTables->create($data)->id;

            //replace model_type and model_id(WarehouseOrder)
            $whInventory = WarehouseInventories::where('model_type', 'App\Models\Purchases\Order')->where('model_id', $inventory->model_id)->get();
            foreach ($whInventory as $key => $inv) {
                $inv->outlet_id = 1;
                $inv->model_type = 'App\Models\Warehouse\Order\WarehouseOrder';
                $inv->model_id = $whOrderId;
                $inv->save();
            }
        }

        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock
        AS
        SELECT product_code, outlet_id,

        sum(case when inv_type = '8010' OR inv_type = '8000'
        then inv_amount
        else 0
        END)
        AS physical_stock,

        sum(case when inv_type = '8001'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved,

        sum(case when inv_type = '8011'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved_offline,

        sum(case when inv_type = '8000' OR inv_type = '8001' OR inv_type = '8011' OR inv_type = '8010'
        then inv_amount
        else 0
        end)
        as virtual_stock

        from wh_inventories
        GROUP BY product_code, outlet_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('WarehouseOrder');
    }
}
