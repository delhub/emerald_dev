@php

$menu = [
    'home-tab' => ['route_id' => 'shop.dashboard.customer.home', 'icon' => '/images/icons/dashboard-icons/md-overview-icon.png', 'title' => 'Overview'],
    'income-tab' => ['route_id' => 'shop.customer.orders', 'icon' => '/images/icons/dashboard-icons/md-value-record-icon.png', 'title' => 'Value Record'],
    'perfect-tab' => ['route_id' => 'shop.wishlist.home', 'icon' => '/images/icons/dashboard-icons/md-perfectlist-icon.png', 'title' => 'Perfect List'],
    'voucher-tab' => ['route_id' => 'shop.voucher.list', 'icon' => '/images/icons/dashboard-icons/md-voucher-icon.png', 'title' => 'Vouchers'],
    'promo-tab' => ['route_id' => 'shop.promo.list', 'icon' => '/images/icons/dashboard-icons/md-promo-icon.png', 'title' => 'Promotion/ Special Offer'],
    'rbs-tab' => ['route_id' => 'shop.rbs.remind', 'icon' => '/images/icons/dashboard-icons/md-promo-icon.png', 'title' => 'Recurring Booking System (RBS)', 'child-one' => ['route_id' => 'shop.rbs.remind', 'icon' => '/images/icons/dashboard-icons/yearly-icon.png', 'title' => 'Postpaid Method'], 'child-two' => ['route_id' => 'shop.rbs.product-racking', 'icon' => '/images/icons/dashboard-icons/monthly-icon.png', 'title' => 'Prepaid Method']],
    'chg-pw-tab' => ['route_id' => 'shop.change.password.form', 'icon' => '/images/icons/dashboard-icons/md-chg-pw-icon.png', 'title' => 'Change Password'],
    'ct-shop-tab' => ['route_id' => 'shop.index', 'icon' => '/images/icons/dashboard-icons/md-continue-shop-icon.png', 'title' => 'Continue Shopping'],
];

@endphp
<!-- 2021 -->
{{-- <div class="wrapper"> --}}
{{-- <div class="sidebar sidebar-bg-color h-100"> --}}
{{-- <div class="col-lg-3 ad-box"> --}}
<div class="flex-column my-do-text">
    <h4 class="my-nav ad-tittle">My Dashboard</h4>
</div>

<!-- Dynamic menu mobile start-->
<div id="ag-u-db-m" class="user db-slide responsive ad slider">

    @foreach ($menu as $key => $_menu)
        <div class="" id="{{ $key }}">
            @if ($key == 'rbs-tab')
                <a @if ($_menu['route_id'] == Route::currentRouteName() || $_menu['child-one']['route_id'] == Route::currentRouteName() || $_menu['child-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="#" data-toggle="modal" data-target=".statementSubmenu">
                    <img class="do-menuicon-m" src={{ asset($_menu['icon']) }}>
                    <p>{{ $_menu['title'] }}</p>
                </a>


            @else
                <a @if ($_menu['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="{{ route($_menu['route_id']) }}">
                    <img class="do-menuicon-m" src={{ asset($_menu['icon']) }}>
                    <p>{{ $_menu['title'] }}</p>
                </a>
            @endif
        </div>
    @endforeach

</div>

<!-- Dynamic menu web start-->
<nav id="ag-u-db" class="user sidebar-nav">
    <ul class="nav flex-column my-do-text nav-pills mb-3">
        @foreach ($menu as $key => $_menu)
            <li class="nav-item my-nav" id="{{ $key }}">

                @if ($key == 'rbs-tab')
                    <a class="nav-link" href="{{ route($_menu['route_id']) }}" data-toggle="collapse"
                        data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <p>{{ $_menu['title'] }}</p>
                        <i class="rbs-i-drop fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <div @if ($_menu['child-one']['route_id'] == Route::currentRouteName() || $_menu['child-two']['route_id'] == Route::currentRouteName()) class="collapse navbar-collapse in show" @else  class="collapse navbar-collapse" @endif id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a @if ($_menu['child-one']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="{{ route($_menu['child-one']['route_id']) }}">
                                    <p>{{ $_menu['child-one']['title'] }}</p>
                                </a>
                            </li>
                            <li class="nav-item">

                                <a @if ($_menu['child-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="{{ route($_menu['child-two']['route_id']) }}">
                                    <p>{{ $_menu['child-two']['title'] }}</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                @else
                    <a @if ($_menu['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="{{ route($_menu['route_id']) }}">
                        <p>{{ $_menu['title'] }}</p>
                    </a>
                    {{-- <a @if ($_menu['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif  href="{{route($_menu['route_id'])}}">
               <p>{{$_menu['title']}}</p>
            </a> --}}
                @endif

            </li>
        @endforeach
    </ul>
</nav>

<div class="modal fade statementSubmenu" tabindex="-1" role="dialog" aria-labelledby="statementList" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="pop-i-menu modal-content">
            <button type="button" class="close i-back-btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @foreach ($menu as $key => $_menu2)
                @if ($key == 'rbs-tab')
                    {{-- <p>yes it is</p> --}}
                    <a @if ($_menu2['child-one']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="{{ route($_menu2['child-one']['route_id']) }}">
                        <img src={{ asset($_menu2['child-one']['icon']) }}>
                        <p>{{ $_menu2['child-one']['title'] }}</p>
                    </a>
                    <a @if ($_menu2['child-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="{{ route($_menu2['child-two']['route_id']) }}">
                        <img src={{ asset($_menu2['child-two']['icon']) }}>
                        <p>{{ $_menu2['child-two']['title'] }}</p>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</div>

@php
use App\Models\Sidemenus\Sidemenu;
use App\Models\Globals\Countries;

$banner_county = 'MY';
$country = country()->country_id;

if ($country == 'SG') {
    $banner_county = 'SG';
}

$menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
$allMenus = Sidemenu::pluck('menu_title', 'id')->all();
@endphp

@foreach ($menus as $menu)
    @if ($menu->menu_group == 'UM' && $menu->menu_show == 1 && $menu->menu_parent == '0' && $menu->country_id == $banner_county)
        <li class="nav-item my-nav">
            {{-- <div class="icon-text-align"> --}}
            <a data-toggle="tab" class="nav-link" href="{{ $menu->menu_link }}">
                {{-- <img src="{{asset('/storage/customer/sidebar-icons/' . $menu->menu_icon)}}" alt="{{ $menu->menu_title }}" style="height: 100px; width: 100px;"> --}}
            </a>

            {{-- <ul class="list-unstyled components" style="padding: 0px 0;">
                  @if (count($menu->childs))
                        @include('layouts.shop.navigation.menusub',['childs' => $menu->childs])
                  @endif
                  </ul> --}}
            {{-- </div> --}}
        </li>

    @endif
@endforeach
</ul>
</nav>

@push('script')
    <script>
        $('.responsive.ad').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerPadding: '50px',
            autoplay: false,
            arrows: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        infinite: true,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        centerPadding: '100px',
                        slidesToScroll: 3,
                        arrows: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        centerPadding: '50px',
                        slidesToScroll: 3,
                        arrows: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        const v = @JSON($menu);
        //console.log(v);
    </script>
@endpush
