@extends('layouts.administrator.main')

@section('content')

<h1>Global Settings</h1>
@if($errors->any())
<h5 class="bg-success text-white px-md-2 py-md-2">{{$errors->first()}}</h5>
@endif
{{-- <h5 >'asdasd'</h5> --}}

<div class="card shadow-sm">
    <div class="card-body">
        <form action="{{ route('administrator.settings.store') }}" method="post">
            @csrf
            <div class="table-responsive m-2">
                @if(count($settings) > 0)
                @foreach ($settings as $setting)
                <div class="mb-3 col-md-8">
                    <label for="{{ $setting->name }}" class="form-label">{{ $setting->title }}</label>
                    <input type="{{ $setting->type }}" class="form-control" id="{{ $setting->name }}"
                        name="{{ $setting->name }}" value="{{ $setting->value }}">
                </div>
                @endforeach
                @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Setting found</p>
                @endif
            </div>
            <div>
                <div class="row">
                    <div class="col-12 text-left pl-md-5 p-2">
                        <button class="btn btn-primary" style=" font-style: normal; border-radius: 5px;"
                            type="submit">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
