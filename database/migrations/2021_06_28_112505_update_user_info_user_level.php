<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateUserInfoUserLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //update all related dealer table account_id/referrer_id/agent_id to bigInteger
        Schema::table('dealer_addresses', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('dealer_bank', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('dealer_contacts', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('dealer_country', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('dealer_data', function (Blueprint $table) {
            $table->bigInteger('dealer_id')->unsigned()->change();
        });
        Schema::table('dealer_employments', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });

        Schema::table('dealer_infos', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
            $table->bigInteger('referrer_id')->unsigned()->change();
        });

        Schema::table('dealer_registrations', function (Blueprint $table) {
            $table->bigInteger('agent_id')->unsigned()->change();
        });
        Schema::table('dealer_sales', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('dealer_spouses', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });


        //update all related users table account_id/referrer_id to bigInteger
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->autoIncrement()->change();
        });
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('user_contacts', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('user_country', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
        });
        Schema::table('user_infos', function (Blueprint $table) {
            $table->bigInteger('account_id')->unsigned()->change();
            $table->bigInteger('referrer_id')->unsigned()->change();
        });

        DB::table('user_infos')
            ->update(
                ['user_level' => '6001']
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
