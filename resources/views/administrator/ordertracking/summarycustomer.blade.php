@extends('layouts.administrator.main')
@section('content')

    <div style="font-size:small;">

        <div class="row">
            <div class="col-12 mb-3">
                <h1 style="display:inline;">Order Tracking Summary (Customer) </h1>
            </div>
        </div>

        <form action="{{ route('administrator.ordertracking.summary.customer') }}" method="GET">
            <div class="row mt-2 ml-2">

                <!-- Panel filter -->
                <div class="col-md-auto px-1">
                    <select name="panel" id="panel" class="col-12 text-left custom-select">
                        <option value="all" selected>All Panels</option>
                        @foreach ($panels as $panel)
                        <option value="{{ $panel->account_id }}"
                            {{ $request->panel == $panel->account_id ? 'selected' : ''}}>
                            {{ $panel->company_name ?? '' }}
                        </option>

                        @endforeach
                    </select>
                </div>

                <!-- Date filter from date to date-->
                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{ ($request->datefrom) ? $request->datefrom : $request->dateto }}" autocomplete="off">
                        {{-- <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </div> --}}
                    </div>
                </div>

                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{ ($request->dateto) ? $request->dateto : $request->datefrom }}" autocomplete="off">
                        {{-- <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </div> --}}
                    </div>
                </div>

                <div class="col-md-auto px-1">
                    <button type="submit" class="btn btn-warning" style="color:black;" border="2px solid">Filter</button>
                </div>
                <div>
                    <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.ordertracking.summary.customer') }} style="color:black;">Reset Filter</a></button>
                </div>
                <div  class="col-md-auto px-1">
                    <button type="submit" class="btn btn-warning" style="color:black;"><a href="{{route('administrator.ordertracking.exportCsv.customer', ['status'=> $request->status, 'states'=> $request->states, 'datefrom' => $request->datefrom, 'dateto' =>  $request->dateto, 'selectSearch' => $request->selectSearch , 'searchBox' => $request->searchBox])}}" id="export" style="color:black; ">Download</a></button>
                </div>

                <div class="col-md-auto ml-auto">
                    <div class="row">
                        <div class="col-md-8 p-0">
                            <select class="custom-select" id="selectSearch" name="selectSearch">
                                <option value="ALL" selected>Choose...</option>
                                <option value="NAME" {{ $request->selectSearch == 'NAME' ? 'selected' : '' }}>Product Name</option>
                                <option value="CODE" {{ $request->selectSearch == 'CODE' ? 'selected' : '' }}>Product Code</option>
                            </select>

                            <input type="text" class="form-control" id="" name="searchBox"
                                value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                        </div>
                        <div class="col-md-4 ">
                            <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
        <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Item) :  <b>{{ count($ForCountcustomerOrders)}} </b></span></p>
        <div class="row">
            <div class="col-auto ml-auto">
                {!! $items->render() !!}
            </div>
        </div>

    </div>

    <div class="col-12 sales-product">
        <div class="row">
            <div class="col-12">
                <div class="tableFixHead">
                    <table class="table table-bordered text-center" id="sales-tracking">

                        <thead>
                            <tr>
                                <th scope="col">Product Code/ID</th>
                                <th scope="col">Product Name </th>
                                <th scope="col">Panel</th>
                                <th scope="col">Item Placed</th>
                                <th scope="col">Item Shipped</th>
                                <th scope="col">Item Delivered</th>
                                <th scope="col">Item Cancel</th>
                                <th scope="col">Item Pending Payment</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($list as $key => $listresult)
                                <tr class="tr1">

                                    <td rowspan="3">
                                        {{ $key  }}
                                    </td>

                                    <td rowspan="3">
                                        @if(is_object( $listresult['item']->product) )
                                        {{$listresult['item']->product->parentProduct->name }}
                                        @endif

                                        <br>

                                        @if(array_key_exists('product_color_name', $listresult['information']))
                                        <p class="text-capitalize text-secondary m-0">Color:
                                            {{ $listresult['information']['product_color_name'] }}
                                        </p>
                                        @endif
                                        @if(array_key_exists('product_size', $listresult['information']))
                                        <p class="text-capitalize text-secondary m-0">Size:
                                            {{ $listresult['information']['product_size'] }}
                                        </p>
                                        @endif
                                        @if(array_key_exists('product_curtain_size', $listresult['information']))
                                        <p class="text-capitalize text-secondary m-0">Curtain Model:
                                            {{ $listresult['information']['product_curtain_size'] }}
                                        </p>
                                        @endif
                                        @if(array_key_exists('product_miscellaneous', $listresult['information']))
                                        <p class="text-capitalize text-secondary m-0">
                                            {{ $listresult['information']['product_miscellaneous'] }}
                                        </p>
                                        @endif
                                        @if(array_key_exists('invoice_number', $listresult['information']))
                                        <p class="text-capitalize text-secondary m-0">Invoice Number:
                                            {{ $listresult['information']['invoice_number'] }}
                                        </p>
                                        @endif
                                        @if(array_key_exists('product_temperature', $listresult['information']))
                                        <p class="text-capitalize text-secondary m-0">Color Temperature:
                                            {{ $listresult['information']['product_temperature'] }}</p>
                                        @endif
                                    </td >

                                    <td rowspan="3">
                                        {{ $listresult['item']->order->panel ? $listresult['item']->order->panel->company_name : '' }}
                                    </td>
                                </tr>

                                    <tr class="tr2">
                                        <td>{{ $listresult['order_placed']['case'] }}</td>
                                        <td>{{ $listresult['order_shipped']['case'] }}</td>
                                        <td>{{ $listresult['order_delivered']['case'] }}</td>
                                        <td>{{ $listresult['order_cancel']['case'] }}</td>
                                        <td>{{ $listresult['order_pending_payment']['case'] }}</td>
                                        <td>{{ $listresult['total']['case'] }}</td>
                                    </tr>

                                    <tr class="tr1">
                                        {{-- <td bgcolor="#E8E8E8"> --}}
                                        <td>
                                            @if (isset($listresult['order_placed']['case']) && !empty($listresult['order_placed']['case']))
                                            <form action="{{ route('administrator.ordertracking.detail')}}">
                                                <input type="hidden" name="manyID" value="{{ implode("," , $listresult['order_placed']['itemsid'] ) }}">
                                                <button type="submit">Detail</button>
                                            </form>
                                            @endif

                                        </td>
                                        <td>
                                            @if (isset($listresult['order_shipped']['case']) && !empty($listresult['order_shipped']['case']))
                                            <form action="{{ route('administrator.ordertracking.detail')}}">
                                                <input type="hidden" name="manyID" value="{{ implode("," , $listresult['order_shipped']['itemsid'] ) }}">
                                                <button type="submit">Detail</button>
                                            </form>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($listresult['order_delivered']['case']) && !empty($listresult['order_delivered']['case']))
                                            <form action="{{ route('administrator.ordertracking.detail')}}">
                                                <input type="hidden" name="manyID" value="{{ implode("," , $listresult['order_delivered']['itemsid'] ) }}">
                                                <button type="submit">Detail</button>
                                            </form>
                                            @endif

                                        </td>
                                        <td>
                                            @if (isset($listresult['order_cancel']['case']) && !empty($listresult['order_cancel']['case']))
                                            <form action="{{ route('administrator.ordertracking.detail')}}">
                                                <input type="hidden" name="manyID" value="{{ implode("," , $listresult['order_cancel']['itemsid'] ) }}">
                                                <button type="submit">Detail</button>
                                            </form>
                                            @endif

                                        </td>
                                        <td>
                                            @if (isset($listresult['order_pending_payment']['case']) && !empty($listresult['order_pending_payment']['case']))
                                            <form action="{{ route('administrator.ordertracking.detail')}}">
                                                <input type="hidden" name="manyID" value="{{ implode("," , $listresult['order_pending_payment']['itemsid'] ) }}">
                                                <button type="submit">Detail</button>
                                            </form>
                                            @endif

                                        </td>
                                        <td>
                                            <form action="{{ route('administrator.ordertracking.detail')}}">
                                                <input type="hidden" name="manyID" value="{{ implode("," , $listresult['total']['itemsid'] )  }}">

                                            <button type="submit">Detail</button>
                                            </form>
                                        </td>
                                    </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
                <div class="col-auto ml-auto py-3">
                    {!! $items->render() !!}
                </div>
        </div>
    </div>

@endsection

@push('script')
<script>

    $(function() {

        $(".date").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0,
            changeMonth: true,
            changeYear: true
            });

            $(".filterDate").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            changeMonth: true,
            changeYear: true
            });

    });

</script>
@endpush

@push('style')
    <style>
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

        table  { border-collapse: collapse; width: 100%; }
        th     { background:#eee; }
        .tr1:nth-child(odd) {background-color: #f2f2f2;}
        .tr2:nth-child(even) {background-color: #f2f2f2;}
    </style>
@endpush
