<?php

use App\Models\Faq\Faq;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class AddFaqToFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Faq::truncate();
        DB::table('faq')->insert([

            [
                'id' => 1,
                'faq_type' => 13,
                'question' => 'How to check my order?',
                'answer' => '<p>Step 1: At BUJISHU home page, click on “MY ACCOUNT” at the top right hand corner, select “My Dashboard”.</p>
                           <p>Step 2: Select “Value Records”.</p>
                           <p>Step 3: Select the Purchase# and you may find the order status in there.</p>',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'faq_type' => 10,
                'question' => 'How to Contact Bujishu Customer Service?',
                'answer' => '<p>Sent us an email at <b>bujishu-cs@delhubdigital.com</b>.  Our working hour is Monday to Friday <b>9:00am - 6:00pm</b>.</p>
                          <p>Note: Please provide the invoice number (if possible) in the email.   This would be very helpful for Customer Service to serve you.</p>',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'faq_type' => 11,
                'question' => 'How to reset password?',
                'answer' => '<p>Step 1: At BUJISHU login page, click on “Forgot Password”</p>
                            <p>Step 2: Enter your login ID which is your email address and click “Sent Password Reset Link”</p>
                            <p>Step 3: Check your electronic mail box, you will receive an email from us “Reset Password Notification”.  Just click on the “Reset Password” button in the email.</p>
                            <p>Step 4:  Enter a new password and reconfirm the new password.</p>',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 4,
                'faq_type' => 10,
                'question' => 'How to update my personal data?',
                'answer' => '<p>Step 1: At BUJISHU home page, click on “MY ACCOUNT” at the top right hand corner, select “Profile”.</p>
                            <p>Step 2: Under “My Profile”, click the edit icon <i class="px-2 fas fa-edit" style="color: #ffcc00;" data-toggle="modal"></i> next to "Profile Information" to
                            update your billing address, shipping address, and phone numbers.  Once completed, click on “Update Profile” to save your data.</p>',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 5,
                'faq_type' => 13,
                'question' => 'Can I change my shipping address after placing an order?',
                'answer' => '<p>Shipping address can be change before payment made.  Once payment is made, shipping address CANNOT change.</p>
                            <p style="font-weight: 700;">Scenario 1: Update shipping address permanently.</p>
                            <p>Step 1: At BUJISHU home page, click on “MY ACCOUNT” at the top right hand corner, select “Profile”.</p>
                            <p>Step 2: Under “My Profile”, click on the edit icon <i class="px-2 fas fa-edit" style="color: #ffcc00;" data-toggle="modal"></i> beside the field, update your shipping address.  Once completed, click on “Update Profile” to save your profile.</p>
                            <p style="font-weight: 700;">Scenario 2: Update shipping address for the specific purchase.</p>
                            <p>Step 1: Go to BUJISHU website to do shopping and add selected items to your shopping cart.</p>
                            <p>Step 2: Go to your shopping cart at the top right hand corner below “MY ACCOUNT”, </p>
                            <ol type="i">
                            <li>Select the items you wanted to check out.</li>
                            <li>At the “Ship to”, please check the shipping address for this purchase.  If the shipping address is not correct, please click edit icon <i class="px-2 fas fa-edit" style="color: #ffcc00;" data-toggle="modal"></i> next to the “Ship to” and update the correct shipping address.</li>
                            <li>Click on “PROCEED TO CHECKOUT” button.  Please ensure the shipping details (Name, contact, and shipping address) are correct.</li>
                            </ol>
                            <p>Step 3: Select the Payment Method and complete the payment.</p>',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 6,
                'faq_type' => 11,
                'question' => 'My customer is unable to login after registration.',
                'answer' => '<p style="font-weight: 700;">Scenario 1:  Your customer receive a message “Verify  Your Email Address”.</p>
                            <p>Please request agent to check his/her mail box for the verification email.(If email not in Inbox folder, do also check in the Spam folder)</p>
                            <p>Note: The verification email is valid for 24 hours only.</p>
                            <p style="font-weight: 700;">Scenario 2:  Your customer receive a message “These credentials do not match our records.”.</p>
                            <ol type="i">
                            <li>Please confirm your customer has successfully registered. </li>
                            <li>Please ensure email address and password are entered correctly.</li>
                            <li>After you have confirm i and ii, you may try to reset your password.  Refer to FAQ “How to reset password?”</li>
                            </ol>
                            <p>If all the above is not working, sent an email to <b>bujishu-cs@delhubdigital.com</b>.</p>',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 7,
                'faq_type' => 12,
                'question' => 'My transaction hang/time out during payment.',
                'answer' => '<p>Step 1: Check your order in “My Dashboard”.  Refer to FAQ “1. How to check my order?”</p>
                            <p>If the purchase does not exist in the Value Records, proceed to step 2.</p>
                            <p>If the purchase status is “FAILED”, please resubmit your purchase.</p>
                            <p>Step 2: If payment by <b>Credit / Debit Card</b>, kindly contact your bank to confirm if the payment was successful. If payment was successful, sent an email to <b>bujishu-cs@delhubdigital.com</b>.If payment was not successful, please resubmit your purchase again.</p>
                            <p>For order payment type like <b>Offline payment</b> or <b>Blue Point</b>, please sent an email to <b>bujishu-cs@delhubdigital.com</b> to check your purchase.',
                'faq_status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Faq::truncate();
    }
}