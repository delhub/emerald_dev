@extends('emails.partials.email-wrapper')

@section('content')

    <div class="content-box">
        <p class="card-text">Hello {{ $recipient_name }}!</p>
        <p class="card-text">
            {!! $content !!}
        </p>
        <div style="height: 40px;"></div>
        <p class="card-text mb-1">Regards,<br />
            <strong>Formula2u.com</strong>
        </p>
    </div>
@endsection
