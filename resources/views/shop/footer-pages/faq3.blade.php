@extends('layouts.shop.main')

@section('content')

<div class="container mt-5" style="min-height: 75vh;margin-top: 6rem!important;">

    <div class="row">
        <div class="col-12 offset-md-4 col-md-6">
            <h1 style="color:#263a8b; font-weight:700;">We're here to help</h1>
            <h5 class="ml-md-4" style="color:#263a8b;">Frequently asked Questions</h5>
        </div>
    </div>
   
      <div class="row mt-md-6" >
        
          <div class="col-md-2 " data-toggle="buttons" >

            <button type="button" class="my btn btn-block btn-toggle focus-only" data-target="all" 
                aria-expanded="true" aria-controls="global_statuses-all" style="text-transform:uppercase;"><b>All</b></button>
            @if(count($statuses) > 0)
            @foreach($statuses as $Status)
            {{-- <p>{{$Status->id}} --}}
                
            <button type="button" class="my btn btn-block btn-toggle focus-only" style="text-transform: uppercase;"  data-target="global_statuses-{{$Status->id}}" 
            aria-expanded="true" aria-controls="global_statuses-{{$Status->id}}"><b>{{$Status->name}}</b></button>
            
            @endforeach
            @endif
          </div>
        
            <div class="col-md-10 abc2" id="mainButton">

            @if(count($faqs) > 0)
            @foreach($faqs as $faq)
            {{-- <p>{{$faq->faq_type}}</p>  --}}
                                       
                <div class="mg-25 accordion col-md-10 global_statuses-{{$faq->faq_type}} " >
                <div class="card" style="border-radius:5px;">   
                    <div class="card-header p-1"  style="background-color: #e7e7e7; color: black;">      

                            <button class="btn-2 col-12 btn btn-link stretched-link panel-heading accordion-toggle collapsed"  type="button" data-toggle="collapse" data-target="#collapse-{{$faq->id}}" aria-expanded="true" aria-controls="" >                 
                                <h6 class="mc-25 mb-1">
                                <i class="fa fa-question-circle" aria-hidden="true"></i> 
                                <b>{{$faq->question}}</b> 
                                </h6>            
                            </button>                          
                    </div>

                        <div id="collapse-{{$faq->id}}"  data-parent="#mainButton" class="pa-1 panel-collapse collapse button-item" aria-labelledby="headingOne" >
                            <div class="card-body">
                                <div class="row">
                                    
                                    <div class="col-sm-10">
                                        {!! $faq->answer !!}
                                    </div>
                                </div>
                            </div>
                        </div>      
                </div> 
                </div>  
                     
            @endforeach
            @endif
            
      </div>       
</div>     
@endsection
@push('style')
<style>
    .btn-2 {
        padding: 0.375rem 0.75rem !important;
        text-align: left;
        width: 100% !important;
    }
    .my.btn.btn-block.btn-toggle.focus-only {
        text-align: left;
        background-color: #f8fafc;
        border: 2px solid #e7e7e7;
        margin: 0px 10px 10px 10px;
        padding: 10px 10px;
        width: 100%;
    }
    .my.btn.btn-block.btn-toggle.focus-only:focus {
        background-color: #e7e7e7;
        border: solid 3px #263a8b;
        box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
    }
    @media only screen and (max-width: 768px) {
        .my.btn.btn-block.btn-toggle.focus-only {
            text-align: left;
            background-color: #f8fafc;
            border: 2px solid #e7e7e7;
            height: 35px;
            margin: 5px;
            padding: 5px 5px;
            font-size: 12px;
            width: fit-content;
        }
        .btn {
            float: left;
        }
        div[class^="col-"] {
            margin-bottom: 0;
        }
        .mg-25 {
            margin-left: 0px;
            padding: 3px;
        }
    }
</style>
@endpush
@push('script')

    <script>
        $(".btn-toggle").click(function(){
            var button = $(this);
            var thisclass = button.data('target');
            var faq_items = $(".accordion");
            // put addClass = btn-secondary

            if (thisclass == 'all'){
  
                faq_items.each(function(){
                    if (! $(this).is(':visible')){

                        $(this).toggle();
                        // $(this).toggle('slow');
                           
                        // $(this).fadeToggle(200);           

                    }

                });
              
            } else{

                faq_items.each(function(){
                    if ( $(this).hasClass(thisclass)){

                        if (! $(this).is(':visible')){

                        $(this).toggle();
                        // $(this).toggle('slow');

                        // $(this).fadeToggle(200);                               
                    }
                      
                    } else{
                        if ( $(this).is(':visible')){

                        $(this).toggle();
                        // $(this).toggle('slow');

                        // $(this).fadeToggle(200);

                        }
                    }
                });              
            }
        });       
    </script>
@endpush