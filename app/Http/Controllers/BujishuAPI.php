<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App;

class BujishuAPI extends Controller
{
    public static $client;
    public static $apiServer;

    public static function apiServer($country = null)
    {
        if($country == null) $country = country()->country_id;

        if ($country == 'MY'){
            if (App::environment('prod')) {
                return 'api.formula2u.com';
            } else {
                return 'emerald-staging.dcvital.com';
            }
        }

    }

    public static function createUser($bjs_user_id, $name, $upline_agent_id, $ic, $email, $referral_name, $group_id, $create_bjs_agent = '', $agent_id = 0, $bp_award = 0)
    {

        return;
        $body = array(
            'bjs_user_id' => $bjs_user_id,
            'name' => $name,
            'upline_agent_id' => $upline_agent_id,
            'ic' => $ic,
            'email' => $email,
            'create_bjs_agent' => $create_bjs_agent,
            'agent_id' => $agent_id,
            'referral_name' => $referral_name,
            'group_id' => $group_id,
            'bp_award' => $bp_award,
            'country' => country()->country_id
        );

        $client = new Client();
        $response = $client->post(
            "https://" . self::apiServer() . "/wp-json/bjs/v1/customer/",
            ['form_params' => $body, 'http_errors' => false]
        );

        return  $response;
    }

    public static function get_monthly_statement($agent_id, $page = 'current')
    {

        $client = new Client();
        $request = $client->get(
            "https://" . self::apiServer() . "/wp-json/bjs/v1/agent/" . $agent_id . "/statement/" . $page,
            [
                'http_errors' => false,
            ]
        );

        $responseStatus = $request->getStatusCode();
        if ($responseStatus == 200) {
            $response = json_decode($request->getBody(), true);


        } else {

            $response = false;
        }
        return  $response;
    }

    public static function get_yearly_statement($agent_id, $year)
    {

        $client = new Client();
        $request = $client->get(
            "https://" . self::apiServer() . "/wp-json/bjs/v1/agent/" . $agent_id . '/yearly-statement/'. $year,
            [
                'http_errors' => false,
                'verify' => false
            ]
        );

        $responseStatus = $request->getStatusCode();
        if ($responseStatus == 200) {
            $response = json_decode($request->getBody(), true);


        } else {

            $response = false;
        }
        return  $response;
    }

    public static function get_yearly_available($agent_id)
    {

        $client = new Client();
        $request = $client->get(
            "https://" . self::apiServer() . "/wp-json/bjs/v1/agent/" . $agent_id . "/yearly-available/",
            [
                'http_errors' => false,
                'verify' => false
            ]
        );

        $responseStatus = $request->getStatusCode();
        if ($responseStatus == 200) {
            $response = json_decode($request->getBody(), true);
        } else {

            $response = false;
        }
        return  $response;
    }

    public static function createOrder($user_account_id, $bjs_number, $total_amount, $gateway, $items, $transaction_id)
    {
      return; //disable create ordre

        $body = array(
            'bjs_user_id' => $user_account_id,
            'order_id' => $bjs_number,
            'total' => $total_amount,
            'gateway' => $gateway,
            'product' => $items,
            'transaction_id' => $transaction_id,
        );
        $client = new Client();
        $response = $client->post(
            "https://" . self::apiServer() . "/wp-json/bjs/v1/order",
            ['form_params' => $body, 'http_errors' => false]
        );

        return  $response;
    }

    public static function changeOrderStatus($purchase, $status)
    {

        return;

        $bjn_number = $purchase->getFormattedNumber();
        $body = array(
            'status' => $status,
        );
        $client = new Client();
        $response = $client->post(
            "https://" . self::apiServer($purchase->country_id) . "/wp-json/bjs/v1/order/".$bjn_number,
            ['form_params' => $body, 'http_errors' => false]
        );

        return $response;
    }

    public static function updateBank($dealer_id, $bank_code, $bank_acc, $bank_acc_name, $account_regid)
    {
       return;
        $body = array(
            'bank_code' => $bank_code,
            'bank_acc' => $bank_acc,
            'bank_acc_name' => $bank_acc_name,
            'account_regid' => $account_regid,

        );
        $client = new Client();
        $response = $client->post(
            "https://" . self::apiServer() . "/wp-json/bjs/v1/agent/{$dealer_id}/update_bank",
            ['form_params' => $body, 'http_errors' => false]
        );

        return  $response;
    }


}
