<?php

namespace App\Traits;

use App\Models\Globals\Products\Product as GP;
use App\Models\Products\Product as PP;
use App\Models\Products\ProductAttribute as PA;
use App\Models\Products\ProductPrices;
use App\Models\Globals\Countries;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Users\Panels\PanelAddress;
use App\Models\Categories\Category;
use App\Models\Categories\PivCategory;

use function GuzzleHttp\json_decode;

trait ImportProduct
{
    public function importProduct($data)
    {
        if (count($data)) {
            foreach ($data as $k => $v) {
                if (!empty($v['web_fixed_price'])) {
                    switch (strtolower($v['type'])) {
                        case 'attribute':
                            $gp = GP::where('product_code', $v['product_code_main'])->first();

                            if ($gp) {

                                $pp = PP::where('global_product_id', $gp->id)->first('id');

                                // panel product attribute
                                $pa = PA::where([['product_code', $v['product_code']]])->first();

                                if (!$pa) {
                                    $pa = new PA;
                                } else {
                                    //$pa->delete();
                                }

                                $pa->panel_product_id = $pp->id;
                                $pa->attribute_type = 'size';
                                $pa->attribute_name = $v['attribute'];
                                $pa->product_sv = isset($v['product_sv']) ? $v['product_sv'] : 0;
                                // $pa->color_images = $v[30];
                                $pa->product_code = $v['product_code'];
                                $pa->product_measurement_length = isset($v['product_measurement_length']) ? $v['product_measurement_length'] : 0;
                                $pa->product_measurement_width = isset($v['product_measurement_width']) ? $v['product_measurement_width'] : 0;
                                $pa->product_measurement_depth = isset($v['product_measurement_depth']) ? $v['product_measurement_depth'] : 0;
                                $pa->product_measurement_weight = isset($v['product_measurement_weight']) ? $v['product_measurement_weight'] / 1000 : 0;

                                $pa->allow_rbs = isset($v['allow_rbs']) ? $v['allow_rbs'] : 0;
                                $pa->allow_reminder = isset($v['allow_reminder']) ? $v['allow_reminder'] : 0;
                                $pa->rbs_frequency = isset($v['rbs_frequency']) ? explode(',',$v['rbs_frequency']) : NULL;

                                if(isset($v['rbs_times_send_3_price']) || isset($v['rbs_times_send_4_price']) || isset($v['rbs_times_send_5_price'])) {

                                    $rbs_time_send_3_price = isset($v['rbs_times_send_3_price']) ? $v['rbs_times_send_3_price'] * 100 : NULL;
                                    $rbs_time_send_3_qty = isset($v['rbs_times_send_3_qty']) ? $v['rbs_times_send_3_qty'] : NULL;
                                    $rbs_time_send_3_shipping_category = isset($v['rbs_times_send_3_shipping_category']) ? $v['rbs_times_send_3_shipping_category'] : NULL;

                                    $rbs_time_send_4_price = isset($v['rbs_times_send_4_price']) ? $v['rbs_times_send_4_price'] * 100 : NULL;
                                    $rbs_time_send_4_qty = isset($v['rbs_times_send_4_qty']) ? $v['rbs_times_send_4_qty'] : NULL;
                                    $rbs_time_send_4_shipping_category = isset($v['rbs_time_send_4_shipping_category']) ? $v['rbs_time_send_4_shipping_category'] : NULL;

                                    $rbs_time_send_5_price = isset($v['rbs_times_send_5_price']) ? $v['rbs_times_send_5_price'] * 100 : NULL;
                                    $rbs_time_send_5_qty = isset($v['rbs_times_send_5_qty']) ? $v['rbs_times_send_5_qty'] : NULL;
                                    $rbs_time_send_5_shipping_category = isset($v['rbs_time_send_5_shipping_category']) ? $v['rbs_time_send_5_shipping_category'] : NULL;

                                    $rbs_time_send_merge = '{"3":{"price":"' . $rbs_time_send_3_price . '","qty":"' . $rbs_time_send_3_qty . '","shipping_category":"' . $rbs_time_send_3_shipping_category . '"},"4":{"price":"' . $rbs_time_send_4_price . '","qty":"' . $rbs_time_send_4_qty . '","shipping_category":"' . $rbs_time_send_4_shipping_category . '"},"5":{"price":"' . $rbs_time_send_5_price . '","qty":"' . $rbs_time_send_5_qty . '","shipping_category":"' . $rbs_time_send_5_shipping_category . '"}}';

                                    $rbs_time_send_json = json_decode($rbs_time_send_merge, true);


                                    if(count($rbs_time_send_json)) {
                                        foreach ($rbs_time_send_json as $key => $val) {
                                            if(empty($val['price'])){
                                                unset($rbs_time_send_json["$key"]);
                                            }

                                            if($val['shipping_category'] == NULL) {
                                                unset($rbs_time_send_json["$key"]["shipping_category"]);
                                            }
                                        }
                                    }

                                    $pa->rbs_times_send = $rbs_time_send_json;
                                }

                                $pa->save();

                                $this->InsertPanelProductPrice($v, $pa->id, 'attribute');
                            }

                            break;
                            // case 2:
                            //     break;
                        default:
                            // Global product

                            $gp = GP::where('product_code', $v['product_code_main'])->first();
                            $att = PA::where('product_code', $v['product_code'])->first();

                            if(true) {
                                if (!$gp) {
                                    $gp = new GP;
                                }

                                if (isset($v['video'])) {
                                    $video_link = \explode(',', $v['video']);

                                    $product_data = [
                                        'video' => [
                                            'link' => $video_link,
                                            'introduction' => $v['video_introduction'],
                                            'recommend' => $v['video_recommend'],
                                        ]
                                    ];
                                } else {
                                    $product_data = [];
                                }

                                $gp->product_type = isset($v['outlet_category']) ? $v['outlet_category'] : 1;
                                $gp->moh_code = isset($v['moh_code']) ? $v['moh_code'] : null;
                                $gp->product_code = $v['product_code_main'];
                                $gp->product_code = $v['product_code_main'];
                                $gp->moh_code = isset($v['moh_code']) ? $v['moh_code'] : null;
                                $gp->name = $v['name'];
                                $gp->name_slug = isset($gp->id) ? $gp->id . '-' . str_slug($v['name']) : str_slug($v['name']);
                                $gp->quality_id = isset($v['quality_id']) ? $this->getQualityID($v['quality_id']) : 1;
                                $gp->product_status = isset($v['product_status']) ? $v['product_status'] : 1;
                                $gp->eligible_discount = isset($v['eligible_discount']) && $v['eligible_discount'] == 'Yes' ? 0 : 1;
                                $gp->generate_discount = isset($v['generate_discount']) && $v['generate_discount'] == 'Yes' ? 0 : 1;
                                $gp->can_remark = isset($v['can_remark']) ? $v['can_remark'] : 0;
                                $gp->can_selfcollect = isset($v['can_selfcollect']) ? $v['can_selfcollect'] : 0;
                                $gp->can_preorder = isset($v['can_preorder']) ? $v['can_preorder'] : 0;
                                $gp->can_installment = '{"loanAmount":null,"firstPayment":null,"monthlyPayment":null,"interestRate":null,"tenure":null,"processingFee":null,"productDescription":""}';
                                $gp->data = json_encode($product_data);
                                $gp->hot_selection = isset($v['hot_selection']) ? $v['hot_selection'] : 0;
                                $gp->key_selection = isset($v['key_selection']) ? $v['key_selection'] : 0;

                                $gp->save();

                                //save name slug
                                $gp->name_slug = isset($gp->id) ? $gp->id . '-' . str_slug($v['name']) : str_slug($v['name']);
                                $gp->save();

                                // Insert/update Panel Info
                                $this->InsertPanelInfo($v);
                                // Insert/update Panel Address
                                $this->InsertPanelAddress($v);

                                // Panel Product
                                $pp = PP::where('global_product_id', $gp->id)->first();

                                if (!$pp) {
                                    $pp = new PP;
                                }

                                $account_id = (isset($v['panel_account_id']) ? $v['panel_account_id'] : 2011000000);

                                $data_origin = isset($v['place_of_origin']) ? $v['place_of_origin'] : 'Malaysia';

                                $origin = Category::where([['name', $data_origin], ['type', 'origin_country']])->first();

                                $pivMultiple = PivCategory::where([['product_id', $gp->id], ['category_id', $origin->id]])->get();

                                if (count($pivMultiple) > 1) {
                                    // PivCategory::where([['product_id', $gp->id], ['category_id', $origin->id]])->delete();
                                }

                                //insert piv origin
                                if ($origin) {
                                    $pivCat = PivCategory::where([['product_id', $gp->id], ['category_id', $origin->id]])->first();

                                    if (!$pivCat) {
                                        $pivCat = new PivCategory;
                                    }

                                    $pivCat->product_id = $gp->id;
                                    $pivCat->category_id = $origin->id;
                                    $pivCat->save();
                                }

                                $pp->global_product_id = $gp->id;
                                $pp->panel_account_id = $account_id;
                                $pp->display_panel_name = isset($v['display_panel_name']) ? $v['display_panel_name'] : 'FORMULA HEALTHCARE SDN. BHD';
                                $pp->short_description =  isset($v['short_description']) ? $v['short_description'] : NULL;
                                $pp->product_description = isset($v['product_description']) ? $v['product_description'] : NULL;
                                $pp->place_of_origin = json_encode([$origin->id]);
                                $pp->product_content_1 = isset($v['product_content_1']) ? $v['product_content_1'] : NULL;
                                $pp->product_content_2 = isset($v['product_content_2']) ? $v['product_content_2'] : NULL;
                                $pp->product_content_3 = isset($v['product_content_3']) ? $v['product_content_3'] : NULL;
                                $pp->product_content_4 = isset($v['product_content_4']) ? $v['product_content_4'] : NULL;
                                $pp->product_content_5 = isset($v['product_content_5']) ? $v['product_content_5'] : NULL;
                                $pp->estimate_ship_out_date = isset($v['estimate_ship_out_day']) ? $v['estimate_ship_out_day'] : 0;
                                $pp->product_sv = isset($v['product_sv']) ? $v['product_sv'] : 0;
                                $pp->origin_state_id = 0;
                                // $pp->shipping_category = 0;

                                $pp->save();

                                // $this->InsertPanelProductPrice($v,$pp->id,'product');

                                // panel product attribute
                                $pa = PA::where([['product_code', $v['product_code']]])->first();

                                if (!$pa) {
                                    $pa = new PA;
                                }

                                $pa->panel_product_id = $pp->id;
                                $pa->attribute_type = 'size';
                                $pa->attribute_name = $v['attribute'];
                                $pa->product_sv = isset($v['product_sv']) ? $v['product_sv'] : 0;
                                // $pa->color_images = $v[30];
                                $pa->product_code = $v['product_code'];
                                $pa->product_measurement_length = isset($v['product_measurement_length']) ? $v['product_measurement_length'] : 0;
                                $pa->product_measurement_width = isset($v['product_measurement_width']) ? $v['product_measurement_width'] : 0;
                                $pa->product_measurement_depth = isset($v['product_measurement_depth']) ? $v['product_measurement_depth'] : 0;
                                $pa->product_measurement_weight = isset($v['product_measurement_weight']) ? $v['product_measurement_weight'] / 1000 : 0;

                                $pa->allow_rbs = isset($v['allow_rbs']) ? $v['allow_rbs'] : 0;
                                $pa->allow_reminder = isset($v['allow_reminder']) ? $v['allow_reminder'] : 0;
                                $pa->rbs_frequency = isset($v['rbs_frequency']) ? explode(',',$v['rbs_frequency']) : NULL;

                                if(isset($v['rbs_times_send_3_price']) || isset($v['rbs_times_send_4_price']) || isset($v['rbs_times_send_5_price'])) {

                                    $rbs_time_send_3_price = isset($v['rbs_times_send_3_price']) ? $v['rbs_times_send_3_price'] * 100 : NULL;
                                    $rbs_time_send_3_qty = isset($v['rbs_times_send_3_qty']) ? $v['rbs_times_send_3_qty'] : NULL;
                                    $rbs_time_send_3_shipping_category = isset($v['rbs_times_send_3_shipping_category']) ? $v['rbs_times_send_3_shipping_category'] : NULL;

                                    $rbs_time_send_4_price = isset($v['rbs_times_send_4_price']) ? $v['rbs_times_send_4_price'] * 100 : NULL;
                                    $rbs_time_send_4_qty = isset($v['rbs_times_send_4_qty']) ? $v['rbs_times_send_4_qty'] : NULL;
                                    $rbs_time_send_4_shipping_category = isset($v['rbs_time_send_4_shipping_category']) ? $v['rbs_time_send_4_shipping_category'] : NULL;

                                    $rbs_time_send_5_price = isset($v['rbs_times_send_5_price']) ? $v['rbs_times_send_5_price'] * 100 : NULL;
                                    $rbs_time_send_5_qty = isset($v['rbs_times_send_5_qty']) ? $v['rbs_times_send_5_qty'] : NULL;
                                    $rbs_time_send_5_shipping_category = isset($v['rbs_time_send_5_shipping_category']) ? $v['rbs_time_send_5_shipping_category'] : NULL;

                                    $rbs_time_send_merge = '{"3":{"price":"' . $rbs_time_send_3_price . '","qty":"' . $rbs_time_send_3_qty . '","shipping_category":"' . $rbs_time_send_3_shipping_category . '"},"4":{"price":"' . $rbs_time_send_4_price . '","qty":"' . $rbs_time_send_4_qty . '","shipping_category":"' . $rbs_time_send_4_shipping_category . '"},"5":{"price":"' . $rbs_time_send_5_price . '","qty":"' . $rbs_time_send_5_qty . '","shipping_category":"' . $rbs_time_send_5_shipping_category . '"}}';

                                    $rbs_time_send_json = json_decode($rbs_time_send_merge, true);


                                    if(count($rbs_time_send_json)) {
                                        foreach ($rbs_time_send_json as $key => $val) {
                                            if(empty($val['price'])){
                                                unset($rbs_time_send_json["$key"]);
                                            }

                                            if($val['shipping_category'] == NULL) {
                                                unset($rbs_time_send_json["$key"]["shipping_category"]);
                                            }
                                        }
                                    }

                                    $pa->rbs_times_send = $rbs_time_send_json;
                                }

                                $pa->save();

                                $this->InsertPanelProductPrice($v, $pa->id, 'attribute');

                                $category = Category::where([['name', $v['product_category']], ['type', 'shop']])->first();

                                //insert piv category
                                if (isset($v['product_category'])) {
                                    $pivCats = PivCategory::where([['product_id', $gp->id], ['category_id', $category->id]])->first();

                                    if (!$pivCats) {
                                        $pivCats = new PivCategory;
                                    }

                                    $pivCats->product_id = $gp->id;
                                    $pivCats->category_id = $category->id;
                                    $pivCats->save();
                                }
                            }
                    }
                }
            }
        }
    }

    public function InsertPanelProductPrice($row, $id, $attribute)
    {
        $country = Countries::whereIn('country_id', ['MY', 'SG'])->get('country_id');

        $model_type = 'App\Models\Products\ProductAttribute';
        if (count($country) > 0) {

            foreach ($country as $val) {

                $fixed_price = ($val['country_id'] == 'MY') ? $row['web_fixed_price'] * 100 : 0;
                $outlet_price = ($val['country_id'] == 'MY') ? $row['outlet_fixed_price'] * 100 : 0;
                $standard_price = ($val['country_id'] == 'MY') ? $row['standard_price'] * 100 : 0;
                $advance_price = ($val['country_id'] == 'MY') ? $row['advance_price'] * 100 : 0;
                $premier_price = ($val['country_id'] == 'MY') ? $row['premier_price'] * 100 : 0;
                $web_offer_price = ($val['country_id'] == 'MY') ? $row['web_offer_price'] * 100 : 0;
                $outlet_offer_price = ($val['country_id'] == 'MY') ? $row['outlet_offer_price'] * 100 : 0;

                $pPrice = ProductPrices::where([
                    ['model_id', $id], ['model_type', $model_type], ['country_id', $val['country_id']],
                    ['fixed_price', $fixed_price], ['outlet_price', $outlet_price], ['standard_price', $standard_price],
                    ['advance_price', $advance_price], ['premier_price', $premier_price], ['web_offer_price', $web_offer_price],
                    ['outlet_offer_price', $outlet_offer_price]
                ])->first();

                if (!$pPrice) {
                    $inactiveThis  = ProductPrices::where([['model_id', $id], ['country_id', $val['country_id']]])->get();

                    if (count($inactiveThis)) {
                        foreach ($inactiveThis as $key => $inactive) {
                            $inactive->active = 0;
                            $inactive->save();
                        }
                    }

                    $maxPricekey = ProductPrices::where('model_type', $model_type)->where('country_id', $val['country_id'])->where('model_id', $id)->max('price_key');

                    $pPrice = new ProductPrices;
                    $pPrice->price_key = $maxPricekey + 1;
                }

                $pPrice->model_type = $model_type;
                $pPrice->model_id = $id;
                $pPrice->product_code = $row['product_code'];
                $pPrice->country_id = $val['country_id'];
                $pPrice->product_sv = isset($row['product_sv']) ? $row['product_sv'] : 0;
                $pPrice->fixed_price = $fixed_price;
                $pPrice->outlet_price = $outlet_price;
                $pPrice->standard_price = $standard_price;
                $pPrice->advance_price = $advance_price;
                $pPrice->premier_price = $premier_price;
                $pPrice->web_offer_price = $web_offer_price;
                $pPrice->outlet_offer_price = $outlet_offer_price;
                $pPrice->is_offer = $row['is_offer'] != NULL ? $row['is_offer'] : 0;
                $pPrice->is_free = $row['is_free'] != NULL ? $row['is_free'] : 0;

                $pPrice->save();

                $dd[] = $pPrice;
            }

            // dd($dd);


        }
    }

    public function InsertPanelInfo($data)
    {
        $account_id = (isset($data['panel_account_id']) ? $data['panel_account_id'] : 2011000000);

        $pi = PanelInfo::where('account_id', $account_id)->first();

        if (!$pi) {
            $pi = new PanelInfo;
        }
        $pi->user_id = time();
        $pi->account_id = $account_id;
        $pi->account_status = 1;
        $pi->company_name = (isset($data['display_panel_name']) ? $data['display_panel_name'] : 'Formula2u');
        $pi->ssm_number = 1;
        $pi->company_email = 1;
        $pi->company_phone = 1;
        $pi->pic_name = 1;
        $pi->pic_nric = 1;
        $pi->pic_contact = 1;
        $pi->pic_email = 1;
        $pi->name_for_display = (isset($data['display_panel_name']) ? $data['display_panel_name'] : 'Formula2u');

        $pi->save();
    }

    public function InsertPanelAddress($data)
    {
        $account_id = (isset($data['panel_account_id']) ? $data['panel_account_id'] : 2011000000);
        $pAdd = PanelAddress::where('account_id', $account_id)->first();

        if (!$pAdd) {
            $pAdd = new PanelAddress;
        }

        $pAdd->account_id = $account_id;
        $pAdd->address_1 = 'address1';
        $pAdd->address_2 = 'address2';
        $pAdd->address_3 = 'address3';
        $pAdd->address_1 = 'address1';
        $pAdd->postcode = 12345;
        $pAdd->city = 'city';
        $pAdd->city_key = 0;
        $pAdd->state_id = 14;
        $pAdd->is_correspondence_address = 1;
        $pAdd->is_billing_address = 1;

        $pAdd->save();
    }

    public function getQualityID($quality) {
        switch (ucwords($quality)) {
            case 'Import':
                $quality_id = 2;
                break;
            case 'Exclusive':
                $quality_id = 3;
                break;
            default:
                $quality_id = 1;
        }

        return $quality_id;
    }
}
