<?php

namespace App\Models\Products;

use App\Traits\TableLogs;
use Illuminate\Database\Eloquent\Model;

class WarehouseBatchDetail extends Model
{
    use TableLogs;

    // Set table
    protected $table = 'wh_batch_detail';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function items()
    {
        return $this->hasMany('App\Models\Products\WarehouseBatchItem', 'detail_id', 'id');
    }

    public function unusedItems()
    {
        return $this->hasMany(WarehouseBatchItem::class, 'detail_id', 'id')
            ->whereNull('picking_order_id')
            ->whereNull('type');
    }

    public function mainBatch()
    {
        return $this->hasOne('App\Models\Products\WarehouseBatch', 'batch_id', 'batch_id');
    }

    public function productAttribute()
    {
        return $this->hasOne('App\Models\Products\ProductAttribute', 'product_code', 'product_code');
    }

    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if ($selectSearch == 'batch_id') {
                return $query
                    ->where('batch_id', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'product_code') {
                return $query
                    ->where('product_code', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'vendor_name') {
                $query
                    ->join('wh_batch', 'wh_batch.batch_id', '=', 'wh_batch_detail.batch_id')
                    ->where('wh_batch.vendor_desc', 'like', '%' . $searchFilter . '%');
                return $query;
            } elseif ($selectSearch == 'product_name') {
                $query
                    ->join('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'wh_batch_detail.product_code')
                    ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
                    ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
                    ->where('global_products.name', 'LIKE', '%' . $searchFilter . '%');
                return $query;
            }
        }

        return $query;
    }

    public function scopeMonthsLeftFilter($query, $monthsLeft)
    {
        if ($monthsLeft === 0) {
            return $query->whereDate('expiry_date', '<', now());
        } else {
            $monthsLeftDate = now()->addMonths($monthsLeft);
            return $query->whereDate('expiry_date', '<=', $monthsLeftDate)
                ->whereDate('expiry_date', '>=', now());
        }
    }

    public function scopeLocationFilter($query, $location_id)
    {
        if ($location_id != 'all' && $location_id != NULL) {
            return $query
                ->join('wh_batch', 'wh_batch.batch_id', '=', 'wh_batch_detail.batch_id')
                ->join('wh_location', 'wh_location.id', '=', 'wh_batch.location_id')
                ->where('wh_location.id', 'like', '%' . $location_id . '%');
        } else {
            // return $query;
        }
    }
}
