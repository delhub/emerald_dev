<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Purchases\Purchase;
use App\Models\Purchases\Counters;
use Illuminate\Support\Carbon;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;

class AddInvNumberInPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->string('inv_number')->after('user_id')->nullable();
            $table->date('inv_date')->after('purchase_type')->nullable();
        });

        Schema::table('counters', function (Blueprint $table) {
            $table->string('date_month')->after('counter_types')->nullable();
        });

        DB::table('counters')->where('counter_types', 'invoice')->update(['counter_types' => 'purchase', 'counter_prefix' => 'FWP']);

        //update purchase, inv_number  and inv_date
        $purchases = Purchase::whereIn('purchase_status', [4002, 4007])->orderBy('created_at', 'ASC')->get();
        foreach ($purchases as $key => $purchase) {
            if (!isset($purchase->inv_number) || !isset($purchase->inv_date)) {

                $counters = Counters::autoIncrementInv(['invoice'], $purchase);
                $inv_date = Carbon::createFromFormat('d/m/Y', $purchase->purchase_date)->format('Y-m-d');

                $purchase->inv_number =  $counters['invoice'];
                $purchase->inv_date = $inv_date;

                $purchase->save();

                GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropColumn('inv_number');
            $table->dropColumn('inv_date');
        });

        Schema::table('counters', function (Blueprint $table) {
            $table->dropColumn('date_month');
        });

        DB::table('counters')->where('counter_types', 'purchase')->update(['counter_types' => 'invoice', 'counter_prefix' => 'FWS']);
    }
}
