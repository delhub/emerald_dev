<?php

namespace App\Http\Controllers\Administrator\Panels;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\Countries;
use App\Models\Globals\Products\Product;
use App\Models\Globals\Quality;
use App\Models\Products\ProductBrand;
use App\Models\Products\ProductBrandLogs;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Users\Panels\PanelAddress;
use Carbon\Carbon;

class PanelsController extends Controller
{
    public static $home;
    public static $header;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$home = 'administrator.panels.index';
        self::$header = 'Panel';

        self::$create = 'administrator.panels.create';
        self::$store = 'administrator.panels.store';

        self::$edit = 'administrator.panels.edit';
        self::$update = 'administrator.panels.update';

        self::$destroy = 'administrator.panels.destroy';

        // link to view page
        // view('administrator.panels.create');
    }


    public function index()
    {
        $tables = PanelInfo::orderBy('company_name', 'ASC')->paginate(10);

        return view(self::$home, compact('tables', 'header'));
    }

    public function create()
    {
        return view(self::$create);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required|string|max:50',
            // 'name_for_display' => 'required|string|max:50',

        ]);

        $existingdata  = PanelInfo::where('company_name', $request->input('company_name'))->first();

        if (!$existingdata) {
            $data = new PanelInfo;

            $data->account_id = PanelInfo::LargestPanelId() + 1;
            $data->account_status = 1;
            $data->company_name = $request->input('company_name');
            $data->name_for_display = $request->input('name_for_display');
            $data->ssm_number = $request->input('ssm_number');
            $data->company_email = $request->input('company_email');
            $data->company_phone = $request->input('company_phone');
            $data->pic_name = $request->input('pic_name');
            $data->pic_contact = $request->input('pic_contact');
            $data->pic_email = $request->input('pic_email');

            $data->save();

            $dataAddress = new PanelAddress;

            $dataAddress->account_id = $data->account_id;
            $dataAddress->address_1 = '1-26-05,';
            $dataAddress->address_2 = 'MENARA BANGKOK BANK, BERJAYA CENTRAL PARK,';
            $dataAddress->address_3 = 'NO.105, JALAN AMPANG,';
            $dataAddress->postcode = '50450';
            $dataAddress->city = 'Kuala Lumpur';
            $dataAddress->city_key = 'KUL_KUL';
            $dataAddress->state_id = 14;
            $dataAddress->is_correspondence_address = 1;
            $dataAddress->is_billing_address = 1;

            $dataAddress->save();

            return redirect()->route(self::$home)->with('success', 'submit done.');
        } else {
            return back()->with(['error' => self::$header . ' already exist  !!! ']);
        }
    }

    // public function edit($id)
    // {
    //     $data = PanelInfo::where('account_id', $id)->first();

    //     return view(self::$edit)->with('data', $data);
    // }
    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'company_name' => 'required|string|max:50',
    //         'name_for_display' => 'required|string|max:50',

    //     ]);

    //     $data = PanelInfo::where('account_id', $id)->first();

    //     $data->company_name = $request->input('company_name');
    //     $data->name_for_display = $request->input('name_for_display');
    //     $data->ssm_number = $request->input('ssm_number');
    //     $data->company_email = $request->input('company_email');
    //     $data->company_phone = $request->input('company_phone');
    //     $data->pic_name = $request->input('pic_name');
    //     $data->pic_contact = $request->input('pic_contact');
    //     $data->pic_email = $request->input('pic_email');

    //     $data->save();

    //     return redirect()->route(self::$home)->with('success', self::$header . ' Updates');
    // }

    // public function destroy($id)
    // {
    //     $data = PanelInfo::find($id);

    //     $data->delete();

    //     return redirect()->route(self::$home)->with('success', self::$header . ' Remove');
    // }

}
