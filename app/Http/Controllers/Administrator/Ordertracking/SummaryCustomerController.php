<?php

namespace App\Http\Controllers\Administrator\Ordertracking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Purchases\Item;

class SummaryCustomerController extends Controller
{
    public static function getProduct($request){
        //Panels
        $panels = PanelInfo::all();
        //Items
        $items = Item::selectRaw('items.*, items.id')
            ->join('orders', 'orders.order_number', '=', 'items.order_number')
            ->whereNotIn('items.item_order_status', ['1000']) // remove status record created
            ->whereNotIn('orders.order_status', ['1000']) // remove order status record created
            ->panelFilter($request->panel)
            ->statusFilter($request->status, $request->date, $request->period)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->orderBy('items.product_code', 'DESC')
            ->orderBy('items.created_at', 'DESC');

        $ForCountcustomerOrders = $items->get();

        $items = $items->paginate(1000);
        $items->appends(request()->query());

        $list = array();

        foreach ($items as $item) {

            if (isset($item->product_code) && !empty($item->product_code)) {
                $productnumber = $item->product_code;
            } else {
                $productnumber = $item->product_id;
            }

            $product_info = $item->product_information;
            $item_status = $item->item_order_status;

            $placed = 1001;
            $acd_delivered = 1008;

            $shipped = 1002;
            $acd_shipOut = 1010;
            $acd_self_delivered = 1011;

            $delivered = 1003;
            $acd_collected = 1009;

            $cancel = 1004;

            $pending_payment = 1005;

            if (!isset($list[$productnumber])) {

                $list[$productnumber]['order_placed']['case'] = 0;
                $list[$productnumber]['order_placed']['itemsid'] = array();

                $list[$productnumber]['order_shipped']['case'] = 0;
                $list[$productnumber]['order_shipped']['itemsid'] = array();

                $list[$productnumber]['order_delivered']['case'] = 0;
                $list[$productnumber]['order_delivered']['itemsid'] = array();

                $list[$productnumber]['order_cancel']['case'] = 0;
                $list[$productnumber]['order_cancel']['itemsid'] = array();

                $list[$productnumber]['order_pending_payment']['case'] = 0;
                $list[$productnumber]['order_pending_payment']['itemsid'] = array();

                $list[$productnumber]['total']['case'] = 0;
                $list[$productnumber]['total']['itemsid'] = array();
            }

            if ($item_status ==  $placed || $item_status ==  $acd_delivered) {

                // Order Placed
                $list[$productnumber]['order_placed']['case']++;
                $list[$productnumber]['order_placed']['itemsid'][] = $item->id;
            } elseif ($item_status ==  $shipped || $item_status == $acd_shipOut || $item_status ==  $acd_self_delivered) {

                // Order Shipped
                $list[$productnumber]['order_shipped']['case']++;
                $list[$productnumber]['order_shipped']['itemsid'][] = $item->id;
            } elseif ($item_status ==  $delivered || $item_status == $acd_collected) {

                // Order Delivered
                $list[$productnumber]['order_delivered']['case']++;
                $list[$productnumber]['order_delivered']['itemsid'][] = $item->id;
            } elseif ($item_status ==  $cancel) {

                // Order Cancel
                $list[$productnumber]['order_cancel']['case']++;
                $list[$productnumber]['order_cancel']['itemsid'][] = $item->id;
            } elseif ($item_status ==  $pending_payment) {

                // Order Pending Payment
                $list[$productnumber]['order_pending_payment']['case']++;
                $list[$productnumber]['order_pending_payment']['itemsid'][] = $item->id;
            }

            $list[$productnumber]['total']['case']++;
            $list[$productnumber]['total']['itemsid'][] = $item->id;


            $list[$productnumber]['information'] = $product_info;
            $list[$productnumber]['item'] = $item;
        }

        $product['panels'] = $panels;
        $product['items'] = $items;
        $product['list'] = $list;
        $product['ForCountcustomerOrders'] = $ForCountcustomerOrders;
        return $product;
    }

    public function index(Request $request)
    {
        $product = self::getProduct($request);

        $panels = $product['panels'];
        $items = $product['items'];
        $list = $product['list'];
        $ForCountcustomerOrders = $product['ForCountcustomerOrders'];

        return view('administrator.ordertracking.summarycustomer')
            ->with('panels', $panels)
            ->with('request', $request)
            ->with('items', $items)
            ->with('list', $list)
            ->with('ForCountcustomerOrders', $ForCountcustomerOrders);
    }

    //Download Customer
    public function exportCsvCustomer(Request $request)
    {
        $fileName = 'Order Tracking Details (Customer).csv';

        $product = self::getProduct($request);


        $panels = $product['panels'];
        $items = $product['items'];
        $list = $product['list'];
        $ForCountcustomerOrders = $product['ForCountcustomerOrders'];

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Product Code', 'Product Name', 'Size', 'Panel', 'Item Placed', 'Item Shipped', 'Item Delivered', 'Item Cancel', 'Item Pending Payment', 'Total');

        $callback = function () use ($list, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($list as $key => $listResult) {

                $row['Product Code']    = $key;
                $row['Product Name']    = is_object($listResult['item']->product ) ? $listResult['item']->product->parentProduct->name : '';
                $row['Size']    = array_key_exists('product_size', $listResult['information']) ? $listResult['information']['product_size'] : '';
                $row['Panel']    = $listResult['item']->order->panel ? $listResult['item']->order->panel->company_name : '';

                $row['Item Placed'] = $listResult['order_placed']['case'];
                $row['Item Shipped'] = $listResult['order_shipped']['case'];
                $row['Item Delivered'] = $listResult['order_delivered']['case'];
                $row['Item Cancel'] = $listResult['order_cancel']['case'];
                $row['Item Pending Payment'] = $listResult['order_pending_payment']['case'];
                $row['Total'] = $listResult['total']['case'];


                fputcsv($file, array($row['Product Code'], $row['Product Name'], $row['Size'], $row['Panel'], $row['Item Placed'], $row['Item Shipped'], $row['Item Delivered'], $row['Item Cancel'], $row['Item Pending Payment'], $row['Total']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
