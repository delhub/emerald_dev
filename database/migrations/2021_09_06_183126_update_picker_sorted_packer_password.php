<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Users\User;
use Illuminate\Support\Facades\Hash;

class UpdatePickerSortedPackerPassword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $picker = User::where('email', 'picker1@email.com')->first();
        $picker->email = 'pick1@email.com';
        $picker->password = Hash::make('pick1234');
        $picker->save();

        $sorter = User::where('email', 'sorter1@email.com')->first();
        $sorter->email = 'sort1@email.com';
        $sorter->password = Hash::make('sort1234');
        $sorter->save();


        $packer = User::where('email', 'packer1@email.com')->first();
        $packer->email = 'packer1@email.com';
        $packer->password = Hash::make('packer1234');
        $packer->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
