<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Traits\Voucher;

class VoucherCount extends Command
{
    use Voucher;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update voucher use count';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->ResetVoucherCount();
        
        if($this->updateVoucherCount()) {
            echo "Voucher count update --- OK \n";
        }
        else {
            echo "Error -- Voucher count update!...\n";
        }
    }
}
