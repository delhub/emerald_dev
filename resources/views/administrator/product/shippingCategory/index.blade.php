@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@push('style')
<style>
    .normal-font-color{
        color: black
    }

    .special-font-color{
        color: red
    }

    .largerCheckBox{
        width: 18px;
        height: 18px;
    }
</style>

@endpush

@section('content')

@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<!-- Empty Modal -->
<div class="modal fade" id="ModalId" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Ajax response loaded here -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">Product Attribute</h1>
        </div>
    </div>

    {{-- select bulk action  --}}
    <form id="bulkactionform" action={{route('bulkupdate.administrator.product.shippingCategory') }} method="POST">
        <div class="row mt-2 ml-2">
            <div class="col-md-auto px-1">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="action_ids" id="action_ids" value="">

        <div class="input-group mb-1">
            <select name="bulkaction" id="bulkaction" class="col-12 text-left custom-select">
                <option value="">Choose action...</option>
                <option value="shipping"> Mark Shipping Category </option>
                <option value="shipping_special">Mark Shipping Category (Special)</option>
            </select>

            <div class="col-md-auto px-1">
                <select name="shippingCategory" id="shippingCategory" class="col-12 text-left custom-select">
                    <div id="shippingCategory"></div>
                </select>
            </div>

            <div class="input-group-append">
                <input type="submit" class="bjsh-btn-gradient" value="Submit" id="btnid">
            </div>
        </div>
            </div>
        </div>
    </form>

    <form action="{{ route('administrator.product.shippingCategory') }}" method="GET">
        <div class="row mt-2 ml-2">
            <!-- Panel -->
            <div class="col-md-auto px-1">
                <select name="shippingCategory" id="shippingCategory" class="col-12 text-left custom-select">
                    <option value="all" selected>All Shipping Category</option>
                    @foreach ($shippingCategories as $categroy)
                    <option value="{{ $categroy->id }}"
                        {{ $request->shippingCategory == $categroy->id ? 'selected' : ''}}>{{ $categroy->cat_name }}
                    </option>

                    @endforeach
                </select>
            </div>

            <div class="col-md-auto px-1">
                <select name="shippingCategorySpecial" id="shippingCategorySpecial" class="col-12 text-left custom-select" value="special">
                    <option value="all" selected>All Shipping Category (Special)    </option>
                    <option value="0">NONE</option>
                    @foreach ($shippingCategoriesSpecial as $categroy)
                    <option value="{{ $categroy->id }}"
                        {{ $request->shippingCategorySpecial == $categroy->id ? 'selected' : ''}}>
                        {{ $categroy->cat_name }}
                    </option>

                    @endforeach
                </select>
            </div>

            <div class="col-md-auto px-1">
                <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
            </div>
            <div>
                <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.product.shippingCategory') }} style="color:black;">Reset Filter</a></button>
            </div>

            <div class="col-md-auto ml-auto">
                <div class="row">
                    <div class="col-md-8 p-0">
                        <select class="custom-select" id="selectSearch" name="selectSearch">
                            <option value="ALL" selected>Choose...</option>
                            <option value="name" {{ $request->selectSearch == 'name' ? 'selected' : '' }}>Product Name</option>
                            <option value="code" {{ $request->selectSearch == 'code' ? 'selected' : '' }}>Product Code</option>
                        </select>

                        <input type="text" class="form-control" id="" name="searchBox"
                            value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                    </div>
                    <div class="col-md-4 ">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Item) : <b>{{ count($ForCountproducts)}}</b></span></p>
        <div class="row">
            <div class="col-auto ml-auto">
                {!! $productsAttributes->render() !!}
            </div>
        </div>
    </form>

        <div class="table-responsive m-2">
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td><input class="largerCheckBox" type="checkbox" id="checkAll"/>Action</td>
                        <td>Product Code</td>
                        <td>Attribute Name</td>
                        <td>Attribute Weight (KG)</td>
                        <td>Stock/on hand</td>
                        <td>Shipping Categories</td>
                        <td><div class="special-font-color">Shipping Categories Special</div></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($productsAttributes as $attribute)
                    <tr>
                        <td style="width: 3%;">
                            <input type="checkbox" class="cb-element largerCheckBox" name="itemsid[]" value="{{ $attribute->id }}">{{ $loop->iteration }}
                        </td>
                        <td style="width: 15%;">{{ $attribute->product_code }}</td>

                        <td style="width: 12%;">{{ $attribute->attribute_name }}</td>
                        <td style="width: 3%;">{{ $attribute->product_measurement_weight }}</td>
                        <td style="width: 2%;">
                            {{ $attribute->stock }}
                            <br>
                            {{ $attribute->onhand_stock }}
                        </td>

                        <td style="width: 27.5%;">
                            <button type="button" class="btn btn-dark edit-markup" style="background-color:white" data-toggle="modal" data-target="#newProductModaledit"
                            data-price-id="{{ $attribute->id}}" >
                            <div class="normal-font-color"> Shipping Category : [ {{ !empty($attribute->getShippingCategory()->cat_name) ? $attribute->getShippingCategory()->cat_name : '' }} ]</div>
                            </button>
                        </td>

                        <td style="width: 27.5%;">
                            <button type="button" class="btn btn-dark edit-markup-special" style="background-color:white" data-toggle="modal" data-target="#newProductModaledit"
                            data-price-id="{{ $attribute->id}}" >
                            <div class="special-font-color"> <b>Special</b> : [ {{ !empty($attribute->getShippingSpecial()->cat_name) ? $attribute->getShippingSpecial()->cat_name : 'NONE' }} ]</div>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</div>

<div class="col-md-4-md-12" style="float:left;">
    {!! $productsAttributes->render() !!}
</div>

@endsection

@push('script')
<script>
    $(document).ready(function() {

        document.getElementById("shippingCategory").style.display = "none";

        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        //Get checked id
        $("#bulkactionform").submit(function(){
            var checked = new Array();
            var ids = new Array();

            checked = $('input[name="itemsid[]"]:checked');

            for (i = 0; i < checked.length; i++) {
                ids.push(checked[i].value);
                }

            var stringids = ids.toString();
            $("#action_ids").val(stringids);
        });

        //show or hide when selected bulk action
        $("#bulkaction").on("change",function(){

            var variableID = $(this).val();

            $.ajax({
                type:"get",
                url:'{{route("administrator.product.shippingCategory.decide-shipping")}}',
                data:$("#bulkactionform").serialize(),
                    success:function(toajax){
                        $("#shippingCategory").html(toajax);

                        if (bulkaction.value =='shipping' || bulkaction.value =='shipping_special') {
                            document.getElementById("shippingCategory").style.display = "block";
                        }else{
                            document.getElementById("shippingCategory").style.display = "none";
                        }
                    }
            });
        });

        //edit shipping category normal
        $('#app').on('click', '.edit-markup', function() {
            // Price Edit URL - GET
            editURL = "{{ route('administrator.product.shippingCategory.edit', ['id' => 'id']) }}";

            let id = $(this).data('price-id');

            editURL = editURL.replace('id', id);

            let outsideMarkupButton = this;

            $.ajax({
                async: true,
                beforeSend: function() {
                },
                complete: function() {
                },
                url: editURL,
                type: 'GET',
                success: function(response) {
                    $('#ModalId .modal-body').empty();

                    // Change modal title
                    $('#ModalId .modal-title').text('Edit Price')
                    // Add response in Modal body
                    $('#ModalId .modal-body').html(response);

                    // Display Modal
                    $('#ModalId').modal('show');

                    $('.select2-edit').select2({
                        dropdownParent: $('#ModalId')
                    });

                    $('#modalSubmitBtn').on('click', function() {

                        //normal
                        const markupCategry = document.getElementById('markup_category');
                        const categoryId = markupCategry.options[markupCategry.selectedIndex].value;

                        const markupName = $(markupCategry).find(':selected').data('markup-name')

                        $.ajax({
                            async: true,
                            beforeSend: function() {
                                // Show loading spinner.
                                // loading.show();
                            },
                            complete: function() {
                                // Hide loading spinner.
                                // loading.hide();
                            },
                            url: '/administrator/product/shippingCategory/update/' + $(this).data('item-id'),
                            type: "GET",
                            data:{
                                // POST data.
                                category_id: categoryId,
                                // category_id_special: categoryId_special,

                            },
                            success: function(result) {
                                // change button text
                                outsideMarkupButton.innerHTML =
                                '<div class="normal-font-color"> Shipping Category : [ '+markupName+' ]</div>';

                                //hiden modal
                                $("#ModalId").modal('hide');
                            },
                            error: function(result) {
                                // Log into console if there's an error.
                                console.log(result.status + ' ' + result.statusText);
                            }
                        });

                    });

                },
                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON.message);
                }
            });

        });

        //edit shipping category special
        $('#app').on('click', '.edit-markup-special', function() {
            // Price Edit URL - GET
            editURL = "{{ route('administrator.product.shippingCategory.edit.special', ['id' => 'id']) }}";

            let id = $(this).data('price-id');

            editURL = editURL.replace('id', id);

            let outsideMarkupButton = this;

            $.ajax({
                async: true,
                beforeSend: function() {
                },
                complete: function() {
                },
                url: editURL,
                type: 'GET',
                success: function(response) {
                    $('#ModalId .modal-body').empty();

                    // Change modal title
                    $('#ModalId .modal-title').text('Edit Price')
                    // Add response in Modal body
                    $('#ModalId .modal-body').html(response);

                    // Display Modal
                    $('#ModalId').modal('show');

                    $('.select2-edit').select2({
                        dropdownParent: $('#ModalId')
                    });

                    $('#modalSubmitBtn').on('click', function() {

                        //special
                        const markupCategry_special = document.getElementById('markup_category_special');
                        const categoryId_special = markupCategry_special.options[markupCategry_special.selectedIndex].value;

                        const markupName_special = $(markupCategry_special).find(':selected').data('markup-name-special')

                        $.ajax({
                            async: true,
                            beforeSend: function() {
                                // Show loading spinner.
                                // loading.show();
                            },
                            complete: function() {
                                // Hide loading spinner.
                                // loading.hide();
                            },
                            url: '/administrator/product/shippingCategory/update/' + $(this).data('item-id'),
                            type: "GET",
                            data:{
                                // POST data.
                                // category_id: categoryId,
                                category_id_special: categoryId_special,

                            },
                            success: function(result) {
                                // change button text
                                outsideMarkupButton.innerHTML =
                                '<div class="special-font-color"> <b>Special</b> : [ '+markupName_special+' ]</div>';

                                //hiden modal
                                $("#ModalId").modal('hide');
                            },
                            error: function(result) {
                                // Log into console if there's an error.
                                console.log(result.status + ' ' + result.statusText);
                            }
                        });

                    });

                },
                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON.message);
                }
            });

        });

    });
</script>
@endpush
