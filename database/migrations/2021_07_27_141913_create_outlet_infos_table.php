<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateOutletInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('outlet_infos');

        Schema::create('outlet_infos', function (Blueprint $table) {
            $table->id();
            $table->string('outlet_key');
            $table->string('outlet_name');
            $table->string('country_id', 2);
            $table->string('printer_email');
            $table->string('contact_number');
            $table->text('default_address');
            $table->string('postcode', 50);
            $table->string('city_key');
            $table->integer('state_id');
            $table->text('operation_time');
            $table->timestamps();
        });

        DB::table('global_statuses')->insert(
            array(
                array('id' => '1010', 'name' => 'Order Self Collect', 'description' => 'Status for order status.', 'status_type' => ''),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_infos');
    }
}
