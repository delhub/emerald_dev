@extends('layouts.management.main-agent')

@section('content')

@section('page_title')
{{ "Agent - Membership Registration" }}
@endsection

<div class="fml-container pb-3">
    <div class="row">
        <div>
            <h4>Membership Registration</h4>
        </div>
    </div>
</div>
<div class="container-fluid p-0">
    <div class="row mb-4">
        <div class="col-md-12 col-12">
            <div class="checkout-box-s">
                <div class="card-body">
                    <div class="form-row register-info">
                        <div class="form-group col-md-6">
                            <div class="title-ms pb-0">
                                <img src="{{asset('/images/icons/copy-link.png')}}" alt="">
                                <h5 class="pl-3 pb-0">Registration Link</h5>
                            </div>

                            <div class="notice-member">
                                <div class="col-md-12" style="text-align:center">
                                   
                                        {!! QrCode::size(150)->generate( URL::to('/member-info/' . $dealer->account_id) ); !!}
                                  
                                </div>
                                <form action="" class="col-md-12">
                                    <div class="form-row coupon-box d-flex">
                                        <div class="input-group  d-flex align-items-center">
                                            <input type="text" placeholder="Registration Link" class="form-control" id="copy-link" value="{{ URL::to('/member-info/' . $dealer->account_id) }}">
                                            <div class="ag-copy-btn blue-btn pl-3" onclick="copyPayDirectLink()">
                                                <img id="img" class="s-icon pr-1" src="{{asset('/images/icons/copy-icon.svg')}}" alt=""> Copy
                                            </div>
                                        </div>
                                    </div>
                                    <label for="formFileLg" class="form-label">*Copy and share this link to your customer.</label>
                                </form>
                            </div>

                        </div>
                        <div class="r-link form-group col-md-6">
                            <div class="title-ms pb-4">
                                <img src="{{asset('/images/icons/sop.png')}}" alt="">
                                <h5 class="pl-3">Instruction For Registration</h5>
                            </div>
                            <a href="{{ URL::asset('/images/icons/sop-registration.pdf')}}" download="SOP REGISTRATION" class="fml-btn buy-btn link-product" data-bs-original-title="" title=""> Download SOP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function copyPayDirectLink() {
        var copyText = document.getElementById("copy-link");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        toastr.success('Link successfully copied!', 'Success');
    }
</script>