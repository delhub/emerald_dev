<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddEggReportToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('categories')->insert(['name' => 'Egg Report', 'slug' => 'egg-report', 'type' => 'agent', 'parent_category_id' => '0', 'featured' => '0', 'featured_mobile' => '0', 'position' => '9999', 'active' => '0', 'created_at' => now(),
        'updated_at' => now()]);
       //
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
