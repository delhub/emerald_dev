<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class InsertShippingRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_rates', function (Blueprint $table) {
            $table->decimal('min_rates', 11, 2)->change();
            $table->decimal('max_rates', 11, 2)->change();
        });


        DB::table('shipping_rates')->insert(
            array(
                array('zone_id' => '1', 'category_id' => '1', 'price' => '600', 'condition' => 'weight', 'min_rates' => '0.0', 'max_rates' => '3', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '1', 'category_id' => '1', 'price' => '1500', 'condition' => 'weight', 'min_rates' => '3.01', 'max_rates' => '4', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '1', 'category_id' => '1', 'price' => '1700', 'condition' => 'weight', 'min_rates' => '4.01', 'max_rates' => '5', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '1', 'category_id' => '1', 'price' => '2100', 'condition' => 'weight', 'min_rates' => '5.01', 'max_rates' => '6', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '1', 'category_id' => '1', 'price' => '2300', 'condition' => 'weight', 'min_rates' => '6.01', 'max_rates' => '7', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
            )
        );

        $price = 2500;
        $min_rates = 7;
        $max_rates = 8;

        for ($i = 0; $i < 50; $i++) {

            DB::table('shipping_rates')->insert(
                array('zone_id' => '1', 'category_id' => '1', 'price' => $price, 'condition' => 'weight', 'min_rates' => '' . $min_rates . '.01', 'max_rates' => $max_rates, 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y')
            );

            $price += 200;
            $min_rates++;
            $max_rates++;
        }




        DB::table('shipping_rates')->insert(
            array(
                array('zone_id' => '2', 'category_id' => '1', 'price' => '1700', 'condition' => 'weight', 'min_rates' => '0.00', 'max_rates' => '1', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
            )
        );

        $price = 2700;
        $min_rates = 1;
        $max_rates = 2;

        for ($i = 0; $i < 50; $i++) {

            DB::table('shipping_rates')->insert(
                array('zone_id' => '2', 'category_id' => '1', 'price' => $price, 'condition' => 'weight', 'min_rates' => '' . $min_rates . '.01', 'max_rates' => $max_rates, 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y')
            );

            $price += 1000;
            $min_rates++;
            $max_rates++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
