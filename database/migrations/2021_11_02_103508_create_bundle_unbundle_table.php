<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Users\User;
use App\Models\Users\UserInfo;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use Illuminate\Support\Facades\Hash;
use App\Models\Users\UserCountry;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CreateBundleUnbundleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_product_b_unbundle', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('location_id');
            $table->string('type');
            $table->string('product_code');
            $table->Integer('quantity');
            $table->dateTime('expiry_date');
            $table->bigInteger('panel_id');
            $table->tinyInteger('completed')->default(0);
            $table->text('remark');
            $table->timestamps();
        });

        Schema::create('wh_product_b_unbundle_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('b_unbundle_key');
            $table->Integer('batch_item_id');
            $table->timestamps();
        });

        DB::table('global_statuses')->insert(
            array(
                array('id' => 8019, 'name' => 'Bundle/Unbundle Add', 'description' => 'Bundle/Unbundle Add', 'status_type' => 'inventory_status', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('id' => 8020, 'name' => 'Bundle/Unbundle Minus', 'description' => 'Bundle/Unbundle Minus', 'status_type' => 'inventory_status', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        DB::table('roles')->insert(
            array(
                array('name' => 'wh_b_unbundle', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        DB::table('counters')->insert(
            array(
                array('counter_types' => 'b_unbundle', 'country_id' => 'MY', 'counter_value' => '0', 'counter_prefix' => 'BUBDL', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        $role = 'b_unbundle';
        $this->createCustomer($role, $verify = true, 'MY');

        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock
        AS
        SELECT product_code, outlet_id,

        sum(case when inv_type = '8010' OR inv_type = '8000' OR inv_type = '8013' OR inv_type = '8014' OR inv_type = '8017' OR inv_type = '8018' OR inv_type = '8019' OR inv_type = '8020'
        then inv_amount
        else 0
        END)
        AS physical_stock,

        sum(case when inv_type = '8001'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved,

        sum(case when inv_type = '8011'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved_offline,

        sum(case when inv_type = '8012'
        then inv_amount
        else 0
        end)
        as pos_completed,

        sum(case when inv_type = '8013'
        then inv_amount
        else 0
        end)
        as total_stockTransferIn,

        sum(case when inv_type = '8014'
        then inv_amount
        else 0
        end)
        as total_stockTransferOut,

        sum(case when inv_type = '8017'
        then inv_amount
        else 0
        end)
        as total_adjustOut,

        sum(case when inv_type = '8018'
        then inv_amount
        else 0
        end)
        as total_adjustIn,

        sum(case when inv_type = '8019'
        then inv_amount
        else 0
        end)
        as total_b_bundle_add,

        sum(case when inv_type = '8020'
        then inv_amount
        else 0
        end)
        as total_b_bundle_minus,

        sum(case when inv_type = '8000' OR inv_type = '8001' OR inv_type = '8011' OR inv_type = '8010' OR inv_type = '8012' OR inv_type = '8013'OR inv_type = '8014'OR inv_type = '8017'OR inv_type = '8018' OR inv_type = '8019'OR inv_type = '8020'
        then inv_amount
        else 0
        end)
        as virtual_stock

        FROM wh_inventories
        GROUP BY product_code, outlet_id
        ORDER BY outlet_id ASC
        ");
    }

    public static function createCustomer($role, $verify = true, $country_id)
    {
        // Users table.
        $user = new User;
        $user->email = $role . '1@email.com';
        $user->password = Hash::make($role . '1234');
        if ($verify) $user->email_verified_at = Carbon::now();
        $user->save();

        // Generating new customer account id.
        $largestCustomerId = UserInfo::largestCustomerId() + 1;

        // User_infos table.
        $userInfo = new UserInfo;
        $userInfo->user_id = $user->id;
        $userInfo->account_id = $largestCustomerId;
        $userInfo->user_level = 6001;

        $userInfo->account_status  = 1;

        $userInfo->full_name = $role;
        $userInfo->nric = 1234;
        $userInfo->referrer_id = 1;
        $userInfo->group_id = 13;
        $userInfo->save();

        // Users table.
        $userCountry = new UserCountry;
        $userCountry->account_id = $largestCustomerId;
        $userCountry->country_id = $country_id;
        $userCountry->referrer_id = $country_id . '1010';
        $userCountry->group_id = 13;
        $userCountry->save();

        // User_addresses table(two records - billing address and shipping address)
        $userAddress_billing_address = new UserAddress;
        $userAddress_billing_address->account_id = $userInfo->account_id;
        $userAddress_billing_address->address_1 = 1;
        $userAddress_billing_address->city_key = 0;
        $userAddress_billing_address->state_id = 14;
        $userAddress_billing_address->country_id = $country_id;
        $userAddress_billing_address->is_shipping_address = 1;
        $userAddress_billing_address->is_residential_address = 1;
        $userAddress_billing_address->is_mailing_address = 1;
        $userAddress_billing_address->save();

        // User_contacts table (Home).
        $userContactHome = new UserContact;
        $userContactHome->account_id = $userInfo->account_id;
        $userContactHome->contact_num = 1;
        $userContactHome->is_home = 1;
        $userContactHome->is_mobile = 1;
        $userContactHome->is_office = 1;

        $userContactHome->save();

        $user->assignRole('wh_' . $role);

        return $user;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_unbundle');
    }
}
