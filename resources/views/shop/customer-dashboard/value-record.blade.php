@extends('layouts.management.main-customer')



@section('content')

@section('page_title')
{{ "Welcome to My Dashboard" }}
@endsection

{{-- new layout --}}
<div class="fml-container vr-title-tp">

    @if($purchases->isNotEmpty())
        <div class="col-md-12">
            <div class="db-value-title">
                <p>Value Records</p>
            </div>
        </div>
            {{-- search and pagination --}}
        <div class="col-md-12 search-filter-box">
            <form action="{{ route('shop.customer.orders') }}" method="GET">
                <div class="row">

                    <!-- Date filter from date to date-->
                    <div class="col-md-auto">
                        <div class="input-group mb-3 vr-b-radius">
                            <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                                aria-label="Username" aria-describedby="basic-addon1"
                                value="{{ ($request->datefrom) ? $request->datefrom : $request->dateto }}" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-md-auto">
                        <div class="input-group mb-3 vr-b-radius">
                            <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                                aria-label="Username" aria-describedby="basic-addon1"
                                value="{{ ($request->dateto) ? $request->dateto : $request->datefrom }}" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-md-auto">
                        <button type="submit" class="btn vr-filt-btn">Filter</button>
                    </div>
                    <div class="col-md-auto vr-mt vr-pb-20">
                        <button type="submit" class="btn vr-filt-btn"><a href={{ route('shop.customer.orders') }}">Reset Filter</a></button>
                    </div>
                    <div class="col-md-auto db-order-no">
                        <p>{{count($ForCount)}} <span>Order(s) Placed</span></p>
                    </div>

                </div>
            </form>
        </div>

        <div class="vr-col col-md-4-md-12" style="float:left;">
            {!! $purchases->render() !!}
        </div>
        {{-- end search and pagination --}}

        @foreach($purchases as $purchase)
        <div class="vr-or-dt">
            <div class="vr-box">
                <div class="purchase-list-t">
                    <h4>Order: <span>{{ $purchase->purchase_number}}</span></h4>
                    @if($purchase->inv_number)
                    <h4>Invoice: <span>{{ $purchase->inv_number}}</span></h4>
                    @endif
                </div>

                <div class="oderdate-t">
                <h5>Order Date : <span>{{ $purchase->purchase_date }}</span></h5>
                </div>
                <div class="vr-ir-btn">
                    <div class="vr-btn1">
                        <a class="btn">{{$purchase->showStatus()}}</a>
                    </div>
                    @if (in_array($purchase->purchase_status, [4002, 4007]))
                    <div class="vr-btn2">
                        <a href="{{asset('/storage/documents/invoice/'.$purchase->getFormattedNumber().(($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/').$purchase->getFormattedNumber().'.pdf')}}"
                            class="btn" data-bs-original-title="" title="" target="_blank">Invoice</a>
                    </div>
                    <div class="vr-btn3">
                        <a href="{{asset('/storage/documents/invoice/'.$purchase->getFormattedNumber().(($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/').$purchase->getFormattedNumber().'-receipt.pdf')}}"
                            class="btn" data-bs-original-title="" title="" target="_blank">Receipt</a>
                    </div>
                    @endif
                </div>
            </div>
            @foreach($purchase->sortItems() as $key => $item)
                @if ($item->bundle_id == 0)
                    <div class="row vr-order-lst">
                        <div class="col-md-9 vr-pt-m0">
                        <div class="row body-inner vr-i-align">
                            <div class="col-md-3 col-3">
                                {{-- @if(array_key_exists('product_color_img', $item->product_information) && isset($item->product))

                                @foreach($item->product->parentProduct->images as $value)

                                    @if ($value['id'] == $item->product_information['product_color_img'] )
                                        <img class="vr-p-img" src="{{ asset('storage/' . $value['path'] . '' . $value['filename']) }}"/>
                                    @endif

                                @endforeach

                                @elseif(isset($item->product->parentProduct)) --}}
                                @if ($item->product->parentProduct->defaultImage)
                                    <img class="vr-p-img"
                                    src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename) }}"/>
                                @else
                                    <img class="imgproduct" src="{{asset('/images/product_item/default-p.png')}}" alt="">
                                @endif
                                {{-- @endif --}}
                            </div>
                            <div class="col-md-9 col-9 vr-p-d">
                                @if (isset($item->product->parentProduct))
                                    <a class="ordername text-capitalize pb-3"
                                    href="{{$item->product->parentProduct->getUrlAttribute()}}">
                                        {{ $item->product->parentProduct->name }}

                                        <p>
                                            <small>
                                                {{ array_key_exists('product_size', $item->product_information) ? $item->product_information['product_size'] : '' }}
                                            <br>
                                                {{ $item->order_type == 'rbs' ?  'Deliver '.$item->quantity.' item(s) every '.
                                                ( array_key_exists('product_rbs_frequency',$item->product_information) ? $item->product_information['product_rbs_frequency'] : '' )
                                                . ' month(s) for '.
                                                ( array_key_exists('product_rbs_times',$item->product_information) ? $item->product_information['product_rbs_times'] : '' )
                                                .' times.' : '' }}
                                            </small>
                                        </p>

                                    </a>
                                @endif
                                <p>Quantity: <span>{{ array_key_exists('product_rbs_frequency',$item->product_information) ? array_sum(array_column($purchase->sortItems(),'quantity')) : $item->quantity }}</span></p>
                                @if (in_array($item->item_order_status,[1011,1012]))
                                    <p class="text-danger"><small>Refunded</small></p>
                                @endif

                            </div>
                        </div>
                        </div>

                        <!-- Show ship out date -->
                        @php

                        if ($item->order->shipping_date != 'Pending') {
                            $shipDate = date("d-m-Y", strtotime($item->order->shipping_date));
                        }else{
                            $shipDate = $item->order->shipping_date;
                        }

                        if ((isset($item->order->received_date) && $item->order->received_date != '')) {
                            $collectedDate = $item->order->received_date;
                        }elseif ($item->order->collected_date != 'Pending') {
                            $collectedDate = date("d-m-Y", strtotime($item->order->collected_date));
                        }else{
                            $collectedDate = $item->order->collected_date;
                        }

                        switch ($item->order->order_status ) {
                            case '1002': // Order Shipped
                            case '1008': // ACD Delivered
                            case '1010': // ACD Shipout
                            case '1011': // ACD Self Delivered
                            $icon = 'lorry';
                            break;

                            case '1009': // ACD Collected
                            case '1003': // Order Delivered
                            $icon = 'collected';
                            break;

                            default:
                            $icon = 'none';
                                break;
                        }
                        @endphp

                        @if ($loop->last)
                        <div class="col-md-3 col-3">
                            <div class="vr-o-step">
                                <div class="vr-process1 step1">
                                    <img class="img-process active"{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending')  ? 'active' : '' }}
                                    title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? 'Estimate ship out date : ' . $shipDate : '' }}"
                                    src="{{ asset('images/icons/vr-step1.png') }}"
                                    data-toggle="tooltip">
                                </div>

                                <div class="midline"></div>

                                <div class="vr-process1 step2">
                                    <img class="img-process {{  ( $icon == 'collected' || $item->order->order_status == '1002') ? 'active' : '' }}"
                                    title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? ($item->order->delivery_method == 'Self-pickup') ?'Ready For Collection Date : ' . $shipDate : 'Ship out date : ' . $shipDate : '' }}"
                                    {{-- title="Order Placed" --}}
                                    src="{{ asset('images/icons/vr-step2.png') }}"
                                    data-toggle="tooltip">
                                </div>

                                <div class="midline"></div>

                                <div class="vr-process1 step3">
                                    <img class="img-process {{( $icon == 'collected') ? 'active' : ''}}"
                                        title="{{ ($item->order->order_status == '1003') || ($item->order->order_status == '1003') || ($item->order->order_status == '1009')
                                        || ($item->order->order_status == '1009') ? 'Order Complete : ' . $collectedDate : '' }}"
                                        src="{{ asset('images/icons/vr-step3.png') }}"
                                        data-toggle="tooltip">
                                </div>

                                <div class="midline"></div>

                                <div class="vr-process1">
                                    <img class="img-process" src="{{ asset('images/icons/vr-step4.png')}}" data-toggle="tooltip" title="">
                                </div>
                            </div>
                            <br>
                            <br>
                                @if(isset($item->order->courier_name) && isset($item->order->tracking_number))
                                    <div class="btn vr-filt-btn">
                                        @php $tracking_link = \App\Models\Courier\Courier::trackingURL($item->order->courier_name, $item->order->tracking_number); @endphp

                                        <a  @if($tracking_link != "#") target="_blank" href="{{$tracking_link}}"@endif >{{$item->order->courier_name}} Tracking: {{$item->order->tracking_number}}</a>
                                    </div>
                                @endif
                        </div>
                        @endif

                    </div>
                @endif
            @endforeach
        </div>
        <br>
        @endforeach
    @else
        <div class="col-md-9 col-6">
            <div class="db-value-title">
                <p>Value Records</p>
            </div>
        </div>
        <div class="col-md-3 col-6 db-order-no">
            <p>{{count($ForCount)}} <span>Order Placed</span></p>
        </div>
        <div class="col-md-12 col-12 vr-or-dt vr-no-pd">
            <div class="vr-noitem">
                <h6> There are no orders found.</h6>
                <div class="vr-continue-shop">
                    <a class="btn btn-vr-blue" href="/shop">Continue Shopping</a>
                </div>
            </div>
        </div>
    @endif

    <div class="vr-col col-md-4-md-12" style="float:left;">
        {!! $purchases->render() !!}
    </div>
</div>

@endsection

@push('rbsStyles')
    <style>
    .vr-ir-btn {
        margin-left: auto;
        width: auto;
    }
    @media only screen and (max-width: 768px) {
        .vr-ir-btn {
            margin-left: 0px;
        }
    }
    </style>
@endpush

@push('script')
<script>
  //tooltips function//
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endpush

