<?php

namespace App\Models\ShippingInstallations;

use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Products\ProductAttribute;

class Ship_category extends Model
{
    // Set table
    protected $table = 'shipping_categories';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'cat_name',
        'cat_desc',
        'cat_type',
    ];

    /**
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'installation_category');
    }

    // Seach
    public function scopesearchFilter($query, $searchFilter)
    {

        if (!is_null($searchFilter)) {
            return $query
                // ->where([
                //     ['cat_name', 'like', '%' . $searchFilter . '%'],
                //     ['cat_desc', 'like', '%' . $searchFilter . '%'],
                //     ['cat_type', 'like', '%' . $searchFilter . '%'],
                // ])
                ->where('cat_name', 'like', '%' . $searchFilter . '%')
                ->orWhere('cat_type', 'like', '%' . $searchFilter . '%')
                ->orWhere('cat_desc', 'like', '%' . $searchFilter . '%');
        }
        return $query;
    }

    // Rates table
    public function categoriesRates()
    {
        return $this->hasMany('App\Models\ShippingInstallations\Rates', 'category_id', 'id');
    }


    public function attributeShipping()
    {
        return $result = $this->hasMany('App\Models\Products\ProductAttribute', 'shipping_category', 'id');
    }

    public function showActive()
    {
        if ($this->active == 0) {
            $result = 'Inactive';
        }else{
            $result = 'active';

        }
        return $result;
    }
}
