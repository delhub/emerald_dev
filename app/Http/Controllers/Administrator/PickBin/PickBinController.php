<?php

namespace App\Http\Controllers\Administrator\PickBin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Models\Pick\PickBin;
use App\Models\Warehouse\Location\Wh_location;

class PickBinController extends Controller
{
    public static $home;
    public static $header;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$home = 'administrator.pickBin.index';
        self::$header = 'PickBin ';

        self::$create = 'administrator.pickBin.create';
        self::$store = 'administrator.pickBin.store';

        self::$edit = 'administrator.outlet.edit';
        self::$update = 'administrator.outlet.update';

        self::$destroy = 'administrator.outlet.destroy';

        // link to view page
        // view('administrator.pickBin.index');
    }


    public function index(Request $request)
    {
        $tables = PickBin::orderBy('bin_number', 'ASC')
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->locationFilter($request->location)
            ->paginate(10);

        $tables->appends(request()->query());

        $locations = wh_location::get();
        $header = self::$header;

        return view(self::$home, compact('header', 'request', 'tables', 'locations'));
    }

    public function create()
    {
        $countries = PickBin::orderBy('bin_number', 'ASC')->get();

        $locations = wh_location::get();
        return view(self::$create, compact('countries', 'locations'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'bin_number' => 'required',
            'location_id' => 'required',
        ]);

        $existingdata  = PickBin::where('bin_number', $request->input('bin_number'))->first();

        if (!$existingdata) {
            $data = new PickBin;
            $data->bin_number = $request->input('bin_number');
            $data->location_id = $request->input('location_id');

            $data->save();

            return redirect()->route(self::$home)->with('success', 'submit done.');
        } else {
            return back()->with(['error' => self::$header . ' already exist  !!! ']);
        }
    }


    public function qrScanned(Request $request, $bin_number)
    {
        dd($request, $bin_number);

        return view('shop.payment.deliveries.qr-scanned')
            // ->with('order', $order)->with('message', $message)
        ;
    }
}
