<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSortedColumnInWhPickingOrderAndWhBatchItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wh_batch_item', function (Blueprint $table) {
            $table->bigInteger('picking_order_id')->nullable()->after('shelf_id');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->integer('sorted_qty')->default(0)->after('payment_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wh_picking_order', function (Blueprint $table) {
            $table->dropColumn('batch_items_id');
        });
        Schema::table('wh_batch_item', function (Blueprint $table) {
            $table->dropColumn('picking_order_id');
        });
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('sorted_qty');
        });
    }
}
