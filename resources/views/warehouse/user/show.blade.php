@extends('layouts.administrator.main')

@section('pageTitle', 'Users Detail')

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
{{--
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light"> Users / </span> List
    </h4> --}}

    <div class="row">
        <div class="col-md-12">
          <div class="card mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2> Show User</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('warehouse.user.index') }}"> Back</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {{ $user->userInfo->full_name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $user->email }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Roles:</strong>
                            @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                    <label class="badge badge-success">{{ $v }}</label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            </div>
        </div>
    </div>
</div>
@endsection
