<?php

namespace App\Models\Purchases;

use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Traits\AttributeImage;
use App\Models\Products\ViewInventoriesStock;
use App\Models\Purchases\Item;

class ItemBundle extends Model
{
    use AttributeImage;

    // Set table
    protected $table = 'items_bundle';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    // Cast column to another datatype.
    protected $casts = [];

    public function item()
    {
        return $this->belongsTo(Item::class, 'id', 'item_id');
    }

    public function productAttribute()
    {
        return $this->hasOne('App\Models\Products\ProductAttribute', 'product_code', 'primary_product_code');
    }
}
