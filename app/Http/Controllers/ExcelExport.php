<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\Export\ExportController;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelExport  extends DefaultValueBinder implements FromArray, WithHeadings, ShouldAutoSize, WithCustomValueBinder,WithStyles
{
    use Exportable;

    public $heading;
    public $data;

    protected $request;

    public function __construct($request = null)
    {
        $this->request = $request;
    }

    public function setData($data){

        $this->data = $data;
    }

    public function setHeadings($heading){

        $this->heading = $heading;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return $this->heading;
    }

    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);

        return true;

        // else return default behavior
        // return parent::bindValue($cell, $value);
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],

        ];
    }

    public static function prepareDataExport($data){

        $prepared = json_encode($data);
        return $prepared;
    }

    public static function prepareDataImport($data){

        $prepared = json_decode($data,true);
        return $prepared;
    }




}
