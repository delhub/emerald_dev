<?php

namespace App\Http\Controllers\Warehouse\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Models\Globals\City;
use App\Models\Globals\Countries;
use App\Models\Globals\Outlet;
use App\Models\Globals\State;
use App\Models\Warehouse\Location\Wh_location;
use App\Models\Warehouse\Order\WarehouseOrder;

class OrderController extends Controller
{
    public function index()
    {
        dd('this is index in OrderController');
    }

    public static function createNew($data)
    {
        self::process_inventory($data['inventoryRecords']);
    }

    public static function process_WarehouseOrder($data)
    {
        $inventoryTables = new WarehouseOrder();
        return $inventoryTables->create($data)->id;
    }

    public static function process_inventory($data)
    {
        InventoryController::insertData($data);
    }
}
