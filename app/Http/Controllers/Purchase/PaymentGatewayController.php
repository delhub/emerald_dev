<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Models\Purchases\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Jobs\PDF\GeneratePdfPoDoEmail;
use App\Models\Purchases\Purchase;
use App\Models\Users\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Purchases\Order;
use Illuminate\Support\Facades\URL;
use App\Models\Dealers\DealerGroup;
use App\Models\Registrations\Dealer\Registration;

use App\Models\Users\Customers\Cart;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\Dealers\DealerSales;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\BujishuAPI;

use App\Models\Globals\Products\Product;
use App\Models\ShippingInstallations\Fee;

use App\Models\Purchases\Counters;
use App\Traits\Voucher;
use App\Http\Controllers\XilnexAPI;
use App\Http\Controllers\PoslajuAPI;
use App\Http\Controllers\Register\v1\DealerRegisterController;
use App\Models\Globals\BankName;
use App\Models\Globals\PaymentGateway\Response;
use App\Models\Dealers\DealerDirectPay;
use App\Models\Users\Dealers\DealerCountry;
use App\Models\Users\UserPoints;
use stdClass;
use App\Models\Globals\Outlet;
use App\Models\Products\ProductAttribute;
use App\Models\Purchases\RBS;
use App\Models\Rbs\RbsPostpaid;
use Illuminate\Support\Facades\Log;

class PaymentGatewayController extends Controller
{
    use Voucher;
    var $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        // $this->middleware(function ($request, $next) {
        //     // Check if user is authenticated or not.


        //     if (Auth::check()) {
        //         // If authenticated, then get their cart.
        //         $this->cart = Auth::user()->carts->where('status', 2001)->sum('quantity');
        //         $this->user = User::find(Auth::user()->id);
        //     } else {
        //     }
        //     // Get all categories, with subcategories and its images.
        //     $categories = Category::topLevelCategory();

        //     // Share the above variable with all views in this controller.
        //     View::share('categories', $categories);
        //     View::share('getCartQuantity', $this->cart);

        //     // Return the request.
        //     return $next($request);
        // });
    }
    public function paymentStatus(Request $request, $id)
    {
        // sleep(5);
        // return $request->status;
        $purchase = Purchase::where('purchase_number', $id)->first();

        if ($purchase->purchase_status == '4006' || $request->status == 'canceled' && $purchase->purchase_type == 'Hitpay') {

            $errorMessage = array();
            $errorMessage['id'] = 0;
            $errorMessage['response_code'] = 'FAILED';

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $purchaseNumberFormatted = $purchase->getFormattedNumber();
        $installment = $purchase->installment_data;

        $fpx = null;
        $banks = null;
        $responses = null;
        if ($purchase->purchase_type == 'Fpx') {
            $payments = $purchase->payments->first();
            $fpx = json_decode($payments->fpx_field);
            $banks = BankName::where('bank_code', $fpx->fpx_buyerBankId)->first();
            $responses = Response::where('response_code', $fpx->fpx_debitAuthCode)->where('type', 'fpx')->first();
        }
        // if ($purchase->items->first()->product->parentProduct->categories->where('slug','agent-sg-launch')) {

        //     $country = country()->country_id;

        //     return view('sg-launch.package-success')
        //     ->with('purchase', $purchase)
        //     ->with('installment', $installment)
        //     ->with('country', $country)
        //     ->with('purchaseNumberFormatted', $purchaseNumberFormatted);

        // } else{
        $dealer = null;
        $directPayTables = DealerDirectPay::where('purchase_id', $purchase->id)->first();

        if ($directPayTables) {
            $dealer = DealerInfo::where('user_id', $purchase->user_id)->first();
        }

        return view('shop.payment.success')
            ->with('purchase', $purchase)
            ->with('installment', $installment)
            ->with('directPayTables', $directPayTables)
            ->with('dealer', $dealer)
            ->with('purchaseNumberFormatted', $purchaseNumberFormatted)
            ->with('fpx', $fpx)
            ->with('responses', $responses)
            ->with('banks', $banks);
        // }

    }
    /**
     * Submit a new payment gateway request.
     */
    public function paymentGatewayRequest(Request $request)
    {
        $purchaseLimit = PurchaseController::purchaseLimitCheck($request);

        if (!empty($purchaseLimit)) {
            return redirect()->route('cart.index');
        };

        if ($request->directID != null) {

            $directPayTables = DealerDirectPay::where('uuid_link', $request->directID)->first();

            $user = User::find($directPayTables->user_id);

            $request->request->add(['direct_link' => $directPayTables->id]);
            //$user =  something();
        } else {
            $request->request->add(['direct_link' => null]);
            $user =  User::find(Auth::user()->id);
        };



        // dd($user);


        switch ($request->input('payment_option')) {
            case 'Duitnow':
                $paymentOption = 'Offline';
                break;

            case 'Voucher':
                $paymentOption = 'Healthpoint';
                break;

            default:
                $paymentOption = $request->input('payment_option');
                break;
        }

        $discount = Session::get('bjs_cart_discount');

        $autoApply = Session::get('bjs_cart_autoApply');

        if(isset($autoApply)) {
            $discount = $autoApply;
        }

        // dd($discount);

        $voucherData = [];

        if (isset($discount['voucher']) && !empty($discount['voucher'])) {

            foreach ($discount['voucher'] as $key => $voucher) {

                // dd($voucher);
                // $data['code'] = ['coupon_code' => $discount['discount_code'], 'coupon_amount' => $discount['discount_amount']];
                $data['code']['coupon_code'][] = $voucher['discount_code'];
                $data['code']['coupon_amount'][] = $voucher['discount_amount'];

                $data['details'][$key]['coupon_code'] = $voucher['discount_code'];
                $data['details'][$key]['coupon_amount'] = $voucher['discount_amount'];
                // $data['details'][][$voucher['discount_code']][] = $voucher['discount_amount'];
            }

            // dd($data);

            unset($discount['voucher']);

            $data['discount'] = array();

            foreach ($discount as $val) {
                $data['discount'][] = $val;
            }
            // dd();



            $voucherData = ['discount' => $data['discount'], 'code' => $data['code'], 'details' => $data['details']];
        }

        $deliveryMethod = Session::get('bjs_cart_delivery_method');
        $deliveryOutletId = Session::get('bjs_cart_delivery_outletId');

        if (($deliveryMethod == NULL || ($deliveryMethod == 'Self-pickup') && empty($deliveryOutletId)) && $request->directID == null) return redirect('shop/cart');
        $deliveryData = ['method' => $deliveryMethod, 'outletId' => $deliveryOutletId];

        // dd('method', $deliveryData);
        // dd($request, $deliveryMethod, $deliveryOutletId, $deliveryData);
        $purchase = $this->createPurchaseRecord($request, $user, $request->input('payment_option'), $voucherData, $deliveryData);

        if ($paymentOption == 'Offline') {
            $purchase->offline_reference = $request->input('reference_number');
        }

        $gateway_class = 'App\\Http\\Controllers\\Purchase\\' . $paymentOption . 'GatewayController';

        $gatewayController = new $gateway_class; //new controller

        return $gatewayController->submitForm($request, $purchase, $user, $paymentOption);
        // return $gatewayController->gatewayResponse($request, $purchase, $user, $paymentOption);

    }

    public function sendToMarketingServer($user_account_id, $bjs_number, $purchase, $total_amount, $gateway, $transaction_id = '')
    {

        return; // bypass send to server b

        $items = array();
        foreach ($purchase->orders as $order) {
            foreach ($order->items as $key => $item) {
                if ($item->bundle_id != 0) continue;

                $product_code = $item->product_code;
                $variation = $item->product_information;

                if ($product_code == $item->product->parentProduct->product_code) $product_code = self::sanitize_product_code($product_code, $variation);

                $quality =  $item->product->parentProduct->quality->name;
                $item_obj = array();

                $item_obj['sku'] = $product_code;
                $item_obj['title'] = $item->product->parentProduct->name . ' (' . $quality . ')';
                $item_obj['price'] = $item->unit_price / 100;
                $item_obj['quantity'] = $item->quantity;
                $item_obj['delivery'] = $item->delivery_fee / 100;
                $item_obj['variation'] = $variation;

                $items[] = $item_obj;
            }
        }

        $request = BujishuAPI::createOrder($user_account_id, $bjs_number, $total_amount, $gateway, $items, $transaction_id);

        return $request;
    }
    public function markOrderComplete($purchase, $user, $cookies = null)
    {
        // dd($purchase->orders, $user, $cookies);

        if (isset($cookies->cartIds)) {

            $cartItems = Cart::where('user_id', $user->id)
                ->where('status', 2001)
                // ->whereIn('product_id', $productIds)
                ->whereIn('id', $cookies->cartIds)
                ->get();
        } else {
            $productIds = array();

            foreach ($purchase->orders as  $order) {
                foreach ($order->items as  $item) {

                    $cartTables = Cart::where('user_id', $user->id)
                        ->where('status', 2001)
                        ->where('product_id', $item->product_id)
                        ->where('country_id', country()->country_id)
                        ->whereNotIn('disabled', [5])
                        ->get();

                    foreach ($cartTables as $cartTable) {
                        if ($cartTable->product_information === $item->product_information || $cartTable->freegift == 1) {
                            $productIds[] = $cartTable->id;
                        }
                    }
                }
            }

            $cartItems = Cart::whereIn('id', $productIds)->get();
        }

        $directPay = DealerDirectPay::where('user_id', $user->id)->where('purchase_id', $purchase->id)->first();

        if ($directPay) {
            RbsPostpaid::where('active_dp_id', $directPay->id)->update(['active_dp_id' => null]);
            $cartItems = Cart::whereIn('id', json_decode($directPay->cart_id))->get();
        }

        // dd($directPay,$cartItems,json_decode($directPay->cart_id));

        foreach ($cartItems as $cartItem) {
            $cartItem->status = 2003;
            $cartItem->selected = 0;
            $cartItem->save();
        }

        $session = Session::get('bjs_cart_discount');

        // $cookie = json_decode(Cookie::get('bjs_cart'));

        // $voucher = json_decode($purchase->voucher_data, true);

        if (!isset($session['voucher']) && empty($session['voucher'])) {
            $this->generate_purchase_discount_code_v2($purchase, $cartItems);
        } else {
            foreach ($session['voucher'] as $voucher) {
                // dd($session['voucher'],$voucher);
                $this->updateVoucherCodeCount($voucher['discount_code']);
            }
        }

        if(isset($session['voucher'])) {

            if(count($session['voucher']) == 2) {
                $vcode = $session['voucher'][0]['discount_code'];
                $vcode1 = $session['voucher'][1]['discount_code'];
                if(substr($vcode, 0, 2) != 'PW' && substr($vcode1, 0, 2) != 'PW') {
                    $this->generate_purchase_with_purchase($purchase);
                }
            }
            else {
                $vcode = $session['voucher'][0]['discount_code'];
                if(substr($vcode, 0, 2) != 'PW') {
                    $this->generate_purchase_with_purchase($purchase);
                }
            }
        }
        else {
            $this->generate_purchase_with_purchase($purchase);
        }



        // //create sales order
        // $so = XilnexAPI::curlCreateSalesOrder($user->id, $cartItems, $purchase);

        Session::forget('bjs_cart_discount');
        Session::forget('discount_code');
        Session::forget('bjs_cart_autoApply');

        Session::forget('bjs_cart_delivery_method');
        Session::forget('bjs_cart_delivery_outletId');

        //delete item removed status
        $cartItems = Cart::where('user_id', $user->id)->where('status', 2002)->delete();

        $directPayTables = DealerDirectPay::where('purchase_id', $purchase->id)->first();

        $url = isset($directPayTables) ? '/direct-pay/payment/' : '/payment/';

        if ($purchase->process == 'fpxd') {
            return 'OK';
        }

        return redirect($url . $purchase->purchase_number);
    }

    public function createOrderStoreRegistration($purchase, $user, $check_registration, $paymentOption, $send_email, $fpxChecksum = false)    {
        // if purchase 4002/4007 - update inv_number and inv_date
        if (in_array($purchase->purchase_status, [4002, 4007]) && !$purchase->inv_number) {
            $counters = Counters::autoIncrementInv(['invoice'], $purchase);
            $purchase->inv_number =  $counters['invoice'];
            if (!$purchase->inv_date) $purchase->inv_date = Carbon::now();

            $purchase->save();
        }

        $registrationItem = $registrationLaunching = 0;

        foreach ($purchase->orders as $order) {
            if ($paymentOption != 'Offline') {
                if ($fpxChecksum && $fpxChecksum == '09') {
                    $order->order_status = 1005;
                    foreach ($order->items as $item) {
                        // $item->delivery_order = str_replace("PO", "BDO", $item->order_number);
                        $item->item_order_status = 1005;
                        $item->save();
                        $attributes = ProductAttribute::where('product_code', $item->product_code)->first();
                        $currentSold = $attributes->sales_count;
                        $attributes->sales_count = $currentSold + $item->quantity;
                        $attributes->save();
                    }
                } else {
                    $order->order_status = ($order->delivery_method == 'Self-pickup') ? 1010 : 1001;
                    $order->order_date = Carbon::now()->format('d/m/Y');
                    $rbsMonth = $purchase->rbs_data ? $purchase->rbs_data['month']:null;
                    $i = 0;
                    foreach ($purchase->orders->where('order_type', 'rbs') as $rbsOrder) {
                        // dd($rbsOrder,$rbsMonth);
                        if ($i == 0) {
                            $rbsOrder->order_date = Carbon::now()->format('d/m/Y');
                        } else {
                            $rbsOrder->order_date = Carbon::now()->addMonths($rbsMonth)->format('d/m/Y');
                            $rbsMonth += $purchase->rbs_data['month'];
                        }
                        $i++;
                    }

                    foreach ($order->items as $item) {
                        // $item->delivery_order = str_replace("PO", "BDO", $item->order_number);

                        $item->item_order_status = ($order->delivery_method == 'Self-pickup') ? 1010 : 1001;
                        $item->save();
                        $attributes = ProductAttribute::where('product_code', $item->product_code)->first();
                        $currentSold = $attributes->sales_count;
                        $attributes->sales_count = $currentSold + $item->quantity;
                        $attributes->save();
                    }
                }
            }

            // $order->delivery_order = str_replace("PO", "BDO", $order->order_number);
            // $order->delivery_order = str_replace("PO", "BDO", $order->order_number);
            $order->save();
            GeneratePdfPoDoEmail::dispatch($order, $order->panel->company_email);

            if ($check_registration) {
                foreach ($order->items as $item) {
                    if ($item->product->parentProduct->categories->where('name', 'Launching Registration')->count() >= 1) {
                        $registrationLaunching = 1;
                        $package = $item->product->parentProduct;
                    }
                    if ($item->product->parentProduct->categories->where('slug', 'agent-registration')->count() >= 1) {
                        $registrationItem = 1;
                        $package = $item->product->parentProduct;
                    }
                    if ($item->product->parentProduct->categories->where('slug', 'agent-upgrade')->count() >= 1) {
                        $registrationItem = 0;
                        $package = $item->product->parentProduct;
                    }
                }
            }
        }

        if ($registrationItem == 1 && $check_registration) {

            $item = Registration::where('invoice_number', $purchase->inv_number)->first();
            if ($item) return;

            $registration = new Registration;
            $registration->invoice_number = $purchase->inv_number;
            $registration->package_id = $package->id;
            $registration->buyer_id = $user->id;
            $registration->country_id = $purchase->country_id;
            $registration->save();

        } else if ($registrationLaunching == 1 && $check_registration) {

            $delearInfo = DealerInfo::where('user_id', $user->id)->first();
            if (!$delearInfo) {
                $registrationRecord = new Registration;
                $registrationRecord->package_id = '0';
                $registrationRecord->agent_id = '0';
                $registrationRecord->country_id = country()->country_id;
                (new DealerRegisterController)->createDealer(request(), $user, $registrationRecord, true);

                $delearInfo = $user->dealerInfo;
                $dealer = new DealerCountry;
                $dealer->account_id = $delearInfo->account_id;
                $dealer->country_id = country()->country_id;
                $dealer->dealer_id = country()->country_id . $delearInfo->account_id;
                $dealer->referrer_id = country()->country_id . $delearInfo->referrer_id;
                $dealer->group_id = $delearInfo->group_id;
                $dealer->save();
            }
        }
    }


    public function updateGroupSales($user, $purchase)
    {

        $referId = DealerInfo::where('account_id', $user->userInfo->referrer_id)->first();



        if ($referId) {
            $groupSales = DealerGroup::find($referId->group_id);
            if (isset($groupSales)) {
                $groupSales->sales_amount = $groupSales->sales_amount + $purchase->purchase_amount;
                $groupSales->save();
            }
        } else {
            $groupSales = DealerGroup::find(1);
            $groupSales->sales_amount = $groupSales->sales_amount + $purchase->purchase_amount;
            $groupSales->save();
        }
        // return $groupSales;
        // dd ($groupSales);
    }


    /**
     * Create Purchase, Order & Item records
     */
    public function createPurchaseRecord($purchases, $user, $paymentOption, $voucherData, $deliveryData)
    {
        // Test date using carbon
        // $knownDate = Carbon::create(2021, null, null, null);
        // Carbon::setTestNow($knownDate);
        // return ($purchases->address->address1);
        // return $purchases;
        $counters = Counters::autoIncrement(['purchase', 'receipt']);
        $purchaseNumber = $counters['purchase'];

        // dd($paymentDetails);
        // Create a new purchase record.
        $purchase = new Purchase;
        // Assign user to the purchase record.
        $purchase->user_id = $user->id;
        // Generate a unique number used to identify the purchase.
        $purchase->purchase_number = $purchaseNumber;
        // Assign a status to the purchase. Unpaid, paid.
        $purchase->purchase_status = 3000;
        $purchase->invoice_version = 0;

        // Assign the current date to the purchase in the form of DD/MM/YYYY.
        $purchase->purchase_date = Carbon::now()->format('d/m/Y');
        if (isset($voucherData["code"]) && isset($voucherData["code"]["coupon_amount"])) {
            $disc = 0;
            foreach ($voucherData["code"]["coupon_amount"] as $coupon_amount) {
                $disc += $coupon_amount;
            }
        }
        // isset($voucherData["code"]["coupon_amount"]) ? $disc = array_sum(array_column($voucherData["code"],'coupon_amount')):  $disc = 0;

        // dd($voucherData["code"],((float)$disc) * 100, $purchases->finalTotal, (((float)$disc) * 100) + $purchases->finalTotal);

        $purchase->purchase_amount = $purchases->finalTotal < 0 ?  (((float)$disc) * 100) + $purchases->finalTotal : $purchases->finalTotal;
        $purchase->point_amount = $purchases->finalTotalPoint;


        // if (country()->country_id == 'SG') {
        //     if (request()->input('installmentRadio') == 'installment'){
        //         $purchase->purchase_amount = request()->input('installmentMY');

        //     } elseif (request()->input('installmentRadio') == 'full'){
        //         $purchase->purchase_amount = (request()->input('fullMY'));

        //     } else {
        //         $purchase->purchase_amount = request()->input('total_price') ?? $purchases->purchase_amount;
        //     }
        // }
        $purchase->purchase_amount = str_replace(',', '', $purchase->purchase_amount);

        $receiptNumber = $counters['receipt'];
        // dd($purchases->address);
        // return is_object($purchases->address);

        $address = json_decode($purchases->address);
        // return $address->name;
        $purchase->receipt_number = $receiptNumber;

        // if ($deliveryData['method'] == 'Self-pickup' && $deliveryData['outletId'] != NULL && !empty($deliveryData['outletId'])) {

        //     $outletInfo = Outlet::where('id', $deliveryData['outletId'])->first();

        //     $purchase->ship_full_name = $outletInfo->outlet_name;
        //     $purchase->ship_contact_num = $outletInfo->contact_number;
        //     $purchase->ship_address_1 = $outletInfo->default_address;
        //     // $purchase->ship_address_2 = $address->address_2;
        //     // $purchase->ship_address_3 = $address->address_3;
        //     $purchase->ship_postcode = $outletInfo->postcode;
        //     // $purchase->ship_city =  $address->city;
        //     $purchase->ship_city_key =  $outletInfo->city_key;
        //     $purchase->ship_state_id = $outletInfo->state_id;
        // } else {
        $purchase->ship_full_name = $address->name;
        $purchase->ship_contact_num = $address->mobile;
        $purchase->ship_address_1 = $address->address_1;
        $purchase->ship_address_2 = $address->address_2;
        $purchase->ship_address_3 = $address->address_3;
        $purchase->ship_postcode = $address->postcode;
        $purchase->ship_city =  $address->city;
        $purchase->ship_city_key =  $address->city_key;
        $purchase->ship_state_id = $address->state_id;
        // }

        $purchase->installment = 0;
        $purchase->country_id = country()->country_id;

        $purchase->purchase_type = $paymentOption == 'Cybersource' ? 'Card' : $paymentOption;

        if ($this->checkAvailableRule('generate_checkout')) {
            $purchase->voucher_data = json_encode($voucherData);
        } else {
            $purchase->voucher_data = NULL;
        }
        // return $purchase;

        if (isset($voucherData["code"]["coupon_code"]) && !empty($voucherData["code"]["coupon_code"])) {
            $voucher = $voucherData;

            if (count($voucher['details'])) {

                foreach ($voucher['details'] as $k => $v) {

                    $vCount = collect($voucher['details'])->where('coupon_code', $v["coupon_code"])->count();

                    if ($vCount > 1) {
                        $usage_count = $this->voucherUsageCount($v["coupon_code"]) + $k + 1;
                        $v["coupon_code"] = $v["coupon_code"] . '_' . $usage_count;
                        $voucher_details[] = $v;
                    } else {
                        $usage_count = $this->voucherUsageCount($v["coupon_code"]) + 1;
                        $v["coupon_code"] = $v["coupon_code"] . '_' . $usage_count;
                        $voucher_details[] = $v;
                    }
                }
            }

            $voucher_info = ['purchase_number' => $purchase->purchase_number, 'voucher_code' => $voucher["code"]["coupon_code"], 'details' => $voucher_details];

            // dd($paymentOption, $voucherData['code']['coupon_amount'] > ($purchase->purchase_amount/100),$voucherData['code']['coupon_amount'] , ($purchase->purchase_amount/100));

            if ($paymentOption == 'Voucher') {
                $purchase->payment_type = 7004;
            } else {
                $purchase->payment_type = 7003;
            }
        }

        $purchase->save();

        if ($purchases->input('direct_link') != null) {
            $directPayTables = DealerDirectPay::where('id', $purchases->input('direct_link'))->first();
            $directPayTables->purchase_id = $purchase->id;
            $directPayTables->save();
        }

        if (isset($voucher["code"]["coupon_code"]) && !empty($voucher["code"]["coupon_amount"])) {
            $voucher['shipping_information'] = json_encode($voucher_info);

            // $shipping_information = array_merge($voucher_info,$voucher["details"]);

            // dd($voucher['shipping_information']);

            $coupon_amount = 0;
            $shippingInfo = json_decode($voucher['shipping_information']);
            foreach ($shippingInfo->details as $voucherKey => $details) {


                $fee = new Fee;
                $fee->name = ucwords('Discount Fee') . ' (' . $details->coupon_code . ')';
                $fee->purchase_id = $purchase->id;
                $fee->type = 'rebate_fee';
                $fee->amount = $details->coupon_amount * 100;
                $fee->shipping_information = json_encode(['voucher_code' => $details->coupon_code, 'purchase_number' => $purchase->purchase_number]);
                //  {"voucher_code": "A2D9D6FD", "purchase_number": "FWS21-007514"} $voucher['shipping_information'];

                $fee->save();
            }
        }

        $carTotalsFee = json_decode($purchases->shippingFee);
        if (isset($carTotalsFee->amount) && !empty($carTotalsFee->amount)) {

            $fee = new Fee;

            $fee->name = ucwords('shipping Fee');
            $fee->purchase_id = $purchase->id;
            $fee->type = 'shipping_fee';
            $fee->amount = $carTotalsFee->amount;
            $fee->shipping_information = json_encode($carTotalsFee->shipping_information);

            $fee->save();
        }

        $panels = array();

        // Create order record.
        // Foreach item in the cart..
        // dd(unset($purchases->product));
        $product = json_decode($purchases->product);
        // if (($key = array_search(['issuer'], $product)) !== false) {
        //     unset($messages[$key]);
        //     dd($key);
        // }
        // $productUnset = array_diff($product, ['issuer', 'finalTotal']);

        $cartItems = Cart::whereIn('id', collect($product))->get();
        $bundle_parent = array();


        // dd($purchases, $cartIds, $cartItems, $user, $paymentDetails, $carTotalsFee, $voucherData);
        // dd($cartItems);

        foreach ($cartItems as $cartItem) {
            if ($cartItem->bundle_id != 0) {
                if (!isset($bundle_parent[$cartItem->bundle_id])) $bundle_parent[$cartItem->bundle_id] = true;
            }
            // Create a new PO Number for each different panel belonging to an item.
            // will do in better way for panel_id (2011000000 = Formula2u)
            $panels[2011000000][] =  $cartItem;
        }

        // Initialize an empty variable to store panel's id.
        $panelId = null;
        // Foreach PO Number..

        $data = [
            'content_description' => 'Suppliment', // package content
            'content_value' => number_format(($purchase->purchase_amount / 100), 2),
            'receiver_name' => $purchase->ship_full_name,
            'receiver_phone' => $purchase->ship_contact_num,
            'receiver_email' => $user->email,
            'receiver_address_line_1' => $purchase->ship_address_1,
            'receiver_address_line_2' => $purchase->ship_address_2,
            'receiver_address_line_3' => $purchase->ship_address_3,
            'receiver_address_line_4' => '',
            'receiver_postcode' => $purchase->ship_postcode,
        ];

        // $pl = PoslajuAPI::generate_consignment_note($data);
        // dd( $pl);
        $rbs = array();
        foreach ($panels as $key => $panelProducts) {
            // dd($panelProducts);
            // Create a new order record.
            $order = new Order;
            // Create a new PO Number for each different panel belonging to an item.
            $counters = Counters::autoIncrement(['po', 'do']);
            // $po_number = $counters['invoice'];
            // Assign PO Number to the order.
            $order->order_number = $counters['po'];
            // Assign DO Number to the order.
            $order->delivery_order = $counters['do'];

            // Assign purchase id to the order
            $order->purchase_id = $purchase->id;

            // Assign the panel id to the order record
            $order->panel_id = $key;

            // Assign a status for the order. Placed, Shipped, Delivered.
            $order->order_status = 1000;
            // Assign empty value for order amount first.
            $orderAmount = 0;
            $orderPointAmount = 0;
            $order->order_amount = 0;
            $order->point_amount = 0;
            $order->delivery_method = $deliveryData['method'];

            if ($deliveryData['method'] == 'Self-pickup' && $deliveryData['outletId'] != NULL && !empty($deliveryData['outletId'])) {
                $order->delivery_method = $deliveryData['method'];
                $order->delivery_outlet = ($deliveryData['outletId']) ? $deliveryData['outletId'] : 0;
            }
            $order->delivery_date = "Pending";
            $order->received_date = "";
            $order->claim_status = "";
            $order->country_id = country()->country_id;
            //  $order->delivery_method = 'Delivery';
            // $order->delivery_outlet = 1;
            // $order->delivery_info = json_encode($pl);

            $order->save();

            $dealer_sales = new DealerSales;

            //Assign PO number to order
            $dealer_sales->order_number = $counters['po'];

            //Assign purchase id to dealer sales
            $dealer_sales->purchase_id = $purchase->id;

            //Assign dealer id to the order record
            // FIXME: What if the customer is not a dealer?
            // Wan replaced with refferer_id instead of using dealer ID from dealer account.
            $dealer_sales->account_id = $user->userInfo->referrer_id;

            //Assign empty value for order amount first
            $dealer_sales->order_amount = $orderAmount;

            // Assign a status for the order. Placed, Shipped, Delivered.
            $dealer_sales->order_status = 1000;

            $panelId = $key;
            foreach ($panelProducts as $cartItem) {
                // return $cartItem->product->id;
                $item = new Item;
                // Assign an order number to the item
                $item->order_number = $counters['po'];
                // Assign an order number to the item
                $item->delivery_order = $counters['do'];
                // Assign a product id to the item
                $item->product_code = isset($cartItem->product_code) ? $cartItem->product_code : $cartItem->product->parentProduct->product_code;
                // Assign currently price key
                $item->price_key = $cartItem->price_key;
                // Assign a product id to the item
                $item->product_id = $cartItem->product->id;
                // Get the cart product's information. Color, dimension or length..
                // Store it in an array, easier to access later and avoid creating another column just for an attribute of a product
                // $cartItem->product_information
                $product_information = $cartItem->product_information;
                // $product_information['product_code'] =  $cartItem->freegift == 0 ? $product_information['product_code'] : $product_information['product_code'] . ' (Freegift)';

                $item->product_information = $product_information;
                // Assign item quantity.
                $item->quantity = $cartItem->quantity;
                // Assign unit price.
                $item->unit_price = $cartItem->freegift == 0 ? $cartItem->unit_price : 0;
                // Assign subtotal price.
                $item->subtotal_price = $cartItem->freegift == 0 ? $cartItem->subtotal_price : 0;
                // Assign unit price.
                $item->unit_point = $cartItem->unit_point;
                // Assign subtotal price.
                $item->subtotal_point = $cartItem->subtotal_point;

                $item->payment_type = $cartItem->payment_type;
                // Assign delivery fee.
                $item->delivery_fee = $cartItem->delivery_fee;
                // Assign rebate fee.
                $item->rebate = $cartItem->rebate;
                // Assign installation fee.
                $item->installation_fee = $cartItem->installation_fee;
                // Assign a status for the order. Placed, Shipped, Delivered.
                $item->item_order_status = 1000;
                // Assign status of the item. Placed, shipped, delivered.
                $item->status_id = 1;

                // After checkout, cart items should be removed from cart page

                // Auto Calculate estimate ship out date in items
                if ($cartItem->product->estimate_ship_out_date != '0') {
                    $orderdate = now();
                    $estimate_dates = $cartItem->product->estimate_ship_out_date;

                    $item->ship_date = date('Y-m-d', strtotime($orderdate . ' + ' . $estimate_dates . ' days'));
                }
                // TODO: Change status after payment successful.
                // $cartItem->status = 2001;
                // $cartItem->save();
                $item->bundle_id = $cartItem->bundle_id;

                $item->country_id = country()->country_id;

                $item->save();

                if (isset($bundle_parent[$cartItem->id])) $bundle_parent[$cartItem->id] = $item->id;
                $item->save();

                // if (isset($cartItem->product_information['product_rbs_id'])) {

                //     $rbs_purchases = new RBS;
                //     $rbs_purchases->purchases_id = $purchase->id;
                //     $rbs_purchases->total_months = $cartItem->product_information['rbs_total_months'];
                //     $rbs_purchases->months_completed = 1;
                //     $rbs_purchases->reminder_interval_months = $cartItem->product_information['rbs_reminder_interval_months'];
                //     $rbs_purchases->start_reminder_date = Carbon::now();
                //     $rbs_purchases->latest_reminder_date = Carbon::now();
                //     $rbs_purchases->first_po = $item->order_number;
                //     $rbs_purchases->first_do = $item->delivery_order;
                //     $rbs_purchases->payment_type = 'fully_paid';
                //     $rbs_purchases->rbs_status = 'pending';
                //     $rbs_purchases->save();

                //     $purchase->order_type = 'rbs';
                //     $purchase->save();

                //     $order->order_type = 'rbs';
                //     $order->purchases_rbs_id = $rbs_purchases->id;
                //     $order->save();
                // }

                $orderAmount = $orderAmount + $cartItem->subtotal_price;
                $orderPointAmount = $orderPointAmount + $cartItem->subtotal_point;

                if (array_key_exists('product_rbs_times', $cartItem['product_information'])) {
                    $rbs[$order->order_number]['times'] =  $cartItem['product_information']['product_rbs_times'];
                    $rbs[$order->order_number]['month'] =  $cartItem['product_information']['product_rbs_frequency'];
                }
            }
            $order->order_amount = $orderAmount;
            $order->point_amount = $orderPointAmount;
            $order->payment_type = $cartItem->payment_type;

            $order->save();

            $bundle_orders[] = $item->order_number;

            $dealer_sales->order_amount = $orderAmount;
            $dealer_sales->save();
        }

        if (!empty($rbs)) {
            foreach ($rbs as $key => $rbsMain) {
                // dd($rbsMain);
                $orders = Order::where('order_number', $key)->first();
                $items = Item::where('order_number', $key)->get();

                $order->order_type = 'rbs';
                $order->save();


                for ($i = 2; $i <= $rbsMain['times']; $i++) {
                    $newOrders = $orders->replicate();
                    $newOrders->id = 'new';
                    $newOrders->order_type = 'rbs';
                    $newOrders->order_number = $orders->order_number . '-' . $i;
                    $newOrders->delivery_order = $orders->delivery_order . '-' . $i;
                    $newOrders->push();


                    foreach ($items as $item) {
                        $item->order_type = 'rbs';
                        $item->save();

                        $newItems = $item->replicate();
                        $newItems->id = 'new';
                        $newItems->bundle_id = $item->id;
                        $newItems->order_type = 'rbs';
                        $newItems->order_number = $item->order_number . '-' . $i;
                        $newItems->delivery_order = $item->delivery_order . '-' . $i;
                        $newItems->push();
                    }
                }
            }
            // dd($rbsMain);
            $purchase->rbs_data = $rbsMain;
            $purchase->order_type = 'rbs';
            $purchase->save();
        }

        // $items = Item::whereIn('order_number',  $bundle_orders)->get();

        // // query all items by current purchase
        // foreach ($items as $item) {
        //     if (isset($bundle_parent[$item->bundle_id])) {
        //         $item->bundle_id = $bundle_parent[$item->bundle_id];
        //     } else {
        //         $item->bundle_id = 0;
        //     }
        //     $item->save();
        // }
        Log::channel('debugg')->info('createPurchaseRecord - ' . $order);
        return $purchase;
    }


    public function regenerateInvoice(Request $request, $purchaseNumber)
    {
        $purchase = Purchase::where('purchase_number', $purchaseNumber)->first();

        if (!$purchase) return 'INVOICE NOT FOUND';

        // $user = User::find($purchase->user_id);

        $purchase->increment('invoice_version');


        try {

            GeneratePdfInvoiceAndReceiptEmail::createInvoiceReceiptPDF($purchase);


            foreach ($purchase->orders as $order) {

                GeneratePdfPoDoEmail::generatePoDoPdf($order);
            }

            return  response()->json('OK');
        } catch (Exception $e) {

            return 'Error - ' . $e->getMessage();
        }
    }


    public function regenerateInvoices(Request $request)
    {
        $date  = $request->input('date');

        $purchases = Purchase::where('purchase_date', $date)->get();

        $array = [];

        foreach ($purchases as $purchase) {

            $array[] = $purchase->purchase_number;
        }

        // dd($array);

?>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

        <script>
            var items = <?php echo json_encode($array); ?>;
            // parameters for ajax calls


            // function to trigger the ajax call
            var ajax_request = function(item) {
                var deferred = $.Deferred();
                var link = '<?php echo URL::to('/') ?>/administrator/regenerate-invoice/' + item;
                $.ajax({
                    url: link,
                    dataType: "json",
                    type: 'GET',
                    success: function(data) {
                        // do something here
                        $('body').append(item + ' Regenerate Success: ' + data + '<br/>');
                        // mark the ajax call as completed
                        console.log(data);
                        deferred.resolve();
                    },
                    error: function(error) {
                        // mark the ajax call as failed
                        $('body').append(item + ' Regenerate Failed: ' + error.responseJSON.message + '<br/>');

                        deferred.resolve();

                        //deferred.reject(error);
                    }
                });

                return deferred.promise();
            };

            var looper = $.Deferred().resolve();

            // go through each item and call the ajax function
            $.when.apply($, $.map(items, function(item, i) {
                looper = looper.then(function() {
                    // trigger ajax call with item data
                    return ajax_request(item);
                });

                return looper;
            })).then(function() {
                // run this after all ajax calls have completed
                $('body').append('Regenerate Done<br/>');
            });
        </script>


<?php


        return 'Regeneration Process Started<br/>';
    }
    /**
     * Handles payment gateway response.
     */
    public function paymentGatewayResponse(Request $request)
    {
        // Go to respective controller
        $gateway_class = 'App\\Http\\Controllers\\Purchase\\' . ucfirst($request->process) . 'GatewayController';

        $gatewayController = new $gateway_class; //new controller
        // return $request;

        return $gatewayController->gatewayResponse($request);
    }

    public function saveUserPoints($payment, $points)
    {

        $userPoint = new UserPoints;
        $userPoint->user_id = $payment->purchase->user_id;
        $userPoint->point_type = $points['point_type'];
        $userPoint->amount = $points['amount'];
        $userPoint->transaction_type = $points['transaction_type'];
        $userPoint->status = $points['status'];
        $userPoint->balance = $points['balance'];
        $userPoint->used = 0;
        $userPoint->details = null;
        $userPoint->purchase_id = $payment->purchase->id;
        $userPoint->ref_id = null;
        $userPoint->created_by = null;
        $userPoint->deleted = 0;
        $userPoint->approved_at = null;
        $userPoint->expiry_at = null;
        $userPoint->save();

        return $userPoint;
    }

    public function errorPage()
    {
        return view('shop.payment.failed');
    }

    public static function sanitize_product_code($product_code, $variation)
    {


        $product_code = str_replace(' ', '.', $product_code);
        $variation;

        if (isset($variation['product_size'])) {
            switch ($size = $variation['product_size']) {
                case '5 Liter':
                    $product_code .= '.5L';
                    break;
                case '1 Liter':
                    $product_code .= '.1L';
                    break;
                case '15 Liter':
                    $product_code .= '.15L';
                    break;
            }
            switch (true) {
                case stristr($size, '5W'):
                    $product_code .= '.5W';
                    break;
                case stristr($size, '10W'):
                    $product_code .= '.10W';
                    break;
                case stristr($size, '15W'):
                    $product_code .= '.15W';
                    break;
                case stristr($size, '18W'):
                    $product_code .= '.18W';
                    break;
                case stristr($size, '12W'):
                    $product_code .= '.12W';
                    break;
                case stristr($size, '22W'):
                    $product_code .= '.22W';
                    break;
            }
        }

        if (isset($variation['product_curtain_size'])) {
            switch ($size = $variation['product_curtain_size']) {
                case 'French Windows':
                    $product_code .= '.FW';
                    break;
                case 'Sliding Door 2 Panel':
                    $product_code .= '.SL2P';
                    break;
                case 'Sliding Door 3 Panel':
                    $product_code .= '.SL3P';
                    break;
                case 'Sliding Door 4 Panel':
                    $product_code .= '.SD4P';
                    break;
                case 'Window 2 Panel':
                    $product_code .= '.W2P';
                    break;
                case 'Window 3 Panel':
                    $product_code .= '.W3P';
                    break;
                case 'Window 4 Panel':
                    $product_code .= '.W4P';
                    break;
                case 'Window L Shape':
                    $product_code .= '.WLS';
                    break;
                case 'Window Special Curve':
                    $product_code .= '.WSC';
                    break;
            }
        }

        return $product_code;
    }

    public function viewPDF($pdf_type, $purchase_number, $order_number = 'nothing')
    {

        $purchase = Purchase::where('purchase_number', $purchase_number)->first();

        switch ($pdf_type) {
            case 'invoice':
                return redirect(URL::asset('storage/documents/invoice/' . $purchase->getFormattedNumber() .
                    (($purchase->invoice_version != 0) ? '/v' . $purchase->invoice_version . '/' : '/') . $purchase->getFormattedNumber() . '.pdf'));
                break;
            case 'receipt':
                return redirect(URL::asset('storage/documents/invoice/' . $purchase->getFormattedNumber() .
                    (($purchase->invoice_version != 0) ? '/v' . $purchase->invoice_version . '/' : '/') . $purchase->getFormattedNumber() . '-receipt.pdf'));
                break;
            case 'delivery-order':
                $order = Order::where('delivery_order', $order_number)->first();
                return redirect(URL::asset('storage/documents/invoice/' . $purchase->getFormattedNumber() .
                    (($purchase->invoice_version != 0) ? '/v' . $purchase->invoice_version . '/' : '/') . 'delivery-orders/' . $order->delivery_order . '.pdf'));
                break;


            default:
                # code...
                break;
        }
    }
}
