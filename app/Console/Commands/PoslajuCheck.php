<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Http\Controllers\PoslajuAPI;
use App\Models\Purchases\Item;

class PoslajuCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poslaju:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Poslaju Checkout Consignment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dt = Carbon::now();
        $dt->subDays(7);

        $items = Item::where([
            ['courier_name','POSLAJU'],
            ['tracking_number',NULL],
            ['created_at','>', $dt->format("Y-m-d H:i:s")]
        ])
        ->whereNotNull('shipment_key')
        ->get();

        if(count($items) > 0) {
            foreach ($items as $key => $val) {
                if(isset($val->shipment_key) && !empty($val->shipment_key)) {
                    PoslajuAPI::posLajuCheckout($val->id,$val->shipment_key);
                    sleep(3);
                }
                
            }
        }
    }
}
