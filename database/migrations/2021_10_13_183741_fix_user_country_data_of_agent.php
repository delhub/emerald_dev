<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Users\Dealers\DealerCountry;

class FixUserCountryDataOfAgent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dealerDatas = DealerCountry::where('country_id' , 'MY')->get();

        foreach ($dealerDatas as $dealerData){

            if (!isset($dealerData->dealerInfo->user->userInfo)) {
                echo $dealerData->account_id . " No User Info. \r\n";
                continue;
            }

            if ($dealerData->dealerInfo->account_status != 3) continue;

            $user_info = $dealerData->dealerInfo->user->userInfo;

            $user_info->referrer_id = $dealerData->account_id;
            $user_info->save();

            $userCountryMY = $user_info->userCountries->where('country_id', 'MY')->first();

            if(!isset($userCountryMY)) {
                echo $dealerData->account_id . " No User Country. \r\n";
                continue;
            }
            $userCountryMY->group_id = $dealerData->group_id;
            $userCountryMY->referrer_id = 'MY' . $dealerData->account_id;
            $userCountryMY->save();

            echo $userCountryMY->account_id . " Updated. \r\n";

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
