@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Cart Page</a></li>
            </ul>
        </div>
        <section>
            <div class="card-home">

                <section class="checkout-page content">
                        <div class="fml-container">
                          <div class="row">
                                <div class="col-12 col-md-8">
                                    <div class="checkout-box pay yellow-ball-bg">
                                        <div class="shopping-tittle blue-box-bg">
                                            <p>Payment Method</p>
                                        </div>
                                        <div class="hr-line-cart"></div>
                                        <div class="payment-box my-mt-1 my-mb-6">
                                          <form class="my-mb-1">
                                            <div class="form-row pay-methodbox5">
                                              <ul class="nav nav-tabs form-row pay-methodbox5" id="myTab" role="tablist">
                                                <li class="nav-item active">
                                                  <a class="nav-link active" id="payOne-tab" data-toggle="tab" href="#payOne" role="tab" aria-controls="payOne" aria-selected="false">
                                                    <div class="pay-itembox">
                                                      <div class="pay checkout-tick">
                                                          <input type="radio" name="payment-mh" value="pay01" id="pay01" checked><label for="pay01"></label>
                                                      </div>
                                                      <img src="{{asset('/images/icons/pay_icon03.svg')}}" alt="">
                                                      <h5>Credit/ Debit Card</h5>
                                                    </div>
                                                  </a>
                                                </li>
                                                <li class="nav-item">
                                                  <a class="nav-link" id="payTwo-tab" data-toggle="tab" href="#payTwo" role="tab" aria-controls="payTwo" aria-selected="false">
                                                    <div class="pay-itembox">
                                                      <div class="pay checkout-tick">
                                                        <input type="radio" name="payment-mh" value="pay02" id="pay02"/><label for="pay02"></label>
                                                      </div>
                                                      <img src="{{asset('/images/icons/pay_icon01.svg')}}" alt="">
                                                      <h5>FPX Online Banking</h5>
                                                    </div>
                                                  </a>
                                                </li>
                                                <li class="nav-item">
                                                  <a class="nav-link" id="paythree-tab" data-toggle="tab" href="#paythree" role="tab" aria-controls="pay1three" aria-selected="false">
                                                    <div class="pay-itembox">
                                                      <div class="pay checkout-tick">
                                                        <input type="radio" name="payment-mh" value="pay03" id="pay03"/><label for="pay03"></label>
                                                      </div>
                                                      <img src="{{asset('/images/icons/pay_icon02.svg')}}" alt="">
                                                      <h5>Offline Payment</h5>
                                                    </div>
                                                  </a>
                                                </li>
                                                <li class="nav-item">
                                                  <a class="nav-link" id="payFour-tab" data-toggle="tab" href="#payFour" role="tab" aria-controls="payFour" aria-selected="false">
                                                    <div class="pay-itembox">
                                                      <div class="pay checkout-tick">
                                                        <input type="radio" name="payment-mh" value="pay04" id="pay04"/><label for="pay04"></label>
                                                      </div>
                                                      <img src="{{asset('/images/icons/bluePoint.png')}}" alt="">
                                                      <h5>Blue Points</h5>
                                                    </div>
                                                  </a>
                                                </li>
                                                <li class="nav-item">
                                                  <a class="nav-link" id="payFive-tab" data-toggle="tab" href="#payFive" role="tab" aria-controls="payFive" aria-selected="false">
                                                    <div class="pay-itembox">
                                                      <div class="pay checkout-tick">
                                                        <input type="radio" name="payment-mh" value="pay05" id="pay05"/><label for="pay05"></label>
                                                      </div>
                                                      <img src="{{asset('/images/icons/duitnow.svg')}}" alt="">
                                                      <h5>Duit now</h5>
                                                    </div>
                                                  </a>
                                                </li>
                                              </ul>

                                            </div>
                                            <div class="form-row tab-content">
                                                <div class="col-12 tab-pane active" id="payOne" role="tabpanel" aria-labelledby="payOne-tab">
                                                  <div class="form-row my-pt-2 red-ball-bg">
                                                    <div class="form-group col-md-6">
                                                      <label for="cart-num">Card Number<span>*<span></label>
                                                      <input type="text" class="form-control" id="card-num" placeholder="card-num">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                      <label for="cart-name">Card Holder Name<span>*<span></label>
                                                      <input type="text" class="form-control" id="card-name" placeholder="card-name">
                                                    </div>
                                                  </div>
                                                  <div class="form-row my-pt-1 pay-mb">
                                                    <div class="form-group col-md-6">
                                                      <label for="cart-type">Card Type<span>*<span></label>
                                                        <select id="cart-type" class="form-control">
                                                          <option selected>Choose...</option>
                                                          <option>...</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                      <label for="cart-date">Card Expiration Date<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-date" placeholder="MM/YY" maxlength="10">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                      <label for="cart-cvv">CVV</label>
                                                      <input type="text" class="form-control" id="cart-cvv" placeholder="000" maxlength="3">
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-12 tab-pane" id="payTwo" role="tabpanel" aria-labelledby="payTwo-tab">
                                                  <div class="form-row my-pt-3 red-ball-bg">
                                                    <div class="form-group col-md-6">
                                                      <label for="cart-num">Card Number<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="cart-num">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                      <label for="cart-name">Card Holder Name<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-name" placeholder="cart-name">
                                                    </div>
                                                  </div>
                                                  <div class="form-row my-pt-1">
                                                    <div class="form-group col-md-6">
                                                      <label for="cart-type">Card Type<span>*<span></label>
                                                        <select id="cart-type" class="form-control">
                                                          <option selected>Choose...</option>
                                                          <option>...</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                      <label for="cart-date">Card Expiration Date<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-date" placeholder="MM/YY" maxlength="10">
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                      <label for="cart-cvv">CVV</label>
                                                      <input type="text" class="form-control" id="cart-cvv" placeholder="000" maxlength="3">
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-12 tab-pane" id="paythree" role="tabpanel" aria-labelledby="paythree-tab">
                                                  <div class="form-row my-pt-3 red-ball-bg">
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-num">Reference Number<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="Reference num">
                                                    </div>
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="formFileLg" class="form-label">Upload Payment Proof<span>*<span></label>
                                                      <input type="file" name="upload" accept="image/*" id="fileUpload" />
                                                    </div>
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-name">Payment Amount (MYR)<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-name" placeholder="Payment Amount">
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-12 tab-pane" id="payFour" role="tabpanel" aria-labelledby="payFour-tab">
                                                  <div class="form-row my-pt-3 red-ball-bg">
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-num">Your Total Blue Point<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="0 Blue Points" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-num">Your Availabe Point<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="0 Blue Points" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-num">Your Total Blue Point<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="10 Blue Points" disabled>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-12 tab-pane" id="payFive" role="tabpanel" aria-labelledby="col-12 payFive-tab">
                                                  <div class="form-row my-pt-3 red-ball-bg">
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-num">Reference Number<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="Reference Number">
                                                    </div>
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="formFileLg" class="form-label">Upload Payment Proof<span>*<span></label>
                                                      <input type="file" name="upload" accept="image/*" id="fileUpload" placeholder="jpg/png" />
                                                    </div>
                                                    <div class="form-group col-md-6 my-pb-1">
                                                      <label for="cart-num">Payment Amount<span>*<span></label>
                                                      <input type="text" class="form-control" id="cart-num" placeholder="0.00">
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="hr-line-cart my-mt-2 my-mb-2"></div>
                                            <div class="form-check checkbox-style form-submit-list padding-zero">
                                              <input class="form-check-input" type="checkbox" id="gridCheck">
                                              <label class="form-check-label" for="gridCheck"></label>
                                              <span> I have read, understand and accepted the <a href="#termsNconditions" id="DeleteCartItem" data-bs-toggle="modal" role="button">terms and conditions</a>. </span>
                                            </div>
                                            <button type="submit" class="btn red-btn float-right">Submit</button>
                                          </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="checkout-box">
                                        <div class="pay red-ball-bg">
                                            <div class="shopping-tittle">
                                                <p>Order Summary</p>
                                            </div>
                                        </div>
                                        <div class="total-price light-bg my-mb-1">
                                            <table class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="price-details-1">Subtotal (Items)</td>
                                                    <td class="price-details-2">MYR13000.00</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="price-details-1">Shipping Fee</td>
                                                    <td class="price-details-2">MYR30.00</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="price-details-1">Discount</td>
                                                    <td class="price-details-2">MYR30.00</td>
                                                  </tr>
                                                  <tr class="hr-line-cart">
                                                    <td class="price-details-red1">Total </td>
                                                    <td class="price-details-red2">MYR13030.00</td>
                                                  </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pay-pd-scroll">
                                          <div class="row pay-itemlist">
                                            <div class="col-4 col-sm-4">
                                              <img src="{{asset('/images/product_item/pay_product.png')}}" class="product-itemimg" alt="">
                                            </div>
                                            <div class="col-8 col-sm-8">
                                              <ul>
                                                <li>
                                                  <p>Blackmores Executive B Stress Formula</p>
                                                </li>
                                                <li>
                                                  <p>Twin Pack (250 Tablets)</p>
                                                </li>
                                                <li class="price-qlt">
                                                  <p>X<span>1</span></p>
                                                  <p><span>MYR 100.00</span></p>
                                                </li>
                                              </ul>
                                            </div>
                                            <div class="hr-line-cart"></div>
                                          </div>
                                          <div class="row pay-itemlist">
                                            <div class="col-4 col-sm-4">
                                              <img src="{{asset('/images/product_item/pay_product.png')}}" class="product-itemimg" alt="">
                                            </div>
                                            <div class="col-8 col-sm-8">
                                              <ul>
                                                <li>
                                                  <p>Blackmores Executive B Stress Formula</p>
                                                </li>
                                                <li>
                                                  <p>Twin Pack (250 Tablets)</p>
                                                </li>
                                                <li class="price-qlt">
                                                  <p>X<span>1</span></p>
                                                  <p><span>MYR 100.00</span></p>
                                                </li>
                                              </ul>
                                            </div>
                                            <div class="hr-line-cart"></div>
                                          </div>

                                          <div class="row pay-itemlist">
                                            <div class="col-4 col-sm-4">
                                              <img src="{{asset('/images/product_item/pay_product.png')}}" class="product-itemimg" alt="">
                                            </div>
                                            <div class="col-8 col-sm-8">
                                              <ul>
                                                <li>
                                                  <p>Blackmores Executive B Stress Formula</p>
                                                </li>
                                                <li>
                                                  <p>Twin Pack (250 Tablets)</p>
                                                </li>
                                                <li class="price-qlt">
                                                  <p>X<span>1</span></p>
                                                  <p><span>MYR 100.00</span></p>
                                                </li>
                                              </ul>
                                            </div>
                                            <div class="hr-line-cart"></div>
                                          </div>

                                          <div class="row pay-itemlist">
                                            <div class="col-4 col-sm-4">
                                              <img src="{{asset('/images/product_item/pay_product.png')}}" class="product-itemimg" alt="">
                                            </div>
                                            <div class="col-8 col-sm-8">
                                              <ul>
                                                <li>
                                                  <p>Blackmores Executive B Stress Formula</p>
                                                </li>
                                                <li>
                                                  <p>Twin Pack (250 Tablets)</p>
                                                </li>
                                                <li class="price-qlt">
                                                  <p>X<span>1</span></p>
                                                  <p><span>MYR 100.00</span></p>
                                                </li>
                                              </ul>
                                            </div>
                                            <div class="hr-line-cart"></div>
                                          </div>

                                          <div class="row pay-itemlist">
                                            <div class="col-4 col-sm-4">
                                              <img src="{{asset('/images/product_item/pay_product.png')}}" class="product-itemimg" alt="">
                                            </div>
                                            <div class="col-8 col-sm-8">
                                              <ul>
                                                <li>
                                                  <p>Blackmores Executive B Stress Formula</p>
                                                </li>
                                                <li>
                                                  <p>Twin Pack (250 Tablets)</p>
                                                </li>
                                                <li class="price-qlt">
                                                  <p>X<span>1</span></p>
                                                  <p><span>MYR 100.00</span></p>
                                                </li>
                                              </ul>
                                            </div>
                                            <div class="hr-line-cart"></div>
                                          </div>

                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                </section>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="address_edit_form" aria-hidden="true" aria-labelledby="address_edit_formLabel" tabindex="-1">
  <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
      <div class="modal-content edit-info-modal-box">
          <div class="modal-header">
          <h1 class="modal-title edit-tittle" id="address_edit_formLabel">Edit Your <span>Details</span></h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form>
                  <div class="mb-3 cart-form1">
                  <label for="exampleInputEmail1" class="form-label">Email address</label>
                  <input type="email" class="checkout-box2 form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="example@gmail.com">
                  <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                  </div>
                  <div class="mb-3 cart-form1">
                      <label for="exampleInputEmail1" class="form-label">Contact (Mobile)</label>
                      <input type="text" class="checkout-box2 form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contact Number">
                      <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                  </div>
                  <div class="cart-edit-location">
                      <div class="mb-3 cart-form3">
                          <label for="">State</label>
                          <select class="form-select checkout-box3" aria-label="Default select example">
                              <option selected>Select</option>
                              <option value="1">Pahang</option>
                              <option value="2">Perlis</option>
                              <option value="3">Sabah</option>
                          </select>
                      </div>
                      <div class="mb-3 cart-form2">
                          <label for="">City</label>
                          <select class="form-select checkout-box3" aria-label="Default select example">
                              <option selected>Select</option>
                              <option value="1">One</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                          </select>
                      </div>
                  </div>

                  <div class="mb-3 cart-form1">
                      <label for="exampleFormControlInput1" class="form-label">Address 1</label>
                      <input type="text" class="checkout-box2 form-control" id="exampleFormControlInput1" placeholder="Address 1">
                  </div>
                  <div class="mb-3 cart-form1">
                      <label for="exampleFormControlInput1" class="form-label">Address 2</label>
                      <input type="text" class="checkout-box2 form-control" id="exampleFormControlInput1" placeholder="Address 2">
                  </div>
                  <div class="mb-3 cart-form1">
                      <label for="exampleFormControlInput1" class="form-label">Address 3</label>
                      <input type="text" class="checkout-box2 form-control" id="exampleFormControlInput1" placeholder="Address 3">
                  </div>
                  <div class="mb-3 cart-form1">
                      <label for="exampleFormControlInput1" class="form-label">Postcode</label>
                      <input type="text" class="checkout-box2 form-control" id="exampleFormControlInput1" placeholder="Postcode">
                  </div>
              </form>
      </div>
      <div class="edit-opt-btn modal-footer">
            <button class="cancel-btn  btn btn-primary">Back</button>
            <button class="savechg-btn btn-primary">Save Changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="termsNconditions" aria-hidden="true" aria-labelledby="termsNconditionsLabel" tabindex="-1">
  <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
      <div class="modal-content edit-info-modal-box">
          <div class="modal-header">
          <h1 class="modal-title edit-tittle" id="termsNconditionsLabel">Terms and Conditions</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body pay-tnc">
              <p>
                Terms and Conditions
              </p>
          </div>
      <div class="edit-opt-btn modal-footer">
            <button class="cancel-btn  btn btn-primary" data-bs-dismiss="modal" aria-label="Close">Close</button>
      </div>
    </div>
  </div>
</div>

<div class=""></div>
@endsection


@push('script')
<script>

</script>
@endpush
