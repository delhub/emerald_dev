<?php

namespace App\Models\Users\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerInfo extends Model
{
    // Set table
    protected $table = 'dealer_infos';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'user_id';

    // Set mass assignable columns
    protected $fillable = [
        'account_id',
        'full_name',
        'nric',
        'is_dealer_employment_address'
    ];

    /**
     * Get the user info associated with the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id');
    }

    //**Get the purchases tied to the dealer***/
    public function purchase()
    {
        return $this->hasMany('App\Models\Purchases\Purchase', 'account_id', 'dealer_id');
    }

    /**
     * Get all of the user's addresses.
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\Users\UserAddress', 'account_id', 'account_id');
    }

    /**
     * Get dealer employment address.
     */
    public function employmentAddress()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerEmployment', 'account_id', 'account_id');
    }

    /**
     * Get dealer billing address.
     */
    public function billingAddress()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerAddress', 'account_id', 'account_id')->where('is_mailing_address', 1);
    }

    /**
     * Get dealer shipping address.
     */
    public function shippingAddress()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerAddress', 'account_id', 'account_id')->where('is_shipping_address', 1);
    }

    /**
     * Get the user contact associated with the user.
     */
    public function contacts()
    {
        return $this->hasMany('App\Models\Users\Dealers\DealerContact', 'account_id', 'account_id');
    }

    /*****
     * Get mobile contact number of dealer
     ***/
    public function dealerMobileContact()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerContact', 'account_id', 'account_id')->where('is_mobile', 1);
    }

    /**
     * Get home contact number.
     */
    public function dealerHomeContact()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerContact', 'account_id', 'account_id')->where('is_home', 1);
    }

    /**
     * Get largest dealer account id.
     */
    public function scopeLargestDealerId($query)
    {
        // return $this->where('account_id', 'LIKE', '3911%')->max('account_id');
        return $query->max('account_id');
    }

    /**
     * Get dealer spouse information.
     */
    public function dealerSpouse()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerSpouse', 'account_id', 'account_id');
    }

    /**
     * Get dealer bank information.
     */
    public function dealerBankDetails()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerBankDetails', 'account_id', 'account_id');
    }


    /**Get dealer sales information**/
    public function dealerSales()
    {
        return $this->hasMany('App\Models\Users\Dealers\DealerSales', 'account_id', 'account_id');
    }

    /**
     * Get agent's gender.
     */
    public function gender()
    {
        return $this->belongsTo('App\Models\Globals\Gender', 'gender_id', 'id');
    }

    /**
     * Get agent's race.
     */
    public function race()
    {
        return $this->belongsTo('App\Models\Globals\Race', 'race_id', 'id');
    }

    /**
     * Get agent's marital status.
     */
    public function maritalStatus()
    {
        return $this->belongsTo('App\Models\Globals\Marital', 'marital_id', 'id');
    }

    /**
     * Get agent's group.
     */
    public function agentGroup()
    {
        return $this->belongsTo('App\Models\Dealers\DealerGroup', 'group_id', 'id');
    }

    /**
     * Get agent's package.
     */
    public function agentPackage()
    {
        return $this->belongsTo('App\Models\Registrations\Dealer\Packages', 'package_type', 'id');
    }

    public function dealerCountries()
    {
        return $this->hasMany('App\Models\Users\Dealers\DealerCountry', 'account_id', 'account_id');
    }

    public function getDealershipAttribute()
    {
        $dealerCountries = $this->dealerCountries;

        $dealership = array();

        foreach ($dealerCountries as $dealerCountry) {
            $dealership[] = $dealerCountry->country_id;
        }
        return $dealership;
    }

    public function dealershipInfo()
    {
        $dealerCountries = $this->dealerCountries;

        $dealershipInfo = array();

        foreach ($dealerCountries as $dealerCountry) {
            $dealershipInfo[$dealerCountry->country_id] = array(
                'dealer_id' => $dealerCountry->dealer_id,
                'referrer_id' => $dealerCountry->referrer_id,
                'group_id' => $dealerCountry->referrer_id
            );
        }
        return $dealershipInfo;
    }

    public function getIsDealershipAttribute(){

        $dealership = array(country()->country_id);

        return array_intersect ( $this->dealership, $dealership )  ? true : false;
    }

    public function dealer_employment_address()
    {
        return $this->hasOne('app\Models\Users\Dealers\DealerEmployment', 'account_id', 'account_id')
            ->where('is_dealer_employment_address', 1);
    }

}
