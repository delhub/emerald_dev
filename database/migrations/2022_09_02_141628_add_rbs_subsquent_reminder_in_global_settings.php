<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddRbsSubsquentReminderInGlobalSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_settings')->insert([
            'name' => 'rbs_subsquent_reminder',
            'title' => 'Rbs Subsquent Reminder',
            'value' => json_encode([
                ['days' => 7, 'title' => "Third Reminder"],
                ['days' => 5, 'title' => "Second Reminder"],
                ['days' => 3, 'title' => "First Reminder"]
            ]),
            'type' => 'json',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('global_settings')->where('name', 'rbs_subsquent_reminder')->where('type', 'json')->delete();
    }
}
