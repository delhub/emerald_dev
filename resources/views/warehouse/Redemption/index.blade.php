@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">Redemption
                @if (getUser('warehouseLocationName') != '-')
                (
                @foreach (getUser('warehouseLocationName') as $location)
                    <b>{{$location}}</b>@if (!$loop->last), @endif
                @endforeach
                )
                @endif
            </h1>
        </div>
    </div>

    <form action="{{ route('warehouse.redemption.index') }}" method="GET">
        <div class="row mt-2 ml-2">
            <!-- Status -->
            <div class="col-md-auto px-1">
                <select name="status" class="col-12 text-left custom-select">
                    <option value="all">All Status</option>
                    @foreach ($statuses as $status)
                    <option value="{{ $status->id }}" {{ $request->status == $status->id ? 'selected' : ''}}>{{$status->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-auto ml-auto">
                <div class="row">
                    <div class="col-md-8 p-0">
                        <select class="custom-select" id="selectSearch" name="selectSearch">
                            <option value="ALL" selected>Choose...</option>
                            <option value="BJN" {{ $request->selectSearch == 'BJN' ? 'selected' : '' }}>Invoice Number</option>
                            <option value="PODO" {{ $request->selectSearch == 'PODO' ? 'selected' : '' }}>DO Number</option>
                        </select>

                        <input type="text" class="form-control" id="" name="searchBox"
                            value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                    </div>
                    <div class="col-md-4 ">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                    </div>
                </div>
            </div>
        </div>

        <div style="text-align:center;">
            <div class="col-md-auto px-1">
                <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
                <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('warehouse.redemption.index') }} style="color:black;">Reset Filter</a></button>
                <button type="submit" class="btn btn-warning" style="color:black;"><a href="{{route('warehouse.redemption.exportCsv', ['status'=> $request->status,'selectSearch' => $request->selectSearch , 'searchBox' => $request->searchBox])}}" id="export" style="color:black; ">Download</a></button>
            </div>
        </div>

        <br>

        <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Orders) : <b>{{ count($ForCountcustomerOrders)}}</b></span></p>
        <div class="row">
            <div class="col-auto ml-auto">
                {{ $customerOrders->links() }}
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-bordered text-center table-striped " id="sales-tracking" style="width:100%">
                    <thead style="background-color:	#E8E8E8;">
                        <tr>
                            <th scope="col">Action</th>
                            <th scope="col">Invoice No./ DO No.</th>
                            <th scope="col">Order Date</th>
                            {{-- <th scope="col">Pending Days</th> --}}
                            <th scope="col">Order Status</th>
                            <th scope="col">States</th>
                            <th scope="col">Pick & Pack Status</th>
                            <th scope="col">Remark</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customerOrders as $customerOrder)
                        <tr>

                            <td>
                                @php $itemid = array(); foreach ($customerOrder->items  as $item) $itemid[] = $item->id; @endphp

                                <form action="{{ route('administrator.ordertracking.detail')}}">
                                    <input type="hidden" name="manyID" value="{{ implode("," , $itemid )}}">
                                    <button class="btn btn-primary" type="submit" formtarget="_blank">Detail</button>
                                </form>
                            </td>

                            <td>
                                <a target="_blank"
                                href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->purchase->getFormattedNumber().(($customerOrder->purchase->invoice_version != 0) ? '/v'.$customerOrder->purchase->invoice_version.'/' : '/').$customerOrder->purchase->getFormattedNumber().'.pdf')}}"
                                >{{$customerOrder->purchase->getFormattedNumber()}}
                                {{ $customerOrder->purchase->invoice_version }}
                                </a>
                                <hr>
                                <a target="_blank"
                                href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->purchase->getFormattedNumber().(($customerOrder->purchase->invoice_version != 0) ? '/v'.$customerOrder->purchase->invoice_version.'/' : '/').'delivery-orders/'.$customerOrder->delivery_order.'.pdf')}}"
                                >{{$customerOrder->delivery_order}}
                                </a>
                            </td>

                            <td>
                                {{ $customerOrder->order_date }}
                            </td>

                            {{-- <td>
                                {{$customerOrder->getPendingAttribute()}}
                            </td> --}}

                            <td>
                                @foreach ($statuses as $status)
                                    @if ($status->id == $customerOrder->order_status)
                                        {{ $status->name }}
                                    @endif
                                @endforeach
                                &nbsp;
                                <br>
                                @if ($customerOrder->order_status = 1010 && isset($customerOrder->outlet))
                                    <p class="text-capitalize text-secondary m-0">{{$customerOrder->outlet->outlet_name}}</p>
                                @endif
                            </td>

                            <td>
                                {{ $customerOrder->purchase->state->name }}
                            </td>

                            <td>
                                {{showPickPackStatus($customerOrder->pick_pack_status)}}
                            </td>

                            <td>
                                <form action="{{ route('update.items.adminremark',[$customerOrder->order_number]) }}"
                                    method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <textarea name='admin_remarks' class="form-control" value="{{$customerOrder->admin_remarks}}" placeholder="Admin remark" >{{$customerOrder->admin_remarks}}</textarea>

                                <input type="submit" class="btn btn-primary" value="Submit">
                                </form>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-auto ml-auto py-3">
            {{ $customerOrders->links() }}
        </div>
    </div>

</div>

@push('style')
    <style>
        .text-bold {
            font-weight: bold;
        }

        .table .thead-light th {
            font-weight: bold;
            font-family: 'Nunito', sans-serif;
        }

        .largerCheckBox{
            width: 18px;
            height: 18px;
        }

        @media(max-width:767px) {
            .hidden-sm {
                display: none;
            }

        }

        @media(min-width:767px) {
            .hidden-md {
                display: none;
            }
        }


        .text-font-family {
            font-family: 'Nunito', sans-serif;
        }

        /* freeze header  */
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

            table  { border-collapse: collapse; width: 100%; }
            th     { background:#eee; }

    </style>
@endpush
@endsection
