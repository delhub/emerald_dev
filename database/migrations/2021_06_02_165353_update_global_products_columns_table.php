<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\ShippingInstallations\Zones;
use App\Models\ShippingInstallations\Locations;
use App\Models\ShippingInstallations\Ship_category;

class UpdateGlobalProductsColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->string('ingredients')->nullable()->after('name_slug');
            $table->string('warning')->nullable()->after('ingredients');
            $table->string('caution')->nullable()->after('warning');
        });

        Schema::table('panel_products', function (Blueprint $table) {
            $table->string('product_content_3')->nullable()->change();

            $table->bigInteger('panel_account_id')->nullable()->change();
            $table->string('display_panel_name')->nullable()->change();
            $table->integer('origin_state_id')->nullable()->change();
            $table->bigInteger('shipping_category')->nullable()->change();
        });

        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->bigInteger('fixed_price')->nullable(false)->default(0)->change();
            $table->bigInteger('price')->nullable(false)->default(0)->change();
            $table->bigInteger('member_price')->nullable(false)->default(0)->change();
            $table->bigInteger('offer_price')->nullable(false)->default(0)->change();
        });

        DB::table('categories')->insert(
            array(
                array('name' => 'Adult', 'slug' => 'adult', 'type' => 'suitable_for', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Elderly', 'slug' => 'elderly', 'type' => 'suitable_for', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Kids', 'slug' => 'kids', 'type' => 'suitable_for', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

                array('name' => 'Malaysia', 'slug' => 'malaysia', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Australia', 'slug' => 'australia', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Indonesia', 'slug' => 'indonesia', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'India', 'slug' => 'india', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        /*********************    Zone   *********************/
        $zoneName = array(
            array('zone_nameFromExcel' => 'Peninsular  Malaysia', 'sequence' => '50'), //2
            array('zone_nameFromExcel' => 'Sabah & Sarawak', 'sequence' => '45'), //3
        );

        $locatonids = array(
            array('location_type' => 'state', 'location_id' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 16]), //2 - West Malaysia
            array('location_type' => 'state', 'location_id' => [10, 11]), //3 - East West Coat include Johor
        );


        foreach ($zoneName as $key => $value) {
            $zone = new Zones;
            $zone->zone_name = $value['zone_nameFromExcel'];
            $zone->sequence = $value['sequence'];
            $zone->country_id = 'MY';
            $zone->save();

            $zoneIds[] = $zone->id;
        }

        foreach ($locatonids as $key => $ids) {
            foreach ($ids['location_id'] as $k => $id) {
                $location = new Locations;
                $location->zone_id = $zoneIds[$key];
                $location->location_type = $ids['location_type'];
                $location->location_id = $id;
                $location->save();
            }
        }
        /*********************    End Zone   *********************/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
