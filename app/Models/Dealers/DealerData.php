<?php

namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class DealerData extends Model
{
    // Set table
    protected $table = 'dealer_data';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'dealer_id'
    ];

    // Cast column to another datatype.
    protected $casts = [];


    static function getAgentData($name)
    {

        $id = Auth::user()->dealerInfo->account_id;

        return self::getData($id, $name);
    }

    static function getData($id, $name)
    {

        $item = DealerData::where('dealer_id', $id)->where('name', $name)->first();

        return (isset($item)) ? $item->value : null;
    }

    static function getAllAgentData($id)
    {

        $allData = DealerData::where('dealer_id', $id)->get();

        return $allData;
    }

    public function stockLedger()
    {
        return $this->hasMany('App\Models\Dealers\DealerStockLedger', 'agent_id', 'dealer_id');
    }

    public function dealerInfo()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerInfo', 'account_id', 'dealer_id');
    }

    public function archimedesTransaction()
    {
        return $this->hasMany('App\Models\Dealers\DealerTask', 'referrer_id', 'dealer_id');
    }

    public function scopesearchFilter($query, $search)
    {
        if (!is_null($search)) {
            return $query
                ->join('dealer_infos', 'dealer_infos.account_id', '=', 'dealer_data.dealer_id')
                ->select('dealer_infos.full_name', 'dealer_data.*')
                ->where('dealer_data.dealer_id', 'like', '%' . $search . '%')
                ->orWhere('dealer_infos.full_name', 'like', '%' . $search . '%');
        }

        return $query->whereRaw('1 = 1');
    }
}
