<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Models\Globals\BankName;
use App\Models\Globals\Cards\Issuer;
use App\Models\Globals\PaymentGateway\MerchantID;
use App\Models\Globals\PaymentGateway\Response;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use App\Models\Users\Customers\PaymentInfo;
use Illuminate\Support\Facades\URL;
use App\Models\Users\Customers\Cart;
use App\Models\Users\User;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\InteractsWithTime;

class FpxGatewayController extends Controller implements ShouldQueue
{
    // use Dispatchable, InteractsWithTime, Queueable, SerializesModels;
    public function privKey()
    {
        if (\App::environment('prod')) {
            $keyN =  'EX00011298';
        } else {
            $keyN =  'EX00013186';
        }
        return $priv_key = file_get_contents(storage_path('keys/' . $keyN . '.key'));
    }

    public function fpxID($type)
    {
        if (\App::environment('prod')) {
            if ($type == 'xchange') {
                return 'EX00011298';
            } else if ($type == 'seller') {
                return 'SE00048091';
            } else {
                return "fpxprod_Merchant";
            }
        } else {
            if ($type == 'xchange') {
                return 'EX00013186';
            } else if ($type == 'seller') {
                return 'SE00014726';
            } else {
                return "fpxuat_current";
            }
        }
    }

    public function submitForm($request, $purchase, $user, $paymentOption)
    {

        // dd($request, $purchase,$user,$paymentOption);
        //  dd(array_slice(json_decode($request->product), 0, -6));

        // array_slice($request->product, 0, 3);
        // Handle fpx payment option
        //  $product_desc = Cart::whereIn('id',$cookie->cartIds)->pluck('product_code')->toArray();

        $fpx = array();

        $fpx['fpx_msgType'] = "AR";
        $fpx['fpx_msgToken'] = $request->bankRadioOptions == 'b2c' ? '01' : '02';
        $fpx['fpx_sellerExId'] = $this->fpxID('xchange');
        $fpx['fpx_sellerExOrderNo'] = $purchase->purchase_number . '-' . uniqid();
        $fpx['fpx_sellerTxnTime'] = date('YmdHis');
        $fpx['fpx_sellerOrderNo'] = $purchase->purchase_number;
        $fpx['fpx_sellerId'] = $this->fpxID('seller');
        $fpx['fpx_sellerBankCode'] = "01";
        $fpx['fpx_txnCurrency'] = "MYR";
        $fpx['fpx_txnAmount'] = number_format(($purchase->purchase_amount / 100), 2, '.', '');
        $fpx['fpx_buyerEmail'] = $user->email;
        $fpx['fpx_checkSum'] = "";
        $fpx['fpx_buyerName'] = "";
        $fpx['fpx_buyerBankId'] = $request->bankRadioOptions == 'b2c' ? $request->bankSelectedb2c : $request->bankSelectedb2b1;
        $fpx['fpx_buyerBankBranch'] = "";
        $fpx['fpx_buyerAccNo'] = "";
        $fpx['fpx_buyerId'] = "";
        $fpx['fpx_makerName'] = "";
        $fpx['fpx_buyerIban'] = "";
        $fpx['fpx_productDesc'] = 'Formula Healthcare products';
        $fpx['fpx_paymentOption'] = $paymentOption;
        $fpx['fpx_version'] = "7.0";

        // return $fpx;

        /* Generating signing String */
        $data = $fpx['fpx_buyerAccNo'] . "|" . $fpx['fpx_buyerBankBranch'] . "|" . $fpx['fpx_buyerBankId'] . "|" . $fpx['fpx_buyerEmail'] . "|" . $fpx['fpx_buyerIban'] . "|" . $fpx['fpx_buyerId'] . "|" . $fpx['fpx_buyerName'] . "|" . $fpx['fpx_makerName'] . "|" . $fpx['fpx_msgToken'] . "|" . $fpx['fpx_msgType'] . "|" . $fpx['fpx_productDesc'] . "|" . $fpx['fpx_sellerBankCode'] . "|" . $fpx['fpx_sellerExId'] . "|" . $fpx['fpx_sellerExOrderNo'] . "|" . $fpx['fpx_sellerId'] . "|" . $fpx['fpx_sellerOrderNo'] . "|" . $fpx['fpx_sellerTxnTime'] . "|" . $fpx['fpx_txnAmount'] . "|" . $fpx['fpx_txnCurrency'] . "|" . $fpx['fpx_version'];

        /* Reading key */
        // $priv_key = file_get_contents(storage_path('key/EX00013186.key'));
        $pkeyid = openssl_get_privatekey($this->privKey());
        openssl_sign($data, $binary_signature, $pkeyid, OPENSSL_ALGO_SHA1);
        $fpx['fpx_checkSum'] = strtoupper(bin2hex($binary_signature));

        // return $fpx;

        Log::channel('fpx')->info($fpx);

        if (App::environment('prod')) {
            $url = 'https://www.mepsfpx.com.my/FPXMain/seller2DReceiver.jsp';
        } else {
            $url = 'https://uat.mepsfpx.com.my/FPXMain/seller2DReceiver.jsp';
        }


        // $responseUrl = URL::to('/payment/fpx-gateway-response');

        $payment = new Payment;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->purchase_uuid = $fpx['fpx_sellerExOrderNo'];
        $payment->gateway_response_code = 'PENDING';
        $payment->amount = $purchase->purchase_amount;
        $payment->local_amount =  (country()->country_id == 'SG') ? round($fpx['fpx_txnAmount'] * 100) : 0;
        $payment->gateway_string_result = '-';
        $payment->auth_code = '-';
        $payment->last_4_card_number = '-';
        $payment->expiry_date = '-';
        $payment->gateway_eci = '-';
        $payment->gateway_security_key_res = '-';
        $payment->gateway_hash = '-';
        $payment->save();

        // $responseUrl = URL::to('/payment/gateway-response');
        return view('shop.payment.fpx.post-to-bank')
            // ->with('request', $request)
            ->with('fpx', $fpx)
            ->with('url', $url)
            // ->with('responseUrl', $responseUrl)
        ;
    }

    public function getBankLists($msgToken)
    {

        // return $this->environment();

        $fpx_msgToken = $msgToken;
        $fpx_msgType = "BE";
        $fpx_sellerExId = $this->fpxID('xchange');
        $fpx_version = "7.0";
        /* Generating signing String */
        $data = $fpx_msgToken . "|" . $fpx_msgType . "|" . $fpx_sellerExId . "|" . $fpx_version;
        /* Reading key */
        $pkeyid = openssl_get_privatekey($this->privKey());
        openssl_sign($data, $binary_signature, $pkeyid, OPENSSL_ALGO_SHA1);
        $fpx_checkSum = strtoupper(bin2hex($binary_signature));


        //extract data from the post

        extract($_POST);
        $fields_string = "";

        //set POST variables
        if (App::environment('prod')) {
            $url = 'https://www.mepsfpx.com.my/FPXMain/RetrieveBankList';
        } else {
            $url = 'https://uat.mepsfpx.com.my/FPXMain/RetrieveBankList';
        }

        $fields = array(
            'fpx_msgToken' => urlencode($fpx_msgToken),
            'fpx_msgType' => urlencode($fpx_msgType),
            'fpx_sellerExId' => urlencode($fpx_sellerExId),
            'fpx_checkSum' => urlencode($fpx_checkSum),
            'fpx_version' => urlencode($fpx_version)
        );

        Log::channel('fpx')->info($fields);

        $response_value = array();
        $bank_list = array();

        // $client = new Client();
        // $response = $client->post( $url,$fields);
        // $token = strtok($response->getBody(), "&");
        //   dd($token);

        try {
            //url-ify the data for the POST
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }
            rtrim($fields_string, '&');

            //open connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //execute post
            $result = curl_exec($ch);

            //close connection
            curl_close($ch);

            if ($result == 'ERROR') return array();

            $token = strtok($result, "&");

            while ($token !== false) {
                list($key1, $value1) = explode("=", $token);
                $value1 = urldecode($value1);
                $response_value[$key1] = $value1;
                $token = strtok("&");
            }

            $fpx_msgToken = reset($response_value);


            //Response Checksum Calculation String
            $data = $response_value['fpx_bankList'] . "|" . $response_value['fpx_msgToken'] . "|" . $response_value['fpx_msgType'] . "|" . $response_value['fpx_sellerExId'];

            //  dd($data);


            Log::channel('fpx')->info($response_value);


            $token = strtok($response_value['fpx_bankList'], ",");
            //echo '<pre>';
            //print_r($response_value['fpx_bankList']);
            //echo '</pre>';
            while ($token !== false) {
                list($key1, $value1) = explode("~", $token);
                $value1 = urldecode($value1);
                $bank_list[$key1] = $value1;
                $token = strtok(",");
            }
        } catch (Exception $e) {
            echo 'Error :', ($e->getMessage());
        }
        // return $bank_list;
        $bank = array();

        if (App::environment('prod')) {
            if ($msgToken == '01') {
                $type = 'b2c_prod';
            } else {
                $type = 'b2b1_prod';
            }
        } else {
            if ($msgToken == '01') {
                $type = 'b2c_staging';
            } else {
                $type = 'b2b1_staging';
            }
        }

        foreach ($bank_list as $key => $value) {
            $bankData = BankName::where('bank_code', $key)->where('type', $type)->first();
            $bank[$key]['name'] = $bankData ? $bankData->bank_name : $key;
            $bank[$key]['status'] = $value == 'A' ? '' : '- Offline';
        }

        $lowerCase = array_map('strtolower', array_column($bank, 'name'));

        array_multisort($lowerCase, SORT_ASC, $bank);

        return $bank;
    }

    public function gatewayResponse($request)
    {

        Log::channel('transactions')->info($request->input());

        $paymentGatewayResponse = new PaymentGatewayController;

        $payment = Payment::where('purchase_number', $request->fpx_sellerOrderNo)->first();

        $purchase = Purchase::where('purchase_number', $request->fpx_sellerOrderNo)->first();

        if ($purchase && $payment) {
            $user = User::find($purchase->user_id);
            $response = $request->fpx_debitAuthCode;

            $data = $request->fpx_buyerBankBranch . "|" . $request->fpx_buyerBankId . "|" . $request->fpx_buyerIban . "|" . $request->fpx_buyerId . "|" . $request->fpx_buyerName . "|" . $request->fpx_creditAuthCode . "|" . $request->fpx_creditAuthNo . "|" . $request->fpx_debitAuthCode . "|" . $request->fpx_debitAuthNo . "|" . $request->fpx_fpxTxnId . "|" . $request->fpx_fpxTxnTime . "|" . $request->fpx_makerName . "|" . $request->fpx_msgToken . "|" . $request->fpx_msgType . "|" . $request->fpx_sellerExId . "|" . $request->fpx_sellerExOrderNo . "|" . $request->fpx_sellerId . "|" . $request->fpx_sellerOrderNo . "|" . $request->fpx_sellerTxnTime . "|" . $request->fpx_txnAmount . "|" . $request->fpx_txnCurrency;

            $checksum = $this->verifySign_fpx($request->fpx_checkSum, $data);

            if ($request->process == 'fpxd') {
                return;
            }


            if (($response == '00' || $response == '99') && in_array($checksum, ['00', '09'])) {

                if (!in_array($payment->gateway_response_code, ['00'])) {

                    // Success
                    $success = true;
                    $this->processFpxPayment($request, $purchase, $success);
                    // checksum failed, status pending
                    if ($checksum == '09') {
                        $purchase->purchase_status = 4000;
                        $purchase->save();
                    }
                    $paymentGatewayResponse->updateGroupSales($user, $purchase);
                    $check_registration = true;
                    $purchase->increment('invoice_version');
                    $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration, false, true, $checksum);
                    GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);
                    $purchaseNumberFormatted = $purchase->getFormattedNumber();
                    $cookie = json_decode(Cookie::get('bjs_cart'));
                    $gateway = $request->fpx_msgType;
                    $transaction_id = 'Fpx ' .  $request->fpx_fpxTxnId . ' - ' .  $request->fpx_debitAuthNo;
                    $paymentGatewayResponse->sendToMarketingServer($user->userInfo->account_id, $purchase->getFormattedNumber(),  $purchase, $purchase->purchase_amount / 100, $gateway, $transaction_id);
                }

                return $paymentGatewayResponse->markOrderComplete($purchase, $user, $cookie);
            } else {

                $success = false;
                $failed_message = $this->processFpxPayment($request, $purchase, $success);
                $errorMessage = array();
                $errorMessage['failed_message'] = $failed_message;
                $errorMessage['payments'] = $request;
                $errorMessage['purchases'] = $purchase;
                $errorMessage['banks'] = BankName::where('bank_code', $request->fpx_buyerBankId)->first();
                $errorMessage['responses'] = Response::where('response_code', $request->fpx_debitAuthCode)->where('type', 'fpx')->first();
                return view('shop.payment.failed')->with('errorMessage', $errorMessage);
            }
        } else {
            $errorMessage['description'] = "Payment Not Found.";
            $errorMessage['purchases'] = null;
            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }
    }

    public function processFpxPayment($request, $purchase, $success)
    {

        $payment = Payment::where('purchase_number', $request->fpx_sellerOrderNo)->first();

        $payment->gateway_string_result = $request->fpx_sellerExOrderNo;
        $payment->gateway_response_code = $request->fpx_debitAuthCode;
        $payment->auth_code = $request->fpx_debitAuthNo;
        $payment->gateway_eci = $request->fpx_msgType;
        $payment->gateway_security_key_res = $request->fpx_fpxTxnId;
        $payment->gateway_hash = $request->fpx_checkSum;
        $payment->fpx_field = $request->toArray();
        if ($success) $payment->save();

        $response = array();
        $response['code'] = Response::where('response_code', $payment->gateway_response_code)->where('type', 'fpx')->first();
        $response['request'] = $payment;

        $payment->response_message = ($response) ? $payment->response_code . ' - ' . $response['code']->description : $payment->response_code . ' - Not Specified.';

        if ($success && $request->fpx_debitAuthCode == '00') {
            if (in_array($purchase->payment_type, ['7003', '7004'])) {
                $status = 4007;
            } else {
                $status = 4002;
            }
        } else if ($success && $request->fpx_debitAuthCode == '99') {
            $status = 4001;
        } else {
            $status = 4006;
        }

        $purchase->purchase_status = $status;
        $purchase->save();


        return $response;
    }

    public function secondRequest($purchase_number)
    {
        $payment = Payment::where('purchase_number', $purchase_number)->first();
        $purchase = Purchase::where('purchase_number', $purchase_number)->first();

        $fpx_field = json_decode($payment->fpx_field);
        // dd($fpx_field->fpx_buyerBankId);
        $fpx_msgType = "AE";
        $fpx_msgToken = $fpx_field->fpx_msgToken;
        $fpx_sellerExId = $this->fpxID('xchange');
        $fpx_sellerExOrderNo = $fpx_field->fpx_sellerExOrderNo;
        $fpx_sellerTxnTime = $fpx_field->fpx_sellerTxnTime;
        $fpx_sellerOrderNo = $fpx_field->fpx_sellerOrderNo;
        $fpx_sellerId = $this->fpxID('seller');
        $fpx_sellerBankCode = "01";
        $fpx_txnCurrency = "MYR";
        $fpx_txnAmount = $fpx_field->fpx_txnAmount;
        $fpx_buyerEmail = $purchase->user->email;
        $fpx_checkSum = "";
        $fpx_buyerName = "";
        $fpx_buyerBankId = $fpx_field->fpx_buyerBankId;
        $fpx_buyerBankBranch = "";
        $fpx_buyerAccNo = "";
        $fpx_buyerId = "";
        $fpx_makerName = "";
        $fpx_buyerIban = "";
        $fpx_productDesc = "SampleProduct";
        $fpx_version = "7.0";

        /* Generating signing String */
        $data = $fpx_buyerAccNo . "|" . $fpx_buyerBankBranch . "|" . $fpx_buyerBankId . "|" . $fpx_buyerEmail . "|" . $fpx_buyerIban . "|" . $fpx_buyerId . "|" . $fpx_buyerName . "|" . $fpx_makerName . "|" . $fpx_msgToken . "|" . $fpx_msgType . "|" . $fpx_productDesc . "|" . $fpx_sellerBankCode . "|" . $fpx_sellerExId . "|" . $fpx_sellerExOrderNo . "|" . $fpx_sellerId . "|" . $fpx_sellerOrderNo . "|" . $fpx_sellerTxnTime . "|" . $fpx_txnAmount . "|" . $fpx_txnCurrency . "|" . $fpx_version;


        /* Reading key */
        $pkeyid = openssl_get_privatekey($this->privKey());
        openssl_sign($data, $binary_signature, $pkeyid, OPENSSL_ALGO_SHA1);
        $fpx_checkSum = strtoupper(bin2hex($binary_signature));



        //extract data from the post

        extract($_POST);
        $fields_string = "";

        //set POST variables
        if (App::environment('prod')) {
            $url = 'https://mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp';
        } else {
            $url = 'https://uat.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp';
        }

        $fields = array(
            'fpx_msgType' => urlencode($fpx_msgType),
            'fpx_msgToken' => urlencode($fpx_msgToken),
            'fpx_sellerExId' => urlencode($fpx_sellerExId),
            'fpx_sellerExOrderNo' => urlencode($fpx_sellerExOrderNo),
            'fpx_sellerTxnTime' => urlencode($fpx_sellerTxnTime),
            'fpx_sellerOrderNo' => urlencode($fpx_sellerOrderNo),
            'fpx_sellerId' => urlencode($fpx_sellerId),
            'fpx_sellerBankCode' => urlencode($fpx_sellerBankCode),
            'fpx_txnCurrency' => urlencode($fpx_txnCurrency),
            'fpx_txnAmount' => urlencode($fpx_txnAmount),
            'fpx_buyerEmail' => urlencode($fpx_buyerEmail),
            'fpx_checkSum' => urlencode($fpx_checkSum),
            'fpx_buyerName' => urlencode($fpx_buyerName),
            'fpx_buyerBankId' => urlencode($fpx_buyerBankId),
            'fpx_buyerBankBranch' => urlencode($fpx_buyerBankBranch),
            'fpx_buyerAccNo' => urlencode($fpx_buyerAccNo),
            'fpx_buyerId' => urlencode($fpx_buyerId),
            'fpx_makerName' => urlencode($fpx_makerName),
            'fpx_buyerIban' => urlencode($fpx_buyerIban),
            'fpx_productDesc' => urlencode($fpx_productDesc),
            'fpx_version' => urlencode($fpx_version)
        );

        Log::channel('fpx')->info($fields);
        $response_value = array();


        try {
            //url-ify the data for the POST
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }
            rtrim($fields_string, '&');

            //open connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //execute post
            $result = curl_exec($ch);
            //echo "RESULT";
            //echo $result;


            //close connection
            curl_close($ch);

            $token = strtok($result, "&");
            while ($token !== false) {
                list($key1, $value1) = explode("=", $token);
                $value1 = urldecode($value1);
                $response_value[$key1] = $value1;
                $token = strtok("&");
            }

            $fpx_debitAuthCode = reset($response_value);
            //Response Checksum Calculation String
            $data = $response_value['fpx_buyerBankBranch'] . "|" . $response_value['fpx_buyerBankId'] . "|" . $response_value['fpx_buyerIban'] . "|" . $response_value['fpx_buyerId'] . "|" . $response_value['fpx_buyerName'] . "|" . $response_value['fpx_creditAuthCode'] . "|" . $response_value['fpx_creditAuthNo'] . "|" . $fpx_debitAuthCode . "|" . $response_value['fpx_debitAuthNo'] . "|" . $response_value['fpx_fpxTxnId'] . "|" . $response_value['fpx_fpxTxnTime'] . "|" . $response_value['fpx_makerName'] . "|" . $response_value['fpx_msgToken'] . "|" . $response_value['fpx_msgType'] . "|" . $response_value['fpx_sellerExId'] . "|" . $response_value['fpx_sellerExOrderNo'] . "|" . $response_value['fpx_sellerId'] . "|" . $response_value['fpx_sellerOrderNo'] . "|" . $response_value['fpx_sellerTxnTime'] . "|" . $response_value['fpx_txnAmount'] . "|" . $response_value['fpx_txnCurrency'];

            $val = $this->verifySign_fpx($response_value['fpx_checkSum'], $data);
            Log::channel('fpx')->info($response_value);

            // dd($val);
            // val == 00 verification success

        } catch (Exception $e) {
            echo 'Error :', ($e->getMessage());
        }

        if ($val == '00') {
            $payment->gateway_response_code = $response_value['fpx_debitAuthCode'];
            $payment->save();
            if ($response_value['fpx_debitAuthCode'] == '00') {
                $purchase->purchase_status = 4002;
                $purchase->save();
            } else {
                $purchase->purchase_status = 4006;
                $purchase->save();
            }
            return back()->with(['successful_message' => 'FPX status updated']);
        } else {
            return back()->with(['error_message' => 'FPX status not updated']);
        }
    }


    function verifySign_fpx($sign, $toSign)
    {
        error_reporting(0);

        return $this->validateCertificate(storage_path('cert/'), $sign, $toSign);
    }

    function validateCertificate($path, $sign, $toSign)
    {
        global  $ErrorCode;

        $d_ate = date("Y");
        //validating Last Three Certificates

        $fpxcert = array($path . $this->fpxID('cert'), $path . $this->fpxID('cert') . ".cer");

        $certs = $this->checkCertExpiry($fpxcert);
        $signdata = $this->hextobin($sign);


        if (count($certs) == 1) {

            $pkeyid = openssl_pkey_get_public($certs[0]);
            $ret = openssl_verify($toSign, $signdata, $pkeyid);
            if ($ret != 1) {
                $ErrorCode = " Your Data cannot be verified against the Signature. " . " ErrorCode :[09]";
                return "09";
            }
        } elseif (count($certs) == 2) {

            $pkeyid = openssl_pkey_get_public($certs[0]);
            $ret = openssl_verify($toSign, $signdata, $pkeyid);
            if ($ret != 1) {

                $pkeyid = openssl_pkey_get_public($certs[1]);
                $ret = openssl_verify($toSign, $signdata, $pkeyid);
                if ($ret != 1) {
                    $ErrorCode = " Your Data cannot be verified against the Signature. " . " ErrorCode :[09]";
                    return "09";
                }
            }
        }
        if ($ret == 1) {

            $ErrorCode = " Your signature has been verified successfully. " . " ErrorCode :[00]";
            return "00";
        }


        return $ErrorCode;
    }


    function checkCertExpiry($path)
    {
        global  $ErrorCode;

        $stack = array();
        $t_ime = time();
        $curr_date = date("Ymd", $t_ime);
        for ($x = 0; $x < 2; $x++) {
            error_reporting(0);
            $key_id = file_get_contents($path[$x]);
            if ($key_id == null) {
                $cert_exists++;
                continue;
            }
            $certinfo = openssl_x509_parse($key_id);
            $s = $certinfo['validTo_time_t'];
            $crtexpirydate = date("Ymd", $s - 86400);
            if ($crtexpirydate > $curr_date) {
                if ($x > 0) {
                    if ($this->certRollOver($path[$x], $path[$x - 1]) == "true") {
                        array_push($stack, $key_id);
                        return $stack;
                    }
                }
                array_push($stack, $key_id);
                return $stack;
            } elseif ($crtexpirydate == $curr_date) {
                if ($x > 0 && (file_exists($path[$x - 1]) != 1)) {
                    if ($this->certRollOver($path[$x], $path[$x - 1]) == "true") {
                        array_push($stack, $key_id);
                        return $stack;
                    }
                } else if (file_exists($path[$x + 1]) != 1) {

                    array_push($stack, file_get_contents($path[$x]), $key_id);
                    return $stack;
                }


                array_push($stack, file_get_contents($path[$x + 1]), $key_id);

                return $stack;
            }
        }
        if ($cert_exists == 2)
            $ErrorCode = "Invalid Certificates.  " . " ErrorCode : [06]";  //No Certificate (or) All Certificate are Expired
        else if ($stack . Count == 0 && $cert_exists == 1)
            $ErrorCode = "One Certificate Found and Expired " . "ErrorCode : [07]";
        else if ($stack . Count == 0 && $cert_exists == 0)
            $ErrorCode = "Both Certificates Expired " . "ErrorCode : [08]";
        return $stack;
    }

    function hextobin($hexstr)
    {
        $n = strlen($hexstr);
        $sbin = "";
        $i = 0;
        while ($i < $n) {
            $a = substr($hexstr, $i, 2);
            $c = pack("H*", $a);
            if ($i == 0) {
                $sbin = $c;
            } else {
                $sbin .= $c;
            }
            $i += 2;
        }
        return $sbin;
    }

    function certRollOver($old_crt, $new_crt)
    {

        if (file_exists($new_crt) == 1) {

            rename($new_crt, $new_crt . "_" . date("YmdHis", time())); //FPXOLD.cer to FPX_CURRENT.cer_<CURRENT TIMESTAMP>

        }
        if ((file_exists($new_crt) != 1) && (file_exists($old_crt) == 1)) {
            rename($old_crt, $new_crt);                                 //FPX.cer to FPX_CURRENT.cer
        }


        return "true";
    }
}
