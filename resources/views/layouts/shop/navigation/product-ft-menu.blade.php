<div class="ft-menu-mb ft-tab">
    <div class="close-cart">
        <i class="fa fa-times" aria-hidden="true"></i>
    </div>
    @include('layouts.shop.footer-menu.product-ft-price')
    
    @include('layouts.shop.footer-menu.product-ft-cartmb')
</div>

<div class="user-edit ft-tab">
  <div class="close-cart">
    <i class="fa fa-times" aria-hidden="true"></i>
  </div>
  @include('layouts.shop.footer-menu.user-ft-menu')
</div>

@if (Auth::check())
<div class="footer-menu">

  <ul class="ft-cart-nav">
      <li class="item01 col ">
        <button class="btn openBtn">
          <i class="fa fa-user-o" aria-hidden="true"></i>
        </button>
      </li>
     
      {{-- @php
      dd($products->show_warning,$products->display_only);
      @endphp --}}
      @if($products->show_warning == 1 && $products->display_only == 0)
      <li class="item04 col">
        <div class="clickme-info mobile">
          <a data-bs-toggle="modal" href="#waringInfo" id="warning-modal" data-button="Back" role="button">
              <img src="{{asset('/images/icons/clickme-icon.png')}}" alt="">
          </a>
        </div>
      </li>
      <li class="item02 col warning">
        <button class="btn openBtn">
          Add To Cart
        </button>
      </li>
      <li class="item03 col warning">
        <a href="#">
          Buy Now
        </a>
      </li>
      @elseif($products->show_warning == 0 && $products->display_only == 1)
      <li class="item02 col display">
        <button class="btn openBtn">
          Add To Cart
        </button>
      </li>
      <li class="item03 col display">
        <a href="#">
          Buy Now
        </a>
      </li>
      @elseif($products->display_only == 1 && $products->show_warning == 1)
      <li class="item04 col">
        <div class="clickme-info mobile">
          <a data-bs-toggle="modal" href="#waringInfo" id="warning-modal" data-button="Back" role="button">
              <img src="{{asset('/images/icons/clickme-icon.png')}}" alt="">
          </a>
        </div>
      </li>
      <li class="item02 col warning display" >
        <button class="btn openBtn">
          Add To Cart
        </button>
      </li>
      <li class="item03 col warning display">
        <a href="#">
          Buy Now
        </a>
      </li>
      @else
      <li class="item02 col">
        <button class="btn openBtn">
          Add To Cart
        </button>
      </li>
      <li class="item03 col">
        <a href="#">
          Buy Now
        </a>
      </li>
      @endif
  </ul>

</div>
@else
<div class="view-product-without-login-box-mb">
    <div class="text-messages">
        Register as member to purchase the product 
    </div>
    <div class="login-button" onclick="window.location='/';" >
        Login/Register
    </div>
</div>
@endif
