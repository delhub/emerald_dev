<?php

namespace App\Http\Controllers\Administrator\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Purchases\Purchase;

class SqlController extends Controller
{

    public function index(Request $request)
    {
        return view('administrator.account.sql-index');
    }
    public function invoiceExport(Request $request)
    {
        $request->validate([
            'i_date_start' => ['required'],
            'i_date_end' => ['required'],
        ], [
            'i_date_start.required' => 'Field is required.',
            'i_date_end.required' => 'Field is required.',
        ]);
        $date_start = $request->i_date_start;
        $date_end = $request->i_date_end;
        $contents = array();

        $purchases = Purchase::whereIn('purchase_status', [4002, 4007])
            ->whereIn('purchase_date', self::displayDates($date_start, $date_end))
            ->with('items.product.parentProduct.accountingCat')
            ->with('user.userInfo.userCountry')
            ->with('fees')
            ->whereHas('successfulPayment')
            ->with('successfulPayment')
            ->get();


        $TERMS = "CASH";
        $CANCELLED = 'F';
        $CURRENCYRATE = '1.000000000';
        $PROJECT = '----';
        $TAX = '';
        $TAXINCLUSIVE = $TAXAMT = '0';

        foreach ($purchases->chunk(100) as $chunk) {
            foreach ($chunk as $purchase) {
                $details = array();
                $subtotal = $rebate = 0;
                $payment = $purchase->successfulPayment;
                $DOCNO = $DOCNOEX = $purchase->inv_number;
                $POSTDATE = $DOCDATE = $TAXDATE = $purchase->purchase_date;
                $customer = $purchase->user->userInfo;
                $customer_name = strtoupper($customer->full_name);
                $DESCRIPTION = "{$DOCNOEX} - {$customer_name}";
                $AREA = $purchase->ship_city_key;
                $AGENT = ($purchase->dealer_id) ? ($purchase->dealer_id) : ($customer->userCountry->referrer_id);

                switch ($purchase->user->userInfo->account_id) {
                    case 3913000009:
                    case 3913000500: // kk lee
                        $CODE = "300-T0008";
                        break;
                    case 3913000153:  // monalisa
                        $CODE = "300-T0007";
                        break;
                    case 3913000509:  // preon resources
                        $CODE = "300-T0006";
                        break;
                    default:
                        $CODE = "300-T0001";

                };

                foreach ($purchase->items as $item) {
                    if ($item->bundle_id != 0) continue;
                    $product_name = strtoupper(trim(preg_replace("/[^A-Za-z0-9(),. ]/", '', $item->product->parentProduct->name), ' '));
                    $category = isset($item->product->parentProduct->accountingCat[0]) ? $item->product->parentProduct->accountingCat[0] : (object) array('name' => 'Products', 'slug' => '500-000');
                    $ACCOUNT = $category->slug;
                    if ($ACCOUNT == '500-100') $CODE = "300-T0002";
                    $item_quantity = $item->quantity + $purchase->items->where('bundle_id', $item->id)->sum('quantity');
                    $IDESCRIPTION = $customer_name . ' - ' . $item->product_code . ' - ' . strtoupper($product_name) . ' x' . $item_quantity;
                    $IAMOUNT = self::decimal2($item->subtotal_price);
                    $subtotal += $item->subtotal_price;
                    $details[] = "DETAIL;$DOCNO;$ACCOUNT;$IDESCRIPTION;$PROJECT;$TAX;$TAXAMT;$TAXINCLUSIVE;$IAMOUNT;$CODE;";
                }
                foreach ($purchase->fees as $fee) {
                    switch ($fee->type) {
                        case 'rebate_fee':
                            $subtotal -= $fee->amount;
                            $rebate += $fee->amount;
                            $ACCOUNT = '420-100';
                            $IAMOUNT = self::decimal2(-$fee->amount);
                            break;
                        case 'shipping_fee':
                            $subtotal += $fee->amount;
                            $ACCOUNT = '500-001';
                            $IAMOUNT = self::decimal2($fee->amount);
                            break;
                        default:
                            $subtotal += $fee->amount;
                            $ACCOUNT = 'NOT-SET';
                            $IAMOUNT = self::decimal2($fee->amount);
                            break;
                    }
                    if ($purchase->purchase_type == "Voucher" && $fee->type == 'rebate_fee') {
                        $subtotal += $fee->amount;
                        continue;
                    }
                    $IDESCRIPTION = $DOCNO . ' - ' . $customer_name . ' - ' . strtoupper($fee->name);
                    $details[] = "DETAIL;$DOCNO;$ACCOUNT;$IDESCRIPTION;$PROJECT;$TAX;$TAXAMT;$TAXINCLUSIVE;$IAMOUNT;$CODE;";
                }
                $purchase_amount = ($purchase->purchase_type == "Voucher")  ? $rebate : $payment->amount;

                $DOCAMT = self::decimal2($purchase_amount);

                $master = "MASTER;$DOCNO;$DOCNOEX;$DOCDATE;$POSTDATE;$CODE;$TERMS;$DESCRIPTION;$AREA;$AGENT;$PROJECT;$CURRENCYRATE;$DOCAMT;$CANCELLED;$TAXDATE;";

                $surplus = $purchase_amount - $subtotal; // 20 - 19
                // dd( $purchase_amount , $subtotal, $surplus);
                if ($surplus > 0) {
                    $ACCOUNT = '500-002';
                    $IDESCRIPTION =  $customer_name . ' - MISC CHARGES';
                    $IAMOUNT = self::decimal2($surplus);
                    $details[] = "DETAIL;$DOCNO;$ACCOUNT;$IDESCRIPTION;$PROJECT;$TAX;$TAXAMT;$TAXINCLUSIVE;$IAMOUNT;$CODE;";
                }

                $contents[] = $master;
                $contents = array_merge($contents, $details);
            }
        }

        $fdate_start = str_replace('/', '-', $date_start);
        $fdate_end = str_replace('/', '-', $date_end);
        $filename = "formula-sql-invoice-{$fdate_start}-{$fdate_end}.txt";

        return response()->streamDownload(function () use ($contents) {
            foreach ($contents as $content) {
                echo $content  . "\r\n";
            }
        }, $filename);
    }

    public function paymentExport(Request $request)
    {
        $request->validate([
            'p_date_start' => ['required'],
            'p_date_end' => ['required'],
        ], [
            'p_date_start.required' => 'Field is required.',
            'p_date_end.required' => 'Field is required.',
        ]);
        $date_start = $request->p_date_start;
        $date_end = $request->p_date_end;
        $contents = array();

        $COMPANYCODE = "300-T0001";
        $CANCELLED = 'F';
        $CURRENCYRATE = '1.000000000';
        $PROJECT = $PAYMENTPROJECT = '----';
        $UNAPPLIEDAMT = $NONREFUNDABLE = $BANKCHARGE = '0.00';
        $DOCTYPE =  'IV';

        $purchases = Purchase::whereIn('purchase_status', [4002, 4007])
            ->whereIn('purchase_date', self::displayDates($date_start, $date_end))
            ->with('user.userInfo.userCountry')
            ->whereHas('successfulPayment')
            ->with('successfulPayment')
            ->with('fees')
            ->get();

        foreach ($purchases->chunk(100) as $chunk) {
            foreach ($chunk as $purchase) {
                $payment = $purchase->successfulPayment;
                $rebate = 0;
                $DOCNO = $purchase->receipt_number;
                $POSTDATE = $DOCDATE = $purchase->purchase_date;
                $KODOCNO = $purchase->inv_number;

                $customer = $purchase->user->userInfo;
                $customer_name = strtoupper($customer->full_name);
                $AREA = $purchase->ship_city_key;
                $AGENT = ($purchase->dealer_id) ? ($purchase->dealer_id) : ($customer->userCountry->referrer_id);
                $CHEQUENUMBER = $purchase->purchase_type . ' ' . $payment->auth_code;
                $DESCRIPTION = "{$customer_name} PAYMENT FOR INVOICE {$KODOCNO}";
                foreach ($purchase->fees->where('type', 'rebate_fee') as $fee) $rebate += $fee->amount;

                switch ($purchase->purchase_type) {
                    case 'Voucher':
                        $PAYMENTMETHOD = '311-001'; // VOUCHER
                        $purchase_amount = $rebate;
                        break;
                    case 'Metapoint':
                        $PAYMENTMETHOD = '311-002'; // MES POINT
                        $purchase_amount = $payment->amount;
                        break;
                    case 'Card':
                        $PAYMENTMETHOD = '311-000'; // Card
                        $purchase_amount = $payment->amount;
                        break;
                    case 'Fpx':
                        $PAYMENTMETHOD = '311-004'; // Fpx
                        $purchase_amount = $payment->amount;
                    case 'Offline':
                    case 'Duitnow':
                    default:
                        $PAYMENTMETHOD = '310-004'; // PBB
                        $purchase_amount = $payment->amount;
                        break;
                }

                $DOCAMT = self::decimal2($purchase_amount);

                $contents[] = "MASTER;$DOCNO;$COMPANYCODE;$DOCDATE;$POSTDATE;$DESCRIPTION;$AREA;$AGENT;$PAYMENTMETHOD;$CHEQUENUMBER;$PROJECT;$PAYMENTPROJECT;$CURRENCYRATE;$BANKCHARGE;$DOCAMT;$UNAPPLIEDAMT;$CANCELLED;$NONREFUNDABLE;";
                $contents[] = "DETAIL;$DOCNO;$DOCTYPE;$KODOCNO;$DOCAMT;";
            }
        }
        $fdate_start = str_replace('/', '-', $date_start);
        $fdate_end = str_replace('/', '-', $date_end);
        $filename = "formula-sql-payment-{$fdate_start}-{$fdate_end}.txt";

        return response()->streamDownload(function () use ($contents) {
            foreach ($contents as $content) {
                echo $content  . "\r\n";
            }
        }, $filename);
    }
    public static function decimal2($amount)
    {
        return number_format($amount / 100, 2, ".", "");
    }
    public static function displayDates($date1, $date2, $format = 'd/m/Y')
    {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        while ($current <= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }
}
