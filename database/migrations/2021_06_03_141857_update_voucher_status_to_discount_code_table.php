<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Models\Discount\Code;

class UpdateVoucherStatusToDiscountCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_code', function (Blueprint $table) {
            $dbs = Code::get();

            Code::truncate();

            DB::statement("ALTER TABLE discount_code MODIFY COLUMN coupon_status ENUM('valid', 'invalid', 'expired', 'used', 'active', 'inactive')");

            foreach ($dbs as $db) {
                unset($db->created_at);
                unset($db->updated_at);
            }

            Code::insert($dbs->toArray());

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_code', function (Blueprint $table) {
            //
        });
    }
}
