<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateGlobalQuanlitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_qualities')
            ->where('name', 'Standard')
            ->update(
                ['name' => 'Domestic']
            );

        DB::table('global_qualities')
            ->where('name', 'Moderate')
            ->update(
                ['name' => 'Import']
            );

        DB::table('global_qualities')
            ->where('name', 'Premium')
            ->update(
                ['name' => 'Exclusive']
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
