<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bujishu - Invoice {{ $purchase->purchase_number }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@200;400&display=swap" rel="stylesheet">
     @include('layouts.favicon')
    <style>
        * {
            padding: 0;
            margin: 0;
            font-family: 'Lato', sans-serif;
                font-family: 'Lato', sans-serif;
        }
        .text-grey {
            color: #58595b;
        }
        .topBottomBorder {
            padding: 13px 0px;
            border-top: 1.5px solid #ffcc00;
            border-bottom: 1.5px solid #ffcc00;
            font-size: 14px;
            z-index: 1;
        }
        .topGold {
            border-top: 1px solid #ffcc00;
        }
        .totalAmount {
            border-top: 1px solid #ffcc00;
            width: 126px;
            text-align: center;
            padding: 12px 10px;
            background-color: #fef5d6;
            font-weight: 600;
            font-size: 16px;

        }
        .totalTitle {
            border-top: 1px solid #ffcc00;
            width: 113px;
            padding: 12px 10px;
            text-align: right;
            font-weight: 500;
            font-size: 16px;
        }
        .paymentReceived {
            border-top: 1px solid #ffcc00;
            width: 586px;
            padding: 12px 10px;
        }
        td img.my-img1{
            width: 110px!important;
            height: 110px!important;
        }
        #table-item tr{
            widows: 100%;
        }
        tr.table-td td{
            height: 130px;
            padding: 10px 0px;
        }
        .table-td td p{
            /* height: 110px; */
        }
        .table-td td .description-info{
            /* height:110px; */
            width: auto;
            overflow: hidden;
            text-align: left;
        }
        .non-op .description-item{
            /* height: 80px; */
            width: 240px;
            float: left;
            margin-top: 10px;
            margin-bottom: 10px;
            color: #58595b;
        }
        .op .description-item{
            width: 425px;
            float: left;
            padding: 10px 0;
        }
        .non-op .imgSize{
            float: left;
            width: 120px;
            /* height: 80px; */
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .op .imgSize{
            float: left;
            padding: 10px 0;
            width: 120px;
        }
        #table-item .non-op,#table-item .op{
            padding: 10px 0;
        }
        .table-td.img{
            position: relative;
        }
        .table-td td .unitQlt{
        }
        .table-td td .unitQlt p{
            text-align: center;
        }
        .table-td td .unitPrice{
            text-align: center;
        }
        .table-td td .unitToto{
        }

        .sty-span{
            width: 241px;
            text-align: left;
        }
        .sty-span{
            font-size: .8rem;
        }
        .sty-span .span600 {
            font-weight: 600;
            font-size: 1rem;
        }
        .form-text-center{
            text-align: center;
        }

        .whole-page-body{
            margin: auto;
            padding: 0;
            height: 183vh;
            width: 991px;
            padding: 0px 40px;
        }
        .page-content{
            width: 100%;
            float: left;
        }
        .page-body{
            width: 100%;
            position: relative;
            float: left;
        }
        .tot-col{
            position: relative;
            width: 991px;
            margin-bottom: 0px;
            float: left;
            height: 300px;
            display: table;
        }
        .company-info2{
            width: 100%;
            float: left;
        }
        .company-info2 .left-info{
            width: 55%;
            padding-top: 23px;
            float: left;
        }
        .company-info2 .right-info{
            width: 45%;
            float: left;
        }
        .company-info2 .right-info{
            padding: 10px 0px;
            /* float: left; */
        }
        .company-info2 .left-info .purchaser-info p+p,.company-info2 .left-info .deliver-info p+p{
            margin-top: 5px;
        }
        .company-info2 .left-info .purchaser-info p+p+p,.company-info2 .left-info .deliver-info p+p+p{
            margin-bottom:10px;
        }
        .company-info2 .left-info .purchaser-info p,.company-info2 .left-info .deliver-info p{
            margin-top: 5px;
            font-size: .80rem;
            line-height: 1.4;
            padding-left: 13px;
            letter-spacing: .05em;
            font-weight: normal;
        }
        .company-info2 .left-info .purchaser-info p span,.company-info2 .left-info .deliver-info p span{
            padding-left: 0px;
            font-weight: 600;
            font-size: 1.2em;
            letter-spacing: .05em;
        }
        .company-info2 .left-info .purchaser-info{
            width: 45.5%;
            float: left;
        }
        .company-info2 .left-info .deliver-info{
            width: 51.5%;
            /* padding-right: 3%; */
            float: left;
        }
        .company-info2 .left-info .deliver-info span,.company-info2 .left-info .purchaser-info span{
            padding-left: 13px;
        }
        .myhr{
            margin-top: 6px;
            border-top:2px solid #ffcc00;
        }
        .right-info.delivery-tit span{
            font-size: 1.8rem;
            color: #ffcc00;
            font-weight: 700;
        }
        .right-info .body-info{
            background-color: #fef5d6;
            border-radius: 20px;
            padding: 15px 20px;
        }
        .body-info table{
            width: 100%;
        }
        .body-info tr td{
            width: 50%;
            font-size: 14px;
            line-height: 20px;
            letter-spacing: .07em;
        }
        .body-info .invoice-front p{
            font-weight: 700;
            padding-left: 5px;
            font-size: 15px;
        }
        .right-info .body-info span{
            font-size: 1rem;
            color: #1b1b1b;
            float: right;
            line-height: 15px;
        }
        .body-info .invoice-front-nobold p {
            padding-left: 5px;
            line-height: 20px;
        }

        .page-body img.barcode-img{
            height: 150px;
            width: 150px;
            float: right;
            top: 3px;
            right: 25px;
            position: absolute;
        }
        .second-header{
            width: 100%;
            float: left;
        }
        .tot-col .tot-left{
                width: 65%;
                float: left;
            }
            .tot-col .tot-right{
                width: 35%;
                float: left;
            }
            .tot-col .tot-left .tot-tit p{
                font-size: 1.15rem;
                padding: 0px 15px;
                padding-top: 30px;
                padding-bottom: 10px;
                font-weight: 600;
            }
            .tot-notice-box p{
                font-size: .75rem;
                color: #000000;
                margin-top: 6px;
            }
            .tot-col .tot-left .tot-payment-box table{
                width: 395px;
                background: #fef5d6;
                color: #1b1b1b;
                padding: 19px 19px;
                border-radius: 10px;
            }
            .tot-col .tot-left .tot-payment-box table tr{
                width: 100%;
            }
            .tot-col .tot-left .tot-payment-box p{
                font-weight: 600;
                width: 155px;
                float: left;
                font-size: 17px;
            }
            .tot-col .tot-left .tot-payment-box span{
                width:10px;
                text-align: right;
            }
            .tot-col .tot-left .tot-payment-box table td{
                line-height: 32px;
                display: inline-flex;
                text-align: left;
                width: 50%;
            }
            .tot-col .tot-left .tot-payment-box table td+td{
                width: 48%;
                padding-left: 5px;
                font-weight: 600;
                font-size: 17px;
            }
            .tot-col .tot-left .tot-notice-box{
                width: 94%;
                text-align: left;
                color: #1b1b1b;
                padding: 0 15px;
            }
            .tot-col .tot-right table{
                width: 100%;
                border-bottom: 1px solid #ffcc00;
            }
            .tot-col .tot-right table tr td{
                line-height: 49px;
                font-size: 18px;
                text-align: right;
                padding: 0px 10px;
                border-top: 1px solid #ffcc00;
                font-weight: 600;
            }
            .tot-col .tot-right table tr td.righttd{
                width: 137px;
                /* border-top: 1px solid #ffcc00; */
                background: #fef5d6;
            }
            .footer-end{
                width: 100%;
                position: relative;
                height: 39px;
                float: left;
                bottom: 0;
            }
            .grandTot td{
                color:#ffcc00;
            }
            .grandTot td+td{
                color:#1b1b1b;
                background: #ffcc00!important;
            }
            td img.my-img {
                object-fit: contain!important;
                width: 70px!important;
                float: right;
                margin-right: 10px;
            }
            thead { display:table-header-group; }
            tfoot { display:table-footer-group; }
    </style>

</head>

<body class="whole-page-body">

    <?php

    // Declare batch
    $batchs = array();
    $batch_count = 1;
    $item_count = 0;
    $batchs[$batch_count] = collect();

    // Calculate Subtotal
    $subtotalPrice = 0;
    $deliveryFee = 0;
    $installationFee = 0;
    $rebateFee = 0;
    $balanceDue = 0;
    $gst = 0;
    $grandTotal = 0;

    $key = 0;

    $deliveryFee = ($purchase->getFeeAmount('shipping_fee') != NULL) ? $purchase->getFeeAmount('shipping_fee') : NULL;
    $installationFee = ($purchase->getFeeAmount('installation_fee') != NULL) ? $purchase->getFeeAmount('installation_fee') : NULL;
    $rebateFee = ($purchase->getFeeAmount('rebate_fee') != NULL ) ? $purchase->getFeeAmount('rebate_fee') : NULL;

    foreach ($purchase->sortItems() as $item){

        $subtotalPrice = $subtotalPrice + $item->subtotal_price;
        if($purchase->getFeeAmount('shipping_fee') == NULL) $deliveryFee += $item->delivery_fee;
        if($purchase->getFeeAmount('installation_fee') == NULL) $installationFee += $item->installation_fee;
        if($purchase->getFeeAmount('rebate_fee') == NULL) $rebateFee += $item->rebate;

        $batchs[$batch_count]->add($item);
        $item_count++;

        if ($item_count >= 9 * $batch_count ){
            $batch_count++;
            $batchs[$batch_count] = collect();
        };
    }

    $gst = ($subtotalPrice + $deliveryFee + $installationFee - $rebateFee) * 0.07 ;

    if($purchase->country_id != 'SG'){
        $grandTotal = ($subtotalPrice   - $rebateFee) + ($deliveryFee + $installationFee);
    } else {
        $grandTotal = ($subtotalPrice + $deliveryFee + $installationFee - $rebateFee) + $gst;
    }

    $successPayment = isset($purchase->successfulPayment) ? $purchase->successfulPayment->amount : 0;

    $balanceDue = $grandTotal - $successPayment;

     ?>
    @foreach( $batchs as $batch_id => $batch)
         <!-- First Page -->
        <div class="page-body">

            <!-- End Second Header -->
            <!-- Content -->
            <div id="page-content" class="page-content" >
                <table id="table-item" cellspacing="0" cellpadding="0" style="width:100%;">
                    @foreach ($batch as $item)
                        <tr class=" @if ($item->bundle_id == 0) op @else non-op @endif ">
                            <td @if ($item->bundle_id == 0) class="topGold form-text-center" @endif class="form-text-center" style="width: 50px;">
                                <p>
                                    @php
                                    if (($item->bundle_id == 0)) {
                                        $batchs[$key] = $item;
                                        $key++;
                                        echo $key;
                                    }
                                    @endphp
                                </p>
                            </td>
                            <td @if ($item->bundle_id == 0) class="topGold table-td img" @endif class="table-td img" style="width: 500px;">

                                <div class="imgSize">
                                    @if (isset($item->product_information['product_color_img']) && $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first() != NULL)
                                        @php
                                            $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first()
                                        @endphp

                                        <img class=" mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif " style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};display: inline-block; margin-right: 12px;" src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                    @elseif(isset($item->product_information['product_color_code']) && $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first() !=NULL)

                                        @php
                                            $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first()
                                        @endphp

                                        <img class="mw-100 rounded d-inline" style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};width: 80px; display: inline-block; margin-right: 12px;">

                                    @elseif($item->product->parentProduct->images->isnotEmpty())

                                    <img class="mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif "
                                    src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}" style="display: inline-block;float:right;background-size:contain;object-fit: contain;"
                                    alt="">
                                    @else

                                    <img class="mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif "
                                        src="{{ asset('assets/images/errors/image-not-found.png') }}" style="display: inline-block;float:right;background-size:contain;object-fit: contain;"
                                        alt="">

                                    @endif
                                </div>
                                <!-- Product Name -->
                                <div class="description-item">
                                    <span style="font-weight: 600;color:#000000">{{ $item->product->parentProduct->name }}</span>
                                    @if ($item->bundle_id == 0)
                                    <br>
                                    <span style="font-size:14px;">Product ID :{{ $item->product_code }}</span>
                                    @endif
                                    @if($item->product_id != 175 || $item->product_id != 176)
                                        @if(array_key_exists('product_color_name', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Color: {{ $item->product_information['product_color_name'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_size', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Size: {{ $item->product_information['product_size'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_curtain_size', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Curtain Model: {{ $item->product_information['product_curtain_size'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_temperature', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Color Temperature:
                                            <br />{{ $item->product_information['product_temperature'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_miscellaneous', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            {{ $item->product_information['product_miscellaneous'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('invoice_number', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Invoice Number: {{ $item->product_information['invoice_number'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_preorder_date', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Pre Order
                                            Delivery:<br />{{ $item->product_information['product_preorder_date'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_order_remark', $item->product_information))
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Remarks:<br />{{ $item->product_information['product_order_remark'] }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_order_selfcollect', $item->product_information) &&
                                        $item->product_information['product_order_selfcollect'])
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Self Collection: Yes
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_order_tradein', $item->product_information) &&
                                        $item->product_information['product_order_tradein'])
                                        <br>
                                        <span style="font-size: .8rem;">
                                            Rebate: - RM
                                            {{ number_format($item->product_information['product_order_tradein'] / 100),2 }}
                                        </span>
                                        @endif
                                        @if(array_key_exists('product_installation', $item->product_information) &&
                                        $item->product_information['product_installation'])
                                        <br>
                                        <span style="font-size: .8rem;">
                                            {{($item->installationCategories()!=NULL) ? $item->installationCategories()->cat_name : ''}}
                                        </span>
                                        @endif
                                    @endif
                                </div>

                            </td>
                            <td @if ($item->bundle_id == 0) class="topGold form-text-center table-td" @endif class="form-text-center table-td" style="width: 50px;">
                                <div class="unitQlt">
                                    <p>{{ $item->quantity }}</p>
                                </div>
                            </td>
                            <td @if ($item->bundle_id == 0) class="topGold form-text-center table-td" @endif class="form-text-center table-td" style="width: 157px;">
                                <div class="unitPrice">
                                    <p>
                                        @if ($item->bundle_id == 0) {{ number_format(($item->unit_price / 100), 2) }} @endif
                                    </p>
                                </div>
                            </td>
                            <td @if ($item->bundle_id == 0) class="topGold form-text-center table-td price" @endif class="form-text-center table-td price" style="background-color: #fef5d6;text-align:right;width: 129px;padding-right:15px;">
                                <div class="unitToto">
                                    @if ($item->bundle_id == 0) {{ number_format(($item->subtotal_price / 100), 2) }} @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </table>

                {{-- <p>Alst here</p> --}}
            </div>
            <!-- End Content -->

        </div>
        <!-- End First Page -->
    @endforeach

    <!-- Total Footer -->
    <div class="tot-col">
        <div class="tot-left topGold">
            <div class="tot-tit">
                <p class="text-grey">PAYMENT RECEIVED</p>
            </div>
            <div class="tot-payment-box">

                <table class="table table-sm">
                    <tbody>
                    <tr>
                        <td>
                            <p>Payment Method</p>
                            <span>:</span>
                        </td>
                        <td>{{ $purchase->purchase_type }}</td>
                    </tr>
                    <tr>
                        <td>
                            <p>Reference No</p>
                            <span>:</span>
                        </td>
                        <td>{{ $purchase->successfulPayment->auth_code }}</td>
                    </tr>
                    <tr>
                        <td>
                            <p>Amount Paid</p>
                            <span>:</span>
                        </td>
                        <td> {{ number_format(($purchase->successfulPayment->amount / 100), 2) }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tot-notice-box">
                <p @if($purchase->country_id != 'SG') style="font-size: .75rem;color: #000000;  margin-top: 11%;" @else  style="font-size: .75rem;color: #000000;  margin-top: 3%; " @endif >
                    The issuance of this Official Receipt shall be demeed as your
                    acceptance of the terms and conditions of this Disclaimer. The Bujishu Agent Agreement shall only become effective after full payment is received by the Company.
                </p>
            </div>
        </div>
        <div class="tot-right">
            <table class="table" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>Sub Total</td>
                    <td class="righttd">{{ number_format(($subtotalPrice / 100), 2)}}</td>
                </tr>

                <tr class="">
                    <td>Delivery Fee</td>
                    <td class="righttd">{{ number_format(($deliveryFee / 100), 2)}}</td>
                </tr>
                <tr class="">
                    <td>Installation Fee</td>
                    <td class="righttd">{{ number_format(($installationFee / 100), 2)}}</td>
                </tr>
                @if($purchase->country_id == 'SG')
                <tr class="">
                    <td>GST @ 7%</td>
                    <td class="righttd">{{ number_format(($gst / 100 ), 2)}}</td>
                </tr>
                @endif
                @if($rebateFee != 0)
                <tr>
                    <td class="totalTitle">
                        {{$purchase->fees->where('type','rebate_fee')->first()->name}}
                    </td>
                    <td class="totalAmount"> - {{ number_format(($rebateFee / 100), 2)}}
                    </td>
                </tr>
                @endif
                <tr class="grandTot">
                    <td>Grand Total</td>
                    <td>
                        @if ($grandTotal <= '0')
                            {{ number_format(($grandTotal / 100), 2)}}
                        @else
                            0.00
                        @endif
                    </td>
                </tr>
                <tr class="">
                    {{-- @php
                        dd($purchase->successfulPayment);
                    @endphp --}}
                    <td>Amount Paid</td>
                    <td class="righttd">{{ number_format(($purchase->successfulPayment->amount / 100), 2) }}</td>
                </tr>
                <tr style="border-bottom:1px solid #ffcc00">
                    <td>Balance Due</td>
                    <td class="righttd">{{ number_format(($balanceDue / 100), 2) }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- End Total Footer -->
</body>
</html>

