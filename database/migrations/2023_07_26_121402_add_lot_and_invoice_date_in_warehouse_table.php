<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddLotAndInvoiceDateInWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wh_batch', function (Blueprint $table) {
            $table->date('inv_date')->after('inv_number')->nullable();
        });

        Schema::table('wh_batch_detail', function (Blueprint $table) {
            $table->string('lot')->after('cost_price')->nullable();
        });

        $wh_batchs = DB::table('wh_batch')->where('lot', '!=', null)->get();

        foreach ($wh_batchs as $wh_batch) {
            DB::table('wh_batch_detail')->where('batch_id', $wh_batch->batch_id)->update([
                'lot' => $wh_batch->lot,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wh_batch', function (Blueprint $table) {
            $table->dropColumn('inv_date');
        });
        Schema::table('wh_batch_detail', function (Blueprint $table) {
            $table->dropColumn('lot');
        });
    }
}
