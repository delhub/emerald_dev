@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp

<style>
    /* .navbg-protect.active {
        width: 100%;
        height: calc(100% + 1px);
        background: #000000de;
        position: fixed;
        z-index: 9;
        display: block;
    }

    .navbg-protect {
        display: none;
    } */

    .add-mb-150 {
        margin-bottom: 0px;
    }

    /*Add css below here */

    .panel-heading {
        position: relative;
    }

    .panel-heading[data-toggle="collapse"]:after {
        /* font: normal normal normal 14px/1 FontAwesome; */
        /* content: "\e072"; */
        background: #e7e7e7 url(http://bujishu_web.test/storage/icons/faq/sort-down-solid.svg)no-repeat center;
        background-size: 18px;
        position: contain;
        /* color: #b0c5d8; */
        /* font-size: 18px; */
        /* line-height: 22px;  */
        /* width: 100%; */
        /* height: 30px; */
        top: -10px;
        left: 95%;
        opacity: .3;


        /* top: calc(50% - 10px); */
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }

    .panel-heading[data-toggle="collapse"].collapsed:after {
        -webkit-transform: rotate(0);
        -moz-transform: rotate(0);
        -ms-transform: rotate(0);
        -o-transform: rotate(0);
        transform: rotate(0);
    }

    .my.btn.btn-block.btn-toggle.focus-only {
        text-align: left;
        background-color: #f8fafc;
        border: 2px solid #e7e7e7;
        margin: 0px 10px 10px 10px;
        padding: 10px 10px;
    }

    .my.btn.btn-block.btn-toggle.focus-only:focus {
        background-color: #e7e7e7;
        border: solid 3px #ffcc00;
        box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);

    }

    .mg-25 {
        margin-left: 0px;
    }

    .mc-25.mb-1 {
        width: 80%;
    }

    .btn-2 {
        color: black;
        float: left;
        text-align: left;
        width: 100%;
    }

    @media only screen and (max-width: 768px) {
        .add-mb-150 {
            margin-bottom: 150px;
        }

        .btn {
            width: 50px;
            float: left;

        }

        .panel-heading {
            position: relative;
        }

        .panel-heading[data-toggle="collapse"]:after {
            /* font: normal normal normal 14px/1 FontAwesome; */
            /* content: "\e072"; */
            background: #e7e7e7 url(http://bujishu_web.test/storage/icons/faq/sort-down-solid.svg)no-repeat center;
            background-size: 18px;
            position: contain;
            /* color: #b0c5d8; */
            /* font-size: 18px; */
            /* line-height: 22px;  */
            width: 20%;
            /* height: 30px; */
            top: -10px;
            left: 80%;
            opacity: .3;


            /* top: calc(50% - 10px); */
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .panel-heading[data-toggle="collapse"].collapsed:after {
            -webkit-transform: rotate(0);
            -moz-transform: rotate(0);
            -ms-transform: rotate(0);
            -o-transform: rotate(0);
            transform: rotate(0);
        }

        .mg-25 {
            margin-left: 0px;
            padding: 3px;
        }

        .mc-25.mb-1 {
            width: 80%;
        }

        .btn-2 {
            color: black;
            float: left;
            text-align: left;
            width: 100%;
        }

        .my.btn.btn-block.btn-toggle.focus-only {
            text-align: left;
            background-color: #f8fafc;
            border: 2px solid #e7e7e7;
            height: 35px;

            margin: 5px;
            padding: 5px 5px;

            font-size: 12px;
            width: fit-content;

            /* white-space: normal;
        word-wrap: break-word;   */
        }

        .my.btn.btn-block.btn-toggle.focus-only:focus {
            background-color: #e7e7e7;
            border: solid 3px #ffcc00;
            box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);

            /* margin: 10px;
        padding: 10px 5px; */

            /* height: 42px; */

        }

        .pa-1 {
            text-align: left;
        }
    }
</style>

<nav id="sidebar">
    <div id="dismiss">
        <i class="fas fa-arrow-left"></i>
    </div>

    <div class="sidebar-header">
        <img class="sidebar-logo" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
    </div>

    <ul class="list-unstyled components add-mb-150">
        <h4 style="color: #212529; padding: 5px 10px;"><strong>Shop By Category</strong></h4>
        <li>
            @dealershipcheck

                @if(country()->country_id == 'MY'){
                    <a href="/shop/category/registrations"><strong>Agent / Panel Registrations</strong></a>
                @endif

                  @if(country()->country_id == 'SG'){
                    <a href="/shop/category/agent-sg-launch"><strong>Agent / Panel Registrations</strong></a>
                @endif
            @enddealershipcheck
            @php
            use App\Models\Sidemenus\Sidemenu;
            use App\Models\Globals\Countries;

            $banner_county =  country()->country_id;

            $menus = Sidemenu::orderBy('menu_arrangement','ASC')->get();
            $allMenus = Sidemenu::pluck('menu_title','id')->all();

            @endphp

            <!-- Dynamic menu top start -->
            @foreach($menus as $menu)
                @if($menu->menu_group == "MM" && $menu->menu_show == 1 && $menu->menu_parent == "0" && $menu->menu_arrangement < 19  && $menu->country_id == $banner_county)
                <a href="{{$menu->menu_link}}">
                    <strong>{{ $menu->menu_title }}</strong>
                </a>
                <ul class="list-unstyled components" style="padding: 0px 0;">
                    @if(count($menu->childs))
                    @include('layouts.shop.navigation.menusub',['childs' => $menu->childs])
                    @endif
                </ul>
                @elseif($menu->menu_group == "CG" && $menu->menu_show == 1 && $menu->menu_parent == "0")
                <a href="{{$menu->menu_link}}">
                    {{ $menu->menu_title }}
                </a>
                <ul class="list-unstyled components" style="padding: 0px 0;">
                    @if(count($menu->childs))
                    @include('layouts.shop.navigation.menusub',['childs' => $menu->childs])
                    @endif
                </ul>
                @endif
            @endforeach
            <!-- Dynamic menu top end -->



            <!-- test start -->
            {{-- @foreach($menusides as $menuside)
                    @if($menuside->menu_group == "MM" &&  $menuside->menu_show == 1 )

                        <a href="{{$menuside->menu_link}}" style="color:
            black;">{{$menuside->menu_arrangement}}<strong>{{$menuside->menu_title}}</strong></a>
            @endif
            @endforeach --}}
            <!-- test end -->

            {{-- <a href="/shop/category/new-launching"><strong>New Launching</strong></a> --}}
            {{-- <a href="/shop/product/temp/renovation">Renovation</a> --}}
            {{-- <a href="/shop/product/miscellaneous?panel=1918000101">Miscellaneous</a> --}}

            {{-- @foreach($categories as $category)
            @if($category->name != "Registrations" && $category->name != "New Launching" && $category->name != "New
            Launching0000")
            <a href="/shop/category/{{ $category->slug }}">{{ $category->name }}</a>
            @endif
            @endforeach --}}

            <!-- <a href="/category/bedsheet-mattress">Bedsheet & Mattress</a>
            <a href="/category/bedsheet-mattress">Cupboard</a>
            <a href="/category/bedsheet-mattress">Tables & Chairs</a>
            <a href="/category/bedsheet-mattress">Carpet</a>
            <a href="/category/curtain">Curtain</a>
            <a href="/category/bedsheet-mattress">Tiles</a>
            <a href="/category/bedsheet-mattress">Lighting</a>
            <a href="/category/bedsheet-mattress">Wall Papers</a>
            <a href="/category/bedsheet-mattress">Roof</a>
            <a href="/category/bedsheet-mattress">Doors</a>
            <a href="/category/bedsheet-mattress">Window</a>
            <a href="/category/bedsheet-mattress">Auxillary Propesrity Items</a>
            <a href="/category/product-services">Product & Services</a> -->

            <!-- Dynamic menu bottom start -->
            @foreach($menus as $menu)
            @if($menu->menu_group == "MM" && $menu->menu_show == 1 && $menu->menu_parent == "0" && $menu->menu_arrangement > 19 && $menu->country_id == $banner_county && $menu->country_id == $banner_county)
            <a href="{{$menu->menu_link}}">
                <strong>{{ $menu->menu_title }}</strong>
            </a>
            <ul class="list-unstyled components" style="padding: 0px 0;">
                @if(count($menu->childs))
                @include('layouts.shop.navigation.menusub',['childs' => $menu->childs])
                @endif
            </ul>
            @endif
            @endforeach
            <!-- Dynamic menu bottom end -->
        </li>


        <!-- For future use of navigation bar expand -->
        <!-- <nav id="mysidebarmenu" class="amazonmenu">
            <ul>
                <li><a href="#">Item 1</a></li>
                <li><a href="#">Folder 0</a>
                    <div>
                        <ul>
                            <li><a href="#">Sub Item 0.1</a></li>
                            <li><a href="#">Sub Item 0.2</a></li>
                            <li><a href="#">Sub Item 0.3</a>
                            <li><a href="#">Sub Item 0.4</a>
                            <li><a href="#">Sub Item 0.5</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav> -->
    </ul>
</nav>
<script>
    //Navbar open and close function
    // function mytextbut() {
    //     const getNav = document.querySelector('#mysubpage');
    //     const aac = document.querySelector('#sidebar');
    //     const ccc = document.querySelector('#sidebarCollapse');
    //     const ddd = document.querySelector('#dismiss');
    //     const fff = document.querySelector('.overlay');

    //     getNav.addEventListener('click', function() {
    //         aac.classList.add('active');
    //         fff.classList.add('active');
    //     });

    //     aac.addEventListener('click', function() {
    //         aac.classList.remove('active');
    //         fff.classList.remove('active');
    //     });


    //     ddd.addEventListener('click', function() {
    //         aac.classList.remove('active');
    //         fff.classList.remove('active');
    //     });
    // };

    // mytextbut();

    // Hide Agent Report in menu
    // const agentRp = document.querySelector('#c-103');
    // agentRp.style.display = 'none';
    // console.log(agentRp);

</script>
