<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanelProductsReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panel_products_return', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('name', 255);
            $table->integer('nric');
            $table->integer('contact_number');
            $table->string('customer_address', 255);
            $table->string('delivery_address', 255);
            $table->string('invoice_number');
            $table->string('receiving_date');
            $table->string('product_sku');
            $table->string('product_description');
            $table->string('quantity_ordered');
            $table->string('quantity_returned');
            $table->string('reason');
            $table->string('detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panel_products_return');
    }
}
