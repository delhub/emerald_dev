@extends('layouts.guest.main')
@section('content')

<div class="container-fluid" style="background-color: #fff" >
    <div class="row">
        <div class="col-12 bgIMG">
            <div class="row h-100" style="padding: 10px;">
                <div class="col-md-4 col-12 my-auto middle-box">
                    <center>
                    <h3>Congratulations!</h3>
                    <p>Your Warranty Registration form has been successfully submitted!</p>
                    <button onclick="location.href = '/shop';" class="btn grad2 bjsh-btn-gradient btn-small-screen"> Close </button>
                    </center>
                </div>
            </div>
        </div>


    </div>

</div>



<style>

.bgIMG
{
        background-image: url(/assets/images/background-image/warranty-bg.jpg) ;
        background-repeat: no-repeat;
        background-position: center center;
        /* background-size: contain; */
        min-height: 681px;

}

.warranty-form{
    position: absolute;
    bottom: 22px;
    width: 75%;
    margin: auto;
    right: 40px;
}

.input-form-control{
    width: 300px;
    border: none;
    box-shadow: 0 0 20px 0px lightgrey;
    border-radius: 6px;
    padding: 5px 0px 5px 10px;
}

.input-form-control:focus{
    box-shadow: 0 0 8px 0px #ffcc00ab;
    outline: none !important;

}

.terms-checkbox:hover{
    box-shadow: 0 0 20px 0px #ffcc00;
    border: 0;
    outline: none !important;
}

.middle-box{
    border: 1px solid #ffcc00;
    padding: 20px;
    width: fit-content;
    margin:auto auto;

    background-color: #fff;
}


</style>
@endsection
