<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Traits\Voucher;

class VoucherGen extends Command
{
    use Voucher;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:gen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate voucher for dealer old package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->generateVoucherScript()) {
            echo "Voucher Generate --- OK \n";
        }
        else {
            echo "Error -- Voucher Generate Failed!...\n";
        }
    }
}
