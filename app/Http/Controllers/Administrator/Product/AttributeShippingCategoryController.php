<?php

namespace App\Http\Controllers\Administrator\Product;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductAttribute;
use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Support\Facades\DB;

class AttributeShippingCategoryController extends Controller
{

    public function __construct()
    {
        // 
    }

    public function index(Request $request)
    {
        $productsAttributes = ProductAttribute::orderBy('created_at', 'DESC')
            ->categoryFilter($request->shippingCategory, $request->shippingCategorySpecial)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->findID($request->manyID);

        $ForCountproducts = $productsAttributes->get();

        $productsAttributes = $productsAttributes->paginate(250);
        $productsAttributes->appends(request()->query());


        $shippingCategories = Ship_category::where('cat_type', 'shipping')->get();
        $shippingCategoriesSpecial = Ship_category::where('cat_type', 'shipping_special')->get();

        return view('administrator.product.shippingCategory.index')
            ->with('request', $request)
            ->with('productsAttributes', $productsAttributes)
            ->with('ForCountproducts', $ForCountproducts)
            ->with('shippingCategories', $shippingCategories)
            ->with('shippingCategoriesSpecial', $shippingCategoriesSpecial);
    }

    public function editMarkup($id)
    {
        try {
            $productAttribute = ProductAttribute::find($id);

            $shippingCategory = Ship_category::where('cat_type', 'shipping')->get();

            return view('administrator.product.shippingCategory.edit')
                ->with('productAttribute', $productAttribute)
                ->with('shippingCategory', $shippingCategory);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function editMarkupSpecial($id)
    {
        try {
            $productAttribute = ProductAttribute::find($id);

            $shippingCategoriesSpecial = Ship_category::where('cat_type', 'shipping_special')->get();

            return view('administrator.product.shippingCategory.edit-special')
                ->with('productAttribute', $productAttribute)
                // ->with('shippingCategory', $shippingCategory)
                ->with('shippingCategoriesSpecial', $shippingCategoriesSpecial);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function updateMarkup(Request $request, $id)
    {
        $productAttribute = ProductAttribute::find($id);

        if ($request->input('category_id') != NULL) {
            $productAttribute->shipping_category = $request->input('category_id');
        }

        if ($request->input('category_id_special') != NULL) {
            $productAttribute->shipping_category_special = $request->input('category_id_special');
        }

        $productAttribute->save();
    }

    // Update bulk
    public function bulkUpdate(Request $request)
    {
        $action =  $request->input('bulkaction');
        $id = $request->input('action_ids');
        $arrayid = explode(",", $id);

        switch ($action) {
            case 'shipping':

                $category = $request->input('shippingCategory');
                if (!empty($category)) {
                    if (isset($id) && !empty($id)) {
                        $not_updated = array();

                        $result = DB::table('panel_product_attributes')
                            ->whereIn('panel_product_attributes.id', $arrayid)
                            ->update(['panel_product_attributes.shipping_category' => $category]);

                        $success_message = "Mark Category Successfully.";
                        $result = 'success';
                    } else {
                        $error_message = "Please select item to update";
                    }
                } else {
                    $error_message = "Please select category to update";
                }

                break;

            case 'shipping_special';

                $category = $request->input('shippingCategory');
                if (!empty($category)) {
                    if (isset($id) && !empty($id)) {
                        $not_updated = array();

                        $result = DB::table('panel_product_attributes')
                            ->whereIn('panel_product_attributes.id', $arrayid)
                            ->update(['panel_product_attributes.shipping_category_special' => $category]);

                        $success_message = "Mark Category Successfully.";
                        $result = 'success';
                    } else {
                        $error_message = "Please select item to update";
                    }
                } else {
                    $error_message = "Please select category to update";
                }

                break;

            default:
                $error_message = "Please choose at least one action";
        }

        if (!empty($result) && $result == "success") {
            return back()->with(['successful_message' => $success_message]);
        } else {
            return back()->with(['error_message' =>  $error_message, 'action_ids' => $id]);
        }
    }

    public function decideShippingCategory(Request $request)
    {
        $result = array();
        $type = $request->input('bulkaction');

        $shipping = Ship_category::where('cat_type', $type)->get();

        foreach ($shipping as $category) {
            $result[] = '<option value="' . $category->id . ' " >' . $category->cat_name . ' </option>';
        }
        return $result;
    }
}
