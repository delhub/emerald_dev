<?php

namespace App\Http\Controllers\Warehouse\Redemption;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\Outlet;
use App\Models\Purchases\Order;
use App\Models\Globals\Status;
use App\Models\Warehouse\Location\Wh_location;

class WarehouseRedemptionController extends Controller
{

    public function index(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        if ($userLocationId != '') $userOutletLocationId = $user->user_warehouse_outlet;

        $statuses = Status::whereIn('id', [1001, 1002, 1013])->get();

        $customerOrders = $this->query($request, $user, $userOutletLocationId);

        $ForCountcustomerOrders = $customerOrders->get();

        $customerOrders = $customerOrders->paginate(25);
        $customerOrders->appends(request()->query());

        return view('warehouse.Redemption.index')
            ->with('customerOrders', $customerOrders)
            ->with('statuses', $statuses)
            ->with('request', $request)
            ->with('ForCountcustomerOrders', $ForCountcustomerOrders);
    }

    public function query($request, $user, $userOutletLocationId)
    {
        $customerOrders = Order::selectRaw('orders.*, orders.id')
            ->where('orders.delivery_method', 'Self-pickup')
            ->where('orders.delivery_outlet', '!=', 0)
            ->where('orders.pick_pack_status', 6)
            ->whereIn('orders.order_status', ['1001', '1002', '1013'])
            ->statusFilter($request->status)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->orderBy('orders.created_at', 'DESC');

        if (!$user->hasRole('wh_supervisor')) $customerOrders = $customerOrders->whereIn('orders.delivery_outlet', $userOutletLocationId);

        return $customerOrders;
    }

    //Excel CSV Download
    public function exportCsv(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        if ($userLocationId  != '') $userOutletLocationId = $user->user_warehouse_outlet;

        $customerOrders = $this->query($request, $user, $userOutletLocationId)->get();

        $fileName = 'Warehouse Redemption.csv';

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array(
            'Invoice No.',  'Date of collection', 'Collected by', 'Contanct Number', 'Agent Name', 'Group', 'Location'
        );

        $callback = function () use ($customerOrders, $columns) {


            $file = fopen('php://output', 'z');
            fputcsv($file, $columns);

            foreach ($customerOrders as $customerOrder) {

                $row['Invoice No.']    = $customerOrder->purchase->getFormattedNumber();
                $row['Date of collection']    = $customerOrder->received_date;
                $row['Collected by']    = $customerOrder->purchase->ship_full_name;
                $row['Contanct Number']    = $customerOrder->purchase->ship_contact_num;

                $users = $customerOrder->purchase->user;
                if ($users) {
                    $agentName = $users->userInfo->agentInformation->full_name;

                    $agentGroup = $users->userInfo->agentGroup;
                    $groupName = $agentGroup ? $agentGroup->group_name : '-';
                }
                $row['Agent Name'] = $agentName ?  $agentName : '-';
                $row['Group']  = $agentGroup ? $groupName : '-';

                $outlet = Outlet::where('id', $customerOrder->delivery_outlet)->first();
                $row['Location']  = $outlet ? $outlet->warehouseLocation->location_name : '-';

                fputcsv($file, array(
                    $row['Invoice No.'], $row['Date of collection'], $row['Collected by'],  $row['Contanct Number'],  $row['Agent Name'], $row['Group'], $row['Location']
                ));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
