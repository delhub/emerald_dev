<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Warehouse\StockTransfer\StockTransfer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Users\User;
use App\Models\Users\UserInfo;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use Illuminate\Support\Facades\Hash;
use App\Models\Users\UserCountry;


class AddWhReceiverAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wh_transfer', function (Blueprint $table) {
            $table->bigInteger('user_receive')->nullable()->after('user');
            $table->tinyInteger('type')->default(0)->after('transfer_datetime');
            $table->tinyInteger('status')->default(1)->after('type');
        });

        Schema::table('wh_transfer_item', function (Blueprint $table) {
            $table->integer('received')->default(0)->after('batch_item_id');
        });

        $StockTransfer = StockTransfer::get();
        foreach ($StockTransfer as $key => $transfer) {
            $transfer->status = 5;
            $transfer->save();
        }

        DB::table('roles')->insert(
            array(
                array('name' => 'wh_receiver', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        DB::table('global_statuses')->insert(
            array(
                array('id' => 8021, 'name' => 'Stock Transfer - In transit', 'description' => 'Stock Transfer - In transit', 'status_type' => 'inventory_status', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        $role = 'receiver';
        $this->createCustomer($role, $verify = true, 'MY');
    }

    public static function createCustomer($role, $verify = true, $country_id)
    {
        // Users table.
        $user = new User;
        $user->password = Hash::make($role . '1234');
        $user->email = $role . '1@email.com';

        if ($verify) $user->email_verified_at = Carbon::now();
        $user->save();

        // Generating new customer account id.
        $largestCustomerId = UserInfo::largestCustomerId() + 1;

        // User_infos table.
        $userInfo = new UserInfo;
        $userInfo->user_id = $user->id;
        $userInfo->account_id = $largestCustomerId;
        $userInfo->user_level = 6001;

        $userInfo->account_status  = 1;

        $userInfo->full_name = $role;
        $userInfo->nric = 1234;
        $userInfo->referrer_id = 1;
        $userInfo->group_id = 13;
        $userInfo->save();

        // Users table.
        $userCountry = new UserCountry;
        $userCountry->account_id = $largestCustomerId;
        $userCountry->country_id = $country_id;
        $userCountry->referrer_id = $country_id . '1010';
        $userCountry->group_id = 13;
        $userCountry->save();

        // User_addresses table(two records - billing address and shipping address)
        $userAddress_billing_address = new UserAddress;
        $userAddress_billing_address->account_id = $userInfo->account_id;
        $userAddress_billing_address->address_1 = 1;
        $userAddress_billing_address->city_key = 0;
        $userAddress_billing_address->state_id = 14;
        $userAddress_billing_address->country_id = $country_id;
        $userAddress_billing_address->is_shipping_address = 1;
        $userAddress_billing_address->is_residential_address = 1;
        $userAddress_billing_address->is_mailing_address = 1;
        $userAddress_billing_address->save();

        // User_contacts table (Home).
        $userContactHome = new UserContact;
        $userContactHome->account_id = $userInfo->account_id;
        $userContactHome->contact_num = 1;
        $userContactHome->is_home = 1;
        $userContactHome->is_mobile = 1;
        $userContactHome->is_office = 1;

        $userContactHome->save();

        $user->assignRole('wh_' . $role);

        return $user;
    }

    /** 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
