<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Status;
use Illuminate\Support\Facades\DB;

class AddStockOutGlobalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $newStatus = new Status;
        $newStatus->id = 8012;
        $newStatus->name = 'Purchase Order - Stock Out (POS)';
        $newStatus->description = 'Product out process complete form POS system';
        $newStatus->status_type = 'inventory_status';
        $newStatus->save();

        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock
        AS
        SELECT product_code, outlet_id,

        sum(case when inv_type = '8010' OR inv_type = '8000'
        then inv_amount
        else 0
        END)
        AS physical_stock,

        sum(case when inv_type = '8001'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved,

        sum(case when inv_type = '8011'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved_offline,

        sum(case when inv_type = '8012'
        then inv_amount
        else 0
        end)
        as pos_completed,

        sum(case when inv_type = '8000' OR inv_type = '8001' OR inv_type = '8011' OR inv_type = '8010' OR inv_type = '8012'
        then inv_amount
        else 0
        end)
        as virtual_stock

        from wh_inventories
        GROUP BY product_code, outlet_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
