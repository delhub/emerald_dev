<?php

namespace App\Http\Controllers\Purchase;

class FpxdGatewayController extends FpxGatewayController {

   public function gatewayResponse($request){

      parent::gatewayResponse($request);

      return 'OK';

   }
}