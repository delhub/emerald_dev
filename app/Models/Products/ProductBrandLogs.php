<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductBrandLogs extends Model
{
    // Set table
    protected $table = 'product_brand_logs';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'log_month',
        'brand_count',
        'product_count',
        'new_brand_count',
        'new_product_count',
        'product_suspended',
        'product_terminate',
    ];
}
