@inject('controller', 'App\Http\Controllers\Warehouse\StockAdjustment\StockAdjustmentController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>{{$controller::$header}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        {{-- <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div>
        </div> --}}
        <div class="table-responsive m-2">

            <div class="col-12">
                <form action="{{route($controller::$transferStart)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12 col-md-4">
                        <p>
                            User Name : <p>{{$user->userInfo->full_name}}</p>
                        <input type="hidden" name="user" value="{{$user->id}}">
                        </p>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Location<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="location_id" class="form-control" required>
                            {{-- <option value="">Choose location..</option> --}}
                            @foreach($locations as $location)
                                <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Product Code<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="product_code" class="form-control select2 pc" required>
                            <option value="">Please choose a product code.</option>
                            @foreach($products as $product)
                                <option value="{{ $product->product_code }}">{{ $product->product_code }} - {{ $product->product2->parentProduct->name }} ({{ $product->attribute_name }})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Adjust Type<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="adjustType" class="form-control" required>
                            <option value="">Please choose a type.</option>
                            <option value="8017"> Adjust IN (+)</option>
                            <option value="8018"> Adjust OUT (-)</option>
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Quantity<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <input type="number" name="quantity" min="1" class="form-control" placeholder="Quantity" value="{{old('quantity')}}"required>{{old('quantity')}}
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Remark<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <textarea name="remark" class="form-control" required></textarea>
                    </div>
                    {{-- <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Transfer Date<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                            <input type="date" name="transfer_datetime" id="transfer_datetime" class="form-control" placeholder="Stock In Date" autocomplete="off" value="{{old('transfer_datetime')}}"
                            data-parsley-error-message="This value is required." data-parsley-trigger="change" data-parsley-errors-container="#stockin-error-block" required>
                        </div>
                        <span id="stockin-error-block"></span>
                    </div> --}}

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-danger" style="float: center;"
                           >Start Adjust</button>
                        </div>
                    </div>

                    {{-- <input type="hidden" name="location_id" value="{{$puchongWarehouse->id}}"> --}}

                </form>
            </div>

           {{-- <p style="text-align: center"> Please selct Bin</p>
           <select name="bin_select" class="form-control">
               @foreach ($pickBins as $pickBin)
                <option value="{{$pickBin->id}}">{{$pickBin->pick_number}}<option>
               @endforeach
           </select> --}}
            {{-- <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p> --}}
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit(userId) {
    if (confirm('Are you sure want to adjustment?')) {
        document.getElementById("create-form-id").submit();
    }
}

// $('#stock_in_date').datepicker({
//     // uiLibrary: 'bootstrap4',
//     format: 'yyyy-mm-dd',
//     showOtherMonths: true,
//     selectOtherMonths: true
// });
</script>
@endpush
