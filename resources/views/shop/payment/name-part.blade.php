<div class="col-12 col-md-12">
    <div>
        <div class="form-row">
            <div class="m-0 col-12 form-group">
                <label class="m-0"><b>Name</b></label>
                <p id="attention_to_tag" class="mb-0"
                    style="border: 2px solid #f4f4f4; border-radius: 5px; padding: 10px;background: #e6e6e6;">
                    {{ $currentAddress['name'] }}
                </p>
                @if (empty($currentAddress['name']))
                <small class="pl-2" id="showError" style="color:red;">Please fill in your shipping name</small>
                @endif
            </div>
        </div>

        <div class="form-row my-2">
            <div class="col-12 m-0 form-group">
                <label class="m-0"><b>Contact (Mobile)</b></label>
                <p id="attention_contact_tag" class="mb-0"
                    style="border: 2px solid #f4f4f4; border-radius: 5px; padding: 10px;background: #e6e6e6;">
                    <span id="attention_contact_number_tag">
                        {{ $currentAddress['contact'] }}
                    </span>
                </p>
                 @if (empty($currentAddress['contact']))
                <small class="pl-2" id="showError" style="color:red;">Please fill in your shipping contact</small>
               @endif
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 m-0 form-group">
                <label class="m-0"><b>Shipping Address</b></label>
                <p id="attention_address_tag" class="mb-0"
                    style="border: 2px solid #f4f4f4; border-radius: 5px; padding: 10px;background: #e6e6e6;">
                    <span id="attention_address_1_tag">
                        {!! empty($currentAddress['address1']) ? '' : $currentAddress['address1'] . ',
                        <br>' !!}
                    </span>
                        {!! empty($currentAddress['address2']) ? '' : $currentAddress['address2'] . ',<br>' !!}
                        {!! empty($currentAddress['address3']) ? '' : '' . $currentAddress['address3'] . ', <br>' !!}
                        {!! empty($currentAddress['postcode']) ? '' : $currentAddress['postcode'] . ',' !!}
                        {!! $currentAddress['city_key'] == '0' ? '' . $currentAddress['city'] . ',' : '' . $currentAddress['city_name']->city_name . ',' !!}
                        {!! empty($currentAddress['state']) ? '' : '<br>' . $currentAddress['state_name']->name  !!}
                </p>
                @if (empty($currentAddress['address1']) )
                    <small class="pl-1" id="showError" style="color:red;">Please fill in your shipping address</small>
                @endif
            </div>

        </div>

        {{---- <button type="button" class="btn btn-info d-block w-100" data-toggle="modal"
            data-target="#exampleModal">Edit</button> --}}
    </div>
</div>
