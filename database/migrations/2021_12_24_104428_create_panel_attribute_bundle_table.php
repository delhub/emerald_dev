<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanelAttributeBundleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panel_attribute_bundle', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('attribute_id');
            $table->string('product_code');
            $table->string('primary_product_code');
            $table->bigInteger('primary_attribute_id');
            $table->integer('primary_quantity');
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panel_attribute_bundle');
    }
}
