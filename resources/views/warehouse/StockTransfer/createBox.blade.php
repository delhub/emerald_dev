@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')


<h1>Stock Transfer</h1>
<div class="card shadow-sm">
    <div class="card-header">Warehouse Stock Transfer<small> - Add new Box Label</small>
        {{-- <div class="card-header-actions"><button class="btn btn-primary btn-sm" type="button" onclick="window.location.href = '{{ route('administrator.v1.products.warehouse.index')}}';">Batch List</button></div> --}}
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-10">
                <form id="batch" action="{{ route('warehouse.StockTransfer.print-label')}}" method="GET">
                    @csrf

                    <div class="form-group">
                        <p>
                            Location (from)<small class="text-danger">*</small>
                        </p>
                        {{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})
                        <input type="hidden" name="location_id" value="{{$location->id}}"/>
                        {{-- <select name="location_id" class="form-control">
                            @foreach($locations as $location)
                                <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>
                            @endforeach
                        </select> --}}
                    </div>

                    <div class="form-group">
                        <label class="col-form-label" for="panel">Box Name<small class="text-danger">*(unique, cannot repeat box name)</small></label>
                        <input type="text" name="box_name" class="form-control" placeholder="Box Name" autocomplete="off" value="{{old('box_name')}}" required>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label" for="panel">Notes</label>
                        <textarea type="text" name="notes" id="notes" class="form-control" value="{{old('notes')}}" placeholder="Notes will show under box name">{{old('notes')}}</textarea>
                    </div>

                    <div class="form-group text-center">
                        <input type="hidden" name="method" value="create">
                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
<script>
    // Parsley plugin initialization with tweaks to style Parsley for Bootstrap 4
    $("#batch").parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    });

 // Parsley full doc is avalailable here : https://github.com/guillaumepotier/Parsley.js/
</script>
<script>
    $('#stock_in_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
    /* $('.expiry_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    }); */

    function removeBatchDetails(element) {
        // $("#details").remove("div.details:last");
        $(element).parent().parent().parent().parent().slideUp(300, function() {
            $(this).remove();
        });
        disableFirstMinus();
    }

    function cloneBatchDetails() {
        // $(".details:last").find('.pc').select2('destroy');
        $(".pc").select2('destroy');
        $("button.minus").attr("disabled", false);
        $("#details").clone().insertAfter("div.details:last");
        $("#details div > span").remove();
        disableFirstMinus();
        $('.pc').select2();
    }

    function disableFirstMinus() {
        $("button.minus:first").attr("disabled", true);
    }

    setTimeout(function() {
        new SlimSelect({
            select: '.slimselect'
        })
    }, 300)
</script>
@endpush
