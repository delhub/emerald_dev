@extends('layouts.guest.main')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo" src="{{ asset('images/logo/formula-logo.png') }}" alt="">
        </div>
    </div>

    <div class="my-register-box my-guests-card">
        <div class="my-r-title-box">
            <h5><b>{{ (session()->has('incomplete')) ? 'PROFILE' : 'REGISTRATION'}}</b></h5>
        </div>

        <div class="card-body">

            <form method="POST" action="{{ route('guest.register.customer.information.post') }}" id="register-walk-in">
                @csrf
                <div class="tab-content r-inner-box" id="myTabContent">

                    <div class="r-subt-box tab-pane fade show active pl-3 pr-3 pt-3 pb-3" id="registration" role="tabpanel"
                        aria-labelledby="registration-tab">
                        <h5>Personal Particulars</h5>
                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="full_name">Full Name (as per NRIC) <small class="text-danger">*</small></label>
                                <input type="text" name="full_name"
                                    class="form-control @error('full_name') is-invalid @enderror" id="full_name" required
                                    placeholder=""
                                    value="{{ old('full_name', $user_info->full_name) }}">
                                @error('full_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="validate-customer">Please choose : </label>
                                <div class="validate-customer">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="frm_customer" id="frm_customer_1"
                                            value="1" checked @if (($user_info) ? $user_info->nric !== null : false || old('frm_customer') == '1') checked
                                                @endif>
                                        <label class="form-check-label" for="frm_customer">NRIC Number</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="frm_customer" id="frm_customer_0"
                                            value="0" @if (($user_info) ? $user_info->passport_no !== null : false || old('frm_customer') == '0') checked @endif>
                                        <label class="form-check-label" for="frm_customer">Passport Number</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 if-frm" style="display:none">
                                <label for="nric">NRIC Number <small>(Numbers Only) </small><small class="text-danger">*</small></label>
                                <input type="text" name="nric" class="form-control @error('nric') is-invalid @enderror"
                                    id="nric" placeholder="" value="{{ old('nric', $user_info->nric) }}">
                                @error('nric')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row register-info">
                            <div class="form-group col-md-12 if-notfrm">
                                <label for="date_of_birth">Date Of Birth</label>
                                <input type="date" name="date_of_birth"
                                    class="form-control datepicker @error('date_of_birth') is-invalid @enderror"
                                    id="date_of_birth" value="{{ old('date_of_birth', $user_info->date_of_birth) }}">
                                @error('date_of_birth')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 if-notfrm">
                                <label for="passport_no">Passport Number <small class="text-danger">*</small></label>
                                <input type="text" name="passport_no"
                                    class="form-control @error('passport_no') is-invalid @enderror" id="passport_no"
                                    placeholder="Passport Number" value="{{ old('passport_no', $user_info->passport_no) }}">
                                @error('passport_no')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 if-notfrm">
                                <label for="country">Country</label>
                                <select name="country" id="country"
                                    class="form-control @error('country') is-invalid @enderror">
                                    <option disabled selected>Choose your country..</option>
                                    @foreach ($countries as $country)
                                        <option class="text-capitalize" value="{{ old('country', $country->country_name) }}" @if($country->country_name == $user_info->country) selected @endif>
                                            {{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="address_1">Address Line 1 <small class="text-danger">*</small></label>
                                <input type="text" name="address_1" id="address_1"
                                    class="form-control @error('address_1') is-invalid @enderror"
                                    placeholder="Residential Address Line 1"
                                    value="{{ old('address_1', $user_info->shippingAddress->address_1) }}">
                                @error('address_1')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="address_1">Address Line 2</label>
                                <input type="text" name="address_2" id="address_2"
                                    class="form-control @error('address_2') is-invalid @enderror"
                                    placeholder="Residential Address Line 2"
                                    value="{{ old('address_2', $user_info->shippingAddress->address_2) }}">
                                @error('address_2')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="address_1">Address Line 3</label>
                                <input type="text" name="address_3" id="address_3"
                                    class="form-control @error('address_3') is-invalid @enderror"
                                    placeholder="Residential Address Line 3"
                                    value="{{ old('address_3', $user_info->shippingAddress->address_3) }}">
                                @error('address_3')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="state">State <small class="text-danger">*</small></label>
                                <select name="state" id="state" class="form-control @error('state') is-invalid @enderror">
                                    <option disabled selected>Choose your state..</option>
                                    @foreach ($states as $state)
                                        <option class="text-capitalize" value="{{ old('state', $state->id) }}" @if($user_info->shippingAddress->state_id == $state->id) selected @endif>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                                @error('state')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12" id="class_city_key">
                                <label for="city_key">City <small class="text-danger">*</small></label>
                                <select name="city_key" id="city_key" class="form-control @error('city_key') is-invalid @enderror">
                                    <option disabled selected>Choose your city..</option>
                                    @foreach ($cities as $city)
                                        <option class="text-capitalize" value="{{ old('city_key', $city->city_key) }}" @if($user_info->shippingAddress->city_key == $city->city_key) selected @endif>{{ $city->city_name }}</option>
                                    @endforeach
                                    {{-- <option value="0">Others</option> --}}
                                </select>
                                @error('city_key')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6" id="city">
                                <label for="city">Others City <small class="text-danger">*</small></label>
                                <input type="text" name="city" class="form-control" value="{{ old('city', $user_info->shippingAddress->city)}}"
                                    placeholder="Enter Your City Here">
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="postcode">Postcode <small class="text-danger">*</small><span class="form-text text-danger ajaxMessage d-inline"
                                        style="display: none"></span></label>
                                <input type="text" name="postcode" id="postcode"
                                    class="form-control @error('postcode') is-invalid @enderror" placeholder="Postcode"
                                    value="{{ old('postcode', $user_info->shippingAddress->postcode) }}">
                                @error('postcode')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-6">
                                <label for="contact_number_home">Contact Number (Home)</label>
                                <input type="text" name="contact_number_home" id="contact_number_home"
                                    class="form-control @error('contact_number_home') is-invalid @enderror" placeholder="Contact Number (Home)"
                                    value="{{ old('contact_number_home', $user_info->homeContact->contact_num) }}">
                                @error('contact_number_home')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number_mobile">Contact Number (Mobile) <small class="text-danger">*</small></label>
                                <input type="text" name="contact_number_mobile" id="contact_number_mobile"
                                    class="form-control @error('contact_number_mobile') is-invalid @enderror" placeholder="Contact Number (Mobile)"
                                    value="{{ old('contact_number_mobile', $user_contact->contact_num) }}">
                                @error('contact_number_mobile')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-6">
                                <label for="referrer_id">Agent Formula ID</label>
                                <input type="text" name="referrer_id" id="referrer_id"
                                    class="form-control @error('referrer_id') is-invalid @enderror" placeholder="3911XXXXXX"
                                    value="{{$user_info->referrer_id}}" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="referrer_name">Agent Name</label>
                                <input type="text" name="referrer_name" id="referrer_name"
                                    class="form-control @error('referrer_name') is-invalid @enderror" agent-name
                                    placeholder=""
                                    value="{{$user_info->referrer_name}}" readonly>
                            </div>
                        </div>
                            <div class="r-next-btn ml-auto">
                                <a class="btn r-color-btn" id="submit-btn"><b>Update</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            frmCustomerSelect();
        });

        function frmCustomerSelect() {

            frm_customer = $('input[name="frm_customer"]:checked').val();

            if (frm_customer == '1') {

                $('.if-frm').show();
                $('.if-notfrm').hide();

            } else {

                $('.if-frm').hide();
                $('.if-notfrm').show();
            }

        }

        $('input[name="frm_customer"]').click(frmCustomerSelect);

        // Custom validator for postcode.
        jQuery.validator.addMethod("postcode", function(value, element) {
            return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
        }, "Please provide a valid postcode.");

        // Validate registration tab before moving to the next tab
        $("#register-walk-in").validate({
            rules: {
                full_name: {
                    required: true,
                    minlength: 3
                },
                nric: {
                    required: function() {
                        return frm_customer == 1
                    },
                    digits: true,
                    minlength: 12,
                    maxlength: 12
                },
                passport_no: {
                    required: function() {
                        return frm_customer == 0
                    }
                },
                address_1: {
                    required: true,
                    minlength: 3
                },

                postcode: {
                    required: true,
                    minlength: 5,
                    maxlength: 6
                    // postcode: true
                },
                city_key: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                contact_number_mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 15
                }
            },
            messages: {
                full_name: {
                    required: "Please enter your full name.",
                    minlength: "Your name must be more than 3 characters."
                },
                nric: {
                    required: "Please enter your NRIC number.",
                    minlength: "Please enter a valid NRIC number.",
                    maxlength: "Please enter a valid NRIC number."
                },
                address_1: {
                    required: "Please enter your address"
                },

                postcode: {
                    required: "Please enter your postcode"
                },
                city_key: {
                    required: "Please select your city"
                },
                city: {
                    required: "Please select your city"
                },
                state: {
                    required: "Please select your state"
                },
                contact_number_mobile: {
                    required: "Please enter your mobile number.",
                    digits: "Please enter number only.",
                    minlength: "Contact number must at least be 10 digits.",
                    maxlength: "Please enter a valid contact number."
                }
            }
        });

        $(document).ready(function() {

            /* City */
            var cityKey = document.getElementById("city_key");
            var city = document.getElementById("city");
            var classCityKey = document.getElementById("class_city_key");

            displayCity();

            $('#city_key').on('change', function() {
                displayCity();
            });

            $("#state").on("change", function() {
                var variableID = $(this).val();

                if (variableID) {
                    $.ajax({
                        type: "POST",
                        url: '{{ route('guest.register.customer.state-filter-city') }}',
                        data: $("#register-walk-in").serialize(),
                        success: function(toajax) {
                            $("#city_key").html(toajax);
                            displayCity();
                        }
                    });
                }

            });

            function displayCity() {
                if (cityKey.value == '0') {
                    city.style.display = "block";
                    classCityKey.className = "col-12 col-md-6 form-group";
                } else {
                    city.style.display = "none";
                    classCityKey.className = "col-12 col-md-12 form-group";
                }
            }
            /* End City */

        });

        $("#postcode").keyup(function() {
            var $postcode = $('#postcode').val();
            if ($postcode.length == 5) {
                $.ajax({
                    url: '/get-postcode',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "postcode": $postcode
                    },
                    beforeSend: function() {},
                    success: function(success) {
                        console.log(success.data);
                        if (success.data == false) {
                            // $('#exampleFormControlInput2').attr('style','border:red;')
                            // alert('false');
                            $('.ajaxMessage').show().html('Invalid Postcode');
                            return false;
                        } else {
                            $('.ajaxMessage').hide().html('');
                            $('#cart-form').submit();

                        }

                    },
                    error: function() {
                        alert("error in loading");
                    }

                });
            }
        });

        $('#submit-btn').on('click', function () {
            $('#register-walk-in').submit();
        });

        // Handle keypress enter on all platform
        $('#register-walk-in').keypress((e) => {
            if (e.which === 13) {
                $('#register-walk-in').submit()
            }
        })
    </script>
@endpush
