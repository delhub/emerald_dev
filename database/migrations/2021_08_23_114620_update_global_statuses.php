<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Status;
use Illuminate\Support\Facades\DB;

class UpdateGlobalStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $status = Status::where('id', 8003)->first();

        $status->name = 'Purchase Order - Cancel';
        $status->description = 'Purchase Order that Canceled';

        $status->save();


        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock
        AS
        SELECT product_code,

        	sum(case when inv_type = '8010' OR inv_type = '8000'
        	then inv_amount
        	else 0
        	END)
        	AS physical_stock,


        sum(case when inv_type = '8001'
          then inv_amount
          else 0
          end)
          as virtual_stock_reserved,


        sum(case when inv_type = '8001' OR inv_type = '8010' OR inv_type = '8000'
          then inv_amount
          else 0
          end)
          as virtual_stock

        from wh_inventories
        GROUP BY product_code
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
