<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePanelIdToWhBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wh_batch', function (Blueprint $table) {
            //
        });

        Schema::table('wh_batch', function ($table) {
            $table->bigInteger('panel_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wh_batch', function (Blueprint $table) {
            $table->Integer('panel_id')->change();
        });
    }
}
