<?php

namespace App\Models\Warehouse;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ViewInventoriesStockBundle extends Model
{
    // Set table
    protected $table = 'view_inventories_stock_bundle';

    public function scopeProductFilter($query, $search_box)
    {
        if ($search_box != null) {
            return $query
                ->where('view_inventories_stock_bundle.product_code', 'LIKE', '%' . $search_box . '%')
                ->orwhere('panel_product_attributes.attribute_name', 'LIKE', '%' . $search_box . '%')
                ->orwhere('global_products.name', 'LIKE', '%' . $search_box . '%')
                ->orderBy('panel_product_attributes.product_code', 'ASC');
        } else {
            return $query;
        }
    }

    public function scopeLocationFilter($query, $location_id)
    {
        if ($location_id != 'all' && $location_id != NULL) {
            return $query->where('view_inventories_stock_bundle.outlet_id', $location_id);
        } else {
            // return $query;
        }
    }

    public function scopeProductQualityFilter($query, $quality)
    {
        if ($quality != '0' && $quality != NULL) {
            return $query->where('global_products.quality_id', $quality);
        } else {
            return $query;
        }
    }

    public function warehouseLocation()
    {
        return $this->belongsTo('App\Models\Warehouse\Location\Wh_location', 'outlet_id', 'id');
    }

    //date filter from date to date
    public function scopedatebetweenFilter($query, $datefrom, $dateto)
    {
        $from = Carbon::parse($datefrom);
        $to = Carbon::parse($dateto);

        $from->startOfDay();
        $to->endOfDay();

        if (!is_null($datefrom) && !is_null($dateto)) { //when user entry datefrom & dateto

            return $query
                ->whereBetween('view_inventories_stock_bundle.createdAt', [$from, $to]);
        } elseif (isset($datefrom)) { //when user entry datefrom only

            $endOfDatefrom = Carbon::parse(' ' . $datefrom . ' 23:55:59');

            return $query
                ->whereBetween('view_inventories_stock_bundle.createdAt', [$from, $endOfDatefrom]);
        } elseif (isset($dateto)) { //when user entry dateto only

            $startOfDateto = Carbon::parse(' ' . $dateto . ' 00:00:00');

            return $query
                ->whereBetween('view_inventories_stock_bundle.createdAt', [$startOfDateto, $to]);
        }
    }
}
