@extends('layouts.management.main-agent')
@section('content')

    <div class="container-fluid">

        <div class="row myagentpage-row">
            <div class="ad-my-box col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h4>Monthly Statement</h4>
                    </div>

                </div>

                <div class="d-style-box card">
                    <div class="card-body">
                        <div class="row">
                            <div class="card-title in-title">
                                <div class="yearly-title">
                                    <img class="img-i-icon"
                                        src="{{ asset('/images/icons/dashboard-icons/monthly-icon.png') }}" alt="">
                                    <h4>{{ $responses['business_month'] }} Income Statement</h4>
                                </div>
                                <div class="iselect-box">
                                    <select id="monthsDropdown">
                                        @foreach ($responses['batch'] as $key => $batch)

                                            <option value='{{ $key }}'>{{ $batch }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-text">
                            <div class="row my-pb-3">
                                <div class="col-md-4">
                                    <div class="">
                                        <div class="i-name-detail card-body body-card">
                                            <div class="i-name">
                                                <p>{{ $dealer->full_name }}</p>
                                            </div>
                                            <div class="i-address">
                                                <p>{{ $dealerAddress['address_1'] }}</p>
                                                <p>{{ $dealerAddress['address_2'] }}</p>
                                                <p>{{ $dealerAddress['address_3'] }}</p>
                                                <p>{{ $dealerAddress['postcode'] }} {{ $dealerAddress['city'] }},</p>
                                                <p>{{ $dealerAddress['state'] }},</p>
                                                <p>Malaysia.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="i-address col-md-4">
                                    <div class="row">
                                        <div class="col-5 col-md-5 body-card">
                                            <p>Agent ID</p>
                                        </div>
                                        <div class="col-auto body-card" style="padding:0;">
                                            <p> :</p>
                                        </div>
                                        <div class="col-6 col-md-6 body-card">
                                            <p>{{ $dealerID }}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5 col-md-5 body-card">
                                            <p>Grade</p>
                                        </div>
                                        <div class="col-auto body-card" style="padding:0;">:</div>
                                        <div class="col-6 col-md-6 body-card">
                                            <p>{{ $responses['rank'] }} </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('agent.statement', ['agent' => $dealerID, 'id' => $responses['id'] ]) }}" target="_blank" class="btn vr-filt-btn i-f-size">Download Monthly Statement
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="row hr-i"></div>
                        <div class="card-text">
                            <div class="yearly-title">
                                <img class="img-i-icon"
                                    src="{{ asset('/images/icons/dashboard-icons/summary-icon.png') }}" alt="">
                                <h4>Executive Summary</h4>
                            </div>
                        </div>
                        <div class="card-text">
                            <div class="row">
                                <div class="col-xl-6 col-sm-4 col-md-12 body-card">
                                    <div class="row" style="margin: 5px 10px;">
                                        <div class="i-sale-info col-sm-5 col-md-7 body-card">
                                            <p>Personal Sales</p>
                                        </div>
                                        <div class="col-sm-5 i-sales-box my-auto">
                                            <p>RM {{ number_format($responses['summary']['personal'], 2) }} </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-sm-4 col-md-12 body-card">
                                    <div class="row" style="margin: 5px 10px;">
                                        <div class="i-sale-info col-sm-5 col-md-7 body-card">
                                            <p>Total Revenue</p>
                                        </div>
                                        <div class="col-sm-5 i-sales-box my-auto">
                                            <p>RM {{ number_format($responses['summary']['total'], 2) }}</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6 col-sm-4 col-md-12 body-card">
                                    <div class="row" style="margin: 5px 10px;">
                                        <div class="i-sale-info col-sm-5 col-md-7 body-card">
                                            <p>Group Sales</p>
                                        </div>
                                        <div class="col-sm-5 i-sales-box my-auto">
                                            <p> RM {{ number_format($responses['summary']['group'], 2) }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-sm-4 col-md-12 body-card">
                                    <div class="row" style="    margin: 5px 10px;">
                                        <div class="i-sale-info col-sm-5 col-md-7 body-card">
                                            <p>Health Point Rewards</p>
                                        </div>
                                        <div class="col-sm-5 i-sales-box my-auto">
                                            <p> - </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
@push('script')
    <script>
         $(document).ready(function() {
             var currentPage = {{ $id }};
             if (currentPage == 0) {
                 $('#monthsDropdown').val($("#monthsDropdown option:first").val());
             } else {
                 $('#monthsDropdown').val(currentPage);

             }


             $('#monthsDropdown').on('change', function() {
                 var url = '/agent/monthly-income/' + $(this).val(); // get selected value

                 window.location = url; // redirect

                return false;
             });
         });
    </script>
@endpush
