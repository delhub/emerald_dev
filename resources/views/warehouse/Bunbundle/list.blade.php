@inject('controller', 'App\Http\Controllers\Warehouse\StockTransfer\StockTransferController')

@extends('layouts.administrator.main')

@section('content')

<h1>{{$controller::$headerTransferList}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div> --}}
    </div>
        <div class="table-responsive m-2">
            @if(count($tables) > 0)
            <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Location</th>
                        <th>Type</th>
                        <th>Product Code (From) Selected</th>
                        <th>Product Code (From) Scanned</th>
                        <th>Product Code (To)</th>
                        <th>Quantity</th>
                        <th>Panel</th>
                        <th>Expiry Date</th>
                        <th>Status</th>

                        {{-- <th>Start Time</th>
                        <th>End Time</th> --}}
                        <th>Date</th>
                        <th>Notes</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                </thead>
                <tbody>

                    @foreach($tables as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{ isset($row->theUser->userInfo) ? $row->theUser->userInfo->full_name : '' }}</td>
                        <td>{{$row->location->location_key}} - <b>{{$row->location->location_name}}</b><br>({{$row->location->default_address}})</td>
                        {{-- <td style="width:2%">{{$row->session_start_time}}</td>
                        <td style="width:2%">{{$row->session_end_time}}</td> --}}
                        <td>{{$row->type}}</td>
                        <td>
                            @if ($row->from_product_code != NULL && isset($row->productAttributeFrom))
                                <b>{{$row->from_product_code}}</b> - {{$row->productAttributeFrom->product2->parentProduct->name}} ({{$row->productAttributeFrom->attribute_name}})

                                @elseif ($row->from_product_code != ''&& isset($row->from_product_code))
                                {{-- @php
                                    $fromProducts = json_decode($row->from_product_code);
                                    foreach ($fromProducts as $key => $fromProduct) {
                                        echo $fromProduct;
                                            echo '<br>';
                                    }
                                @endphp --}}
                            @endif
                        </td>
                        <td>
                            @php
                            $tranfer = array();
                                foreach ($row->bUnbundleItems as $item){
                                    if(!isset($tranfer[$item->batchItem->batchDetail->product_code])) {
                                        $tranfer[$item->batchItem->batchDetail->product_code]['qty'] =0;
                                        $tranfer[$item->batchItem->batchDetail->product_code]['productName'] = $item->batchItem->batchDetail->productAttribute->product2->parentProduct->name;

                                    }
                                    $tranfer[$item->batchItem->batchDetail->product_code]['qty'] ++;
                                }

                                foreach ($tranfer as $productCode => $item) {
                                    echo '
                                        <b>'.$item['qty'].'</b> - <b>'.$productCode. '</b>
                                        <br>
                                        ('.$item['productName'].')
                                        <br>
                                    ';
                                }
                                unset($tranfer)
                            @endphp
                        </td>
                        <td>{{$row->product_code}} - {{$row->productAttribute->product2->parentProduct->name}} ({{$row->productAttribute->attribute_name}})</td>
                        <td>{{$row->quantity}}</td>
                        <td>{{$row->panel->company_name}}</td>
                        <td>{{$row->expiry_date}}</td>
                        <td>{{showCompletedStatus($row->completed)}}</td>
                        <td>{{$row->created_at}}</td>
                        <td>
                            {{$row->remark}}
                            {{-- <textarea type="text" name="notes" id="notes{{$row->id}}" class="form-control" value="{{$row->notes}}">{{$row->notes}}</textarea>
                            <div class="ml-auto col-2 mr-4 mt-2">
                                    <button type="button" class="btn btn-danger"
                                        onclick="editNotes({{ $row->id }});">
                                        Edit
                                        <div class="spinner-border spinner-border-sm text-dark variationLoader{{ $row->id }}" role="status" style="display:none;">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </button>
                                </div>
                                <p class="variationThisEmpty{{ $row->id }}" style="color:red;display: none;">Please enter something to add!</p>
                                <p class="variationRefreshPrice{{ $row->id }}" style="color:red;display: none;">Refresh to edit this notes</p>
                            </div> --}}
                        </td>


                            {{-- <a style="color: white; font-style: normal; border-radius: 5px; float: left" class="btn btn-primary shadow-sm"
                            href="{{ route($controller::$edit,['id' => $row->country_id]) }}" >Edit</a> --}}

                            {{-- <form action="{{ route($controller::$destroy,['id' => $row->country_id]) }}" id="delete-form-id" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            @csrf
                                <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>
                            </form> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- {{$tables->links()}} --}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$headerTransferList}} Found</p>
            @endif
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable({
            "order": [[ 0, "desc" ]],
        });
    });

    // function editNotes(batchID) {
    //     const notes =$("#notes"+batchID).val();

    //     if (confirm('Are you sure want to edit this notes? Any unsaved changes will be lost')) {
    //         if (notes=='' ) {
    //             $(".variationThisEmpty"+batchID).show();
    //         } else {
    //             $.ajax({
    //                 // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
    //                 url:"{{ route('warehouse.StockTransfer.stockTransferList.edit-notes', ['batchID' => "+batchID+"]) }}",
    //                 type: 'POST',
    //                 data: {
    //                     "_token": "{{ csrf_token() }}",
    //                     "batchID": batchID,
    //                     "notes": notes,
    //                 },
    //                 beforeSend: function () {
    //                     $(".variationLoader"+batchID).show();
    //                 },
    //                 success: function (success) {
    //                     $(".variationLoader"+batchID).hide();
    //                         $(".variationRefreshPrice"+batchID).show();
    //                 },
    //                 error: function () {
    //                     alert("error in loading");
    //                 }
    //             });
    //         }
    //     }
    // }
</script>
@endpush
