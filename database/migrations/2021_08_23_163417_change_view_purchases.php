<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeViewPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("
        CREATE OR REPLACE VIEW `view_purchases` AS SELECT `a`.`purchase_date` AS `purchase_date`,(a.purchase_amount / 100) total_fee, `a`.`purchase_number` AS `purchase_number`,`a`.`purchase_type` AS `purchase_type`,`a`.`purchase_status` AS `purchase_status`,`f`.`product_code` AS `product_code`,`d`.`product_id` AS `product_id`,`f`.`name_slug` AS `name_slug`,`d`.`quantity` AS `quantity`,(`d`.`unit_price` / 100) AS `unit_price`,(`d`.`subtotal_price` / 100) AS `subtotal_price` ,r.amount/100 AS rebate_fee,s.amount/100 AS shipping_fee,`a`.`ship_state_id` AS `state_id`,`g`.`group_id` AS `group_id`,`a`.`country_id` AS `country_id`
        FROM `purchases` `a`
        LEFT JOIN shipping_fee r ON r.purchase_id = a.id AND r.`type` = 'rebate_fee'
        LEFT JOIN shipping_fee s ON s.purchase_id = a.id AND s.`type` = 'shipping_fee'
        JOIN `user_infos` `g` ON `a`.`user_id` = `g`.`user_id`
        JOIN `orders` `c` ON `a`.`id` = `c`.`purchase_id`
        JOIN `items` `d` ON `c`.`order_number` = `d`.`order_number` AND `d`.`bundle_id` = 0
        JOIN `panel_products` `e` ON `d`.`product_id` = `e`.`id`
        JOIN `global_products` `f` ON `e`.`global_product_id` = `f`.`id`
        WHERE `a`.`purchase_type` in ('bluepoint','offline','card','hitpay','Cybersource','duitnow','voucher') AND `a`.`purchase_status` in (3001,3002,3003,3013,4000,4001,4002,4007)
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
