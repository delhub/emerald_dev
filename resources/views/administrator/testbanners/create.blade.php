@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/testbanners" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Created Banner
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.testbanners.store') }}" id="edit-banner-form" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Banner Title <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="banner_title" name="title" class="form-control" value="{{ old('title') }}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Banner Type <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="banner_type" id="banner_type" class="select2 form-control">
                                    <option value="default">Select a type..</option>
                                    <option value="home">Home</option>
                                    <option value="agent">Agent</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Country id <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="country_id" id="country_id" class="select2 form-control">
                                    <option value="default">Select a type..</option>
                                    <option value="MY">MY</option>
                                    <option value="SG">SG</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Banner Web Image <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                            <img id="banner_web" src="#" alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                                <input type="file" name="banner_web" id="banner_web" class="form-control-file" onchange="document.getElementById('banner_web').src = window.URL.createObjectURL(this.files[0])">
                                <small>Please make sure image is in 1:1 display ratio (square).</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Banner Mobile Image <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                            <img id="banner_mobile" src="#" alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                                <input type="file" name="banner_mobile" id="banner_mobile" class="form-control-file" onchange="document.getElementById('banner_mobile').src = window.URL.createObjectURL(this.files[0])">
                                <small>Please make sure image is in 1:1 display ratio (square).</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Banner Link Page <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="banner_link" name="banner_link" class="form-control" placeholder="#" value="{{ old('banner_link') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Banner Status
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input is-valid" name="banner_show" value="1" id="banner_show" {{-- ($testbanner->banner_show == "1") ? "checked" : "" --}}>
                                    <label class="custom-control-label" for="banner_show">Publish after saving.</label>
                                    <br>
                                    <small>Uncheck if you want the product to not be published after saving.</small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection


