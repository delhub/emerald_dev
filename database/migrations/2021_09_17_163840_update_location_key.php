<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Outlet;
use App\Models\Warehouse\Location\Wh_location;

class UpdateLocationKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $outlet = Outlet::first();
        $outlet->outlet_key = 'MYSGRPCG001';
        $outlet->save();

        $outlet = Wh_location::first();
        $outlet->location_key = 'MYSGRPCG001';
        $outlet->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
