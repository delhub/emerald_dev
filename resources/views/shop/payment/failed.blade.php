@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection
<div class="cart-home">
  <div class="fml-container">
      <div class="subtitle-namelist">
          <ul>
              <li><a href="/">Home</a></li>
              <li><span>/</span></li>
              <li><a href="#">Cart Page</a></li>
          </ul>
      </div>
              <section>
                      <div class="row footer-btm">
                          <div class="col-md-5">
                              <img class="thankyou-3dimg" src="{{asset('/images/formula/payment-fail-bg.png')}}" alt="">
                          </div>
                          <div class="col-md-7">
                              <div class="error-tittle">
                                  <div class="thankyou-text">
                                      <h5>Opps, Looks Like Something Went Wrong. Please Try Again.</h5>
                                      <p>Fail to process your payment.</p>
                                  </div>
                                  <div class="">
                                    @if (isset($errorMessage['purchases']) && $errorMessage['purchases']->purchase_type == 'Fpx')
                                    <div class="card bg-light p-md-3 col-md-10 shadow-sm p-3 mb-5 bg-body rounded mt-md-3">
                                        <div class="row">
                                            <div class="col-md-6 mt-2 mt-md-0">
                                                <p class="m-0 fpxTitle">Transaction Date and Time</p>
                                                <p class="m-0 fpxDesc">{{ date_format(date_create($errorMessage['payments']->fpx_fpxTxnTime),"d-M-Y h:i:s A") }}</p>
                                            </div>
                                            <div class="col-md-6 mt-2 mt-md-0">
                                                <p class="m-0 fpxTitle">Transaction ID</p>
                                                <p class="m-0 fpxDesc">{{ $errorMessage['payments']->fpx_fpxTxnId != null ? $errorMessage['payments']->fpx_fpxTxnId : '-' }}</p>
                                            </div>
                                        </div>
                                        <div class="row my-md-3">
                                            <div class="col-md-6 mt-2 mt-md-0">
                                                <p class="m-0 fpxTitle">Amount</p>
                                                <p class="m-0 fpxDesc">RM {{ $errorMessage['payments']->fpx_txnAmount }}</p>
                                            </div>
                                            <div class="col-md-6 mt-2 mt-md-0">
                                                <p class="m-0 fpxTitle">Bank</p>
                                                <p class="m-0 fpxDesc">{{ $errorMessage['banks'] ? $errorMessage['banks']->bank_name : '$payments->fpx_field[fpx_buyerBankId]' }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mt-2 mt-md-0">
                                                <p class="m-0 fpxTitle">Invoice Number</p>
                                                <p class="m-0 fpxDesc">{{ ($errorMessage['payments']->fpx_sellerOrderNo) }}</p>
                                            </div>
                                            <div class="col-md-6 mt-2 mt-md-0">
                                                <p class="m-0 fpxTitle">Status</p>
                                                <p class="m-0 fpxDesc">{{ $errorMessage['responses'] ? $errorMessage['responses']->description : '$payments->fpx_field[fpx_debitAuthCode]' }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <p>Error Code : {{ $errorMessage['response_code'] }}</p>
                                    <p>Bank Reason: {{ $errorMessage['description'] }}</p>
                                    @endif
                                  </div>
                                  <div class="edit-opt-btn btn-space2">
                                    <a href="{{ isset($errorMessage['directPay_uuid']) ? $errorMessage['directPay_uuid'] : 'cashier' }}">

                                        <button class="savechg-btn btn-primary">Try Again</button>
                                    </a>
                                  </div>
                              </div>
                          </div>
                      </div>
              </section>
  </div>
</div>
@endsection
@push('style')
<style>
    .fpxTitle{
        font-size:16px;
        font-weight: 500;
    }

    .fpxDesc{
        font-size:14px;
        color: #000;
    }
</style>
@endpush

@push('script')
<script>

</script>
@endpush
