<?php

namespace App\Http\Controllers\Administrator\ShippingInstallations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\Products\ProductAttribute;
use App\Models\Globals\Status;
use Carbon\Carbon;

class ShipCategoryController extends Controller
{
    public function index(Request $request)
    {
        $shipCategorys = Ship_category::orderBy('cat_type', 'desc')
            ->searchFilter($request->search)
            ->paginate(10);

        $list = array();

        foreach ($shipCategorys as $category) {
            foreach ($category->attributeShipping as $key => $att_shipping_category) {

                if (!isset($list[$att_shipping_category->shipping_category])) {
                    $list[$att_shipping_category->shipping_category]['attributeId'] = array();
                }

                if (!isset($list[$att_shipping_category->shipping_category_special])) {
                    $list[$att_shipping_category->shipping_category_special]['attributeId_special'] = array();
                }

                $list[$att_shipping_category->shipping_category]['attributeId'][] = $att_shipping_category->id;
                $list[$att_shipping_category->shipping_category_special]['attributeId_special'][] = $att_shipping_category->id;
            }
        }

        $userLevels = Status::where('status_type', 'user_level')->get();

        return view('administrator.shipping-installations.index', compact('shipCategorys', 'request', 'list', 'userLevels'));
    }

    public function create()
    {
        $userLevels = Status::where('status_type', 'user_level')->get();

        return view('administrator.shipping-installations.create')
            ->with('userLevels', $userLevels);
    }

    public function store(Request $request)
    {
        $start_date = $end_date = NULL;
        if ($request->input('cat_type') == 'shipping') {
            $active = 1;
        } else {
            $active = 0;
        }

        if ($request->input('start_date') != NULL || $request->input('end_date') != NULL) {
            $request->validate([
                'cat_name' => ['required', 'max:255'],
                'cat_desc' => ['required', 'max:255'],
                'for_user' => ['required'],
                'start_date' => ['required'],
                'end_date' => ['required'],
            ]);
        } else {
            $request->validate([
                'for_user' => ['required'],
                'cat_name' => ['required', 'max:255'],
                'cat_desc' => ['required', 'max:255'],
            ]);
        }

        if ($request->input('start_date') != NULL || $request->input('end_date') != NULL) {
            $start_date = $request->input('start_date');
            $start_date = Carbon::parse($start_date);
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date, 'Asia/Kuala_Lumpur');

            $end_date = $request->input('end_date');
            $end_date = Carbon::parse($end_date);
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $end_date, 'Asia/Kuala_Lumpur');

            $active = 1;
        }


        $checkName = Ship_category::where('cat_name', $request->input('cat_name'))->first();

        if (!$checkName) {
            $shipCategory = new Ship_category;

            $shipCategory->cat_name = $request->input('cat_name');
            $shipCategory->cat_desc = $request->input('cat_desc');
            $shipCategory->cat_type = $request->input('cat_type');
            $shipCategory->for_user = json_encode($request->input('for_user'));
            $shipCategory->start_date = $start_date;
            $shipCategory->end_date = $end_date;
            $shipCategory->active = $active;


            $shipCategory->save();

            return redirect('/administrator/shipping-installations/ship_category')->with('success', 'Shipping Category Created.');
        } else {
            return back()->with(['error' => "Do not use same category name !!! "]);
        }
    }

    public function edit($id)
    {
        $shipCategorys = Ship_category::find($id);
        $userLevels = Status::where('status_type', 'user_level')->get();

        return view('administrator.shipping-installations.edit')
            ->with('shipCategorys', $shipCategorys)
            ->with('userLevels', $userLevels);
    }

    public function update(Request $request, $id)
    {
        $start_date = $end_date = NULL;
        if ($request->input('cat_type') == 'shipping') {
            $active = 1;
        } else {
            $active = 0;
        }

        if ($request->input('start_date') != NULL || $request->input('end_date') != NULL) {
            $request->validate([
                'cat_name' => ['required', 'max:255'],
                'cat_desc' => ['required', 'max:255'],
                'for_user' => ['required'],
                'start_date' => ['required'],
                'end_date' => ['required'],
            ]);
        } else {
            $request->validate([
                'cat_name' => ['required', 'max:255'],
                'cat_desc' => ['required', 'max:255'],
                'for_user' => ['required'],
            ]);
        }

        if ($request->input('start_date') != NULL || $request->input('end_date') != NULL) {
            $start_date = $request->input('start_date');
            $start_date = Carbon::parse($start_date);
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date, 'Asia/Kuala_Lumpur');

            $end_date = $request->input('end_date');
            $end_date = Carbon::parse($end_date);
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $end_date, 'Asia/Kuala_Lumpur');

            $active = 1;
        }
        $shipCategory = Ship_category::find($id);

        $shipCategory->cat_name = $request->input('cat_name');
        $shipCategory->cat_desc = $request->input('cat_desc');
        $shipCategory->cat_type = $request->input('cat_type');
        $shipCategory->for_user = json_encode($request->input('for_user'));
        $shipCategory->start_date = $start_date;
        $shipCategory->end_date = $end_date;
        $shipCategory->active = $active;

        $shipCategory->save();

        return redirect('/administrator/shipping-installations/ship_category')->with('success', 'Shipping Category Updated');
    }
}
