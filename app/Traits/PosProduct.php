<?php

namespace App\Traits;

use App\Models\Globals\Products\Product as GProduct;
use App\Models\Products\Product as PProduct;
use App\Models\Products\ProductAttribute;
use App\Models\Products\ProductPrices;
use App\Models\Globals\Countries;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Users\Panels\PanelAddress;
use App\Models\Globals\Image;
use App\Models\Categories\Category;
use App\Models\Categories\PivCategory;
use App\Models\Products\ProductBrand;
use Carbon\Carbon;

trait PosProduct
{
    public static function storeProduct($v)
    {

        $gp = GProduct::where('product_code', $v->product_code)->first();

        if (!$gp) {
            $gp = new GProduct;
        }

        if (isset($v->video)) {
            $video_link = \explode(',', $v->video);

            $product_data = [
                'video' => [
                    'link' => $video_link,
                    'introduction' => $v->video_introduction,
                    'recommend' => $v->video_recommend,
                ]
            ];
        } else {
            $product_data = [];
        }

        $gp->product_type = isset($v->outlet_category) ? $v->outlet_category : 1;
        $gp->moh_code = isset($v->moh_code) ? $v->moh_code : null;
        $gp->product_code = $v->product_code;
        $gp->moh_code = isset($v->moh_code) ? $v->moh_code : null;
        $gp->name = $v->name;
        $gp->name_slug = str_slug($v->name);
        $gp->quality_id = isset($v->quality_id) ? $v->quality_id : 1;
        $gp->product_status = isset($v->product_status) ? $v->product_status : 1;
        $gp->eligible_discount = isset($v->eligible_discount) ? $v->eligible_discount : 0;
        $gp->generate_discount = isset($v->generate_discount) ? $v->generate_discount : 0;
        $gp->can_remark = isset($v->can_remark) ? $v->can_remark : 0;
        $gp->can_selfcollect = isset($v->can_selfcollect) ? $v->can_selfcollect : 0;
        $gp->can_preorder = isset($v->can_preorder) ? $v->can_preorder : 0;
        $gp->can_installment = '{"loanAmount":null,"firstPayment":null,"monthlyPayment":null,"interestRate":null,"tenure":null,"processingFee":null,"productDescription":""}';
        $gp->data = json_encode($product_data);
        $gp->hot_selection = isset($v->hot_selection) ? $v->hot_selection : 0;
        $gp->key_selection = isset($v->key_selection) ? $v->key_selection : 0;

        $gp->save();

        // Panel Product
        $pp = PProduct::where('global_product_id', $gp->id)->first();

        if (!$pp) {
            $pp = new PProduct;
        }

        $account_id = (isset($v->panel_account_id) ? $v->panel_account_id : 3011000000);

        $pp->global_product_id = $v->global_product_id;
        $pp->panel_account_id = $account_id;
        $pp->display_panel_name = isset($v->display_panel_name) ? $v->display_panel_name : 'FORMULA HEALTHCARE SDN. BHD';
        $pp->short_description =  isset($v->short_description) ? $v->short_description : NULL;
        $pp->product_description = isset($v->product_description) ? $v->product_description : NULL;
        $pp->place_of_origin = isset($v->place_of_origin) ? $v->place_of_origin : NULL;
        $pp->product_content_1 = isset($v->product_content_1) ? $v->product_content_1 : NULL;
        $pp->product_content_2 = isset($v->product_content_2) ? $v->product_content_2 : NULL;
        $pp->product_content_3 = isset($v->product_content_3) ? $v->product_content_3 : NULL;
        $pp->product_content_4 = isset($v->product_content_4) ? $v->product_content_4 : NULL;
        $pp->product_content_5 = isset($v->product_content_5) ? $v->product_content_5 : NULL;
        $pp->estimate_ship_out_date = isset($v->estimate_ship_out_day) ? $v->estimate_ship_out_day : 0;
        $pp->product_sv = isset($v->product_sv) ? $v->product_sv : 0;
        $pp->origin_state_id = 0;
        // $pp->shipping_category = 0;

        $pp->save();

        // store attribute
        if (count($v->attributes)) {
            foreach ($v->attributes as $k => $d) {
                self::storeProductAttribute($d);
            }
        }

        // store images
        if (count($v->images)) {
            Image::where([['imageable_id', $v->gid], ['imageable_type', 'App\Models\Globals\Products\Product']])->delete();

            foreach ($v->images as $k => $d) {
                self::storeProductImages($d);
            }
        }

        // store piv category
        if (count($v->pivCategory)) {
            foreach ($v->pivCategory as $k => $d) {
                self::storePivCategory($d);
            }
        }

        self::storePanelInfo($v);
        self::storePanelAddress($v);
    }

    public static function storeProductAttribute($v)
    {

        // panel product attribute
        $pa = ProductAttribute::where([['panel_product_id', $v->panel_product_id], ['product_code', $v->product_code]])->first();

        if (!$pa) {
            $pa = new ProductAttribute;
        }

        $pa->panel_product_id = $v->panel_product_id;
        $pa->attribute_type = 'size';
        $pa->attribute_name = $v->attribute_name;
        $pa->product_sv = isset($v->product_sv) ? $v->product_sv : 0;
        // $pa->color_images = $v[30];
        $pa->product_code = $v->product_code;
        $pa->product_measurement_length = isset($v->product_measurement_length) ? $v->product_measurement_length : 0;
        $pa->product_measurement_width = isset($v->product_measurement_width) ? $v->product_measurement_width : 0;
        $pa->product_measurement_depth = isset($v->product_measurement_depth) ? $v->product_measurement_depth : 0;
        $pa->product_measurement_weight = isset($v->product_measurement_weight) ? $v->product_measurement_weight : 0;

        $pa->save();
    }

    public static function storeProductImages($v)
    {

        $image = new Image;

        $image->path = $v->path;
        $image->filename = $v->filename;
        $image->default = $v->filename == 1 ? 1 : 0;
        $image->brand = $v->brand;
        $image->imageable_id = $v->imageable_id;
        $image->imageable_type = $v->imageable_type;

        $image->save();
    }

    public static function storeCategory($v)
    {

        $category = Category::where('slug', $v->slug)->first();

        if (!$category) {
            $category = new Category();
        }

        $category->name = $v->name;
        $category->slug = $v->slug;
        $category->type = $v->type;
        $category->parent_category_id = $v->parent_category_id;
        $category->featured = $v->featured;
        $category->featured_mobile = $v->featured_mobile;
        $category->position = $v->position;
        $category->active = $v->active;

        $category->save();
    }

    public static function storeBrand($v)
    {

        $brand = ProductBrand::where('name', $v->name)->first();

        if (!$brand) {
            $brand = new ProductBrand;
        }

        $brand->name = $v->name;
        $brand->slug = $v->slug;

        $brand->save();
    }

    public static function storePivCategory($v)
    {

        $pivCat = PivCategory::where('product_id', $v->product_id)->first();

        if (!$pivCat) {
            $pivCat = new PivCategory();
        }

        $pivCat->product_id = $v->product_id;
        $pivCat->category_id = $v->category_id;

        $pivCat->save();
    }

    public static function storePrice($v)
    {

        $model_id = ProductAttribute::where('product_code', $v->product_code)->first('id');

        $prices = ProductPrices::where([['model_id', $model_id], ['country_id', $v->country_id]])->first();

        if (!$prices) {
            $prices = new ProductPrices;
        }

        $prices->model_type = $v->model_type;
        $prices->model_id = $v->model_id;
        $prices->product_code = $v->product_code;
        $prices->country_id = $v->country_id;
        $prices->product_sv = isset($v->product_sv) ? $v->product_sv : 0;
        $prices->fixed_price = $v->fixed_price;
        // $prices->advance_price = $v->advance_price;
        $prices->standard_price = $v->standard_price;
        // $prices->advance_price = $v->advance_price;
        // $prices->premier_price = $v->premier_price;
        // $prices->web_offer_price = $v->web_offer_price;
        // $prices->outlet_offer_price = $v->outlet_offer_price;

        $prices->save();
    }

    public static function storePanelInfo($v)
    {
        $account_id = (isset($v->panel_account_id) ? $v->panel_account_id : 3011000000);

        $pi = PanelInfo::where('account_id', $account_id)->first();

        if (!$pi) {
            $pi = new PanelInfo;
        }
        $pi->user_id = time();
        $pi->account_id = $account_id;
        $pi->account_status = 1;
        $pi->company_name = (isset($v->display_panel_name) ? $v->display_panel_name : 'Formula2u');
        $pi->ssm_number = 1;
        $pi->company_email = 1;
        $pi->company_phone = 1;
        $pi->pic_name = 1;
        $pi->pic_nric = 1;
        $pi->pic_contact = 1;
        $pi->pic_email = 1;
        $pi->name_for_display = (isset($v->display_panel_name) ? $v->display_panel_name : 'Formula2u');

        $pi->save();
    }

    public static function storePanelAddress($v)
    {
        $account_id = (isset($v->panel_account_id) ? $v->panel_account_id : 3011000000);
        $pAdd = PanelAddress::where('account_id', $account_id)->first();

        if (!$pAdd) {
            $pAdd = new PanelAddress;
        }

        $pAdd->account_id = $account_id;
        $pAdd->address_1 = 'address1';
        $pAdd->address_2 = 'address2';
        $pAdd->address_3 = 'address3';
        $pAdd->address_1 = 'address1';
        $pAdd->postcode = 12345;
        $pAdd->city = 'city';
        $pAdd->city_key = 0;
        $pAdd->state_id = 14;
        $pAdd->is_correspondence_address = 1;
        $pAdd->is_billing_address = 1;

        $pAdd->save();
    }
}
