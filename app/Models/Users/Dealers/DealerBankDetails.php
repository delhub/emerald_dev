<?php

namespace App\Models\Users\Dealers;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\BankName;
use App\Http\Controllers\BujishuAPI;

class DealerBankDetails extends Model
{
    // Set table
    protected $table = 'dealer_bank';
    public $incrementing = false;
    // Set timestamps
    public $timestamps = false;

    // Set primary key
    protected $primaryKey = 'account_id';

    // Set mass assignable columns
    protected $fillable = [];



    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            self::updateServerB($model);
        });

        self::updated(function ($model) {
            self::updateServerB($model);
        });
    }

    // protected $attributes = ['bank_name'];
    public function getBankNameAttribute()
    {
        $bank_code = $this->bank_code;
        return BankName::getBank($bank_code);
    }

    public static function updateServerB($model)
    {
        $dealer_id = $model->account_id;
        $bank_code = $model->bank_code;
        $bank_acc = $model->bank_acc;
        $bank_acc_name = $model->bank_acc_name;
        $account_regid = $model->account_regid;

        BujishuAPI::updateBank($dealer_id, $bank_code, $bank_acc, $bank_acc_name, $account_regid);

    }
}
