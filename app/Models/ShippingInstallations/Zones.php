<?php

namespace App\Models\ShippingInstallations;

use Illuminate\Database\Eloquent\Model;

class Zones extends Model
{
    // Set table
    protected $table = 'shipping_zones';

    // Set primary key
    protected $primaryKey = 'id';

    // Set timestamps
    public $timestamps = true;

    // Set mass assignable columns
    // protected $fillable = [];

    public $incrementing = true;

    public function locations()
    {
        return $this->hasmany('App\Models\ShippingInstallations\Locations', 'zone_id');
    }

    // Search
    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {
            // Zone id
            if (($selectSearch == 'ZI')) {
                return $query
                    ->where('shipping_zones.id', 'like', '%' . $searchFilter . '%');
            }
            // zone name
            elseif ($selectSearch == 'NM') {
                return $query
                    ->where('shipping_zones.zone_name', 'like', '%' . $searchFilter . '%');
            }
            // Location type
            elseif ($selectSearch == 'LT') {
                return $query
                    ->Where('shipping_locations.location_type', 'like', '%' . $searchFilter . '%');
            }
            // location id
            elseif ($selectSearch == 'LI') {
                return $query
                    ->Where('shipping_locations.location_id', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'city') {
                return $query
                    ->join('shipping_locations', 'shipping_locations.zone_id', '=', 'shipping_zones.id')
                    ->join('global_cities', 'global_cities.id', '=', 'shipping_locations.location_id')
                    ->select('global_cities.*', 'shipping_zones.*')
                    ->orWhere('global_cities.city_name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'state') {
                return $query
                    ->join('shipping_locations', 'shipping_locations.zone_id', '=', 'shipping_zones.id')
                    ->join('global_states', 'global_states.id', '=', 'shipping_locations.location_id')
                    ->select('global_states.*', 'shipping_zones.*')
                    ->orWhere('global_states.name', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }
}
