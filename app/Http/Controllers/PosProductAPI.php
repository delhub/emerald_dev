<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Traits\PosProduct;

class PosProductAPI extends Controller
{
    use PosProduct;

    const APPID = 'Doqb6FDkLvCuqgF6S4kkimzaEPeS4eWs';
    const TOKEN = 'v5_LhY1hFP7I1RUWZjYR0EJ8zUuvw1lLCMNMGhJMm0eD9Y=';
    const AUTH = '5';

    public static $client;
    public static $apiServer;

    public static function apiServer()
    {
        return env('POS_API_SERVER', 'http://pos.test/api/v1');
    }

    //guzzle get api
    public static function guzzleGet($url) {
        $client = new Client();

        $response = $client->get($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }
    }

    //guzzle post api
    public static function guzzlePost($url,$data) {
        $client = new Client();

        $response = $client->post($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ],
            'form_params' => $data/* ,
            'http_errors' => false */
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }
    }

    //guzzle put api
    public static function guzzlePut($url,$data) {
        $client = new Client();

        $response = $client->put($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ],
            'json' => $data
            // 'http_errors' => false
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }
    }

    //guzzle delete api
    public static function guzzleDelete($url) {
        $client = new Client();

        $response = $client->delete($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }
    }

    //get all product
    public static function getAllProduct()
    {
        $url = self::apiServer() . '/product/all';
        $result = self::guzzleGet($url);

        return $result;
    }

    //get all product attribute
    public static function getAllProductAttribute()
    {
        $url = self::apiServer() . '/product/attribute';
        $result = self::guzzleGet($url);

        return $result;
    }

    //get all product price
    public static function getAllProductPrice()
    {
        $url = self::apiServer() . '/product/price';
        $result = self::guzzleGet($url);

        return $result;
    }

    //get all product images
    public static function getAllImages()
    {
        $url = self::apiServer() . '/product/images';
        $result = self::guzzleGet($url);

        return $result;
    }

    //get all product brand
    public static function getAllBrand()
    {
        $url = self::apiServer() . '/product/brand';
        $result = self::guzzleGet($url);

        return $result;
    }

    //get all product categories
    public static function getAllCategories()
    {
        $url = self::apiServer() . '/product/categories';
        $result = self::guzzleGet($url);

        return $result;
    }

    public static function synBrand() {
        $brand = self::getAllBrand();

        if(count($brand)) {
            foreach ($brand as $k => $v) {
                PosProduct::storeBrand($v);
            }
        }
    }

    public static function synCategory() {
        $category = self::getAllCategories();

        if(count($category)) {
            foreach ($category as $k => $v) {
                PosProduct::storeCategory($v);
            }
        }
    }

    public static function synProductPrice() {
        $price = self::getAllProductPrice();

        if(count($price)) {
            foreach ($price as $k => $v) {
                PosProduct::storePrice($v);
            }
        }
    }

    public static function storePosProduct() {
        $PosProduct = self::getAllProduct();

        if(count($PosProduct)) {
            foreach ($PosProduct as $k => $v) {
                PosProduct::storeProduct($v);
            }
        }

        return true;
    }

    public static function synAll() {
        self::storePosProduct();
        self::synBrand();
        self::synCategory();
        self::synProductPrice();

    }







}
