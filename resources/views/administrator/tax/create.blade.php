@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/tax" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                
                <div class="col-12">
                    <h4>
                        Create New Tax
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.tax.store') }}" id="edit-tax-form" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            
                            <select name="country_id" id="country_id" class="select2 form-control" style="width: 100%;">
                                <option value="default">Select Country..</option> }}
                                @foreach ($country as $val)
                                <option value="{{$val['country_id']}}">{{$val['country_name']}}</option> 
                                @endforeach                                   
                            </select>
                            
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>          


                        
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Tax Name <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="tax_name" name="tax_name" class="form-control" placeholder="Tax Name">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Tax Percentage <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="tax_percentage" name="tax_percentage" class="form-control" placeholder="Tax Percentage %">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Tax Registration Number <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="tax_reg_number" name="tax_reg_number" class="form-control" placeholder="Tax Registration Number">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection
