@if($article)
@foreach($mainCtgrSubtit as $categories)
<div class="tab-pane fade show {{ ($loop->first == 'true') ? 'active' : '' }}" id="article-{{$categories->slug}}" role="tabpanel" aria-labelledby="{{$categories->slug}}">
    <div class="user-bg">
        <div class="row multi-inner">
            <div class="col-md-12">

                <nav>
                    <div class="multi-subinfo nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-adult-tab" data-bs-toggle="tab" data-bs-target="#nav-adult-{{$categories->slug}}" type="button" role="tab" aria-controls="nav-adult" aria-selected="true">
                            <img src="{{asset('/images/icons/icon-adult.png')}}" alt="">
                            <p>Adult</p>
                            <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                        </button>

                        <button class="nav-link" id="nav-elderly-tab" data-bs-toggle="tab" data-bs-target="#nav-elderly-{{$categories->slug}}" type="button" role="tab" aria-controls="nav-elderly" aria-selected="false">
                            <img src="{{asset('/images/icons/icon-eldest.png')}}" alt="">
                            <p>Elderly</p>
                            <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                        </button>

                        <button class="nav-link" id="nav-kids-tab" data-bs-toggle="tab" data-bs-target="#nav-kids-{{$categories->slug}}" type="button" role="tab" aria-controls="nav-kids" aria-selected="false">
                            <img src="{{asset('/images/icons/icon-kids.png')}}" alt="">
                            <p>Kids</p>
                            <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                        </button>
                    </div>
                </nav>

                <div class="row">
                    <div class="col-12">
                        <h1 class="my-mb-1 text-center">
                            {{$article->title}}
                            <p>{{$categories->name}}</p>
                            <div class="info-hrline-red"></div>
                        </h1>
                        <div class="inner tab-content  height-limit-scroll" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-adult-{{$categories->slug}}" role="tabpanel" aria-labelledby="nav-adult-tab">
                                <div class="row">
                                    {!! $article->data !!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-elderly-{{$categories->slug}}" role="tabpanel" aria-labelledby="nav-elderly-tab">
                                <div class="row">
                                    {!! $article->data !!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-kids-{{$categories->slug}}" role="tabpanel" aria-labelledby="nav-kids-tab">
                                <div class="row">
                                    {!! $article->data !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif
