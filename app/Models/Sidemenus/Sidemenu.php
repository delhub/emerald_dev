<?php

namespace App\Models\Sidemenus;

use Illuminate\Database\Eloquent\Model;

class Sidemenu extends Model
{
    // Table Name
    protected $table = 'menus';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    //make menu child
    protected $fillable = ['menu_title','menu_parent'];

    public function childs() {
        return $this->hasMany('App\Models\Sidemenus\Sidemenu','menu_parent','id') ;
    }
    
    
}
