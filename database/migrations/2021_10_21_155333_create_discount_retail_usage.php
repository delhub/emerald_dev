<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountRetailUsage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_retail_usage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('outlet_id')->default(0);
			$table->string('order_number', 128)->nullable();
			$table->string('coupon_code', 24)->nullable();
			$table->bigInteger('customer_id')->default(0);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_retail_usage');
    }
}
