<?php

Route::group(['prefix' => 'csv'], function () {
    Route::get('/', 'Administrator\AdministratorController@index')->name('csv.index');
    Route::get('/main', 'API\Export\ExportController@index')->name('csv.main.list');
    Route::get('/agents/{country_id}', 'API\Export\ExportController@agent_csv')->name('csv.agent');
    Route::get('/customers/{country_id}', 'API\Export\ExportController@customer_csv')->name('csv.customer');
    Route::get('/products/{country_id}', 'API\Export\ExportController@product_csv')->name('csv.products');
    Route::get('/orders/{country_id}', 'API\Export\ExportController@order_csv')->name('csv.orders');
    Route::get('/payment/{country_id}', 'API\Export\ExportController@payment_csv')->name('csv.payments');
    Route::get('/admin/{report_name}', 'API\Export\ExportController@admin_report_csv')->name('csv.adminreport');
});



