<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Users\UserInfo;
use App\Models\Users\User;

class CheckCustomerUpline implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $customer =  $this->user;

        if (!isset($customer) || isset($customer->dealerInfo))  return true;

        return (isset($customer->userInfo) && $customer->userInfo->referrer_id == $value) ?  true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Referral ID different with customer record';
    }
}
