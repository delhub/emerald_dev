<?php

namespace App\Mail\Purchase;

use App\Models\Purchases\Purchase;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceAndReceiptEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $purchase;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Purchase $purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Formula Healthcare Order Confirmation - ' . $this->purchase->getFormattedNumber())
            ->view('emails.purchases.invoice-receipt-email')
            ->with('purchase', $this->purchase)
            ->attach(public_path(
                '/storage/documents/invoice/' . $this->purchase->getFormattedNumber() . (($this->purchase->invoice_version != 0) ? '/v'.$this->purchase->invoice_version.'/' : '/') . $this->purchase->getFormattedNumber() . '.pdf'
            ))
            ->attach(public_path(
                '/storage/documents/invoice/' . $this->purchase->getFormattedNumber() . (($this->purchase->invoice_version != 0) ? '/v'.$this->purchase->invoice_version.'/' : '/') . $this->purchase->getFormattedNumber() . '-receipt.pdf'
            ));
    }
}
