{!! $contentHtml !!}

<style>
    body {
        max-width: 1000px;
        margin: 0 auto !important;
    }
    .mytable2, .mytable3 {
        width: 100%;
        padding: 0px;
    }
    .mytable3 {
        margin-bottom: 0px;
    }
    .top-header-section {
        display: none
    }
    .tot-col {
        width: 1000px
    }
    td.company-name {
        display: none;
    }
    .logo-head {
        display: none;
    }
    td.invoice-dt h1{
        text-align: right !important;
        margin-bottom: 20px;
    }
</style>
