@extends('layouts.management.main-customer')

@section('content')

@section('page_title')
    {{ 'Promotion and Special Offer' }}
@endsection

<div class="fml-container">
    <div class="row">
        <div>
            <h4>Postpaid Method - Total {{count($ForCount)}}</h4>
        </div>
    </div>
    <div class="rbs-system">
        <table class="datatables-demo table blue table-striped">
            <thead>
                <tr class="tit-style">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-sort="ascending" aria-label=" Rendering engine: activate to sort column descending"
                        style="width: 60px;">No.
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="Browser: activate to sort column ascending" style="width: 150px;">
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="Platform(s): activate to sort column ascending" style="width: 246px;">Product Name
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="Engine version: activate to sort column ascending" style="width: 170px;">Reminder
                        Start
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 140px;">Reminder Month
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 140px;">Reminder Status
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 173px;">Action
                    </th>

                    {{-- <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 104px;">Delete Reminder
                    </th> --}}
                </tr>
            </thead>
            <tbody class="p-list">

                @foreach ($RbsPostpaid as $postpaid)
                    @php
                        $categorySlug = $postpaid->atrributeProduct->product2->parentProduct->categories->where('parent_category_id',0)->where('type','shop')->first()->slug;
                        $slug = $categorySlug ?? '';
                    @endphp

                    <tr class="gradeA odd" role="row" id="row{{ $postpaid->id }}">
                        <td>{{$loop->iteration}}</td>
                        <td>
                            <div class="a-product">
                                @if ($postpaid->atrributeProduct->product2->parentProduct->defaultImage)
                                    <img class="vr-p-img"
                                    src="{{ asset('storage/' . $postpaid->atrributeProduct->product2->parentProduct->images[0]->path . $postpaid->atrributeProduct->product2->parentProduct->images[0]->filename) }}"/>
                                @else
                                    <img class="imgproduct" src="{{asset('/images/product_item/default-p.png')}}" alt="">
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="title">

                                <a class="ordername text-capitalize pb-3"
                                href="@if ($postpaid->atrributeProduct->product2->parentProduct->product_status == 1)/shop/product/{{$slug}}/{{ $postpaid->atrributeProduct->product2->parentProduct->name_slug}}?panel={{$postpaid->atrributeProduct->product2->panel_account_id}}@else #row{{ $loop->iteration }} @endif">
                                    <h3>{{ $postpaid->atrributeProduct->product2->parentProduct->name }}</h3>
                                </a>
                                <p>{{$postpaid->atrributeProduct->attribute_name}}</p>
                                {{-- @if(array_key_exists('product_size', $postpaid->atrributeProduct->product2->product_information))
                                    <p>({{ $postpaid->atrributeProduct->product2->product_information['product_size'] }})</p>
                                @endif
                                @if(array_key_exists('product_rbs', $postpaid->atrributeProduct->product2->product_information))
                                    <p>({{ $postpaid->atrributeProduct->product2->product_information['product_rbs'] }})</p>
                                @endif --}}
                                @if ($postpaid->atrributeProduct->product2->parentProduct->product_status == 2)
                                    <p style="font-size: 11px" class="text-left text-danger">Product no longer available,<br>and email will not be sending out</p>
                                @endif
                            </div>
                        </td>
                        <td class="center">
                            <div class="date-num">
                                {{date( 'd/m/Y',strtotime($postpaid->start_reminder))}}
                            </div>
                        </td>


                            <td>
                                <form id="formUpdateMonths">
                                    @csrf
                                <div class="rbs-selectbox">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <select id="rbs_month" name="rbs_month" class="form-control">
                                        @foreach ($globalRbs as $rbs)
                                            <option value="{{$rbs->rbs_data}}" {{ $postpaid->months_id == $rbs->id ? 'selected' : '' }}>{{$rbs->rbs_label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <a href="#" class="">
                                    <div class="add-btn mt-2">
                                        <button type="button" class="btn btn-red1" onclick="ajaxPostpaid('updateMonth',{{ $postpaid->id }})">Submit <div class="spinner-border spinner-border-sm text-white pl-2 updateMonthLoader{{ $postpaid->id }}" role="status" style="display: none;">
                                            <span class="sr-only">Loading...</span>
                                            </div></button>
                                    </div>
                                </a>
                                </form>
                            </td>

                            <td>
                                @switch($postpaid->active)
                                    @case(0)
                                        Cancelled
                                        <br>
                                        {{ date('d/m/Y',strtotime($postpaid->cancel_date)) }}
                                        @break
                                    @case(1)
                                        Active
                                        @break
                                @endswitch
                            </td>

                        <td class="center text-center">
                            @if ($postpaid->active == 0)
                            <div class="icon-checkout-rbs" id="deleteReminder{{ $postpaid->id }}">
                                <a onclick="ajaxPostpaid('delete',{{ $postpaid->id }})" id="DeleteCartItem" class="link-product">
                                    <i class="fa fa-trash icon-chekout-1" aria-hidden="true"></i>
                                </a>
                            </div>
                            @else
                            <a href="#">
                                <div class="rbs-disable">
                                    <button type="button" class="btn rbs-grey-btn text-white" id="cancelReminder{{ $postpaid->id }}" onclick="ajaxPostpaid('cancel',{{ $postpaid->id }})"> Cancel </span>
                                        <div class="spinner-border spinner-border-sm text-primary cancelLoader{{ $postpaid->id }}" role="status" style="display: none;">
                                        <span class="sr-only">Loading...</span>
                                        </div>
                                    </button>
                                </div>
                            </a>
                            @endif


                            </td>
                        <td>

                        </td>
                    </tr>

                @endforeach
                {!! $RbsPostpaid->render() !!}
            </tbody>
        </table>
    </div>
</div>


@endsection

@push('script')
<script>

    function ajaxPostpaid(process,id){
        console.log(process,id);

        var formData = 0;

        if (process == 'updateMonth') {
            formData = $('#formUpdateMonths').serialize();
        }

        $.ajax({
            beforeSend: function() {
                $('.'+process+'Loader'+id).show();
            },
            complete: function() {
                //
            },
            url: "{{ route('shop.rbs.product-tracking-reminder-update') }}",
            type: "POST",
            data: {
                _token: "{{ csrf_token() }}",
                process: process,
                id: id,
                data: formData,
            },
            success: function(result) {

                location.reload();
            },
            error: function(result) {
                // label.html('error');
            }
        });
    }

</script>
@endpush
