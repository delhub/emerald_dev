<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\Emails\SendInvoiceAndReceiptEmail;
use App\Jobs\Emails\SendPurchaseOrderEmail;
use App\Models\Globals\Cards\Issuer;
use App\Models\Globals\Employment;
use App\Models\Globals\Gender;
use App\Models\Globals\Marital;
use App\Models\Users\User;
use App\Models\Globals\PaymentGateway\MerchantID;
use App\Models\Globals\Race;
use App\Models\Globals\State;
use App\Models\Globals\BankName;
use App\Models\Purchases\Order;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use App\Models\Users\Customers\PaymentInfo;
use App\Models\Users\Dealers\DealerAddress;
use App\Models\Users\Dealers\DealerContact;
use App\Models\Users\Dealers\DealerEmployment;
use App\Models\Users\Dealers\DealerBankDetails;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\Dealers\DealerSpouse;
use App\Models\Users\Dealers\DealerBank;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use App\Models\Users\UserInfo;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Auth;
use File;
use PDF;
use App\Http\Controllers\BujishuAPI;
use App\Models\Dealers\DealerGroup;
use Illuminate\Support\Facades\Cookie;


class DealerRegisterController extends Controller
{
    /**
     * Show registration form.
     */
    public function showFirstRegistrationForm()
    {
        // return $this->pageDisabled(); // TODO: Temporary disabled.
        return view('auth.register.v1.dealer');
    }

    /**
     * Handles first step of dealer registration.
     */
    public function dealerRegister(Request $request)
    {
        // return $request;

        $validatedData = $request->validate([
            'email' => ['required', 'unique:users', 'max:255'],
            'password' => ['required', 'min:8'],
            'introducer_name' => ['required', 'max:255'],
            'agent_group' => ['required'],
            'package' => ['required'],
            'paymentType' => ['required'],
            'payment_proof' => ['required', 'max:100000', 'mimes:jpeg,png,jpg,gif,svg']
        ]);

        // Users table.
        $user = new User();
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        $credentials = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        );

        Auth::attempt($credentials);

        // Generating new customer account id.
        $largestCustomerId = 0;
        if (UserInfo::all()->count() == 0) {
            $largestCustomerId = 1913000101;
        } else {
            $largestCustomerId = UserInfo::largestCustomerId() + 1;
        }

        // Generating new dealer account id.
        $largestDealerId = 0;
        if (DealerInfo::all()->count() == 0) {
            $largestDealerId = 1911000101;
        } else {
            $largestDealerId = DealerInfo::largestDealerId() + 1;
        }

        $paymentAmount = 0;
        $bluePoint = 0;

        if ($request->input('package') == 1) {
            $paymentAmount = 609000;
            $bluePoint = 1000;
        } elseif ($request->input('package') == 2) {
            $paymentAmount = 609000;
            $bluePoint = 3000;
        } elseif ($request->input('package') == 3) {
            $paymentAmount = 709000;
            $bluePoint = 7000;
        } elseif ($request->input('package') == 4) {
            $paymentAmount = 1709000;
            $bluePoint = 17000;
        } elseif ($request->input('package') == 5) {
            $paymentAmount = 3809000;
            $bluePoint = 38000;
        } elseif ($request->input('package') == 10) {
            $paymentAmount = 10;
            $bluePoint = 10;
        } else {
            abort(500);
        }

        // User_infos table.
        $userInfo = new UserInfo;
        $userInfo->user_id = $user->id;
        $userInfo->account_status = 3;
        $userInfo->account_id = $largestCustomerId;
        $userInfo->referrer_id = $largestDealerId;
        // $userInfo->blue_points = $bluePoint;
        $userInfo->save();

        // Dealer_address table. (Shipping Address)
        $userAddressBilling = new UserAddress;
        $userAddressBilling->account_id = $userInfo->account_id;
        $userAddressBilling->state_id = 0;
        $userAddressBilling->is_shipping_address = 1;
        $userAddressBilling->is_residential_address = 0;
        $userAddressBilling->is_mailing_address = 0;
        $userAddressBilling->save();

        // Dealer_address table. (Mailing Address)
        $userAddressMailing = new UserAddress;
        $userAddressMailing->account_id = $userInfo->account_id;
        $userAddressMailing->state_id = 0;
        $userAddressMailing->is_shipping_address = 0;
        $userAddressMailing->is_residential_address = 0;
        $userAddressMailing->is_mailing_address = 1;
        $userAddressMailing->save();

        // User_contacts table (Mobile).
        $userContactMobile = new UserContact;
        $userContactMobile->account_id = $userInfo->account_id;
        $userContactMobile->is_mobile = 1;
        $userContactMobile->save();

        // User_contacts table (Home).
        $userContactHome = new UserContact;
        $userContactHome->account_id = $userInfo->account_id;
        $userContactHome->is_home = 1;
        $userContactHome->save();

        // Dealer_infos table.
        $dealerInfo = new DealerInfo;
        $dealerInfo->user_id = $user->id;
        $dealerInfo->account_id = $largestDealerId;
        $dealerInfo->account_status = 9;
        $dealerInfo->gender_id = 0;
        $dealerInfo->race_id = 0;
        $dealerInfo->marital_id = 0;
        $dealerInfo->referrer_id = ($request->input('introducer_id') != null) ? $request->input('introducer_id') : 1010;
        $dealerInfo->referrer_name = $request->input('introducer_name');
        $dealerInfo->group_id = $request->input('agent_group');
        $dealerInfo->package_type = $request->input('package');
        $dealerInfo->save();

        // Dealer_address table. (Shipping Address)
        $dealerAddressShipping = new DealerAddress;
        $dealerAddressShipping->account_id = $dealerInfo->account_id;
        $dealerAddressShipping->state_id = 0;
        $dealerAddressShipping->is_shipping_address = 1;
        $dealerAddressShipping->is_residential_address = 0;
        $dealerAddressShipping->is_mailing_address = 0;
        $dealerAddressShipping->save();

        // Dealer_address table. (Mailing Address)
        $dealerAddressMailing = new DealerAddress;
        $dealerAddressMailing->account_id = $dealerInfo->account_id;
        $dealerAddressMailing->state_id = 0;
        $dealerAddressMailing->is_shipping_address = 0;
        $dealerAddressMailing->is_residential_address = 0;
        $dealerAddressMailing->is_mailing_address = 1;
        $dealerAddressMailing->save();

        // Dealer_contacts table (Mobile).
        $dealerContactMobile = new DealerContact;
        $dealerContactMobile->account_id = $largestDealerId;
        $dealerContactMobile->is_mobile = 1;
        $dealerContactMobile->save();

        // Dealer_contacts table (Home).
        $dealerContactHome = new DealerContact;
        $dealerContactHome->account_id = $largestDealerId;
        $dealerContactHome->is_home = 1;
        $dealerContactHome->save();

        // Dealer_spouse table.
        $dealerSpouse = new DealerSpouse;
        $dealerSpouse->account_id = $largestDealerId;
        $dealerSpouse->save();

        // Dealer_employment table
        $dealerEmployment = new DealerEmployment;
        $dealerEmployment->account_id = $largestDealerId;
        $dealerEmployment->company_state_id = 0;
        $dealerEmployment->save();

        $user->assignRole('customer');
        $user->assignRole('dealer');

        // Get latest invoice number sequence.
        $invoiceSequence = Purchase::largestPurchaseNumber();
        $invoiceSequence++;

        // Create a new purchase record.
        $purchase = new Purchase;
        // Assign user to the purchase record.
        $purchase->user_id = $user->id;
        // Generate a unique number used to identify the purchase.
        $purchase->purchase_number = $invoiceSequence;
        $purchase->purchase_type = $request->input('payment_type');

        // Assign a status to the purchase. Unpaid, paid.
        $purchase->purchase_status = 3000;
        $purchase->invoice_version = 1;
        // Assign the current date to the purchase in the form of DD/MM/YYYY.
        $purchase->purchase_date = Carbon::now()->format('d/m/Y');
        // Calculate total price of items in cart.
        $purchase->purchase_amount = $paymentAmount;
        // Generate unique number for receipt.
        $receiptSequence = Purchase::largestReceiptNumber();
        $receiptSequence++;
        $purchase->receipt_number = $receiptSequence;
        $purchase->save();

        // Generate unique number for PO.
        $poSequence = Order::all()->count() + 1;

        $order = new Order;
        $order->order_number = 'PO' . Carbon::now()->format('Y') . Carbon::now()->format('m') . '-' . str_pad($poSequence, 6, "0", STR_PAD_LEFT);
        $order->purchase_id = $purchase->id;
        $order->panel_id = '1918000101';
        $order->order_status = 1000;
        $order->order_amount = $paymentAmount;
        $order->delivery_date = 'Pending';
        $order->received_date = '';
        $order->claim_status = '';
        $order->save();

        if (Auth::attempt(['email' => $user->email, 'password' => $user->password])) {
            return $user;
        }

        $paymentType = $request->input('paymentType');

        if ($paymentType == 'card') {
            // Get merchant id.
            $merchantId = MerchantID::where('card_type', $request->input('card_type'))->first();
            // Get card issuer info.
            $cardIssuer = Issuer::where('issuer_name', $request->input('card_type'))->first();
            // Get cvv
            $cvv2 = $request->input('cvv');
            // Assign response url.
            $responseUrl = URL::to('/register-dealer/payment/response');

            // Create payment info.
            $paymentInfo = new PaymentInfo;
            $paymentInfo->account_id = $user->email;
            $paymentInfo->card_number = $request->input('card_number');
            $paymentInfo->issuer_id = $cardIssuer->id;
            $paymentInfo->name_on_card = $user->email;
            $paymentInfo->expiry_date = $request->input('expiry_date');

            return view('auth.register.v1.payment.payment-gateway-post')
                ->with('purchase', $purchase)
                ->with('paymentInfo', $paymentInfo)
                ->with('cvv2', $cvv2)
                ->with('merchantId', $merchantId)
                ->with('responseUrl', $responseUrl);
            // Create users table record.
            // Redirect to payment gateway with url parameter in return address.
        }

        if ($paymentType == 'offline') {
            $dealerInfo = DealerInfo::find($user->id);
            $dealerInfo->account_status = 3;
            $dealerInfo->save();

            $file = $request->file('payment_proof');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $purchase->getFormattedNumber() . '-payment' . '.' . $fileExtension;
            $destinationPath =
                public_path('/storage/uploads/images/users/' . $purchase->user->userInfo->account_id . '/payments');

            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true);
            }
            $file->move($destinationPath, $fileName);

            $purchase->payment_proof =
                '/storage/uploads/images/users/' . $purchase->user->userInfo->account_id . '/payments' . '/' . $fileName;

            $purchase->purchase_amount = str_replace('.', '', $request->input('offline_amount'));
            $purchase->purchase_type = 'offline';
            $purchase->offline_reference = $request->input('offline_reference');
            $purchase->offline_payment_amount = $request->input('offline_amount');
            $purchase->purchase_status = 3003; // Pending Review - Offline
            $purchase->save();

            $purchaseNumberFormatted = $purchase->getFormattedNumber();

            // Admin is supposed to verify offline payment first before it is labeled success.
            // Temporary solution for deadlines on 17/04/2020.

            $payment = new Payment;
            $payment->purchase_number = $purchase->purchase_number;
            $payment->gateway_string_result = '-';
            $payment->gateway_response_code = '00';
            $payment->auth_code = $request->input('offline_reference');
            $payment->last_4_card_number = '-';
            $payment->expiry_date = '-';
            $payment->amount = $purchase->purchase_amount;
            $payment->gateway_eci = '-';
            $payment->gateway_security_key_res = '-';
            $payment->gateway_hash = '-';
            $payment->country_id = country()->country_id;
            $payment->save();

            $groupId = ($request->input('agent_group')) ? $request->input('agent_group') : 12;
            $groupSales = DealerGroup::find($groupId);
            $groupSales->sales_amount = $groupSales->sales_amount + $purchase->purchase_amount;
            $groupSales->save();

            foreach ($purchase->orders as $order) {
                // Generate PO PDF.
                // Generate PDF.
                $pdf = PDF::loadView('documents.order.dealer-register-purchase-order', compact('order'));
                // Get PDF content.
                $content = $pdf->download()->getOriginalContent();
                // Set path to store PDF file.
                $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/') . 'purchase-orders/');
                // Set PDF file name.
                $pdfName = $order->order_number;
                // Check if directory exist or not.
                if (!File::isDirectory($pdfDestination)) {
                    // If not exist, create the directory.
                    File::makeDirectory($pdfDestination, 0777, true);
                }
                // Place the PDF into directory.
                File::put($pdfDestination . $pdfName . '.pdf', $content);

                // Queue PO email to panels.
                SendPurchaseOrderEmail::dispatch($order->panel->company_email, $order);
            }

            // Generate Invoice PDF.
            // Generate PDF.
            $pdf = PDF::loadView('documents.purchase.dealer-register-invoice', compact('purchase'));
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/'));
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber();
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            // Generate Receipt PDF.
            // Generate PDF.
            $pdf = PDF::loadView('documents.receipt.dealer-registration-receipt', compact('purchase'));
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/'));
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber() . '-receipt';
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            // Queue Invoice email to customer.
            SendInvoiceAndReceiptEmail::dispatch($user->email, $purchase);

            $purchaseNumberFormatted = $purchase->getFormattedNumber();

            $dealerInfo->account_status = 3;
            $dealerInfo->save();

            // Redirect to filling in more info.
            $genders = Gender::all();
            $races = Race::all();
            $maritals = Marital::all();
            $states = State::all();
            $employments = Employment::all();
            $bankNames = BankName::where('type','agent')->where('country_id','MY')->get();
            return view('auth.register.v1.dealer-info')
                ->with('genders', $genders)
                ->with('races', $races)
                ->with('maritals', $maritals)
                ->with('states', $states)
                ->with('employments', $employments)
                ->with('bankNames', $bankNames)
                ->with('user', $user);
        }
    }

    public function dealerRegisterPaymentGatewayResponse(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $response = $request->input('response');

        if ($response == '00') {

            $purchase = Purchase::where('purchase_number', $request->input('invoiceNo'))->firstOrFail();
            $payment = new Payment;
            $payment->purchase_number = $purchase->purchase_number;
            $payment->gateway_string_result = $request->input('result');
            $payment->gateway_response_code = $request->input('response');
            $payment->auth_code = $request->input('authCode');
            $payment->last_4_card_number = $request->input('PAN');
            $payment->expiry_date = $request->input('expiryDate');
            $payment->amount = $request->input('amount');
            $payment->gateway_eci = $request->input('ECI');
            $payment->gateway_security_key_res = $request->input('securityKeyRes');
            $payment->gateway_hash = $request->input('hash');
            $payment->country_id = country()->country_id;
            $payment->save();

            $groupId = ($request->input('agent_group')) ? $request->input('agent_group') : 12;
            $groupSales = DealerGroup::find($groupId);
            $groupSales->sales_amount = $groupSales->sales_amount + $purchase->purchase_amount;
            $groupSales->save();

            foreach ($purchase->orders as $order) {
                $order->order_status = 1001;
                $order->save();

                // Generate PO PDF.
                // Generate PDF.
                $pdf = PDF::loadView('documents.order.dealer-register-purchase-order', compact('order'));
                // Get PDF content.
                $content = $pdf->download()->getOriginalContent();
                // Set path to store PDF file.
                $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/') . 'purchase-orders/');
                // Set PDF file name.
                $pdfName = $order->order_number;
                // Check if directory exist or not.
                if (!File::isDirectory($pdfDestination)) {
                    // If not exist, create the directory.
                    File::makeDirectory($pdfDestination, 0777, true);
                }
                // Place the PDF into directory.
                File::put($pdfDestination . $pdfName . '.pdf', $content);

                // Queue sending PO email.
                SendPurchaseOrderEmail::dispatch($order->panel->company_email, $order);
            }

            // Generate Invoice PDF.
            // Generate PDF.
            $pdf = PDF::loadView('documents.purchase.dealer-register-invoice', compact('purchase'));
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/'));
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber();
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            // Generate Receipt PDF.
            // Generate PDF.
            $pdf = PDF::loadView('documents.receipt.dealer-registration-receipt', compact('purchase'));
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/'));
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber() . '-receipt';
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            SendInvoiceAndReceiptEmail::dispatch($user->email, $purchase);

            $purchaseNumberFormatted = $purchase->getFormattedNumber();
            $purchase->purchase_type = 'card';
            $purchase->purchase_status = 3003;
            $purchase->save();

            $dealerInfo = $user->dealerInfo;
            $dealerInfo->account_status = 3;
            $dealerInfo->save();

            // Redirect to filling in more info.
            $genders = Gender::all();
            $races = Race::all();
            $maritals = Marital::all();
            $states = State::all();
            $employments = Employment::all();
            $bankNames = BankName::where('type','agent')->where('country_id','MY')->get();
            return view('auth.register.v1.dealer-info')
                ->with('genders', $genders)
                ->with('races', $races)
                ->with('maritals', $maritals)
                ->with('states', $states)
                ->with('employments', $employments)
                ->with('bankNames', $bankNames)
                ->with('user', $user);
        } else {
            // Error
            $errorMessage = $request->input('result');
            $purchase = Purchase::where('purchase_number', $request->input('invoiceNo'))->firstOrFail();

            return view('auth.register.v1.payment.failed')
                ->with('errorMessage', $errorMessage)
                ->with('purchase', $purchase);
        }
    }

    public function dealerRegisterOfflinePayment()
    {
        //
    }

    /**
     * Dealer information GET.
     */
    public function dealerInformationRegisterView()
    {
        $user = User::find(Auth::user()->id);

        // Redirect to filling in more info.
        $genders = Gender::all();
        $races = Race::all();
        $maritals = Marital::all();
        $states = State::all();
        $employments = Employment::all();
        $bankNames = BankName::where('type','agent')->where('country_id','MY')->get();
        return view('auth.register.v1.dealer-info')
            ->with('genders', $genders)
            ->with('races', $races)
            ->with('maritals', $maritals)
            ->with('states', $states)
            ->with('employments', $employments)
            ->with('bankNames', $bankNames)
            ->with('user', $user);
    }

    /**
     * Dealer information POST.
     */
    public function dealerInformationRegister(Request $request)
    {
        // return $request;


        $request->validate([
            'nric' => ['required', 'unique:dealer_infos'],

        ]);

        $user = User::find(Auth::user()->id);


        // User
        $userInfo = UserInfo::find($user->id);
        $userInfo->full_name = $request->input('full_name');
        $userInfo->nric = $request->input('nric');
        $userInfo->save();

        $userAddressBilling = $userInfo->shippingAddress;
        $userAddressBilling->address_1 = $request->input('address_1');
        $userAddressBilling->address_2 = $request->input('address_2');
        $userAddressBilling->address_3 = $request->input('address_3');
        $userAddressBilling->postcode = $request->input('postcode');
        $userAddressBilling->city = $request->input('city');
        $userAddressBilling->state_id = ($request->input('state')) ? $request->input('state') : 14;
        $userAddressBilling->save();

        $userAddressMailing = $userInfo->mailingAddress;
        $userAddressMailing->address_1 = $request->input('address_1');
        $userAddressMailing->address_2 = $request->input('address_2');
        $userAddressMailing->address_3 = $request->input('address_3');
        $userAddressMailing->postcode = $request->input('postcode');
        $userAddressMailing->city = $request->input('city');
        $userAddressMailing->state_id =  ($request->input('state')) ? $request->input('state') : 14;
        $userAddressMailing->save();

        if ($request->input('contact_number_home') != null) {
            $userContactHome = $userInfo->homeContact;
            $userContactHome->contact_num = $request->input('contact_number_home');
            $userContactHome->save();
        }

        $userContactMobile = $userInfo->mobileContact;
        $userContactMobile->contact_num = $request->input('contact_number_mobile');
        $userContactMobile->save();
        // End User

        // Dealer
        $dealerInfo = DealerInfo::find($user->id);
        $dealerInfo->full_name = $request->input('full_name');
        $dealerInfo->nric = $request->input('nric');
        $dealerInfo->date_of_birth = $request->input('date_of_birth');
        $dealerInfo->gender_id = $request->input('gender_id');
        $dealerInfo->race_id = $request->input('race_id');
        $dealerInfo->marital_id = $request->input('marital_id');
        $dealerInfo->save();

        $dealerAddressShipping = $dealerInfo->shippingAddress;
        $dealerAddressShipping->address_1 = $request->input('address_1');
        $dealerAddressShipping->address_2 = $request->input('address_2');
        $dealerAddressShipping->postcode = $request->input('postcode');
        $dealerAddressShipping->city = $request->input('city');
        $dealerAddressShipping->state_id =  ($request->input('state')) ? $request->input('state') : 14;
        $dealerAddressShipping->save();

        $dealerAddressMailing = $dealerInfo->billingAddress;
        $dealerAddressMailing->address_1 = $request->input('address_1');
        $dealerAddressMailing->address_2 = $request->input('address_2');
        $dealerAddressMailing->postcode = $request->input('postcode');
        $dealerAddressMailing->city = $request->input('city');
        $dealerAddressMailing->state_id =  ($request->input('state')) ? $request->input('state') : 14;
        $dealerAddressMailing->save();

        if ($request->input('contact_number_home')) {
            $dealerContactHome = $dealerInfo->dealerHomeContact;
            $dealerContactHome->contact_num = $request->input('contact_number_home');
            $dealerContactHome->save();
        }

        $dealerContactMobile = $dealerInfo->dealerMobileContact;
        $dealerContactMobile->contact_num = $request->input('contact_number_mobile');
        $dealerContactMobile->save();

        if ($dealerInfo->marital_id = 2) {
            $dealerSpouse = $dealerInfo->dealerSpouse;
            $dealerSpouse->spouse_name = $request->input('spouse_full_name');
            $dealerSpouse->spouse_nric = $request->input('spouse_nric');
            $dealerSpouse->spouse_date_of_birth = $request->input('spouse_date_of_birth');
            $dealerSpouse->spouse_occupation = $request->input('spouse_occupation');
            $dealerSpouse->spouse_contact_office = $request->input('spouse_contact_office');
            $dealerSpouse->spouse_contact_mobile = $request->input('spouse_contact_mobile');
            $dealerSpouse->spouse_email = $request->input('spouse_email');
            $dealerSpouse->save();
        }

        $dealerEmployment = $dealerInfo->employmentAddress;
        $dealerEmployment->employment_type = ($request->input('employment_id')) ? $request->input('employment_id') : 1;
        $dealerEmployment->company_name = $request->input('employment_name');
        $dealerEmployment->company_address_1 = $request->input('company_address_1');
        $dealerEmployment->company_address_2 = $request->input('company_address_2');
        $dealerEmployment->company_postcode = $request->input('company_postcode');
        $dealerEmployment->company_city = $request->input('company_city');
        $dealerEmployment->company_state_id =  ($request->input('company_state')) ? $request->input('company_state') : 14;
        $dealerEmployment->save();

        $dealer_bank_details = $dealerInfo->dealerBankDetails;

        if (!$dealer_bank_details) {
            $dealer_bank_details = new DealerBankDetails();
            $dealer_bank_details->account_id = $dealerInfo->account_id;
        }

        BujishuAPI::createUser($user->userInfo->account_id, $request->input('full_name'), $dealerInfo->referrer_id, $request->input('nric'), $user->email, 'BHA', $dealerInfo->account_id);

        $dealer_bank_details->bank_code = ($bank_Code = $request->input('bank_name')) ? $bank_Code : '';
        $dealer_bank_details->bank_acc = ($bank_acc = $request->input('bank_account_number')) ? $bank_acc : 0;
        $dealer_bank_details->bank_acc_name = ($bank_acc_name = $request->input('account_holder_name')) ? $bank_acc_name : "";
        $dealer_bank_details->account_regid = ($account_regid = $request->input('account_regid')) ? $account_regid : "";
        $dealer_bank_details->save();




        return redirect('/');
    }

    public function paymentTryAgain(Request $request)
    {
        $invoiceNo = $request->query('purchaseNumber');
        $tryAgain = array();

        if ($request->query('r') && $request->query('r') != '') {
            $tryAgain['reason'] = 'Record of Bujishu Agent registration payment cannot be found for this account.';
            $tryAgain['message'] = 'Please upload proof of payment to continue using Bujishu.';
        }

        return view('auth.register.v1.payment.try-again')
            ->with('invoiceNo', $invoiceNo)
            ->with('tryAgain', $tryAgain);
    }

    public function paymentTryAgainPost(Request $request)
    {
        $paymentType = $request->input('paymentType');

        $user = User::find(Auth::user()->id);

        $purchase = Purchase::where('purchase_number', $request->input('purchaseNumber'))->first();

        if (!$purchase) {
            $invoiceSequence = Purchase::largestPurchaseNumber();
            $invoiceSequence++;

            $purchase = new Purchase;
            $purchase->user_id = $user->id;
            $purchase->purchase_number = $invoiceSequence;
            $purchase->purchase_type = $request->input('payment_type');
            $purchase->purchase_status = 3000;
            $purchase->invoice_version = 1;
            $purchase->purchase_date = Carbon::now()->format('d/m/Y');
            $purchase->purchase_amount = 0;
            $receiptSequence = Purchase::largestReceiptNumber();
            $receiptSequence++;
            $purchase->receipt_number = $receiptSequence;
            $purchase->save();
        }

        if ($paymentType == 'card') {
            // Get merchant id.
            $merchantId = MerchantID::where('card_type', $request->input('card_type'))->first();
            // Get card issuer info.
            $cardIssuer = Issuer::where('issuer_name', $request->input('card_type'))->first();
            // Get cvv
            $cvv2 = $request->input('cvv');
            // Assign response url.
            $responseUrl = URL::to('/register-dealer/payment/response');

            // Create payment info.
            $paymentInfo = new PaymentInfo;
            $paymentInfo->account_id = $user->email;
            $paymentInfo->card_number = $request->input('card_number');
            $paymentInfo->issuer_id = $cardIssuer->id;
            $paymentInfo->name_on_card = $user->email;
            $paymentInfo->expiry_date = $request->input('expiry_date');

            return view('auth.register.v1.payment.payment-gateway-post')
                ->with('purchase', $purchase)
                ->with('paymentInfo', $paymentInfo)
                ->with('cvv2', $cvv2)
                ->with('merchantId', $merchantId)
                ->with('responseUrl', $responseUrl);
        }

        if ($paymentType == 'offline') {
            $file = $request->file('payment_proof');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $purchase->getFormattedNumber() . '-payment' . '.' . $fileExtension;
            $destinationPath =
                public_path('/storage/uploads/images/users/' . $purchase->user->userInfo->account_id . '/payments');

            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true);
            }
            $file->move($destinationPath, $fileName);

            $purchase->payment_proof =
                '/storage/uploads/images/users/' . $purchase->user->userInfo->account_id . '/payments' . '/' . $fileName;

            $purchase->purchase_amount = str_replace('.', '', $request->input('offline_amount'));
            $purchase->purchase_type = 'offline';
            $purchase->offline_reference = $request->input('offline_reference');
            $purchase->offline_payment_amount = $request->input('offline_amount');
            $purchase->purchase_status = 3003; // Pending Review - Offline
            $purchase->save();

            $purchaseNumberFormatted = $purchase->getFormattedNumber();

            // Admin is supposed to verify offline payment first before it is labeled success.
            // Temporary solution for deadlines on 17/04/2020.

            $payment = new Payment;
            $payment->purchase_number = $purchase->purchase_number;
            $payment->gateway_string_result = '-';
            $payment->gateway_response_code = '00';
            $payment->auth_code = $request->input('offline_reference');
            $payment->last_4_card_number = '-';
            $payment->expiry_date = '-';
            $payment->amount = $purchase->purchase_amount;
            $payment->gateway_eci = '-';
            $payment->gateway_security_key_res = '-';
            $payment->gateway_hash = '-';
            $payment->country_id = country()->country_id;
            $payment->save();

            foreach ($purchase->orders as $order) {
                // Generate PO PDF.
                // Generate PDF.
                $pdf = PDF::loadView('documents.order.dealer-register-purchase-order', compact('order'));
                // Get PDF content.
                $content = $pdf->download()->getOriginalContent();
                // Set path to store PDF file.
                $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/') . 'purchase-orders/');
                // Set PDF file name.
                $pdfName = $order->order_number;
                // Check if directory exist or not.
                if (!File::isDirectory($pdfDestination)) {
                    // If not exist, create the directory.
                    File::makeDirectory($pdfDestination, 0777, true);
                }
                // Place the PDF into directory.
                File::put($pdfDestination . $pdfName . '.pdf', $content);

                // Queue PO email to panels.
                SendPurchaseOrderEmail::dispatch($order->panel->company_email, $order);
            }

            // Generate Invoice PDF.
            // Generate PDF.
            $pdf = PDF::loadView('documents.purchase.dealer-register-invoice', compact('purchase'));
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/'));
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber();
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            // Generate Receipt PDF.
            // Generate PDF.
            $pdf = PDF::loadView('documents.receipt.dealer-registration-receipt', compact('purchase'));
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . (($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/'));
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber() . '-receipt';
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            // Queue Invoice email to customer.
            SendInvoiceAndReceiptEmail::dispatch($user->email, $purchase);

            // Update dealer account status.
            $dealerInfo = $user->dealerInfo;
            $dealerInfo->account_status = 3;
            $dealerInfo->save();

            return redirect('/');
        }
    }
}
