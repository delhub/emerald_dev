@extends('layouts.administrator.main')

@section('pageTitle', 'Edit Users')

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">

    {{-- <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light"> Users / </span> Edit
    </h4> --}}

    <div class="row">
        <div class="col-md-12">
          <div class="card mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Edit New User</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('warehouse.user.index') }}"> Back</a>
                        </div>
                    </div>
                </div>

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                  </div>
                @endif
                <form action="{{ route('warehouse.user.update',['id'=>$user->id]) }}" id="edit-courier-form" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name:</strong>
                                <input type="text" id="full_name" name="full_name" class="form-control" value="{{ $user->userInfo->full_name }}" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Email:</strong>
                                <input type="text" id="email" name="email" class="form-control" value="{{ $user->email }}" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Password:</strong>
                                <input type="password" id="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Password">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Confirm Password:</strong>
                                <input type="password" id="confirm-password" name="confirm-password" class="form-control" value="{{ old('confirm-password') }}" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Location:</strong>
                                <select name="location_id" id="location_id" class="form-control">
                                    @foreach ($locations as $location)
                                        <option value="{{$location->id}}" {{( in_array($location->id,$user->user_warehouse_location) ? 'selected':'')}}>{{$location->location_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Role:</strong> <small> (ctrl + right click to choose multiple)</small>
                                <select name="roles[]" id="roles" class="form-control" size="10" multiple>
                                    @foreach ($roles as $role)
                                        <option value="{{$role->name}}" {{( in_array($role->name,$userRole) ? 'selected':'')}}>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
            </div>
        </div>
    </div>
</div>
@endsection
