<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddPaymentTypeInCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->integer('payment_type')->default('7000')->after('disabled');
        });

        Schema::table('purchases', function (Blueprint $table) {
            $table->integer('payment_type')->default('7000')->after('purchase_type');
        });

        DB::table('global_statuses')->insert(
            array(
                [
                    'id' => '7000',
                    'name' => 'Full Cash',
                    'description' => 'Status for purchase(payment type)',
                    'status_type' => 'payment_type',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
                [
                    'id' => '7001',
                    'name' => 'Full Point',
                    'description' => 'Status for purchase(payment type)',
                    'status_type' => 'payment_type',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
                [
                    'id' => '7002',
                    'name' => 'Half Point',
                    'description' => 'Status for purchase(payment type)',
                    'status_type' => 'payment_type',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
            )
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            //
        });
    }
}
