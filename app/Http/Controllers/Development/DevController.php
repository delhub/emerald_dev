<?php

namespace App\Http\Controllers\Development;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\XilnexAPI;
use App\Http\Controllers\PoslajuAPI;
use App\Traits\Membership;
use App\Models\Users\User;

use Carbon\Carbon;

class DevController extends Controller
{
    use Membership;

    public function index(Request $request)
    {
        if (isset($request->run)) {
            switch ($request->run) {
                case 1:
                    $data = [
                        'content_description' => 71, // package content
                        'content_value' => 50.00, // RM 00.00
                        'receiver_name' => 'Harleigh Leech',
                        'receiver_phone' => '0123456789',
                        'receiver_email' => 'mafsa7@gmail.com',
                        'receiver_address_line_1' => 'No 32A-1B, Floor 1',
                        'receiver_address_line_2' => 'Jalan OKD 7',
                        'receiver_address_line_3' => 'Taman Pertama',
                        'receiver_address_line_4' => '',
                        'receiver_postcode' => 43000,
                    ];

                    // dd($data);
                    //$a = PoslajuAPI::create_shipment();

                    // $key = [$a->key];

                    /* $b = PoslajuAPI::checkout(['30613b3ae03af2a806700e82ddfc63cf']);

                    echo "<pre>";
                    print_r($b);
                    exit; */
                    $x = PoslajuAPI::generate_consignment_note($data);
                    // $x = PoslajuAPI::create_shipment($data);

                    dd($x);

                    // $c = PoslajuAPI::get_consignment_note(["DEMO89803662"]);

                    // echo $c;
                    break;
                case 2:
                    $me = PoslajuAPI::getMe();

                    dd($me);
                    break;
                case 3:
                    // $ms = $this::getMembershipSetting(6001);
                    $ms = $this->MembershipCron(5);

                    dd($ms);
                    break;

                case 4:
                    if (!isset($request->tracking_no)) {
                        dd('insert tracking_no &tracking_no=', $request->tracking_no);
                    }
                    $caseFour = PoslajuAPI::get_consignment_note($request->tracking_no);

                    dd($caseFour);
                    break;

                default:
                    // code block
            }
        }
    }
}
