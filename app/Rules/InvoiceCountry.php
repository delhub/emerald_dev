<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Registrations\Dealer\Registration;

class InvoiceCountry implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $registrationRecord = Registration::where('invoice_number', $value)->where('country_id', country()->country_id)->first();
        return ($registrationRecord == NULL) ?  false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invoice in not valid in this country';
    }
}
