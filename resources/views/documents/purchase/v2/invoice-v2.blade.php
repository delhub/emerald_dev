<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formula - Invoice {{ $purchase->purchase_number }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/a4-receipt-invoice.css') }}">
    @include('layouts.favicon')
</head>

<body>
    <div class="a4">
        <div class="header-invoice">
            <table>
                <tr width="100%">
                    <td width="30%">
                        <img class="logo-head" src="{{ asset('images/formula/post-img/logo.png') }}">
                    </td>
                    <td class="company-name" width="50%">

                        <table width="100%">
                            <tr>
                                <td class="header-add">
                                    <p>Formula Healthcare Sdn. Bhd.<br><span>201901009101 (1318429-V)</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="header-contact">
                                    <p>1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="header-contact">
                                    <p>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                </td>
                            </tr>
                            <tr class="company-details">
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="contact-info">
                                                    <p>
                                                        <img class="ci-width"
                                                            src="{{ asset('images/formula/post-img/do-c-icon1.png') }}">formula2u-cs@delhubdigital.com
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="contact-info">
                                                    <p>
                                                        <img class="ci-width"
                                                            src="{{ asset('images/formula/post-img/do-c-icon3.png') }}">formula2u.com
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%"></td>
                </tr>
            </table>
        </div>
        <div class="invoice-body">
            <div class="invoice-dt">
                <h1>INVOICE</h1>
            </div>
            <div class="invoice-dt-box">
                <table width="100%">
                    <tr>
                        <td width="30%">
                            <div width="100%">
                                <table width="100%">
                                    <tr>
                                        <td class="font-bold w-35">
                                                {{ isset($purchase->ship_full_name) ? $purchase->ship_full_name : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold w-35">
                                                {{ isset($purchase->ship_contact_num) ? $purchase->ship_contact_num : '' }}
                                        </td>
                                    </tr>
                                    <tr>
                                        @php
                                            $lines = [];

                                            // Row 1: Address 1 and Address 2
                                            $line1 = ucwords(rtrim($purchase->ship_address_1, ','));
                                            if (str_replace(',', '', $purchase->ship_address_2)) {
                                                $line1 .= ', ' . ucwords(rtrim($purchase->ship_address_2, ','));
                                            }
                                            $lines[] = $line1;

                                            // Row 2: Address 3
                                            if (str_replace(',', '', $purchase->ship_address_3)) {
                                                $lines[] = ucwords(rtrim($purchase->ship_address_3, ','));
                                            }

                                            // Row 3: Postcode and City
                                            $postcode = $purchase->ship_postcode ?? '';
                                            $city = $purchase->cityKey->city_name ?? $purchase->ship_city ?? '';
                                            if ($postcode || $city) {
                                                $lines[] = trim($postcode . ' ' . ucwords($city), ',');
                                            }

                                            // Row 4: State and "Malaysia"
                                            $state = $purchase->state->name ?? '';
                                            $lines[] = trim(ucwords($state), ',') . ', Malaysia.';
                                        @endphp

                                        <td class="invoice-add2" colspan="3">
                                            @foreach ($lines as $line)
                                                <p>{{ $line }}</p>
                                            @endforeach
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="35%" class="pd-side">
                            <div width="100%">
                                @if($purchase->orders[0]->outlet !== null)
                                    <table width="100%">
                                        <tr>
                                            <td class="font-bold w-35">
                                                    {{ isset($purchase->orders[0]->outlet->outlet_name) ? $purchase->orders[0]->outlet->outlet_name : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold w-35">

                                                    {{ isset($purchase->orders[0]->outlet->contact_number) ? $purchase->orders[0]->outlet->contact_number : '' }}

                                            </td>
                                        </tr>
                                        <tr>
                                            @php
                                                $lines = [];

                                                    if ($purchase->orders[0]->outlet->default_address) {
                                                        $lines[] = $purchase->orders[0]->outlet->default_address;
                                                    }
                                                    if ($purchase->orders[0]->outlet->postcode) {
                                                        $postcode = $purchase->orders[0]->outlet->postcode;
                                                    }

                                                    if ($purchase->orders[0]->outlet->city_key) {
                                                        $city = $purchase->orders[0]->outlet->city->city_name;
                                                    }

                                                    if ($purchase->orders[0]->outlet->city_key != null || $purchase->orders[0]->outlet->city_key) {
                                                        $lines[] = $postcode . ' ' . $city . ',';
                                                    }
                                                    if ($purchase->orders[0]->outlet->state->name) {
                                                        $lines[] = $purchase->orders[0]->outlet->state->name . ',' . ' Malaysia. ';
                                                    }

                                                $address = $lines;
                                            @endphp

                                            <td class="invoice-add2" colspan="3">
                                                @foreach ($lines as $line)
                                                    <p>{{ $line }}</p>
                                                @endforeach
                                            </td>
                                        </tr>
                                    </table>
                                @endif
                            </div>
                        </td>
                        <td class="w-32">
                            <div width="100%">
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td class="font-bold w-42">Invoice No.</td>
                                            <td class="w-1">:</td>
                                            <td>{{ $purchase->inv_number }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold w-42">Order No.</td>
                                            <td class="w-1">:</td>
                                            <td>{{ $purchase->getFormattedNumber() }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold w-42" width="42%">Purchase Date</td>
                                            <td class="w-1">:</td>
                                            <td>{{ $purchase->purchase_date }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold w-42">Agent Code</td>
                                            <td class="w-1">:</td>
                                            <td>{{ $purchase->user->userInfo->agentInformation ? $purchase->user->userInfo->agentInformation->account_id : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold w-42">Status</td>
                                            <td class="w-1">:</td>
                                            <td>{{ $purchase->user->userInfo->showUserLevel() }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-bold w-42">Delivery Method</td>
                                            <td class="w-1">:</td>
                                            <td>
                                                @if ($purchase->orders[0]->delivery_method == 'Self-pickup' && !empty($purchase->orders[0]->delivery_outlet))
                                                    {{ $purchase->orders->first()->delivery_method }}
                                                @else
                                                    Delivery By Company
                                                @endif
                                            </td>
                                        </tr>
                                        {{-- @if ($purchase->orders[0]->delivery_method == 'Self-pickup' && !empty($purchase->orders[0]->delivery_outlet))
                                            <tr>
                                                <td class="font-bold w-42">Store Location</td>
                                                <td class="w-1">:</td>
                                                <td>
                                                    {{ $purchase->orders[0]->outlet->outlet_name }}
                                                </td>
                                            </tr>
                                        @endif --}}
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="invoice-description">
            <table class="table-width">
                <thead class="dark-clr2">
                    <tr>
                        <th width="10%">NO.</th>
                        <th width="50%">Description</th>
                        <th width="10%">QTY</th>
                        <th width="15%">UNIT PRICE <br> (RM)</th>
                        <th width="15%">AMOUNT<br> (RM)</th>
                    </tr>
                </thead>
                <tbody>

                    @php
                        $no = 0;

                        $subTotal = 0;

                        foreach ($purchase->items->where('bundle_id', '0') as $item) {
                            $no++;

                            $size = null;

                            if (array_key_exists('product_size', $item->product_information)) {
                                $size = '<br>(Size:' . $item->product_information['product_size'] . ')';
                            }

                            if (array_key_exists('product_rbs_frequency', $item->product_information)) {
                                // Deliver 3 item(s) every 2 months for 3 times
                                $size = '<br><span style="color:red;">*</span> Deliver ' . $item->quantity . ' item(s) every ' . $item->product_information['product_rbs_frequency'] . ' month(s) for ' . $item->product_information['product_rbs_times'] . ' times.';
                            }

                            if (array_key_exists('product_rbs_id', $item->product_information)) {
                                // Deliver 3 item(s) every 2 months for 3 times
                                $size = '<br>(' . $item->product_information['product_rbs'] . ')';
                            }

                            $dateRefunded = null;
                            if ($purchase->getFeeDetails('refund_fee')) {
                                $dateRefunded = $purchase->getFeeDetails('refund_fee')->created_at->toDateTimeString();
                                $dateRefunded = date('d/m/Y', strtotime($dateRefunded));
                            }

                            echo '

                            <tr>
                                <td class="i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">' .
                                $no .
                                '</td>
                                <td class="table-line2">
                                    <div class="table-p-bold2">
                                        <h4 style="font-size:18px;line-height: 20px;font-weight:normal;">' .
                                $item->product_information['product_code'] .
                                ($item->item_order_status == 1011 ? '<small style="color:red;"> - Refunded (' . $dateRefunded . ')</small>' : '') .
                                '</h4>
                                    </div>
                                    <p style="font-size:14px;line-height: 18px;font-weight:normal;">' .
                                $item->product->parentProduct->name .
                                $size .
                                '</p>
                                    ';
                            if (!empty($item->subtotal_point)) {
                                echo '
                                            <div class="i-promo-tag">
                                                <p style="font-size:16px;line-height: 20px;font-weight:normal;">Point applied: ' .
                                    $item->subtotal_point .
                                    '</p>
                                            </div>
                                        ';
                            }
                            echo '

                                </td>
                                <td class="table-line2 i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">' .
                                ($item->quantity + $purchase->items->where('bundle_id', $item->id)->sum('quantity')) .
                                '</td>
                                <td class="table-line2 i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">';

                            if (array_key_exists('product_rbs_unit_price', $item->product_information)) {
                                echo number_format($item->product_information['product_rbs_unit_price'] / 100, 2);
                            } else {
                                echo number_format($item->unit_price / 100, 2);
                            }
                            echo '</td>
                                <td class="i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">' .
                                number_format($item->subtotal_price / 100, 2) .
                                '</td>
                            </tr>
                            ';

                            $subTotal += $item->subtotal_price;
                        }
                    @endphp

                    <tr class="table-line-top">
                        <td colspan="3" class="i-fw pay-receive">
                            <p>PAYMENT RECEIVED</p>
                        </td>
                        <td colspan="1" class="i-discount2 table-line2">
                            Sub Total
                        </td>
                        <td colspan="1" class="i-discount2 i-total">
                            {{ number_format($subTotal / 100, 2) }}
                        </td>
                    </tr>
                    {{-- @dd($purchase->getFeeDetails('rebate_fee')->count()) --}}
                    <tr height="2mm">
                        <td colspan="3"
                            rowspan="5 @if ($purchase->getFeeDetails('rebate_fee')) +{{ $purchase->getFeeDetails('rebate_fee')->count() }} @endif"
                            class="sub-table">
                            <table width="54%" class="pd-pay pay-method text-l i-table-grey">
                                <tr>
                                    <td width="35%">
                                        Payment Method
                                    </td>
                                    <td width="1%">:</td>
                                    <td width="35%">
                                        @if ($purchase->purchase_type == 'Metapoint')
                                            MES eVoucher Cash
                                        @else
                                            {{ $purchase->purchase_type }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        @if ($purchase->purchase_type == 'Metapoint')
                                            Voucher Code
                                        @else
                                            Reference Number
                                        @endif
                                    </td>
                                    <td>:</td>
                                    <td>{{ isset($purchase->successfulPayment->auth_code) ? $purchase->successfulPayment->auth_code : ''}}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Received In
                                    </td>
                                    <td>:</td>
                                    <td>PBB</td>
                                </tr>
                                <tr>
                                    <td>
                                        @if ($purchase->purchase_type == 'Metapoint')
                                            Voucher Amount
                                        @else
                                            Amount Paid
                                        @endif
                                    </td>
                                    <td>:</td>
                                    <td>{{ isset($purchase->successfulPayment->amount) ? number_format($purchase->successfulPayment->amount / 100, 2) : '0.00' }}</td>
                                </tr>
                            </table>
                            @if ($purchase->getFeeDetails('refund_fee'))
                                @php($refundDetails = json_decode($purchase->getFeeDetails('refund_fee')->shipping_information))
                                <br>
                                <p class="i-fw pay-receive" style="font-size:18px;margin-bottom:5px;">REFUND</p>
                                <table width="54%" class="pd-pay pay-method text-l i-table-grey">
                                    <tr>
                                        <td width="35%">
                                            Voucher Number
                                        </td>
                                        <td width="1%">:</td>
                                        <td width="35%">{{ substr_replace($refundDetails->voucher_code, '-', -4, -4) }}
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            Amount Refundable
                                        </td>
                                        <td>:</td>
                                        <td>{{ number_format($purchase->getFeeAmount('refund_fee') / 100, 2) }}</td>
                                    </tr>
                                </table>
                            @endif
                        </td>
                        {{-- <td colspan="2" class="i-fw table-line2 btable-line-top" style="font-size:18px;line-height: 20px;">
                                SST
                            </td>
                            <td class="i-total btable-line-top" style="font-size:18px;line-height: 20px;">
                                0.00
                            </td> --}}
                    </tr>
                    @foreach ($purchase->getFeeDetails('rebate_fee') as $rebate_fee)
                        <tr>
                            <td colspan="1" height="2mm" class="i-fw table-line2 btable-line-top"
                                style="font-size:18px;line-height: 20px;">
                                {{ $rebate_fee->name }}
                            </td>
                            <td class="i-discount2 i-total i-fw btable-line-top">
                                {{ number_format($rebate_fee->amount / 100, 2) }}
                            </td>
                        </tr>
                    @endforeach
                    @if ($purchase->getFeeAmount('shipping_fee'))
                        <tr>
                            <td colspan="1" height="2mm" class="i-fw table-line2 btable-line-top"
                                style="font-size:18px;line-height: 20px;">
                                Delivery Fee
                            </td>
                            <td class="i-discount2 i-total i-fw btable-line-top">
                                {{ number_format($purchase->getFeeAmount('shipping_fee') / 100, 2) }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="1" height="2mm" class="i-fw table-line2 btable-line-top pay-text">
                            Grand Total
                        </td>
                        <td colspan="1" class="i-total2 btable-line-top pay-text">
                            {{ number_format($purchase->purchase_amount / 100, 2) }}
                        </td>
                    </tr>
                    <tr class="btable-line-top">
                    </tr>
                    <tr class="btable-line-top">
                    </tr>
                    <tr>
                        <td class="i-info-btm" colspan="2">
                            The issuance of this invoice shall be deemed as your acceptance of the terms
                            and conditions of this Disclaimer.
                        </td>
                    </tr>
                </tbody>

            </table>
        </div>
        <div class="invoice-footer">
            <div class="footer-dp2">
                <p>This is computer generated invoice, no signature is required.</p>
            </div>
        </div>
    </div>
</body>

</html>
