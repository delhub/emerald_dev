@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">{{ $topLevelSlug }}</a></li>
            </ul>
        </div>
    </div>
    <section>

        <div class="fml-container">
            <div class="row">
                <div class="col-12">
                    <div class="multineed-header my-mb-3">
                        <h1>
                            Hot
                            <span>
                                Selections
                                <div class="info-hrline-red-multi"></div>
                            </span>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="row multineed-box">
                <div class="col-md-12 col-sm-12 multi-info">
                    <div class="r-p-list">
                        <div class="row">
                            @if (count($hotSelections) > 0)
                                @foreach ($hotSelections as $hotSelection)
                                    <div class="col-md-3 item414">
                                        <div class="product-item-list">
                                            <a
                                                href="/shop/product/{{ $topLevelSlug }}/{{ $hotSelection->name_slug }}">

                                                @if ($hotSelection->defaultImage)
                                                    <img src="{{ asset('storage/' . $hotSelection->defaultImage->path . $hotSelection->defaultImage->filename) }}"
                                                        alt="">
                                                @else
                                                    <img src="{{ asset('/images/product_item/default-p.png') }}"
                                                        alt="">
                                                @endif

                                                <h5>{{ $hotSelection->name }}</h5>
                                                <p>{{ country()->country_currency }}
                                                    {{ number_format($hotSelection->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                    </div>
                @else
                    <h3 class="my-mb-1 mt-3">
                        Coming Soon
                    </h3>
                    @endif

                </div>
            </div>
        </div>

        <div class="full-hrline-grey hr-margin2"></div>

        <div class="fml-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="multineed-header my-mb-2">
                        <h1>Recommendation
                            <span>Products
                                <div class="info-hrline-red-multi"></div>
                            </span>
                        </h1>
                    </div>

                    <nav>
                        <div class="multi-inner nav nav-tabs rp-opt-list" id="nav-tab" role="tablist">
                            <button class="mp-icon1 {{ $show == 1 ? 'active' : '' }}" id="nav-home-tab"
                                data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab"
                                aria-controls="nav-home" aria-selected="true">
                                <img class="multi-i-size" src="{{ asset('/images/icons/domestic-icon.png') }}"
                                    alt="">
                                <p>Domestic</p>
                                <div class="downarrow"><img src="{{ asset('/images/icons/multidownarrow.svg') }}"
                                        alt=""></div>
                            </button>

                            <button class="mp-icon1 {{ $show == 2 ? 'active' : '' }}" id="nav-profile-tab"
                                data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab"
                                aria-controls="nav-profile" aria-selected="false">
                                <img class="multi-i-size" src="{{ asset('/images/icons/import-icon.png') }}" alt="">
                                <p>Import</p>
                                <div class="downarrow"><img src="{{ asset('/images/icons/multidownarrow.svg') }}"
                                        alt=""></div>
                            </button>

                            <button class="mp-icon1 {{ $show == 3 ? 'active' : '' }}" id="nav-contact-tab"
                                data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab"
                                aria-controls="nav-contact" aria-selected="false">
                                <img class="multi-i-size" src="{{ asset('/images/icons/exclusive-icon.png') }}"
                                    alt="">
                                <p>Exclusive</p>
                                <div class="downarrow"><img src="{{ asset('/images/icons/multidownarrow.svg') }}"
                                        alt=""></div>
                            </button>
                        </div>
                    </nav>

                    <div class="row">
                        <div class="col-12">
                            <div class="multi inner tab-content height-limit-scroll" id="nav-tabContent"
                                style="border: 0px">
                                <div class="tab-pane fade {{ $show == 1 ? 'show active' : '' }}" id="nav-home"
                                    role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row">
                                        <div class="col-12 mult-info">
                                            <div class="r-p-list">
                                                <div class="row">
                                                    @if (count($domestic) > 0)
                                                        @foreach ($domestic->where('product_status', 1) as $product)
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a
                                                                        href="/shop/product/{{ $topLevelSlug }}/{{ $product->name_slug }}">
                                                                        @if ($product->defaultImage)
                                                                            <img src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                                                                alt="">
                                                                        @else
                                                                            <img src="{{ asset('/images/product_item/default-p.png') }}"
                                                                                alt="">
                                                                        @endif
                                                                        <h5>{{ $product->name }}</h5>
                                                                        <p>{{ country()->country_currency }}
                                                                            {{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                                                        </p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach

                                                    @else
                                                        <h3 class="my-mb-1 mt-3">
                                                            Coming Soon
                                                        </h3>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade {{ $show == 2 ? 'show active' : '' }}" id="nav-profile"
                                    role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="row">
                                        <div class="col-12 mult-info">
                                            <div class="r-p-list">
                                                <div class="row">
                                                    @if (count($import) > 0)
                                                        @foreach ($import->where('product_status', 1) as $product)
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a
                                                                        href="/shop/product/{{ $topLevelSlug }}/{{ $product->name_slug }}">
                                                                        @if ($product->defaultImage)
                                                                            <img src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                                                                alt="">
                                                                        @else
                                                                            <img src="{{ asset('/images/product_item/default-p.png') }}"
                                                                                alt="">
                                                                        @endif
                                                                        <h5>{{ $product->name }}</h5>
                                                                        <p>{{ country()->country_currency }}
                                                                            {{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                                                        </p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach

                                                    @else
                                                        <h3 class="my-mb-1 mt-3">
                                                            Coming Soon
                                                        </h3>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade {{ $show == 3 ? 'show active' : '' }}" id="nav-contact"
                                    role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="row">
                                        <div class="col-12 mult-info">
                                            <div class="r-p-list">
                                                <div class="row">
                                                    @if (count($exclusive) > 0)
                                                        @foreach ($exclusive->where('product_status', 1) as $product)
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a
                                                                        href="/shop/product/{{ $topLevelSlug }}/{{ $product->name_slug }}">
                                                                        @if ($product->defaultImage)
                                                                            <img src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                                                                alt="">
                                                                        @else
                                                                            <img src="{{ asset('/images/product_item/default-p.png') }}"
                                                                                alt="">
                                                                        @endif
                                                                        <h5>{{ $product->name }}</h5>
                                                                        <p>{{ country()->country_currency }}
                                                                            {{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                                                        </p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach

                                                    @else
                                                        <h3 class="my-mb-1 mt-3">
                                                            Coming Soon
                                                        </h3>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
@endsection


@push('script')
<script>
    $(document).ready(function() {
        // Setup ajax to include csrf token / including Like fucntion
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function toggleUpdateLike(obj) {

        let button = $(obj);
        let product_id = button.data('liked');
        let loader = button.find('.spinner-border');
        let label = button.find('.fa-heart');

        //let count = button.find('.liked-btn');
        $.ajax({
            async: true,
            beforeSend: function() {
                // Show loading spinner.
                // label.html( '');
                // count.val( '' );
                // loader.show();
                // console.log(count);

            },
            complete: function() {
                loader.hide();
            },
            url: '{{ route('shop.toggle-perfect-list') }}',
            type: "POST",
            data: {
                id: product_id
            },
            success: function(result) {
                label.toggleClass('fas');
                label.toggleClass('far');

                //button.data('test' ,result.data.likes);
                button.attr('data-count', result.data.likes);
            },
            error: function(result) {
                label.html('error');
            }
        });
    };
</script>

@endpush
