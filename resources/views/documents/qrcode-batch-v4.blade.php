<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $id }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <style>
        body {
            width: 100%;
            height: 100%;
            margin: 0 auto;
            padding: 0;
            font: 12pt "Tahoma";
        }

        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .page {
            /* width: 210mm;
            height: 297mm; */
            margin-right: -0.2%;
            padding: 4mm 0mm;
            page-break-after: always;
        }

        .my-style {
            background: rgb(255 255 255);
            height: 24.45mm;
            width: 33.07mm;
            /* border: 1px dotted #000000; */
            float: left;
        }

        .itemid {
            display: block;
            font-size: 8px;
            position: relative;
            top: -7px;
        }

        .start {
            display: block;
            font-size: 9px;
            position: relative;
            top: 0mm;
            margin: 16px auto;
        }
    </style>
</head>

<body>
    @foreach ($batchs as $val)
        @if ($loop->iteration % 120 == 1)
            <div class="page">
        @endif
        @if (is_string($val))
            <div class="my-style text-center">
                <span class="start one">
                    {{ $val }}<br>
                    {{ $val }}<br>
                    {{ $val }}<br>
                    {{ $val }}<br>
                </span>
            </div>
        @else
            <div class="my-style text-center">
                <img
                    src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size(75)->generate(Request::getHost() . '/qr/item/' . $val->items_id)) }} ">
                <span class="itemid">{{ $val->items_id }}</span>
            </div>
        @endif
        @if ($loop->iteration % 120 == 0)
            </div>
        @endif
    @endforeach
</body>

</html>
