@extends('layouts.management.main-agent')

@section('page_title')
    {{ 'Agent - Income Statements' }}
@endsection

@section('content')
<div class="fml-container">
    <div class="row">
        <div>
            <h4>Yearly Statement</h4>
        </div>
    </div>
</div>

    <div class="container-fluid">
        <div class="row myagentpage-row">
            <div class="ad-my-box col-md-12">
                <div class="d-style-box card">
                    <div class="card-body">
                        <div class="row">
                            <div class="card-title in-title">
                                <div class="yearly-title">
                                    <img class="img-i-icon"
                                        src="{{ asset('/images/icons/dashboard-icons/yearly-icon.png') }}" alt="">
                                    <h4><span class="first-year"></span> Yearly Income Statement</h4>
                                </div>
                                <div class="iselect-box">
                                    <select id="yearDropdown">
                                        @foreach ($yearly_data as $year)
                                            <option value='{{ $year }}'>{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4" id="yearData">
                            @foreach ($yearly_data as $year)
                            <div class="col-12 i-yearly-ml year-class" id="yearData_{{$year}}"">
                                <form id="logout-form" action="{{ route('agent.yearly-statement', ['year' => $year]) }}" method="POST" target="_blank" style="display:inline">
                                    @csrf
                                    <button type='submit' target="_blank" class="btn cv-reject-btn ibtn-mr i-f-size"
                                        data-original-title="" title=""> Download Yearly Statement {{$year}}</button>
                                </form>

                                &nbsp;
                                <form id="logout-form" action="{{ route('agent.cp58', ['year' => $year]) }}" method="POST" target="_blank" style="display:inline">
                                    @csrf
                                    <button type='submit' target="_blank" class="btn vr-filt-btn i-f-size"
                                        data-original-title="" title="">Download CP58 {{$year}}</button>
                                </form>
                            </div>
                            @endforeach
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
    <script>
    $(document).ready(function() {

        $('#yearDropdown').on('change', function() {
            selected = 'yearData_'+ $(this).val();
            console.log( selected );

            $('#yearData > .year-class').each(function(){
                this.style.display = (selected == this.id) ? "block" : "none";
            });

            selectedyear = $("#yearDropdown option:selected").val();
            $('.first-year').text(selectedyear);

        });
        $('#yearDropdown').trigger("change");
    });
    </script>
@endpush