@extends('layouts.administrator.main')

@section('pageTitle', 'Edit Users')

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4 mt-4">
    </h4>

    <div class="row">
        <div class="col-md-12">
          <div class="card mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb d-flex justify-content-between">
                        <div class="">
                            <h2>Edit Password/Roles</h2>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('administrator.user') }}"> Back</a>
                        </div>
                    </div>
                </div>

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                  </div>
                @endif

                <form method="POST" action="{{ route('administrator.users.update', ['id' => $user->id, 'form_type' => 'null']) }}">
                    @csrf
                    @method('POST') <!-- Update this if the route requires a specific method -->
                    
                    <div class="row">
                        <!-- Password -->
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Password:</strong>
                                <input type="password" name="password" placeholder="Password" class="form-control">
                            </div>
                        </div>

                        <!-- Confirm Password -->
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Confirm Password:</strong>
                                <input type="password" name="confirm-password" placeholder="Confirm Password" class="form-control">
                            </div>
                        </div>

                        <!-- Roles -->
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Role:</strong>
                                <select name="roles[]" class="multi" multiple>
                                    @foreach($roles as $role => $label)
                                        <option value="{{ $role }}" {{ in_array($role, $userRole) ? 'selected' : '' }}>{{ $label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- Settings -->
                        {{--@foreach ($settings as $name => $value)
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="{{ $name }}" name="{{ $name }}" 
                                        {{ ($user->userInfo->userData->where('type', 'SETTINGS')->where('name', $name)->first()->value ?? $value) == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="{{ $name }}">{{ ucfirst(str_replace('_', ' ', $name)) }}</label>
                                </div>
                            </div>
                        @endforeach--}}
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="allow_bypass" name="allow_bypass" @if($user->userInfo->acc_code == 'bypass') checked @endif>
                                <label class="form-check-label" for="allow_bypass">Allow Offline Payment</label>
                            </div>
                        </div>

                        <!-- Submit Button -->
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.css" rel="stylesheet"></link>
<style>
    .app-header .navbar-brand {
        height: 46px;
    }
</style>
@endpush

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.js"></script>

<script>
new SlimSelect({
  select: '.multi'
})
</script>
@endpush
