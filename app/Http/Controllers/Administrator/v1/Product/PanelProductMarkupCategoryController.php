<?php

namespace App\Http\Controllers\Administrator\v1\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductMarkupCategory;

class PanelProductMarkupCategoryController extends Controller
{
    public static $home;
    public static $header;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$home = 'administrator.products.v1.panel.markup-category.index';
        self::$header = 'Markup Category';

        self::$create = 'administrator.products.v1.panel.markup-category.create';
        self::$store = 'administrator.products.v1.panel.markup-category.store';

        self::$edit = 'administrator.products.v1.panel.markup-category.edit';
        self::$update = 'administrator.products.v1.panel.markup-category.update';

        self::$destroy = 'administrator.products.v1.panel.markup-category.destroy';

        // link to view page
        // view('administrator.products.v1.panel.markup-category.index');

    }


    public function index()
    {
        $tables = ProductMarkupCategory::orderBy('cat_name', 'ASC')->paginate(10);

        return view(self::$home, compact('tables'));
    }

    public function create()
    {
        return view(self::$create);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required',
            'markup_rate' => 'required',
        ]);

        $existingdata  = ProductMarkupCategory::where('cat_name', $request->input('cat_name'))->first();

        if (!$existingdata) {
            $data = new ProductMarkupCategory;
            $data->cat_name = $request->input('cat_name');
            $data->cat_desc = $request->input('cat_desc');
            $data->markup_rate = $request->input('markup_rate');

            $data->save();

            return redirect()->route(self::$home)->with('success', 'submit done.');
        } else {
            return back()->with(['error' => self::$header . ' already exist  !!! ']);
        }
    }

    public function edit($id)
    {
        $data = ProductMarkupCategory::find($id);

        return view(self::$edit, compact('data'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cat_name' => 'required',
            'markup_rate' => 'required',
        ]);

        $data = ProductMarkupCategory::find($id);
        $data->cat_name = $request->input('cat_name');
        $data->cat_desc = $request->input('cat_desc');
        $data->markup_rate = $request->input('markup_rate');

        $data->save();

        return redirect()->route(self::$home)->with('success', self::$header . ' Updates');
    }

    // public function destroy($id)
    // {
    //     $data = ProductMarkupCategory::find($id);

    //     $data->delete();

    //     return redirect()->route(self::$home)->with('success', self::$header . ' Remove');
    // }
}
