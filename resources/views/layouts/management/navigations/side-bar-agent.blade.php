@php

$menu = [
    'home-tab' => ['route_id' => 'agent.overview', 'icon' => '/images/icons/dashboard-icons/ad-overview-icon.png', 'title' => 'Overview'],
    'customer-tab' => ['route_id' => 'agent.customerVerification', 'icon' => '/images/icons/dashboard-icons/ad-customer-verif-icon.png', 'title' => 'Customer Verification'],
    // "income-tab" => array('route_id' => 'agent.incomestatement', "icon" => '/images/icons/dashboard-icons/ad-income-statement-icon.png', 'title' =>  'Income Statement'),
    'income-tab' => [
        'route_id' => 'agent.monthly.income',
        'icon' => '/images/icons/dashboard-icons/ad-income-statement-icon.png',
        'title' => 'Income',
        'childlist-one' => ['route_id' => 'agent.yearly.income', 'icon' => '/images/icons/dashboard-icons/yearly-icon.png', 'title' => 'Yearly Income'],
        'childlist-two' => ['route_id' => 'agent.monthly.income', 'icon' => '/images/icons/dashboard-icons/monthly-icon.png', 'title' => 'Monthly Income'],
    ],
    'membership-tab' => ['route_id' => 'agent.membership-registration', 'icon' => '/images/icons/dashboard-icons/ad-registraion-icon.png', 'title' => 'Membership Registration'],
    'announcements-tab' => ['route_id' => 'agent.announcements', 'icon' => '/images/icons/dashboard-icons/ad-annoucement-icon.png', 'title' => 'Annoucement'],
    'agent-package-product-tab' => ['route_id' => 'agent.agent.package.product.list', 'icon' => '/images/icons/dashboard-icons/ad-annoucement-icon.png', 'title' => 'Agent Package Product'],

    'virtual-quantity-stock' => ['route_id' => 'agent.virtual.quantity.stock', 'icon' => '/images/icons/dashboard-icons/ad-continue-shop-icon.png', 'title' => 'Virtual Quantity Stock(VQS)'],

    'ct-shop-tab' => ['route_id' => 'shop.index', 'icon' => '/images/icons/dashboard-icons/ad-continue-shop-icon.png', 'title' => 'Continue Shopping'],
];
// $incomeSub = count($menu[]);
// dd($incomeSub);
@endphp
<!-- 2021 -->
{{-- <div class="wrapper"> --}}
{{-- <div class="sidebar sidebar-bg-color h-100"> --}}
{{-- <div class="col-lg-3 ad-box"> --}}
<div class="flex-column my-do-text">
    <h4 class="my-nav ad-tittle">Agent Dashboard</h4>
</div>
<!-- Small modal -->

<div class="modal fade statementSubmenu" tabindex="-1" role="dialog" aria-labelledby="statementList" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="pop-i-menu modal-content">
            <button type="button" class="close i-back-btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @foreach ($menu as $key => $_menu1)
                @if ($key == 'income-tab')
                    {{-- <p>yes it is</p> --}}
                    <a @if ($_menu1['childlist-one']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                        href="{{ route($_menu1['childlist-one']['route_id']) }}">
                        <img src={{ asset($_menu1['childlist-one']['icon']) }}>
                        <p>{{ $_menu1['childlist-one']['title'] }}</p>
                    </a>
                    <a @if ($_menu1['childlist-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                        href="{{ route($_menu1['childlist-two']['route_id']) }}">
                        <img src={{ asset($_menu1['childlist-two']['icon']) }}>
                        <p>{{ $_menu1['childlist-two']['title'] }}</p>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</div>

<!-- Dynamic mobile menu start-->
<div id="ag-u-db-m" class="db-slide responsive ad slider">

    @foreach ($menu as $key => $_menu)
        <div class="" id="{{ $key }}">
            @if ($key == 'income-tab')
                <a @if ($_menu['route_id'] == Route::currentRouteName() || $_menu['childlist-one']['route_id'] == Route::currentRouteName() || $_menu['childlist-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif href="#"
                    data-toggle="modal" data-target=".statementSubmenu">
                    <img class="do-menuicon-m" src={{ asset($_menu['icon']) }}>
                    <p>{{ $_menu['title'] }}</p>
                </a>
            @else
                <a @if ($_menu['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                    href="{{ route($_menu['route_id']) }}">
                    <img class="do-menuicon-m" src={{ asset($_menu['icon']) }}>
                    <p>{{ $_menu['title'] }}</p>
                </a>
            @endif
        </div>
    @endforeach

</div>

<!-- Dynamic web menu start-->
<nav id="ag-u-db" class="agent navbar navbar-toggleable-md">
    <ul class="nav flex-column my-do-text nav-pills mb-3">
        @foreach ($menu as $key => $_menu)
            <li class="nav-item my-nav" id="{{ $key }}">
                @if ($key == 'income-tab')
                    {{-- <a @if ($_menu['route_id'] == Route::currentRouteName() || $_menu['childlist-one']['route_id'] == Route::currentRouteName() || $_menu['childlist-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                    href="{{ route($_menu['route_id']) }}" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> --}}
                    <a class="nav-link" href="{{ route($_menu['route_id']) }}" data-toggle="collapse"
                        data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <p>{{ $_menu['title'] }}</p>
                    </a>
                    <div @if ($_menu['childlist-one']['route_id'] == Route::currentRouteName() || $_menu['childlist-two']['route_id'] == Route::currentRouteName()) class="collapse navbar-collapse in show" @else  class="collapse navbar-collapse" @endif
                        id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a @if ($_menu['childlist-one']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                                    href="{{ route($_menu['childlist-one']['route_id']) }}">
                                    <p>{{ $_menu['childlist-one']['title'] }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a @if ($_menu['childlist-two']['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                                    href="{{ route($_menu['childlist-two']['route_id']) }}">
                                    <p>{{ $_menu['childlist-two']['title'] }}</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                @else
                    <a @if ($_menu['route_id'] == Route::currentRouteName()) class="nav-link active" @else class="nav-link" @endif
                        href="{{ route($_menu['route_id']) }}">
                        <p>{{ $_menu['title'] }}</p>
                    </a>
                @endif
            </li>
        @endforeach
    </ul>
    {{-- <a href="#" class="navbar-toggler navbar-toggler-right" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        sss
    </a>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link 1</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown link
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
      </div> --}}
</nav>

@php
use App\Models\Sidemenus\Sidemenu;
use App\Models\Globals\Countries;

$banner_county = 'MY';
$country = country()->country_id;

if ($country == 'SG') {
    $banner_county = 'SG';
}

$menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
$allMenus = Sidemenu::pluck('menu_title', 'id')->all();
@endphp
@foreach ($menus as $menu)
    @if ($menu->menu_group == 'UM' && $menu->menu_show == 1 && $menu->menu_parent == '0' && $menu->country_id == $banner_county)
        <li class="nav-item my-nav">
            {{-- <div class="icon-text-align"> --}}
            <a data-toggle="tab" class="nav-link" href="{{ $menu->menu_link }}">
                {{-- <img src="{{asset('/storage/customer/sidebar-icons/' . $menu->menu_icon)}}" alt="{{ $menu->menu_title }}" style="height: 100px; width: 100px;"> --}}
            </a>

            {{-- <ul class="list-unstyled components" style="padding: 0px 0;">
                  @if (count($menu->childs))
                        @include('layouts.shop.navigation.menusub',['childs' => $menu->childs])
                  @endif
                  </ul> --}}
            {{-- </div> --}}
        </li>
    @endif
@endforeach
@push('script')
    <script>
        $('.responsive.ad').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerPadding: '50px',
            autoplay: false,
            arrows: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        infinite: true,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        centerPadding: '100px',
                        slidesToScroll: 3,
                        arrows: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        centerPadding: '50px',
                        slidesToScroll: 3,
                        arrows: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    </script>
@endpush
