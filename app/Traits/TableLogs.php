<?php

namespace App\Traits;

use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait TableLogs
{
    // public $priceTypes = ['fixed_price', 'price', 'member_price', 'offer_price', 'point', 'point_cash', 'discount_percentage', 'product_sv'];
    // show price for product

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $logs_table = DB::table('wh_logs')->insert([
                'model_type' => self::class,
                'model_id' => $model->id,
                'log_status' => 'created',
                'log_user' => (User::find(Auth::user())) ? User::find(Auth::user()->id)->id : 0,
                'log_data' => json_encode($model),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        });

        self::updated(function ($model) {
            $logs_table = DB::table('wh_logs')->insert([
                'model_type' => self::class,
                'model_id' => $model->id,
                'log_status' => 'updated',
                'log_user' => (User::find(Auth::user())) ? User::find(Auth::user()->id)->id : 0,
                'log_data' => json_encode($model->getChanges()),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        });
    }
}
