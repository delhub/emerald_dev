<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductDataInAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->decimal('product_measurement_length', 11, 2)->default(0)->after('product_sv');
            $table->decimal('product_measurement_width', 11, 2)->default(0)->after('product_measurement_length');
            $table->decimal('product_measurement_depth', 11, 2)->default(0)->after('product_measurement_width');
            $table->decimal('product_measurement_weight', 11, 2)->default(0)->after('product_measurement_depth');
            $table->tinyInteger('active')->default(1)->after('product_measurement_weight');
        });

        Schema::table('global_products', function (Blueprint $table) {
            $table->text('warning')->nullable()->change();
            $table->text('caution')->nullable()->change();
        });

        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->tinyInteger('active')->default(1)->after('product_sv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attribute', function (Blueprint $table) {
            //
        });
    }
}
