<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\Products\ProductAttribute;
use Illuminate\Support\Carbon;

class UpdateShippingSpecial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('shipping_categories')->insert(
            array(
                array('cat_name' => 'NONE', 'cat_desc' => 'NONE', 'cat_type' => 'shipping_special', 'active' => '1', 'for_user' => '["6000","6001","6002"]', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );



        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $shipping = Ship_category::orderBy('created_at', 'DESC')->first();
            $id = $shipping->id;

            $table->bigInteger('shipping_category_special')->default($id)->after('shipping_category')->change();
        });



        $attribute = ProductAttribute::get();

        $shipping = Ship_category::orderBy('created_at', 'DESC')->first();

        foreach ($attribute as $key => $att) {
            $att->shipping_category_special = $shipping->id;
            $att->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
