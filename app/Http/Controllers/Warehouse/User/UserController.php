<?php

namespace App\Http\Controllers\Warehouse\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use App\Models\Outlet\Outlet;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use App\Models\Users\UserInfo;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use Illuminate\Support\Facades\Hash;
use App\Models\Users\UserCountry;
use App\Models\Warehouse\Location\Wh_location;
use Illuminate\Support\Facades\DB;
use App\Models\Warehouse\User\WarehouseUserLocation;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('userInfo')
            ->orderBy('email', 'asc')
            ->whereHas('roles', function (Builder $query) {
                $query->where('name', 'like', 'wh_%');
            })
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->warehouseLocationFilter($request->location_id);

        $users = $users->paginate(50);
        $users->appends(request()->query());

        $locations = Wh_location::orderBy('location_name', 'DESC')->whereNotIn('location_name', ['DUMMY'])->get();

        return view('warehouse.user.index', compact('request', 'users', 'locations'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name', 'LIKE', 'wh_%')->get();
        $locations = Wh_location::orderBy('id', 'DESC')->whereNotIn('location_name', ['DUMMY'])->get();

        return view('warehouse.user.create', compact('roles', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password|min:8',
            'location_id' => 'required',
            'roles' => 'required'
        ]);
        $input = $request->all();

        $user = $this->createCustomer($input);

        $user->assignRole($request->input('roles'));

        // foreach ($input['location_id'] as $key => $value) {
        $userLocation = new  WarehouseUserLocation;

        $userLocation->account_id = $user->userInfo->account_id;
        $userLocation->location_id = $input['location_id'];
        $userLocation->save();
        // }

        return redirect()->route('warehouse.user.index')
            ->with('success', 'User created successfully, Name : ' . $user->userInfo->full_name . ' --- Email Name : ' . $user->email);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('warehouse.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::orderBy('id')->where('name', 'LIKE', 'wh_%')->get();
        $userRole = $user->roles->pluck('name', 'name')->all();
        $locations = Wh_location::orderBy('id', 'DESC')->whereNotIn('location_name', ['DUMMY'])->get();

        return view('warehouse.user.edit', compact('user', 'locations', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            // 'roles' => 'required'
        ]);

        $input = $request->input();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);

        $userInfo = $user->userInfo;
        $userInfo->update($input);

        $userLocation = WarehouseUserLocation::firstOrCreate([
            'account_id' => $userInfo->account_id
        ]);
        $userLocation->update($input);

        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('warehouse.user.index')
            ->with('success', 'User updated successfully, Name : ' . $user->userInfo->full_name . ' --- Email Name : ' . $user->email);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     User::find($id)->delete();
    //     return redirect()->route('warehouse.user.index')
    //         ->with('success', 'User deleted successfully');
    // }

    // public static function createCustomer($role, $verify = true, $country_id, $no)
    public static function createCustomer($input)
    {
        $country_id = 'MY';

        // Users table.
        $user = new User;
        $user->password = Hash::make($input['password']);
        $user->email = $input['email'];

        $user->email_verified_at = Carbon::now();
        $user->save();

        // Generating new customer account id.
        $largestCustomerId = UserInfo::largestCustomerId() + 1;

        // User_infos table.
        $userInfo = new UserInfo;
        $userInfo->user_id = $user->id;
        $userInfo->account_id = $largestCustomerId;
        $userInfo->user_level = 6001;

        $userInfo->account_status  = 1;

        $userInfo->full_name = $input['full_name'];
        $userInfo->nric = 1234;
        $userInfo->referrer_id = 1;
        $userInfo->group_id = 13;
        $userInfo->save();

        // Users table.
        $userCountry = new UserCountry;
        $userCountry->account_id = $largestCustomerId;
        $userCountry->country_id = $country_id;
        $userCountry->referrer_id = $country_id . '1010';
        $userCountry->group_id = 13;
        $userCountry->save();

        // User_addresses table(two records - billing address and shipping address)
        $userAddress_billing_address = new UserAddress;
        $userAddress_billing_address->account_id = $userInfo->account_id;
        $userAddress_billing_address->address_1 = 1;
        $userAddress_billing_address->city_key = 0;
        $userAddress_billing_address->state_id = 14;
        $userAddress_billing_address->country_id = $country_id;
        $userAddress_billing_address->is_shipping_address = 1;
        $userAddress_billing_address->is_residential_address = 1;
        $userAddress_billing_address->is_mailing_address = 1;
        $userAddress_billing_address->save();

        // User_contacts table (Home).
        $userContactHome = new UserContact;
        $userContactHome->account_id = $userInfo->account_id;
        $userContactHome->contact_num = 1;
        $userContactHome->is_home = 1;
        $userContactHome->is_mobile = 1;
        $userContactHome->is_office = 1;

        $userContactHome->save();

        return $user;
    }
}
