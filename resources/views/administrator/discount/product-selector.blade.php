@extends('layouts.administrator.mainVue')

@section('content')
@php

@endphp

<h1>Product Selector</h1>
<div class="card shadow-sm">
    <div class="card-header"> Product Select / Deselect<small> - select or deselect all product Eligible/Generate discount</small>
        <div class="card-header-actions"><a class="card-header-action" href="#" target="_blank"><small class="text-muted">docs</small></a></div>
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-6">
                <form action="{{ route('administrator.discount.selectorPost')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label" for="category">Category</label>
                        <select class="form-control" name="category" aria-readonly="true" readonly>
                            <option value="0">-- All Product --</option>
                            {{-- <option v-for="(c,i) in categories" :value="c.id">@{{c.name}}</option> --}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="category">Option</label>
                        <select class="form-control" name="option">
                            <option value="eligible_discount">Eligible Discount</option>
                            <option value="generate_discount">Generate Discount</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="category">Select Type</label>
                        <select class="form-control" name="type">
                            <option value="0">Select All</option>
                            <option value="1">Deselect All</option>
                        </select>
                    </div>
                    <hr>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </form>
            </div>



        </div>
    </div>
</div>
@endsection

@push('script')
<script>
Vue.use(VueTables.ClientTable);

    var app = new Vue({
    el: '#app',
    data: {
        categories: [],
    },
    mounted() { //created // mounted //beforeMount
        this.getCategory()
    },
    methods: {
        getCategory() {
        axios
            .get('/administrator/discount/get-category')
            .then(response => {
                this.categories = response.data
                })
            .catch(error => console.log(error))
        }
    }
    });
</script>
@endpush
