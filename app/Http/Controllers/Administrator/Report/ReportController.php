<?php

namespace App\Http\Controllers\Administrator\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{

    public function index(Request $request)
    {
        //get all panels
        $panels = DB::table('panel_infos')
            ->get();

        $status1 = array();
        $status2 = array();
        $status3 = array();
        $totalsales = array();
        $products = array();

        foreach ($panels as $key => $panel) {

            //count order status 1001
            $o1001 = DB::table('orders')
                ->join('panel_infos', 'panel_infos.account_id', '=', 'orders.panel_id')
                ->where('orders.panel_id', '=', $panel->account_id)
                ->where('orders.order_status', [1001])
                ->count();

            //count order status 1002
            $o1002 = DB::table('orders')
                ->join('panel_infos', 'panel_infos.account_id', '=', 'orders.panel_id')
                ->where('orders.panel_id', '=', $panel->account_id)
                ->where('orders.order_status', [1002])
                ->count();

            //count order status 1003
            $o1003 = DB::table('orders')
                ->join('panel_infos', 'panel_infos.account_id', '=', 'orders.panel_id')
                ->where('orders.panel_id', '=', $panel->account_id)
                ->where('orders.order_status', [1003])
                ->count();

            $status1[] = $o1001;
            $status2[] = $o1002;
            $status3[] = $o1003;

            //sum total sales result
            $totalsalesresult =  DB::table('orders')
                ->join('items', 'items.order_number', '=', 'orders.order_number')
                ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
                ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
                ->where('orders.panel_id', '=', $panel->account_id)
                ->whereIn('order_status', [1001, 1002, 1003])
                ->sum('items.subtotal_price');

            $totalsales[] = ($totalsalesresult / 100);

            //get order product
            $productsresult = DB::table('orders')
                ->join('panel_infos', 'panel_infos.account_id', '=', 'orders.panel_id')
                ->join('items', 'items.order_number', '=', 'orders.order_number')
                ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
                ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')

                // ->selectRaw('global_products.name, sum(items.quantity) quantity')
                ->select(DB::raw(" global_products.name, sum(items.quantity) as quantity"))
                ->where('orders.panel_id', '=', $panel->account_id)
                ->whereIn('order_status', [1001, 1002, 1003])
                ->groupBy('items.product_id')
                ->get();

            $products[] = $productsresult;
        }

        $date_format = 'd/m/Y';
        $date = now();
        // $dateArray = array('day' => $day, 'month' => $month, 'year' => $year);
        // $date = strtotime($year . '-' . $month . '-' . $day);
        $currentday = date($date_format, strtotime($date));

        return view('administrator.report.panelreport')
            ->with('panels', $panels)
            ->with('status1', $status1)
            ->with('status2', $status2)
            ->with('status3', $status3)
            ->with('totalsales', $totalsales)
            ->with('products', $products)
            ->with('productsresult', $productsresult)
            ->with('currentday', $currentday);
    }
}
