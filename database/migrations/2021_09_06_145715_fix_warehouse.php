<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Purchases\Order;
use App\Models\Products\WarehouseBatch;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Products\WarehouseInventories;
use Carbon\Carbon;

class FixWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        WarehouseBatch::truncate();
        WarehouseBatchDetail::truncate();
        WarehouseBatchItem::truncate();
        WarehouseInventories::truncate();
        DB::table('wh_logs')->truncate();

        //update all old orders to reserved investory
        $orders = Order::whereNotIn('orders.order_status', ['1000', '1002', '1003', '1004'])->get();

        foreach ($orders as  $order) {
            foreach ($order->items as  $item) {

                $productIds[] = $item->product_id;

                $inventoryRecords[$item->product_code]['outlet_id'] = $order->delivery_outlet;
                $inventoryRecords[$item->product_code]['inv_type'] = ($order->order_status == 1001) ? 8001 : 8011;
                // $inventoryRecords[$item->product_code]['inv_status'] = 'incomplete';
                $inventoryRecords[$item->product_code]['inv_amount'] = - ($item->quantity);
                $inventoryRecords[$item->product_code]['inv_user'] = $order->purchase_id;
                $inventoryRecords[$item->product_code]['model_type'] = 'App\Models\Purchases\Order';
                $inventoryRecords[$item->product_code]['model_id'] = $order->id;
                $inventoryRecords[$item->product_code]['product_code'] = $item->product_code;
                $inventoryRecords[$item->product_code]['inv_reference'] = 'starting migration';

                //manual create wh_inventories
                DB::table('wh_inventories')->insert([
                    'outlet_id' => $order->delivery_outlet,
                    'inv_type' => ($order->order_status == 1001) ? 8001 : 8011,
                    'inv_amount' => - ($item->quantity),
                    'inv_user' => $order->purchase_id,
                    'model_type' => 'App\Models\Purchases\Order',
                    'model_id' => $order->id,
                    'product_code' => $item->product_code,
                    'inv_reference' => 'starting migration',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            //manual create wh_log
            $logs_table = DB::table('wh_logs')->insert([
                'model_type' => 'App\Models\Products\WarehouseInventories',
                'model_id' => $order->id,
                'log_status' => 'created',
                'log_user' => $order->purchase->user_id,
                'log_data' => json_encode($inventoryRecords),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
