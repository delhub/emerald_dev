@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Warehouse Batch Label</h1>
<div class="card shadow-sm">
    <div class="card-header">Warehouse Batch Label<small> - Add new Box Label</small>
        {{-- <div class="card-header-actions"><button class="btn btn-primary btn-sm" type="button" onclick="window.location.href = '{{ route('administrator.v1.products.warehouse.index')}}';">Batch List</button></div> --}}
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-10">
                <form id="batch" action="{{ route('administrator.v1.products.warehouse.printLabel')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label" for="panel">Batch</label>
                        <select name="batch" class="form-control" required>
                            <option value="">Please choose a batch.</option>
                            @foreach($batchs as $batch)
                            <option value="{{ $batch->batch_id }}">{{ $batch->batch_id }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="form-group">
                        <label class="col-form-label" for="po_number">PO Number</label>
                        <input type="text" name="po_number" class="form-control" placeholder="PO Number" autocomplete="off" value="{{old('po_number')}}" required>
                    </div> --}}
                    <hr>
                    <div class="form-row details" id="details">
                        <div class="form-group col-md-7">
                          <label for="product_code"><small>Product Code</small></label>
                          <select name="product_code[]" class="form-control select2 pc" required>
                            <option value="">Please choose product code.</option>
                                @foreach($products as $product)
                                <option value="{{ $product->product_code }}">{{ $product->product_code }} - {{ $product->product2->parentProduct->name }} ({{ $product->attribute_name }})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                          <label for="expiry_date"><small>Quantity</small></label>

                            <div class="input-group">
                                <input type="text" name="quantity[]" class="form-control" placeholder="Quantity" data-parsley-error-message="" data-parsley-errors-container=".date-error-block" required>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger minus" onclick="removeBatchDetails(this);" disabled><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-success plus" onclick="cloneBatchDetails();"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>

                            <span class="date-error-block"></span>
                        </div>
                      </div>
                    <hr>
                    <div class="form-group text-center">
                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </form>
            </div>



        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
<script>
    // Parsley plugin initialization with tweaks to style Parsley for Bootstrap 4
    $("#batch").parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    });

 // Parsley full doc is avalailable here : https://github.com/guillaumepotier/Parsley.js/
</script>
<script>
    $('#stock_in_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
    /* $('.expiry_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    }); */

    function removeBatchDetails(element) {
        // $("#details").remove("div.details:last");
        $(element).parent().parent().parent().parent().slideUp(300, function() {
            $(this).remove();
        });
        disableFirstMinus();
    }

    function cloneBatchDetails() {
        // $(".details:last").find('.pc').select2('destroy');
        $(".pc").select2('destroy');
        $("button.minus").attr("disabled", false);
        $("#details").clone().insertAfter("div.details:last");
        $("#details div > span").remove();
        disableFirstMinus();
        $('.pc').select2();
    }

    function disableFirstMinus() {
        $("button.minus:first").attr("disabled", true);
    }

    setTimeout(function() {
        new SlimSelect({
            select: '.slimselect'
        })
    }, 300)
</script>
@endpush
