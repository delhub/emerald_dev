@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<h1>Logistic Report - Exclusives Product(Qty)</h1>

<div class="card shadow-sm">
    <div class="card-header">Logistic Report - Exclusives Product(Qty)<small> (data from 1st Oct 2021)</small></div>
    <div class="card-body">

        <form action="{{ route('administrator.ordertracking.logisticReport') }}" method="GET" class="col-md-6">
            <div class="row mt-2 ml-2">
                <div class="col-5 pr-0">
                    <input type="text" class="form-control d-inline" id="searchBox" name="search_box"
                        placeholder="Product Code / Name"
                        value="{{ ($request->search_box) ?? '' }}">
                </div>
                {{-- <div class="col-5 pr-0">
                    <select class="form-control" name="quality">
                        <option value="0"{{($request->quality == 0) ? 'selected' :''}}>All Quality</option>
                        <option value="1"{{($request->quality == 1) ? 'selected' :''}}>Domestic</option>
                        <option value="2"{{($request->quality == 2) ? 'selected' :''}}>Import</option>
                        <option value="3"{{($request->quality == 3) ? 'selected' :''}}>Exclusive</option>
                    </select>
                </div> --}}
                {{-- <div class="col-5 pr-0">
                    <select class="form-control" name="location">
                        <option value="all">All Location</option>
                        @foreach ($locations as $location)
                            <option value="{{$location->id}}"{{($request->location == $location->id) ? 'selected' :''}}>{{$location->location_name}}</option>
                        @endforeach
                    </select>
                </div> --}}

                 <!-- Date filter from date to date-->
                 <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{ ($request->datefrom) ? $request->datefrom : $request->dateto }}" autocomplete="off">
                        {{-- <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </div> --}}
                    </div>
                </div>

                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{ ($request->dateto) ? $request->dateto : $request->datefrom }}" autocomplete="off">
                        {{-- <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </div> --}}
                    </div>
                </div>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="col-2 px-0 ml-3">
                    <button type="submit" class="btn btn-outline-primary d-inline"
                        style="color:black;">Search</button>
                        <a href="{{ route('administrator.ordertracking.logisticReport') }}" class="border border-primary" style="padding:10px">Reset </a>
                        {{-- <form action="{{ route('administrator.ordertracking.logisticReport') }}" method="GET">
                            <button type="submit" class="btn btn-outline-primary" style="color:black;">Reset</button>
                        </form> --}}
                </div>
            </div>
        </form>

        <div class="row mt-2 ml-2">
            {{ $lists->links() }}
        </div>

        <div class="tableFixHead">
            <table class="table table-sm table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="align-middle">#</th>
                        <th scope="col" class="align-middle mx-0">Product</th>
                        {{-- <th scope="col" class="align-middle">Product Name</th> --}}
                        {{-- <th scope="col" class="align-middle">Product Attribute</th> --}}
                        {{-- <th scope="col" class="align-middle">Product Quanlity</th> --}}


                        {{-- <th scope="col" class="align-middle">In (Stock In)</th> --}}
                        <th scope="col" class="align-middle">Total Sold</th>
                        <th scope="col" class="align-middle" >Retail Sold</th>
                        {{-- <th scope="col" class="align-middle" style="border-left: 2px solid black;">Online Sold</th> --}}

                        <th scope="col" class="align-middle" style="border-left: 2px solid black;">Online Sold(exclude pending)</th>
                        <th scope="col" class="align-middle">Shipped</th>
                        <th scope="col" class="align-middle">Delivered</th>
                        <th scope="col" class="align-middle">Cancel</th>
                        <th scope="col" class="align-middle" style="border-left: 2px solid black;">Pending (sold, not yet pack)</th>
                        <th scope="col" class="align-middle" style="background-color:#f5dada;">Pending(30 days)</th>
                        <th scope="col" class="align-middle" style="background-color:#f7b9b9;">Pending(60 days)</th>
                        <th scope="col" class="align-middle" style="background-color:#f78a8a;">Pending(90 days)</th>
                        <th scope="col" class="align-middle" style="background-color:#fa5656;">Pending(120 days & above)</th>
                        {{-- <th scope="col" class="align-middle">Balance Physical</th> --}}
                        {{-- <th scope="col" class="align-middle">Balance Virtual</th> --}}


                        {{-- <th scope="col" class="align-middle">
                            Inventory --}}
                            {{-- <i class="bi bi-arrow-right-circle" onclick="showRow()">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right" viewBox="0 0 16 16">
                                    <path d="M6 12.796V3.204L11.481 8 6 12.796zm.659.753 5.48-4.796a1 1 0 0 0 0-1.506L6.66 2.451C6.011 1.885 5 2.345 5 3.204v9.592a1 1 0 0 0 1.659.753z"/>
                                </svg>
                            </i> --}}
                        {{-- </th> --}}
                    </tr>
                </thead>

                <tbody>
                    {{-- @dd($lists) --}}
                    @foreach ($lists as $product_code => $list)
                    <tr>
                        <td scope="row" class="text-center">{{ $loop->iteration }}</td>
                        <td>
                            <p class="mb-0 mx-0">
                                Product Code:
                                <span class="text-capitalize font-weight-bold">{{ $product_code }}</span>
                            </p>
                            <p class="mb-0 mx-0">
                                Product Name:
                                <span class="text-capitalize font-weight-bold">{{ $list['global_product_name'] }}</span>
                            </p>
                            <p class="mb-0 mx-0">
                                Product Attribute:
                                <span class="text-capitalize font-weight-bold">{{ $list['product_name'] }}</span>
                            </p>
                        </td>
                        {{-- <td>{{ showGlobalProductQuality($list['quality']) }}</td> --}}

                        {{-- <td class="text-success">{{ $list['total_stockIn'] }}</td> --}}
                        <td class="text-danger">{{ $list['total_sold'] }}</td>
                        <td class="text-danger">{{ $list['pos_completed'] }}</td>
                        {{-- <td class="text-danger" style="border-left: 2px solid black;">{{ $list['online_sold'] }}</td> --}}
                        {{-- <td class="text-success">{{ $list['overallPhysical'] }}</td> --}}
                        {{-- <td class="text-success">{{ $list['virtual_stock'] }}</td>  --}}




                        {{-- <td colspan="6" id="row_" class="bg-light" style="display: none">
                            Virtual stock:
                        </td> --}}
                    {{-- </tr> --}}
                    {{-- <tr class="tr1"> --}}
                        <td style="border-left: 2px solid black;">
                            {{-- @if (!empty($list['total']['qty'])) --}}
                                <div class="text-danger" style="text-align: center">{{ $list['total']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['total']['itemsid'] )  }}">

                                    <button type="submit">Detail</button>
                                    </form>
                                </div>
                            {{-- @endif --}}
                        </td>

                        {{-- <td bgcolor="#E8E8E8"> --}}
                        {{-- <td>
                            @if (isset($list['order_placed']['case']) && !empty($list['order_placed']['case']))
                            <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                <input type="hidden" name="manyID" value="{{ implode("," , $list['order_placed']['itemsid'] ) }}">
                                <button type="submit">Detail</button>
                            </form>
                            @endif

                        </td> --}}
                        {{-- @dd($list,$list['order_shipped']['qty'],$list['order_shipped']['itemsid']) --}}
                        <td>
                            @if (isset($list['order_shipped']['qty']) && !empty($list['order_shipped']['qty']))
                                <div style="text-align: center">{{ $list['order_shipped']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_shipped']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>
                        <td>
                            @if (isset($list['order_delivered']['qty']) && !empty($list['order_delivered']['qty']))
                                <div style="text-align: center">{{ $list['order_delivered']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_delivered']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif

                        </td>
                        <td>
                            @if (isset($list['order_cancel']['qty']) && !empty($list['order_cancel']['qty']))
                                <div style="text-align: center">{{ $list['order_cancel']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_cancel']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>
                        <td style="border-left: 2px solid black;">
                            @if (isset($list['order_pending_payment']['qty']) && !empty($list['order_pending_payment']['qty']))
                                <div style="text-align: center">{{ $list['order_pending_payment']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_pending_payment']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>


                        <td style="background-color:#f5dada;">
                            @if (isset($list['order_pending_payment_30days']['qty']) && !empty($list['order_pending_payment_30days']['qty']))
                                <div style="text-align: center">{{ $list['order_pending_payment_30days']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_pending_payment_30days']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>

                        <td style="background-color:#f7b9b9;">
                            @if (isset($list['order_pending_payment_60days']['qty']) && !empty($list['order_pending_payment_60days']['qty']))
                                <div style="text-align: center">{{ $list['order_pending_payment_60days']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_pending_payment_60days']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>

                        <td style="background-color:#f78a8a;">
                            @if (isset($list['order_pending_payment_90days']['qty']) && !empty($list['order_pending_payment_90days']['qty']))
                                <div style="text-align: center">{{ $list['order_pending_payment_90days']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_pending_payment_90days']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>

                        <td style="background-color:#fa5656;">
                            @if (isset($list['order_pending_payment_120days']['qty']) && !empty($list['order_pending_payment_120days']['qty']))
                                <div style="text-align: center">{{ $list['order_pending_payment_120days']['qty'] }}
                                    <form action="{{ route('administrator.ordertracking.old-detail')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $list['order_pending_payment_120days']['itemsid'] ) }}">
                                        <button type="submit">Detail</button>
                                    </form>
                                </div>
                            @endif
                        </td>

                        {{-- <td class="text-center">
                            <form action="{{ route('administrator.warehouse.inventory.index') }}" method="GET">
                                <input type="hidden" class="form-control d-inline" id="searchBox" name="search_box"  value="{{ ($product_code) ?? '' }}">
                                <button type="submit" class="btn btn-primary">Inventory</button>
                            </form>

                        </td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="row mt-2 ml-2">
            {{ $lists->links() }}
        </div>

    </div>
</div>

@push('script')
<script>

    $(function() {

        $(".date").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0,
            changeMonth: true,
            changeYear: true
            });

            $(".filterDate").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            changeMonth: true,
            changeYear: true
            });
    });

</script>
@endpush
@push('style')
    <style>
        /* freeze header  */
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

            table  { border-collapse: collapse; width: 100%; }
            th     { background:#eee; }
    </style>
@endpush

{{-- @push('script')

<script>
    function showRow() {
        var row_ = document.querySelectorAll('#row_');

        row_.forEach(item => {
            if (item.style.display == "none") {
                item.style.display = "block";
            }else{
                item.style.display = "none";
            }
        });
    }
</script>

@endpush --}}
@endsection


