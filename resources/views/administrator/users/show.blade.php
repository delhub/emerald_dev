@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
@endif

<div class="container-fluid flex-grow-1 container-p-y">
    <div class="d-flex justify-content-between">
        <div>
        <h4 id="customer_info_name" class="font-weight-bold py-3 mb-4 info-detail" style="display: block;">
            {{ $user->userInfo->full_name }} <span class="text-muted font-weight-light"> ({{ $user->userInfo->account_id }})</span>
        </h4>
        @if($user->hasRole('dealer') && isset( $user->dealerInfo))
        <h4 id='dealer_info_name' class="font-weight-bold py-3 mb-4 info-detail" style="display: none;">
            {{ $user->dealerInfo->full_name }} <span class="text-muted font-weight-light"> ({{ $user->dealerInfo->account_id }})</span>
        </h4>
        @endif
        </div>
        <div style="padding-top: 10px; padding-right: 25px;"><a class="btn btn-primary" href="{{ route('administrator.user') }}"> Back</a></div>
    </div>

    <div class="">
        <div class="">
            <h5 for="exampleFormControlInput1" class="form-label font-weight-bold">Roles : </h5> 
            @if(!empty($user->getRoleNames()))
            <div>
                @foreach($user->getRoleNames() as $v)
                <label class="badge badge-success px-4 py-2" style="font-size: 14px;font-weight:100">{{ $v }}</label>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    <div class="d-flex">
        <button onclick="showSection('customer_info')" class="my-profile-opt-btn tab active" id="customer_infoTab">
            <i class="fa fa-user mr-1 active"></i><strong>Customer Information</strong>
        </button>
        @if($user->hasRole('dealer') && isset($user->dealerInfo))
            <button onclick="showSection('dealer_info')" class="my-profile-opt-btn tab" id="dealer_infoTab">
                <i class="fa fa-address-book-o mr-1"></i><strong>Dealer Profile</strong>
            </button>
            @if(isset($user->dealerInfo->dealerBankDetails))
                <button onclick="showSection('bank_info')" class="my-profile-opt-btn tab" id="bank_infoTab">
                    <i class="fa fa-address-book-o mr-1"></i><strong>Bank Details</strong></a>
                </button>
            @endif
        @endhasrole
        @if($user->hasRole('panel'))
            <button onclick="showSection('panel_info')" class="my-profile-opt-btn tab" id="panel_infoTab">
                <i class="fa fa-address-book-o mr-1"></i><strong>Panel Information</strong>
            </button>
        @endhasrole
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">                
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 margin-tb d-flex justify-content-between">
                            <div class="pull-left">
                                <h2 id="customer_info_header">Customer Information</h2>
                                <h2 id="dealer_info_header" style="display: none;">Dealer Information</h2>
                                <h2 id="bank_info_header" style="display: none;">Bank Information</h2>
                                <h2 id="panel_info_header" style="display: none;">Panel Information</h2>
                            </div>
                            <div class="float-right d-flex">
                                <div style="padding-right: 10px">
                                    <button class="btn btn-success" type="button" id="submitBtn" style="display:none; color: white;">Submit</button>
                                    <button class="btn btn-primary" type="button" onclick="enableEditing()" id="enableEditing" style="color: black;">Edit</button>
                                </div>                                
                                
                            </div>
                        </div>
                    </div>
                    <form id="customer_info_form" method="POST" action="{{ route('administrator.users.update', ['id' => $user->id, 'form_type' => 'customer_info']) }}">
                        @csrf
                        <input type="hidden" id="activeSection" name="activeSection" value="{{ old('activeSection', 'customer_info') }}">
                        <div id="customer_info" class="user_information" style="display: block;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Full Name</label>
                                        <input type="text" class="form-control" name="full_name" 
                                            value="{{ old('full_name', optional($user->userInfo)->full_name) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">NRIC Number</label>
                                        <input type="text" class="form-control" name="nric" 
                                            value="{{ old('nric', optional($user->userInfo)->nric) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Shipping Address</label>
                                        <input type="text" class="form-control" name="shipping_address[address_1]" 
                                            value="{{ old('shipping_address.address_1', optional($user->userInfo->shippingAddress)->address_1) }}" readonly>
                                        <input type="text" class="form-control" name="shipping_address[address_2]" 
                                            value="{{ old('shipping_address.address_2', optional($user->userInfo->shippingAddress)->address_2) }}" readonly>
                                        <input type="text" class="form-control" name="shipping_address[address_3]"  
                                            value="{{ old('shipping_address.address_3', optional($user->userInfo->shippingAddress)->address_3) }}" readonly>
                                        <input type="text" class="form-control" name="shipping_address[postalCode]" 
                                            value="{{ old('shipping_address.postalCode', optional($user->userInfo->shippingAddress)->postcode) }}" placeholder="Postal Code" readonly>
                                        <div class="d-flex">
                                            <div class="w-50">
                                                <select class="form-control state" name="shipping_address[state]" disabled>
                                                    <option value="" disabled selected>Select a State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" 
                                                            {{ old('shipping_address.state', optional($user->userInfo->shippingAddress)->state_id) == $state->id ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="w-50">
                                                <select class="form-control city" name="shipping_address[city]" disabled>
                                                    <option value="" disabled selected>Select a City</option>
                                                    @foreach($cities as $city)
                                                        <option data-state-id="{{ $city->state_id }}" value="{{ $city->city_key }}"
                                                            {{ old('shipping_address.city', optional($user->userInfo->shippingAddress)->city_key) == $city->city_key ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">Mobile Phone</label>
                                        <input type="text" class="form-control" name="contact_num" 
                                            value="{{ old('contact_num', optional($user->userInfo->mobileContact)->contact_num) }}" readonly>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label">Email Address</label>
                                        <input type="text" class="form-control" name="email" 
                                            value="{{ old('email', $user->email) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Mailing Address</label>
                                        <input type="text" class="form-control" name="mailingAddress[address_1]" 
                                            value="{{ old('mailingAddress.address_1', optional($user->userInfo->mailingAddress)->address_1) }}" readonly>
                                        <input type="text" class="form-control" name="mailingAddress[address_2]" 
                                            value="{{ old('mailingAddress.address_2', optional($user->userInfo->mailingAddress)->address_2) }}" readonly>
                                        <input type="text" class="form-control" name="mailingAddress[address_3]" 
                                            value="{{ old('mailingAddress.address_3', optional($user->userInfo->mailingAddress)->address_3) }}" readonly>
                                        <input type="text" class="form-control" name="mailingAddress[postalCode]" 
                                            value="{{ old('mailingAddress.postalCode', optional($user->userInfo->mailingAddress)->postcode) }}" placeholder="Postal Code" readonly>
                                        <div class="d-flex">
                                            <div class="w-50">
                                                <select class="form-control state" name="mailingAddress[state]" disabled>
                                                    <option value="" disabled selected>Select a State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" 
                                                            {{ old('mailingAddress.state', optional($user->userInfo->mailingAddress)->state_id) == $state->id ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="w-50">
                                                <select class="form-control city" name="mailingAddress[city]" disabled>
                                                    <option value="" disabled selected>Select a City</option>
                                                    @foreach($cities as $city)
                                                        <option data-state-id="{{ $city->state_id }}" value="{{ $city->city_key }}"
                                                            {{ old('mailingAddress.city', optional($user->userInfo->mailingAddress)->city_key) == $city->city_key ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if(isset($user->userInfo))
                                <div class="col-md-6 pt-3 border-top">
                                    <div class="mb-3">
                                        <label>Agent Group Name</label>
                                        <input type="text" class="form-control" name="agentGroup" 
                                            value="{{ old('agentGroup', optional($user->userInfo->agentGroup)->group_name) }}" disabled>
                                    </div>

                                    <div class="mb-3">
                                        <label>Agent Name</label>
                                        <input type="text" class="form-control" name="agentName" 
                                            value="{{ old('agentName', optional($user->userInfo->agentInformation)->full_name) }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 pt-3 border-top">
                                    <div class="mb-3">
                                        <label>Agent ID</label>
                                        <input type="text" class="form-control" name="agentId" 
                                            value="{{ old('agentId', optional($user->userInfo)->referrer_id) }}" disabled>
                                    </div>
                                    <div class="mb-3">
                                        <label>Agent NRIC</label>
                                        <input type="text" class="form-control" name="agentNric" 
                                            value="{{ old('agentNric', optional($user->userInfo->agentInformation)->nric) }}" disabled>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>

<!-- DEALER INFO -->
                    @if($user->hasRole('dealer') && isset($user->dealerInfo))
                    <form id="dealer_info_form" method="POST" action="{{ route('administrator.users.update', ['id' => $user->id, 'form_type' => 'dealer_info']) }}">
                        @csrf
                        <input type="hidden" id="activeSection" name="activeSection" value="{{ old('activeSection', 'dealer_info') }}">
                        <div id="dealer_info" class="user_information" style="display: none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Agent Full Name</label>
                                        <input type="text" name="full_name" class="form-control" 
                                            value="{{ old('full_name', optional($user->dealerInfo)->full_name) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label>Date Of Birth</label>
                                        <input type="text" id="agent_date_of_birth{{ $user->id }}" name='date_of_birth' class="form-control" 
                                        value="{{ old('date_of_birth', optional($user->dealerInfo)->date_of_birth) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label>Marital Status</label>
                                        <select class="form-control city" name="maritalStatus" disabled>
                                            <option value="" disabled selected>Select a Marital Status</option>
                                            @foreach($maritals as $marital)
                                                <option class="text-capitalize" value="{{ $marital->id }}"
                                                    {{ old('maritalStatus', optional($user->dealerInfo)->marital_id) == $marital->id ? 'selected' : '' }}>
                                                    {{ ucfirst($marital->name) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="mb-3">
                                        <label>Agent Payment Invoice Number</label>
                                        <input type="text" name='payment_proof' class="form-control" 
                                        value="{{ old('payment_proof', optional($user->dealerInfo)->payment_proof)}}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label>Agent Group</label>
                                        <select class="form-control city" name="agentGroup" disabled>
                                            <option value="" disabled selected>Select a Group</option>
                                            @foreach($dealerGroups as $dealerGroup)
                                            <option  class="text-capitalize" value="{{ $dealerGroup->id }}"
                                            {{ old('agentGroup', optional($user->dealerInfo)->group_id) == $dealerGroup->id ? 'selected' : '' }}>
                                            {{ ucfirst($dealerGroup->group_name) }}
                                            </option>
                                            @endforeach
                                        </select>                                
                                    </div>
                                    <div class="mb-3">
                                        <label>Agent Package</label>
                                        <select class="form-control" name="package_name" disabled>
                                            <option value="" disabled selected>Select a Package</option>
                                            @foreach($packages as $package)
                                            <option class="text-capitalize" value="{{ $package->id }}"
                                            {{ old('package_name', optional($user->dealerInfo)->package_type) == $package->id ? 'selected' : '' }}>
                                            {{ ucfirst($package->package_name) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Shipping Address</label>
                                        <input type="text" class="form-control" name="shippingAddress[address_1]" 
                                            value="{{ old('shippingAddress.address_1', optional($user->dealerInfo->shippingAddress)->address_1) }}" readonly>

                                        <input type="text" class="form-control" name="shippingAddress[address_2]" 
                                            value="{{ old('shippingAddress.address_2', optional($user->dealerInfo->shippingAddress)->address_2) }}" readonly>
                                        <input type="text" class="form-control" name="shippingAddress[address_3]"  
                                            value="{{ old('shippingAddress.address_3', optional($user->dealerInfo->shippingAddress)->address_3) }}" readonly>
                                        <input type="text" class="form-control" name='shippingAddress[postalCode]' 
                                            value="{{ old('shippingAddress.postalCode', optional($user->dealerInfo->shippingAddress)->postcode) }}" readonly>
                                        <div class="d-flex">
                                            <div class="w-50">
                                                <select id="state" class="form-control state" name="shippingAddress[state]" disabled>
                                                    <option value="" disabled selected>Select a State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" 
                                                            {{ old('shippingAddress.state', optional($user->dealerInfo->shippingAddress)->state_id) == $state->id ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="w-50">
                                                <select id="city" class="form-control city" name="shippingAddress[city]" disabled>
                                                    <option value="" disabled selected>Select a City</option>
                                                    @foreach($cities as $city)
                                                        <option data-state-id="{{ $city->state_id }}" value="{{ $city->city_key }}"
                                                            {{ old('shippingAddress.city', optional($user->dealerInfo->shippingAddress)->city_key) == $city->city_key ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>NRIC Number</label>
                                        <input type="text" name='nric' class="form-control" 
                                        value="{{ old('nric', optional($user->dealerInfo)->nric) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleFormControlInput1" class="form-label">Mobile Phone</label>
                                        <input type="text" class="form-control" name='contact_num' value="{{ old('contact_num', optional($user->dealerInfo->dealerMobileContact)->contact_num) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label>Gender</label>
                                        <select class="form-control city" name="gender" disabled>
                                            <option value="" disabled selected>Select a Gender</option>
                                            @foreach($genders as $gender)
                                            <option class="text-capitalize" value="{{ $gender->id }}"
                                                {{ old('gender', optional($user->dealerInfo)->gender_id) == $gender->id ? 'selected' : '' }}>
                                                {{ ucfirst($gender->name) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label>Race</label>
                                        <select class="form-control city" name="race" disabled>
                                            <option value="" disabled selected>Select a Race</option>
                                            @foreach($races as $race)
                                            <option class="text-capitalize" value="{{ $race->id }}"
                                                {{ old('race', optional($user->dealerInfo)->race_id) == $race->id ? 'selected' : '' }}>
                                                {{ ucfirst($race->name) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label>Agent Referrer Name</label>
                                        <input type="text" name="referrer_name" class="form-control"
                                            value="{{ old('referrer_name', optional($user->dealerInfo)->referrer_name) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label>Agent Referrer ID</label>
                                        <input type="text" name='referrer_id' class="form-control" 
                                        value="{{ old('referrer_id', optional($user->dealerInfo)->referrer_id) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Billing Address</label>
                                        <input type="text" class="form-control" name="billingAddress[address_1]" 
                                            value="{{ old('billingAddress.address_1', optional($user->dealerInfo->billingAddress)->address_1) }}" readonly>
                                        <input type="text" class="form-control" name="billingAddress[address_2]" 
                                            value="{{ old('billingAddress.address_2', optional($user->dealerInfo->billingAddress)->address_2) }}" readonly>
                                        <input type="text" class="form-control" name="billingAddress[address_3]"  
                                            value="{{ old('billingAddress.address_3', optional($user->dealerInfo->billingAddress)->address_3) }}" readonly>
                                        <input type="text" class="form-control" name='billingAddress[postalCode]' 
                                            value="{{ old('billingAddress.postalCode', optional($user->dealerInfo->billingAddress)->postcode) }}" readonly>
                                        <div class="d-flex">
                                            <div class="w-50">
                                                <select id="state" class="form-control state" name="billingAddress[state]" disabled>
                                                    <option value="" disabled selected>Select a State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" 
                                                            {{ old('billingAddress.state', optional($user->dealerInfo->billingAddress)->state_id) == $state->id ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="w-50">
                                                <select id="city" class="form-control city" name="billingAddress[city]" disabled>
                                                    <option value="" disabled selected>Select a City</option>
                                                    @foreach($cities as $city)
                                                        <option data-state-id="{{ $city->state_id }}" value="{{ $city->city_key }}"
                                                            {{ old('billingAddress.city', optional($user->dealerInfo->billingAddress)->city_key) == $city->city_key ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 pt-3 border-top">
                                    <div class="mb-3">
                                        <label>Employment</label>
                                        <select class="form-control" name="employment" disabled>
                                            <option value="" disabled selected>Select a Employment </option>
                                            
                                            @foreach($employments as $employment)
                                            <option  class="text-capitalize" value="{{ $employment->id }}"
                                                {{ old('employment', optional($user->dealerInfo->employmentAddress)->employment_type) == $employment->id ? 'selected' : '' }}>
                                                {{ ucfirst($employment->name) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Company Name</label>
                                        <input type="text" class="form-control" name='company_name' 
                                        value="{{ old('company_name', optional($user->dealerInfo->employmentAddress)->company_name) }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 pt-3 border-top">
                                    <div class="mb-3">
                                        <label class="form-label">Employment Address</label>
                                        <input type="text" class="form-control" name="employmentAddress[address_1]" 
                                            value="{{ old('employmentAddress.address_1', optional($user->dealerInfo->employmentAddress)->company_address_1) }}" readonly>
                                        <input type="text" class="form-control" name="employmentAddress[address_2]" 
                                            value="{{ old('employmentAddress.address_2', optional($user->dealerInfo->employmentAddress)->company_address_2) }}" readonly>
                                        <input type="text" class="form-control" name="employmentAddress[address_3]"  
                                            value="{{ old('employmentAddress.address_3', optional($user->dealerInfo->employmentAddress)->company_address_3) }}" readonly>
                                        <input type="text" class="form-control" name='employmentAddress[postalCode]' 
                                            value="{{ old('employmentAddress.postalCode', optional($user->dealerInfo->employmentAddress)->company_postcode) }}" readonly>
                                        <div class="d-flex">
                                            <div class="w-50">
                                                <select id="state" class="form-control state" name="employmentAddress[state]" disabled>
                                                    <option value="" disabled selected>Select a State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" 
                                                            {{ old('employmentAddress.state', optional($user->dealerInfo->employmentAddress)->company_state_id) == $state->id ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="w-50">
                                                <select id="city" class="form-control city" name="employmentAddress[city]" disabled>
                                                    <option value="" disabled selected>Select a City</option>
                                                    @foreach($cities as $city)
                                                        <option data-state-id="{{ $city->state_id }}" value="{{ $city->city_key }}"
                                                            {{ old('employmentAddress.city', optional($user->dealerInfo->employmentAddress)->company_city_key) == $city->city_key ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 pt-3 border-top">
                                    <div class="mb-3">
                                        <label class="form-label">Spouse Name</label>
                                        <input type="text" class="form-control" name="spouse_name" 
                                            value="{{ old('spouse_name', optional($user->dealerInfo->dealerSpouse)->spouse_name) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Spouse Date Of Birth</label>
                                        <input type="text" class="form-control" name="spouse_date_of_birth" 
                                            value="{{ old('spouse_date_of_birth', optional($user->dealerInfo->dealerSpouse)->spouse_date_of_birth) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Spouse Contact Office</label>
                                        <input type="text" class="form-control" name="spouse_contact_office" 
                                            value="{{ old('spouse_contact_office', optional($user->dealerInfo->dealerSpouse)->spouse_contact_office) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Spouse Email</label>
                                        <input type="text" class="form-control" name="spouse_email" 
                                            value="{{ old('spouse_email', optional($user->dealerInfo->dealerSpouse)->spouse_email) }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 pt-3 border-top">
                                    <div class="mb-3">
                                        <label class="form-label">Spouse NRIC</label>
                                        <input type="text" class="form-control" name="spouse_nric" 
                                            value="{{ old('spouse_nric', optional($user->dealerInfo->dealerSpouse)->spouse_nric ) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Spouse Occupation</label>
                                        <input type="text" class="form-control" name="spouse_occupation" 
                                            value="{{ old('spouse_occupation', optional($user->dealerInfo->dealerSpouse)->spouse_occupation) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Spouse Contact Mobile</label>
                                        <input type="text" class="form-control" name="spouse_contact_mobile" 
                                            value="{{ old('spouse_contact_mobile', optional($user->dealerInfo->dealerSpouse)->spouse_contact_mobile) }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                    <!-- Bank INFO -->
                    @if(isset($user->dealerInfo->dealerBankDetails))
                    <form id="bank_info_form" method="POST" action="{{ route('administrator.users.update', ['id' => $user->id, 'form_type' => 'bank_info']) }}">
                        @csrf
                        <input type="hidden" id="activeSection" name="activeSection" value="{{ old('activeSection', 'bank_info') }}">
                        <div id="bank_info" class="user_information" style="display: none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Bank Name</label>
                                        <select class="form-control city" name="bank_code" disabled>
                                            <option value="" disabled selected>Select a Bank</option>
                                            @foreach($banks as $bank)
                                            <option class="text-capitalize" value="{{ $bank->bank_code }}"
                                                {{ old('bank_code', optional($user->dealerInfo->dealerBankDetails)->bank_code) == $bank->bank_code ? 'selected' : '' }}>
                                                {{ ucfirst($bank->bank_name) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label>Bank Account IC/Registration Number</label>
                                        <input type="text" name="account_regid" class="form-control" 
                                            value="{{ old('account_regid', optional($user->dealerInfo->dealerBankDetails)->account_regid) }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label>Bank Account Number</label>
                                        <input type="text" name="bank_acc" class="form-control" 
                                            value="{{ old('bank_acc', optional($user->dealerInfo->dealerBankDetails)->bank_acc) }}" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label>Bank Account Name</label>
                                        <input type="text" name="bank_acc_name" class="form-control" 
                                            value="{{ old('bank_acc_name', optional($user->dealerInfo->dealerBankDetails)->bank_acc_name) }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    @endif
                    @endhasrole
                    <!-- PANEL INFO -->
                    <div id="panel_info" class="user_information" style="display: none;">
                        <h5 class="modal-title" id="panelInfoModal{{ $user->id }}Label">
                            Coming Soon
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    #panel_info h5{
        text-align: center;
        padding: 50px 0px;
        font-size: 35px;
    }
    button.btn{
        border-radius: 10px; 
        min-width: 90px
    }
    .my-profile-opt-btn {
        padding: 10px 13px;
        background: #fff;
        border-top: 1px solid rgba(0, 0, 0, 0.125);
        border-left: 1px solid rgba(0, 0, 0, 0.125);
        border-right: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 20px 20px 0px 0px;
        color: #000;
        font-weight: normal;
        margin-right: 6px;
        width: auto;
        border: lightgray 1px solid;
    }
    .my-profile-opt-btn.active {
        border-top: 1px solid rgba(0, 0, 0, 0.125);
        border-left: 1px solid rgba(0, 0, 0, 0.125);
        border-right: 1px solid rgba(0, 0, 0, 0.125);
        background: linear-gradient(180deg, #ef3842 0%, #fd5b64 100%);
        border-radius: 20px 20px 0px 0px;
        width: auto;
        height: auto;
        padding: 10px;
        color: #fff;
    }
</style>
