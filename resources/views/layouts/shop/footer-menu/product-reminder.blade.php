<form action="/shop/addRbsPostpaid" method="post">
    @csrf
<div class="row reminder-product">
    <div class="col-md-6">
        <div class="card-box-left">
            <h5>Product <span>Details</span></h5>
            <hr>
            @foreach ($products->panelProduct->attributes as $attributes)
            <div class="body popupReminder" id="popupReminder-{{ $attributes->id }}" style="{{ $loop->first && $attributes->allow_rbs == 1 ? '' : 'display:none;' }}">
                <p>{{ $attributes->product2->parentProduct->name }}</p>
                <span>{{ $attributes->attribute_name }}</span>
                <div class="price">
                    <p>MYR {{ number_format(($attributes->getPriceAttributes('web_offer_price')/100),2) }} <span>Offer Price</span></p>
                </div>
                <input type="hidden" name="product_attributes" value="{{ $attributes->id }}">
            </div>
            @endforeach
        </div>
    </div>
    <div class="col-md-6">
        <div class="card-box-right">
            <h5>Reminder <span>Frequency</span></h5>
            <div class="rbs-col reminder">
                <div class="rbs-time-slc">
                    @foreach ($rbsReminders->where('option_type','frequency') as $rbsReminder)
                    <div class="list-rbs">
                        <label>
                           <input type="radio" name="monthReminder" id="permonth" value="{{ $rbsReminder->rbs_data }}" >
                           <div class="dot"></div>
                           {{ $rbsReminder->rbs_label }}
                        </label>
                     </div>
                    @endforeach
                </div>
             </div>
             <div class="item-buy-section">
                <div class="item-quality">
                   <h6>Quantity</h6>
                   <div class="item-buy-section-quantity" >
                      <button class="product-add-btn" type="button" name="fml-add-btn">
                         <i class="fa fa-plus" aria-hidden="true"></i>
                      </button>
                      <input type="text" name="productQuantity" value="" id="varqty" class="varqty">
                      <button class="product-minus-btn " type="button" name="fml-minus-btn">
                         <i class="fa fa-minus" aria-hidden="true"></i>
                      </button>
                   </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-12 border-top">
        <div class="card-box-center">
            <h5>Delivery <span>Address</span></h5>
            <div class="body">
                <div class="col-six">
                    <div class="mb-3 cart-form1">
                        <label for="exampleInputEmail0" class="form-label">Name<small class="text-danger">*</small></label>
                        <input type="text" class="checkout-box2 form-control valid" id="exampleInputEmail0" aria-describedby="emailHelp" placeholder="Your Name" name="address[name]" value="{{ optional($cookie)->name }}">
                    </div>
                    <div class="mb-3 cart-form1">
                        <label for="exampleFormControlInput0" class="form-label">Address 1<small class="text-danger">*</small></label>
                        <input type="text" class="checkout-box2 form-control " id="exampleFormControlInput0" placeholder="Address 1" name="address[address1]" value="{{ optional($cookie)->address_1 }}">
                    </div>

                    <div class="mb-3 cart-form1">
                        <label for="exampleFormControlInput1" class="form-label">Address 3</label>
                        <input type="text" class="checkout-box2 form-control" id="exampleFormControlInput1" placeholder="Address 3" name="address[address3]" value="{{ optional($cookie)->address_3 }}">
                    </div>
                    {{-- <div class="cart-edit-location"> --}}
                        <div class="mb-3 cart-form1">
                            <label for="stateChoices">State<small class="text-danger">*</small></label>
                            <select class="form-select checkout-box2 " aria-label="Default select example" id="stateChoices" onchange="editAddressForm(this)" name="address[state]">
                                <option selected disabled>Select...</option>
                                @foreach ($states as $state)
                                    <option value="{{ $state->id }}"
                                        {{ $cookie && $cookie->state_id == $state->id && !in_array($cookie->state_id,[10,11,15]) ? 'selected' : '' }}
                                        {!! in_array($state->id,[10,11,15]) ? 'style="color:lightgrey;" disabled' : '' !!}>
                                        {{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    {{-- </div> --}}

                </div>
                <div class="col-six">
                    <div class="mb-3 cart-form1">
                        <label for="exampleInputEmail1" class="form-label">Contact (Mobile)<small class="text-danger">*</small></label>
                        <input type="text" class="checkout-box2 form-control " id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contact Number" name="address[phone]" value="0123456789">
                    </div>
                    <div class="mb-3 cart-form1">
                        <label for="exampleFormControlInput1" class="form-label">Address 2</label>
                        <input type="text" class="checkout-box2 form-control" id="exampleFormControlInput1" placeholder="Address 2" name="address[address2]" value="{{ optional($cookie)->address_2 }}">
                    </div>
                    <div class="mb-3 cart-form1">
                        <label for="exampleFormControlInput2" class="form-label">Postcode<small class="text-danger">* </small> <span class="form-text text-danger ajaxMessage d-inline" style="display: none"></span></label>
                        <input type="text" class="checkout-box2 form-control " id="exampleFormControlInput2" placeholder="Postcode" name="address[postcode]" value="{{ optional($cookie)->postcode }}">
                    </div>
                    {{-- <div class="cart-edit-location"> --}}
                        <div class="mb-3 cart-form1">
                            <label for="cityChoices">City<small class="text-danger">*</small></label>
                            <select class="form-select checkout-box2" aria-label="Default select example" id="cityChoices" name="address[city]">
                                <option selected="" disabled="">Select...</option>
                                @foreach ($cities as $city)
                                    <option value="{{ $city->city_key }}"
                                        {{ $cookie && $cookie->city_key == $city->city_key ? 'selected' : '' }}>
                                        {{ $city->city_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    {{-- </div> --}}
                </div>

                <div class="footer row border-top pt-3">
                    <div class="col-12 text-right">
                        <p>
                            After submit, you may view/edit settings at “My Dashboard”-> Recurring Booking System(RBS)
                        </p>

                    </div>
                    <div class="col-12 text-right">
                        <button type="button" class="cancel-btn btn btn-primary" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                        <button type="submit" class="savechg-btn btn-primary" id="cartAddress">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>