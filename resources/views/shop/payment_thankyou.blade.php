@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection
<div class="cart-home">
  <div class="fml-container">
      <div class="subtitle-namelist">
          <ul>
              <li><a href="/">Home</a></li>
              <li><span>/</span></li>
              <li><a href="#">Cart Page</a></li>
          </ul>
      </div>
      <section>
        <div class="fml-container ">
            <div class="row footer-btm">
                <div class="col-md-5">
                    <img class="thankyou-3dimg" src="{{asset('/images/formula/thankyou-bg.png')}}" alt="">
                </div>
                <div class="col-md-7">
                    <div class="thank-info">
                        <div class="thankyou-text dd">
                            <h5>Your order has been placed. Thank you for shopping with us.</h5>
                        </div>
                        <div class="edit-opt-btn">
                            <button class="cancel-btn btn-space btn btn-primary">Download Invoice</button>
                            <button class="savechg-btn btn-primary">View Orders</button>
                        </div>
                    </div>
                    {{-- <div class="continue-shop">
                        <div class="shop-for-text">
                            <h5>Continue <span>Shop For</span></h5>
                        </div>
                        <div class="row">
                            <div class="continue-shop-category col-md-4">
                                <a class="thank-adult" href="#">
                                    <img src="{{asset('/images/icons/icon-eldest.png')}}" alt="">
                                    <p>Elderly</p>
                                </a>
                            </div>
                            <div class="continue-shop-category col-md-4">
                                <a class="thank-adult" href="#">
                                    <img src="{{asset('/images/icons/icon-adult.png')}}" alt="">
                                    <p>Adult</p>
                                </a>
                            </div>
                            <div class="continue-shop-category col-md-4">
                                <a class="thank-adult" href="#">
                                    <img src="{{asset('/images/icons/icon-kids.png')}}" alt="">
                                    <p>Kids</p>
                                </a>
                            </div>
                        </div>

                    </div> --}}
                </div>
            </div>
    </section>
  </div>
</div>
@endsection


@push('script')
<script>
    
</script>
@endpush