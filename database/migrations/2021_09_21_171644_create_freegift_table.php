<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFreegiftTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('freegift', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('outlet_id')->default(0);
			$table->string('gift_name', 128)->nullable();
			$table->string('gifted', 64)->nullable();
            $table->text('contains')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('gift_type', 64)->nullable();
			$table->text('gift_product')->nullable();
			$table->integer('gift_price_value')->default(0)->nullable();
			$table->integer('gift_price_percentage')->default(0)->nullable();
            $table->tinyInteger('status')->default(0);
			$table->dateTime('start_date');
            $table->dateTime('end_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('freegift');
	}

}
