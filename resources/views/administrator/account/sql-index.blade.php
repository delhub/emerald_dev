@extends('layouts.administrator.main')

@section('content')
    @php

    @endphp

    <h1>Account Operation</h1>
    <div class="card shadow-sm">
        <h5 class="card-header">SQL - Invoice Export</h5>
        <div class="card-body px-5">
            <form action="{{ route('administrator.account.sql.invoice') }}" method="post" id="sql_invoice">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-2 p-0">
                        <label for="i_date_start">Start Date <span class="text-danger">*</span></label>
                        <input class="form-control @error('i_date_start') is-invalid @enderror" name="i_date_start"
                            id="i_date_start" type="date" placeholder="Select Date Start">
                        @error('i_date_start')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-2">
                        <label for="i_date_end">End Date <span class="text-danger">*</span></label>
                        <input class="form-control @error('i_date_end') is-invalid @enderror" name="i_date_end"
                            id="i_date_end" type="date" placeholder="Select Date End">
                        @error('i_date_end')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-2">
                        <label for="i_submit">&nbsp;</label>
                        <input type="submit" name="i_submit" class="btn btn-primary btn-block" value="Download Data">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card shadow-sm">
        <h5 class="card-header">SQL - Payment Export</h5>
        <div class="card-body px-5">
            <form action="{{ route('administrator.account.sql.payment') }}" method="post" id="sql_payment">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-2 p-0">
                        <label for="p_date_start">Start Date <span class="text-danger">*</span></label>
                        <input class="form-control @error('p_date_start') is-invalid @enderror" name="p_date_start"
                            id="p_date_start" type="date" placeholder="Select Date Start">
                        @error('p_date_start')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-2">
                        <label for="p_date_end">End Date <span class="text-danger">*</span></label>
                        <input class="form-control @error('p_date_end') is-invalid @enderror" name="p_date_end"
                            id="p_date_end" type="date" placeholder="Select Date End">
                        @error('p_date_end')
                            <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-2">
                        <label for="p_submit">&nbsp;</label>
                        <input type="submit" name="p_submit" class="btn btn-primary btn-block" value="Download Data">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
    <script>
        $('#sql_invoice .btn').on('click', function() {

            $("#sql_invoice").validate({
                errorClass: "form-text text-danger is-invalid error",
                rules: {
                    i_date_start: {
                        required: true
                    },
                    i_date_end: {
                        required: true
                    }
                },
                messages: {
                    i_date_start: {
                        required: "Required field.",
                    },
                    i_date_end: {
                        required: "Required field.",
                    }
                }
            })

        })

        $('[id*=date_start]').on('change', function () {
            console.log('#' + this.name[0] + '_date_end');
            $('#' + this.name[0] + '_date_end').prop('min', this.value)
        })

        $('[id*=date_end]').on('change', function () {
            $('#' + this.name[0] + '_date_start').prop('max', this.value)
        })


        // $('input[id*=date]').on('change', function () {
        //     if (this.name.includes('end')) {
        //         $('#' + this.name[0] + '_date_start').prop('max', this.value)
        //     } else {
        //         $('#' + this.name[0] + '_date_end').prop('min', this.value)
        //     }
        // })

        $('#sql_payment .btn').on('click', function() {

            $("#sql_payment").validate({
                errorClass: "form-text text-danger is-invalid error",
                rules: {
                    p_date_start: {
                        required: true
                    },
                    p_date_end: {
                        required: true
                    }
                },
                messages: {
                    p_date_start: {
                        required: "Required field.",
                    },
                    p_date_end: {
                        required: "Required field.",
                    }
                }
            })

        })
    </script>
@endpush
