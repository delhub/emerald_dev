@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{ route('administrator.shipping-installations.ship_category.index') }}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Created Ship Category
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.shipping-installations.ship_category.store') }}" id="edit-courier-form" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Category Name <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="cat_name" name="cat_name" class="form-control" value="{{ old('cat_name') }}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                   Category Description <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="cat_desc" name="cat_desc" class="form-control" value="{{ old('cat_desc') }}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Category Type <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="cat_type" class="form-control">
                                    <option value="shipping" selected>Shipping</option>
                                    <option value="shipping_special">Shipping Special</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    For user <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select  class="select2 select2-edit form-control" style="width: 100%;" multiple="multiple"
                                    id="for_user" name="for_user[]">
                                    @foreach($userLevels as $level)
                                        <option value="{{ $level->id }}">{{ $level->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Start Date <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input name="start_date" type="text" class="form-control col-12 startDate" placeholder="Start Date"
                                    aria-label="Username" aria-describedby="basic-addon1" value="{{(old('start_date') != NULL) ? old('start_date') : NULL}}"  autocomplete="off">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    End Date <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input name="end_date" type="text" class="form-control col-12 endDate" placeholder="End Date"
                                    aria-label="Username" aria-describedby="basic-addon1" value="{{ old('end_date') }}" autocomplete="off">
                            </div>
                        </div>

                        {{-- <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Status <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="active" class="form-control">
                                    <option value="0" {{  old('cat_desc') == 0 ? 'selected' : '' }} >Inactive</option>
                                    <option value="1"{{  old('cat_desc')  == 1 ? 'selected' : '' }}>Active</option>
                                </select>
                            </div>
                        </div> --}}

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
<script>
$(document).ready(function () {

    $(".startDate").datepicker({
        dateFormat: 'yy-mm-d',
        minDate: 1,
        changeMonth: true,
        changeYear: true
    });

    $(".endDate").datepicker({
        dateFormat: 'yy-mm-d',
        minDate: 2,
        changeMonth: true,
        changeYear: true
    });
    
});
</script>
@endpush

