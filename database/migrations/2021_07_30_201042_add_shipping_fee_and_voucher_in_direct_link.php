<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingFeeAndVoucherInDirectLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            $table->text('voucher')->after('purchase_id')->nullable();
            $table->text('shipping_fee')->after('purchase_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            //
        });
    }
}
