@extends('layouts.administrator.mainVue')

@section('content')
@php

@endphp

<h1>Discount Rule</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/discount/create-rule" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Rule</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-agent-registration-tab" data-toggle="tab" href="#nav-agent-registration" @@click="getRuleTabs('agent_registration')" role="tab" aria-controls="nav-agent-registration" aria-selected="false"><strong>Agent Registration</strong></a>
                  <a class="nav-item nav-link" id="nav-auto-apply-tab" data-toggle="tab" href="#nav-auto-apply" @@click="getRuleTabs('auto_apply')" role="tab" aria-controls="nav-auto-apply" aria-selected="false"><strong>Auto Apply</strong></a>
                  <a class="nav-item nav-link" id="nav-generate-checkout-tab" data-toggle="tab" href="#nav-generate-checkout" @@click="getRuleTabs('generate_checkout')" role="tab" aria-controls="nav-generate-checkout" aria-selected="true"><strong>Generate Checkout</strong></a>
                  <a class="nav-item nav-link" id="nav-manual-apply-tab" data-toggle="tab" href="#nav-manual-apply" role="tab" @@click="getRuleTabs('manual_apply')" aria-controls="nav-manual-apply" aria-selected="false"><strong>Manual Apply</strong></a>
                  <a class="nav-item nav-link" id="nav-trade-in-tab" data-toggle="tab" href="#nav-trade-in" role="tab" @@click="getRuleTabs('tradeIn')" aria-controls="nav-trade-in" aria-selected="false"><strong>Trade In</strong></a>
                  <a class="nav-item nav-link" id="nav-purchase-tab" data-toggle="tab" href="#nav-purchase" role="tab" @@click="getRuleTabs('purchase')" aria-controls="nav-purchase" aria-selected="false"><strong>Purchase</strong></a>
                  <a class="nav-item nav-link" id="nav-refund-tab" data-toggle="tab" href="#nav-refund" role="tab" @@click="getRuleTabs('refund')" aria-controls="nav-refund" aria-selected="false"><strong>Refund</strong></a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-agent-registration" role="tabpanel" aria-labelledby="nav-agent-registration-tab">

                    <v-client-table :columns="columns" :data="tabs" :options="options">
                        <span slot="name" slot-scope="p">
                            <p>@{{ p.row.country_id }} - @{{ p.row.name }}</p>
                        </span>
                        <span slot="coupon_type" slot-scope="p">
                            <p>@{{ p.row.discount_type.toUpperCase() }} | @{{ p.row.coupon_type.toUpperCase() }}</p>
                        </span>
                        <span slot="maximum" slot-scope="p">
                            <p>@{{ p.row.minimum }} - @{{ p.row.maximum }}</p>
                        </span>
                        <span slot="general_type" slot-scope="p">
                            <p class="small">@{{ p.row.rule_type.toUpperCase() }} <br> @{{ p.row.general_type.toUpperCase() }}</p>
                        </span>
                        <span slot="step" slot-scope="p">
                            <p>
                                <span class="small">Step/Breakdown:</span> <span class="float-right">@{{ p.row.step }}</span><br>
                                <span class="small">Voucher Value:</span> <span class="float-right">@{{ p.row.value }}</span><br>
                                <span class="small">Monthly/Max Usage:</span> <span class="float-right">@{{ p.row.limit }}</span><br>
                                <span class="small">Available For:</span> <span class="float-right">@{{ p.row.available_for }}</span>
                            </p>
                        </span>
                        <span slot="expiry_date" slot-scope="p">
                            <p v-show="p.row.expiry_date_type ==0 || p.row.expiry_date_type ==3">@{{ p.row.expiry_date_type == 3 ? p.row.breakdown_exp_date + ' Day' : p.row.expiry_date }}</p>
                            <p v-show="p.row.expiry_date_type ==1 || p.row.expiry_date_type ==2">@{{ p.row.breakdown_exp_date }} <span>@{{ p.row.expiry_date_type == 1 ? 'Month' : 'Year' }}</span></p>
                        </span>
                        <span slot="__action" slot-scope="p">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ruleEdit"  @@click="getRuleById(p.row.id)">Edit</button>
                            <button type="button" class="btn btn-danger" @@click="deleteRule(p.row.id,'agent-registration')">Delete</button>
                        </span>

                    </v-client-table>

                </div>
                <div class="tab-pane fade" id="nav-auto-apply" role="tabpanel" aria-labelledby="nav-auto-apply-tab">

                    <v-client-table :columns="columns" :data="tabs" :options="options">
                        <span slot="name" slot-scope="p">
                            <p>@{{ p.row.country_id }} - @{{ p.row.name }}</p>
                        </span>
                        <span slot="coupon_type" slot-scope="p">
                            <p>@{{ p.row.discount_type.toUpperCase() }} | @{{ p.row.coupon_type.toUpperCase() }}</p>
                        </span>
                        <span slot="maximum" slot-scope="p">
                            <p>@{{ p.row.minimum }} - @{{ p.row.maximum }}</p>
                        </span>
                        <span slot="general_type" slot-scope="p">
                            <p class="small">@{{ p.row.rule_type.toUpperCase() }} <br> @{{ p.row.general_type.toUpperCase() }}</p>
                        </span>
                        <span slot="step" slot-scope="p">
                            <p>
                                <span class="small">Step/Breakdown:</span> <span class="float-right">@{{ p.row.step }}</span><br>
                                <span class="small">Voucher Value:</span> <span class="float-right">@{{ p.row.value }}</span><br>
                                <span class="small">Monthly/Max Usage:</span> <span class="float-right">@{{ p.row.limit }}</span><br>
                                <span class="small">Available For:</span> <span class="float-right">@{{ p.row.available_for }}</span>
                            </p>
                        </span>
                        <span slot="expiry_date" slot-scope="p">
                            <p v-show="p.row.expiry_date_type ==0">@{{ p.row.expiry_date }}</p>
                            <p v-show="p.row.expiry_date_type ==1 || p.row.expiry_date_type ==2">@{{ p.row.breakdown_exp_date }} <span>@{{ p.row.expiry_date_type == 1 ? 'Month' : 'Year' }}</span></p>
                        </span>
                        <span slot="__action" slot-scope="p">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ruleEdit"  @@click="getRuleById(p.row.id)">Edit</button>
                            <button type="button" class="btn btn-danger" @@click="deleteRule(p.row.id,'auto_apply')">Delete</button>
                        </span>

                    </v-client-table>

                </div>
                <div class="tab-pane fade" id="nav-generate-checkout" role="tabpanel" aria-labelledby="nav-generate-checkout-tab">

                    <v-client-table :columns="columns" :data="tabs" :options="options">
                        <span slot="name" slot-scope="p">
                            <p>@{{ p.row.country_id }} - @{{ p.row.name }}</p>
                        </span>
                        <span slot="coupon_type" slot-scope="p">
                            <p>@{{ p.row.discount_type.toUpperCase() }} | @{{ p.row.coupon_type.toUpperCase() }}</p>
                        </span>
                        <span slot="maximum" slot-scope="p">
                            <p>@{{ p.row.minimum }} - @{{ p.row.maximum }}</p>
                        </span>
                        <span slot="general_type" slot-scope="p">
                            <p class="small">@{{ p.row.rule_type.toUpperCase() }} <br> @{{ p.row.general_type.toUpperCase() }}</p>
                        </span>
                        <span slot="step" slot-scope="p">
                            <p>
                                <span class="small">Step/Breakdown:</span> <span class="float-right">@{{ p.row.step }}</span><br>
                                <span class="small">Voucher Value:</span> <span class="float-right">@{{ p.row.value }}</span><br>
                                <span class="small">Monthly/Max Usage:</span> <span class="float-right">@{{ p.row.limit }}</span><br>
                                <span class="small">Available For:</span> <span class="float-right">@{{ p.row.available_for }}</span>
                            </p>
                        </span>
                        <span slot="expiry_date" slot-scope="p">
                            <p v-show="p.row.expiry_date_type ==0">@{{ p.row.expiry_date }}</p>
                            <p v-show="p.row.expiry_date_type ==1 || p.row.expiry_date_type ==2">@{{ p.row.breakdown_exp_date }} <span>@{{ p.row.expiry_date_type == 1 ? 'Month' : 'Year' }}</span></p>
                        </span>
                        <span slot="__action" slot-scope="p">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ruleEdit"  @@click="getRuleById(p.row.id)">Edit</button>
                            <button type="button" class="btn btn-danger" @@click="deleteRule(p.row.id,'generate_checkout')">Delete</button>
                        </span>

                    </v-client-table>

                </div>
                <div class="tab-pane fade" id="nav-manual-apply" role="tabpanel" aria-labelledby="nav-manual-apply-tab">

                    <v-client-table :columns="columns" :data="tabs" :options="options">
                        <span slot="name" slot-scope="p">
                            <p>@{{ p.row.country_id }} - @{{ p.row.name }}</p>
                        </span>
                        <span slot="coupon_type" slot-scope="p">
                            <p>@{{ p.row.discount_type.toUpperCase() }} | @{{ p.row.coupon_type.toUpperCase() }}</p>
                        </span>
                        <span slot="maximum" slot-scope="p">
                            <p>@{{ p.row.minimum }} - @{{ p.row.maximum }}</p>
                        </span>
                        <span slot="general_type" slot-scope="p">
                            <p class="small">@{{ p.row.rule_type.toUpperCase() }} <br> @{{ p.row.general_type.toUpperCase() }}</p>
                        </span>
                        <span slot="step" slot-scope="p">
                            <p>
                                <span class="small">Step/Breakdown:</span> <span class="float-right">@{{ p.row.step }}</span><br>
                                <span class="small">Voucher Value:</span> <span class="float-right">@{{ p.row.value }}</span><br>
                                <span class="small">Monthly/Max Usage:</span> <span class="float-right">@{{ p.row.limit }}</span><br>
                                <span class="small">Available For:</span> <span class="float-right">@{{ p.row.available_for }}</span>
                            </p>
                        </span>
                        <span slot="expiry_date" slot-scope="p">
                            <p v-show="p.row.expiry_date_type ==0">@{{ p.row.expiry_date }}</p>
                            <p v-show="p.row.expiry_date_type ==1 || p.row.expiry_date_type ==2">@{{ p.row.breakdown_exp_date }} <span>@{{ p.row.expiry_date_type == 1 ? 'Month' : 'Year' }}</span></p>
                        </span>
                        <span slot="__action" slot-scope="p">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ruleEdit"  @@click="getRuleById(p.row.id)">Edit</button>
                            <button type="button" class="btn btn-danger" @@click="deleteRule(p.row.id,'manual_apply')">Delete</button>
                        </span>

                    </v-client-table>

                </div>
                <div class="tab-pane fade" id="nav-trade-in" role="tabpanel" aria-labelledby="nav-trade-in-tab">

                    <v-client-table :columns="tradein_table" :data="tabs" :options="options">
                        <span slot="name" slot-scope="p">
                            <p>@{{ p.row.country_id }} - @{{ p.row.name }}</p>
                        </span>
                        <span slot="__action" slot-scope="p">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ruleEdit"  @@click="getRuleById(p.row.id)">Edit</button>
                            <button type="button" class="btn btn-danger" @@click="deleteRule(p.row.id,'tradeIn')">Delete</button>
                        </span>

                    </v-client-table>

                </div>
              </div>

              <!-- Start Edit Modal -->
              <form v-on:submit.prevent>
                @csrf
            <div class="modal fade" :id="'ruleEdit'" tabindex="-1" role="dialog" aria-labelledby="RuleEditLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                    <h5 class="modal-title" id="RuleEditLabel">Edit Rule #@{{rules.id}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="col-md-12 mt-2" v-show="dupdated">
                        <div class="alert alert-success">
                            <strong>Success!</strong> Data has been updated!...</a>.
                          </div>
                    </div>
                    <div class="modal-body" style="height: 60vh; overflow-y: auto;">

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="rule_type">Rule Type</label>
                                    <select v-model="rules.rule_type" class="form-control">
                                    <option value="agent_registration">Agent Registration</option>
                                    <option value="auto_apply">Auto Apply</option>
                                    <option value="manual_apply">Manual Apply</option>
                                    <option value="generate_checkout">Generate After Checkout Status</option>
                                    <option value="tradeIn">Trade In</option>
                                    <option value="purchase">Purchase With Purchase</option>
                                    <option value="refund">Refund</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Rule Name</label>
                                <input type="text" v-model="rules.name" class="form-control">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="country">Country</label>
                                    <select v-model="rules.country_id" class="form-control">
                                    <option value="MY">Malaysia</option>
                                    <option value="SG">Singapore</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Available For</label>
                                    <select v-model="rules.available_for" class="form-control">
                                    <option value="all">All</option>
                                    <option value="personal">Personal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="coupon_type">Discount Calculation</label>
                                    <select v-model="rules.coupon_type" class="form-control">
                                    <option value="value">Value</option>
                                    <option value="percentage">Percentage</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="discount_type">Discount by</label>
                                    <select v-model="rules.discount_type" class="form-control">
                                    <option value="quantity">Quantity</option>
                                    <option value="price">Price</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row" v-if="(rules.coupon_type =='percentage' && rules.rule_type != 'auto_apply') || rules.general_type =='pairing'">
                                <div class="form-group col-md-12">
                                    <label for="discount_type">Auto Generate Rule ID (Pairing)</label>
                                    <select v-model="rules.generate_rule_id" class="form-control">
                                    <option value="0">Select Auto Generate rule</option>
                                    <option v-for="(m,i) in generate_id" :value="m.id">@{{m.id}} - @{{m.name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Minimum <span v-if="rules.discount_type =='quantity'">quantity</span><span v-else>cart total</span></label>
                                    <input v-model="rules.minimum" type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Maximum <span v-if="rules.discount_type =='quantity'">quantity</span><span v-else>cart total</span></label>
                                    <input v-model="rules.maximum" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="general_type" class="text-danger">General Type</label>
                                    <select v-model="rules.general_type" class="form-control">
                                        <option value="normal">Normal</option>
                                        <option value="breakdown">Breakdown</option>
                                        <option value="pairing">Pairing</option>
                                        <option value="stepBreakdown">Step Breakdown</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label><span v-if="rules.general_type =='breakdown'">Breakdown</span><span v-else>Step</span> value</label>
                                    <input v-model="rules.step" type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Voucher value</label>
                                    <input v-model="rules.value" type="text" class="form-control">
                                    <span class="form-text text-muted small">Per-step value</span>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Times Exceed</label>
                                    <input v-model="rules.times_exceed" type="text" class="form-control">
                                    <span class="form-text text-muted small">Times Exceed</span>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label><span v-if="rules.general_type =='breakdown'">Monthly Max Usage</span><span v-else>Max Usage</span></label>
                                    <input type="text" v-model="rules.limit" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Limit Type</label>
                                    <select v-model="rules.limit_type" @@change="getLimit(rules.limit_type)" class="form-control">
                                        <option value="na">Not Available</option>
                                        <option value="ex_category">Exclude Category</option>
                                        <option value="in_category">Include Category</option>
                                        <option value="ex_product">Exclude Product</option>
                                        <option value="in_product">Include Product</option>
                                        <option value="ex_product_attribute">Exclude Product Attribute</option>
                                        <option value="in_product_attribute">Include Product Attribute</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" v-show="!limit_hide">
                                <label>Limit ID</label>
                                    <select v-model="limit_id" class="multi" multiple="multiple" style="width: 100%">
                                        <option v-if="(rules.limit_type == 'ex_category' || rules.limit_type == 'in_category' || rules.limit_type == 'ex_product' || rules.limit_type == 'in_product')" v-for="(l,i) in lid" :value="l.id">@{{l.name}}</option>
                                        <option v-if="(rules.limit_type == 'ex_product_attribute' || rules.limit_type == 'in_product_attribute')" v-for="(l,i) in lid" :value="l.product_code">@{{l.productName}} - (@{{l.attribute_name}}) ~ @{{l.product_code}}</option>
                                    </select>
                            </div>
                            {{-- <div class="form-group" v-if="(rules.limit_type == 'ex_product_attribute' || rules.limit_type == 'in_product_attribute')">
                                <label>Limit ID</label>
                                    <select v-model="limit_id" class="multi" multiple="multiple" style="width: 100%">
                                        <option v-for="(l,i) in lid" :value="l.product_code">@{{l.productName}} - (@{{l.attribute_name}}) ~ @{{l.product_code}}</option>
                                    </select>
                            </div> --}}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Start Date</label>
                                    <input type="date" v-model="rules.start_date" class="form-control" autocomplete="off">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>End Date</label>
                                    <input type="date" v-model="rules.end_date" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Expiry Date Type</label>
                                    <select v-model="rules.expiry_date_type" class="form-control">
                                        <option value="0">Fixed</option>
                                        <option value="3">Daily Range</option>
                                        <option value="1">Monthly Range</option>
                                        <option value="2">Yearly Range</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <div v-show="rules.expiry_date_type =='0'">
                                        <label>Expiry Date</label>
                                        <input type="date" v-model="rules.expiry_date" class="form-control" autocomplete="off">
                                    </div>
                                    <div v-show="rules.expiry_date_type =='3'">
                                        <label>Expiry Date</label>
                                        <select v-model="rules.breakdown_exp_date" class="form-control">
                                            <option value="0">-- Select daily Range --</option>
                                            @for ($i = 1; $i <= 30; $i++)
                                            <option value="{{$i}}">{{$i}} day</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div v-show="rules.expiry_date_type =='1'">
                                        <label>Expiry Date</label>
                                        <select v-model="rules.breakdown_exp_date" class="form-control">
                                            <option value="0">-- Select Monthly Range --</option>
                                            @for ($i = 1; $i <= 60; $i++)
                                            <option value="{{$i}}">{{$i}} Month</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div v-show="rules.expiry_date_type =='2'">
                                        <label>Expiry Date</label>
                                        <select v-model="rules.breakdown_exp_date" class="form-control">
                                            <option value="0">-- Select Yearly Range --</option>
                                            @for ($i = 1; $i <= 10; $i++)
                                            <option value="{{$i}}">{{$i}} Year</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Remarks</label>
                                <input type="text" v-model="rules.remarks" class="form-control">
                            </div>

                            <div class="mb-5" style="height: 200px;"></div>
                    </div>
                    <div class="modal-footer">
                    <input type="hidden" v-model="rules.generate_rule_id" v-if="rules.coupon_type !='percentage'">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" @@click="updateRule(rules.id)">Save changes</button>
                    </div>

                </div>
                </div>
            </div>
            </form>
            <!-- End Edit Modal -->




        </div>
    </div>
</div>

@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-tables-2@2.2.1/dist/vue-tables-2.min.js"></script>
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script>
Vue.use(VueTables.ClientTable);

    var app = new Vue({
    el: '#app',
    data: {
        rule: [],
        rules: [],
        lid: [],
        limit_id: [],
        limit_hide: false,
        generate_id: [],
        tabs: [],
        current_tabs: '',
        dupdated: false,
        columns: ['id','name','coupon_type','maximum','general_type','step','limit_type','limit_id','expiry_date','__action'],
        tradein_table: ['id','name','country_id','rule_type','value','__action'],
        options: {
          headings: {
            // discount_type: 'Discount By',
            coupon_type: 'Calculation',
            // minimum: 'Min',
            maximum: 'Min/Max',
            available_for: 'For',
            step: 'Option',
            limit_type: 'Restricted',
            limit_id: 'Limit ID',
            limit: 'Monthly/Max Usage',
            __action: 'Action'
          },
          perPage: 10
        }
    },
    mounted() { //created // mounted //beforeMount
        this.getRule()
        this.getRuleByType('manual_apply')
        this.getRuleTabs('agent_registration')
        //this.getGlobalProduct()
        //this.getCategory()
    },
    methods: {
        getGlobalProduct() {
        axios
            .get('/administrator/discount/get-global-product')
            .then(response => {
                this.lid = response.data
                })
            .catch(error => console.log(error))
        },
        getProductAttribute() {
        axios
            .get('/administrator/discount/get-product-attribute')
            .then(response => {
                this.lid = response.data
                })
            .catch(error => console.log(error))
        },
        getCategory() {
        axios
            .get('/administrator/discount/get-category')
            .then(response => {
                this.lid = response.data
                })
            .catch(error => console.log(error))
        },
        getRule() {
        axios
            .get('/administrator/discount/getRule')
            .then(response => {
                this.rule = response.data
                })
            .catch(error => console.log(error))
        },
        getRuleById(id) {
        axios
            .get('/administrator/discount/getRuleById/' + id)
            .then(response => {
                this.rules = response.data
                this.limit_id = response.data.limit_id

                app.getLimit(response.data.limit_type)
                })
            .catch(error => console.log(error))
        },
        getRuleTabs(type) {
            this.current_tabs = type
        axios
            .get('/administrator/discount/getRule?type=' + type)
            .then(response => {
                this.tabs = response.data
                })
            .catch(error => console.log(error))
        },
        getRuleByType(type) {
        axios
            .get('/administrator/discount/getRule?type=' + type)
            .then(response => {
                this.generate_id = response.data
                })
            .catch(error => console.log(error))
        },
        updateRule(id) {
            let ctabs = this.current_tabs
            this.dupdated = true

            axios.post('/administrator/discount/updateRule/' + id, {
                    id: id,
                    limit_id: this.limit_id,
                    data: this.rules
                })
                .then(function (response) {
                    app.getRuleTabs(ctabs)
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });

                setTimeout(() => { this.dupdated = false }, 3000)
        },
        deleteRule(id) {
            axios.post('/administrator/discount/deleteRule/' + id, {
                //
                })
                .then(function (response) {
                    app.getRule()
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getLimit(type) {
            switch(type) {
            case 'ex_category':
            case 'in_category':
                this.lid = [];
                app.getCategory()
                this.limit_hide = false
            break;
            case 'ex_product':
            case 'in_product':
                this.lid = [];
                app.getGlobalProduct()
                this.limit_hide = false
            break;
            case 'ex_product_attribute':
            case 'in_product_attribute':
                this.lid = [];
                app.getProductAttribute()
                this.limit_hide = false
            break;
            default:
                this.limit_hide = true
                this.limit_id = [0]
            }

            //console.log(type)
        },
        deleteRule(id,type) {
            axios.post('/administrator/discount/deleteRule/' + id, {
                //
                })
                .then(function (response) {
                    app.getRuleTabs(type)
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
    });
</script>
<script>
$(document).ready(function() {
    $('select.select2').select2({
        theme: 'bootstrap4',
    });
});

const selects = document.querySelectorAll('.multi')
selects.forEach((selectElement) => {
  new SlimSelect({
    select: selectElement
  })
});
</script>
@endpush
