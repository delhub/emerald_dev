<?php

namespace App\Console\Commands;

use App\Models\Globals\Products\Product;
use App\Models\Products\ProductBrand;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProductBrandLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'productbrandlogs:new';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new line for product and brands report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('product_brand_logs')->insert([
            'log_month' => Carbon::now(),
            'brand_count' => ProductBrand::count(),
            'product_count' => Product::whereIn('product_status',[1,2])->count(),
            'new_brand_count' => 0,
            'new_product_count' => 0,
            'product_suspended' => Product::where('display_only',1)->where('product_status',1)->count(),
            'product_terminate' => Product::where('product_status',2)->count(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
