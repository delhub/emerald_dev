<?php

namespace App\Http\Controllers\Administrator\FreeGift;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Gift\Freegift;
use App\Models\Outlet\Outlet;
use App\Traits\FreeGiftraits;

class FreeGiftController extends Controller
{
    use FreeGiftraits;

    public function index(Request $request) {
        $data = Freegift::orderBy('id','DESC')->paginate(20);
        return view('administrator.free-gift.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 20);
    }

    public function create() {
        $data = Outlet::orderBy('id','DESC')->pluck('outlet_name','id')->all();

        return view('administrator.free-gift.create',compact('data'));
    }

    public function store(Request $request) {

       $freegift = new Freegift;

       $freegift->outlet_id = 1;
       $freegift->gift_name = $request->gift_name;
       $freegift->gifted = $request->gifted;
       $freegift->contains = $freegift->gifted == 'quantity' ? $request->contains : json_encode($request->contains);
       $freegift->quantity = $request->quantity;
       $freegift->gift_type = $request->gift_type;
       $freegift->gift_product = $request->gift_type == 'product' ? $request->gift_product : json_encode($request->gift_product);
       $freegift->gift_price_value = isset($freegift->gift_type) == 'price_value' ? $request->gift_price_value : null;
       $freegift->gift_price_percentage = isset($freegift->gift_type) == 'price_percentage' ? $request->gift_price_percentage : null;
       $freegift->start_date = $request->start_date;
       $freegift->end_date = Carbon::createFromFormat('Y-m-d', $request->end_date)->endOfDay()->toDateTimeString();
       $freegift->save();

       return redirect()->route('freegift.index')
                        ->with('success','Freegift rules successfully created!');

    }

    public function CheckFreeGift(Request $request) {
        return $this->freeGift_AutoApply($request,2);
    }

    public function edit($id)
    {
        $freegift = Freegift::find($id);
        $data = Outlet::orderBy('id','DESC')->pluck('outlet_name','id')->all();

        return view('administrator.free-gift.edit',compact('data','freegift'));
    }

    public function update(Request $request, $id)
    {
        if($request->_method == 'PUT') {
            $freegift = Freegift::find($id);

            $freegift->status = $freegift->status == 1 ? 0 : 1;

            $freegift->save();

            return redirect()->route('freegift.index');
        }
        else {
            $input = $request->all();

            switch ($input["gift_type"]) {
                case 'multiproduct':
                    $input["gift_product"] = $input["gift_multiproduct"];
                    break;
                default:
            }

            if($input["gifted"] != 'quantity') {
                $input["quantity"] = null;
            }

            $input["end_date"] = Carbon::createFromFormat('Y-m-d', $input["end_date"])->endOfDay()->toDateTimeString();

            $freegift = Freegift::find($id);

            $freegift->update($input);

            return redirect()->route('freegift.index')
                            ->with('success','Freegift rules updated successfully');
        }


    }
}
