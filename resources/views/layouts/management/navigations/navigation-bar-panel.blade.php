@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp
<header class="app-header navbar navbar-bg-color navbar-border-bottom-color nav-height ">

    {{--Navbar icon mobile--}}
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <i class="fa fa-bars navigation-icon"></i>
    </button>

    {{--Navbar icon desktop--}}
    {{-- <button class="navbar-toggler sidebar-toggler d-md-down-none"  type="button" data-toggle="sidebar-lg-show">
        <i class="fa fa-bars navigation-icon"></i>
    </button> --}}

    <a class="navbar-brand" href="/shop">
        <img class="navbar-brand-full margin-logo-mobile margin-tablet" src="{{asset("storage/logo/bujishu-logo-$country.png")}}" width="85" height="50" alt="Bujishu Logo">
        <img class="navbar-brand-minimized" src="{{asset("storage/logo/bujishu-logo-$country.png")}}" width="30" height="30" alt="Bujishu Logo">
    </a>


    <h4 class="toptitle hidden-sm" style="color:#ffcc00;">Welcome to Panel Dashboard</h4>
    <ul class="nav navbar-nav ml-auto">
        <h5 style="margin-right:10px; color:#ffcc00" class="welcome-text ">{{Auth::user()->userInfo->full_name}}</h5>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="{{asset('storage/avatar/default-avatar.jpg')}}" alt="{{ Auth::user()->userInfo->full_name }}">
            </a>
            <div class="dropdown-menu dropdown-menu-right">

                <a class="dropdown-item profile-menu" href="{{ route('management.company.profile') }}" style="padding: 10px 0px 10px 20px;border-radius: 30px 30px 0 0;">
                    <img src="{{ asset('assets/images/icons/profile_b.png') }}" alt="Profile" style="height:23px; padding:0 10px 0 0px;"> Profile
                </a>

                <a class="dropdown-item myDashboard-menu" href="{{ route('shop.dashboard.customer.home') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="My Dashboard" style="height:23px; padding:0 10px 0 0px !important;">
                    My Dashboard
                </a>

                @hasrole('dealer')
                <a class="dropdown-item agentDashboard-menu" href="{{ route('agent.overview') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Agent Dashboard" style="height:23px; padding:0 10px 0 0px !important;"> Agent Dashboard
                </a>
                @endhasrole

                @hasrole('panel')
                <a class="dropdown-item panelDashboard-menu" href="{{ route('management.panel.home') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Panel Dashboard" style="height:23px; padding:0 10px 0 0px !important;"> Panel Dashboard
                </a>
                @endhasrole
                @hasrole('redemption')
                <a href="{{ route('administrator.redemption') }}" class="dropdown-item redemptionDashboard-menu"><img
                        src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Administrator"
                        style="height:23px; padding:0 10px 0 0px !important;"> Redemption</a>
                <!-- <a href="/administrator/sales" class="dropdown-item saleSummary-menu"><img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Sale Summary" style="height:23px; padding:0 10px 0 0px !important;"> Sale Summary</a> -->
                @endhasrole

                <a class="dropdown-item logout-menu" href="{{ route('logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();" style="border-radius: 0 0 30px 30px;">
                    <img src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout" style="height:23px; padding:0 10px 0 0px !important;"> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
    {{-- <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button> --}}


</header>

<style>
    @media(max-width:768px) {
        .hidden-sm {
            display: none;
        }

        .margin-logo-mobile {
            margin-right: 150px;
            margin-top: 10px;
        }
    }

    .profile-menu:active img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    .profile-menu:hover img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    /* .myDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .myDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .agentDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .agentDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .panelDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .panelDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .logout-menu:active, img{
    content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }*/

    .logout-menu:hover img {
        content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }
</style>