@inject('controller', 'App\Http\Controllers\Administrator\Product\MProductController')

@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <a href="{{ route('administrator.product') }}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
    </div>
</div>
<div class="card shadow-sm mt-3">
    <div class="card-body">

        @include('inc.messages')

        <div class="row">
            <div class="col-12">
                <h4>
                    Product Information ..
                </h4>
            </div>
            <div class="col-12">
                <form action="{{ route('administrator.product.updateCreate', ['id' => $globalProduct->id]) }}"
                    id="edit-global-product-form" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="col-12 row">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Product Name <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-4 form-group">
                            <input type="text" id="productName" name="productName" class="form-control"
                                value="{{ ($globalProduct && $globalProduct->name) ? $globalProduct->name : old('productName') }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>

                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Product Code
                            </p>
                        </div>
                        <div class="col-12 col-md-4 form-group">
                            <input type="text" name="productCode" id="productCode" class="form-control"
                                value="{{ $globalProduct->product_code }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 row">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Product Quality
                            </p>
                        </div>
                        <div class="col-12 col-md-4 form-group">
                            <select name="productQuality" id="productQuality" class="select2 form-control">
                                @foreach($qualities as $quality)
                                <option value="{{ $quality->id }}"
                                    {{ (($globalProduct && $globalProduct->quality_id) ? $globalProduct->quality_id == $quality->id  : old('productQuality') == $quality->id ) ? 'selected' : '' }}>{{ $quality->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Package Type
                            </p>
                        </div>
                        <div class="col-12 col-md-4 form-group">
                            <select name="product_type" id="product_type" class="select2 form-control">
                                <option value="single" {{ ($globalProduct->product_type == 'single') ? 'selected' : '' }}>Single</option>
                                <option value="vqs" {{ ($globalProduct->product_type == 'vqs') ? 'selected' : '' }}>Virtual Quantity Stock (VQS)</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-12">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Panel Account ID <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-4 form-group">
                            <select name="panel_id" id="panel_id" class="select2 form-control">
                                <option value="">Please choose a panel.</option>
                                @foreach($panels as $panel)
                                <option value="{{ $panel->account_id }}"
                                    {{ ($panelproduct->panel_account_id == $panel->account_id) ? 'selected' : '' }}>
                                    {{ $panel->account_id }} - {{ $panel-> company_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Panel name for display
                            </p>
                        </div>
                        <div class="col-12 col-md-4 form-group">
                            <select name="display_panel_name" id="display_panel_name"
                                class="select2 form-control">
                                <option value="">Please choose a company name for display.</option>

                                @foreach($panels as $panel)
                                <option value="{{ $panel->company_name}}"
                                    {{ ($panelproduct->display_panel_name == $panel->company_name) ? 'selected' : '' }}>
                                    {{ $panel->account_id }} -
                                    {{ $panel->company_name }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>


                    <!-- Ingredients new -->
                    <div class="row">
                        <div class="col-12 col-md-2 text-md-right ">
                            <p>
                                Ingredients
                            </p>
                        </div>
                        <div class="col-12 col-md-9" id="availableInContainer">
                            @if($globalProduct->ingredients != NULL)
                            @php
                            $ingredientsDecode = json_decode($globalProduct->ingredients);
                            @endphp
                            @foreach (collect($ingredientsDecode) as $key => $ingredient)
                            <div class="row my-2 @if ($loop->last) default-item @endif">
                                <div class="col-12 col-md-5">
                                    <input type="text" name="ingredients_key[]" class="select2 form-control available-in" value="{{ $key }}">
                                </div>
                                <div class="col-12 col-md-6 row">
                                    <div class="col-md-10">
                                        <textarea name="ingredients_desc[]" class="select2 form-control available-in" value="{{ $ingredient }}">{{ $ingredient }}</textarea>
                                    </div>
                                    <div class="col-md-2 my-auto p-0">
                                        {{-- @if ($loop->count > 1) --}}
                                        <button type="button" class="btn btn-danger" onclick="removeAvailableInElement(this);">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        {{-- @endif --}}
                                        @if ($loop->last)
                                        <button type="button" class="btn btn-success" onclick="addMoreAvailableInElement();">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="row my-2 default-item">
                                <div class="col-12 col-md-4">
                                    <input type="text" name="ingredients_key[]" class="select2 form-control available-in" value="">
                                </div>
                                <div class="col-12 col-md-8 row">
                                    <div class="col-md-10">
                                        <textarea name="ingredients_desc[]" class="select2 form-control available-in" value=""></textarea>
                                    </div>
                                    <div class="col-md-2 my-auto p-0">
                                        <button type="button" class="btn btn-danger d-none" onclick="removeAvailableInElement(this);">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-success" onclick="addMoreAvailableInElement();">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row offset-md-1 mb-2 pl-4">
                                <div class="custom-control custom-checkbox col-12 col-md-12 offset-md-1">
                                    <input type="checkbox" class="custom-control-input" value="1" name="show_warning" id="show_warning"
                                    {{ ($globalProduct) && ($globalProduct->show_warning == 1)  ? "checked" : "" }}>
                                    <label class="custom-control-label text-danger" for="show_warning"><b>Tick to show this Warnings / Caution</b></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <p>
                                        Warnings / Caution
                                    </p>
                                </div>
                                <div class="custom-control custom-checkbox col-12 col-md-10">
                                    <textarea name="product_content_5" id="product_content_5" cols="30" rows="10" class="form-control summernote">
                                        {!! $panelproduct->product_content_5 !!}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row pt-4">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <p>
                                        Product Description
                                    </p>
                                </div>
                                <div class="col-12 col-md-10 form-group">
                                    <textarea name="product_description" id="product_description" cols="30" rows="10"
                                        class="form-control summernote">
                                    {!! $panelproduct->product_description !!}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-1 text-md-right my-auto">
                            <p>
                                Usage & Dossage
                            </p>
                        </div>
                        <div class="col-12 col-md-5 form-group">
                            <textarea name="product_content_2" id="product_content_2" cols="30" rows="10"
                                class="form-control summernote">
                                {!! $panelproduct->product_content_2 !!}
                            </textarea>
                        </div>

                        <div class="col-12 col-md-1 text-md-right my-auto">
                            <p>
                                Summary
                            </p>
                        </div>
                        <div class="col-12 col-md-5 form-group">
                            <textarea name="product_content_1" id="product_content_1" cols="30" rows="10"
                                class="form-control summernote">
                                {!! $panelproduct->product_content_1 !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-1 text-md-right my-auto">
                            <p>
                                Suitable For
                            </p>
                        </div>
                        <div class="col-12 col-md-5 form-group">
                            <textarea name="product_content_3" id="product_content_3" cols="30" rows="10"
                                class="form-control summernote">
                                {!! $panelproduct->product_content_3 !!}
                            </textarea>
                        </div>
                        <div class="col-12 col-md-1 text-md-right my-auto">
                            <p>
                                Short Description
                            </p>
                        </div>
                        <div class="col-12 col-md-5 form-group">
                            <textarea name="short_description" id="short_description" cols="30" rows="10"
                                class="form-control summernote">
                                {!! $panelproduct->short_description !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-1 text-md-right my-auto">
                            <p>
                                Product Article
                            </p>
                        </div>
                        <div class="col-12 col-md-5 form-group">
                            <textarea name="product_content_4" id="product_content_4" cols="30" rows="10"
                                class="form-control summernote">
                                {!! $panelproduct->product_content_4 !!}
                            </textarea>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="row">
                                <div class="col-6 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Featured Image<small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-6 col-md-8 form-group">
                                    <img id="featuredPreview"
                                        src="@if ($globalProduct->defaultImage) {{ asset('storage/' . $globalProduct->defaultImage->path . $globalProduct->defaultImage->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif"
                                        alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                                    <input type="file" name="productImage" id="productImage" class="form-control-file"
                                        onchange="document.getElementById('featuredPreview').src = window.URL.createObjectURL(this.files[0])">
                                    <small>Please make sure image is in 1:1 display ratio (square).</small>
                                </div>
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Image Gallery
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <button type="button" class="btn btn-secondary shadow-sm" data-toggle="modal"
                                        data-target="#imageGalleryModal">Add More Image</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row pl-5 mx-5 my-4">
                        <div class="row p-0 col-md-9 mx-auto border border-primary">
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="1" name="mespayPurchaseable" id="mespayPurchaseable"
                                        {{ ($globalProduct->mespay_purchaseable == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="mespayPurchaseable"><b>Mes Pay Purchase</b></label>
                                </div>
                            </div>

                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="1" name="keySelection" id="canPreorder"
                                        {{ ($globalProduct->key_selection == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="canPreorder"><b>Key Selection</b></label>
                                </div>
                            </div>

                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="1" name="hotSelection" id="canRemark"
                                        {{ ($globalProduct->hot_selection == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="canRemark"><b>Hot Selection</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" value="1" name="displayOnly" id="displayOnly"
                                        {{ ($globalProduct->display_only == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="displayOnly"><b>Display Only</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="canPreorder" id="canPreorder"
                                        {{ ($globalProduct->can_preorder == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="canPreorder"><b>Can Pre Order</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="canRemark" id="canRemark"
                                        {{ ($globalProduct->can_remark == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="canRemark"><b>Can Add Remarks</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="canSelfcollect"
                                        id="canSelfcollect" {{ ($globalProduct->can_selfcollect == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="canSelfcollect"><b>Can Self Collect</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="eligibleDiscount" id="eligibleDiscount"
                                        {{ ($globalProduct->eligible_discount == 0) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="eligibleDiscount"><b>Eligible Discount</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="generateDiscount" id="generateDiscount"
                                    {{ ($globalProduct->generate_discount == 0) ? 'checked' : '' }}>
                                <label class="custom-control-label" for="generateDiscount"><b>Generate Discount</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="productPublished"
                                        id="productPublished" {{ ($globalProduct->product_status != 2) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="productPublished"><b>Publish after saving.</b></label>
                                </div>
                            </div>
                            <div class="col-12 col-md-3 m-0 py-2 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="no_stockProduct" id="no_stockProduct"
                                        {{ ($globalProduct->no_stockProduct == 1) ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="no_stockProduct"><b>No Stock Product</b></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 row">


                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                Product Brand Logo
                            </p>
                        </div>
                        <div class="col-12 col-md-10 form-group">
                            <select name="brand_id" id="brand_id" class="select2 form-control">
                                <option value="0" >Select Brand...</option>
                                @foreach($productBrand as $brand)
                                <option value="{{ $brand->id }}"
                                    {{ ($globalProduct->brand_id == $brand->id) ? 'selected' : '' }}>{{ $brand->name }}
                                </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    @foreach ($controller::$category_type as $key => $category_name)

                    <div class="row">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <p>
                                {{$category_name}} @if ($key == 'shop' || $key == 'origin_country') <small class="text-danger">*</small> @endif
                            </p>
                        </div>

                        <div class="col-12 col-md-10 form-group">
                            <select class="select2 form-control productCategory" id="{{$key}}Category" name="{{$key}}categories[]" multiple="multiple">
                                @foreach($categories->where('type', $key)->sortBy('label'); as $category)
                                    <option value="{{ $category->id }}" {{ ($globalProduct->categories->contains($category->id)) ? 'selected' : '' }}>{{ $category->label }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endforeach



                    <hr >
                    <!-- Delivery Coondition -->

                    <div class="row my-2">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <h4 class="mb-3"><u>Product Delivery..</u></h4>
                        </div>
                    </div>

                    <div class="row col-12 my-3">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            Ships From
                        </div>
                        <div class="col-6 col-md-4">
                            <select name="ship_from" id="ship_from" class="select2 form-control">
                                <option value="">Please choose origin state..</option>
                                @foreach($states as $state)
                                <option value="{{ $state->id }}"
                                    {{ ($panelproduct->origin_state_id == $state->id) ? 'selected' : '' }}>
                                    {{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12 col-md-2 text-md-right my-auto">
                            Estimate Ship Out Date (days)
                        </div>
                        <div class="col-6 col-md-4">
                            <input type="number" name="estimate_ship_out_date" id="estimate_ship_out_date"
                            class="form-control" value="{{ $panelproduct->estimate_ship_out_date }}">
                        </div>
                    </div>

                    @if ($user->hasRole('administrator') || $user->hasRole('management')|| $user->hasRole('formula_pic'))

                        <div class="row">
                            <div class="col-12 col-md-2 text-md-right my-auto">
                                <p>
                                    Mark Up Category(Price)
                                </p>
                            </div>
                            <div class="col-12 col-md-5 form-group">
                                <select name="markup_category[]" id="markup_category"
                                    class="select2 form-control">
                                    <option value="0">Choose one markup category</option>
                                    @foreach($markupCategories as $category)
                                    <option value="{{ $category->id }}" {{ $panelproduct->markupCategories && $panelproduct->markupCategories->contains($category->id) ? 'selected' : '' }}
                                    >Name : {{ $category->cat_name }}, Description : {{ $category->cat_desc}}, Markup Rate {{ $category->markup_rate}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 col-md-2 text-md-right my-auto">
                                <p>
                                    Show Fixed Price
                                </p>
                            </div>
                            <div class="col-12 col-md-5 form-group">
                                <input class="form-check-input" type="checkbox" name="fixed_showitem"
                                id="fixed_showitem" value="1" style="margin-left: 0rem;margin-top: 10px;"
                                {{ ($panelproduct) && ($panelproduct->fixed_showitem == 1)  ? "checked" : "" }}> {{$panelproduct->fixed_showitem}}
                            <label class="form-check-label" for="fixed_showitem"
                                style="margin-left: 20px;margin-top: 5px;"><small
                                    class="text-danger">*</small>if click will show Fixed Price</label>
                            </div>
                        </div>

                    @endif
                    <hr >


                    <div class="row">
                        <div class="col-12 col-md-2 text-md-right my-auto">
                            <h4 class="mb-3">Product Variations ..</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 ml-5 pr-5">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                  <a class="nav-item nav-link active" id="nav-active-tab" data-toggle="tab" href="#nav-active" role="tab" aria-controls="nav-active" aria-selected="true">Active</a>
                                  <a class="nav-item nav-link" id="nav-inactive-tab" data-toggle="tab" href="#nav-inactive" role="tab" aria-controls="nav-inactive" aria-selected="false">Inactive</a>
                                </div>
                            </nav>
                            <div class="tab-content bg-light" id="nav-tabContent">
                                <div class="tab-pane fade show bg-light active" id="nav-active" role="tabpanel" aria-labelledby="nav-active-tab">
                                    <div class="row">
                                        <div class="col-12 col-md-2 text-md-right my-auto">
                                            {{-- <p>
                                                Variations <small class="text-danger">*</small>
                                            </p> --}}
                                        </div>


                                        <div class="col-12 col-md-12" id="variationContainer">
                                            <small>Product variation needs to have at least one.</small>
                                            @php
                                                $inactive = false;
                                            @endphp
                                            @if($panelproduct->attributes->count() > 0)
                                            @foreach($panelproduct->attributes as $attribute)
                                                @include('administrator.products.v1.panel.partial.product-variations')
                                                <br>
                                            @endforeach
                                            @else
                                            @php
                                                $attribute = null;
                                            @endphp
                                                @include('administrator.products.v1.panel.partial.product-variations')
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-inactive" role="tabpanel" aria-labelledby="nav-inactive-tab">
                                    @foreach($panelproduct->inactiveAttributes as $attribute)
                                    @php
                                        $inactive = true;
                                    @endphp
                                    @include('administrator.products.v1.panel.partial.product-variations')
                                    <br>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-- currently product type "single" only --}}
                    @if ($globalProduct->product_type =='bundle')
                        <hr>

                        <div class="row">
                            <div class="col-12 col-md-2 text-md-right my-auto">
                                <h5 class="mb-3">Product Bundle ..</h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-2 text-md-right my-auto">
                                <p>Set <small class="text-danger">*</small></p>
                            </div>

                            <div class="col-12 col-md-10" id="bundledesignContainer">
                                @if($panelproduct->bundles->count() > 0)
                                    @foreach($panelproduct->bundles as $bundle)

                                        <div class="col-12 bundle-box @if($loop->last) default-item @endif bundlebox" data-group="{{$bundle->bundle_id}}">
                                            <div class="row no-gutters p-2 mb-1" style="border: 1px solid #b3b3b3; border-radius: 5px;">
                                                <input type="hidden" class="product_bundle_id" name="product_bundle_id[]" id="newProductbundle" value="{{ $bundle->id }}">

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="bundle_id[]" class="select2 form-control bundle_id-in">
                                                        <option value="default">Select a bundle ID..</option>
                                                        <option value="1" {{ ($bundle->bundle_id == '1') ? 'selected' : '' }}>Type 1</option>
                                                        <option value="2" {{ ($bundle->bundle_id == '2') ? 'selected' : '' }}>Type 2</option>
                                                        <option value="3" {{ ($bundle->bundle_id == '3') ? 'selected' : '' }}>Type 3</option>
                                                        <option value="4" {{ ($bundle->bundle_id == '4') ? 'selected' : '' }}>Type 4</option>
                                                        <option value="5" {{ ($bundle->bundle_id == '5') ? 'selected' : '' }}>Type 5</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <input type="number" name="bundle_qty[]" id="bundle_qty" class="form-control input-mask-price d-inline bundle_qty-in" placeholder="Price" style="width: 100%;" value="{{ $bundle->bundle_qty }}">
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="bundle_type[]" class="select2 form-control bundle_type-in">asd
                                                        <option value="default">Select a bundle type..</option>
                                                        <option value="0" {{ ($bundle->bundle_type == '0') ? 'selected' : '' }}>Optional</option>
                                                        <option value="1" {{ ($bundle->bundle_type == '1') ? 'selected' : '' }}>Non - Optional</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="product_attribute_id[]" class="select2 form-control bundle_product_attribute_id-in">
                                                        <option value="default">Please choose a product code.</option>
                                                        {{-- @foreach ($globalProducts as $product)
                                                        <option value="{{ $product->id }}" {{ ($bundle->product_attribute_id == $product->id) ? 'selected' : '' }}>
                                                            {{$product->product_code}} - {{$product->name}} ({{$product->attribute_name}})
                                                        </option>
                                                        @endforeach --}}
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <input type="text" name="bundle_title[]" class="form-control bundle_title-in" placeholder="Title Name" value="{{ $bundle->bundle_title }}">
                                                </div>

                                                {{-- decode json, bundle variation --}}
                                                <?php $bundlejson = json_decode($bundle->bundle_variation,FALSE); ?>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="bundle_variation[]" class="select2 form-control">
                                                        <option value="default">Select a variation type..  </option>
                                                        <option value="color" {{ ($bundlejson->selection == 'color') ? 'selected' : '' }}>Color</option>
                                                        <option value="size" {{ ($bundlejson->selection == 'size') ? 'selected' : '' }}>Size</option>
                                                        <option value="light-temperature" {{ ($bundlejson->selection == 'light-temperature') ? 'selected' : '' }}>Light Temperature</option>
                                                        <option value="curtain-size" {{ ($bundlejson->selection == 'curtain-size') ? 'selected' : '' }}>Curtain Size</option>
                                                        <option value="miscellanous" {{ ($bundlejson->selection == 'miscellanous') ? 'selected' : '' }}>Miscellanous</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <input type="text" name="bundle_variation_selection[]"
                                                    class="form-control" placeholder="Name" value="{{ $bundlejson->name }}">
                                                </div>

                                                {{-- <div class="ml-auto col-2 p-1">
                                                    @if(!$loop->last)
                                                    <button type="button" class="btn btn-danger" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                    @else
                                                    <button type="button" class="btn btn-danger d-none" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                                        <i class="fa fa-minus"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-success" style="width: 100%;" onclick="addMoreBundleDesignSetElement();">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                    @endif
                                                </div> --}}
                                                {{-- <div class='error-message text-danger'></div> --}}
                                            </div>
                                        </div>
                                    @endforeach

                                @else

                                    <div class="col-12 bundle-box default-item" data-group="1">
                                        <div class="row no-gutters p-2 mb-1" style="border: 1px solid #b3b3b3; border-radius: 5px;">

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <select name="bundle_id[]" class="select2 form-control bundle_id-in">
                                                    <option value="default">Select a bundle ID..</option>
                                                    <option value="1">Type 1</option>
                                                    <option value="2">Type 2</option>
                                                    <option value="3">Type 3</option>
                                                    <option value="4">Type 4</option>
                                                    <option value="5">Type 5</option>
                                                </select>
                                            </div>

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <input type="number" name="bundle_qty[]" class="form-control bundle_qty-in" placeholder="bundle_qty">
                                            </div>

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <select name="bundle_type[]" class="select2 form-control bundle_type-in">
                                                    <option value="default">Select a bundle type..</option>
                                                    <option value="0">Optional</option>
                                                    <option value="1">Non - Optional</option>
                                                </select>
                                            </div>

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <select name="product_attribute_id[]" class="select2 form-control bundle_product_attribute_id-in">
                                                    <option value="default">Please choose a product code.</option>
                                                    {{-- @foreach ($products as $product)
                                                    <option value="{{ $product->id }}">{{$product->product_code}} - {{$product->name}} ({{$product->attribute_name}})</option>
                                                    @endforeach --}}
                                                </select>
                                            </div>

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <input type="text" name="bundle_title[]" class="form-control bundle_title-in" placeholder="Title Name">
                                            </div>

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <select name="bundle_variation[]" class="select2 form-control">
                                                    <option value="default">Select a variation type..</option>
                                                    <option value="color">Color</option>
                                                    <option value="size">Size</option>
                                                    <option value="light-temperature">Light Temperature</option>
                                                    <option value="curtain-size">Curtain Size</option>
                                                    <option value="miscellanous">Miscellanous</option>
                                                </select>
                                            </div>

                                            <div class="col-12 col-md-3 form-group p-1">
                                                <input type="text" name="bundle_variation_selection[]" class="form-control" placeholder="Name">
                                            </div>

                                            <div class="ml-auto col-2 p-1">
                                                <button type="button" class="btn btn-danger d-none" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                                    <i class="fa fa-minus"></i>
                                                </button>

                                                <button type="button" class="btn btn-success" style="width: 100%;" onclick="addMoreBundleDesignSetElement(this);">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>

                                            <div class='error-message text-danger'></div>

                                        </div>
                                    </div>

                                @endif
                            </div>
                        </div>
                    @endif

                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- Image Gallery Modal -->
<div class="modal fade" id="imageGalleryModal" tabindex="-1" role="dialog" aria-labelledby="imageGalleryModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <form method="post"
                            action="{{ route('administrator.products.store-image', ['productId' => $globalProduct->id]) }}"
                            enctype="multipart/form-data" class="dropzone" id="dropzone">
                            @csrf
                        </form>
                    </div>
                    <div class="col-12">
                        <small>Image will be stored automatically.</small>
                        <small>Please make sure image is in 1:1 display ratio (square).</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


 <!-- Empty Modal -->
 <div class="modal fade" id="ModalId" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Ajax response loaded here -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    // Dropzone
    Dropzone.options.dropzone = {
        maxFilesize: 12,
        renameFile: function(file) {
            var productId = '{{ $globalProduct->product_code }}';
            var dt = new Date();
            var time = dt.getTime();
            var ext = file.name.split('.').pop();
            return productId + '-' + file.name ;
        },
        init: function() {
            myDropzone = this;
            $.ajax({
                url: "{{ route('administrator.v1.products.get-image', ['id' => $globalProduct->id])}}",
                type: 'GET',
                data: {
                    request: 'fetch'
                },
                dataType: 'json',
                success: function(response) {
                    console.log(response);
                    $.each(response, function(key, value) {
                        var mockFile = {
                            filename: value.name,
                            size: value.size
                        };


                        myDropzone.emit("addedfile", mockFile);
                        myDropzone.emit("thumbnail", mockFile, "/storage/uploads/images/products/{{ $globalProduct->product_code }}/" + value.name);
                        myDropzone.emit("complete", mockFile);
                        myDropzone.files.push(mockFile);
                    });

                }
            });
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 50000,
        removedfile: function(file) {
            console.log(file.filename);
            var name;
            if (!file.filename) {
                name = file.upload.filename;
            } else {
                name = file.filename;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: 'POST',
                url: "{{ route('administrator.v1.products.delete-image', ['id' => $globalProduct->id]) }}",
                data: {
                    filename: name
                },
                success: function(data) {
                    console.log("File has been successfully removed!!");
                },
                error: function(e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {
            console.log(response);
        },
        error: function(file, response) {
            return false;
        }

    };

    function reloadImage(id)
    {
        console.log('reload_images');
        $( ".imgclr-"+id ).load(window.location.href + " .imgclr-"+id );
    }

    $(document).ready(function() {

        $('#canInstallment').click(function() {
            var element = document.getElementById("installmentDetails");
            element.classList.toggle("d-none");

                // var inputValue = 'check';
                // $("." + inputValue).toggle();
            });

            let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '99999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }

        //

        $('select.select2').select2({
            theme: 'bootstrap4',
        });

        $('.summernote').summernote({
            height: 300, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
        });

        // Variable Initialization.
        // Input into variables.
        const productName = $('#productName');
        const productCode = $('#productCode');
        const productQuality = $('#productQuality');

        // Input validation
        productName.on('keyup', function() {
            if ($(this).val().length == 0) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        productCode.on('keyup', function() {
            if ($(this).val().length == 0) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        // End input validation.

        // Form validation & submission.
        $('#edit-global-product-form').on('submit', function(event) {

            //temporary here
            return true;

            let errors = 0;

            // Validation
            if (productName.val().length == 0) {
                errors = errors + 1;
                productName.removeClass('is-valid');
                productName.addClass('is-invalid');
            }
            if (productCode.val().length == 0) {
                errors = errors + 1;
                productCode.removeClass('is-valid');
                productCode.addClass('is-invalid');
            }
            if (productQuality.val() == 'default') {
                errors = errors + 1;
                productQuality.removeClass('is-valid');
                productQuality.addClass('is-invalid');
            }
            if ($('.productCategory :selected').length == 0) {
                errors = errors + 1;
                $('.productCategory').removeClass('is-valid');
                $('.productCategory').addClass('is-invalid');
            }

            if (errors > 0) {
                toastr.error('Please make sure the input highlighted in RED is correctly filled in.');
                return false;
            } else {
                return true;
            }
        });


    });
</script>
@endpush

@push('style')
<style>
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 30px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 30px;
        margin-top: 0;
    }
    .dropzone .dz-preview .dz-image img{
        width: 100%;
    }
    .select2.select2-container {
        width: 100% !important;
    }
</style>
@endpush


{{-- panel product --}}
<!-- Styles -->
@push('script')
<script>

    // $.each($("input[name='cartItemId[]']:checked"), function() {
        // input[type="radio"]
    $("#normalDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', false);
    });
    $("#individualDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', false);
    });
    $("#groupDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', false);
    });
    $('.check input:checkbox').click(function() {
        $('.check input:checkbox').not(this).prop('checked', false);
    });
    //
    function addMoreAvailableInElement() {
        let availableInContainer = $('#availableInContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('id')
        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.find('select.select2').removeClass('is-invalid')
        cloneItem.find('select.select2').removeClass('is-valid')
        cloneItem.addClass('default-item');
        // cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeAvailableInElement(element) {
        $(element).parent().parent().parent().slideUp(500, function() {
            $(this).remove();
        });

        if ($(element).parent().parent().parent().hasClass('default-item')) {
            $(element).parent().parent().parent().prev().addClass('default-item');
            $(element).parent().parent().parent().prev().find('.btn.btn-success').removeClass('d-none');
        }
        $(element).parent().parent().parent().prev('default-item').find('.btn.btn-success').removeClass('d-none');

    }

    function addMoreProductVariationElement() {
        let availableInContainer = $('#variationContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');


        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        // cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.find('.product_variation_id').val('new');
        cloneItem.find('.hideRBS').attr('style','display:none;');
        cloneItem.find('.noClone').remove();

        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeProductVariationElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
        if ($(element).parent().parent().hasClass('default-item')) {
            // $(element).parent().parent().prev().prev().attr('style','border:1px red solid;');
            $(element).parent().parent().prev().prev().addClass('default-item');
            $(element).parent().parent().prev().prev().find('.btn.btn-success').removeClass('d-none');
        }
        $(element).parent().parent().prev().prev('default-item').find('.btn.btn-success').removeClass('d-none');
        // $(element).parent().parent().prev('default-item').attr('style','background:blue;');
    }

    function addMoreBundleDesignSetElement(element) {

        let availableInContainer = $(element).parent().find('.bundleRepeatableContainer');

        let currentItem = availableInContainer.find('.default-item2');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item2');


        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item2');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.find('.product_bundle_id').val('new');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();
        bundleDefaultAction(cloneItem);

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeBundleDesignSetElement(element) {
        $(element).parent().slideUp(500, function() {
            $(this).remove();
           // if($(element).parent().parent().children().length == 1){

           // }
        });
    }

    function addDeliveryRangeElement() {
        let deliveryRangeContainer = $('#deliveryRangeContainer')

        let currentItem = deliveryRangeContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.newID').val('new');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(deliveryRangeContainer);
        cloneItem.slideDown();

        deliveryRangeContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeDeliveryRangeElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

        $('.bundle-box').each(function(index, _selectGroup){
            bundleDefaultAction(_selectGroup);
        });

        function bundleDefaultAction(_selectGroup){

            $(_selectGroup).find('[name="bundle_id[]"]').change(function(){
                selectvalue = $(_selectGroup).find('[name="bundle_id[]"] option:selected').val();

                $(_selectGroup).data('group', selectvalue);
                $(_selectGroup).attr('data-group',selectvalue);

                saveBtn = document.querySelector('form #saveBtn');
                bundleAutoChange($(_selectGroup));
            });

            $(_selectGroup).find('[name="bundle_title[]"]').change(function(){
                bundleAutoChange($(_selectGroup));
            });

            $(_selectGroup).find('[name="bundle_qty[]"]').change(function(){
                bundleAutoChange($(_selectGroup));
            });

            $(_selectGroup).find('[name="bundle_type[]"]').change(function(){
                bundleAutoChange($(_selectGroup));
            });

        }

        function bundleAutoChange(_selectGroup){

            selectvalue = $(_selectGroup).data('group');
            bundle_title = $(_selectGroup).find('[name="bundle_title[]"]').val();
            bundle_qty = $(_selectGroup).find('[name="bundle_qty[]"]').val();
            bundle_type = $(_selectGroup).find('[name="bundle_type[]"]').val();

            bundle_title_error = true;
            bundle_qty_error = true;
            bundle_type_error = true;

            $('[data-group="'+selectvalue+'"]').each(function(index,_typeGroup){
               if ( $(_typeGroup).find('[name="bundle_title[]"]').val() != bundle_title) bundle_title_error = false;
               if ( $(_typeGroup).find('[name="bundle_qty[]"]').val() != bundle_qty) bundle_qty_error = false;
               if ( $(_typeGroup).find('[name="bundle_type[]"] option:selected').val() != bundle_type) bundle_type_error = false;
            });

            if(!bundle_title_error || !bundle_qty_error || !bundle_type_error){
                $(_selectGroup).find('.error-message').html('Type' +selectvalue+' : qty or choice or title are different');
                $(saveBtn).attr("disabled", true);
            }else{
                $(_selectGroup).find('.error-message').html('');
                $(saveBtn).attr("disabled", false);
            }

        }

        function InputMaskPrice(){
            let priceSelectors = document.getElementsByClassName('input-mask-price');

            let priceInputMask = new Inputmask({
                'mask': '99999.99',
                'numericInput': true,
                'digits': 2,
                'digitsOptional': false,
                'placeholder': '0'
            });

            for (var i = 0; i < priceSelectors.length; i++) {
                priceInputMask.mask(priceSelectors.item(i));
            }
        }

    $(document).ready(function() {
        InputMaskPrice();

        $('select.select2').select2({
            theme: 'bootstrap4',
        });

        $('.summernote').summernote({
            height: 200, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
        });

        // Variables Initialization
        // Input Into Variables.


        const globalproductName = $('#productName');
        const globalproductCode = $('#productCode');

        const shopCategory = $('#shopCategory');
        const agentCategory = $('#agentCategory');
        const accountingCategory = $('#accountingCategory');


        const panelAccountId = $('#panel_id');
        const panelDisplayName = $('#display_panel_name');



        const variationType = $('#attriButeType');
        const VariationName = $('#product_variation_name');


        const bundleType = $('#bundle_type');
        const bundleProduct = $('#product_attribute_id');

        // const price = $('#price');
        // const memberPrice = $('#member_price');
        // const bpRequired = $('#bp_required');
        // const productSv = $('#product_sv');
        // const variationName = $('#product_variation_name');
        // const bundleID = $('#bundle_id');
        // const bundleqty = $('#bundle_qty');
        // const shipFrom = $('#ships_from');
        // const shipState = $('#ships_state');
        // let delivery = $('.available-in');
        // let bundleTitle = $('.bundle_title-in');


        const price = $('#price');
        const memberPrice = $('#member_price');
        const bpRequired = $('#bp_required');
        const productSv = $('#product_sv');
        const productVariationSv = $('#product_variation_sv');
        const productCode = $('#product_variation_code');
        const bundleqty = $('#bundle_qty');
        const shipFrom = $('#ship_from');

        const shipCategory = $('#shipping_category');
        // const attributeType = $('#attriButeType');
        const attrcol = document.querySelectorAll('#colattr');
        // const imgcheck = document.querySelectorAll('#colattr #imgclr');

        // const attributeType = document.querySelectorAll('select#attriButeType');
        const myNotice = document.getElementById('mynote');
        const bundleGroup = document.querySelectorAll('.bundlebox');
        // const attValue = attributeType.options[attributeType.selectedIndex].value;
        const delivery = $('.available-in');
        // let bundleType = $('.bundle_type-in');
        // let bundleID = $('.bundle_id-in');
        // let bundleProduct = $('.bundle_product_attribute_id-in');
        // let bundleTitle = $('.bundle_title-in');


        globalproductName.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        globalproductCode.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        shopCategory.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        agentCategory.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        accountingCategory.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });


        panelAccountId.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        panelDisplayName.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        variationType.on('change', function() {
            if ($(this).val() == 'default') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        VariationName.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });


        price.on('keyup', function() {
            if ($(this).val() == 0) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        memberPrice.on('keyup', function() {
            if ($(this).val() == 0) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        //curtain images check
        // attrcol.forEach(attitem => {
        //     const a1 = attitem.querySelector('.type-attr');
        //     const b1 = attitem.querySelector('.curtain_size_icon');
        //     // console.log(typeof a1 + " " + 'type a1');

        //     a1.onchange = function() {
        //         console.log(a1.value);
        //         if(a1.value == 'default' || a1.value == 'curtain-size' && b1.value == '0'){
        //             a1.classList.add('is-invalid');
        //             b1.classList.add('is-invalid');
        //         } else {
        //             a1.classList.remove('is-invalid');
        //             b1.classList.remove('is-invalid');
        //         }
        //     };

        //     b1.onchange = function() {
        //         console.log(b1.value);
        //         if(b1.value == '0' && a1.value == 'curtain-size' ){
        //             a1.classList.add('is-invalid');
        //             b1.classList.add('is-invalid');
        //         } else {
        //             a1.classList.remove('is-invalid');
        //             b1.classList.remove('is-invalid');
        //         }
        //     }

        // });

        //bundle check
        bundleGroup.forEach(item =>{
            const a = item.querySelector('.bundle_id-in');
            const b = item.querySelector('.bundle_product_attribute_id-in');
            const c = item.querySelector('.bundle-variation');
            const h = item.querySelector('.bundle_type-in');
            const j = item.querySelectorAll('input');

            a.onchange = function(){
                if(a.value == 'default'){
                    a.classList.add('is-invalid');
                }else {
                    a.classList.remove('is-invalid');
                }
            };

            b.onchange = function(){
                if(b.value == 'default'){
                    b.classList.add('is-invalid');
                }else {
                    b.classList.remove('is-invalid');
                }
            };

            c.onchange = function(){
                if(c.value == 'default'){
                    c.classList.add('is-invalid');
                }else {
                    c.classList.remove('is-invalid');
                }
            };

            h.onchange = function(){
                if(h.value == 'default'){
                    h.classList.add('is-invalid');
                }else {
                    h.classList.remove('is-invalid');
                }
            };

            j.forEach(inputItem => {
                inputItem.onchange = function(){
                    if(inputItem.value == '' || inputItem.value < 1){
                        inputItem.classList.add('is-invalid');
                    }else {
                        inputItem.classList.remove('is-invalid');
                    }
                };
            });
        });
        /*
        bpRequired.on('keyup', function() {
            if ($(this).val == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        */
        productSv.on('keyup', function() {
            if ($(this).val == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        bundleqty.on('keyup', function() {
            if ($(this).val == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        shipFrom.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        // shipCategory.on('change', function() {
        //     if ($(this).val() == '') {
        //         $(this).removeClass('is-valid');
        //         $(this).addClass('is-invalid');
        //     } else {
        //         $(this).removeClass('is-invalid');
        //         $(this).addClass('is-valid');
        //     }
        // });

        // console.log(attributeType);
        // attributeType.forEach(function(testA){

        //     testA.addEventListener( 'change', function() {
        //         console.log(testA.classList);
        //         if(testA.value === 'default') {
        //             testA.classList.add('is-invalid');
        //             testA.classList.remove('is-valid');
        //         } else {
        //             testA.classList.remove('is-invalid');
        //             testA.classList.add('is-valid');
        //         }
        //     });
        // });

        $('#edit-global-product-form').on('submit', function(event) {
            let errors = 0;
            let bundleCount = 0;

            function countBundle(){
                bundleCount = bundleCount + 1;
            }

            function countAttr(){
                errors = errors + 1;
            }

            // Validation
            if (globalproductName.val() == '') {
                errors = errors + 1;
                globalproductName.removeClass('is-valid');
                globalproductName.addClass('is-invalid');
            }
            if (globalproductCode.val() == '') {
                errors = errors + 1;
                globalproductCode.removeClass('is-valid');
                globalproductCode.addClass('is-invalid');
            }

            if (shopCategory.val() == '') {
                errors = errors + 1;
                shopCategory.removeClass('is-valid');
                shopCategory.addClass('is-invalid');
            }
            if (agentCategory.val() == '') {
                errors = errors + 1;
                agentCategory.removeClass('is-valid');
                agentCategory.addClass('is-invalid');
            }
            if (accountingCategory.val() == '') {
                errors = errors + 1;
                accountingCategory.removeClass('is-valid');
                accountingCategory.addClass('is-invalid');
            }

            if (panelAccountId.val() == '') {
                errors = errors + 1;
                panelAccountId.removeClass('is-valid');
                panelAccountId.addClass('is-invalid');
            }
            if (panelDisplayName.val() == '') {
                errors = errors + 1;
                panelDisplayName.removeClass('is-valid');
                panelDisplayName.addClass('is-invalid');
            }


            variationType.each(function() {
                if ($(this).val() == 'default') {
                    errors = errors + 1;
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                }
            });
            variationName.each(function() {
                if ($(this).val() == 'default') {
                    errors = errors + 1;
                    $(this).removeClass('is-valid');
                    $(this).addClass('is-invalid');
                }
            });


            if (price.val() == 0) {
                errors = errors + 1;
                price.removeClass('is-valid');
                price.addClass('is-invalid');
            }
            if (memberPrice.val() == 0) {
                errors = errors + 1;
                memberPrice.removeClass('is-valid');
                memberPrice.addClass('is-invalid');
            }
            if (shipFrom.val() == '') {
                errors = errors + 1;
                shipFrom.removeClass('is-valid');
                shipFrom.addClass('is-invalid');
            };

            if (shipCategory.val() == '') {
                errors = errors + 1;
                shipCategory.removeClass('is-valid');
                shipCategory.addClass('is-invalid');
            };



            //special for curtain attribute type
            attrcol.forEach(attitem => {
                const cc = attitem.querySelector('.type-attr');
                const dd = attitem.querySelector('.curtain_size_icon');
                // console.log(bb.value);

                if(cc.value == 'curtain-size' && dd.value == '0' || cc.value == 'default'){
                    countAttr();
                }
            });

            //bundel all type validation
            bundleGroup.forEach(d =>{
                const a = d.querySelector('.bundle_id-in');
                const b = d.querySelector('.bundle_product_attribute_id-in');
                const c = d.querySelector('.bundle-variation');
                const f = d.querySelector('.bundle_type-in');
                const g = d.querySelector('.input-mask-price');
                const h = d.querySelectorAll('input');

                if(a.value == 'default' || b.value == 'default' || c.value == 'default' || f.value == 'default' || g.value == ''){
                    countBundle();
                }

                h.forEach(inputItemValid => {
                    if(inputItemValid.value == '' || inputItemValid.value < 1) {
                        countBundle();
                    }
                });

            });

            // const attributeType = document.querySelectorAll('#attriButeType');

            // attributeType.forEach(function(checkpass){
            //     if(checkpass.value === 'default') {
            //         errors = errors + 1;
            //         $(checkpass).removeClass('is-valid');
            //         $(checkpass).addClass('is-invalid');
            //     } else {
            //         $(checkpass).addClass('is-valid');
            //         $(checkpass).removeClass('is-invalid');
            //     }
            // });

            /*
            if (bpRequired.val() == '') {
                errors = errors + 1;
                bpRequired.removeClass('is-valid');
                bpRequired.addClass('is-invalid');
            }
            */

            // attrcol.forEach(function(checkimg) {
            //     if(checkedOne.checked == false) {
            //         errors = errors + 1;
            //         console.log('function');
            //         myNotice.setAttribute("style", "display:block;color:#f00;");
            //     }
            // });

                // if(checkedOne == false) {
                //     errors = errors + 1;
                //     console.log(checkedOne);
                //     // $(this).removeClass('is-valid');
                //     // $(myNotice).addClass('is-invalid');
                //     myNotice.setAttribute("style", "display:block;color:#f00;");
                // }


            if (productSv.val() == '') {
                errors = errors + 1;
                productSv.removeClass('is-valid');
                productSv.addClass('is-invalid');
            }

            // if ($('.productCategory :selected').val() == '0') {
            //     // if (productSv.val() == '') {
            //     errors = errors + 1;
            //     $('.productCategory').removeClass('is-valid');
            //     $('.productCategory').addClass('is-invalid');
            // }
            // if (bundleqty.val() == 'aaa') {
            //     errors = errors + 1;
            //     bundleqty.removeClass('is-valid');
            //     bundleqty.addClass('is-invalid');
            // }

            // if (bundleTitle.val() == '') {
            //     errors = errors + 1;
            //     bundleTitle.removeClass('is-valid');
            //     bundleTitle.addClass('is-invalid');
            // }

            // delivery = $('.available-in');

            // delivery.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            // bundleType = $('.bundle_type-in');

            // bundleType.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            // bundleID = $('.bundle_id-in');

            // bundleID.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            // bundleProduct = $('.bundle_product_attribute_id-in');

            // bundleProduct.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            if (errors > 0 || bundleCount > 0) {
                toastr.error('Please make sure the input highlighted in RED is correctly filled in.');
                return false;
            } else {
                return true;
            }
        });

        let editURL;
        let updateURL;

        $('#app').on('click', '.edit-price', function() {
            // Price Edit URL - GET
            editURL = "{{ route('administrator.v1.products.panels.price.edit', ['id' => 'id']) }}";

            let id = $(this).data('price-id');
            editURL = editURL.replace('id', id);

            $.ajax({
                async: true,
                beforeSend: function() {
                },
                complete: function() {
                    InputMaskPrice();
                },
                url: editURL,
                type: 'GET',
                success: function(response) {
                    $('#ModalId .modal-body').empty();

                    // Change modal title
                    $('#ModalId .modal-title').text('Edit Price')
                    // Add response in Modal body
                    $('#ModalId .modal-body').html(response);

                    // Display Modal
                    $('#ModalId').modal('show');

                    $('.select2-edit').select2({
                        dropdownParent: $('#ModalId')
                    });
                },

                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON.message);
                }

            });

        });



    });

    function enableVariations(attributeID) {
        if (confirm('Any unsaved changes will be lost')) {
            $.ajax({
            url: '/administrator/product/enabled-attributes',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "attributeID": attributeID
            },
            beforeSend: function () {
                $(".variationLoader").show();
            },
            success: function (success) {
                console.log(success);
                $(".variationLoader").hide();
                $(".variationRefresh").show();
                $(".variationSaveFirst").hide();
            },
            error: function () {
                alert("error in loading");
            }

        });
        }

    }

    function activePrice(priceID) {
        if (confirm('Active this price? Will replace current price. Remember update price notes. Any unsaved changes will be lost')) {
            $.ajax({
                url: '/administrator/product/attributes-active-price',
                type: 'POST',
                dataType:'html',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "price_id": priceID,
                },
                beforeSend: function () {
                    $(".variationLoader").show();
                },
                success: function (success) {
                    $(".variationLoaderPriceActive").hide();
                    $(".variationRefreshPriceActive").show();
                },
                error: function () {
                    alert("error in loading");
                }
            });
        }
    }

    function editPreviousPriceNotes(priceID) {
        const previous_notes =$("#previous_notes"+priceID).val();

        if (confirm('Are you sure update this notes?')) {
            $.ajax({
                url: '/administrator/product/update-previous-notes',
                type: 'POST',
                dataType:'html',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "price_id": priceID,
                    "previous_notes": previous_notes,
                },
                beforeSend: function () {
                    $(".variationLoaderUpdateNotes").show();
                },
                success: function (success) {
                    console.log('success',success)
                    $(".variationLoaderUpdateNotes").hide();
                    $(".variationUpdateNotes"+priceID).show();
                },
                error: function () {
                    alert("error in loading");
                }
            });
        }
    }

    function addNewPrice(attributeID, active= 0) {

        const new_fixed_price =$("#new_fixed_price"+attributeID).val();
        const new_price =$("#new_price"+attributeID).val();
        const new_member_price =$("#new_member_price"+attributeID).val();
        const new_offer_price =$("#new_offer_price"+attributeID).val();
        const new_web_offer_price =$("#new_web_offer_price"+attributeID).val();
        const new_premier_price =$("#new_premier_price"+attributeID).val();
        const new_outlet_fixed_price =$("#new_outlet_fixed_price"+attributeID).val();
        const new_outlet_offer_price =$("#new_outlet_offer_price"+attributeID).val();

        const new_product_sv =$("#new_product_sv"+attributeID).val();
        const new_notes =$("#new_notes"+attributeID).val();

        const new_point =$("#new_point"+attributeID).val();
        const new_discount_percentage =$("#new_discount_percentage"+attributeID).val();
        const new_point_cash_point =$("#new_point_cash_point"+attributeID).val();
        const new_point_cash_cash =$("#new_point_cash_cash"+attributeID).val();
        if (confirm('Are you sure want to add this price? Any unsaved changes will be lost')) {
            if (new_fixed_price=='' || new_price=='') {
                $(".variationPriceEmpty").show();
            } else {
                $.ajax({
                    url: '/administrator/product/attributes-add-new-price',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "attributeID": attributeID,
                        "active": active,

                        "new_fixed_price": new_fixed_price,
                        "new_price": new_price,
                        "new_member_price": new_member_price,
                        "new_offer_price": new_offer_price,
                        "new_premier_price": new_premier_price,

                        "new_web_offer_price": new_web_offer_price,
                        "new_outlet_offer_price": new_outlet_offer_price,
                        "new_outlet_fixed_price": new_outlet_fixed_price,

                        "new_product_sv": new_product_sv,
                        "new_notes": new_notes,

                        "new_point": new_point,
                        "new_discount_percentage": new_discount_percentage,
                        "new_point_cash_point": new_point_cash_point,
                        "new_point_cash_cash": new_point_cash_cash,
                    },
                    beforeSend: function () {
                        $(".variationLoader").show();
                    },
                    success: function (success) {
                        $(".variationLoaderPrice").hide();
                        if(active == 0){
                            $(".variationRefreshPrice").show();
                        }else{
                            $(".variationRefreshPriceActive").show();
                        }
                    },
                    error: function () {
                        alert("error in loading");
                    }
                });
            }
        }

    }

    function variationDetails(obj) {
        if ($(obj).val() === 'rbs') {
            $('.rbs-show').show();

        } else {
            $('.rbs-show').hide();
        }

    }

    // $('#frequencyOptions').on('select2:selecting', function(e) {
    //   console.log('Frequency: ' , e.params.args.data.id);
    // });

    // $('.rbsOptions').on('select2:selecting', function(e) {

    //     let attrID = $(this).attr('data-attributes-id');

    //     let functionName = $(this).attr('id');

    //     if ($('.rbsDetails'+e.params.args.data.id+'-'+attrID).attr('style') == 'display:none;') {
    //         $('.rbsDetails'+e.params.args.data.id+'-'+attrID).attr('style','');
    //     } else {
    //         $('.rbsDetails'+e.params.args.data.id+'-'+attrID).attr('style','display:none;');
    //     }
    //     // });
    // });

    // $('#timesOptions').on('select2:selecting', function(e) {
    //     $(this).
    //     // $(this).parent().attr('style','border:1px solid black;');
    //     $('.rbsDetails'+e.params.args.data.id).show();
    // });

    // $('#timesOptions').on('select2:unselecting', function(e) {
    //     // console.log('Times: ' , e.params.args.data.id);
    //     $('.rbsDetails'+e.params.args.data.id).hide();
    // });

</script>
@stack('variation-scripts')
@endpush

@push('style')
<style>
    /* HIDE RADIO */
    [type=radio] {
    position: absolute;
    opacity: 0;
    width: 0;
    height: 0;
    }

    /* IMAGE STYLES */
    [type=radio] + img {
    cursor: pointer;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + img {
    outline: 2px solid #f00;
    }
    .custom-control-input:checked~.custom-control-label::before {
        color: #fff;
        border-color: #ffcc00;
        background-color: #ffcc00;
    }
</style>
@endpush
