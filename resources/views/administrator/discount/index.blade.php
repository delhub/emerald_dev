@extends('layouts.administrator.mainVue')

@section('content')

<h1>Discount Code</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/discount/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Code</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            <v-client-table :columns="columns" :data="code" :options="options">

                <span slot="created_by_purchase" slot-scope="p">
                    <p class="small">
                        @{{ p.row.created_by_purchase }} <br>
                        @{{ p.row.country_id }} |
                        {{-- @{{ p.row.coupon_type }} --}}
                        Rule ID: @{{ p.row.coupon_rule_id }}
                    </p>
                </span>

                <span slot="coupon_amount" slot-scope="p">
                    {{-- <span>@{{ p.row.voucher[0].coupon_code }} @{{ p.row.voucher[1].coupon_code }}</span> --}}

                    <table class="table table-striped table-sm">
                        <thead>
                          <tr>
                            <th scope="col">Voucher Code</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Usage</th>
                            <th scope="col">Expired Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-for="(v,i) in p.row.voucher">
                            <th scope="row">@{{ v.coupon_code }}</th>
                            <td>@{{ v.coupon_amount }}</td>
                            <td>@{{ v.usage_counter + '/' + v.max_voucher_usage }}</td>
                            <td class="small">@{{ v.expiry }}</td>
                            <td><span class="badge" :class="{ 'badge-success': v.coupon_status == 'valid', 'badge-secondary': v.coupon_status == 'expired', 'badge-dark': v.coupon_status == 'used', 'badge-warning': v.coupon_status == 'inactive' }">@{{ v.coupon_status.toUpperCase() }}</span></td>
                            <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#codeEdit" @@click="getCodeById(v.id)">Edit</button></td>
                          </tr>
                        </tbody>
                      </table>

                      <span>REMARKS: @{{ p.row.rule.remarks }}</span>
                </span>

                {{-- <span slot="__action" slot-scope="p">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#codeEdit" @@click="getCodeById(p.row.id)">Edit</button>
                    <button type="button" class="btn btn-danger" @@click="deleteCode(p.row.id)">Delete</button>
                </span> --}}

            </v-client-table>

            <!-- Start Edit Modal -->
            <form {{-- v-on:submit.prevent --}}>
                @csrf
            <div class="modal fade" id="codeEdit" tabindex="-1" role="dialog" aria-labelledby="codeEditLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                    <h5 class="modal-title" id="codeEditLabel">Edit Voucher Code #@{{country_id}} | @{{coupon_code}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">

                            <div class="form-group">
                                <label for="user_id">User ID</label>
                                <input type="text" v-model="user_id" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="coupon_code">Coupon Code</label>
                                <input type="text" v-model="coupon_code" class="form-control" placeholder="Coupon Code" readonly>
                            </div>
                            <div class="form-group">
                                <label for="coupon_amount">Coupon Amount</label>
                                <input type="text" v-model="coupon_amount" class="form-control" placeholder="Coupon Amount" readonly>
                            </div>
                            <div class="form-group">
                                <label for="platform">Platform</label>
                                <select v-model="platform" class="form-control">
                                <option value="1">Web</option>
                                <option value="2">Retail Shop</option>
                                <option value="3">Web + Retail</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="outlet_id">Outlet</label>
                                <select v-model="outlet_id" class="form-control">
                                    <option value="1">Formula2u, MBB</option>
                                    <option value="2">Wisma Formula, Puchong</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="coupon_status">Coupon Status</label>
                                <select v-model="coupon_status" class="form-control">
                                <option value="valid">Valid</option>
                                <option value="invalid">Invalid</option>
                                <option value="inactive">Inactive</option>
                                <option value="expired">Expired</option>
                                <option value="used">Used</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="coupon_code">Expiry Date</label>
                                <input type="date" v-model="expiry" class="form-control" placeholder="Expiry Date">
                            </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" @@click="updateCode(id)">Save changes</button>
                    </div>

                </div>
                </div>
            </div>
        </form>
        <!-- End Edit Modal -->

        </div>
    </div>
</div>
@endsection


@push('script')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-tables-2@2.2.1/dist/vue-tables-2.min.js"></script>
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script>
Vue.use(VueTables.ClientTable);

    var app = new Vue({
    el: '#app',
    data: {
        code: [],
        id: '',
        user_id: '',
        country_id: '',
        coupon_code: '',
        coupon_status: '',
        coupon_type: '',
        coupon_amount: '',
        platform: '',
        outlet_id: '',
        expiry: '',
        columns: ['id','user_id','created_by_purchase','coupon_amount'],
        options: {
          headings: {
            id: '#',
            user_id: 'User ID',
            country_id: 'Country ID',
            coupon_code: 'Voucher Code',
            created_by_purchase: 'Create By Purchase',
            coupon_type: 'Coupon Type',
            'rule.remarks': 'Remarks ',
            coupon_amount: 'Coupon Attribute',
            coupon_status: 'Coupon Status',
            __action: 'Action'
          },
          perPage: 10,
          cellClasses:{
            coupon_status: [{ class:'text-center', condition: row => row.coupon_status }],
            usage_counter: [{ class:'text-center', condition: row => row.usage_counter }]
          }
        }
    },
    mounted() { //created // mounted //beforeMount
        this.getCode()
    },
    methods: {
        getCode() {
        axios
            .get('/administrator/discount/getCode')
            .then(response => {
                this.code = response.data
                })
            .catch(error => console.log(error))
        },
        getCodeById(id) {
        axios
            .get('/administrator/discount/getCodeById/' + id)
            .then(response => {
                this.id = response.data.id
                this.user_id = response.data.user_id
                this.country_id = response.data.country_id
                this.coupon_code = response.data.coupon_code
                this.created_by_purchase = response.data.created_by_purchase
                this.coupon_type = response.data.coupon_type
                this.coupon_status= response.data.coupon_status
                this.coupon_amount = response.data.coupon_amount
                this.platform = response.data.platform
                this.outlet_id = response.data.outlet_id
                this.expiry = response.data.expiry
                })
            .catch(error => console.log(error))
        },
        updateCode(id) {
            axios.post('/administrator/discount/update/' + id, {
                    id: id,
                    user_id: this.user_id,
                    platform: this.platform,
                    outlet_id: this.outlet_id,
                    coupon_status: this.coupon_status,
                    expiry: this.expiry
                })
                .then(function (response) {
                    app.getCode()
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        deleteCode(id) {
            axios.post('/administrator/discount/delete/' + id, {
                //
                })
                .then(function (response) {
                    app.getCode()
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
    });
</script>
@endpush
