<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutletInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlet_info', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('outlet_id')->default(0);
            $table->string('name',255)->nullable();
            $table->string('printer_email',255)->nullable();
            $table->text('default_address')->nullable();
            $table->integer('state_id')->default(0);
            $table->string('phone_number',24)->nullable();
            $table->string('operation_time',24)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlet_info');
    }
}
