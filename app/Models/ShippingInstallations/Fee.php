<?php

namespace App\Models\ShippingInstallations;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    // Set table
    protected $table = 'shipping_fee';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'purchase_id',
        'type'
    ];

    // Cast column to another datatype.
    protected $casts = [];

    public function purchase()
    {
        return $this->belongsTo('App\Models\Purchases\Purchase', 'purchase_id', 'id');
    }
}
