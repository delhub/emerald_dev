<?php

namespace App\Http\Controllers\Shop;

use Auth;
use Cookie;
use Carbon\Carbon;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\Globals\State;
use App\Models\Users\UserInfo;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use App\Models\Users\Customers\Cart;
use Illuminate\Support\Facades\View;
use App\Models\Products\ProductBundle;
use App\Models\Products\ProductDelivery;
use App\Models\Products\ProductAttribute;
use App\Models\Products\Product as PanelProduct;
use App\Http\Controllers\Purchase\PaymentGatewayController;
use App\Http\Controllers\WEB\Shop\v1\CartController as V1CartController;
use App\Models\Dealers\DealerDirectPay;
use App\Models\Discount\Code;
use App\Models\Globals\City;
use App\Models\Globals\Image;
use App\Models\Globals\Status;
use App\Models\ShippingInstallations\Locations;
use App\Models\ShippingInstallations\Rates;
use App\Models\ShippingInstallations\Zones;
use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Session;
use App\Models\Globals\Outlet;
use App\Traits\Voucher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Traits\FreeGiftraits;

class CartController extends Controller
{
    use Voucher, FreeGiftraits;

    const LESS_NO_PURCHASE = 1;
    const LESS_CAN_PURCHASE = 2;
    const BETWEEN_PRICE = 3;
    const MAX_FREE_DELIVERY = 4;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        // Session::forget('bjs_cart_delivery_method');
        // Session::forget('bjs_cart_delivery_outletId');

        // $dataDelivery = 'delivery';
        // Session::put('bjs_cart_delivery_method', $dataDelivery);

        // Get user
        $user = User::find(Auth::user()->id);

        $customer = $user->userInfo;
        $shippingAddress = $customer->shippingAddress;

        $states = State::all();

        $carts = $user->carts->where('status', 2001);

        foreach ($carts->where('selected', 1) as $val) {
            $dat = [];
            $dat['eligible_discount'] = $val->product->parentProduct->eligible_discount;
            $dat['generate_discount'] = $val->product->parentProduct->generate_discount;
            $dat['subtotal_price'] = $val->subtotal_price;
            $dat['cart_id'] = $val->id;

            $data[] = $dat;
        }
        // dd($data);

        $existingSession = $session = null;
        // $data['cart_total'] = $cartTotals['total_new'];
        if (isset($data)) {
            $session = Session::put('bjs_cart_discount', $data);
        }

        $checkRBS = array();

        foreach ($carts as $cart) {
            $cart->disabled = 0;
            // check not eligible discount
            if ($cart->product->parentProduct->eligible_discount != 0) {
                $cart->disabled = 9;
            }

            // check display only products
            if ($cart->product->parentProduct->display_only == 1) {
                $cart->disabled = 8;
                $cart->selected = 0;
            }

            // check disabled products
            if ($cart->product->parentProduct->product_status == 2) {
                $cart->disabled = 4;
                $cart->selected = 0;
            }

            // check rbs product
            if (array_key_exists('product_rbs_times', $cart->product_information)) {
                $cart->selected = 0;
            }

            $directPay = DealerDirectPay::whereJsonContains('cart_id', $cart->id)->where('user_id', $user->id)->first();
            if (isset($directPay)) {
                $cart->selected = 0;
            }

            $product_information = $cart->product_information;
            $attributes = ProductAttribute::where('id', reset($product_information))->first();
            if (!array_key_exists('product_rbs_frequency', $product_information) && $attributes) {
                $attributePrice = ($customer->user_level == 6001) ? $attributes->getPriceAttributes('web_offer_price') : $attributes->getPriceAttributes('fixed_price');

                if ($attributePrice <= 0) $cart->status = 2002;
            }

            $cart->save();
        }

        $cartsItems = array(
            'items' => array(),
        );

        $cookie = $new_shipCookie = json_decode(Cookie::get('addressCookie'));

        if (isset($cookie->discount) && $cookie->discount != null) {
            $existingSession = Session::get('bjs_cart_discount');
            $existingSession['voucher'] = json_decode(json_encode($cookie->discount), true);
            Session::put('bjs_cart_discount', $existingSession);
            $new_shipCookie->discount = null;
            // dd($session,$data);
        }

        // Cookie::queue('addressCookie', json_encode($cookie));

        $cartsItems = self::arrayProduct(compact('carts', 'shippingAddress', 'cartsItems', 'customer'));
        $state_id = isset($cookie) ? $cookie->state_id : $shippingAddress->state_id;
        $cities = City::where('state_id', $state_id)->orderBy('city_name')->get();
        $globalMinPoint = Status::where('status_type', 'minimum_point')->first()->description;

        $totals = 0;

        $outlets = Outlet::where('country_id', country()->country_id)->get();

        $sessionDelivery = Session::get('bjs_cart_delivery_method');

        $voucherLists = $this->getAvailableVoucher($user);

        foreach ($voucherLists as $key => $voucherList) {

            if ($voucherList['usageThisMonth'] != null && $voucherList['usageThisMonth'] >= $voucherList['limit']) {
                unset($voucherLists[$key]);
            }
        }

        // dd(Session::get('bjs_cart_discount'));
        // return $cartsItems;
        // !!!! Anything that put here need to put in direct pay controller also !!!!
        // dd($existingSession);
        return response(
            view('shop.cart.cart')
            ->with('existingSession', $existingSession)
                ->with('user', $user)
                ->with('shippingAddress', $shippingAddress)
                ->with('cartsItems', $cartsItems)
                ->with('totals', $totals)
                ->with('cities', $cities)
                ->with('cookie', $cookie)
                ->with('globalMinPoint', $globalMinPoint)
                ->with('states', $states)
                ->with('sessionDelivery', $sessionDelivery)
                ->with('outlets', $outlets)
                ->with('voucherLists', $voucherLists)
        )->cookie('addressCookie', json_encode($new_shipCookie), 3600);
    }

    public static function arrayProduct($args)
    {
        $defaults = [
            'isdirectPay' => false,
            'isCommand' => false
        ];
        $args = array_merge($defaults, $args);
        extract($args);
        $finalTotal = 0;
        $finalTotalPoint = 0;
        $zone = self::maybeCreateCookieAddress();

        // dd(request()->route());
        if ((request()->route() && request()->route()->getName() == 'agent.direct.pay') || $isCommand) {
            $zone['state_id'] = $shippingAddress->state_id;
            $zone['city_key'] = $shippingAddress->city_key;
        }

        $zoneId = self::getZone($zone['state_id'], $zone['city_key']);

        // foreach ($carts->where('selected',1) as $val) {
        //     $dat = [];
        //     $dat['eligible_discount'] = $val->product->parentProduct->eligible_discount;
        //     $dat['generate_discount'] = $val->product->parentProduct->generate_discount;
        //     $dat['subtotal_price'] = $val->subtotal_price;
        //     $dat['cart_id'] = $val->id;

        //     $data[] = $dat;
        // }
        // dd($data);
        // $session = null;
        // $data['cart_total'] = $cartTotals['total_new'];
        // if (isset($data)) {
        $session = Session::get('bjs_cart_discount');
        // }

        $autoApply = Voucher::calculateAutoApply($carts);

        $freegift = FreeGiftraits::freeGift_AutoApply($carts, 1);

        if (isset($_REQUEST['freegift']) && $_REQUEST['freegift'] == 1) {
            dd($freegift);
        }

        $croute = request()->path();

        foreach ($carts as $cart) {
            if ($croute == "payment/cashier") {
                //freegift check
            } else {
                if ($cart->freegift == 1) {
                    $cart->status = 2002;

                    $cart->save();
                }
            }

            $product_information = $cart->product_information;
            $attributes = ProductAttribute::where('id', reset($product_information))->first();

            $product_limit = null;
            $period = null;
            $purchase_limit_eligible = false;
            $purchase_limit_left = 100;

            if ($attributes->productLimit->isNotEmpty()) {
                $product_limit = $attributes->productLimit->where('user_id', auth()->user()->id)->first();
                if(!$product_limit)$product_limit = $attributes->productLimit->where('user_id', -1)->first();
                if(!$product_limit) continue;
                // dd($product_limit);
                $result = $product_limit->checkPurchaseLimit($cart->user_id, $product_limit);

                $purchase_limit_eligible = $result['purchase_limit_eligible'];
                $purchase_limit_left = $result['purchase_limit_left'];
                if ($purchase_limit_left < 0) $purchase_limit_left = 0;
                $period = $result['period'];
            }

            // $directPay = DealerDirectPay::where('cart_id', 'like', $cart->id)->first();
            $directPay = DealerDirectPay::whereJsonContains('cart_id', $cart->id)->where('user_id', $customer->user_id)->first();
            // dd($directPay->uuid);

            // dd($attributes->product2->getPriceAttributes('point_cash'));

            // return 'adsas';
            // dd($cart->product->parentProduct);
            // dd($customer->user_level);

            if (!array_key_exists('product_rbs_frequency', $product_information)) {
                $cart->unit_price = $customer->user_level == 6001 ? $attributes->getPriceAttributes('web_offer_price') : $attributes->getPriceAttributes('fixed_price');
                $cart->price_key = $attributes->getPriceAttributes('price_key');
                $cartsItems['id']['normal'][] = $cart->id;
            } else {
                if ($cart->selected == 1) {
                    $cartsItems['id']['rbs'][] = $cart->id;
                }
            }

            $cart->subtotal_price = $cart->unit_price * $cart->quantity;

            $categories = $cart->product->parentProduct->categories->where('parent_category_id', 0)->where('type', 'shop')->first();
            $categoriesName = $categories ? $categories->slug : '';

            $cartsItems['items'][$cart->id]['product_id'] = $cart->product_id;
            $cartsItems['items'][$cart->id]['link'] = 'product/' . $categoriesName . '/' . $cart->product->parentProduct->name_slug;
            $cartsItems['items'][$cart->id]['name'] = $cart->freegift == 0 ? $cart->product->parentProduct->name : $cart->product->parentProduct->name . " (Free Gift)";
            $cartsItems['items'][$cart->id]['status'] = $cart->selected;
            $cartsItems['items'][$cart->id]['freegift'] = $cart->freegift;
            $cartsItems['items'][$cart->id]['disabled'] = $cart->disabled;
            $cartsItems['items'][$cart->id]['errorMessage'] = $cart->getDisabledMessageAttribute();
            $cartsItems['items'][$cart->id]['quantity'] = $cart->quantity;
            $cartsItems['items'][$cart->id]['price'] = $cart->freegift == 0 ? $cart->unit_price : 0;
            $cartsItems['items'][$cart->id]['price_key'] = $cart->price_key;
            $cartsItems['items'][$cart->id]['point'] = $attributes->getPriceAttributes('point');
            $cartsItems['items'][$cart->id]['half_point'] = $attributes->getPriceAttributes('point_cash') != null ? json_decode($attributes->getPriceAttributes('point_cash'))->point : 0;
            $cartsItems['items'][$cart->id]['half_cash'] = $attributes->getPriceAttributes('point_cash') != null ? json_decode($attributes->getPriceAttributes('point_cash'))->cash : 0;
            $cartsItems['items'][$cart->id]['state'] = $shippingAddress->state_id;
            $cartsItems['items'][$cart->id]['country'] = $shippingAddress->country_id;
            $cartsItems['items'][$cart->id]['purchase_limit']['amount'] = ($product_limit) ? $product_limit->amount : null;
            $cartsItems['items'][$cart->id]['purchase_limit']['eligible'] = $purchase_limit_eligible;
            $cartsItems['items'][$cart->id]['purchase_limit']['purchase_left'] = $purchase_limit_left;
            $cartsItems['items'][$cart->id]['purchase_limit']['period'] = $period;
            $cartsItems['items'][$cart->id]['attributes'] = $cart->product_information;
            $cartsItems['items'][$cart->id]['attributes']['product_color_img'] = $attributes->color_images;
            $cartsItems['items'][$cart->id]['direct_pay']['uuid'] = isset($directPay) ? $directPay->uuid_link : null;
            $cartsItems['items'][$cart->id]['direct_pay']['customer_details'] = isset($directPay) ? json_decode($directPay->customer_details, true) : null;
            $cartsItems['items'][$cart->id]['selfcollect-only'] = 'false';


            // $cartsItems['items'][$cart->id]['direct_pay']['customer_details'] = $directPay ? json_decode($directPay->customer_details) : null;
            // get images
            // return $cartsItems['items'][$cart->id]['attributes']['product_color_img'];

            if (isset($cartsItems['items'][$cart->id]['attributes']['product_color_img'])) {
                $images = Image::where('id', $cartsItems['items'][$cart->id]['attributes']['product_color_img'])->first();
            } else {
                $images = Image::where('imageable_id',  $cart->product->parentProduct->id)->where('default', 1)->first();
            }

            // filter selected and not selected
            $cartSelectedCase = isset($directPay) ? 1 : $cartsItems['items'][$cart->id]['status'];

            // dd(request()->route()->getName());
            // dd(!in_array(request()->route()->getName(),['agent.direct.pay','gateway.responses','direct.pay.success']));
            $cashSubtotal =  0;
            $pointSubtotal = 0;

            if ($cartsItems['items'][$cart->id]['status'] == 1 || (request()->route() && request()->route()->getName() == 'agent.direct.pay')) {
                switch ($cart->payment_type) {
                    case '7000':
                        $cashSubtotal =  $cartsItems['items'][$cart->id]['price'] * $cartsItems['items'][$cart->id]['quantity'];
                        $pointSubtotal = 0;
                        break;
                    case '7001':
                        $cashSubtotal = 0;
                        $pointSubtotal = $cartsItems['items'][$cart->id]['point'] * $cartsItems['items'][$cart->id]['quantity'];
                        break;
                    case '7002':
                        $cashSubtotal =  $cartsItems['items'][$cart->id]['half_cash'] * $cartsItems['items'][$cart->id]['quantity'];
                        $pointSubtotal =  $cartsItems['items'][$cart->id]['half_point'] * $cartsItems['items'][$cart->id]['quantity'];
                        break;
                    default:
                        $cashSubtotal =  0;
                        $pointSubtotal = 0;
                        break;
                }
            }
            //

            $cartsItems['items'][$cart->id]['pointCash'] = $cart->payment_type;
            $cartsItems['items'][$cart->id]['images'] = $images ? '/' . $images->path . $images->filename : '';
            $cartsItems['items'][$cart->id]['subtotalPrice'] = $cashSubtotal;
            $cartsItems['items'][$cart->id]['subtotalPoint'] = $pointSubtotal;
            $cartsItems['items'][$cart->id]['activeSubtotal'] = $cartsItems['items'][$cart->id]['status'] == 2001 ? $cartsItems['items'][$cart->id]['subtotal'] : 0;
            // $cartsItems['items'][$cart->id]['delivery_fee'] = 0;

            $finalTotal += $cartsItems['items'][$cart->id]['subtotalPrice'];
            $finalTotalPoint += $cartsItems['items'][$cart->id]['subtotalPoint'];


            $cart->delivery_fee = 0;
            $cart->save();

            // return $rules['condition'];

            $sessionDelivery = ($isCommand) ? 'delivery' : Session::get('bjs_cart_delivery_method');

            /*************************  startcalculate shipping feeeeeee *********************************/
            // dd($cartsItems['items'][$cart->id]['status']);
            // if (

            // ) {
            $getID = $cart->product_information['product_size_id'] ?? $cart->product_information['product_rbs_id'];
            $attributeId = ProductAttribute::find($getID);
            if ((array_key_exists('product_order_selfcollect', $cart->product_information) && $cart->product_information['product_order_selfcollect']) == 0) {

                //Shipping
                $product_weight = $attributeId->product_measurement_weight * $cart->quantity;
                if ($attributeId->allow_rbs == 1 && array_key_exists('product_rbs_shipping', $cartsItems['items'][$cart->id]['attributes']) && $cartsItems['items'][$cart->id]['attributes']['product_rbs_shipping'] != 0) {
                    $shipping_category  = $cartsItems['items'][$cart->id]['attributes']['product_rbs_shipping'];
                } else {
                    $shipping_category  = $attributeId->shipping_category;
                }


                if (!empty($attributeId->shipping_category_special)) {

                    $attributeId_special = Ship_category::find($attributeId->shipping_category_special);

                    if (in_array($customer->user_level, json_decode($attributeId_special->for_user)) && $attributeId_special->active == 1) {
                        if (Carbon::now()->between($attributeId_special->start_date, $attributeId_special->end_date)) {
                            $shipping_category  = $attributeId->shipping_category_special;
                        }
                    }
                }

                if (!empty($attributeId->shipping_category) && $shipping_category != 0) {
                    if (!empty($zoneId)) {
                        foreach ($zoneId as $key => $zone_id) {
                            $zone[] = $zone_id->id;
                        }

                        $Srates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $shipping_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                    }

                    if (($cartsItems['items'][$cart->id]['status'] == '1' || $isdirectPay) && $sessionDelivery == 'delivery' && (empty($cartsItems['items'][$cart->id]['disabled']) || $cartsItems['items'][$cart->id]['disabled'] == 8 || $cartsItems['items'][$cart->id]['disabled'] == 9)) {
                        if (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-shipping-category')) {

                            if (!isset($shippingByCategories[$shipping_category])) {
                                // dd(!isset($shippingByCategories[$shipping_category]));

                                $shippingByCategories[$shipping_category]['qty'] = 0;
                                $shippingByCategories[$shipping_category]['subtotalPrice'] = 0;
                                $shippingByCategories[$shipping_category]['weight'] = 0;
                            }

                            $shippingByCategories[$shipping_category]['qty'] += $cart->quantity;
                            $shippingByCategories[$shipping_category]['subtotalPrice']  += $cart->subtotal_price;
                            $shippingByCategories[$shipping_category]['weight']  += $product_weight;
                            $shippingByCategories[$shipping_category]['item'] =  $cart;
                            $shippingByCategories[$shipping_category]['items'][] =  $cart;
                        } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-line-quantity')) {

                            if (!isset($shippingByCategories[$shipping_category][$cart->product_id])) {
                                $shippingByCategories[$shipping_category][$cart->product_id]['qty'] = 0;
                                $shippingByCategories[$shipping_category][$cart->product_id]['subtotalPrice'] = 0;
                                $shippingByCategories[$shipping_category][$cart->product_id]['weight'] = 0;
                            }

                            $shippingByCategories[$shipping_category][$cart->product_id]['qty'] += $cart->quantity;
                            $shippingByCategories[$shipping_category][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                            $shippingByCategories[$shipping_category][$cart->product_id]['weight']  += $product_weight;
                            $shippingByCategories[$shipping_category][$cart->product_id]['item'] =  $cart;
                            $shippingByCategories[$shipping_category][$cart->product_id]['items'][] =  $cart;
                        }
                    }

                    if (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'self-collect-only')) {
                        $cartsItems['items'][$cart->id]['selfcollect-only'] = 'true';
                    }
                }
            }
        }

        $cartTotals = array(
            'fee' => array(
                'shipping' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
                // 'rebate' => array(
                //     'amount' => 0,
                //     'shipping_information' => array(),
                // ),
            ),
        );

        // get array, loop for calculate fee
        if (isset($shippingByCategories)) {
            foreach ($shippingByCategories as $shipping_category => $shipping_carts) {

                if (!empty($zoneId)) {
                    foreach ($zoneId as $key => $zone_id) {
                        $zone[] = $zone_id->id;
                    }
                    $Srates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $shipping_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                }

                if (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-shipping-category')) {
                    $calculatedFee = self::calculateFee($shipping_carts['item'], $shipping_carts['qty'], $shipping_carts['subtotalPrice'], $shipping_carts['weight'], $zoneId, $shipping_category, $shipping_carts['items'], 'shipping');

                    $cartTotals['fee']['shipping']['amount'] += $calculatedFee['fee'];
                    $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];

                    foreach ($shipping_carts['items'] as $groupIitem) {
                        if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                            $disabled_cart_ids[] = $groupIitem->id;
                        }
                    }
                } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-line-quantity')) {

                    foreach ($shipping_carts as $shipping_cart) {
                        $calculatedFee = self::calculateFee($shipping_cart['item'], $shipping_cart['qty'], $shipping_cart['subtotalPrice'], $shipping_cart['weight'], $zoneId, $shipping_category, $shipping_cart['items'], 'shipping');

                        $cartTotals['fee']['shipping']['amount'] += $calculatedFee['fee'];
                        $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];
                        if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                            $disabled_cart_ids[] = $shipping_cart['item']['id'];
                        }
                    }
                }
            }
        }

        $cartsItems['shippingFee'] = $cartTotals['fee']['shipping'];
        // $cartsItems['discount'] = isset($session['discount_amount']) ? (float)$session['discount_amount'] * 100 : 0;
        // array_sum(array_column($cartsItems, 'subtotalPrice')

        $cartTables = Cart::where('status', '2001')->where('selected', 1)->where('user_id', $customer->user_id)->get();

        if(isset($autoApply['total']) && $autoApply['total'] > 0) {

            $cartsItems['discount'] = $autoApply['total'] * 100;
            $cartsItems['autoApply'] = 1;
        }
        else {
            // Session::forget('bjs_cart_discount');
            $voucher = isset($session['voucher']) ? $session['voucher'] : [];
            $total_discount = collect($voucher)->sum('discount_amount') * 100;
            $cartsItems['discount'] = $total_discount > 0 ? $total_discount : 0;
            $cartsItems['autoApply'] = 0;
        }

        $cartsItems['finalTotalPoint'] = +$finalTotalPoint;
        $minusDiscount = $finalTotal - $cartsItems['discount'];
        $cartsItems['finalTotal'] = $minusDiscount <= 0 ? $cartTotals['fee']['shipping']['amount'] : $minusDiscount + $cartTotals['fee']['shipping']['amount'];

        if (request()->route() && request()->route()->getName() == 'agent.direct.pay') {
            $directPayTables = DealerDirectPay::where('uuid_link', request()->directID)->first();
            if ($directPayTables->shipping_fee == null) {
                $directPayTables->shipping_fee = json_encode($cartsItems['shippingFee']);
                $directPayTables->save();
            }
        }

        // dd($finalTotal, $cartTotals['fee']['shipping']['amount'], $cartsItems['discount']);

        // $cartsItems['subtotal'] =  (array_sum($cartTables->pluck('subtotal_price')->toArray()));
        // $cartsItems['subtotalCash'] =  $cartTables->where('selected', 1)->sum('subtotal_price');
        // $cartsItems['subtotalPoint'] = $cartTables->where('selected', 1)->sum('subtotal_point');
        // $cartsItems['itemSubtotalPrice'] =  $carts->subtotal_price;
        // $cartsItems['itemSubtotalPoint'] =  $carts->subtotal_point;

        // dd($cartsItems);
        return $cartsItems;
    }

    public static function calculateFee($carts, $qty, $subTotalPrice, $weight, $zoneId, $categoryId, $items, $type)
    {
        $fee = 0;
        $cartItems =  $shipping_information = array();

        if (!isset($cartItems[$carts->product_id])) {
            $cartItems[$carts->product_id]['totalQty'] = 0;
            $cartItems[$carts->product_id]['totalPrice'] = 0;
            $cartItems[$carts->product_id]['totalWeight'] = 0;
        }

        $cartItems[$carts->product_id]['totalQty'] += $qty;
        $cartItems[$carts->product_id]['totalPrice'] += ($subTotalPrice / 100);
        $cartItems[$carts->product_id]['totalWeight'] += $weight;

        //get rates
        if (!empty($zoneId)) {

            foreach ($zoneId as $key => $zone_id) {
                $zone[] = $zone_id->id;
            }
            $rates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $categoryId)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
        }

        if (isset($rates) && $rates->isNotEmpty()) {

            foreach ($rates as $rate) {
                $minimun = $rate->min_rates;
                $maximum = $rate->max_rates;

                //check condition first, then check TOTAL qty|price|weight between minimun and maximun or not, then check what is the calculation type
                if ($rate->condition == 'quantity') {
                    if ($cartItems[$carts->product_id]['totalQty'] >= $minimun && $cartItems[$carts->product_id]['totalQty'] <= $maximum) {

                        $fee = self::rateFee($carts, $rate, $cartItems[$carts->product_id]['totalQty']);
                    } else {

                        if ($cartItems[$carts->product_id]['totalQty'] >= $maximum && $minimun == $maximum) {

                            $fee = self::rateFee($carts, $rate, $cartItems[$carts->product_id]['totalQty']);
                        } else {

                            if ($type == 'shipping') {
                                if ($cartItems[$carts->product_id]['totalQty'] <= $minimun) {
                                    $carts->disabled = 1;
                                } else {
                                    $carts->disabled = 2;
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalQty'] <= $minimun) {
                                    $carts->disabled = 7;
                                } else {
                                    $carts->disabled = 6;
                                }
                            }

                            $fee = 0;
                        }
                    }
                } elseif ($rate->condition == 'price') {

                    if ($cartItems[$carts->product_id]['totalPrice'] >= $minimun && $cartItems[$carts->product_id]['totalPrice'] <= $maximum) {

                        $fee = self::rateFee($carts, $rate, $cartItems[$carts->product_id]['totalQty']);
                    } else {

                        if ($cartItems[$carts->product_id]['totalPrice'] >= $maximum && $minimun == $maximum) {

                            $fee = self::rateFee($carts, $rate, $cartItems[$carts->product_id]['totalQty']);
                        } else {

                            if ($type == 'shipping') {

                                if ($cartItems[$carts->product_id]['totalPrice'] <= $minimun) {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 1;
                                    }
                                } else {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 2;
                                    }
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalPrice'] <= $minimun) {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 7;
                                    }
                                } else {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 6;
                                    }
                                }
                            }

                            $fee = 0;
                        }
                    }
                } elseif ($rate->condition == 'weight') {
                    if ($cartItems[$carts->product_id]['totalWeight'] >= $minimun && $cartItems[$carts->product_id]['totalWeight'] <= $maximum) {

                        $fee = self::rateFee($carts, $rate, $cartItems[$carts->product_id]['totalQty']);
                    } else {

                        if ($cartItems[$carts->product_id]['totalWeight'] >= $maximum && $minimun == $maximum) {

                            $fee = self::rateFee($carts, $rate, $cartItems[$carts->product_id]['totalQty']);
                        } else {

                            if ($type == 'shipping') {
                                if ($cartItems[$carts->product_id]['totalWeight'] <= $minimun) {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 1;
                                    }
                                } else {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 2;
                                    }
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalWeight'] <= $minimun) {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 7;
                                    }
                                } else {
                                    if ($rate->set_status != 0) {
                                        $carts->disabled = $rate->set_status;
                                    } else {
                                        $carts->disabled = 6;
                                    }
                                }
                            }

                            $fee = 0;
                        }
                    }
                }

                if ($fee != 0 && isset($fee) && $fee > 0) {
                    $qty = $cartItems[$carts->product_id]['totalQty'];
                    $price = $cartItems[$carts->product_id]['totalPrice'];
                    $weight = $cartItems[$carts->product_id]['totalWeight'];

                    $shipping_information[] = "Fee: {RM" . number_format(($fee / 100), 2) . "} - RateID: {$rate->id}, Condition: {$rate->condition}, Min: {$minimun}, Max: {$maximum}, Type: {$rate->calculation_type}, Total Qty: {$qty}, Price: {RM" . number_format(($price / 100), 2) . "} , Total Weight: {$weight}";
                    break;
                }
            }
        } else {

            if ($type == 'shipping') {
                $carts->disabled = 2;
            } else {
                $carts->disabled = 6;
            }

            $fee = 0;
        }

        $carts->save();

        //update same catgeory items disabled
        foreach ($items as $item) {
            $id[] = $item->id;
        }

        $updates = Cart::whereIn('id', $id)
            ->where('bundle_id', 0)
            ->where('status', 2001)
            ->where('selected', 1)
            ->get();

        if (isset($updates)) {
            foreach ($updates as $update) {
                if ((array_key_exists('product_order_selfcollect', $update->product_information) && $update->product_information['product_order_selfcollect']) == 0) {
                    $update->disabled = $carts->disabled;
                    $update->save();
                }
            }
        }
        //end update same catgeory items disabled

        return compact('fee', 'carts', 'shipping_information');
    }

    public static function rateFee($carts, $rate, $cartTotalQty)
    {

        if ($rate->calculation_type == 'per-shipping-category') {

            if ($rate->set_status != 0) {
                $carts->disabled = $rate->set_status;
            } else {
                $carts->disabled = 0;
            }

            $fee = $rate->price;
        } elseif ($rate->calculation_type == 'per-line-quantity') {

            if ($rate->set_status != 0) {
                $carts->disabled = $rate->set_status;
            } else {
                $carts->disabled = 0;
            }

            $fee = $rate->price * $cartTotalQty;
        }

        return $fee;
    }

    public static function maybeCreateCookieAddress()
    {
        $zone['city_key'] = 0;
        $zone['state_id'] = 0;

        $cookiesCountryId = country()->country_id;
        $shipCookie = json_decode(Cookie::get('addressCookie'));

        $zone['city_key'] = (isset($shipCookie->city_key)) ? $shipCookie->city_key : NULL;
        $zone['state_id'] = (isset($shipCookie->state_id)) ? $shipCookie->state_id : 0;

        // if ($cookiesCountryId == 'MY') {
        //     $zone['city_key'] = (isset($shipCookie->MY->city_key)) ? $shipCookie->MY->city_key : NULL;
        //     $zone['state_id'] = (isset($shipCookie->MY->state_id)) ? $shipCookie->MY->state_id : 0;
        // } elseif ($cookiesCountryId == 'SG') {
        //     $zone['city_key'] = (isset($shipCookie->SG->city_key)) ? $shipCookie->SG->city_key : NULL;
        //     $zone['state_id'] = (isset($shipCookie->SG->state_id)) ? $shipCookie->SG->state_id : 0;
        // }

        return $zone;
    }

    public static function getZone($state_id, $city_key)
    {
        //get user profile country, city and state
        $userCity = City::where('city_key', $city_key)->pluck('id')->first();
        $userState = $state_id;

        $userCity = (isset($userCity)) ? $userCity : NULL;
        $userState = (isset($userState)) ? $userState : NULL;

        //state
        $locations = Locations::join('shipping_zones', 'shipping_zones.id', '=', 'shipping_locations.zone_id')
            ->where('shipping_locations.location_type', 'state')
            ->whereIn('shipping_locations.location_id', [$userState, country()->country_id])
            ->get();

        //city
        $locationss = Locations::join('shipping_zones', 'shipping_zones.id', '=', 'shipping_locations.zone_id')
            ->where('shipping_locations.location_type', 'city')
            ->whereIn('shipping_locations.location_id', [$userCity, country()->country_id])
            ->get();
        $array = array();

        foreach ($locations as $location) {
            $array[$location->sequence] = $location;
        }

        foreach ($locationss as $location) {
            $array[$location->sequence] = $location;
        }
        if (!empty($array)) {
            ksort($array);
        }

        return $array;
    }

    public function getAvailableVoucher($user)
    {
        //
        $allCodes = $this->getUserVoucherListWithRule($user->id, country()->country_id);
        $voucherLists = array();

        foreach ($allCodes as $allCode) {

            foreach ($allCode->voucher as $couponCode) {

                if ($couponCode->general_type == 'breakdown') {
                    $totalUsageThisMonths = $this->monthlyUsageShipping($user->id, str_replace("-", "", $couponCode->coupon_code));

                    if ($couponCode->coupon_status == 'valid' && $totalUsageThisMonths < $allCode->rule->limit) {
                        $voucherLists[$couponCode->coupon_code]['discount_code'] = $couponCode->coupon_code;
                        $voucherLists[$couponCode->coupon_code]['discount_amount'] = $couponCode->coupon_amount;
                        $voucherLists[$couponCode->coupon_code]['usageThisMonth'] = $totalUsageThisMonths;
                        $voucherLists[$couponCode->coupon_code]['limit'] = $allCode->rule->limit;
                        $voucherLists[$couponCode->coupon_code]['invoice'] = $couponCode->created_by_purchase;
                    }
                } else {
                    if ($couponCode->coupon_status == 'valid') {
                        $voucherLists[$couponCode->coupon_code]['discount_code'] = $couponCode->coupon_code;
                        $voucherLists[$couponCode->coupon_code]['discount_amount'] = $couponCode->coupon_amount;
                        $voucherLists[$couponCode->coupon_code]['usageThisMonth'] = null;
                        $voucherLists[$couponCode->coupon_code]['limit'] = $allCode->rule->limit;
                        $voucherLists[$couponCode->coupon_code]['invoice'] = $couponCode->created_by_purchase;
                    }
                }
            }
        }

        return $voucherLists;
    }

    public function clearVoucher($key = null)
    {
        $oldData = array();
        $data = Session::get('bjs_cart_discount');
        // dd($key,$data['voucher'][$key]);
        $oldData = $data['voucher'][$key];
        unset($data['voucher'][$key]);

        Session::forget('bjs_cart_discount');

        Session::put('bjs_cart_discount', $data);

        return $oldData;

        // unset($data["discount_code"]);
        // unset($data["discount_amount"]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function updateCart(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $carts = Cart::find($request->id);

        $voucherAmount = $request->coupon;

        $session = Session::get('bjs_cart_discount');
        $data = array();
        $data['quantityError'] = null;
        switch ($request->process) {
            case 'status':
                if ($carts->selected == 1) {
                    $carts->selected = 0;
                    if ($carts->disabled == 10) {
                        $carts->disabled = 0;
                    }
                } else {
                    $carts->selected = 1;
                }
                break;

            case 'delete':
                $carts->status = 2002;
                break;

            case 'quantity':
                // dd($carts->product_information,$request->request);
                $carts->quantity = $request->quantity;
                if (array_key_exists('product_rbs_minimum', $carts->product_information) && $request->quantity < $carts->product_information['product_rbs_minimum']) {
                    $carts->quantity = $carts->product_information['product_rbs_minimum'];
                    $data['quantityError'] = $carts->product_information['product_rbs_minimum'];
                } else {
                    $carts->subtotal_price = $request->quantity * $carts->unit_price;
                    $carts->subtotal_point = $request->quantity * $carts->unit_point;
                }

                break;

            case 'pointCash':

                $point_cash = json_decode($carts->product->getPriceAttributes('point_cash'));


                switch ($user->userInfo->user_level) {
                    case '6000':
                        $price = $carts->product->getPriceAttributes('fixed_price');
                        break;
                    case '6001':
                        $price = $carts->product->getPriceAttributes('web_offer_price');
                        break;
                    case '6002':
                        $price = $carts->product->getPriceAttributes('member_price');
                        break;
                }

                $carts->payment_type = $request->payment_type;

                if ($request->payment_type == '7002') {
                    $carts->unit_price = $point_cash->cash;
                    $carts->subtotal_price = $point_cash->cash * $carts->quantity;
                    $carts->unit_point = $point_cash->point;
                    $carts->subtotal_point = $point_cash->point * $carts->quantity;
                } else if ($request->payment_type == '7001') {
                    $carts->unit_price = 0;
                    $carts->subtotal_price = 0;
                    $carts->unit_point = $carts->product->getPriceAttributes('point');
                    $carts->subtotal_point = $carts->product->getPriceAttributes('point') * $carts->quantity;
                } else {
                    $carts->unit_price = $price;
                    $carts->subtotal_price = $price * $carts->quantity;
                    $carts->unit_point = 0;
                    $carts->subtotal_point = 0;
                }
        }
        if (!in_array($carts->disabled, [0, 3, 9])) {
            $carts->selected = 0;
        }
        $carts->save();

        if ($request->emptys === '0') {
            $data['discount'] = 0;
            $data['subtotal'] =  0;
            $data['shippingFee'] =  0;
            $data['subtotalCash'] =  0;
            $data['subtotalPoint'] = 0;
            $data['finalTotal'] =  0;
            $data['finalTotalPoint'] =  0;
            $data['itemSubtotalPrice'] =  $carts->subtotal_price;
            $data['itemSubtotalPoint'] =  $carts->subtotal_point;

            return $data;
        }

        // $session['discount_amount']= $voucherAmount == '' ? 0 : $voucherAmount;

        $cartTables = Cart::where('status', '2001')->where('selected', 1)->where('user_id', $user->id)->get();

        $cartsItems = array(
            'id' => array(
                'rbs' => array(),
                'normal' => array()
            ),
            'items' => array()
        );
        $itemSubtotalPrice = $carts->subtotal_price;
        $itemSubtotalPoint = $carts->subtotal_point;
        $carts = $cartTables;
        $customer = $user->userInfo;
        $shippingAddress = $customer->shippingAddress;
        $otherFee = self::arrayProduct(compact('carts', 'shippingAddress', 'cartsItems', 'customer'));

        // dd($otherFee);

        // $data = array();
        // $data['discount'] =  isset($session['voucher']['discount_amount']) ? ((float)$session['voucher']['discount_amount']) * 100 : 0.00;
        $data['discount'] =  $otherFee['discount'];
        $data['autoApply'] =  $otherFee['autoApply'];
        $data['subtotal'] =  (array_sum($cartTables->pluck('subtotal_price')->toArray()));
        $data['shippingFee'] =  $otherFee['shippingFee']['amount'];
        $data['subtotalCash'] =  $cartTables->where('selected', 1)->sum('subtotal_price');
        $data['subtotalPoint'] = $cartTables->where('selected', 1)->sum('subtotal_point');
        $data['finalTotal'] =  ($data['subtotalCash'] + $data['shippingFee']) - $data['discount'];
        $data['finalTotalPoint'] =  $data['subtotalPoint'];
        $data['itemSubtotalPrice'] =  $itemSubtotalPrice;
        $data['itemSubtotalPoint'] =  $itemSubtotalPoint ;
        $data['rbs'] =  $otherFee['id']['rbs'];
        $data['normal'] =  $otherFee['id']['normal'];

        return $data;
    }

    public static function cartAddFreegift(Request $request)
    {
        if (isset($request->item) && !empty($request->item)) {

            $rule = json_decode($request->rule);

            $first = collect($rule)->where('freegift', $request->item)->first();

            if ($first) {
                $request->quantity = $first->quantity;
            } else {
                foreach ($rule as $k => $v) {
                    if ($v->status == true) {
                        $first[$k]['in'] = collect($v->freegift)->contains($request->item);
                        $first[$k]['product_code'] = $request->item;
                        $first[$k]['quantity'] = $v->quantity;
                    }
                }

                $isContains = collect($first);
                $checkContains = $isContains->where('in', true)->first();

                $request->quantity = $checkContains['quantity'];
            }
        }

        $user = User::find(Auth::user()->id);
        $fg = self::freegiftAddToCart($user, $request);

        return redirect()->route('payment.cashier')->with('success', 'Freegift added..');
    }

    public function getCity($stateID)
    {
        $user = User::find(Auth::user()->id);
        $customer = $user->userInfo;
        $shippingAddress = $customer->shippingAddress;
        $cities = City::where('state_id', $stateID)->get();
        $cityChoices[] = '<option value="0" selected disabled>Select...</option>';
        foreach ($cities as $city) {
            if ($shippingAddress->city_key != 0 && $shippingAddress->city_key == $city->city_key) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            $cityChoices[] = '<option value="' . $city->city_key . '"' . $selected . '>' . $city->city_name . '</option>';
        }
        return $cityChoices;
    }

    public static function calculateRules($cartsItems, $cart, $address)
    {
        $cartRules = array();

        $locations = Locations::where('location_type', 'state')->where('location_id', $address['state_id'])->first();
        if (!$locations) {
            return $cartRules = null;
        }

        $rates = Rates::where('zone_id', $locations->zone_id)->where('category_id', $cart->product->shipping_category)->get();
        foreach ($rates as $key => $rate) {
            $cartRules[$key]['condition'] = $rate->condition ?? '';
            $cartRules[$key]['min'] = $rate->min_rates ?? '';
            $cartRules[$key]['max'] = $rate->max_rates ?? '';
            $cartRules[$key]['shippingPrice'] = $rate->price ?? '';
            $cartRules[$key]['calculation_type'] = $rate->calculation_type ?? '';
        }

        return $cartRules;
    }

    public function checkoutItems()
    {
        // Get user
        $user = User::find(Auth::user()->id);

        $customer = $user->userInfo;
        $shippingAddress = $customer->shippingAddress;

        $states = State::all();

        $carts = $user->carts->where('status', '<=', 2001)
            // ->sortByDesc(
            //     function ($item) {
            //         return $item['status'];
            //     }
            // )
        ;

        return $cartsItems = self::arrayProduct(compact('carts', 'shippingAddress'));
    }

    public function cartAddress(Request $request)
    {
        // return $request;
        // return State::find($request->input('state_id'));
        $user = User::find(Auth::user()->id);
        $userAddress = $user->userInfo->shippingAddress;
        $state = State::all();
        $requestCity = City::where('city_key', $request->input('city'))->first();

        $shipCookie = json_decode(Cookie::get('addressCookie'));

        $addressVoucher = Session::get('bjs_cart_discount') ?? array();
        $discount_code = array_key_exists('voucher', $addressVoucher) ? $addressVoucher['voucher'] : null;

        $new_shipCookie = json_encode(array(
            'address_1' => $request->input('address1'),
            'address_2' => $request->input('address2'),
            'address_3' => $request->input('address3'),
            'postcode' => $request->input('postcode'),
            'city' => $requestCity ? $requestCity->city_name : '',
            'city_key' => $request->input('city'),
            'city_name' => City::where('city_key', $request->input('city'))->first(),
            'state_name' => State::find($request->input('state')),
            'state_id' => $request->input('state'),
            'name' => $request->input('name'),
            'mobile' => $request->input('phone'),
            'discount' => $discount_code
        ));

        // dd($new_shipCookie);



        // dd($addressVoucher,$discount_code);
        $cartItems = $user->carts->where('status', 2001);

        return redirect()->back()->withCookie(Cookie::make('addressCookie', $new_shipCookie, 3600));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return self::globalAddToCart($user, $request);
    }

    public static function globalAddToCart($user, $request)
    {


        $attributes = ProductAttribute::find($request->id);

        $product_information = array();
        $product_information['product_' . $attributes->attribute_type . '_id'] = $attributes->id;
        $product_information['product_' . $attributes->attribute_type] = $attributes->attribute_name;
        $product_information['product_code'] = $attributes->product_code;
        $product_information['product_color_img'] = $attributes->color_images;
        $product_information['product_order_selfcollect'] = 0;
        if ($request->rbs_frequency != '0' && $request->rbs_times != '0') {
            $product_information['product_rbs_frequency'] = $request->rbs_frequency;
            $product_information['product_rbs_times'] = $request->rbs_times;
            $product_information['product_rbs_minimum'] = $request->rbs_minimum;
            $product_information['product_rbs_unit_price'] = $request->rbs_price_per;
            $product_information['product_rbs_shipping'] = $request->rbs_shipping_category;
        }

        // return $request->rbs_price;
        // $product_information['shipping'] = $attributes->shipping_category;
        // $product_information['special_shipping'] = $attributes->shipping_category_special;
        // return $attributes->product_code;
        $existingCarts = Cart::where('user_id', $user->id)->where('product_code', $attributes->product_code)->where('status', 2001)->get();
        $directPay = $normal_cartID = $directPay_cartID = $normal_cartID = null;


        if ($existingCarts->count() > 0) {
            foreach ($existingCarts as $existingCart) {
                $directPay = DealerDirectPay::whereJsonContains('cart_id', $existingCart->id)->where('user_id', $user->id)->first();

                if (!$directPay && (!array_diff($existingCart->product_information, $product_information) && !array_diff($product_information, $existingCart->product_information))) {
                    $normal_cartID = $existingCart->id;
                }
            }
        }

        // price for first launch only
        // $priceAttributes = $attributes->getPriceAttributes('web_offer_price') ? $attributes->getPriceAttributes('web_offer_price') : $attributes->getPriceAttributes('price');
        $price = $request->rbs_price == 0 ? $attributes->getPriceAttributes('web_offer_price') : $request->rbs_price * 100;

        if ($request->rbs_price == 0) {
            switch ($user->userInfo->user_level) {
                case '6001':
                    $price = $attributes->getPriceAttributes('web_offer_price');
                    break;
                default:
                $price = $attributes->getPriceAttributes('fixed_price');
                // case '6002':
                //     $price = $attributes->getPriceAttributes('member_price');
                    break;
            }
        } else {
            $price = $request->rbs_price * 100;
        }


        // if ($attributes->getPriceAttributes('offer_price') != 0) {
        //     $price = $attributes->getPriceAttributes('offer_price');
        // } else {
        //     switch ($user->userInfo->user_level) {
        //         case '6000':
        //             $price = $attributes->getPriceAttributes('fixed_price');
        //             break;
        //         case '6001':
        //             $price = $attributes->getPriceAttributes('web_offer_price');
        //             break;
        //         case '6002':
        //             $price = $attributes->getPriceAttributes('member_price');
        //             break;
        //     }
        // }

        // return $attributes->product2->userLevelPrice;
        // return $request->quantity;
        $quantity = 0;



        if ($normal_cartID) {

            $cart = Cart::where('id', $normal_cartID)->first();

            $cart->price_key = $attributes->getPriceAttributes('price_key');
            $cart->quantity = $cart->quantity + $request->quantity;
            $cart->selected = 1;
            $cart->unit_price = $price;
            $cart->subtotal_price = $cart->unit_price *  $cart->quantity;
            $cart->save();
        } else {

            $cart = new Cart;
            $cart->user_id = $user->id;
            $cart->product_id = $attributes->panel_product_id;
            $cart->bundle_id = 0;
            $cart->product_code = $attributes->product_code;
            $cart->price_key = $attributes->getPriceAttributes('price_key');
            $cart->product_information = $product_information;
            $cart->unit_price = $price;
            $cart->quantity = $request->quantity;
            $cart->subtotal_price = $cart->unit_price * $request->quantity;
            $cart->delivery_fee = 0;
            $cart->installation_fee = 0;
            $cart->rebate = 0;
            $cart->auto_apply = 0;
            $cart->country_id = 'MY';
            $cart->status = 2001;
            $cart->selected = 1;
            $cart->disabled = 0;
            $cart->save();
        }

        return $cart;
    }

    public static function freegiftAddToCart($user, $request)
    {

        if (!isset($request->item) && empty($request->item)) return;

        $attributes = ProductAttribute::where('product_code', $request->item)->first();

        $product_information = array();
        $product_information['product_' . $attributes->attribute_type . '_id'] = $attributes->id;
        $product_information['product_' . $attributes->attribute_type] = $attributes->attribute_name;
        $product_information['product_code'] = $attributes->product_code . " (Free Gift)";
        $product_information['product_color_img'] = $attributes->color_images;
        $product_information['product_order_selfcollect'] = 0;

        /* if ($request->rbs_frequency != '0' && $request->rbs_times != '0') {
            $product_information['product_rbs_frequency'] = $request->rbs_frequency;
            $product_information['product_rbs_times'] = $request->rbs_times;
            $product_information['product_rbs_minimum'] = $request->rbs_minimum;
            $product_information['product_rbs_unit_price'] = $request->rbs_price_per;
            $product_information['product_rbs_shipping'] = $request->rbs_shipping_category;
        } */

        $existingCarts = Cart::where('user_id', $user->id)->where('product_code', $attributes->product_code)->where([['status', 2001], ['freegift', 1]])->get();
        $directPay = $normal_cartID = $directPay_cartID = $normal_cartID = null;


        if ($existingCarts->count() > 0) {
            foreach ($existingCarts as $existingCart) {
                $directPay = DealerDirectPay::whereJsonContains('cart_id', $existingCart->id)->where('user_id', $user->id)->first();

                if (!$directPay && (!array_diff($existingCart->product_information, $product_information) && !array_diff($product_information, $existingCart->product_information))) {
                    $normal_cartID = $existingCart->id;
                }
            }
        }

        $quantity = 0;


        if ($normal_cartID) {

            $cart = Cart::where('id', $normal_cartID)->first();

            $cart->price_key = $attributes->getPriceAttributes('price_key');
            $cart->quantity = $request->quantity;
            $cart->selected = 1;
            $cart->unit_price = 0;
            $cart->subtotal_price = $cart->unit_price *  $cart->quantity;
            $cart->save();
        } else {

            $cart = new Cart;
            $cart->user_id = $user->id;
            $cart->product_id = $attributes->panel_product_id;
            $cart->bundle_id = 0;
            $cart->product_code = $attributes->product_code;
            $cart->price_key = $attributes->getPriceAttributes('price_key');
            $cart->product_information = $product_information;
            $cart->unit_price = 0;
            $cart->quantity = $request->quantity;
            $cart->subtotal_price = 0;
            $cart->delivery_fee = 0;
            $cart->installation_fee = 0;
            $cart->rebate = 0;
            $cart->freegift = 1;
            $cart->auto_apply = 0;
            $cart->country_id = 'MY';
            $cart->status = 2001;
            $cart->selected = 1;
            $cart->disabled = 0;
            $cart->save();
        }

        return $cart;
    }

    public function storeOneProductToCart($request, $product, $user, $attributes, $bundle, $qty, $bundleVariation)
    {
        //bundle
        $productType = $product->parentProduct->product_type;
        $bundlejson = json_decode($bundleVariation, FALSE);
        //bundle end

        $productCategories = $product
            ->parentProduct
            ->categories;

        $productCategoriesArray = [];

        foreach ($productCategories as $key => $productCategory) {
            $productCategoriesArray[$key] = $productCategory->id;
        }

        $freeDeliveryMinPrices = $product
            ->panel
            ->categoriesWithMinPrice
            ->whereIn('category_id', $productCategoriesArray)
            ->all();
        //
        $userInfo = $user->userInfo;

        //
        $userShipping = $userInfo->shippingAddress;
        $states = State::all();

        $shipCookie = json_decode(Cookie::get('addressCookie'));
        if (isset($shipCookie)) {
            $stateDelivery = $shipCookie->state_id;
        } else {
            $stateDelivery = $userShipping->state_id;
        }

        $deliveryFee = $product->deliveries->where('state_id', $stateDelivery)->first();


        $panel = $product->panel;

        $activeCartItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();

        $totalSubtotalPrice = 0;

        $deliveryItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->where('disabled', 0)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();


        foreach ($deliveryItems as $deliveryItem) {
            $totalSubtotalPrice = $totalSubtotalPrice + $deliveryItem->subtotal_price;
        }


        $isDeliveryFree = 0;
        $category_id = null;
        foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {
            // More than min, less than max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_NO_PURCHASE;
                break;
            }
            // Less than min min equal to max, can purchase
            if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_CAN_PURCHASE;
                break;
            }
            // More than min, less than max, can purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $isDeliveryFree = self::BETWEEN_PRICE;
                break;
            }
            // More than max, min equal to max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                $isDeliveryFree = self::MAX_FREE_DELIVERY;
                break;
            }

            $category_id = $freeDeliveryMinPrice->category_id;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 0;
        }


        // Variables initiliazation.
        $color = null;
        $size = null;
        $curtainSize = null;
        $temperature = null;
        $preorder_date = null;
        $order_remark = null;
        $order_selfcollect = null;
        $order_tradein = null;
        $order_installation = null;


        if ($request->input('product_preorder_date') != null) {
            $preorder_date = $request->input('product_preorder_date');
        }

        if ($request->input('product_order_remark') != null) {
            $order_remark = $request->input('product_order_remark');
        }

        if ($request->input('product_order_selfcollect') != null) {
            $order_selfcollect = $request->input('product_order_selfcollect');
        }
        // Trade In
        if ($request->input('product_order_tradein') != null) {
            $order_tradein = $request->input('product_order_tradein');
        }
        // Installation
        if ($request->input('product_installation') != null) {
            $order_installation = $request->input('product_installation');
        }
        //
        // If the post request has product color id value in it..
        if ($attributes['color'] != null) {
            // Get selected product color.
            $color = $product->colorAttributes
                ->where('id', $attributes['color'])
                ->first();

            // Set color id for checking purposes.
            $colorId = $color->id;
        }

        // If the post request has product dimension id value in it..
        if ($attributes['product_attribute_size'] != null) {
            $size = $product->sizeAttributes
                ->where('id', $attributes['product_attribute_size'])
                ->first();

            // Set dimension id for checking purposes.
            $sizeId = $size->id;
        }

        // If the post request has product dimension id value in it..
        if ($attributes['product_attribute_curtain_size'] != null) {
            // Get selected product dimension.
            $curtainSize = $product->curtainSizeAttributes
                ->where('id', $attributes['product_attribute_curtain_size'])
                ->first();

            // Set dimension id for checking purposes.
            $curtainSizeId = $curtainSize->id;
        }

        // If the post request has product length id value in it..
        if ($attributes['product_attribute_temperature'] != null) {
            // Get selected product length.
            $temperature = $product->lightTemperatureAttributes
                ->where('id', $attributes['product_attribute_temperature'])
                ->first();

            // Set length id for checking purposes.
            $temperatureId = $temperature->id;
        }

        // Check if the exact item is already in the cart..
        $existingCartItem = new Cart;

        $existingCartItem = $existingCartItem->where('user_id', $user->id);

        $existingCartItem->where('product_id', $product->id);

        if ($color != null) {
            $existingCartItem->where('product_information->product_color_id', $colorId);
        }
        if ($size != null) {
            $existingCartItem->where('product_information->product_size_id', $sizeId);
        }
        if ($curtainSize != null) {
            $existingCartItem->where('product_information->product_curtain_size_id', $curtainSizeId);
        }
        if ($temperature != null) {
            $existingCartItem->where('product_information->product_temperature_id', $temperatureId);
        }
        if ($preorder_date != null) {
            $existingCartItem->where('product_information->product_preorder_date', $preorder_date);
        }
        if ($order_remark != null) {
            $existingCartItem->where('product_information->product_order_remark', $order_remark);
        }
        if (isset($bundle['parentBundleProductID']) && !empty($bundle['parentBundleProductID'])) {
            $existingCartItem->where('bundle_id', $bundle['parentBundleProductID']);
        } else {
            $existingCartItem->where('bundle_id', 0);
        }
        if ($order_selfcollect != null) {
            $existingCartItem->where('product_information->product_order_selfcollect', $order_selfcollect);
        }
        if ($order_tradein != null) {
            $existingCartItem->where('product_information->product_order_tradein', $order_tradein);
        }
        if ($order_installation != null) {
            $existingCartItem->where('product_information->product_installation', $order_installation);
        }
        if ($bundleVariation != NULL) {
            $existingCartItem->where('product_information->bundle_variation', $bundleVariation);
        }


        $existingCartItem = $existingCartItem->where('status', 2001)->first();
        // If item doesn't exist in cart..
        if ($existingCartItem == null || $productType == 'bundle') {
            // Initialize product information array.
            $productInformation = array();

            // Create a new cart item.
            $newCartItem = new Cart;
            $newCartItem->user_id = $user->id;
            $newCartItem->product_id = $product->id;

            $price = 0;
            // Check if the post request has product color id in it..
            if ($color != null) {
                // If yes, assign the color id and name
                $productInformation['product_color_id'] = $color->id;
                $productInformation['product_color_name'] = $color->attribute_name;
                $productInformation['product_code'] = $color->product_code ? $color->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $color->color_hex;
                $productInformation['product_color_img'] = $color->color_images;
            }
            // Check if the post request has product length id in it..
            if ($temperature != null) {
                // If yes, assign the length id and concate the length and measurement unit.
                $productInformation['product_temperature_id'] = $temperature->id;
                $productInformation['product_temperature'] = $temperature->attribute_name;
                $productInformation['product_code'] = $temperature->product_code ? $temperature->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $temperature->color_hex;
                $productInformation['product_color_img'] = $temperature->color_images;
            }
            // Check if the post request has product dimension id in it..
            if ($size != null) {
                // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
                $productInformation['product_size_id'] = $size->id;
                $productInformation['product_size'] = $size->attribute_name;
                $productInformation['product_code'] = $size->product_code ? $size->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $size->color_hex;
                $productInformation['product_color_img'] = $size->color_images;
            }
            // Check if the post request has product dimension id in it..
            if ($curtainSize != null) {
                // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
                $productInformation['product_curtain_size_id'] = $curtainSize->id;
                $productInformation['product_curtain_size'] = $curtainSize->attribute_name;
                $productInformation['product_code'] = $curtainSize->product_code ? $curtainSize->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $curtainSize->color_hex;
                $productInformation['product_color_img'] = $curtainSize->color_images;
            }

            if ($preorder_date != null) {

                $productInformation['product_preorder_date'] = $preorder_date;
            }
            if ($bundleVariation != NULL && ($bundlejson->selection != 'default') && ($bundlejson->name != 'null')) {
                if ($bundlejson->selection == 'color') {
                    $productInformation['product_color_name'] = $bundlejson->name;
                }
                if ($bundlejson->selection == 'size') {
                    $productInformation['product_size'] = $bundlejson->name;
                }
                if ($bundlejson->selection == 'light-temperature') {
                    $productInformation['product_temperature'] = $bundlejson->name;
                }
                if ($bundlejson->selection == 'curtain-size') {
                    $productInformation['product_curtain_size'] = $bundlejson->name;
                }
            }
            // if ($size != null) {
            //     if ($size->price != 0) {
            //         $price = $size->premium_price; // $price = $size->price;  TODO: Temporary default to member price.
            //     } else {
            //         $price = $product->premium_price; // $price = $product->price; TODO: Temporary default to member price.
            //     }
            // } else {
            //     $price = $product->premium_price; // $price = $product->price; TODO: Temporary default to member price.
            // }

            if ($order_remark != null) {

                $productInformation['product_order_remark'] = $order_remark;
            }

            if ($order_selfcollect != null) {

                $productInformation['product_order_selfcollect'] = $order_selfcollect;
            }
            if ($order_tradein != null) {

                $productInformation['product_order_tradein'] = $order_tradein;
                $newCartItem->rebate = $productInformation['product_order_tradein'];
            }
            if ($order_installation != null) {

                $productInformation['product_installation'] = $order_installation;
            }



            if ($userInfo->account_status == '1') { // 1 is DC customer

                if ((isset($size)) && ($size->premium_price != '0')) {

                    $price = $size->premium_price;
                } elseif ((isset($curtainSize)) && $curtainSize->premium_price != '0') {

                    $price = $curtainSize->premium_price;
                } else {

                    $price = $product->premium_price;
                }
            } else {
                if ((isset($size)) && $size->price != '0') {

                    $price = $size->price;
                } elseif ((isset($curtainSize)) && $curtainSize->price != '0') {

                    $price = $curtainSize->price;
                } else {

                    $price = $product->price;
                }
            }

            if ($productInformation['product_code'] == $product->parentProduct->product_code) {

                $productInformation['product_code'] = PaymentGatewayController::sanitize_product_code($productInformation['product_code'], $productInformation);
            }



            $newCartItem->product_information = $productInformation;
            $newCartItem->product_code = $productInformation['product_code'];
            $newCartItem->quantity = $qty;

            if ($bundle['truefalse'] == false) {
                $newCartItem->delivery_fee = $product->delivery_fee;
                $newCartItem->installation_fee = $product->installation_fee;
                $newCartItem->unit_price = $price;
                $newCartItem->subtotal_price = $price * $qty;
            } else {
                $newCartItem->delivery_fee = 0;
                $newCartItem->installation_fee = 0;
                $newCartItem->unit_price = 0;
                $newCartItem->subtotal_price = 0;
                $newCartItem->bundle_id = $bundle['parentBundleProductID'];
            }
            // if ($isDeliveryFree == 1) {
            //     $newCartItem->delivery_fee = 0;
            //     $newCartItem->disabled = 0;
            // } elseif ($isDeliveryFree == 2) {
            //     $newCartItem->delivery_fee = 0;
            //     $newCartItem->disabled = 1;
            // } else {
            //     $dlvryfee  = $deliveryFee->delivery_fee * $qty;
            //     $newCartItem->delivery_fee = isset($dlvryfee) ? $dlvryfee : 0;
            // }

            $newCartItem->save();
            $cartItemId = $newCartItem->id;
        } else {
            // If item exist in cart..
            // Add to the existing quantity.
            $existingCartItem->quantity = $existingCartItem->quantity + $qty;
            // Re calculate the total price.
            if ($bundle['truefalse'] == false) {
                $existingCartItem->subtotal_price = $existingCartItem->quantity * $existingCartItem->unit_price;
            } else {
                $existingCartItem->unit_price = 0;
                $existingCartItem->subtotal_price = 0;
                $existingCartItem->bundle_id = $bundle['parentBundleProductID'];
            }
            // if ($isDeliveryFree == 1) {
            //     $existingCartItem->delivery_fee = 0;
            //     $existingCartItem->disabled = 0;
            // } elseif ($isDeliveryFree == 2) {
            //     $existingCartItem->delivery_fee = 0;
            //     $existingCartItem->disabled = 1;
            // } else {
            //     $existingCartItem->delivery_fee = $deliveryFee->delivery_fee * $qty;
            // }
            $existingCartItem->save();
            $cartItemId = $existingCartItem->id;
        }

        $totalSubtotalPrice = 0;

        $deliveryItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->where('disabled', 0)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();

        foreach ($deliveryItems as $deliveryItem) {
            $totalSubtotalPrice = $totalSubtotalPrice + $deliveryItem->subtotal_price;
        }

        $isDeliveryFree = 0;
        $category_id = null;
        foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {
            // More than min, less than max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_NO_PURCHASE;
                break;
            }
            // Less than min min equal to max, can purchase
            if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_CAN_PURCHASE;
                break;
            }
            // More than min, less than max, can purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $isDeliveryFree = self::BETWEEN_PRICE;
                break;
            }
            // More than max, min equal to max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                $isDeliveryFree = self::MAX_FREE_DELIVERY;
                break;
            }

            $category_id = $freeDeliveryMinPrice->category_id;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 0;
        }

        foreach ($deliveryItems as $currentCartItem) {
            switch ($isDeliveryFree) {
                case self::LESS_NO_PURCHASE:
                    $fee = 0;
                    $disabled = 1;
                    break;
                case self::LESS_CAN_PURCHASE:
                    if ($group == 'g') {
                        $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                    } else {
                        $fee = $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                    }
                    $disabled = 0;
                    break;
                case self::BETWEEN_PRICE:
                    if ($group == 'g') {
                        $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                    } else {
                        $fee = $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                    }
                    $disabled = 0;
                    break;
                case self::MAX_FREE_DELIVERY:

                    if ($freeStates) {
                        if (in_array($stateDelivery, $freeStates)) {
                            $disabled = 0;
                            $fee =  0;
                        } else {
                            $disabled = 3;
                            $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                        }
                    } else {
                        $disabled = 0;
                        $fee = 0;
                    }

                    break;
                default:
                    $fee =  $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                    $disabled = 0;
            }

            if ($bundle['truefalse'] == false) {
                // $currentCartItem->delivery_fee = $fee;
                $this_selfcollect = (isset($currentCartItem->product_information['product_order_selfcollect']) && $currentCartItem->product_information['product_order_selfcollect']);
                $currentCartItem->delivery_fee =  ($this_selfcollect) ? 0 : $fee;
            } else {
                $currentCartItem->delivery_fee = 0;
            }

            $currentCartItem->disabled =  $disabled;
            $this->itemCount[$category_id]++;
            $currentCartItem->save();
        }



        return $cartItemId;
    }

    public function buyNowStore(Request $request)
    {
        // Get user
        $user = User::find(Auth::user()->id);
        $userInfo = UserInfo::find(Auth::user()->id);

        // Get product
        $product = PanelProduct::find($request->input('product_id'));

        //Get bundle item
        $inputBundleItem = $request->input('bundle-product');

        $panel = $product->panel;

        $userInfo = $user->userInfo;
        $userShipping = $userInfo->shippingAddress;

        $shipCookie = json_decode(Cookie::get('addressCookie'));
        if (isset($shipCookie)) {
            $stateDelivery = $shipCookie->state_id;
        } else {
            $stateDelivery = $userShipping->state_id;
        }

        $deliveryFee = $product->deliveries->where('state_id', $stateDelivery)->first();

        $bundle['truefalse'] = false;
        $qty = $request->input('productQuantity');

        $attributes['color'] = $request->input('product_attribute_color');
        $attributes['product_attribute_temperature'] = $request->input('product_attribute_temperature');
        $attributes['product_attribute_curtain_size'] = $request->input('product_attribute_curtain_size');
        $attributes['product_attribute_size'] = $request->input('product_attribute_size');


        if (empty($inputBundleItem)) {
            $cartItemId = $this->addOneProductToCart($request, $user, $userInfo, $product, $panel, $deliveryFee, $stateDelivery, $attributes, $bundle, $qty, NULL);
        } else {

            // Bundle
            $getProductBundleID = array();
            $bundleqty = array();

            foreach ($inputBundleItem as $bundleID => $quantity) {
                $getProductBundleID[] = $bundleID;
                $bundleqty[$bundleID] = $quantity;
            }

            $bundleItemWithAttribute = ProductAttribute::selectRaw('panel_product_bundle.id as bundleID, panel_product_bundle.bundle_variation, panel_product_attributes.id as id, panel_product_attributes.attribute_type, panel_product_attributes.attribute_name, panel_product_attributes.panel_product_id')
                ->join('panel_product_bundle', 'panel_product_bundle.product_attribute_id', '=', 'panel_product_attributes.id')
                ->whereIn('panel_product_bundle.id', $getProductBundleID)->get();

            if ($bundleItemWithAttribute->isNotEmpty()) {

                // check if sub items are in bundle
                $checkingSubItem = ProductBundle::where('panel_product_id', $request->input('product_id'))->whereIn('id', $getProductBundleID)->get();
                if ($checkingSubItem->isEmpty()) return back()->with(['error_message' => 'Wrong sub item added !']);

                $cartItemId = $this->addOneProductToCart($request, $user, $userInfo, $product, $panel, $deliveryFee, $stateDelivery, $attributes, $bundle, $qty, NULL);

                $bundle['truefalse'] = true;
                $bundle['parentBundleProductID'] = $cartItemId;
                $bundleLproducts = array();
                $attriLbutes = array();

                foreach ($bundleItemWithAttribute as $key => $item) {
                    $bundleattributes = array();

                    $bundleproducts = PanelProduct::find($item->panel_product_id);
                    $bundleVariation[$item->bundleID] = $item->bundle_variation;

                    if ($item->attribute_type == 'color') {
                        $bundleattributes['color'] = $item->id;
                    } else {
                        $bundleattributes['color'] = NULL;
                    }
                    if ($item->attribute_type == 'light-temperature') {
                        $bundleattributes['product_attribute_temperature'] = $item->id;
                    } else {
                        $bundleattributes['product_attribute_temperature'] = NULL;
                    }
                    if ($item->attribute_type == 'curtain-size') {
                        $bundleattributes['product_attribute_curtain_size'] = $item->id;
                    } else {
                        $bundleattributes['product_attribute_curtain_size'] = NULL;
                    }
                    if ($item->attribute_type == 'size') {
                        $bundleattributes['product_attribute_size'] = $item->id;
                    } else {
                        $bundleattributes['product_attribute_size'] = NULL;
                    }

                    $this->addOneProductToCart($request, $user, $userInfo, $bundleproducts, $panel, $deliveryFee, $stateDelivery, $bundleattributes, $bundle, $bundleqty[$item->bundleID], $bundleVariation[$item->bundleID]);
                }
            }
        }

        return redirect('/shop/cart?buynow=' . $cartItemId);
    }

    public function addOneProductToCart($request, $user, $userInfo, $product, $panel, $deliveryFee, $stateDelivery, $attributes, $bundle, $qty, $bundleVariation)
    {
        //bundle
        $productType = $product->parentProduct->product_type;
        $bundlejson = json_decode($bundleVariation, FALSE);
        //bundle end

        $productCategories = $product
            ->parentProduct
            ->categories;

        $productCategoriesArray = [];

        foreach ($productCategories as $key => $productCategory) {
            $productCategoriesArray[$key] = $productCategory->id;
        }
        $totalSubtotalPrice = 0;

        $freeDeliveryMinPrices = $product
            ->panel
            ->categoriesWithMinPrice
            ->whereIn('category_id', $productCategoriesArray)
            ->all();

        $deliveryItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->where('disabled', 0)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();


        foreach ($deliveryItems as $deliveryItem) {
            $totalSubtotalPrice = $totalSubtotalPrice + $deliveryItem->subtotal_price;
        }

        $isDeliveryFree = 0;
        $category_id = null;
        foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {
            // More than min, less than max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_NO_PURCHASE;
                break;
            }
            // Less than min min equal to max, can purchase
            if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_CAN_PURCHASE;
                break;
            }
            // More than min, less than max, can purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $isDeliveryFree = self::BETWEEN_PRICE;
                break;
            }
            // More than max, min equal to max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                $isDeliveryFree = self::MAX_FREE_DELIVERY;
                break;
            }

            $category_id = $freeDeliveryMinPrice->category_id;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 0;
        }

        // Variables initiliazation.
        $color = null;
        $size = null;
        $curtainSize = null;
        $temperature = null;
        $preorder_date = null;
        $order_remark = null;
        $order_selfcollect = null;
        $order_tradein = null;
        $order_installation = null;

        // dd($request);

        if ($request->input('product_preorder_date') != null) {
            $preorder_date = $request->input('product_preorder_date');
        }

        if ($request->input('product_order_remark') != null) {
            $order_remark = $request->input('product_order_remark');
        }

        if ($request->input('product_order_selfcollect') != null) {
            $order_selfcollect = $request->input('product_order_selfcollect');
        }
        //Rebate
        if ($request->input('product_order_tradein') != null) {
            $order_tradein = $request->input('product_order_tradein');
        }
        // Installation
        if ($request->input('product_installation') != null) {
            $order_installation = $request->input('product_installation');
        }

        // If the post request has product color id value in it..
        if ($attributes['color'] != null) {

            // Get selected product color.
            $color = $product->colorAttributes->where('id', $attributes['color'])->first();

            // Set color id for checking purposes.
            $colorId = $color->id;
        }



        // If the post request has product dimension id value in it..
        if ($attributes['product_attribute_size'] != null) {
            // Get selected product dimension.
            $size = $product->sizeAttributes->where('id', $attributes['product_attribute_size'])->first();
            // Set dimension id for checking purposes.
            $sizeId = $size->id;
        }
        // If the post request has product dimension id value in it..
        if ($attributes['product_attribute_curtain_size'] != null) {
            // Get selected product dimension.
            $curtainSize = $product->curtainSizeAttributes->where('id', $attributes['product_attribute_curtain_size'])->first();
            // Set dimension id for checking purposes.
            $curtainSizeId = $curtainSize->id;
        }
        // If the post request has product length id value in it..
        if ($attributes['product_attribute_temperature'] != null) {
            // Get selected product length.
            $temperature = $product->lightTemperatureAttributes->where('id', $attributes['product_attribute_temperature'])->first();
            // Set length id for checking purposes.
            $temperatureId = $temperature->id;
        }

        // Check if the exact item is already in the cart..
        $existingCartItem = new Cart;

        $existingCartItem = $existingCartItem->where('user_id', $user->id);

        $existingCartItem->where('product_id', $product->id);

        if ($color != null) {
            $existingCartItem->where('product_information->product_color_id', $colorId);
        }
        if ($size != null) {
            $existingCartItem->where('product_information->product_size_id', $sizeId);
        }
        if ($curtainSize != null) {
            $existingCartItem->where('product_information->product_curtain_size_id', $curtainSizeId);
        }
        if ($temperature != null) {
            $existingCartItem->where('product_information->product_temperature_id', $temperatureId);
        }
        if ($preorder_date != null) {
            $existingCartItem->where('product_information->product_preorder_date', $preorder_date);
        }
        if ($order_remark != null) {
            $existingCartItem->where('product_information->product_order_remark', $order_remark);
        }
        if (isset($bundle['parentBundleProductID']) && !empty($bundle['parentBundleProductID'])) {
            $existingCartItem->where('bundle_id', $bundle['parentBundleProductID']);
        } else {
            $existingCartItem->where('bundle_id', 0);
        }
        if ($order_selfcollect != null) {
            $existingCartItem->where('product_information->product_order_selfcollect', $order_selfcollect);
        }
        if ($order_tradein != null) {
            $existingCartItem->where('product_information->product_order_tradein', $order_tradein);
        }
        if ($order_installation != null) {
            $existingCartItem->where('product_information->product_installation', $order_installation);
        }

        if ($bundleVariation != NULL) {
            $existingCartItem->where('product_information->bundle_variation', $bundleVariation);
        }

        $existingCartItem = $existingCartItem->where('status', 2001)->first();

        // If item doesn't exist in cart..
        if ($existingCartItem == null || $productType == 'bundle') {
            // Initialize product information array.
            $productInformation = array();

            // Create a new cart item.
            $newCartItem = new Cart;
            $newCartItem->user_id = $user->id;
            $newCartItem->product_id = $product->id;

            $price = 0;
            // Check if the post request has product color id in it..
            if ($color != null) {
                // If yes, assign the color id and name
                $productInformation['product_color_id'] = $color->id;
                $productInformation['product_color_name'] = $color->attribute_name;
                $productInformation['product_code'] = $color->product_code ? $color->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $color->color_hex;
                $productInformation['product_color_img'] = $color->color_images;
            }
            // Check if the post request has product length id in it..
            if ($temperature != null) {
                // If yes, assign the length id and concate the length and measurement unit.
                $productInformation['product_temperature_id'] = $temperature->id;
                $productInformation['product_temperature'] = $temperature->attribute_name;
                $productInformation['product_code'] = $temperature->product_code ? $temperature->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $temperature->color_hex;
                $productInformation['product_color_img'] = $temperature->color_images;
            }
            // Check if the post request has product dimension id in it..
            if ($size != null) {
                // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
                $productInformation['product_size_id'] = $size->id;
                $productInformation['product_size'] = $size->attribute_name;
                $productInformation['product_code'] = $size->product_code ? $size->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $size->color_hex;
                $productInformation['product_color_img'] = $size->color_images;
            }
            // Check if the post request has product dimension id in it..
            if ($curtainSize != null) {
                // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
                $productInformation['product_curtain_size_id'] = $curtainSize->id;
                $productInformation['product_curtain_size'] = $curtainSize->attribute_name;
                $productInformation['product_code'] = $curtainSize->product_code ? $curtainSize->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $curtainSize->color_hex;
                $productInformation['product_color_img'] = $curtainSize->color_images;
            }
            if ($preorder_date != null) {

                $productInformation['product_preorder_date'] = $preorder_date;
            }
            if ($bundleVariation != NULL && ($bundlejson->selection != 'default') && ($bundlejson->name != 'null')) {
                if ($bundlejson->selection == 'color') {
                    $productInformation['product_color_name'] = $bundlejson->name;
                }
                if ($bundlejson->selection == 'size') {
                    $productInformation['product_size'] = $bundlejson->name;
                }
                if ($bundlejson->selection == 'light-temperature') {
                    $productInformation['product_temperature'] = $bundlejson->name;
                }
                if ($bundlejson->selection == 'curtain-size') {
                    $productInformation['product_curtain_size'] = $bundlejson->name;
                }
            }
            if ($order_remark != null) {

                $productInformation['product_order_remark'] = $order_remark;
            }
            if ($order_selfcollect != null) {

                $productInformation['product_order_selfcollect'] = $order_selfcollect;
            }
            if ($order_tradein != null) {

                $productInformation['product_order_tradein'] = $order_tradein;
                $newCartItem->rebate = $productInformation['product_order_tradein'];
            }
            if ($order_installation != null) {

                $productInformation['product_installation'] = $order_installation;
                $newCartItem->rebate = $productInformation['product_installation'];
            }
            if ($userInfo->account_status == '1') { // 1 is DC customer

                if ((isset($size)) && ($size->premium_price != '0')) {

                    $price = $size->premium_price;
                } elseif ((isset($curtainSize)) && $curtainSize->premium_price != '0') {

                    $price = $curtainSize->premium_price;
                } else {

                    $price = $product->premium_price;
                }
            } else {
                if ((isset($size)) && $size->price != '0') {

                    $price = $size->price;
                } elseif ((isset($curtainSize)) && $curtainSize->price != '0') {

                    $price = $curtainSize->price;
                } else {

                    $price = $product->price;
                }
            }
            if ($productInformation['product_code'] == $product->parentProduct->product_code) {

                $productInformation['product_code'] = PaymentGatewayController::sanitize_product_code($productInformation['product_code'], $productInformation);
            }

            //  return $size->price;

            // if ($size != null) {
            //     if ($size->price != 0) {
            //         $price = $sizePrice; // $price = $size->price; TODO: Temporary default to member price.
            //     } else {
            //         $price = $productPrice; // $price = $product->price; TODO: Temporary default to member price.
            //     }
            // } else {
            //     $price = $productPrice; // $price = $product->price; TODO: Temporary default to member price.
            // }

            $newCartItem->product_information = $productInformation;
            $newCartItem->quantity = $qty;
            $newCartItem->product_code = $productInformation['product_code'];



            if ($bundle['truefalse'] == false) {
                $newCartItem->delivery_fee = $product->delivery_fee;
                $newCartItem->installation_fee = $product->installation_fee;
                $newCartItem->unit_price = $price;
                $newCartItem->subtotal_price = $price * $qty;
            } else {
                $newCartItem->delivery_fee = 0;
                $newCartItem->installation_fee = 0;
                $newCartItem->unit_price = 0;
                $newCartItem->subtotal_price = 0;
                $newCartItem->bundle_id = $bundle['parentBundleProductID'];
            }
            // if ($isDeliveryFree == 1) {
            //     $newCartItem->delivery_fee = 0;
            //     $newCartItem->disabled = 0;
            // } elseif ($isDeliveryFree == 2) {
            //     $newCartItem->delivery_fee = 0;
            //     $newCartItem->disabled = 1;
            // } else {
            //     $newCartItem->delivery_fee = $deliveryFee['delivery_fee']* $qty;
            // }
            $newCartItem->save();

            $cartItemId = $newCartItem->id;
        } else {
            // If item exist in cart..
            // Add to the existing quantity.
            $existingCartItem->quantity = $existingCartItem->quantity + $qty;
            // Re calculate the total price.
            if ($bundle['truefalse'] == false) {
                $existingCartItem->subtotal_price = $existingCartItem->quantity * $existingCartItem->unit_price;
            } else {
                $existingCartItem->unit_price = 0;
                $existingCartItem->subtotal_price = 0;
                $existingCartItem->bundle_id = $bundle['parentBundleProductID'];
            }
            // $existingCartItem->delivery_fee = $deliveryFee['delivery_fee'] * $existingCartItem->quantity;
            // if ($isDeliveryFree == 1) {
            //     $existingCartItem->delivery_fee = 0;
            //     $existingCartItem->disabled = 0;
            // } elseif ($isDeliveryFree == 2) {
            //     $existingCartItem->delivery_fee = 0;
            //     $existingCartItem->disabled = 1;
            // } else {
            //     $existingCartItem->delivery_fee = $deliveryFee['delivery_fee'] * $qty;
            // }
            $existingCartItem->save();

            $cartItemId = $existingCartItem->id;
        }

        $totalSubtotalPrice = 0;

        $deliveryItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->where('disabled', 0)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();


        foreach ($deliveryItems as $deliveryItem) {
            $totalSubtotalPrice = $totalSubtotalPrice + $deliveryItem->subtotal_price;
        }
        $isDeliveryFree = 0;
        $category_id = null;
        foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {
            // More than min, less than max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_NO_PURCHASE;
                break;
            }
            // Less than min min equal to max, can purchase
            if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_CAN_PURCHASE;
                break;
            }
            // More than min, less than max, can purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $isDeliveryFree = self::BETWEEN_PRICE;
                break;
            }
            // More than max, min equal to max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                $isDeliveryFree = self::MAX_FREE_DELIVERY;
                break;
            }

            $category_id = $freeDeliveryMinPrice->category_id;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 0;
        }

        foreach ($deliveryItems as $currentCartItem) {
            switch ($isDeliveryFree) {
                case self::LESS_NO_PURCHASE:
                    $fee = 0;
                    $disabled = 1;
                    break;
                case self::LESS_CAN_PURCHASE:
                    if ($group == 'g') {
                        $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                    } else {
                        $fee = $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                    }
                    $disabled = 0;
                    break;
                case self::BETWEEN_PRICE:
                    if ($group == 'g') {
                        $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                    } else {
                        $fee = $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                    }
                    $disabled = 0;
                    break;
                case self::MAX_FREE_DELIVERY:

                    if ($freeStates) {
                        if (in_array($stateDelivery, $freeStates)) {
                            $disabled = 0;
                            $fee =  0;
                        } else {
                            $disabled = 3;
                            $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                        }
                    } else {
                        $disabled = 0;
                        $fee = 0;
                    }

                    break;
                default:
                    $fee =  $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                    $disabled = 0;
            }

            if ($bundle['truefalse'] == false) {
                // $currentCartItem->delivery_fee = $fee;
                $this_selfcollect = (isset($currentCartItem->product_information['product_order_selfcollect']) && $currentCartItem->product_information['product_order_selfcollect']);
                $currentCartItem->delivery_fee =  ($this_selfcollect) ? 0 : $fee;
            } else {
                $currentCartItem->delivery_fee = 0;
            }
            $currentCartItem->disabled =  $disabled;

            $this->itemCount[$category_id]++;
            $currentCartItem->save();
        }




        // return redirect('/shop/cart?buynow=' . $cartItemId);
        return $cartItemId;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cartItem = Cart::find($id);
        $cartItem->status = 2002;
        $cartItem->save();

        return $cartItem;
    }

    public function updateDeliveryMethod(Request $request, $method)
    {
        $data = $method;
        Session::put('bjs_cart_delivery_method', $data);

        $user = User::find(Auth::user()->id);
        $carts  = Cart::where('status', '2001')->where('selected', 1)->where('user_id', $user->id)->get();
        $customer = $user->userInfo;
        $shippingAddress = $customer->shippingAddress;
        $cartsItems = [];
        $otherFee = self::arrayProduct(compact('carts', 'shippingAddress', 'cartsItems', 'customer'));

        $dataa['shippingFee'] =  $otherFee['shippingFee']['amount'];
        $dataa['finalTotal'] =  $otherFee['finalTotal'];

        return $dataa;
    }

    public function updateDeliveryOutletId($outletId)
    {
        $data = $outletId;
        Session::put('bjs_cart_delivery_outletId', $data);
    }

    public function getOutletInfo($outletId)
    {
        $src = NULL;
        $outlet = Outlet::where('id', $outletId)->first();

        if (!isset($outlet)) {
            return null;
        }

        if ($outlet->defaultImage) {
            $src = asset('storage/' . $outlet->defaultImage->path . $outlet->defaultImage->filename);
        } else {
            $src = asset('assets/images/errors/image-not-found.png');
        }

        $showAddress = '
            <img class="shop-img1" src="' . $src . '">
            <p>' . $outlet->outlet_name . '</p>
            <p>' . $outlet->contact_number . '</p>
            <p>' . $outlet->default_address . '</p>
            <p>' . $outlet->postcode  . ',' .  $outlet->city->city_name . '</p>
            <p>' . $outlet->state->name . '</p>
        ';
        return $showAddress;
    }

    public function checkOutModal()
    {
        $deliveryMethod = Session::get('bjs_cart_delivery_method');
        $cookie = json_decode(Cookie::get('addressCookie'));

        try {
            if (auth()->user()) {
                $carts = auth()->user()->carts->where('status', 2001)->where('selected', 1);
                $customer = auth()->user()->userInfo;
                $shippingAddress = $customer->shippingAddress;
                $cartsItems = [];
                if ($carts->isNotEmpty()) {
                    $cartsItems = self::arrayProduct(compact('carts', 'shippingAddress', 'cartsItems', 'customer'));
                    if (collect($cartsItems['items'])->where('selfcollect-only', "true")->isNotEmpty()) {
                        $method = 'Self-pickup';
                        $deliveryMethod = $method;
                        Session::put('bjs_cart_delivery_method', $method);
                    }
                }
            }
        } catch (\Exception $e) {
            info('Override delivery method self collect only error : ' . $e);
        }

        try {
            $deliveryOutletId =  $outlet = NULL;

            if ($deliveryMethod == 'Self-pickup') {
                $deliveryOutletId = Session::get('bjs_cart_delivery_outletId');
                if ($deliveryOutletId == 0) {
                    return $deliveryOutletId;
                }
                $outlet = Outlet::where('id', $deliveryOutletId)->first();
            }

            return view('shop.cart.checkOutModalBody')
                ->with('deliveryMethod', $deliveryMethod)
                ->with('deliveryOutletId', $deliveryOutletId)
                ->with('outlet', $outlet)
                ->with('cookie', $cookie);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }
}
