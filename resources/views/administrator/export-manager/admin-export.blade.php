@extends('layouts.administrator.main')

@section('content')


    <h1>Egg Export</h1>

    @hasrole('administrator|formula_pic')
    <div class="card">
        <div class="card-header">
            Download Egg Order

        </div>

        <div class="card-body">
            <!-- Date filter from date to date-->
            <form class="row" action="{{ route('csv.adminreport', ['report_name' => 'egg']) }}" method="get">
                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{-- ($request->datefrom)?$request->datefrom:'' --}}" autocomplete="off" required>
                    </div>
                </div>

                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{-- ($request->dateto)?$request->dateto:'' --}}" autocomplete="off" required>
                    </div>
                </div>

                {{-- @dd($branches)
                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <select name="branch" class="form-control col-12">
                            <option value="">Select Branch</option>
                            @foreach($branches as $branch)
                                <option value="{{ $branch->id }}">{{ $branch->outlet_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}

                <div class="col-md-auto px-1">
                    <div class="input-group mb-3">
                        <button type="submit" name="downloadType" value="download" class="btn btn-primary mx-2">Download</button>
                    </div>
                </div>
            </form>
            @if($errors->any())
            <br> <p class="text-danger">{{$errors->first()}}</p>
            @endif
        </div>
    </div>
    @endhasrole



@endsection

@push('script')
<script>
//
$(function() {
    $(".date").datepicker({
        dateFormat: 'yy/mm/dd',
        changeMonth: true,
        changeYear: true
        });

        $(".estimatedateonly").datepicker({
        dateFormat: 'yy/mm/dd',
        minDate: 0,
        changeMonth: true,
        changeYear: true
        });

        $(".filterDate").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
});
</script>
@endpush
