<?php

namespace App\Models\Warehouse\User;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;
use App\Models\Warehouse\Location\Wh_location;


class WarehouseUserLocation extends Model
{
    use TableLogs;

    protected $table = 'wh_user_location';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'location_id',
    ];

    public function location()
    {
        return $this->belongsTo(Wh_location::class, 'location_id', 'id');
    }
}
