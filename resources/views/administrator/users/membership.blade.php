@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

<div class="card shadow-sm w-100">
    <div class="card-body row">
        <div class="col-md-12">
            <h1>Membership</h1>
            <br>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="col-md-12">
            <form action="{{ route('administrator.user.membership') }}" method="GET" >

                <div class="row mb-md-4">
                    <div class="form-group col-md-2 mb-md-0">
                        <label for="r" class="m-0">Rank</label>
                        <select class="custom-select" id="r" name="r">
                            <option value="ALL">Choose rank...</option>
                            @foreach ($userLevels as $userLevel)
                            <option value="{{ $userLevel->id }}" @if(request()->r == $userLevel->id) selected @endif>{{ $userLevel->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group col-md-5 col-12 d-flex align-items-end">
                        <select class="custom-select" id="selectSearch" name="selectSearch">
                            <option value="ALL">Choose...</option>
                            <option value="ID" {{ (request()->selectSearch == null || request()->selectSearch == 'ID')
                                ? 'selected' : '' }}>Account ID</option>
                            <option value="NM" {{ request()->selectSearch == 'NM' ? 'selected' : '' }}>Name</option>
                        </select>
                        <div class="input-group-append col-md-4 p-0">
                            <input type="text" class="form-control" id="searchBox" name="searchBox"
                                value="{{ (request()->searchBox) ? request()->searchBox : '' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-auto d-flex align-items-end mb-md-0">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                        <a href="{{ route('administrator.user.membership') }}" class="mx-md-2">
                            <button type="button" class="btn btn-warning" style="color:black;">Reset</button>
                        </a>
                    </div>
                </div>
            </form>
        </div>


        <div class="table-responsive m-2 fixed-table-body">
            <table id="membership-tb" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>User ID</td>
                        <td>Name (NRIC)</td>
                        <td>Rank</td>
                        <td>Dealer</td>
                        <td>Data</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ ($users->currentpage()-1) * $users->perpage() + $loop->index + 1 }}</td>
                        <td>
                            {{ $user->userInfo->account_id }}
                        </td>
                        <td>
                            {{ $user->userInfo->full_name }}
                            <br>
                            ({{ $user->userInfo->nric ?? '-' }})
                        </td>
                        <td>{{ $user->userInfo->showUserLevel() }}</td>
                        <td>
                            <b>{{ optional($user->userInfo->agentInformation)->account_id }}</b>
                            <br>
                            {{ optional($user->userInfo->agentInformation)->full_name }}

                        </td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary d-flex justify-content-center" data-toggle="modal" data-target="#member_{{ $user->id }}">
                                View Data
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="member_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="member_{{ $user->id }}Title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Data for <b>{{ $user->userInfo->full_name  }} ({{ $user->userInfo->account_id }})</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-sm table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <td>Started date</th>
                                                    <td>{{ optional($user->created_at)->format('d/m/Y') }}</th>
                                                </tr>
                                                <tr>
                                                    <td>Total Expenses</th>
                                                    <td>{{ $user->userInfo->user_level == 6000 ? 'MYR '.number_format($user->purchases->sum('purchase_amount')/100,2) : 'N.A' }}</th>
                                                </tr>
                                                <tr>
                                                    <td>Reason</th>
                                                    <td>{{ $user->userMembership ? $user->userMembership->reason : 'N.A' }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Supporting document</th>
                                                    <td>
                                                        @php
                                                            $docs = optional($user->userMembership)->getDoc();
                                                        @endphp
                                                        @if ($docs && $docs['type'] == 'file')
                                                        <a href="{{ $docs['url'] }}">Supporting Document</a>
                                                        @else
                                                        <img class="mw-100" src="{{ $docs['url'] }}" alt="">
                                                        @endif

                                                    </td>
                                                </tr>
                                            </tbody>
                                          </table>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                    </div>
                                </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            @if ($user->userInfo->user_level == 6000)
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-outline-danger d-flex justify-content-center" data-toggle="modal" data-target="#upgrade_{{ $user->id }}">
                                    Upgrade
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="upgrade_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="upgrade_{{ $user->id }}Title" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Upgrade for <b>{{ $user->userInfo->full_name  }} ({{ $user->userInfo->account_id }})</b></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <form action="{{ route('administrator.user.upgradeMembership') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <input name="user_id" type="hidden" class="form-control" value="{{ $user->id }}">

                                                <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="reason">Reason</label>
                                                            <input name="reason" type="text" class="form-control" id="reason" required>
                                                            </div>
                                                        <div class="form-group">
                                                            <label for="supporting_doc">Supporting document</label>
                                                            <input name="supporting_doc" type="file" class="form-control-file" id="supporting_doc" required>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $users->links() }}
    </div>
</div>

@endsection


@push('script')
{{-- <script>
    $(document).ready(function() {
        $('#membership-tb').DataTable();
    });
</script> --}}
@endpush
