<?php

namespace App\Http\Controllers\Shop\v1;

use Auth;
use Cookie;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\Globals\State;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use App\Models\Users\Customers\Cart;
use Illuminate\Support\Facades\View;
use App\Models\Products\ProductAttribute;
use App\Models\Products\Product as PanelProduct;
use App\Http\Controllers\Purchase\PaymentGatewayController;
use App\Http\Controllers\WEB\Shop\CartController;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\Globals\City;
use App\Models\Globals\Countries;
// use App\Http\Controllers\XilnexAPI;
// use App\Http\Controllers\PoslajuAPI;

class CartControllers extends Controller
{
    const LESS_NO_PURCHASE = 1;
    const LESS_CAN_PURCHASE = 2;
    const BETWEEN_PRICE = 3;
    const MAX_FREE_DELIVERY = 4;
    var $itemCount = array();

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // Check if user is authenticated or not.
            if (Auth::check()) {
                // If authenticated, then get their cart.
                $this->cart = Auth::user()->carts->where('status', 2001);
            }
            // Get all categories, with subcategories and its images.
            $categories = Category::orderBy('position', 'ASC')->topLevelCategory();

            // Share the above variable with all views in this controller.
            View::share('categories', $categories);
            View::share('cart', $this->cart);

            // Return the request.
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $currentAddress = array();
        $cities = null;
        $getShipCookie = json_decode(Cookie::get('addressCookie'));
        $user = User::find(Auth::user()->id);
        $customer = $user->userInfo;
        $shippingAddress = $customer->shippingAddress;

        if (isset($getShipCookie)) {

            if (country()->country_id == 'MY') {
                $address3 = $getShipCookie->address_1 ? $getShipCookie->address_3 : '';
            } else {
                $address3 = '';
                if (!isset($getShipCookie->city) && empty($getShipCookie->city)) $getShipCookie->city = 'city';
            }

            $currentAddress['name'] = $getShipCookie->name;
            $currentAddress['contact'] = $getShipCookie->mobile;
            $currentAddress['address1'] = $getShipCookie->address_1;
            $currentAddress['address2'] = $getShipCookie->address_1 ? $getShipCookie->address_2 : '';
            $currentAddress['address3'] = $address3;
            $currentAddress['postcode'] = $getShipCookie->address_1 ? $getShipCookie->postcode : '';
            $currentAddress['city_key'] = $getShipCookie->address_1 ? $getShipCookie->city_key : '';
            $currentAddress['city_name'] = $getShipCookie->address_1 && $getShipCookie->city_key != '0' ? City::where('city_key', $getShipCookie->city_key)->first() : '';
            $currentAddress['city'] = $getShipCookie->address_1 ? $getShipCookie->city : '';
            $currentAddress['state_name'] = $getShipCookie->address_1 ? State::where('id', $getShipCookie->state_id)->first() : '';
            $currentAddress['state'] = $getShipCookie->address_1 ? $getShipCookie->state_id : '';
        }

        $carts = $user->carts->where('status', 2001)->where('country_id', country()->country_id);

        $states = State::where('country_id', country()->country_id)->get();

        $currentAddress = $this->sanitizeAddress($currentAddress, $states);

        if (isset($currentAddress['state'])) {
            $cities = City::orderBy('city_name', 'asc')->where('state_id', $currentAddress['state'])->where('country_id', country()->country_id)->get();
        }

        // dd($carts);

        return view('shop.cart.cart')
            ->with('user', $user)
            ->with('shippingAddress', $shippingAddress)
            ->with('carts', $carts)
            ->with('states', $states)
            ->with('cities', $cities)
            ->with('currentAddress', $currentAddress);
    }

    private function sanitizeAddress($currentAddress, $states)
    {

        if (!empty($currentAddress) && !$states->where('id', $currentAddress['state'])->first()) {
            $state = $states->first();
            $currentAddress['state'] = $state->id;
            $currentAddress['state_name'] = $state;

            $cities  = City::where('state_id', $currentAddress['state'])->where('country_id', country()->country_id)->first();

            $currentAddress['city_key'] =  $cities ? $cities->city_key : '0';
            $currentAddress['city_name'] = $cities ? $cities : '';
            $currentAddress['postcode'] = $currentAddress['address1'] = $currentAddress['address2'] = $currentAddress['address3'] = '';
        }

        return $currentAddress;
    }
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $panelProductId = $request->input('product_id');
        $product = PanelProduct::find($panelProductId);

        $attributes['color'] = $request->input('product_attribute_color');
        $attributes['product_attribute_temperature'] = $request->input('product_attribute_temperature');
        $attributes['product_attribute_curtain_size'] = $request->input('product_attribute_curtain_size');
        $attributes['product_attribute_size'] = $request->input('product_attribute_size');

        $bundle['subItem'] = false;
        $qty = $request->input('productQuantity');

        $inputBundleItem = $request->input('bundle-product');

        if (empty($inputBundleItem)) {
            //Single
            if ($product->parentProduct->product_type == 'bundle') return back()->with(['error_message' => 'cannot add item !!!']);
            $cartItemId = $this->storeOneProductToCart($request, $user, $product, $attributes, $bundle, $qty, NULL);
        } else {

            // Bundle
            $getProductBundleID = array();
            $bundleqty = array();

            foreach ($inputBundleItem as $bundleID => $quantity) {
                $getProductBundleID[] = $bundleID;
                $bundleqty[$bundleID] = $quantity;
            }

            // Double check sub item
            $error = CartController::doubleCheckingSubitem($panelProductId, 1, $getProductBundleID, $bundleqty); // 1 , add 1 bundle only
            if ($error) return back(); // return back with no error message

            $bundleItemWithAttribute = ProductAttribute::selectRaw('panel_product_bundle.id as bundleID, panel_product_bundle.bundle_variation, panel_product_attributes.id as id, panel_product_attributes.attribute_type, panel_product_attributes.attribute_name, panel_product_attributes.panel_product_id')
                ->join('panel_product_bundle', 'panel_product_bundle.product_attribute_id', '=', 'panel_product_attributes.id')
                ->where('panel_product_bundle.panel_product_id', $panelProductId)
                ->whereIn('panel_product_bundle.id', $getProductBundleID)
                ->get();

            if ($bundleItemWithAttribute->isNotEmpty()) {

                $cartItemId = $this->storeOneProductToCart($request, $user, $product, $attributes, $bundle, $qty, NULL);

                $bundle['subItem'] = true;
                $bundle['parentBundleProductID'] = $cartItemId;

                foreach ($bundleItemWithAttribute as $item) {
                    $bundleattributes = array();

                    if ($item->attribute_type == 'color') {
                        $bundleattributes['color'] = $item->id;
                    } else {
                        $bundleattributes['color'] = NULL;
                    }
                    if ($item->attribute_type == 'light-temperature') {
                        $bundleattributes['product_attribute_temperature'] = $item->id;
                    } else {
                        $bundleattributes['product_attribute_temperature'] = NULL;
                    }
                    if ($item->attribute_type == 'curtain-size') {
                        $bundleattributes['product_attribute_curtain_size'] = $item->id;
                    } else {
                        $bundleattributes['product_attribute_curtain_size'] = NULL;
                    }
                    if ($item->attribute_type == 'size') {
                        $bundleattributes['product_attribute_size'] = $item->id;
                    } else {
                        $bundleattributes['product_attribute_size'] = NULL;
                    }

                    $bundleProducts = PanelProduct::find($item->panel_product_id);
                    $bundleVariation[$item->bundleID] = $item->bundle_variation;

                    $this->storeOneProductToCart($request, $user, $bundleProducts, $bundleattributes, $bundle, $bundleqty[$item->bundleID], $bundleVariation[$item->bundleID]);
                }
            }
        }
        if ($request->server('REQUEST_URI') == '/shop/cart/buy-now') {
            return redirect('/shop/cart?buynow=' . $cartItemId);
        } else {
            return redirect()->back();
        }
    }

    public function storeOneProductToCart($request, $user, $product, $attributes, $bundle, $qty, $bundleVariation)
    {
        // Variables initiliazation.
        $color = null;
        $size = null;
        $curtainSize = null;
        $temperature = null;
        $preorder_date = null;
        $order_remark = null;
        $order_selfcollect = null;
        $order_tradein = null;
        $order_installation = null;

        $select_attribute = array();
        // If the post request has product color id value in it..
        if ($attributes['color'] != null) {
            // Get selected product color.
            $color = $product->colorAttributes->where('id', $attributes['color'])->first();
            // Set color id for checking purposes.
            $colorId = $color->id;
            $select_attribute[] = $color;
        }
        // If the post request has product dimension id value in it..
        if ($attributes['product_attribute_size'] != null) {
            $size = $product->sizeAttributes->where('id', $attributes['product_attribute_size'])->first();
            // Set dimension id for checking purposes.
            $sizeId = $size->id;
            $select_attribute[] = $size;
        }
        // If the post request has product dimension id value in it..
        if ($attributes['product_attribute_curtain_size'] != null) {
            // Get selected product dimension.
            $curtainSize = $product->curtainSizeAttributes->where('id', $attributes['product_attribute_curtain_size'])->first();
            // Set dimension id for checking purposes.
            $curtainSizeId = $curtainSize->id;
            $select_attribute[] = $curtainSize;
        }
        // If the post request has product length id value in it..
        if ($attributes['product_attribute_temperature'] != null) {
            // Get selected product length.
            $temperature = $product->lightTemperatureAttributes->where('id', $attributes['product_attribute_temperature'])->first();
            // Set length id for checking purposes.
            $temperatureId = $temperature->id;
            $select_attribute[] = $temperature;
        }

        // Pre Order Date
        if ($request->input('product_preorder_date') != null) {
            $preorder_date = $request->input('product_preorder_date');
        }
        // Remark
        if ($request->input('product_order_remark') != null) {
            $order_remark = $request->input('product_order_remark');
        }
        // Self Collect
        if ($request->input('product_order_selfcollect') != 0) {
            $order_selfcollect = $request->input('product_order_selfcollect');
        }
        // Trade In
        if ($request->input('product_order_tradein') != 0) {
            $order_tradein = $request->input('product_order_tradein');
        }
        // Installation
        if ($request->input('product_installation') != null) {
            $order_installation = $request->input('product_installation');
        }

        // Check if the exact item is already in the cart..
        $existingCartItem = new Cart;

        $existingCartItem = $existingCartItem->where('user_id', $user->id);

        $existingCartItem->where('product_id', $product->id);

        if ($color != null) {
            $existingCartItem->where('product_information->product_color_id', $colorId);
        }
        if ($size != null) {
            $existingCartItem->where('product_information->product_size_id', $sizeId);
        }
        if ($curtainSize != null) {
            $existingCartItem->where('product_information->product_curtain_size_id', $curtainSizeId);
        }
        if ($temperature != null) {
            $existingCartItem->where('product_information->product_temperature_id', $temperatureId);
        }
        if ($preorder_date != null) {
            $existingCartItem->where('product_information->product_preorder_date', $preorder_date);
        }
        if ($order_remark != null) {
            $existingCartItem->where('product_information->product_order_remark', $order_remark);
        }
        if ($order_selfcollect != null) {
            $existingCartItem->where('product_information->product_order_selfcollect', $order_selfcollect);
        }
        if ($order_tradein != null) {
            $existingCartItem->where('product_information->product_order_tradein', $order_tradein);
        }
        if ($order_installation != null) {
            $existingCartItem->where('product_information->product_installation', $order_installation);
        }

        if (isset($bundle['parentBundleProductID'])) {
            $existingCartItem->where('bundle_id', $bundle['parentBundleProductID']);
        } else {
            $existingCartItem->where('bundle_id', 0);
        }
        if ($bundleVariation != NULL) {
            $existingCartItem->where('product_information->bundle_variation', $bundleVariation);
        }
        if ($request->input('countryId') != NULL) {
            $existingCartItem->where('country_id', $request->input('countryId'));
        }

        $existingCartItem = $existingCartItem->where('status', 2001)->first();

        $productType = $product->parentProduct->product_type;

        // If item doesn't exist in cart..
        if ($existingCartItem == null || $productType == 'bundle') {
            // Initialize product information array.
            $productInformation = array();

            // Create a new cart item.
            $newCartItem = new Cart;
            $newCartItem->user_id = $user->id;
            $newCartItem->product_id = $product->id;

            // Check if the post request has product color id in it..
            if ($color != null) {
                // If yes, assign the color id and name
                $productInformation['product_color_id'] = $color->id;
                $productInformation['product_color_name'] = $color->attribute_name;
                // $productInformation['product_code'] = $color->product_code ? $color->product_code : $product->parentProduct->product_code;
                $productInformation['product_color_code'] = $color->color_hex;
                if (isset($color->color_images)) {
                    $productInformation['product_color_img'] = $color->color_images;
                }
            }
            // Check if the post request has product dimension id in it..
            if ($size != null) {
                // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
                $productInformation['product_size_id'] = $size->id;
                $productInformation['product_size'] = $size->attribute_name;
                // $productInformation['product_code'] = $size->product_code ? $size->product_code : $product->parentProduct->product_code;
                if (isset($size->color_images)) {
                    $productInformation['product_color_img'] = $size->color_images;
                }
            }
            // Check if the post request has product dimension id in it..
            if ($curtainSize != null) {
                // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
                $productInformation['product_curtain_size_id'] = $curtainSize->id;
                $productInformation['product_curtain_size'] = $curtainSize->attribute_name;
                // $productInformation['product_code'] = $curtainSize->product_code ? $curtainSize->product_code : $product->parentProduct->product_code;
                if (isset($curtainSize->color_images)) {
                    $productInformation['product_color_img'] = $curtainSize->color_images;
                }
            }
            // Check if the post request has product length id in it..
            if ($temperature != null) {
                // If yes, assign the length id and concate the length and measurement unit.
                $productInformation['product_temperature_id'] = $temperature->id;
                $productInformation['product_temperature'] = $temperature->attribute_name;
                // $productInformation['product_code'] = $temperature->product_code ? $temperature->product_code : $product->parentProduct->product_code;
                if (isset($temperature->color_images)) {
                    $productInformation['product_color_img'] = $temperature->color_images;
                }
            }

            if ((isset($preorder_date)) && $preorder_date != null) {
                $productInformation['product_preorder_date'] = $preorder_date;
            }
            if ((isset($preordorder_remarker_date)) && $order_remark != null) {
                $productInformation['product_order_remark'] = $order_remark;
            }
            if ((isset($order_selfcollect)) && $order_selfcollect != 0) {
                $productInformation['product_order_selfcollect'] = $order_selfcollect;
            }
            if ((isset($order_tradein)) && $order_tradein != null) {
                $productInformation['product_order_tradein'] = $order_tradein;
                $newCartItem->rebate = $productInformation['product_order_tradein'];
            }
            if ((isset($order_remark)) && $order_remark != null) {
                $productInformation['product_order_remark'] = $order_remark;
            }
            if ((isset($order_installation)) && $order_installation != null) {
                $productInformation['product_installation'] = $order_installation;
            }

            if ($bundleVariation != NULL) {
                $bundlejson = json_decode($bundleVariation, FALSE);

                if ($bundlejson->selection != 'default' && $bundlejson->name != 'null') {
                    if ($bundlejson->selection == 'color') {
                        $productInformation['product_color_name'] = $bundlejson->name;
                    }
                    if ($bundlejson->selection == 'size') {
                        $productInformation['product_size'] = $bundlejson->name;
                    }
                    if ($bundlejson->selection == 'light-temperature') {
                        $productInformation['product_temperature'] = $bundlejson->name;
                    }
                    if ($bundlejson->selection == 'curtain-size') {
                        $productInformation['product_curtain_size'] = $bundlejson->name;
                    }
                }
            }

            $productInformation['product_code'] =  $this->selectMainAttribute($select_attribute, $product, $user->userInfo)['productcode'];

            if ($productInformation['product_code'] == $product->parentProduct->product_code) {
                $productInformation['product_code'] = PaymentGatewayController::sanitize_product_code($productInformation['product_code'], $productInformation);
            }

            $newCartItem->product_information = $productInformation;
            $newCartItem->product_code = $productInformation['product_code'];
            $newCartItem->quantity = $qty;

            // Product Price
            $price = 0;
            $userInfo = $user->userInfo;

            if ($userInfo->account_status == '1') { // 1 is DC customer
                if ((isset($size)) && ($size->getPriceAttributes('member_price') != '0')) {
                    $price = $size->getPriceAttributes('member_price');
                } elseif ((isset($curtainSize)) && $curtainSize->getPriceAttributes('member_price') != '0') {
                    $price = $curtainSize->getPriceAttributes('member_price');
                } elseif ((isset($temperature)) && $temperature->getPriceAttributes('member_price') != '0') {
                    $price = $temperature->getPriceAttributes('member_price');
                } elseif ((isset($color)) && $color->getPriceAttributes('member_price') != '0') {
                    $price = $color->getPriceAttributes('member_price');
                } else {
                    $price = $product->getPriceAttributes('member_price');
                }
            } else {
                if ((isset($size)) && $size->getPriceAttributes('price') != '0') {
                    $price = $size->getPriceAttributes('web_offer_price');
                } elseif ((isset($curtainSize)) && $curtainSize->getPriceAttributes('price') != '0') {
                    $price = $curtainSize->getPriceAttributes('web_offer_price');
                } elseif ((isset($temperature)) && $temperature->getPriceAttributes('price') != '0') {
                    $price = $temperature->getPriceAttributes('web_offer_price');
                } elseif ((isset($color)) && $color->getPriceAttributes('price') != '0') {
                    $price = $color->getPriceAttributes('web_offer_price');
                } else {
                    $price = $product->getPriceAttributes('web_offer_price');
                }
            }
            $newCartItem->country_id = $request->input('countryId');

            if ($bundle['subItem'] == false) {
                $newCartItem->delivery_fee = $product->delivery_fee;
                $newCartItem->installation_fee = $product->installation_fee;
                $newCartItem->unit_price = $price;
                $newCartItem->subtotal_price = $price * $qty;
            } else {
                $newCartItem->delivery_fee = 0;
                $newCartItem->installation_fee = 0;
                $newCartItem->unit_price = 0;
                $newCartItem->subtotal_price = 0;
                $newCartItem->bundle_id = $bundle['parentBundleProductID'];
            }

            $newCartItem->save();
            $cartItemId = $newCartItem->id;
        } else {

            // If item exist in cart..
            // Add to the existing quantity.
            $existingCartItem->quantity = $existingCartItem->quantity + $qty;
            // Re calculate the total price.
            if ($bundle['subItem'] == false && $existingCartItem->bundle_id == 0) {
                $existingCartItem->subtotal_price = $existingCartItem->quantity * $existingCartItem->unit_price;
            } else {
                $existingCartItem->unit_price = 0;
                $existingCartItem->subtotal_price = 0;
                $existingCartItem->bundle_id = $bundle['parentBundleProductID'];
            }

            $existingCartItem->save();
            $cartItemId = $existingCartItem->id;
        }


        //*********************** Start Delivery Fee Calculation **********************//

        //product categories
        $productCategoriesArray = [];

        $productCategories = $product->parentProduct->categories;

        foreach ($productCategories as $key => $productCategory) {
            $productCategoriesArray[$key] = $productCategory->id;
        }
        //end product categories

        $panel = $product->panel;

        //cart item
        $deliveryItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->where('disabled', 0)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();

        $freeDeliveryMinPrices = $product->panel->categoriesWithMinPrice
            ->whereIn('category_id', $productCategoriesArray)
            ->all();

        $totalSubtotalPrice = 0;
        foreach ($deliveryItems as $deliveryItem) {
            if ($deliveryItem->product->parentProduct->product_status != 2) {
                $totalSubtotalPrice = $totalSubtotalPrice + $deliveryItem->subtotal_price;
            }
        }

        $isDeliveryFree = 0;
        $category_id = null;
        foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {

            // More than min, less than max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_NO_PURCHASE;
                break;
            }
            // Less than min min equal to max, can purchase
            if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_CAN_PURCHASE;
                break;
            }
            // More than min, less than max, can purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $isDeliveryFree = self::BETWEEN_PRICE;
                break;
            }
            // More than max, min equal to max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                $isDeliveryFree = self::MAX_FREE_DELIVERY;
                break;
            }

            $category_id = $freeDeliveryMinPrice->category_id;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 1;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 0;
        }

        //delivery fee
        $shipCookie = json_decode(Cookie::get('addressCookie'));
        $userShipping = $user->userInfo->shippingAddress;

        if (isset($shipCookie)) {
            $stateDelivery = $shipCookie->state_id;
        } else {
            $stateDelivery = $userShipping->state_id;
        }
        //end delivery fee

        foreach ($deliveryItems as $currentCartItem) {
            $deliveryFee = $currentCartItem
                ->product
                ->deliveries
                ->where('state_id', $stateDelivery)
                ->first();

            if ($deliveryFee) {
                switch ($isDeliveryFree) {
                    case self::LESS_NO_PURCHASE:
                        $fee = 0;
                        if ($currentCartItem->product->parentProduct->product_status != 2) {
                            $disabled = 1;
                        } else {
                            $disabled = 0;
                        }
                        break;
                    case self::LESS_CAN_PURCHASE:
                        if ($group == 'g') {
                            $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                        } else {
                            $fee = $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                        }
                        $disabled = 0;
                        break;
                    case self::BETWEEN_PRICE:
                        if ($group == 'g') {
                            $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                        } else {
                            $fee = $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                        }
                        $disabled = 0;
                        break;
                    case self::MAX_FREE_DELIVERY:

                        if ($freeStates) {
                            if (in_array($stateDelivery, $freeStates)) {
                                $fee =  0;
                                $disabled = 0;
                            } else {
                                $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                                $disabled = 3;
                            }
                        } else {
                            $fee = 0;
                            $disabled = 0;
                        }
                        break;
                    default:
                        $fee =  $deliveryFee['delivery_fee'] * $currentCartItem->quantity;
                        $disabled = 0;
                }
            } else {
                $fee = 0;
                $disabled = 2;
            }





            if ($bundle['subItem'] == false && $currentCartItem->bundle_id == 0) {
                $this_selfcollect = (isset($currentCartItem->product_information['product_order_selfcollect']) && $currentCartItem->product_information['product_order_selfcollect']);
                $currentCartItem->delivery_fee =  ($this_selfcollect) ? 0 : $fee;
            } else {
                $currentCartItem->delivery_fee = 0;
            }

            $currentCartItem->disabled =  $disabled;
            $this->itemCount[$category_id]++;

            $currentCartItem->save();
        }

        //********************* End Delivery Fee Calculation ******************************//

        return $cartItemId;
    }

    public function destroy($id)
    {
        $cartItem = Cart::find($id);
        $cartItem->status = 2002;
        $cartItem->save();

        return $cartItem;
    }

    public function selectMainAttribute($attributes, $product, $userInfo)
    {
        $productcode = NULL;
        $price = 0;
        $countAttributes = count($attributes);

        if ($countAttributes > 1) {

            foreach ($attributes as $key => $att) {
                if ($userInfo->account_status == '1') { // 1 is DC customer
                    if ((isset($att)) && ($att->getPriceAttributes('member_price') != '0')) {
                        $price = $att->getPriceAttributes('member_price');
                        $productcode = $att->product_code ? $att->product_code : $product->parentProduct->product_code;

                        break;
                    } else {
                        $price = $product->getPriceAttributes('member_price');
                        $productcode = $att->product_code ? $att->product_code : $product->parentProduct->product_code;
                    }
                } else {
                    if ((isset($att)) && $att->getPriceAttributes('price') != '0') {
                        $price = $att->getPriceAttributes('web_offer_price');
                        $productcode = $att->product_code ? $att->product_code : $product->parentProduct->product_code;

                        break;
                    } else {
                        $price = $product->getPriceAttributes('web_offer_price');
                        $productcode = $att->product_code ? $att->product_code : $product->parentProduct->product_code;
                    }
                }
            }
        } else {

            if ($userInfo->account_status == '1') { // 1 is DC customer
                if ((isset($attributes[0])) && ($attributes[0]->getPriceAttributes('member_price') != '0')) {
                    $price = $attributes[0]->getPriceAttributes('member_price');
                }
            } else {
                if ((isset($attributes[0])) && $attributes[0]->getPriceAttributes('price') != '0') {
                    $price = $attributes->getPriceAttributes('web_offer_price');
                }
            }

            $productcode = $attributes[0]->product_code ? $attributes[0]->product_code : $product->parentProduct->product_code;
        }

        return compact('productcode', 'price');
    }
}
