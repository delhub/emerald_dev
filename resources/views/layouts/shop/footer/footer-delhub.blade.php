<footer>
<div class="row pl-5 pr-2" style="margin-top: 2rem;">
    <div class="col-6 col-md-3 text-center text-md-left ">
        <ul class="list-unstyled pr-2 pl-2">
            <li>
                <p style="font-size: 1.1rem; color:white;">CONTACT US</p>
                <p class="bujishu-white mb-1 " style="font-size: 0.9rem;">
                    {!!country()->company_address!!}
                </p>
            </li>
            <li>
                <p class="bujishu-white mb-1 " style="font-size: 0.9rem;">
                    Tel: {{country()->company_phone}}
                </p>
            </li>

        </ul>
    </div>
    <div class="offset-5 col-md-3 " style="padding-top:100px;">
        <h6 class="text-right mb-0 pt-1 pb-1 " style="color:black; font-size:13pt;">© 2021 {{country()->company_name}} All Rights Reserved. Website powered by Delhub Digital Sdn. Bhd.</h6>
    </div>
</div>

</footer>
