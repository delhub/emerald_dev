<?php

namespace App\Http\Controllers\Administrator\Warehouse;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ExcelExport;
use App\Models\Globals\Outlet;
use App\Models\Globals\Products\Product;
use App\Models\Globals\Status;
use App\Models\Products\ViewInventoriesStock;
use App\Models\Warehouse\ViewInventoriesStockBundle;
use App\Models\Products\WarehouseInventories;
use App\Models\Users\User;
use App\Traits\TableLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Products\ProductAttribute;
use App\Models\Warehouse\Location\Wh_location;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Models\Products\ProductAttributeBundle;

class InventoryController extends Controller
{
    use TableLogs;

    public static function wh_inventories($country = null)
    {
        $inventoryTables = new WarehouseInventories();
        return $inventoryTables;
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        $locations = wh_location::get();

        $viewProductStocks = ViewInventoriesStock::selectRaw('view_inventories_stock.*')
            ->leftJoin('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'view_inventories_stock.product_code')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            ->orderBy('view_inventories_stock.product_code', 'ASC')
            ->orderBy('view_inventories_stock.physical_stock', 'ASC')
            ->productFilter($request->search_box)
            ->locationFilter($request->location_id);

        if ($user = Auth::user() && !$user->hasRole('wh_supervisor')) {
            $locations = $locations->whereIn('id', $userLocationId);
            $viewProductStocks = $viewProductStocks->where('view_inventories_stock.outlet_id', $userLocationId);
        }

        $viewProductStocks = $viewProductStocks->paginate(50);

        $productStocks = array();
        $checkNull = ViewInventoriesStock::paginate(30);
        foreach ($viewProductStocks as $viewProductStock) {
            ($viewProductStock->product_code != null || $request->search_box == null) ?: $viewProductStock->product_code = $request->search_box;
            $global_products = ProductAttribute::where('product_code', $viewProductStock->product_code)->first();
            // dd($global_products);
            $viewProductStock->product_name = $global_products ? $global_products->attribute_name : '-';
            $viewProductStock->global_product_name = $global_products ? $global_products->product2->parentProduct->name : '-';


            if (!isset($productStocks[$viewProductStock->product_code]['location_id'])) $productStocks[$viewProductStock->product_code]['location_id'] = $viewProductStock->warehouseLocation->location_name;
            if (!isset($productStocks[$viewProductStock->product_code]['product_code'])) $productStocks[$viewProductStock->product_code]['product_code'] = $viewProductStock->product_code;
            if (!isset($productStocks[$viewProductStock->product_code]['product_name'])) $productStocks[$viewProductStock->product_code]['product_name'] = $viewProductStock->product_name;
            if (!isset($productStocks[$viewProductStock->product_code]['physical_stock'])) $productStocks[$viewProductStock->product_code]['physical_stock'] = $viewProductStock->physical_stock;
            if (!isset($productStocks[$viewProductStock->product_code]['virtual_stock_reserved'])) $productStocks[$viewProductStock->product_code]['virtual_stock_reserved'] = $viewProductStock->virtual_stock_reserved;
            if (!isset($productStocks[$viewProductStock->product_code]['virtual_stock_reserved_offline'])) $productStocks[$viewProductStock->product_code]['virtual_stock_reserved_offline'] = $viewProductStock->virtual_stock_reserved_offline;
            if (!isset($productStocks[$viewProductStock->product_code]['virtual_stock'])) $productStocks[$viewProductStock->product_code]['virtual_stock'] = $viewProductStock->virtual_stock;

            if (!isset($productStocks[$viewProductStock->product_code]['pos_completed'])) $productStocks[$viewProductStock->product_code]['pos_completed'] = $viewProductStock->pos_completed;
            if (!isset($productStocks[$viewProductStock->product_code]['online_sold'])) $productStocks[$viewProductStock->product_code]['online_sold'] = $viewProductStock->online_sold;
            if (!isset($productStocks[$viewProductStock->product_code]['total_sold'])) $productStocks[$viewProductStock->product_code]['total_sold'] = $viewProductStock->total_sold;
        }
        $productStocks = ExcelExport::prepareDataExport($productStocks);

        return view('administrator.warehouse.inventory', compact(['viewProductStocks', 'request']))
            ->with('productStocks', $productStocks)
            ->with('locations', $locations);
    }

    public function overall(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        $locations = wh_location::whereIn('id', $userLocationId)->get();

        $viewProductStocks = ViewInventoriesStock::selectRaw('view_inventories_stock.*')
            ->leftJoin('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'view_inventories_stock.product_code')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            ->whereNotIn('view_inventories_stock.outlet_id', [9, 999])
            ->orderBy('view_inventories_stock.product_code', 'ASC')
            ->productFilter($request->search_box);

        if ($user = Auth::user() && !$user->hasRole('wh_supervisor')) {
            $viewProductStocks = $viewProductStocks->where('view_inventories_stock.outlet_id', $userLocationId);
        }

        $viewProductStocks = $viewProductStocks->get(50);

        $lists = array();

        foreach ($viewProductStocks as $viewProductStock) {
            if ($viewProductStock->outlet_id != 999) {

                if (!isset($lists[$viewProductStock->product_code])) {
                    $global_products = ProductAttribute::where('product_code', $viewProductStock->product_code)->first();

                    $lists[$viewProductStock->product_code]['product_name'] = $global_products ? $global_products->attribute_name : '-';
                    $lists[$viewProductStock->product_code]['global_product_name'] = $global_products ? $global_products->product2->parentProduct->name : '-';

                    $lists[$viewProductStock->product_code]['overallPhysical'] = 0;
                    $lists[$viewProductStock->product_code]['virtual_stock_reserved'] = 0;
                    $lists[$viewProductStock->product_code]['virtual_stock_reserved_offline'] = 0;
                    $lists[$viewProductStock->product_code]['virtual_stock'] = 0;

                    $lists[$viewProductStock->product_code]['pos_completed'] = 0;
                    $lists[$viewProductStock->product_code]['online_sold'] = 0;
                    $lists[$viewProductStock->product_code]['total_sold'] = 0;
                }

                $lists[$viewProductStock->product_code]['overallPhysical'] += $viewProductStock->physical_stock;
                $lists[$viewProductStock->product_code]['virtual_stock_reserved'] += $viewProductStock->virtual_stock_reserved;
                $lists[$viewProductStock->product_code]['virtual_stock_reserved_offline'] += $viewProductStock->virtual_stock_reserved_offline;
                $lists[$viewProductStock->product_code]['virtual_stock'] += $viewProductStock->virtual_stock;

                $lists[$viewProductStock->product_code]['pos_completed'] += $viewProductStock->pos_completed;
                $lists[$viewProductStock->product_code]['online_sold'] += $viewProductStock->online_sold;
                $lists[$viewProductStock->product_code]['total_sold'] += $viewProductStock->total_sold;
            }
        }

        $lists = $this->paginate($lists, 200);
        $lists->setPath('');
        $lists->appends(request()->query());

        return view('administrator.warehouse.inventory-overall', compact(['request', 'lists']))
            ->with('locations', $locations);
    }

    public function overallManagement(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        $locations = wh_location::get();

        $viewProductStocks = ViewInventoriesStock::selectRaw('view_inventories_stock.*')
            ->leftJoin('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'view_inventories_stock.product_code')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            ->orderBy('view_inventories_stock.product_code', 'ASC')
            ->productFilter($request->search_box)
            ->locationFilter($request->location)
            ->productQualityFilter($request->quality)
            // ->datebetweenFilter($request->datefrom, $request->dateto)
            ->get();

        if ($user = Auth::user() && !$user->hasRole('wh_supervisor')) {
            $locations = $locations->whereIn('id', $userLocationId);
            $viewProductStocks = $viewProductStocks->where('view_inventories_stock.outlet_id', $userLocationId);
        }

        $lists = array();

        foreach ($viewProductStocks as $key => $viewProductStock) {
            // if (
            //     $viewProductStock->outlet_id != 999 ||
            //     ($viewProductStock->outlet_id == 999 && ($viewProductStock->model_type == 'App\Models\Warehouse\StockTransfer\StockTransfer' || $viewProductStock->model_type == 'App\Models\Warehouse\Order\WarehouseOrder'))
            // ) {

            if (!isset($lists[$viewProductStock->product_code])) {
                $global_products = ProductAttribute::where('product_code', $viewProductStock->product_code)->first();

                $lists[$viewProductStock->product_code]['product_name'] = $global_products ? $global_products->attribute_name : '-';
                $lists[$viewProductStock->product_code]['global_product_name'] = $global_products ? $global_products->product2->parentProduct->name : '-';
                $lists[$viewProductStock->product_code]['quality'] = $global_products ? $global_products->product2->parentProduct->quality_id : '-';

                $lists[$viewProductStock->product_code]['overallPhysical'] = 0;
                $lists[$viewProductStock->product_code]['virtual_stock'] = 0;
                $lists[$viewProductStock->product_code]['total_sold'] = 0;
                $lists[$viewProductStock->product_code]['pos_completed'] = 0;
                $lists[$viewProductStock->product_code]['online_sold'] = 0;

                $lists[$viewProductStock->product_code]['total_stockIn'] = 0;
            }

            $lists[$viewProductStock->product_code]['overallPhysical'] += $viewProductStock->physical_stock;
            $lists[$viewProductStock->product_code]['virtual_stock'] += $viewProductStock->virtual_stock;
            $lists[$viewProductStock->product_code]['total_sold'] += $viewProductStock->total_sold;
            $lists[$viewProductStock->product_code]['pos_completed'] += $viewProductStock->pos_completed;
            $lists[$viewProductStock->product_code]['online_sold'] += $viewProductStock->online_sold;

            $lists[$viewProductStock->product_code]['total_stockIn'] += $viewProductStock->total_stockIn;
            // }
        }


        $lists = $this->paginate($lists, 200);
        $lists->setPath('');
        $lists->appends(request()->query());

        return view('administrator.warehouse.inventory-overall-management', compact(['request', 'lists']))
            ->with('locations', $locations);
    }

    public function overallManagementv2(Request $request)
    {
        $locations = wh_location::get();

        $viewProductStocks = ViewInventoriesStockBundle::selectRaw('view_inventories_stock_bundle.*')
            ->leftJoin('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'view_inventories_stock_bundle.product_code')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            ->orderBy('view_inventories_stock_bundle.product_code', 'ASC')
            ->productFilter($request->search_box)
            ->locationFilter($request->location)
            ->productQualityFilter($request->quality)
            ->get();

        $lists = array();

        foreach ($viewProductStocks as $key => $viewProductStock) {

            if ($viewProductStock->bundle) {
                $product_bundle =  ProductAttributeBundle::where('product_code', $viewProductStock->product_code)->whereIn('active', [1, 2])->first();
                $actual_product_code = $product_bundle->primary_product_code;
                $quantity = $product_bundle->primary_quantity;
            } else {
                $actual_product_code = $viewProductStock->product_code;
                $quantity = 1;
            }

            if (!isset($lists[$actual_product_code])) {
                $global_products = ProductAttribute::where('product_code', $actual_product_code)->first();

                $lists[$actual_product_code]['product_name'] = $global_products ? $global_products->attribute_name : '-';
                $lists[$actual_product_code]['global_product_name'] = $global_products ? $global_products->product2->parentProduct->name : '-';
                $lists[$actual_product_code]['quality'] = $global_products ? $global_products->product2->parentProduct->quality_id : '-';

                $lists[$actual_product_code]['overallPhysical'] = 0;
                $lists[$actual_product_code]['virtual_stock'] = 0;
                $lists[$actual_product_code]['total_sold'] = 0;
                $lists[$actual_product_code]['pos_completed'] = 0;
                $lists[$actual_product_code]['online_sold'] = 0;

                $lists[$actual_product_code]['total_stockIn'] = 0;
            }

            $lists[$actual_product_code]['overallPhysical'] += $viewProductStock->physical_stock * $quantity;
            $lists[$actual_product_code]['virtual_stock'] += $viewProductStock->virtual_stock * $quantity;
            $lists[$actual_product_code]['total_sold'] += $viewProductStock->total_sold * $quantity;
            $lists[$actual_product_code]['pos_completed'] += $viewProductStock->pos_completed * $quantity;
            $lists[$actual_product_code]['online_sold'] += $viewProductStock->online_sold * $quantity;

            $lists[$actual_product_code]['total_stockIn'] += $viewProductStock->total_stockIn * $quantity;
        }

        $lists = $this->paginate($lists, 200);
        $lists->setPath('');
        $lists->appends(request()->query());

        return view('administrator.warehouse.inventory-overall-management-v2', compact(['request', 'lists']))
            ->with('locations', $locations);
    }


    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


    public function download(Request $request)
    {
        $excelExport = new ExcelExport();
        $viewProductStocks = ViewInventoriesStock::productFilter($request->search_box)
            ->locationFilter($request->location_id);

        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        if ($user = Auth::user() && !$user->hasRole('wh_supervisor')) {
            $viewProductStocks = $viewProductStocks->where('view_inventories_stock.outlet_id', $userLocationId);
        }
        $viewProductStocks = $viewProductStocks->get();

        $productStocks = array();
        foreach ($viewProductStocks as $key => $viewProductStock) {
            $global_products = ProductAttribute::where('product_code', $viewProductStock->product_code)->first();
            $viewProductStock->product_name = $global_products ? $global_products->attribute_name : '-';
            $viewProductStock->global_product_name = $global_products ? $global_products->product2->parentProduct->name : '-';

            if (!isset($productStocks[$key]['location_id'])) $productStocks[$key]['location_id'] = $viewProductStock->warehouseLocation->location_name;
            if (!isset($productStocks[$key]['product_code'])) $productStocks[$key]['product_code'] = $viewProductStock->product_code;
            if (!isset($productStocks[$key]['global_product_name'])) $productStocks[$key]['global_product_name'] = $viewProductStock->global_product_name;
            if (!isset($productStocks[$key]['product_name'])) $productStocks[$key]['product_name'] = $viewProductStock->product_name;
            if (!isset($productStocks[$key]['physical_stock'])) $productStocks[$key]['physical_stock'] = $viewProductStock->physical_stock;
            if (!isset($productStocks[$key]['virtual_stock_reserved'])) $productStocks[$key]['virtual_stock_reserved'] = $viewProductStock->virtual_stock_reserved;
            if (!isset($productStocks[$key]['virtual_stock_reserved_offline'])) $productStocks[$key]['virtual_stock_reserved_offline'] = $viewProductStock->virtual_stock_reserved_offline;
            if (!isset($productStocks[$key]['pos_completed'])) $productStocks[$key]['pos_completed'] = $viewProductStock->pos_completed;
            if (!isset($productStocks[$key]['virtual_stock'])) $productStocks[$key]['virtual_stock'] = $viewProductStock->virtual_stock;
        }

        $data = $productStocks;
        $excelExport->setData($data);

        $excelExport->setHeadings(array(
            'Location', 'Product Code', 'Product Name', 'Product Attribute', 'Physical Stock', 'Virtual Stock (Reserved)', 'Virtual Stock (Reserved Offline)', 'POS Completed', 'Virtual Stock'
        ));
        $filename = date('Ymd');
        $fileName = "formula-warehouse-{$filename}.xlsx";
        return $excelExport->download($fileName);
    }

    public function details(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $global_products = ProductAttribute::where('product_code', $request->product_code)->first() ?? false;
        if (!$global_products) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
        $global_products->global_product_name = $global_products ? $global_products->product2->parentProduct->name : '-';

        $outlets = Wh_location::select('id', 'location_key', 'location_name')->get();

        $statuses = Status::all();

        $wh_inventories = WarehouseInventories::where('product_code', $request->product_code)
            ->inventoryOutletFilter($request->inventory_outlet)
            ->inventoryStatusFilter($request->inventory_status)
            ->orderBy('created_at', 'DESC')
            ->paginate(50);

        if (!$user->hasRole('wh_supervisor')) $wh_inventories = $wh_inventories->whereIn('outlet_id', $userLocationId);

        $inv_total = 0;

        foreach ($wh_inventories as $wh_inventory) {
            $model_type = $wh_inventory->model_type;
            $get_model = new $model_type;
            $model_detail = array();
            switch ($model_type) {
                case 'App\Models\Warehouse\Order\WarehouseOrder':
                    $model = $get_model->where('id', $wh_inventory->model_id)->first();
                    $wh_inventory->model = $model->purchase ? $model->purchase->purchase_number : '';
                    break;
                case 'App\Models\Products\WarehouseBatchDetail':
                    $model = $get_model->where('id', $wh_inventory->model_id)->first();
                    $wh_inventory->model = $model ? $model->batch_id : '';
                    break;
                case 'App\Models\Warehouse\StockTransfer\StockTransfer':
                    $model = $get_model->where('id', $wh_inventory->model_id)->first();
                    $wh_inventory->model = $model ? $model->transfer_datetime : '';
                    break;
                    // case 'App\Models\Warehouse\StockAdjustment\StockAdjustment':
                    //     $model = $get_model->where('id', $wh_inventory->model_id)->first();
                    //     $wh_inventory->model = $model ? $model->adjust_type : '';
                    //     break;
                default:
                    $wh_inventory->model = '-';
                    break;
            }

            $inv_total  += $wh_inventory->inv_amount;
        }
        // return $wh_inventories;

        return view('administrator.warehouse.inventory-details', compact(['request', 'global_products', 'wh_inventories', 'user', 'outlets', 'statuses', 'inv_total']));
    }

    // insert directly
    public static function insertData($items)
    {

        foreach ($items as $key => $item) {

            $inventoryTables = self::wh_inventories();
            $inventoryTables->create($item);
        }
    }

    // update from form
    public static function update(Request $request)
    {
        $rq = $request->inv;
        if (in_array($rq[0]['inv_type'], ['transfer_out', 'outlet_sale', 'return', 'damaged'])) {
            $rq[0]['inv_amount'] = -$rq[0]['inv_amount'];
        }

        self::insertData($rq);

        return redirect()->back()->withErrors(['success', 'The Message']);
    }

    // insert directly
    public static function updateData($inventoryRecords)
    {

        //  foreach ($inventoryRecords as $key => $item) {

        //      $inventoryTables = self::wh_inventories();
        //      $inventoryTables->create($item);
        //  }
    }

    public static function findData($inventoryData)
    {
        $wh_inventories = WarehouseInventories::where('model_type', $inventoryData['model_type'])->where('model_id', $inventoryData['model_id'])->first();
    }
}
