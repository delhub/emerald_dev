<?php

namespace App\Http\Controllers\Administrator\Discount;

use App\Discount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Discount\Code;
use App\Models\Discount\Rule;
use App\Models\Discount\Usage;
use App\Models\Globals\Products\Product;
use App\Models\Products\ProductAttribute;
use App\Models\Categories\Category;
use App\Models\Globals\Countries;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use DB;
use stdClass;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrator.discount.index');
    }

    public function getCode()
    {
        $code = Code::groupBy('created_by_purchase')->orderBy('id','DESC')->get();

        if(count($code) > 0) {
            foreach ($code as $k => $v) {
                $v['voucher'] = $this->getCodeByPurchase($v['created_by_purchase']);
                $v->rule;

                $data[] = $v;
            }
            return response()->json($data);
        }
    }

    public function getCodeByPurchase($inv)
    {
        $code = Code::where('created_by_purchase',$inv)->get();

        if(count($code)) {
            return $code;
        }
    }

    public function getCodeById($id)
    {
        $code = Code::where('id',$id)->first() ;

        if($code) {
            $code->expiry= Carbon::parse($code->expiry)->format('Y-m-d');
            return response()->json($code);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCode()
    {
        return view('administrator.discount.newcode');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new stdClass;
        $gen_amount = isset($request->generate_amount) ? $request->generate_amount : 1;

        $data->gen_amount = $gen_amount;
        $data->country_id = $request->country_id;
        $data->coupon_code = $request->coupon_code;
        $data->coupon_type = $request->coupon_type;
        $data->coupon_amount = $request->coupon_amount;
        $data->coupon_rule_id = $request->coupon_rule_id;
        $data->created_by_purchase = isset($request->created_by_purchase) ? $request->created_by_purchase : 'ADM-' .time();
        $data->coupon_status = $request->coupon_status;
        $data->max_voucher_usage = $request->max_usage;
        $data->platform = $request->platform;
        $data->outlet_id = $request->outlet_id;
        $data->expiry = Carbon::createFromFormat('Y-m-d', $request->expiry)->endOfDay()->toDateTimeString();

        for ($x = 1; $x <= $gen_amount; $x++) {

            if($gen_amount > 1) {
                $data->coupon_code = $this->genManualCode(8);
                $this->createManualCode($data);
            }
            else {
                $this->createManualCode($data);
            }
        }

        return redirect('/administrator/discount')->with('success', 'submit done.');
    }

    public function createManualCode($data) {

        $code = new Code;
        $code->country_id = $data->country_id;
        $code->coupon_code = $data->coupon_code;
        $code->coupon_type = $data->coupon_type;
        $code->coupon_amount = $data->coupon_amount;
        $code->coupon_rule_id = $data->coupon_rule_id;
        $code->created_by_purchase = isset($data->created_by_purchase) ? $data->created_by_purchase : 'ADM-' .time();
        $code->coupon_status = $data->coupon_status;
        $code->max_voucher_usage = $data->max_voucher_usage;
        $code->platform = $data->platform;
        $code->outlet_id = $data->outlet_id;
        // $code->expiry = $data->input('expiry');
        $code->expiry = $data->expiry;

        $code->save();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function updateCode(Request $request, $id)
    {
        $this->validate($request, [
            'coupon_status' => 'required'
        ]);

        $code = Code::find($id);
        $code->user_id = isset($request->user_id) ? $request->user_id : -1;
        $code->coupon_status = $request->coupon_status;
        $code->platform = $request->platform;
        $code->outlet_id = $request->outlet_id;
        $code->expiry = Carbon::createFromFormat('Y-m-d', $request->input('expiry'))->endOfDay()->toDateTimeString();
        $code->save();

        //return redirect('/administrator/discount')->with('success', 'Discount Code Updates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function deleteCode($id)
    {
        $code = Code::find($id);
        $code->delete();
        //return redirect('/administrator/discount')->with('success', 'Code Remove');
    }

    public function generateCode($length = 8)
    {
        $d['code'] = Code::generateCode($length);

        if(isset($d['code']) && !empty($d['code'])) {
            return response()->json($d);
        }
    }

    function genManualCode($length = 8) {
        return Code::generateCode($length);
    }


    //Discount rule

    public function rule()
    {
        return view('administrator.discount.rule');
    }

    public function getRule(Request $request)
    {
        if(isset($request->type) && !empty($request->type)) {
            $rule = Rule::where('rule_type',$request->type)->orderBy('id','DESC')->get();
        }
        else
        {
            $rule = Rule::orderBy('id','DESC')->get();
        }

        if(count($rule) > 0) {
            foreach($rule as $val) {
                $val->minimum = round($val->minimum);
                $val->maximum = round($val->maximum);
                $val->step = round($val->step);
                $val->value = round($val->value);
                $val->start_date = Carbon::parse($val->start_date)->format('d/m/Y');
                $val->end_date = Carbon::parse($val->end_date)->format('d/m/Y');
                $val->expiry_date = Carbon::parse($val->expiry_date)->format('d/m/Y');

                $rules[] = $val;
            }

            return response()->json($rules);
        }
        else {
            return [];
        }
    }

    public function getRuleById($id)
    {
        $rule = Rule::where('id',$id)->first();
        $rule->limit_id = json_decode($rule->limit_id);
        $rule->minimum = round($rule->minimum);
        $rule->maximum = round($rule->maximum);
        $rule->step = round($rule->step);
        $rule->value = round($rule->value);
        $rule->start_date = Carbon::parse($rule->start_date)->format('Y-m-d');
        $rule->end_date = Carbon::parse($rule->end_date)->format('Y-m-d');
        $rule->expiry_date = Carbon::parse($rule->expiry_date)->format('Y-m-d');

        if($rule) {
            return response()->json($rule);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRule()
    {
        return view('administrator.discount.newrule');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRule(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'country_id' => 'required',
            'name' => 'required',
            'rule_type' => 'required',
            'coupon_type' => 'required',
            'minimum' => 'required',
            'maximum' => 'required',
            'available_for' => 'required',
            'limit' => 'required',
            'discount_type' => 'required',
            'general_type' => 'required',
            'step' => 'required',
            'value' => 'required',
            // 'times_exceed' => 'required',
            'limit_type' => 'required',
            // 'limit_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        /* $input = $request->all();

        Rule::create($input); */

        $rule = new Rule;

        $rule->country_id = $request->input('country_id');
        $rule->name = $request->input('name');
        $rule->rule_type = $request->input('rule_type');
        $rule->generate_rule_id = $request->input('generate_rule_id');
        $rule->coupon_type = $request->input('coupon_type');
        $rule->minimum = $request->input('minimum');
        $rule->maximum = $request->input('maximum');
        $rule->available_for = $request->input('available_for');
        $rule->limit = $request->input('limit');
        $rule->discount_type = $request->input('discount_type');
        $rule->general_type = $request->input('general_type');
        $rule->step = $request->input('step');
        $rule->value = $request->input('value');
        $rule->times_exceed = $request->input('times_exceed');
        $rule->limit_type = $request->input('limit_type');
        $rule->limit_id = json_encode($request->input('limit_id'),JSON_NUMERIC_CHECK);
        $rule->start_date = Carbon::createFromFormat('Y-m-d', $request->input('start_date'))->startOfDay()->toDateTimeString();
        $rule->end_date = Carbon::createFromFormat('Y-m-d', $request->input('end_date'))->endOfDay()->toDateTimeString();
        $rule->expiry_date = Carbon::createFromFormat('Y-m-d', $request->input('expiry_date'))->endOfDay()->toDateTimeString();
        $rule->expiry_date_type = $request->input('expiry_date_type');
        $rule->breakdown_exp_date = $request->input('breakdown_exp_date');
        $rule->remarks = $request->input('remarks');
        $rule->save();

        return redirect('/administrator/discount/rule')->with('success', 'submit done.');
    }

    public function usage()
    {
        return view('administrator.discount.usage');
    }

    public function usageList()
    {
        $usage = Usage::orderBy('id', 'desc')->get();

        if(count($usage)) {
            return response()->json($usage);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function updateRule(Request $request, $id)
    {
        $this->validate($request, [
            'data.name' => 'required',
            'data.country_id' => 'required',
            'data.rule_type' => 'required',
            'data.coupon_type' => 'required',
            'data.minimum' => 'required',
            'data.maximum' => 'required',
            'data.available_for' => 'required',
            'data.limit' => 'required',
            'data.discount_type' => 'required',
            'data.general_type' => 'required',
            'data.step' => 'required',
            'data.value' => 'required',
            'data.times_exceed' => 'required',
            'data.limit_type' => 'required',
            'limit_id' => 'required',
            'data.start_date' => 'required',
            'data.end_date' => 'required',
            'data.expiry_date' => 'required'
        ]);

        $rule = Rule::find($id);

        $rule->name = $request->input('data.name');
        $rule->country_id = $request->input('data.country_id');
        $rule->rule_type = $request->input('data.rule_type');
        $rule->generate_rule_id = $request->input('data.generate_rule_id');
        $rule->coupon_type = $request->input('data.coupon_type');
        $rule->minimum = $request->input('data.minimum');
        $rule->maximum = $request->input('data.maximum');
        $rule->available_for = $request->input('data.available_for');
        $rule->limit = $request->input('data.limit');
        $rule->discount_type = $request->input('data.discount_type');
        $rule->general_type = $request->input('data.general_type');
        $rule->step = $request->input('data.step');
        $rule->value = $request->input('data.value');
        $rule->times_exceed = $request->input('data.times_exceed');
        $rule->limit_type = $request->input('data.limit_type');
        $rule->limit_id = json_encode($request->input('limit_id'));
        $rule->start_date = $request->input('data.start_date');
        $rule->end_date = Carbon::createFromFormat('Y-m-d', $request->input('data.end_date'))->endOfDay()->toDateTimeString();
        $rule->expiry_date = Carbon::createFromFormat('Y-m-d', $request->input('data.expiry_date'))->endOfDay()->toDateTimeString();
        $rule->breakdown_exp_date = $request->input('data.breakdown_exp_date');
        $rule->expiry_date_type = $request->input('data.expiry_date_type');
        $rule->remarks = $request->input('data.remarks');
        $rule->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function deleteRule($id)
    {
        $rule= Rule::find($id);
        $rule->delete();
        //return redirect('/administrator/discount/rule')->with('success', 'Code Remove');
    }

    public function summary() {
        $codes = Code::orderBy('id', 'DESC')->whereBetween('coupon_rule_id', [1, 6])->get()->groupBy(function($data) {
            return $data->created_at->format('F Y');
        });

        foreach ($codes as $date => $code) {
            // $firstMonth = Carbon::now()->subMonths($m);
            // $endMonth = Carbon::now()->subMonths($m);

            /* $code = Code::groupBy('created_by_purchase')
                ->orderBy('id', 'DESC')
                ->whereBetween('created_at', [$firstMonth->startOfMonth(), $endMonth->endOfMonth()])->get(); */

            /* $code = Code::orderBy('id', 'DESC')
            ->whereBetween('created_at', [$firstMonth->startOfMonth(), $endMonth->endOfMonth()])->get(); */

            for ($i=1; $i <= 6; $i++) {
                $data[$date]["Package_" .$i] = collect($code)->where('coupon_rule_id', $i)->count();
                $data[$date]["usage_" .$i] = collect($code)->where('coupon_rule_id', $i)->sum('usage_counter');
            }
        }

        return view('administrator.discount.summary')
            ->with('data', $data);
    }

    public function selector() {
        return view('administrator.discount.product-selector');
    }

    public function selectorPost(Request $request) {
        if($request->category == 0) {

            switch ($request->option) {
                case 'eligible_discount':
                    if($request->type == 0) {
                        $eligible = 0;
                        $selected_msg = "All product has been selected for eligible discount!";
                    }
                    else {
                        $eligible = 1;
                        $selected_msg = "All product has been deselected from eligible discount!";
                    }

                    $update = ['eligible_discount' => $eligible];

                    break;
                default:
                    if($request->type == 0) {
                        $generate = 0;
                        $selected_msg = "All product has been selected for generate voucher!";
                    }
                    else {
                        $generate = 1;
                        $selected_msg = "All product has been deselected from generate voucher!";
                    }

                    $update = ['generate_discount' => $generate];
            }

            $result = DB::table('global_products')->update($update);
        }

        return redirect('/administrator/discount/product-selector')->with('success', $selected_msg);
    }

    public function getCategory()
    {
        $categories = Category::select('id','name')->orderBy('name','ASC')->get();

        if(count($categories) > 0) {
            return response()->json($categories);
        }
    }

    public function getGlobalProduct()
    {
        $gp = Product::where('product_status',1)->whereNotNull('name')->orderBy('name','ASC')->get();

        if(count($gp) > 0) {
            return response()->json($gp);
        }
    }

    public function getProductAttribute()
    {
        $attribute = ProductAttribute::all();

        if(count($attribute) > 0) {
            foreach ($attribute as $k => $val) {
                $val->productName = $val->product2->parentProduct['name'];
                $data[] = $val;

                unset($val->product2);
            }

            return $data;
        }
    }




}
