<?php

namespace App\Models\Pick;

use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse\Location\Wh_location;

class PickBin extends Model
{
    protected $table = 'pick_bin';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    public function location()
    {
        return $this->hasOne(Wh_location::class, 'id', 'location_id');
    }

    public function pick_user()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'picker');
    }

    public function sort_user()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'sorter');
    }

    public function pack_user()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'packer');
    }

    public function endPickBatch()
    {
        return $this->hasone('App\Models\Pick\PickBatch', 'bin_id', 'id')
            ->where('wh_picking.pick_pack_status', 1);
    }

    public function sortStartBatch()
    {
        return $this->hasone('App\Models\Pick\PickBatch', 'bin_id', 'id')
            ->where('wh_picking.pick_pack_status', 2);
    }

    public function sortEndBatch()
    {
        return $this->hasone('App\Models\Pick\PickBatch', 'bin_id', 'id')
            ->where('wh_picking.pick_pack_status', 3);
    }

    public function packStartBatch()
    {
        return $this->hasone('App\Models\Pick\PickBatch', 'bin_id', 'id')
            ->where('wh_picking.pick_pack_status', 4);
    }

    public function packEndBatch()
    {
        return $this->hasone('App\Models\Pick\PickBatch', 'bin_id', 'id')
            ->where('wh_picking.pick_pack_status', 5);
    }

    public function scopeLocationFilter($query, $location_id)
    {
        if ($location_id != '0' && $location_id != NULL) {
            return $query->where('location_id', $location_id);
        }
    }

    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if ($selectSearch == 'name') {

                return $query
                    ->where('bin_number', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }
}
