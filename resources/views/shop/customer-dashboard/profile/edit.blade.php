@extends('layouts.management.main-customer')

@section('content')

    <form action="{{ route('profile.update', [$customerInfo->id]) }}" method="POST" id="profile-update">
        <input type="hidden" name="_method" value="POST">
        @csrf
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-12 col-md-10">
                <div class="row col-md-12 mywebmenu2">
                    <div class="my-profile-opt-btn active">
                        <i class="fa fa-user mr-1"></i> <a href="{{ route('shop.dashboard.customer.profile.edit') }}"
                            class="text-color-header"><strong>Edit My Profile</strong></a>
                    </div>
                    @hasrole('dealer')
                    <div class="my-profile-opt-btn">
                        <i class="fa fa-address-book-o mr-1"></i> <a
                            href="{{ route('shop.dashboard.dealer.profile.edit') }}"
                            class="text-color-header "><strong>Edit
                                Agent Profile</strong></a>
                    </div>
                    @endhasrole
                    @hasrole('panel')
                    {{-- <div class="my-profile-opt-btn">
          <i class="fa fa-building-o mr-1"></i><a href="{{route('management.company.profile.edit')}}"
            class="text-color-header "><strong> Edit Panel Profile</strong></a>
        </div> --}}
                    @endhasrole
                </div>
                <div class="col-12 mymobilemenu2">
                    <button class="btn btn-secondary mybut3 bjsh-btn-gradient" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        style="background-color:rgb(250, 172, 24); color:black;">
                        Edit My Profile<i style="font-size: 10px;" class="fa fa-arrow-down"> </i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Edit My Profile</a>
                        @hasrole('dealer')
                        <a class="dropdown-item" href="{{ route('shop.dashboard.dealer.profile.edit') }}">Edit Agent
                            Profile</a>
                        @endhasrole
                        @hasrole('panel')
                        <a class="dropdown-item" href="{{ route('management.company.profile.edit') }}">Edit Panel
                            Profile</a>
                        @endhasrole
                    </div>
                </div>
                <div class="card shadow-sm">
                    <div class="card-body">
                        <div class="form-group row ">
                            <div class="col-md-4 m-0">
                                <h4>Profile Information</h4>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row ">
                            <label for="avatar" class=" col-12 col-md-3 col-form-label">Avatar:</label>
                            <div class="col-md-1 m-0">
                                <img src="{{ asset('/images/formula/default-avatar.jpg') }}"
                                    style="max-width:80px; max-height:80px;" alt="avatar">
                            </div>
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="account_id" class="col-md-3 col-form-label">Formula ID</label>
                            <div class="col-md-9 m-0">
                                <input type="text" name="account_id" value="{{ $customerInfo->userInfo->account_id }}"
                                    class="form-control @error('account_id') is-invalid @enderror"
                                    value="{{ old('account_id') }}" readonly>
                            </div>
                            @error('account_id')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="full_name" class="col-md-3 col-form-label">Full Name(NRIC)</label>
                            <div class="col-md-3 m-0">
                                <input type="text" name="full_name" value="{{ $customerInfo->userInfo->full_name }}"
                                    class="form-control @error('full_name') is-invalid @enderror"
                                    value="{{ old('full_name') }}" readonly>
                            </div>
                            @error('full_name')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        @if ($customerInfo->userInfo->nric)
                            <div class="form-group row p-form-box">
                                <label for="birth_date" class="col-12 col-md-3 col-form-label">Full NRIC Number</label>
                                <div class="col-12 col-md-4 m-0">
                                    <input type="number" name="birth_date" id="birth_date"
                                        value="{{ $customerInfo->userInfo->nric }}"
                                        class="form-control  @error('birth_date') is-invalid @enderror"
                                        value="{{ old('birth_date') }}">
                                </div>
                            </div>
                        @elseif($customerInfo->userInfo->date_of_birth)
                            <div class="form-group row p-form-box">
                                <label for="birth_date" class="col-12 col-md-3 col-form-label">Birth Of Date</label>
                                <div class="col-12 col-md-4 m-0">
                                    <input type="date" name="birth_date" id="birth_date"
                                        value="{{ $customerInfo->userInfo->date_of_birth }}"
                                        class="form-control  @error('birth_date') is-invalid @enderror"
                                        value="{{ old('birth_date') }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row p-form-box">
                                <label for="birth_date" class="col-12 col-md-3 col-form-label">Passport No.</label>
                                <div class="col-12 col-md-4 m-0">
                                    <input type="text" name="birth_date" id="birth_date"
                                        value="{{ $customerInfo->userInfo->passport_no }}"
                                        class="form-control  @error('birth_date') is-invalid @enderror"
                                        value="{{ old('birth_date') }}" readonly>
                                </div>
                            </div>
                            <div class="form-group row p-form-box">
                                <label for="country" class="col-12 col-md-3 col-form-label">Country</label>
                                <div class="col-12 col-md-4 m-0">
                                    <input type="text" name="country" id="country"
                                        value="{{ $customerInfo->userInfo->country }}"
                                        class="form-control  @error('country') is-invalid @enderror"
                                        value="{{ old('country') }}" readonly>
                                </div>
                            </div>
                        @endif
                        <div class="form-group row p-form-box">
                            <label for="billing_address_1" class="col-md-3 col-form-label">Billing Address </label>
                            <div class="col-md-9 m-0">
                                <input type="text" name="billing_address_1"
                                    value="{{ $customerInfo->userInfo->mailingAddress->address_1 }}"
                                    class="form-control @error('billing_address_1') is-invalid @enderror"
                                    value="{{ old('billing_address_1') }}">
                            </div>
                            @error('billing_address_1')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box pbox-2">
                            <div class=" offset-md-3 col-md-9">
                                <input type="text" name="billing_address_2"
                                    value="{{ $customerInfo->userInfo->mailingAddress->address_2 }}"
                                    class="form-control @error('billing_address_2') is-invalid @enderror">
                            </div>
                            @error('billing_address_2')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box pbox-2">
                            <div class="offset-md-3 col-md-9">
                                <input type="text" name="billing_address_3"
                                    value="{{ $customerInfo->userInfo->mailingAddress->address_3 }}"
                                    class="form-control @error('billing_address_3') is-invalid @enderror">
                            </div>
                            @error('billing_address_3')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="billing_city" class="col-md-3 col-form-label">Billing City</label>
                            <div class="col-md-4 m-0">
                                <select name="billing_city_key" id="billing_city_key" class="form-control text-capitalize"
                                    style="width: 100%; @error('billing_city_key') is-invalid @enderror"
                                    value="{{ old('billing_city_key') }}">
                                    @foreach ($billcities as $city)
                                        <option value="{{ $city->city_key }}"
                                            {{ $city->city_key == $billing_city_key ? 'selected' : '' }}>
                                            {{ $city->city_name }}</option>
                                    @endforeach
                                    <option value="0" {{ $billing_city_key == '0' ? 'selected' : '' }}>Others</option>
                                </select>
                            </div>
                            <input type="text" id="billing_city" name="billing_city" class="col-md-4 m-0"
                                value="{{ isset($customerInfo->userInfo->mailingAddress->city) ? $customerInfo->userInfo->mailingAddress->city : '' }}"
                                placeholder="Enter Your City Here">
                            @error('billing_city')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="billing_postcode" class="col-md-3 col-form-label">Billing Postcode</label>
                            <div class="col-md-4 m-0">
                                <input type="text" name="billing_postcode"
                                    value="{{ $customerInfo->userInfo->mailingAddress->postcode }}"
                                    class="form-control @error('billing_postcode') is-invalid @enderror"
                                    value="{{ old('billing_postcode') }}">
                            </div>
                            @error('billing_postcode')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="billing_state" class="col-md-3 col-form-label">Billing State</label>
                            <div class="col-md-9 m-0">
                                <select name="billing_state" id="billing_state" class="form-control text-capitalize">
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}"
                                            {{ $state->id == $billing_stateId ? 'selected' : '' }}>
                                            {{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('billing_state')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="company_address" class="col-md-3 col-form-label">Shipping Address </label>
                            <div class="col-md-9 m-0">
                                <input type="text" name="shipping_address_1"
                                    value="{{ isset($customerInfo->userInfo->cartShippingAddress->address_1) ? $customerInfo->userInfo->cartShippingAddress->address_1 : '' }}"
                                    class="form-control @error('shipping_address_1') is-invalid @enderror"
                                    value="{{ old('shipping_address_1') }}">
                            </div>
                            @error('shipping_address_1')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box pbox-2">
                            <div class="offset-md-3 col-md-9">
                                <input type="text" name="shipping_address_2"
                                    value="{{ isset($customerInfo->userInfo->cartShippingAddress->address_2) ? $customerInfo->userInfo->cartShippingAddress->address_2 : '' }}"
                                    class="form-control @error('shipping_address_2') is-invalid @enderror"
                                    value="{{ old('shipping_address_2') }}">
                            </div>
                            @error('shipping_address_2')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box pbox-2">
                            <div class="offset-md-3 col-md-9">
                                <input type="text" name="shipping_address_3"
                                    value="{{ isset($customerInfo->userInfo->cartShippingAddress->address_3) ? $customerInfo->userInfo->cartShippingAddress->address_3 : '' }}"
                                    class="form-control @error('shipping_address_3') is-invalid @enderror"
                                    value="{{ old('shipping_address_3') }}">
                            </div>
                            @error('shipping_address_3')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="shipping_city" class="col-md-3 col-form-label">Shipping City</label>
                            <div class="col-md-4 m-0">
                                <select name="shipping_city_key" id="shipping_city_key" class="form-control text-capitalize"
                                    style="width: 100%; @error('shipping_city_key') is-invalid @enderror"
                                    value="{{ old('shipping_city_key') }}">
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->city_key }}"
                                            {{ $city->city_key == $shipping_city_key ? 'selected' : '' }}>
                                            {{ $city->city_name }}</option>
                                    @endforeach
                                    <option value="0" {{ $shipping_city_key == '0' ? 'selected' : '' }}>Others
                                    </option>
                                </select>
                            </div>
                            <input type="text" id="shipping_city" name="shipping_city" class="col-md-4 m-0"
                                value="{{ isset($customerInfo->userInfo->cartShippingAddress->city) ? $customerInfo->userInfo->cartShippingAddress->city : '' }}"
                                placeholder="Entry Your City Here">
                            @error('shipping_city')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="company_address" class="col-md-3 col-form-label">Shipping Postcode</label>
                            <div class="col-md-4 m-0">
                                <input type="text" name="shipping_postcode"
                                    value="{{ isset($customerInfo->userInfo->cartShippingAddress->postcode) ? $customerInfo->userInfo->cartShippingAddress->postcode : '' }}"
                                    class="form-control @error('shipping_postcode') is-invalid @enderror"
                                    value="{{ old('shipping_postcode') }}">
                            </div>
                            @error('shipping_postcode')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="shipping_state" class="col-md-3 col-form-label">Shipping State</label>
                            <div class="col-md-9 m-0">
                                <select name="shipping_state" id="shipping_state" class="form-control text-capitalize">
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}"
                                            {{ $state->id == $stateId ? 'selected' : '' }}>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('shipping_state')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="mobile_phone" class="col-md-3 col-form-label">Mobile Phone</label>
                            <div class="col-md-4 m-0">
                                <input type="text" name="mobile_phone"
                                    value="{{ $customerInfo->userInfo->mobileContact->contact_num }}"
                                    class="form-control @error('mobile_phone') is-invalid @enderror"
                                    value="{{ old('mobile_phone') }}">
                            </div>
                            @error('mobile_phone')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group row p-form-box">
                            <label for="email" class="col-md-3 col-form-label">Email Address</label>
                            <div class="col-md-4 m-0">
                                <input type="hidden" name="emailOri" id="emailOri" value="{{ $customerInfo->email }}">
                                <input type="text" name="email" id="email" value="{{ $customerInfo->email }}"
                                    class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                            </div>
                            @error('email')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="form-group row p-form-box">
                            <label for="id_information" class="col-md-3 col-form-label">Type Customer</label>
                            <div class="col-md-9 m-0">
                                <input type="text" name="id_information"
                                    value="{{ $customerInfo->userInfo->showUserLevel() }}"
                                    class="form-control @error('id_information') is-invalid @enderror" readonly>
                            </div>
                            @error('id_information')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group p-btn-btm row">
                            <div class="btn-p-update offset-5 offset-md-8 col-md-5 p-pl">
                                <input type="submit" class="btn" id="submitForm" value="Update Profile">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Modal -->
    <div class="modal fade" id="email_change" aria-hidden="true" aria-labelledby="address_edit_formLabel" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
            <div class="modal-content edit-info-modal-box">
                <div class="modal-header">
                    <h1 class="modal-title edit-tittle" id="address_edit_formLabel">You had been <span>logout!</span></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="message">
                    </div>
                </div>
                <div class="edit-opt-btn modal-footer pb-0">
                    <button type="button" class="savechg-btn btn-primary" data-bs-dismiss="modal" aria-label="Close">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    @if($emailChange)
        <a class="dropdown-item" id="logout" href="#" style="display: none;">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </a>
    @endif

    <style>
        .text-color-header {
            color: black;
            font-size: 12pt;
        }


        @media(min-width:300px) and (max-width:699px) {
            .margin-left-phone {
                margin-left: 30px;
            }
        }

        @media(min-width:700px) and (max-width:999px) {
            .margin-left-tablet {
                margin-left: 30px;
            }
        }

        @media(min-width:1000px) and (max-width:1199px) {
            .margin-left-xs {
                margin-left: 70px;
            }
        }

        @media(min-width:1200px) and (max-width:1599px) {
            .margin-left-sm {
                margin-left: 185px;
            }
        }

        @media(min-width:1600px) and (max-width:2000px) {
            .margin-left-md {
                margin-left: 330px;
            }
        }

    </style>

    @push('script')
        <script>
           /*  $('#submitForm').click(function (event) {
                let email = $('#email').val();
                let emailOri = $('#emailOri').val();
                if(emailOri !== email) {
                    $('.modal-body .message').html('<p style="font-size: 1.1rem;" class="message-body-text"> Your login details has been changed. Please login again.</p>');
                    $('#email_change').modal('show');
                    setTimeout(() => $("#profile-update").submit(), 3000);
                    return false;
                }
                event.preventDefault();
                $("#profile-update").submit();
            }) */
            $(document).ready(function() {

                @if($emailChange)
                    $('.modal-body .message').html('<p style="font-size: 1.1rem;" class="message-body-text"> Your login details has been changed. Please login again.</p>');
                    $('#email_change').modal('show');
                    setTimeout(() => $("#logout-form").trigger('submit'), 2000);
                @endif
                /* City */
                var cityKey = document.getElementById("shipping_city_key");
                var city = document.getElementById("shipping_city");
                // var classCityKey = document.getElementById("class_city_key");

                displayCity();

                $('#shipping_city_key').on('change', function() {
                    displayCity();
                });

                $("#shipping_state").on("change", function() {
                    var variableID = $(this).val();

                    if (variableID) {
                        $.ajax({
                            type: "POST",
                            url: '{{ route('web.shop.cart.state-filter-city') }}',
                            data: $("#profile-update").serialize(),
                            success: function(toajax) {
                                $("#shipping_city_key").html(toajax);
                                displayCity();
                            }
                        });
                    }

                });

                function displayCity() {
                    if (cityKey.value == '0') {
                        city.style.display = "block";
                        // classCityKey.className ="col-12 col-md-6 form-group";
                    } else {
                        city.style.display = "none";
                        // classCityKey.className ="col-12 col-md-12 form-group";
                    }
                }
                /* End City */

            });
            $(document).ready(function() {

                /* City */
                var billcityKey = document.getElementById("billing_city_key");
                var billcity = document.getElementById("billing_city");
                // var classCityKey = document.getElementById("class_city_key");


                displaybillCity();

                $('#billing_city_key').on('change', function() {
                    displaybillCity();
                });

                $("#billing_state").on("change", function() {
                    var variableID = $(this).val();

                    if (variableID) {
                        $.ajax({
                            type: "POST",
                            url: '{{ route('web.shop.cart.state-filter-billing-city') }}',
                            data: $("#profile-update").serialize(),
                            success: function(toajax) {
                                $("#billing_city_key").html(toajax);
                                displaybillCity();
                            }
                        });
                    }

                });

                function displaybillCity() {
                    if (billcityKey.value == '0') {
                        billcity.style.display = "block";
                        // classCityKey.className ="col-12 col-md-6 form-group";
                    } else {
                        billcity.style.display = "none";
                        // classCityKey.className ="col-12 col-md-12 form-group";
                    }
                }
                /* End City */

            });

            //side menu disable and layout size
            const hideLeftNMenu = document.querySelector('.row.db-width.hide .col-md-3');
            const hideRightNMenu = document.querySelector('.row.db-width.hide .col-md-9');
            hideLeftNMenu.style.display = 'none';
            hideRightNMenu.className = 'col-md-12 my-box-pd';
        </script>

    @endpush

@endsection
