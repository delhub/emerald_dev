<?php

namespace App\Models\Users;

use App\Models\Globals\Image;
use Illuminate\Database\Eloquent\Model;

class UserMembershipLogs extends Model
{
     // Set table
    protected $table = 'user_memberships_logs';
         // Set timestamps
    public $timestamps = true;

    // Set mass assignable columns
    protected $fillable = [
        'user_id',
        'date_start',
        'date_end',
        'last_checked',
        'total_expenses',
        'reason',
        'image_id',
        'admin_id',
        'checking_status',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id');
    }

    public function getDoc()
    {
        $image = Image::where('id',$this->image_id)->first();
        $docs = array(
            'type' => 'img',
            'url' => 'i##',
        );
        if ($image) {
            $docs['url'] = \asset('storage/'.$image->path.$image->filename);

            $extension = pathinfo($docs['url'], PATHINFO_EXTENSION);
            $imgExtArr = ['jpg', 'jpeg', 'png'];
            if(!in_array($extension, $imgExtArr)){
                $docs['type'] = 'file';
            }
        }
        return $docs;
    }

    public function scopeCheckingStatus($query,$status){
     //
     if (!is_null($status) && $status != 'all') {
          $query = $query->where('checking_status', $status);
     }
     return $query;

    }
    public function scopesearchFilter($query,$searchBox, $selectSearch){
     //
     if (!is_null($searchBox)) {
        $query
        ->join('user_infos', 'user_infos.user_id', '=', 'user_memberships_logs.user_id')
        ->select('user_infos.full_name', 'user_infos.user_id','user_infos.account_id', 'user_memberships_logs.*');

        if ($selectSearch == 'ID') {
            $query
                ->where('user_infos.account_id', 'like', '%' . $searchBox . '%');
        } elseif ($selectSearch == 'NM') {
            $query
                ->where('user_infos.full_name', 'like', '%' . $searchBox . '%');
        }
    }
    return $query;
    }


}
