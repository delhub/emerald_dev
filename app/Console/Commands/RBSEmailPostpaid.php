<?php

namespace App\Console\Commands;

use App\Http\Controllers\Register\v1\DealerRegisterController;
use App\Http\Controllers\Shop\CartController;
use App\Mail\Orders\RBSPostpaidEmailReminder;
use App\Models\Dealers\DealerDirectPay;
use App\Models\Globals\City;
use App\Models\Globals\Settings;
use App\Models\Globals\State;
use App\Models\Products\ProductAttribute;
use App\Models\Rbs\Rbs;
use Illuminate\Console\Command;
use App\Models\Rbs\RbsPostpaid;
use App\Models\Users\Customers\Cart;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use stdClass;

class RBSEmailPostpaid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbs:postpaid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check RBS Postpaid and send email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() // Check if today 1st of every month
    {
        // $firstEveryMonth = Carbon::parse(Carbon::now()->startOfMonth()->format('d-m-Y'))->eq(Carbon::parse(Carbon::now()->format('d-m-Y')));
        $firstEveryMonth = true;

        if ($firstEveryMonth) {

            $this->groupData();
        }
    }

    function groupData() // find active rbs postpaid , foreach save database
    {
        $rbsPostpaidsTables = RbsPostpaid::where('active', 1)->get();
        $rbsPostpaids = array();

        foreach ($rbsPostpaidsTables as $key => $rbsPostpaidsTable) {

            $very_first_reminder =  $initial_reminder =  $subsequent_reminder = false;
            $current_reminders[$key] = null;

            if ($rbsPostpaidsTable->last_reminder == null) {

                // check last order same with today (equal & bigger)
                $very_first_order = Carbon::parse($rbsPostpaidsTable->start_reminder)->lte(today());

                if ($very_first_order) $very_first_reminder = true;
            } else {

                // check active order same with today (equal & bigger)
                $initial_order = Carbon::parse($rbsPostpaidsTable->active_reminder)->addMonths($rbsPostpaidsTable->months_id)->lte(today());

                if ($initial_order && Carbon::parse($rbsPostpaidsTable->last_reminder)->lt(today())) $initial_reminder = true;
            }


            if (!($very_first_reminder || $initial_reminder) && ($rbsPostpaidsTable->active_dp_id !== null)) {

                // check if active_directpay_id is  null  - continue

                $check_subsequent_reminder = $this::check_subsequent_reminder($rbsPostpaidsTable->active_reminder, $rbsPostpaidsTable->last_reminder, today());

                $current_reminders[$key] = $check_subsequent_reminder['title'];
                $subsequent_reminder = $check_subsequent_reminder['subsequent_reminder'];
            }

            if (!$initial_reminder && !$subsequent_reminder && !$very_first_reminder) continue;

            $findAttributes = ProductAttribute::where('id', $rbsPostpaidsTable->attribute_id)->where('active', 1)->first();

            $productStatus = 2;

            if ($findAttributes) {
                $productStatus = $findAttributes->product2->parentProduct->product_status;
            }
            if ($productStatus == 2) continue;

            $user = User::find($rbsPostpaidsTable->user_id);

            $shipping_address = $rbsPostpaidsTable->shipping_address;

            $cartRequest = new stdClass();
            $cartRequest->id = $rbsPostpaidsTable->attribute_id;
            $cartRequest->quantity = $rbsPostpaidsTable->rbs_quantity;
            $cartRequest->rbs_frequency = 0;
            $cartRequest->rbs_price = 0;

            $cart = CartController::globalAddToCart($user, $cartRequest);
            $cart->status = 2004;
            $cart->save();
            info('rbs:postpaid #globalAddToCart cart: ' . $cart);

            $rbsPostpaids[$rbsPostpaidsTable->user_id][json_encode($shipping_address)]['cart'][] = $cart->id;
            $rbsPostpaids[$rbsPostpaidsTable->user_id][json_encode($shipping_address)]['rbsID'][] = $rbsPostpaidsTable->id;
            $rbsPostpaids[$rbsPostpaidsTable->user_id][json_encode($shipping_address)]['active_directpay_id'][] = $rbsPostpaidsTable->active_dp_id;
            $rbsPostpaids[$rbsPostpaidsTable->user_id][json_encode($shipping_address)]['current_reminders'][$rbsPostpaidsTable->id] =  $current_reminders[$key];

            if (!$subsequent_reminder) {
                $rbsPostpaidsTable->active_reminder = now();
                $rbsPostpaidsTable->save();
            }
        }

        // dd($rbsPostpaids);

        $this->saveDatabase($rbsPostpaids);
    }

    public static function check_subsequent_reminder($active_date, $last_reminder, $today = null)
    {
        if (!$today) {
            $today = today();
        } else {
            $today = Carbon::parse($today);
        }

        $default_rbs_subsquent_reminder = array(
            array('days' => 6, 'title' => "Third Reminder"),
            array('days' => 4, 'title' => "Second Reminder"),
            array('days' => 2, 'title' => "First Reminder"),
        );

        $rbs_subsquent_reminder = Settings::getData('rbs_subsquent_reminder', 'json', $default_rbs_subsquent_reminder);

        $rbs_subsquent_reminder = (is_array($rbs_subsquent_reminder)) ? $rbs_subsquent_reminder : json_decode($rbs_subsquent_reminder, true);

        $subsequent_reminders = $rbs_subsquent_reminder;
        $title = null;
        $subsequent_reminder = false;
        $debug = array();
        foreach ($subsequent_reminders as  $reminder) {
            if (
                Carbon::parse($active_date)->addDays($reminder['days'])->eq($today)
                && Carbon::parse($last_reminder)->lt($today)
            ) {
                $subsequent_reminder = true;
                $title  =  $reminder['title'];
            }
            
            // TODO: check for fail safe
            /*  else if (
                Carbon::parse($active_date)->addDays($reminder['days'])->lt($today)
                && in_array(Carbon::parse($last_reminder)->addDays($reminder['days'] + 2)->diffInDays($today, true), [0, 1])
                && Carbon::parse($active_date)->addDays(10)->gt($today)
            ) {
                $subsequent_reminder = true;
                $title  = $reminder['title'];
            } */
        }

        return compact('subsequent_reminder', 'title', 'debug');
    }

    function saveDatabase($rbsPostpaids)
    {
        // dd($rbsPostpaids);
        foreach ($rbsPostpaids as $userID => $rbsAddress) {

            foreach ($rbsAddress as $key => $rbsPostpaid) {


                $shipping_address = json_decode($key);

                // dd($key);

                $globalCity = City::where('city_key', $shipping_address->city)->first();
                $globalState = State::find($shipping_address->state);

                $cokieAddresses = new stdClass();
                $cokieAddresses->name = $shipping_address->name;
                $cokieAddresses->mobile = $shipping_address->phone;
                $cokieAddresses->address_1 = $shipping_address->address1;
                $cokieAddresses->address_2 = $shipping_address->address2;
                $cokieAddresses->address_3 = $shipping_address->address3;
                $cokieAddresses->postcode = $shipping_address->postcode;
                $cokieAddresses->city = $globalCity ? $globalCity->city_name : '';
                $cokieAddresses->city_key = $shipping_address->city;
                $cokieAddresses->city_name = $globalCity;
                $cokieAddresses->state_name = $globalState;
                $cokieAddresses->state_id = $globalState ? $globalState->id : '';
                $cokieAddresses->country_id = country()->country_id;

                $user = User::find($userID);

                // save Direct Pay
                $sessionDeliveryFromEmail = 'delivery';
                $sessionOutletFromEmail = '1';

                // dd($rbsAddress);

                $directPayRequest = new stdClass;
                $carts = Cart::find($rbsPostpaid['cart']);
                $shippingAddress = $cokieAddresses;
                $cartsItems = [];
                $customer = $user->userInfo;
                $isdirectPay = $isCommand = true;
                $directPayRequest->cartItems = CartController::arrayProduct(compact('carts', 'shippingAddress', 'cartsItems', 'customer', 'isdirectPay', 'isCommand'));
                info('rbs:postpaid #arrayProduct cartItems: ' . collect($directPayRequest->cartItems));

                // check reminder
                $directpayReminder = (in_array(null, $rbsPostpaid['current_reminders'])) ? null : collect($rbsPostpaid['current_reminders'])->first();

                // check if multiple directpay link, create new else
                // check if active_directpay is paid, if paid, only create new
                $active_directPay = null;
                if (count($rbsPostpaid['active_directpay_id']) == 1) {
                    $current_activepay_id = $rbsPostpaid['active_directpay_id'][0] ?? 0;
                } else {
                    if (count(array_unique($rbsPostpaid['active_directpay_id'], SORT_REGULAR)) === 1) {
                        $current_activepay_id = $rbsPostpaid['active_directpay_id'][0] ?? 0;
                    } else {
                        $current_activepay_id = 0;
                    }
                }

                if ($current_activepay_id != 0) {
                    $active_directPay =  DealerDirectPay::find($current_activepay_id);

                    if (optional($active_directPay)->purchase && ($active_directPay->purchase->purchase_status == 4002)) {
                        $current_activepay_id = 0;
                    }
                }

                if ($current_activepay_id == 0) {
                    $directPayUuid = DealerRegisterController::globalSaveDirectPay(array_values(array_unique($rbsPostpaid['cart'])), $cokieAddresses, $directPayRequest, $user->id, $sessionDeliveryFromEmail, $sessionOutletFromEmail, 'rbs');
                } else {
                    $directPayUuid = $active_directPay->uuid_link;
                }

                $directPay = DealerDirectPay::where('uuid_link', $directPayUuid)->first();

                // RBSPostpaidEmailReminder::dispatch($user->email, $directPay);
                Mail::to($user->email)->send(new RBSPostpaidEmailReminder($directPay, $directpayReminder));


                $directPay->rbs_id = json_encode($rbsPostpaid['rbsID']);
                $directPay->save();

                // dd($directPayRequest->cartItems);

                $updateRBS = RbsPostpaid::whereIn('id', $rbsPostpaid['rbsID'])->get();

                foreach ($updateRBS as $value) {
                    $value->last_reminder = now();
                    $value->active_dp_id = $directPay->id;
                    $value->save();
                }
            }
        }
    }
}
