<?php

namespace App\Models\Pick;

use Illuminate\Database\Eloquent\Model;
use App\Models\Purchases\Order;

class PickBatchDetail extends Model
{
    protected $table = 'wh_picking_order';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    public function batch()
    {
        return $this->belongsTo('App\Models\Pick\PickBatch', 'key');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Purchases\Item', 'delivery_order', 'delivery_order');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'delivery_order', 'delivery_order');
    }
}
