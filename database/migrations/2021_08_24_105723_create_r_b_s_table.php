<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRBSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases_rbs', function (Blueprint $table) {
            $table->id();
            $table->integer('purchases_id');
            $table->integer('total_months');
            $table->integer('months_completed');
            $table->integer('reminder_interval_months');
            $table->date('start_reminder_date');
            $table->date('latest_reminder_date');
            $table->string('first_po');
            $table->string('first_do');
            $table->string('payment_type')->default('fully_paid')->comment('fully_paid,monthly_paid');
            $table->string('rbs_status')->default('active')->comment('pending,active,cancelled');
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('purchases_rbs_id')->nullable();
            $table->string('order_type')->default('normal')->after('point_amount')->comment('normal,rbs');
        });

        Schema::table('purchases', function (Blueprint $table) {
            $table->string('order_type')->default('normal')->after('point_amount')->comment('normal,rbs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases_rbs');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('order_type');
            $table->dropColumn('purchases_rbs_id');
        });
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropColumn('order_type');
        });
    }
}
