<!-- Orders -->
@php
$pendingTaskLists = [];
foreach ($taskLists as $taskList) {
    if ($taskList->prebuy_redemption == 1 && $taskList->prebuy_redemption_id == null) {
        $pendingTaskLists[] = $taskList;
    }
}
@endphp

@if (count($pendingTaskLists) < 1)
    <div class="card my-2 px-lg-5  px-2">
        <div class="card-body row">
            <div class="col-auto mx-auto">
                <strong>
                    No Order Found.
                </strong>
            </div>
        </div>
    </div>
@else
    @php
        $products = [];
    @endphp
    @foreach ($pendingTaskLists as $pendingTaskList)
        <div class="vr-or-dt mb-4">
            <div class="vr-box">
                <div class="purchase-list-t">
                    <h4>Purchase : <span>{{ $pendingTaskList->inv_number }}</span></h4>
                </div>

                <div class="oderdate-t">
                    <h5>Order Date : <span>{{ $pendingTaskList->purchase_date }}</span></h5>
                </div>
            </div>

            <div class="row vr-order-lst">
                <div class="col-md-7 vr-pt-m0">
                    <div class="row body-inner vr-i-align">
                        <div class="col-md-3 col-3">
                            @if ($pendingTaskList->items->product->parentProduct->defaultImage)
                                <img class="vr-p-img"
                                    src="{{ asset('storage/' .$pendingTaskList->items->product->parentProduct->images[0]->path .$pendingTaskList->items->product->parentProduct->images[0]->filename) }}" />
                            @else
                                <img class="imgproduct" src="{{ asset('/images/product_item/default-p.png') }}"
                                    alt="">
                            @endif
                        </div>
                        <div class="col-md-9 col-9 vr-p-d">
                            <a class="text-dark"
                                href="{{ $pendingTaskList->items->product->parentProduct->getUrlAttribute() }}">
                                <strong style="font-size:15px;" class="font-weight-bold">
                                    {{ $pendingTaskList->items->product->parentProduct->name }}
                                </strong>
                            </a>
                            <br>
                            Quantity : {{ $pendingTaskList->quantity }}
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-3 ">
                    <div class="vr-o-step row">
                        <div class="col-md-3 p-0">Purchaser :</div>
                        <div class="col-md-8 p-0">{{ $pendingTaskList->full_name }}</div>
                        <div class="col-md-3 p-0">Recipient :</div>
                        <div class="col-md-8 p-0">{{ $pendingTaskList->ship_full_name }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @php
            if (!isset($products[$pendingTaskList->items->product_code])) {
                $products[$pendingTaskList->items->product_code]['total'] = 0;
                // $products[$pendingTaskList->items->product_code]['name'] = [];
                $products[$pendingTaskList->items->product_code]['parentProduct'] = [];
            }
            $products[$pendingTaskList->items->product_code]['total'] += $pendingTaskList->quantity;
            // $products[$pendingTaskList->items->product_code]['name'] = $pendingTaskList->items->product->parentProduct->name;
            $products[$pendingTaskList->items->product_code]['parentProduct'] = $pendingTaskList->items->product->parentProduct;

        @endphp
    @endforeach
    <div class="card">
        <h2 style="margin-left: 25px;margin-top: 10px">Total Pending Approval :</h2>
        <div class="card-body">
            @foreach ($products as $product)
                @if ($product['parentProduct']['defaultImage'])
                    <img style="width: 15%;margin: 10px 8px;"
                        src="{{ asset('storage/' . $product['parentProduct']['images'][0]['path'] . $product['parentProduct']['images'][0]['filename']) }}" />
                @else
                    <img class="imgproduct" src="{{ asset('/images/product_item/default-p.png') }}" alt="">
                @endif

                {{ $product['parentProduct']['name'] }}
                &nbsp;&nbsp;&nbsp; <b> X {{ $product['total'] }}</b> pending.
                <br>
            @endforeach
        </div>
    </div>

@endif
