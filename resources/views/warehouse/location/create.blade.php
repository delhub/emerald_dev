@inject('controller', 'App\Http\Controllers\Warehouse\Location\LocationController')

@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{route($controller::$home)}}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Create {{$controller::$header}}
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{route($controller::$store)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf

                        {{-- <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Outlet Image <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <img id="featuredPreview"
                                src=""
                                alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                                <input type="file" name="outletImage" id="outletImage" class="form-control-file"
                                    onchange="document.getElementById('featuredPreview').src = window.URL.createObjectURL(this.files[0])">
                                <small>Please make sure image is in 1:1 display ratio (square).</small>
                            </div>
                        </div> --}}

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Location Name <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="location_name" name="location_name" class="form-control" value="{{ old('location_name') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Type<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="type" class="form-control">
                                    <option value="WH">Warehouse</option>
                                    <option value="SH">Shop Outlet</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Printer Email
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="printer_email" name="printer_email" class="form-control" value="{{ old('printer_email') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Country<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="country_id" class="form-control">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->country_id }}" {{ $country->country_id == 'MY' ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Contact Number
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="contact_number" name="contact_number" class="form-control" value="{{ old('contact_number') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Default Address
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <textarea type="text" id="default_address" name="default_address" class="form-control" value="{{ old('default_address') }}">{{ old('default_address') }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Postcode
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="number" id="postcode" name="postcode" class="form-control" value="{{ old('postcode') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    State<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select  class="select2 select2-edit form-control" style="width: 100%;"
                                    id="state_id" name="state_id">
                                    @foreach($states as $state)
                                        <option value="">Select State....</option>
                                        <option value="{{ $state->id }}">{{ $state->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select  class="select2 select2-edit form-control" style="width: 100%;"
                                    id="city_key" name="city_key">
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Operation Time
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <textarea type="text" id="operation_time" name="operation_time" class="form-control" value="{{ old('operation_time') }}">{{ old('operation_time') }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @push('script')

<script>
$(document).ready(function() {

    /* City */
    var cityKey = document.getElementById("city_key");
    var city = document.getElementById("city");
    // var classCityKey = document.getElementById("class_city_key");


    $("#state_id").on("change",function(){
        var variableID = $(this).val();

        if(variableID){
            $.ajax({
                type:"POST",
                url:'{{route("web.shop.cart.state-filter-city")}}',
                data:$("#create-form-id").serialize(),
                success:function(toajax){
                    $("#city_key").html(toajax);
                }
            });
        }

    });
    /* End City */
});

</script>
@endpush
@endsection


