<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserConnectedPlatform extends Model
{
    // Set table
    protected $table = 'user_connected_platforms';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set incremeneting to false.
    public $incrementing = true;

    // Set mass assignable columns
    protected $fillable = ['email', 'secret_key', 'platform_key'];

    // Casts
    protected $casts = [];
}
