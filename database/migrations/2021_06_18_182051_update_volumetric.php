<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateVolumetric extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->decimal('product_measurement_length', 11, 2)->nullable()->default(0)->change();
            $table->decimal('product_measurement_width', 11, 2)->nullable()->default(0)->change();
            $table->decimal('product_measurement_depth', 11, 2)->nullable()->default(0)->change();
            $table->decimal('product_measurement_weight', 11, 2)->nullable()->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('product_measurement_length');
            $table->dropColumn('product_measurement_width');
            $table->dropColumn('product_measurement_depth');
            $table->dropColumn('product_measurement_weight');
        });
    }
}
