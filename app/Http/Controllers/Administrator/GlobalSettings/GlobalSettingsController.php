<?php

namespace App\Http\Controllers\Administrator\GlobalSettings;

use App\Http\Controllers\Controller;
use App\Models\Globals\Settings;
use Illuminate\Http\Request;

class GlobalSettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::all();
        return view('administrator.global-settings.index', compact('settings'));
    }

    public function store(Request $request)
    {

        foreach ($request->request as $key => $value) {
            // dd($request->request,$key,$value);
            $settings = Settings::where('name',$key)->first();
            if ($settings) {
                $settings->value = $value;
                $settings->save();
            }

        }
        return redirect()->back()->withErrors(['msg' => 'Updated Successfully']);
    }

}
