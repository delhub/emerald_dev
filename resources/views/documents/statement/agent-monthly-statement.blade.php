<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@200;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/monthly-statement.css') }}">
     @include('layouts.favicon')
    <title>Monthly Statement - {{ $dealerID }}</title>
</head>

<body>
    <div class="page-size-a4">
        <div class="inner-page">
            <table class="header">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr class="cpn-address">
                                    <td width="35%">
                                        <img class="cpn-logo" src="{{ asset('images/formula/post-img/logo.png') }}">
                                    </td>
                                    <td width="65%">
                                        <p>Formula Healthcare Sdn. Bhd.<span>(1318429-V)</p>
                                        <p>1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                        <p>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                        <table>
                                            <tr>
                                                <td>
                                                    <p class="cpn-email">
                                                        <img
                                                            src="{{ asset('images/formula/post-img/do-c-icon1.png') }}">
                                                        formula-cs@delhubdigital.com
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="cpn-weblink">
                                                        <img
                                                            src="{{ asset('images/formula/post-img/do-c-icon3.png') }}">
                                                        formula2u.com
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="page-title">
                                        <h4>MONTHLY STATEMENT</h4>
                                        <hr class="dbl-line">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="user-info">
                        <td width='60%'>
                            <p><strong>{{ $dealer->full_name }}</strong></p>
                            <p>{{ $dealerAddress['address_1'] }}</p>
                            <p>{{ $dealerAddress['address_2'] }}</p>
                            <p>{{ $dealerAddress['address_3'] }}</p>
                            <p>{{ $dealerAddress['postcode'] }} {{ $dealerAddress['city'] }},</p>
                            <p>{{ $dealerAddress['state'] }},</p>
                            <p>Malaysia.</p>

                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p>
                                            Date
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $responses['date'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Business Month
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $responses['business_month'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Agent Code
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $dealerID }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Grade
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $responses['rank'] }}</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="body">
                <tbody>
                    <tr class="tit-bg">
                        <td colspan="4">
                            <h4>EXECUTIVE SUMMARY</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Personal Sales</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['summary']['personal'], 2) }}</p>
                        </td>
                        <td>
                            <p>Total Revenue</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['summary']['total'], 2) }}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Group Sales</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['summary']['group'], 2) }}</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Performance Grade</p>
                        </td>
                        <td>
                            <p>{{ $responses['summary']['grade'] }}</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="spc-line">
                        <td colspan="4">
                            <h4>REVENUE BREAKDOWN</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Personal Revenue</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['breakdown']['personal_r'], 2) }}</p>
                        </td>
                        <td>
                            <p>Personal Order</p>
                        </td>
                        <td>
                            <p>{{ $responses['breakdown']['personal_o'] }} Cases</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Group Revenue</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['breakdown']['group_r'], 2) }}</p>
                        </td>
                        <td>
                            <p>Group Order</p>
                        </td>
                        <td>
                            <p>{{ $responses['breakdown']['group_o'] }} Cases</p>
                        </td>
                    </tr>
                    {{-- <tr>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                            <p>Business Development</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['breakdown']['biz'], 2) }}
                            </p>
                        </td>

                    </tr> --}}
                    {{-- <tr>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                            <p>Dealership Incentive</p>
                        </td>
                        <td>
                            <p>RM {{ number_format($responses['breakdown']['incentive'], 2) }}
                            </p>
                        </td>

                    </tr> --}}
                    <tr>
                        <td colspan="4">
                            <hr class="line-20">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Total Revenue</p>
                        </td>
                        <td>
                            <p>

                                RM {{ number_format($responses['summary']['total'], 2) }}
                            </p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="spc-line2">
                        <td colspan="4">
                            <h4>PERSONAL SALES RECORDS</h4>
                        </td>
                    </tr>
                    {{-- <tr>
                        <td colspan="4">
                            <table class="total-price">
                                <tbody>
                                    <tr class="line-bot-top">
                                        <th>Date</th>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th>Total Price (RM)</th>
                                        <th>SV</th>
                                    </tr>
                                    @foreach ($responses['sales_record'] as $sales_record)
                                    <tr class="line-bot">
                                        <td>
                                            {{ $sales_record['date'] }}
                    </td>
                    <td>
                        {{ $sales_record['product'] }}
                    </td>
                    <td class="text-c">
                        {{ $sales_record['quantity'] }}
                    </td>
                    <td class="text-c">
                        {{ $sales_record['price'] }}
                    </td>
                    <td class="text-c bg-light">
                        {{ $sales_record['sv'] }}
                    </td>
                    </tr>
                    @endforeach
                    <tr class="line-bot">
                        <td class="remove-line">

                        </td>
                        <td class="text-r">
                            Total
                        </td>
                        <td class="text-c">
                            {{ array_sum(array_column($responses['sales_record'], 'quantity')) }}
                        </td>
                        <td class="text-c">
                            {{ array_sum(array_column($responses['sales_record'], 'price')) }}
                        </td>
                        <td class="text-c bg-dark">
                            {{ array_sum(array_column($responses['sales_record'], 'sv')) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
            </tr> --}}
                </tbody>
            </table>
            <table class="total-price">
                <tbody>
                    <tr class="line-bot-top">
                        <th width="10%">Date</th>
                        <th width="50%">Product</th>
                        <th width="5%">Qty</th>
                        <th width="20%">Total Price (RM)</th>
                        <th width="20%">SV</th>
                    </tr>
                    @foreach ($responses['sales_record'] as $sales_record)
                        <tr class="line-bot">
                            <td>
                                {{ $sales_record['date'] }}
                            </td>
                            <td>
                                {{ $sales_record['product'] }}
                            </td>
                            <td class="text-c">
                                {{ $sales_record['quantity'] }}
                            </td>
                            <td class="text-c">
                                {{ number_format($sales_record['price'], 2) }}
                            </td>
                            <td class="text-c bg-light">
                                {{ number_format($sales_record['sv'], 2) }}
                            </td>
                        </tr>
                    @endforeach
                    <tr class="line-bot">
                        <td class="remove-line">

                        </td>
                        <td class="text-r">
                            Total
                        </td>
                        <td class="text-c">
                            {{ array_sum(array_column($responses['sales_record'], 'quantity')) }}
                        </td>
                        <td class="text-c">
                            {{ number_format(array_sum(array_column($responses['sales_record'], 'price')), 2) }}
                        </td>
                        <td class="text-c bg-dark">
                            {{ number_format(array_sum(array_column($responses['sales_record'], 'sv')), 2) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="footer">
                <tbody>
                    <tr>
                        <td>
                            <hr class="dbl-line">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                This is computer generated statement no signature required. The company must be notified
                                of any discrepanct from the statement within 7 days upon received, otherwise the
                                information of the statement will be considered accurate.
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
