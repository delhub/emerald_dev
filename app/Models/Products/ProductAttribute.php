<?php

namespace App\Models\Products;

use Cookie;
use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\Countries;
use App\Traits\PriceCalculation;
use App\Models\Products\ProductAttributeBundle;

class ProductAttribute extends Model
{
    use PriceCalculation;
    // Set table
    protected $table = 'panel_product_attributes';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Cast column to another datatype.
    protected $casts = [
        'rbs_frequency' => 'array',
        'rbs_times_send' => 'array'
    ];

    // Set mass assignable columns
    protected $fillable = [
        'panel_product_id',
        'attribute_type',
        'attribute_name',
        'color_hex',
        'color_images',
        'active'
    ];

    /**
     * Get an attribute's product.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Products\Product', 'product_id');
    }

    /* public function globalProductByPanel()
    {
        return $this->belongsTo('App\Models\Globals\Products\Product', 'global_product_id');
    } */

    public function product2()
    {
        return $this->belongsTo('App\Models\Products\Product', 'panel_product_id');
    }

    /**
     * Get all curtain icon.
     */
    public function curtainIcon()
    {
        return $this->belongsTo('App\Models\Globals\Image', 'size_icon');
    }

    public function defaultImage()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable')
            // ->where('default', 1)
        ;
    }

    public function measurement()
    {
        return $this->hasOne('App\Models\Products\ProductMeasurement', 'product_code', 'product_code');
    }

    public function getPrice()
    {
        return $this->morphMany('App\Models\Products\ProductPrices', 'model');
    }

    /**
     * product image attribute.
     */
    public function imageItem()
    {
        return $this->belongsTo('App\Models\Globals\Image', 'color_images');
    }
    public function getImagePathAttribute()
    {
        // dd($this);
        if ($this->imageItem) return asset('storage/' . $this->imageItem->path . $this->imageItem->filename);

        return '';
    }

    public function getShippingCategory()
    {
        return $this->hasOne('App\Models\ShippingInstallations\Ship_category', 'id', 'shipping_category')
            ->where('cat_type', 'shipping')
            ->first();
    }

    //special
    public function getShippingSpecial()
    {
        return $this->hasOne('App\Models\ShippingInstallations\Ship_category', 'id', 'shipping_category_special')
            ->where('cat_type', 'shipping_special')
            ->first();
    }

    public function inventory()
    {
        return $this->hasOne('App\Models\Products\ViewInventoriesStock', 'product_code', 'product_code');
    }

    public function puchongWarehouseinventory($location_id = 1)
    {
        return $this->hasOne('App\Models\Products\ViewInventoriesStock', 'product_code', 'product_code')
            ->where('outlet_id', $location_id);
    }

    public function puchongRetailinventory($location_id = 2)
    {
        return $this->hasOne('App\Models\Products\ViewInventoriesStock', 'product_code', 'product_code')
            ->where('outlet_id', $location_id);
    }

    public function dummyInventory($location_id = 999)
    {
        return $this->hasOne('App\Models\Products\ViewInventoriesStock', 'product_code', 'product_code')
            ->where('outlet_id', $location_id);
    }

    //online + retail
    public function totalSold()
    {
        $online = $retail = $dummy = $total = $online_reserved = $online_reserved_offline = $online_completed = 0;
        $dummy_reserved = $dummy_reserved_offline = $dummy_completed = 0;

        if ($this->puchongWarehouseinventory) {
            $online_reserved = $this->puchongWarehouseinventory->virtual_stock_reserved;
            $online_reserved_offline = $this->puchongWarehouseinventory->virtual_stock_reserved_offline;
            $online_completed = $this->puchongWarehouseinventory->online_completed;
        }

        $online = $online_reserved + $online_reserved_offline + $online_completed;

        $retail = ($this->puchongRetailinventory != NULL) ? (int)$this->puchongRetailinventory->pos_completed : 0;

        if ($this->dummyInventory) {
            $dummy_reserved = $this->dummyInventory->virtual_stock_reserved;
            $dummy_reserved_offline = $this->dummyInventory->virtual_stock_reserved_offline;
            $dummy_completed = $this->dummyInventory->online_completed;
        }

        $dummy = $dummy_reserved + $dummy_reserved_offline + $dummy_completed;
        // dd($dummy, $dummy_reserved, $dummy_reserved_offline, $dummy_completed);

        // if ($this->puchongRetailinventory)  (int)$this->puchongRetailinventory->pos_completed;
        $total = $online + $retail + $dummy;
        // dd($online, $retail, $dummy, $total);

        // $total = str_replace('-', '', $total);

        return ([
            'online' => $online,
            'retail' => $retail,
            'dummy' => $dummy,
            'total' => $total,
        ]);
    }

    public function scopecategoryFilter($query, $shippingCategory, $shippingCategorySpecial)
    {
        if (!is_null($shippingCategory) && $shippingCategory != 'all') {
            $query =  $query->where('shipping_category', $shippingCategory);
        }

        if (!is_null($shippingCategorySpecial) && $shippingCategorySpecial != 'all') {
            $query =  $query->where('shipping_category_special', $shippingCategorySpecial);
        }

        return $query;
    }

    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if ($selectSearch == 'name') {

                return $query
                    ->where('attribute_name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'code') {

                return $query
                    ->where('product_code', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }

    // find items.id
    public function scopefindID($query, $ids)
    {
        $findid = explode(",", $ids);

        if (!is_null($ids)) {

            return $query
                ->whereIn('id', $findid);
        }
    }

    public function rbsFrequency()
    {
        return $this->hasMany('App\Models\Rbs\RbsOptions', 'id', 'rbs_frequency')
            ->where('option_status', 'active')
            ->where('option_type', 'frequency');
    }

    public function productBundle()
    {
        return $this->hasMany(ProductAttributeBundle::class, 'attribute_id')->where('active', 2);
    }

    public function productLimit()
    {
        return $this->hasMany(ProductAttributeLimit::class, 'attribute_id')->where('active', 1);
    }

}
