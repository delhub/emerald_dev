@inject('controller', 'App\Http\Controllers\Administrator\v1\Product\ProductByPanelController')

@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <a href="/administrator/products/panels" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back</a>
    </div>
</div>
<div class="card shadow-sm mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <h4>
                    Product Information
                </h4>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-md-2">
                        <img src="@if ($globalProduct->defaultImage) {{ asset('storage/' . $globalProduct->defaultImage->path . $globalProduct->defaultImage->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif"
                            class="mw-100" alt="">
                    </div>
                    <div class="col-12 col-md-10">
                        <div class="row">
                            <div class="col-12 col-md-4 form-group">
                                <label for="productName">Product Name</label>
                                <input type="text" id="productName" class="form-control" disabled
                                    value="{{ $globalProduct->name }}" style="background-color: #ffffff;">
                            </div>
                            <div class="col-12 col-md-4 form-group">
                                <label for="productQuality">Product Quality</label>
                                <input type="text" id="productQuality" class="form-control" disabled
                                    value="{{ $globalProduct->quality->name }}" style="background-color: #ffffff;">
                            </div>
                            <div class="col-12 col-md-4 form-group">
                                <label for="productCode">Product Code</label>
                                <input type="text" id="productCode" class="form-control" disabled
                                    value="{{ $globalProduct->product_code }}" style="background-color: #ffffff;">
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <!-- Start Form -->
                    <div class="col-12 col-md-10 offset-md-1">
                        <form action="{{ route('administrator.v1.products.panels.store') }}" method="POST"
                            id="create-panel-product-form">
                            @csrf
                            <input type="hidden" name="global_product_id" value="{{ $globalProduct->id }}">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="mb-3">Product By Panel ..</h5>
                                </div>

                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Panel Account ID <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <select name="panel_id" id="panel_id" class="select2 form-control">
                                        <option value="default">Please choose a panel.</option>
                                        @foreach($panels as $panel)
                                        <option value="{{ $panel->account_id }}">
                                            {{ $panel->account_id }} - {{ $panel-> company_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Panel name for display <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <select name="display_panel_name" id="display_panel_name"
                                        class="select2 form-control">
                                        <option value="default">Please choose a company name for display.</option>

                                        @foreach($panels as $panel)
                                        <option value="{{ $panel->company_name}}"
                                            {{ ($panel->name_for_display == $panel->company_name) ? 'selected' : '' }}>
                                            {{ $panel->account_id }} -
                                            {{ $panel->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Estimate Date for Ship Out (days)<small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <input type="number" name="estimate_ship_out_date" id="estimate_ship_out_date"
                                        class="form-control">
                                </div>
                            </div>

                            {{-- @foreach ($controller::$category_type as $key => $category_name)
                                <div class="row">
                                    <div class="col-12 col-md-4 text-md-right my-auto">
                                        <p>
                                            {{$category_name}}<small class="text-danger">*</small>
                                        </p>
                                    </div>

                                    <div class="col-12 col-md-8 form-group">
                                        <select class="select2 form-control productCategory" id="{{$key}}Category" name="{{$key}}categories">
                                            <option value="default">Please choose {{$key}} category. </option>
                                            @foreach($shippingCategory->where('cat_type', $key); as $shippingcategory)
                                            <option value="{{ $shippingcategory->id }}">Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endforeach --}}

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Shipping Categories<small class="text-danger">*</small>
                                    </p>
                                </div>
                                @php
                                $key = 'shipping';
                                $categoryName = $key.'_category';
                                @endphp
                                <div class="col-12 col-md-8 form-group">
                                    <select class="select2 form-control productCategory" id="{{$key}}Category" name="{{$key}}categories{{ $key == 'installation' ? '[]' : '' }}" {{ $key == 'installation' ? 'multiple="multiple"' : '' }}>
                                        <option value="0">Please choose {{$key}} category. </option>
                                        @foreach($shippingCategory->where('cat_type', $key); as $shippingcategory)
                                        <option value="{{ $shippingcategory->id }}">Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Installation Categories<small class="text-danger">*</small>
                                    </p>
                                </div>
                                @php
                                $key = 'installation';
                                $categoryName = $key.'_category';
                                @endphp
                                <div class="col-12 col-md-8 form-group">
                                    <select class="select2 form-control productCategory" id="installationCategory" name="installationCategories[]"multiple="multiple">
                                        <option value="0">Please choose installation category. </option>
                                        @foreach($shippingCategory->where('cat_type', 'installation'); as $shippingcategory)
                                        <option value="{{ $shippingcategory->id }}">Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Ships From <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <select name="ships_from" id="ships_from" class="select2 form-control">
                                        <option value="default">Please choose origin state..</option>
                                        @foreach($states as $state)
                                        <option value="{{ $state->id }}">
                                            {{ $state->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Available In <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8" id="availableInContainer">
                                    <div class="row no-gutters default-item">
                                        <div class="col-12 col-md-6 form-group p-1">
                                            <select name="available_in[]" id="ships_state" class="select2 form-control available-in">
                                                <option value="default">Please choose a state..</option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}">
                                                    {{ $state->name }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-10 col-md-6 form-group p-1">
                                            <input type="text" name="available_in_price[]"
                                                class="form-control input-mask-price d-inline-block"
                                                style="width: 75%;">


                                            <button type="button" class="btn btn-danger d-none" style="width: 20%;"
                                                onclick="removeAvailableInElement(this);">

                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style="width: 20%;"
                                                onclick="addMoreAvailableInElement();">

                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Know More <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_description" id="product_description" cols="30" rows="10"
                                        class="form-control summernote"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Materials <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_material" id="product_material" cols="30" rows="10"
                                        class="form-control summernote"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Consistency <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_consistency" id="product_consistency" cols="30" rows="10"
                                        class="form-control summernote"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Package <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_package" id="product_package" cols="30" rows="10"
                                        class="form-control summernote"></textarea>
                                </div>
                            </div>

                            <hr>
                            <!-- Delivery Coondition -->

                            <div class="row my-2">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <h5 class="mb-3">Product Delivery ..</h5>
                                </div>
                                <div class="col-6 col-md-8">
                                    <div class="form-check form-check-inline ">
                                        <label class="form-check-label check" for="individualDeliveryRange">
                                            <input class="inputGroup form-check-input" type="checkbox"
                                                id="individualDeliveryRange"
                                                {{ $deliveries->pluck('group_or_individual')->first() == 'i' ? 'checked' : '' }}>
                                            Individual <i class="fas fa-question-circle"
                                                title="Delivery fee is based on each product."></i>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline ">
                                        <label class="form-check-label check" for="groupDeliveryRange">
                                            <input class="form-check-input" type="checkbox" id="groupDeliveryRange"
                                                {{ $deliveries->pluck('group_or_individual')->first() == 'g' ? 'checked' : '' }}>
                                            Group <i class="fas fa-question-circle"
                                                title="Delivery fee is group by product category."></i>
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <div class="row my-2">
                                <div class="col-12 offset-md-2 col-md-10 border" id="deliveryRangeContainer">
                                    @if($deliveries->count() > 0 )
                                    @foreach ($deliveries as $deliveryRange)
                                    <div class="row px-3 p-3 @if($loop->last) default-item @endif">
                                        <div class="col-6 d-none">
                                            <input type="text" class="newID" name="firstCategory[]"
                                                value="{{ $deliveryRange->id }}">
                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="individualDeliveryRange"><input
                                                        class="inputGroup form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="individualDeliveryRange" value="i"
                                                        {{ $deliveryRange->group_or_individual=='i'?'checked':'' }}>Individual</label>
                                            </div>
                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="groupDeliveryRange"><input
                                                        class="form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="groupDeliveryRange" value="g"
                                                        {{ $deliveryRange->group_or_individual=='g'?'checked':'' }}>Group</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="min_delivery" class="col-5 pr-0 col-form-label my-auto">Min
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="min_delivery[]"
                                                        class="input-mask-price form-control" id="min_delivery"
                                                        value="{{ $deliveryRange->min_price }}">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="max_delivery" class="col-5 pr-0 col-form-label my-auto">Max
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="max_delivery[]"
                                                        class="input-mask-price form-control" id="max_delivery"
                                                        value="{{ $deliveryRange->max_price }}">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="inputPassword"
                                                    class="col-5 pr-0 col-form-label my-auto">Delivery
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="fixed_delivery[]"
                                                        class="input-mask-price form-control" id="fixed_delivery"
                                                        value="{{ $deliveryRange->delivery_fee }}">
                                                </div>
                                            </div>
                                            <div class="col-2 row mx-auto px-0">
                                                <div class="form-check my-auto">
                                                    <select name="no_purchase[]"
                                                        class="select2 form-control no-purchase">
                                                        <option value="0"
                                                            {{ $deliveryRange->no_purchase == '0' ? 'selected' : '' }}>
                                                            Can Purchase</option>
                                                        <option value="1"
                                                            {{ $deliveryRange->no_purchase == '1' ? 'selected' : '' }}>
                                                            No Purchase</option>
                                                    </select>
                                                </div>
                                            </div>
                                            @if(!$loop->last)
                                            <button type="button" class="btn btn-danger" style=""
                                                onclick="removeDeliveryRangeElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            @else
                                            <button type="button" class="btn btn-danger d-none" style=""
                                                onclick="removeDeliveryRangeElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style=""
                                                onclick="addDeliveryRangeElement();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="row px-3 p-3 default-item">
                                        <div class="col-6 d-none">

                                            <input type="text" class="newID" name="firstCategory[]" value="new">

                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="individualDeliveryRange"><input
                                                        class="inputGroup form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="individualDeliveryRange"
                                                        value="i">Individual</label>
                                            </div>
                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="groupDeliveryRange"><input
                                                        class="form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="groupDeliveryRange"
                                                        value="g">Group</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="min_delivery" class="col-5 pr-0 col-form-label my-auto">Min
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="min_delivery[]"
                                                        class="input-mask-price form-control" id="min_delivery">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="max_delivery" class="col-5 pr-0 col-form-label my-auto">Max
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="max_delivery[]"
                                                        class="input-mask-price form-control" id="max_delivery">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="inputPassword"
                                                    class="col-5 pr-0 col-form-label my-auto">Delivery
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="fixed_delivery[]"
                                                        class="input-mask-price form-control" id="fixed_delivery">
                                                </div>
                                            </div>
                                            <div class="col-2 row mx-auto px-0">
                                                <div class="form-check my-auto">
                                                    <select name="no_purchase[]" class="select2 form-control ">
                                                        <option value="0">
                                                            Can Purchase</option>
                                                        <option value="1">
                                                            No Purchase</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-danger d-none" style=""
                                                onclick="removeDeliveryRangeElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style=""
                                                onclick="addDeliveryRangeElement();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    Delivery Information
                                </div>
                                <div class="col-6 col-md-10">
                                    <textarea name="delivery_text" id="delivery_text" rows="1"
                                        class="form-control summernote">
                                            {!! isset($deliveries->first()->delivery_text) ? $deliveries->first()->delivery_text : '' !!}
                                        </textarea>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    Free delivery state
                                </div>
                                <div class="col-6 col-md-10">
                                    <div class="row">
                                        @foreach($states as $state)
                                        <div class="col-md-2">
                                            <span class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input item-checkbox"
                                                    name="freeStates[]" id="freeStates-{{ $state->id }}"
                                                    value="{{ $state->id }}" @if ($deliveries->count() > 0)

                                                @if(($deliveries->first()->free_state != null))
                                                {{ in_array($state->id , json_decode($deliveries->first()->free_state)) ? 'checked' : '' }}
                                                @endif

                                                @endif
                                                >
                                                <label class="custom-control-label" for="freeStates-{{ $state->id }}">
                                                    {{ $state->name }}</label>
                                            </span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <h5 class="mb-3">Product Variations ..</h5>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <p>
                                        Variations <small class="text-danger">*</small>
                                    </p>
                                </div>

                                <div class="col-12 col-md-10" id="variationContainer">
                                    @php
                                        $attribute = null;
                                    @endphp
                                    @include('administrator.products.v1.panel.partial.product-variations')
                                </div>
                            </div>

                            {{-- Bundle Design --}}
                            @if ($globalProduct->product_type =='bundle')
                                <hr>

                                <div class="row">
                                    <div class="col-12 col-md-2 text-md-right my-auto">
                                        <h5 class="mb-3">Product Bundle ..</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-2 text-md-right my-auto">
                                        <p>set <small class="text-danger">*</small></p>
                                    </div>

                                    <div class="col-12 col-md-10" id="bundledesignContainer">
                                        <div class="col-12 bundle-box default-item">
                                            <div class="row no-gutters p-2 mb-1" style="border: 1px solid #b3b3b3; border-radius: 5px;">

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="bundle_id[]" id="bundle_id" class="select2 form-control">
                                                        <option value="default">Select a bundle ID..</option>
                                                        <option value="1">Type 1</option>
                                                        <option value="2">Type 2</option>
                                                        <option value="3">Type 3</option>
                                                        <option value="4">Type 4</option>
                                                        <option value="5">Type 5</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <input type="number" id="bundle_qty"
                                                        name="bundle_qty[]" class="form-control" placeholder="bundle_qty">
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="bundle_type[]" class="select2 form-control" id="bundle_type">
                                                        <option value="default">Select a bundle type..</option>
                                                        <option value="0">Optional</option>
                                                        <option value="1">Non - Optional</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="product_attribute_id[]" id="product_attribute_id" class="select2 form-control">
                                                        <option value="default">Please choose a product code.</option>
                                                        @foreach ($products as $product)
                                                        <option value="{{ $product->id }}">{{$product->product_code}} - {{$product->name}} ({{$product->attribute_name}})</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <input type="text" name="bundle_title[]" class="form-control bundle_title-in" placeholder="Title Name">
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <select name="bundle_variation[]" class="select2 form-control">
                                                        <option value="default">Select a variation type..</option>
                                                        <option value="color">Color</option>
                                                        <option value="size">Size</option>
                                                        <option value="light-temperature">Light Temperature</option>
                                                        <option value="curtain-size">Curtain Size</option>
                                                        <option value="miscellanous">Miscellanous</option>
                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3 form-group p-1">
                                                    <input type="text" name="bundle_variation_selection[]" class="form-control" placeholder="Name">
                                                </div>

                                                <div class="ml-auto col-2 p-1">
                                                    <button type="button" class="btn btn-danger d-none" style="width: 100%;"
                                                        onclick="removeBundleDesignSetElement(this);">
                                                        <i class="fa fa-minus"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-success" style="width: 100%;"
                                                        onclick="addMoreBundleDesignSetElement();">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>

                                                <div class='error-message text-danger'></div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endif

                            <div class="row mt-2">
                                <div class="col-5 text-right ml-auto">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- End Form -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<style>
    .select2.select2-container {
        width: 100% !important;
    }
</style>
@endpush

@push('script')
<script>
    $("#normalDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', false);
    });
        $("#individualDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', false);
    });
    $("#groupDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', false);
    });
    $('.check input:checkbox').click(function() {
    $('.check input:checkbox').not(this).prop('checked', false);
    });

    function addMoreAvailableInElement() {
        let availableInContainer = $('#availableInContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('id')
        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.find('select.select2').removeClass('is-invalid')
        cloneItem.find('select.select2').removeClass('is-valid')
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '999999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }
    }

    function removeAvailableInElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function addMoreProductVariationElement() {
        let availableInContainer = $('#variationContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '99999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }
    }

    function addMoreBundleDesignSetElement() {
        let availableInContainer = $('#bundledesignContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();
        bundleDefaultAction(cloneItem);

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '99999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }
    }

    function removeBundleDesignSetElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function removeProductVariationElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function addDeliveryRangeElement() {
        let deliveryRangeContainer = $('#deliveryRangeContainer')

        let currentItem = deliveryRangeContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.newID').val('new');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(deliveryRangeContainer);
        cloneItem.slideDown();

        deliveryRangeContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '99999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }
    }

    function removeDeliveryRangeElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    $('.bundle-box').each(function(index, _selectGroup){
            bundleDefaultAction(_selectGroup);
        });

    function bundleDefaultAction(_selectGroup){

        $(_selectGroup).find('[name="bundle_id[]"]').change(function(){
            selectvalue = $(_selectGroup).find('[name="bundle_id[]"] option:selected').val();

            $(_selectGroup).data('group', selectvalue);
            $(_selectGroup).attr('data-group',selectvalue);

            saveBtn = document.querySelector('form #saveBtn');
            bundleAutoChange($(_selectGroup));
        });

        $(_selectGroup).find('[name="bundle_title[]"]').change(function(){
            bundleAutoChange($(_selectGroup));
        });

        $(_selectGroup).find('[name="bundle_qty[]"]').change(function(){
            bundleAutoChange($(_selectGroup));
        });

        $(_selectGroup).find('[name="bundle_type[]"]').change(function(){
            bundleAutoChange($(_selectGroup));
        });

    }

    function bundleAutoChange(_selectGroup){

        selectvalue = $(_selectGroup).data('group');
        bundle_title = $(_selectGroup).find('[name="bundle_title[]"]').val();
        bundle_qty = $(_selectGroup).find('[name="bundle_qty[]"]').val();
        bundle_type = $(_selectGroup).find('[name="bundle_type[]"]').val();

        bundle_title_error = true;
        bundle_qty_error = true;
        bundle_type_error = true;

        $('[data-group="'+selectvalue+'"]').each(function(index,_typeGroup){
            if ( $(_typeGroup).find('[name="bundle_title[]"]').val() != bundle_title) bundle_title_error = false;
            if ( $(_typeGroup).find('[name="bundle_qty[]"]').val() != bundle_qty) bundle_qty_error = false;
            if ( $(_typeGroup).find('[name="bundle_type[]"] option:selected').val() != bundle_type) bundle_type_error = false;
        });

        if(!bundle_title_error || !bundle_qty_error || !bundle_type_error){
            $(_selectGroup).find('.error-message').html('Type '+selectvalue+' : qty or choice or title different');
            $(saveBtn).attr("disabled", true);
        }else{
            $(_selectGroup).find('.error-message').html('');
            $(saveBtn).attr("disabled", false);
        }

    }


    $(document).ready(function() {

    let priceSelectors = document.getElementsByClassName('input-mask-price');

    let priceInputMask = new Inputmask({
        'mask': '99999.99',
        'numericInput': true,
        'digits': 2,
        'digitsOptional': false,
        'placeholder': '0'
    });

    for (var i = 0; i < priceSelectors.length; i++) {
        priceInputMask.mask(priceSelectors.item(i));
    }

    $('select.select2').select2({
        theme: 'bootstrap4',
    });

    $('.summernote').summernote({
        height: 200, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
    });

    // Variables Initialization
    // Input Into Variables.
    const panelAccountId = $('#panel_id');
    const variationType = $('#variation_type');
    const bundleType = $('#bundle_type');
    const bundleProduct = $('#product_attribute_id');

    const price = $('#price');
    const memberPrice = $('#member_price');
    const bpRequired = $('#bp_required');
    const productSv = $('#product_sv');
    const variationName = $('#product_variation_name');
    const bundleID = $('#bundle_id');
    const bundleqty = $('#bundle_qty');
    const shipFrom = $('#ships_from');
    const shipState = $('#ships_state');
    let delivery = $('.available-in');
    let bundleTitle = $('.bundle_title-in');
    console.log(delivery);

    panelAccountId.on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    variationType.on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    bundleID.on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    bundleType.on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    bundleProduct.on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    price.on('keyup', function() {
        if ($(this).val() == 0) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    memberPrice.on('keyup', function() {
        if ($(this).val() == 0) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    /*
    bpRequired.on('keyup', function() {
        if ($(this).val == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    */
    productSv.on('keyup', function() {
        if ($(this).val == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    variationName.on('keyup', function() {
        if ($(this).val == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    bundleqty.on('keyup', function() {
        if ($(this).val == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    shipFrom.on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    // shipState.on('change', function() {
    //     if ($(this).val() == 'default') {
    //         $(this).removeClass('is-valid');
    //         $(this).addClass('is-invalid');
    //     } else {
    //         $(this).removeClass('is-invalid');
    //         $(this).addClass('is-valid');
    //     }
    // });

    $('#create-panel-product-form').on('submit', function(event) {
        let errors = 0;

        // Validation
        if (panelAccountId.val() == 'default') {
            errors = errors + 1;
            panelAccountId.removeClass('is-valid');
            panelAccountId.addClass('is-invalid');
        }
        if (price.val() == 0) {
            errors = errors + 1;
            price.removeClass('is-valid');
            price.addClass('is-invalid');
        }
        if (memberPrice.val() == 0) {
            errors = errors + 1;
            memberPrice.removeClass('is-valid');
            memberPrice.addClass('is-invalid');
        }
        if (shipFrom.val() == 'default') {
            errors = errors + 1;
            shipFrom.removeClass('is-valid');
            shipFrom.addClass('is-invalid');
        }
        if (shipState.val() == 'default') {
            errors = errors + 1;
            shipState.removeClass('is-valid');
            shipState.addClass('is-invalid');
        }
        /*
        if (bpRequired.val() == '') {
            errors = errors + 1;
            bpRequired.removeClass('is-valid');
            bpRequired.addClass('is-invalid');
        }
        */
        if (productSv.val() == '') {
            errors = errors + 1;
            productSv.removeClass('is-valid');
            productSv.addClass('is-invalid');
        }
        // if ($('.productCategory :selected').val() == '0') {
        //         // if (productSv.val() == '') {
        //         errors = errors + 1;
        //         $('.productCategory').removeClass('is-valid');
        //         $('.productCategory').addClass('is-invalid');
        //     }

        variationName.each(function() {
        if (variationName.val() == '') {
            errors = errors + 1;
            variationName.removeClass('is-valid');
            variationName.addClass('is-invalid');
        }
        });
        bundleqty.each(function() {
        if (bundleqty.val() == '') {
            errors = errors + 1;
            bundleqty.removeClass('is-valid');
            bundleqty.addClass('is-invalid');
        }
        });
        bundleTitle.each(function() {
        if (bundleTitle.val() == '') {
            errors = errors + 1;
            bundleTitle.removeClass('is-valid');
            bundleTitle.addClass('is-invalid');
        }
        });
        // variationType = $('#variation_type');

        variationType.each(function() {
            if ($(this).val() == 'default') {
                errors = errors + 1;
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            }
        });
        bundleType.each(function() {
            if ($(this).val() == 'default') {
                errors = errors + 1;
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            }
        });
        bundleProduct.each(function() {
            if ($(this).val() == 'default') {
                errors = errors + 1;
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            }
        });
        bundleID.each(function() {
            if ($(this).val() == 'default') {
                errors = errors + 1;
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            }
        });

        if (errors > 0) {
            toastr.error('Please make sure the input highlighted in RED is correctly filled in.');
            return false;
        } else {
            return true;
        }
    });
});
</script>
@endpush

@push('style')
<style>
    .custom-control-input:checked~.custom-control-label::before {
        color: #fff;
        border-color: #ffcc00;
        background-color: #ffcc00;
    }
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
</style>
@endpush
