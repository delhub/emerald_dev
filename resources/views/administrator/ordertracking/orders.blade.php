@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">Pick (Orders)
                @if (getUser('warehouseLocationName') != '-')
                        -Location :
                        @foreach (getUser('warehouseLocationName') as $location)
                            <b>{{$location}}</b>@if (!$loop->last), @endif
                        @endforeach
                @endif
             </h1>
        </div>
    </div>

    {{-- select bulk action  --}}
    <form id="bulkactionform" action={{route('bulkupdate.ordertracking.detail') }} method="POST">
        <div class="row mt-2 ml-2">
            <div class="col-md-auto px-1">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="action_ids" id="action_ids" value="">

        <div class="input-group mb-1">
            <select name="bulkaction" id="bulkaction" class="col-12 text-left custom-select" @if (getUser('warehouseLocationName') != '-') disabled @endif>
                <option value="selection">Select action</option>
                {{-- <option value="mark_order_date"> Mark Date (Estimate Ship Out Date)</option> --}}
                <option value="mark_actual_ship_date"> Mark Date (Actual Ship Out Date)</option>
                <option value="mark_item_collected_date">Mark Date (Item Collected Date)</option>
                <option value="combine_do">Combine(Delivery order)</option>
            </select>

            <div class="col-md-auto px-1">
                <input name='action_date' id="action_date" class="estimatedateonly form-control" type="text"
                placeholder="Select Date" autocomplete="off">
            </div>

            <div class="input-group-append">
                <input type="submit" class="bjsh-btn-gradient" value="Submit" id="btnid" @if (getUser('warehouseLocationName') != '-') disabled @endif>
            </div>
        </div>
            </div>
        </div>
    </form>

    <form action="{{ route('administrator.ordertracking.orders') }}" method="GET">
        <div class="row mt-2 ml-2">
            <!-- Status -->
            <div class="col-md-auto px-1">
                <select name="status" class="col-12 text-left custom-select">
                    <option value="all">All Status</option>
                    @foreach ($statuses as $status)
                    <option value="{{ $status->id }}" {{ $request->status == $status->id ? 'selected' : ''}}>{{$status->name}}</option>
                    @endforeach
                </select>
            </div>

            <!-- States -->
            <div class="col-md-auto px-1">
                <select name="states" class="col-12 text-left custom-select">
                    <option value="all">All States</option>
                    @foreach ($states as $state)
                    <option value="{{ $state->id }}" {{ $request->states == $state->id ? 'selected' : ''}}>
                        {{ $state->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Stock Filter -->
            <div class="col-md-auto px-1">
                <select name="stock_filter" class="col-12 text-left custom-select">
                    <option value="all">All</option>
                    <option value="has_stock" {{ $request->stock_filter == 'has_stock' ? 'selected' : ''}}>Stock Ready</option>
                    <option value="no_stock" {{ $request->stock_filter == 'no_stock' ? 'selected' : ''}}>Stock No Ready</option>
                </select>
            </div>

             <!-- Date filter from date to date-->
             <div class="col-md-auto px-1">
                <div class="input-group mb-3">
                    <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                        aria-label="Username" aria-describedby="basic-addon1"
                        value="{{ ($request->datefrom) ? $request->datefrom : '' }}" autocomplete="off">
                </div>
            </div>

            <div class="col-md-auto px-1">
                <div class="input-group mb-3">
                    <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                        aria-label="Username" aria-describedby="basic-addon1"
                        value="{{ ($request->dateto) ? $request->dateto : '' }}" autocomplete="off">
                </div>
            </div>

            <div class="col-md-auto ml-auto">
                <div class="row">
                    <div class="col-md-8 p-0">
                        <select class="custom-select" id="selectSearch" name="selectSearch">
                            <option value="ALL" selected>Choose...</option>
                            <option value="BJN" {{ $request->selectSearch == 'BJN' ? 'selected' : '' }}>Order Number</option>
                            <option value="PODO" {{ $request->selectSearch == 'PODO' ? 'selected' : '' }}>DO Number</option>
                            {{-- <option value="NAME" {{ $request->selectSearch == 'NAME' ? 'selected' : '' }}>Product Name</option> --}}
                            {{-- <option value="CODE" {{ $request->selectSearch == 'CODE' ? 'selected' : '' }}>Product Code</option> --}}
                        </select>

                        <input type="text" class="form-control" id="" name="searchBox"
                            value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                    </div>
                    <div class="col-md-4 ">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                    </div>
                </div>
            </div>
        </div>

        <div style="text-align:center;">
            <div class="col-md-auto px-1">
                <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
                <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.ordertracking.orders') }} style="color:black;">Reset Filter</a></button>
                {{-- <button type="submit" class="btn btn-warning" style="color:black;"><a href="{{route('administrator.ordertracking.exportCsv', ['status'=> $request->status, 'states'=> $request->states, 'datefrom' => $request->datefrom, 'dateto' =>  $request->dateto, 'stock_filter' =>  $request->stock_filter, 'selectSearch' => $request->selectSearch , 'searchBox' => $request->searchBox])}}" id="export" style="color:black; ">Download</a></button> --}}
            </div>
        </div>

        <br>

        <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Orders) : <b>{{ $countTotal}}</b></span></p>
        <div class="row">
            <div class="col-auto ml-auto">
                {{ $customerOrders->links() }}
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-bordered text-center table-striped " id="sales-tracking" style="width:100%">
                    <thead style="background-color:	#E8E8E8;">
                        <tr>
                            <th scope="col"><input type="checkbox" class="largerCheckBox" id="checkAll"/>Action</th>
                            <th scope="col">Invoice No./ DO No.</th>
                            {{-- <th scope="col">Product Code</th>
                            <th scope="col">Product Name </th> --}}
                            <th scope="col">Order Date</th>
                            {{-- <th scope="col">Qty</th> --}}
                            <th scope="col">Pending Days</th>
                            <th scope="col">Order Status</th>
                            {{-- <th scope="col">Item Status</th> --}}
                            <th scope="col">States</th>
                            <th scope="col">Tracking</th>
                            {{-- <th scope="col">Estimate Ship Out Date</th> --}}
                            <th scope="col">Shipped Out Date / Ready For Collection Date</th>
                            <th scope="col">Order Recevied Date / Scan Date</th>
                            <th scope="col">Pick & Pack Status</th>
                            <th scope="col">Remark</th>
                            {{-- <th scope="col">Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customerOrders as $customerOrder)
                        @if ($customerOrder->est_shipping_date < now() && $customerOrder->shipping_date == 'Pending' && $customerOrder->collected_date == 'Pending' && $customerOrder->order_status != 1004)
                            <tr style="background-color:#ffb3b3">
                        @elseif (isset($customerOrder->tracking_number) && $customerOrder->order_status == 1001)
                            <tr style="background-color:#b3daff">
                        @elseif ($customerOrder->order_status == 1002 || $customerOrder->order_status == 1003)
                            <tr style="background-color:#DFEFCB">
                        @else
                            <tr>
                        @endif

                            <td>
                                <input type="checkbox" class="cb-element largerCheckBox" name="itemsid[]" value="{{ $customerOrder->order_number }}" />

                                @php $itemid = array(); foreach ($customerOrder->items  as $item) $itemid[] = $item->id; @endphp

                                <form action="{{ route('administrator.ordertracking.detail')}}">
                                    <input type="hidden" name="manyID" value="{{ implode("," , $itemid )}}">
                                    <button class="bjsh-btn-gradient" type="submit" formtarget="_blank">Detail</button>
                                </form>
                            </td>

                            <td>
                                <a target="_blank"
                                href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->purchase->getFormattedNumber().(($customerOrder->purchase->invoice_version != 0) ? '/v'.$customerOrder->purchase->invoice_version.'/' : '/').$customerOrder->purchase->getFormattedNumber().'.pdf')}}"
                                >{{$customerOrder->purchase->getFormattedNumber()}}
                                {{ $customerOrder->purchase->invoice_version }}
                                </a>
                                <hr>
                                <a target="_blank"
                                href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->purchase->getFormattedNumber().(($customerOrder->purchase->invoice_version != 0) ? '/v'.$customerOrder->purchase->invoice_version.'/' : '/').'delivery-orders/'.$customerOrder->delivery_order.'.pdf')}}"
                                >{{$customerOrder->delivery_order}}
                                </a>
                            </td>

                            {{-- <td>
                                {!! ($customerOrder->product_code) ? $customerOrder->product_code : '<center>-</center>' !!}
                            </td> --}}

                            {{-- <td> --}}
                                {{-- @if(is_object( $customerOrder->product))
                                {{ $customerOrder->product->parentProduct->name }}
                                @endif

                                @if(array_key_exists('product_color_name', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Color:
                                    {{ $customerOrder->product_information['product_color_name'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_size', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Size:
                                    {{ $customerOrder->product_information['product_size'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_curtain_size', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Curtain Model:
                                    {{ $customerOrder->product_information['product_curtain_size'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_miscellaneous', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">
                                    {{ $customerOrder->product_information['product_miscellaneous'] }}
                                </p>
                                @endif
                                @if(array_key_exists('invoice_number', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Invoice Number:
                                    {{ $customerOrder->product_information['invoice_number'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_temperature', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Color Temperature:
                                    {{ $customerOrder->product_information['product_temperature'] }}</p>
                                @endif
                                @if(array_key_exists('product_preorder_date', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Pre Order Delivery:
                                    {{ $customerOrder->product_information['product_preorder_date'] }}</p>
                                @endif
                                @if(array_key_exists('product_order_selfcollect', $customerOrder->product_information) && $customerOrder->product_information['product_order_selfcollect'])
                                <p class="text-capitalize text-secondary m-0">
                                    Self Collection: Yes
                                </p>
                                @endif
                                 <!-- Trade In -->
                                @if(array_key_exists('product_order_tradein', $customerOrder->product_information) &&
                                $customerOrder->product_information['product_order_tradein'])
                                <p class="text-capitalize text-secondary m-0">Rebate: - RM
                                {{ number_format(($customerOrder->product_information['product_order_tradein'] * $customerOrder->quantity)/100),2 }}
                                {{ $customerOrder->quantity > 1 ? '( Trade-in '. $customerOrder->quantity . ' sofa )' : ''}}
                                </p>
                                @endif
                                @if ($customerOrder->bundle_id != 0)
                                <p class="text-capitalize text-secondary m-0">In Bundle :
                                    ({{$customerOrder->getParentName($customerOrder->bundle_id)}})
                                </p>
                                @endif --}}
                            {{-- </td> --}}

                            <td>
                                {{-- {{ ($customerOrder->purchase->created_at->format('d/m/Y')) ? $customerOrder->purchase->created_at->format('d/m/Y') : '' }} --}}
                                {{ $customerOrder->order_date }}
                            </td>

                            {{-- <td>
                                {{$customerOrder->quantity }}
                            </td> --}}

                            <td>
                                {{$customerOrder->getPendingAttribute()}}
                            </td>

                            <td>
                                @foreach ($statuses as $status)
                                    @if ($status->id == $customerOrder->order_status)
                                        {{ $status->name }}
                                    @endif
                                @endforeach
                                &nbsp;
                                <br>
                                @if ($customerOrder->order_status = 1010 && isset($customerOrder->outlet))
                                    <p class="text-capitalize text-secondary m-0">{{$customerOrder->outlet->outlet_name}}</p>
                                @endif
                            </td>

                            {{-- <td>
                                @foreach ($statuses as $status)
                                @if ($status->id == $customerOrder->item_order_status)
                                {{ str_replace("Order","Item", $status->name ) }}
                                    @if ($status->id === 1004)
                                      <p class="text-capitalize text-secondary m-0">({{$customerOrder->purchase->status->name}})</p>
                                    @endif
                                @endif
                                @endforeach
                            </td> --}}

                            <td>
                                {{ $customerOrder->purchase->state->name }}
                            </td>

                            <td>
                                <form id="tracking{{ $customerOrder->id }}" action="{{ route('update.items.tracking',[$customerOrder->id]) }}"
                                    method="POST" class="my-md-1">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <select id="courier_name{{ $customerOrder->id }}" onchange="showButton({{ $customerOrder->id }})" name="courier_name" class="form-control" @if (getUser('warehouseLocationName') != '-') disabled @endif>
                                            <option value="all" selected>Courier Name</option>
                                            @foreach ($couriers as $courier)
                                            <option value="{{ $courier->courier_name }}"
                                                {{ $customerOrder->courier_name == $courier->courier_name ? 'selected' : ''}}>
                                                {{ $courier->courier_name }}</option>
                                            @endforeach
                                        </select>

                                        <input type="text" name='tracking_number' class="form-control-sm" value="{{$customerOrder->tracking_number}}"placeholder="Tracking Number" @if (getUser('warehouseLocationName') != '-') readonly @endif/>
                                            <br>
                                        <input type="submit" class="bjsh-btn-gradient ml-2" value="Save" @if (getUser('warehouseLocationName') != '-') disabled @endif>
                                </form>

                                <button type="submit" style="{{ $customerOrder->courier_name == 'POSLAJU' && $customerOrder->shipment_key == NULL ? '' : 'display: none;' }}" class="bjsh-btn-gradient ml-2" id="createShipment_{{ $customerOrder->id }}" value="Submit" onclick="getShipmentData({{ $customerOrder->id }})" title="Submit shipment to Poslaju" {{ $customerOrder->shipment_key ? 'disabled' : '' }}>{{ $customerOrder->shipment_key ? 'Shipment Create' : 'Poslaju' }}
                                    <div id="spinnerSubmit{{ $customerOrder->id }}" class="spinner-border spinner-border-sm text-dark d-none" role="status">
                                  </div>
                                </button>

                                <button type="submit" style="{{ $customerOrder->courier_name == 'POSLAJU' && $customerOrder->tracking_number == NULL && $customerOrder->shipment_key != NULL ? '' : 'display: none;' }}" class="bjsh-btn-gradient ml-2" id="checkout_{{ $customerOrder->id }}" value="Submit" onclick="PoslajuCheckout({{ $customerOrder->id }})" title="Checkout Poslaju" {{ $customerOrder->tracking_number ? 'disabled' : '' }}>{{ $customerOrder->tracking_number ? 'Checkout Now' : 'Checkout' }}
                                    <div id="checkoutSpinner{{ $customerOrder->id }}" class="spinner-border spinner-border-sm text-dark d-none" role="status" @if (getUser('warehouseLocationName') != '-') readonly @endif>
                                  </div>
                                </button>

                                <a onclick="PoslajuShip({{ $customerOrder->id }})" href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->purchase->getFormattedNumber().(($customerOrder->purchase->invoice_version != 0) ? '/v'.$customerOrder->purchase->invoice_version.'/' : '/').'delivery-orders/'.$customerOrder->delivery_order.'.pdf')}}" target="_blank" style="{{ ($customerOrder->courier_name == 'POSLAJU' && $customerOrder->tracking_number != NULL) ? '' : 'display: none;' }}"><button type="submit" class="bjsh-btn-gradient ml-2" ><i class="fa fa-cloud-download" aria-hidden="true">
                                    </i> DO + C_NOTE</button></a>
                                <form action="{{ route('update.reset.cancel.poslaju.consigment',[$customerOrder->id]) }}"
                                    method="POST"  style="{{ ($customerOrder->courier_name == 'POSLAJU' && $customerOrder->tracking_number != NULL) ? '' : 'display: none;' }}">

                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->id }}">

                                            <input type="hidden" name='reset_collected_date' value="1"/>
                                            <input type="submit" class="bjsh-btn-gradient ml-1" value="Cancel-Consigment">
                                    </div>
                                </form>
                            </td>


                            {{-- <td>
                                <form action="{{ route('update.ship.out.items',[$customerOrder->order_number]) }}"
                                    method="POST">
                                    <div class="row">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->order_number }}">

                                            <input name='ship_out_date' value="{{$customerOrder->est_shipping_date}}"
                                                class="estimatedateonly form-control mx-1" type="text" placeholder="Select delivery date"
                                                required autocomplete="off">
                                            <input type="submit" class="bjsh-btn-gradient ml-4" value="Sub">
                                </form>

                                <form action="{{ route('update.reset.ship.out.items',[$customerOrder->order_number]) }}"
                                        method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->order_number }}">

                                            <input type="hidden" name='reset_ship_out_date' value="1"/>
                                            <input type="submit" class="bjsh-btn-gradient ml-1" value="Res">
                                    </div>
                                </form>
                            </td> --}}

                            <td>
                                {{-- actual submit --}}
                                <form action="{{ route('update.ship.out.items',[$customerOrder->order_number]) }}"
                                    method="POST">
                                    <div class="row">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->order_number }}">

                                            <input name='actual_ship_out_date' value="{{$customerOrder->shipping_date}}"
                                                class="date form-control mx-1" type="text" placeholder="Select delivery date"
                                                required autocomplete="off" @if (getUser('warehouseLocationName') != '-') readonly @endif>
                                            <input type="submit" class="bjsh-btn-gradient ml-4" value="Sub" @if (getUser('warehouseLocationName') != '-') disabled @endif>
                                </form>

                                {{-- actual reset --}}
                                <form action="{{ route('update.reset.ship.out.items',[$customerOrder->order_number]) }}"
                                    method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->order_number }}">

                                            <input type="hidden" name='reset_actual_ship_out_date' value="1"/>
                                            <input type="submit" class="bjsh-btn-gradient ml-1" value="Res"@if (getUser('warehouseLocationName') != '-') disabled @endif>
                                    </div>
                                </form>
                            </td>

                            <td>
                                {{-- collected submit --}}
                                <form action="{{ route('update.ship.out.items',[$customerOrder->order_number]) }}"
                                    method="POST">
                                    <div class="row">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->order_number }}">

                                            <input name='collected_date' value="{{$customerOrder->collected_date}}"
                                                class="filterDate form-control mx-1" type="text" placeholder="Select delivery date"
                                                required autocomplete="off" @if (getUser('warehouseLocationName') != '-') readonly @endif>
                                            <input type="submit" class="bjsh-btn-gradient ml-4" value="Sub"@if (getUser('warehouseLocationName') != '-') disabled @endif>
                                </form>

                                {{-- collected reset --}}
                                <form action="{{ route('update.reset.ship.out.items',[$customerOrder->order_number]) }}"
                                    method="POST">

                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="lineItem_id" value="{{ $customerOrder->order_number }}">

                                            <input type="hidden" name='reset_collected_date' value="1"/>
                                            <input type="submit" class="bjsh-btn-gradient ml-1" value="Res"@if (getUser('warehouseLocationName') != '-') disabled @endif>
                                    </div>
                                </form>
                                /{{($customerOrder->received_date) ? $customerOrder->received_date : 'Not Scan'}}
                            </td>

                            <td>
                                {{showPickPackStatus($customerOrder->pick_pack_status)}}
                                <br>
                                {{showPickPackBatch($customerOrder->delivery_order)}}

                            </td>

                            <td>
                                {{-- remark --}}
                                {{-- @if(array_key_exists('product_order_remark', $customerOrder->product_information))
                                <p class="text-capitalize">User Remarks:
                                    {{ $customerOrder->product_information['product_order_remark'] }}</p>
                                @endif --}}
                                <form action="{{ route('update.items.adminremark',[$customerOrder->order_number]) }}"
                                    method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <textarea name='admin_remarks' class="form-control" value="{{$customerOrder->admin_remarks}}" placeholder="Admin remark" @if (getUser('warehouseLocationName') != '-') readonly @endif>{{$customerOrder->admin_remarks}}</textarea>

                                <input type="submit" class="bjsh-btn-gradient ml-2" value="Submit" @if (getUser('warehouseLocationName') != '-') disabled @endif>
                                </form>
                            </td>

                            {{-- <td>

                            </td> --}}

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-auto ml-auto py-3">
            {{ $customerOrders->links() }}
        </div>

    </div>

</div>

@push('style')
    <style>
        .text-bold {
            font-weight: bold;
        }

        .table .thead-light th {
            font-weight: bold;
            font-family: 'Nunito', sans-serif;
        }

        .largerCheckBox{
            width: 18px;
            height: 18px;
        }

        @media(max-width:767px) {
            .hidden-sm {
                display: none;
            }

        }

        @media(min-width:767px) {
            .hidden-md {
                display: none;
            }
        }


        .text-font-family {
            font-family: 'Nunito', sans-serif;
        }

        /* freeze header  */
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

            table  { border-collapse: collapse; width: 100%; }
            th     { background:#eee; }

    </style>
@endpush

@push('script')
    <script>
        $(function() {
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
                });

                $(".estimatedateonly").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                changeMonth: true,
                changeYear: true
                });

                $(".filterDate").datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                changeMonth: true,
                changeYear: true
            });
        });

        function showButton(itemID) {

            var opt = document.getElementById("courier_name" + itemID).value;
            console.log('opt');
            console.log(opt);
            if(opt == 'POSLAJU'){
                $("#createShipment_" +itemID).show();
            }else{
                $("#createShipment_" +itemID).hide();
            }
        }

        $(document).ready(function () {

            document.getElementById("action_date").style.display = "none";

            //check all checkbox
            $("#checkAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Get checked id
            $("#bulkactionform").submit(function(){
                var checked = new Array();
                var ids = new Array();

                checked = $('input[name="itemsid[]"]:checked');

                for (i = 0; i < checked.length; i++) {
                    ids.push(checked[i].value);
                    }

                var stringids = ids.toString();
                $("#action_ids").val(stringids);
            });

            //show or hide when selected bulk action
            $("#bulkaction").click(function(){

                if(bulkaction.value == 'combine_do' || bulkaction.value == 'splitPOnDO' || bulkaction.value == 'selection'){

                document.getElementById("action_date").style.display = "none";
                }else if(bulkaction.value == 'mark_order_date'){

                    document.getElementById("action_date").className = "estimatedateonly form-control";

                    $(".estimatedateonly").datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else if(bulkaction.value == 'mark_actual_ship_date'){

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else{

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }
            });
        });

        function getShipmentData(itemID) {
            console.log(itemID);
            var element = document.getElementById("spinnerSubmit" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // disabled button after one click
                    $('#createShipment_'+itemID).attr('disabled',true);
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "/administrator/courier/create_shipment/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("createShipment_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Shipment Create";
                        $("#tracking"+itemID).submit();

                    } else {
                        element.classList.add("d-none");
                        alert(result);
                        $('#createShipment_'+itemID).attr('disabled',false);
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuCheckout(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "/administrator/courier/poslaju_checkout/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("checkout_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Checkout";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuShip(itemID) {
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "/administrator/courier/poslaju_ship/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        // button.disabled = true;
                        // button.innerHTML = "Checkout";
                        // $("#actualSubmit").submit();
                    } else {
                        // element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function exportTasks(_this) {
            let _url = $(_this).data('href');
            window.location.href = _url;
        }

    </script>
@endpush

@endsection
