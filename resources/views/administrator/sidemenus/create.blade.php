
@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/sidemenus" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Created Menu
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.sidemenus.store') }}" id="edit-menu-form" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu Name <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="menu_title" name="menu_title" class="form-control" value="{{ old('menu_title') }}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu Group <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="menu_group" id="menu_group" class="select2 form-control">
                                    <option value="0"  @if (old('menu_group') == '0') selected="selected" @endif>Select menu group..</option>
                                    <option value="MM" @if (old('menu_group') == 'MM') selected="selected" @endif>Main Menu</option>
                                    <option value="AM" @if (old('menu_group') == 'AM') selected="selected" @endif>Agent Menu</option>
                                    <option value="PM" @if (old('menu_group') == 'PM') selected="selected" @endif>Panel Menu</option>
                                    <option value="UM" @if (old('menu_group') == 'UM') selected="selected" @endif>User Menu</option>
                                    <option value="CG" @if (old('menu_group') == 'CG') selected="selected" @endif>Categories Menu</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Country Id <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="country_id" id="country_id" class="select2 form-control">
                                    <option value="MY" @if (old('country_id') == 'MY') selected="selected" @endif>MY</option>
                                    <option value="SG" @if (old('country_id') == 'SG') selected="selected" @endif>SG</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu Parent 
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                
                                <select name="menu_parent" id="menu_parent" class="select2 form-control">
                                    @if(count($sidemenus) > 0)
                                    <option value="0">No parent..</option>
                                    @foreach($sidemenus as $sidemenu)
                                    
                                    <option value="{{$sidemenu->id}}" {!! old('menu_parent') == $sidemenu->id ? 'selected="selected"' : '' !!}>{{$sidemenu->menu_title}}</option>
                                    @endforeach
                                    @else
                                    <option value="0">No parent..</option>
                                    @endif
                                </select>
                                
                                <small>if Main Menu please select: No parent</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu Link Page <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="menu_link" name="menu_link" class="form-control" placeholder="#" value="{{ old('menu_link') }}">
                                <small>if no link please fill: #</small>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu icon image <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                            <img id="menu_icon" src="#" alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                                <input type="file" name="menu_icon" id="menu_icon" class="form-control-file" onchange="document.getElementById('menu_icon').src = window.URL.createObjectURL(this.files[0])">
                                <small>Please make sure image is in 1:1 display ratio (square).</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu Arrangement <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="menu_arrangement" name="menu_arrangement" class="form-control" placeholder="arrangement number..." value="{{old('menu_arrangement')}}">
                                <small>if random please fill: 0</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Menu Status
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="menu_show" value="1" id="menu_show" checked>
                                    <label class="custom-control-label" for="menu_show">Publish after saving.</label>
                                    <br>
                                    <small>Uncheck if you want the product to not be published after saving.</small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection


