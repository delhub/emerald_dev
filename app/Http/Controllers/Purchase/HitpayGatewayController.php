<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use App\Models\Users\User;

class HitpayGatewayController extends Controller
{
    public function submitForm($request, $purchase,$user){

        //
        $unique_id =  uniqid();

        $payment = new Payment;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->purchase_uuid = $purchase->purchase_number .'-'. $unique_id;
        $payment->gateway_response_code = 'PENDING';
        $payment->amount = ($request->total_price) ? str_replace(',','',$request->total_price)*100 : $purchase->purchase_amount;
        $payment->gateway_string_result = '-';
        $payment->auth_code = '-';
        $payment->last_4_card_number = '-';
        $payment->expiry_date = '-';
        $payment->gateway_eci = '-';
        $payment->gateway_security_key_res = '-';
        $payment->gateway_hash = '-';
        $payment->country_id = $purchase->country_id;

        // return $payment;

        // dd($purchase->purchase_amount,$payment->amount);

        $body = array(
            'email' => $user->email,
            'redirect_url' => route('payment.success',[$purchase->purchase_number]),
            'webhook' => url('/payment/gateway-response?process=hitpay'),
            'currency' => 'SGD',
            'amount' => number_format($payment->amount / 100, 2, '.', ''),
            'purpose' => 'Bujishu Order #'.$purchase->getFormattedNumber(),
            'reference_number' => $payment->purchase_uuid
        );
        $headers = array(
            'X-BUSINESS-API-KEY' => env('HITPAY_KEY'),
            'Content-Type' => 'application/x-www-form-urlencoded',
            'X-Requested-With' => 'XMLHttpRequest'
        );

        $client = new Client();
        $response = $client->post(
            env('HITPAY_BASE_URL').'payment-requests',
            ['form_params' => $body, 'headers' => $headers]
        );
        $responseStatus = $response->getStatusCode();
        $request = json_decode($response->getBody(), true);

        // return $request;

        $purchase = Purchase::where('purchase_number', $purchase->purchase_number)->first();
        $purchase->purchase_status = 3000;
        $purchase->save();

        $payment->gateway_string_result = $request['id'];
        $payment->save();


        // return $request;

        return Redirect::to($request['url']);


    }
    public function gatewayResponse($request)
    {
        Log::channel('hitpay')->info($request);

        $paymentGatewayResponse = new PaymentGatewayController;

        $payment = Payment::where('gateway_string_result', $request['payment_request_id'])->first();

        $purchase_number = explode('-',$request['reference_number']);
        $purchase = Purchase::where('purchase_number',$purchase_number[0])->first();

        $valuepair = $request->toArray();
        unset($valuepair['hmac']);
        unset($valuepair['process']);

        // Hash Checking
        $generatedHMAC = $this->generateSignatureArray(env('HITPAY_SALT'), $valuepair);

        // check hash
        if (($request['hmac'] != $generatedHMAC) || (!isset($payment)) || (isset($payment) && in_array($payment->gateway_response_code, ['COMPLETE', 'FAILED']))) {

            $purchase->purchase_status = '4006';
            $purchase->save();

            $payment->gateway_response_code = 'FAILED';
            $payment->save();

            $statusOrders = $purchase->orders;
            $statusItems = $purchase->items;

            foreach ($statusOrders as $order) {
                $order->order_status = '1004';
            }
            foreach ($statusItems as $item) {
                $item->item_order_status = '1004';
            }

            return 'failed';
        }

        $this->statusList($valuepair['status'], $purchase);

        $user = User::find($purchase->user_id);

        $paymentGatewayResponse->updateGroupSales($user, $purchase);

        $check_registration = true;

        $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration , false, false);

        GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);

        $gateway = 'Hitpay';
        $transaction_id = $request['payment_request_id'];

        $paymentGatewayResponse->sendToMarketingServer($user->userInfo->account_id, $purchase->getFormattedNumber(),  $purchase, $purchase->purchase_amount / 100, $gateway, $transaction_id);

        return $paymentGatewayResponse->markOrderComplete($purchase, $user);
    }

    public static function generateSignatureArray($secret, array $args)
    {
        $hmacSource = [];

        foreach ($args as $key => $val) {
            $hmacSource[$key] = "{$key}{$val}";
        }

        // return  $hmacSource;
        ksort($hmacSource);

        $sig            = implode("", array_values($hmacSource));
        $calculatedHmac = hash_hmac('sha256', $sig, $secret);
        return $calculatedHmac;
    }

    // get second request
    public function paymentRequest($id){
        //
        $purchase = Purchase::where('purchase_number',$id)->first();
        $payment = $purchase->payments->first() ?? null;
        if (isset($payment) && $payment) {
            $headers = array(
                'X-BUSINESS-API-KEY' => env('HITPAY_KEY'),
                'Content-Type' => 'application/x-www-form-urlencoded',
                'X-Requested-With' => 'XMLHttpRequest'
            );

            $client = new Client();
            $response = $client->get(
                env('HITPAY_BASE_URL').'payment-requests/'.$payment->gateway_string_result,
                ['headers' => $headers]
            );
            $responseStatus = $response->getStatusCode();
            $request = json_decode($response->getBody(), true);

            $this->statusList($request['status'],$purchase);
        }
        return redirect()->back();
    }

    public function statusList ($hitpayStatus,$purchase){

        $payment = $purchase->payments->first() ?? null;

        if ($payment) {
            if ($hitpayStatus == 'completed') {
                $payment->gateway_response_code = '00';
                $payment->save();

                $purchase->purchase_status = '4002';
                $purchase->save();

                $statusOrders = $purchase->orders;
                $statusItems = $purchase->items;

                foreach ($statusOrders as $order) {
                    $order->order_status = '1001';
                }
                foreach ($statusItems as $item) {
                    $item->item_order_status = '1001';
                }

            }
            if (in_array($hitpayStatus, ['expired','failed','canceled'])) {

                $payment->gateway_response_code = strtoupper($hitpayStatus);
                $payment->save();

                $purchase->purchase_status = '4006';
                $purchase->save();

                $statusOrders = $purchase->orders;
                $statusItems = $purchase->items;

                foreach ($statusOrders as $order) {
                    $order->order_status = 1004;
                }
                foreach ($statusItems as $item) {
                    $item->item_order_status = 1004;
                }
            }
        }
    }
}
