<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserPoints extends Model
{
    // Set table
    protected $table = 'user_points';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    /**
     * Get the user address associated with the user.
     */

    public function credit()
    {
        //
    }

    /**
     * Get state.
     */
    public function debit()
    {
        //
    }

    /**
     * Get city.
     */
    public function balance()
    {
        //
    }
}
