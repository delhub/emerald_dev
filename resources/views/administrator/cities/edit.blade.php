@extends('layouts.administrator.main')

@section('content')

    <div class="row">
        <div class="col-12">
            {{-- href="/administrator/faq" --}}
            <a href="{{ route('administrator.cities.index') }}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>

    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">

                    <h4>
                        Edit Cities
                    </h4>
                    {{-- @include('inc.messages') --}}
                </div>

                <div class="col-12">
                    <form action="{{ route('administrator.cities.update', ['id' => $city->id]) }}"
                        id="edit-global-cities-form" method="POST" enctype="multipart/form-data">

                        @csrf
                        @method('POST')


                        <div class="row">

                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Country
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                <input type="text" id="country_id" name="country_id" class="form-control" value="{{ $city->country_id }}" pattern="[A-Z]{2}" title="Two capital letter country code">

                                {{-- <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div> --}}

                                <small class="text-danger">{{ $errors->first('country_id') }}</small>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    State
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group">
                                {{-- <input type="text" id="state_id" name="state_id" class="form-control" value="{{ $city->state_id }}"> --}}
                                {{-- @if(count($states) > 0) --}}


                                <select name="state_id" id="state_id" class="select2 form-control form-group{{ $errors->has('state_id') ? ' has-error' : '' }}" style="width: 100%;" onchange="key_state()">

                                    @foreach($states as $state)

                                    <option value="{{ $state->id }}" data-key="{{ $state->abbreviation }}" {{ $state->id == $city->state_id ? 'selected' : '' }}>{{ $state->name }}</option>

                                    @endforeach

                                </select>

                                {{-- <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div> --}}

                                <small class="text-danger">{{ $errors->first('state_id') }}</small>

                                {{-- @endif --}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City Key
                                </p>
                            </div>
                            <div class="col-12 col-md-3 form-group{{ $errors->has('city_key') ? ' has-error' : '' }}">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="cityKey_state">{{ $states->where('id',$city->state_id)->first()->abbreviation }}_</span>
                                    </div>
                                    <input type="text" name="cityKey_city" id="cityKey_city" class="form-control" maxlength="3" style="text-transform:uppercase" onkeyup="combineKey()" value="{{ substr($city->city_key, -3) }}">
                                </div>
                                <input type="hidden" name="city_key" id="key" class="form-control" value="{{ $city->city_key }}" maxlength="3" style="text-transform:uppercase">
                                <small class="text-danger">{{ $errors->first('key') }}</small>
                                <small class="text-danger">{{ $errors->first('city_key') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City Name
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" id="name" name="city_name" class="form-control"  value="{{ $city->city_name }}" oninput="myFunction()">

                                {{-- <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div> --}}
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City Data
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                                <input type="text" id="data" name="city_data" class="form-control" value="{{ $city->city_data }}">

                                {{-- <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div> --}}
                                <small class="text-danger">{{ $errors->first('data') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection

@push('script')

<script>
// function myFunction() {
//   var x = document.getElementById("name").value;
//   document.getElementById("data").value =  x;
// }
$( document ).ready(function() {
    // key_state();
});

function key_state() {
var d = $('#state_id').data('key');
var state_abbv = $("#state_id option:selected").attr('data-key');
$('#cityKey_state').html(state_abbv + '_');
combineKey();
}

function combineKey() {
var state = document.getElementById("cityKey_state").innerHTML;
var city = document.getElementById("cityKey_city").value;
var key = state + city;
console.log(city);
document.getElementById("key").value = key;
}

</script>

@endpush
