<?php

namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Dealers\DealerGroup;
use App\Models\Purchases\Purchase;
use Carbon\Carbon;
use App\Models\Warranties\Warranty;

use App\Models\Users\UserInfo;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Warranty\Warranty as WarrantyWarranty;
use Illuminate\Database\Eloquent\Model;

class GuestController extends Controller
{
    /**
     * Return landing page.
     */
    public function index()
    {

        $countryObj = country();
        $country = $countryObj->country_id;

        return view('guests.landing-page', compact('country'));
    }

    /**
     * Return launcing page.
     */
    public function bujishuLaunch(Request $request)
    {
        if ($request->query('date')) {
            $queryDate = $request->query('date');

            $filterDate = date('Y-m-d', strtotime($queryDate));
        } else {
            $filterDate = date('Y-m-d');
        }

        $agentRegistrationSum = DealerInfo::where('created_at', '>=', $filterDate)
            ->get()
            ->count();

        $customerRegistrationSum = UserInfo::where('created_at', '>=', $filterDate)
            ->get()
            ->count();

        $latestThreeAgent = DealerInfo::where('created_at', '>=', $filterDate)
            ->orderBy('created_at', 'DESC')
            ->get()
            ->take(10);

        $totalSales = Purchase::where('purchase_status', 3003)
            ->where('created_at', '>=', $filterDate)
            ->get()
            ->sum('purchase_amount');

        $totalSales = number_format(($totalSales / 100), 2);

        $topThreeSales = DealerGroup::where('id', '!=', 12)->orderBy('sales_amount', 'DESC')->get();

        return view('guests.bujishu-launch')
            ->with('agentRegistrationSum', $agentRegistrationSum)
            ->with('customerRegistrationSum', $customerRegistrationSum)
            ->with('latestThreeAgent', $latestThreeAgent)
            ->with('totalSales', $totalSales)
            ->with('topThreeSales', $topThreeSales);
    }

    public function accountError()
    {
        return view('auth.account-error')->with('country', country());
    }

    public function warrantyForm()
    {
        return view('guests.warranty.bed-mattress');
    }

    public function printLabel($data) {

        dd($data);

        /* $collection = collect($request)->forget(['_token','submit'])->toArray();

        $detail = base64_encode(\json_encode($collection));

        return view('documents.qrcode-label')
            ->with('detail',$detail)
            ->with('request',$request)
            ->with('products',$request->product_code)
            ->with('quantity',$request->quantity)
            ->with('pageTitle',$request->batch); */
    }

    public function warantySubmitted(Request $request)
    {
        $newWarranty = new Warranty;
        $newWarranty->full_name = $request->input('name');
        $newWarranty->delivery_order = $request->input('delivery');
        $newWarranty->contact_num = $request->input('contact');
        $newWarranty->purchase_number = $request->input('invoice');
        $newWarranty->code = $request->input('code');
        // $newWarranty->product_id = '1';
        $newWarranty->product_information = $request->input('product');

             if ($request->input('product') =='Royal Series 1 - King'){
                $newWarranty->product_id = '190';

            } else if ($request->input('product') =='Royal Series 1 - Queen'){
                $newWarranty->product_id = '191';

            } else if ($request->input('product') =='Royal Series 1 - Super Single'){
                $newWarranty->product_id = '192';

            } else if ($request->input('product') =='Wealth Series 1 - King'){
                $newWarranty->product_id = '198';

            } else if ($request->input('product') =='Wealth Series 1 - Queen'){
                $newWarranty->product_id = '199';

            } else if ($request->input('product') =='Wealth Series 1 - Super Single'){
                $newWarranty->product_id = '200';

            }




        //  function get_product_id(Request $request){

        //     $product_idd = 0;

        //     if ($request->input('product') =='Royal Series 1 - King'){
        //         $product_idd = 1;

        //     } else if ($request->input('product') =='Royal Series 1 - Queen'){
        //         $product_idd = 2;

        //     } else if ($request->input('product') =='Royal Series 1 - Super Single'){
        //         $product_idd = 3;

        //     } else if ($request->input('product') =='Wealth Series 1 - King'){
        //         $product_idd = 4;

        //     } else if ($request->input('product') =='Wealth Series 1 - Queen'){
        //         $product_idd = 5;

        //     } else if ($request->input('product') =='Wealth Series 1 - Super Single'){
        //         $product_idd = 6;

        //     }

        //     return $product_idd;
        // }

        $newWarranty->save();

        return view('guests.warranty.warranty-submit');
    }


}
