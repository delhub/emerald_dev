@inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController')

@extends('layouts.administrator.main')

@section('content')

<h1>{{$controller::$header}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div> --}}
        </div>
        <div class="table-responsive m-2">
            @if(count($tables) > 0)
                <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID</th>
                            <th>Batch Number</th>
                            <th>Bin Number</th>
                            <th>Batch Status</th>
                            <th>Picker</th>
                            <th>Sorter</th>
                            <th>Packer</th>
                            <th>Pick Itemize PDF</th>
                            <th>Sort PDF</th>
                            <th>Order Type</th>
                            <th>Created At</th>
                            <th>Last Change At</th>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach($tables as $row)

                        {{-- @php
                            $url = URL::signedRoute(
                                'administrator.pickBin.qr-code',
                                ['bin_number' => $row->bin_number]
                            );
                        @endphp --}}


                        <tr>
                            <td>{{$loop->iteration }}</td>
                            <td>{{$row->id }}</td>
                            <td>{{$row->batch_number }}</td>
                            <td>{{$row->bin->bin_number }}</td>
                            <td>{{showPickPackStatus($row->pick_pack_status) }}</td>
                            <td>{{$row->pick_user->userInfo->full_name }}</td>
                            <td>{{!empty($row->sort_user) ? $row->sort_user->userInfo->full_name : '' }}</td>
                            <td>{{!empty($row->pack_user) ? $row->pack_user->userInfo->full_name : '' }}</td>
                            <td>
                                <a target="_blank"
                                    href="{{ URL::asset('storage/documents/pick/' . $row->batch_number . '/' . $row->id . '.pdf') }}"
                                    >{{ $row->batch_number }}</a>
                            </td>
                            <td>
                                @if ($row->pick_pack_status>= 3)
                                    <a target="_blank" method="POST" href="{{route('administrator.pickBatch.regenerate-combined-sort',['batchId' =>$row->id])}}">Sorted Pdf</a>

                                    @php $itemid = array();
                                        foreach ($row->batchDetail  as $detail){
                                            foreach ($detail->items as $key => $item) {
                                                $itemid[] = $item->id;
                                            }
                                        }
                                    @endphp

                                    <form action="{{ route('administrator.ordertracking.orders')}}">
                                        <input type="hidden" name="manyID" value="{{ implode("," , $itemid )}}">
                                        <button class="btn btn-primary" type="submit" formtarget="_blank">Order Detail</button>
                                    </form>
                                @endif


                            </td>
                            <td>
                                @if (isset($row->batchDetail[0]->items[0]->order) && $row->batchDetail[0]->items[0]->order->delivery_method == 'Self-pickup')
                                    {{$row->batchDetail[0]->items[0]->order->delivery_method}}
                                @else
                                    Delivery By Company
                                @endif
                            </td>
                            <td> {{date( 'd/m/Y H:i:s',strtotime($row->created_at))}}</td>
                            <td> {{date( 'd/m/Y H:i:s',strtotime($row->updated_at))}}</td>

                            {{-- <td>
                                <img src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($url)) }} ">
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$tables->links()}}
            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p>
            @endif
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    });
</script>
@endpush
