<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('point_type');
            $table->integer('amount');
            $table->string('transaction_type');
            $table->string('status');
            $table->integer('balance');
            $table->integer('used');
            $table->string('details')->nullable();
            $table->string('purchase_id')->nullable();
            $table->string('ref_id')->nullable();
            $table->string('created_by')->nullable();
            $table->string('deleted')->nullable();
            $table->string('expiry_at')->nullable();
            $table->string('approved_at')->nullable();
            $table->string('created_at');
            $table->string('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_points');
    }
}
