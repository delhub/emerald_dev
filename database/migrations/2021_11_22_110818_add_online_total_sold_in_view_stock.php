<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnlineTotalSoldInViewStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock
        AS
        select `wh_inventories`.`product_code` AS `product_code`,`wh_inventories`.`outlet_id` AS `outlet_id`,
        sum((case when ((`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8010') or  (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014') or (`wh_inventories`.`inv_type` = '8017') or (`wh_inventories`.`inv_type` = '8018') or (`wh_inventories`.`inv_type` = '8019') or (`wh_inventories`.`inv_type` = '8020')) then `wh_inventories`.`inv_amount` else 0 end)) AS `physical_stock`,
        sum((case when (`wh_inventories`.`inv_type` = '8001') then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock_reserved`,
        sum((case when (`wh_inventories`.`inv_type` = '8011') then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock_reserved_offline`,
        sum((case when (`wh_inventories`.`inv_type` = '8012') then `wh_inventories`.`inv_amount` else 0 end)) AS `pos_completed`,
        sum((case when (`wh_inventories`.`inv_type` = '8013') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockTransferIn`,
        sum((case when (`wh_inventories`.`inv_type` = '8014') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockTransferOut`,
        sum((case when (`wh_inventories`.`inv_type` = '8017') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_adjustOut`,
        sum((case when (`wh_inventories`.`inv_type` = '8018') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_adjustIn`,
        sum((case when (`wh_inventories`.`inv_type` = '8019') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_b_bundle_add`,
        sum((case when (`wh_inventories`.`inv_type` = '8020') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_b_bundle_minus`,
        sum((case when (`wh_inventories`.`inv_type` = '8000') then `wh_inventories`.`inv_amount` else 0 end)) AS `online_sold`,
        sum((case when (`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8012') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_sold`,
        sum((case when ( (`wh_inventories`.`inv_type` = '8001') or (`wh_inventories`.`inv_type` = '8011') or (`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8010') or (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014') or (`wh_inventories`.`inv_type` = '8017') or (`wh_inventories`.`inv_type` = '8018') or (`wh_inventories`.`inv_type` = '8019') or (`wh_inventories`.`inv_type` = '8020')) then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock`
        from `wh_inventories`
        group by `wh_inventories`.`product_code`,`wh_inventories`.`outlet_id`
        order by `wh_inventories`.`outlet_id`
        ");

        Schema::table('view_stock', function (Blueprint $table) {
            //


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('view_stock', function (Blueprint $table) {
            //
        });
    }
}
