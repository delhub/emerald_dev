<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateDealerPrebuyRedemptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_prebuy_redemption', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('dealer_id')->nullable();
            $table->string('channel');
            $table->integer('prebuy_status');
            $table->integer('total_amount')->default(0);
            $table->json('redemption_data')->nullable();
            $table->timestamps();
        });

        Schema::table('dealer_stockledger', function (Blueprint $table) {
            $table->bigInteger('prebuy_redemption_id')->default(0)->after('product_code');
            $table->bigInteger('unit_price')->default(0)->after('quantity');
            $table->Integer('subtotal_price')->default(0)->after('unit_price');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->tinyInteger('prebuy_redemption')->default(0)->after('order_type');
        });

        DB::table('global_statuses')->insert(['id' => '5006', 'name' => 'Purchase - Redeemed', 'description' => 'stockledger_status']);
        DB::table('global_statuses')->insert(['id' => '5007', 'name' => 'Purchase - Approved', 'description' => 'stockledger_status']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_prebuy_redemption');

        Schema::table('dealer_stockledger', function (Blueprint $table) {
            $table->removeColumn('prebuy_redemption_id');
            $table->removeColumn('unit_price');
            $table->removeColumn('subtotal_price');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->removeColumn('prebuy_redemption');
        });
    }
}
