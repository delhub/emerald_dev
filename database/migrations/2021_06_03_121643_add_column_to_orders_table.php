<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('delivery_method',32)->nullable()->after('order_amount');
            $table->string('delivery_outlet',32)->default(0)->after('delivery_method');
            $table->text('delivery_info')->nullable()->after('delivery_outlet');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery_method');
            $table->dropColumn('delivery_outlet');
            $table->dropColumn('delivery_info');
        });
    }
}
