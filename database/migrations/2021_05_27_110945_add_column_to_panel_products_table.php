<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPanelProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_products', function (Blueprint $table) {
            $table->renameColumn('product_material','product_content_1');
            $table->renameColumn('product_consistency','product_content_2');
            $table->renameColumn('product_package','product_content_3');
        });

        Schema::table('panel_products', function (Blueprint $table) {
            $table->text('short_description')->nullable()->after('display_panel_name');
            $table->string('place_of_origin',64)->nullable()->after('product_description');            
            $table->text('product_content_4')->nullable()->after('product_content_3');
            $table->text('product_content_5')->nullable()->after('product_content_4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_products', function (Blueprint $table) {
            $table->dropColumn('short_description');
            $table->dropColumn('place_of_origin');
            $table->dropColumn('product_content_4');
            $table->dropColumn('product_content_5');
            $table->renameColumn('product_content_1','product_material');
            $table->renameColumn('product_content_2','product_consistency');
            $table->renameColumn('product_content_3','product_package');
        });
    }
}
