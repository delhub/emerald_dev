<?php

namespace App\Models\Purchases;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Purchase extends Model
{
    // Set table
    protected $table = 'purchases';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    protected $casts = [
        'rbs_data' => 'array',
        'purchases_notes' => 'array'
    ];

    /**
     * Get all the orders belonging to the purchase.
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Purchases\Order', 'purchase_id');
    }

    public function items()
    {

        return $this->hasManyThrough('App\Models\Purchases\Item', 'App\Models\Purchases\Order', 'purchase_id', 'order_number');
    }

    public function sortItems()
    {
        $items =  $this->items->sortBy('bundle_id');

        $sorted = array();

        foreach ($items as $item) {
            if ($item->bundle_id == 0) {
                strval($item->id);
                $sorted[strval($item->id)] = $item;
            } else {

                $keys = array_keys($sorted);
                $position = array_search($item->bundle_id, $keys);

                $sorted = array_slice($sorted, 0, $position, true) +
                    array($item->id =>  $item) +
                    array_slice($sorted, $position, count($sorted) - $position, true);
            }
        }

        $sorted = array_reverse($sorted);

        return $sorted;
    }

    /**
     * Get user of the purchase.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Purchases\Payment', 'purchase_number', 'purchase_number');
    }

    public function successfulPayment()
    {
        return $this->hasOne('App\Models\Purchases\Payment', 'purchase_number', 'purchase_number')
            ->whereIn('gateway_response_code', ['00', '100', 'HP', 'VOUCHER', 'COMPLETE']);
    }

    public function getFormattedNumber()
    {

        return ltrim($this->purchase_number, "0");
        // return substr($this->purchase_number, 6);
    }

    public function getFormattedDate()
    {
        return $this->created_at->format('d/m/Y');
    }
    public function getInstallmentDataAttribute()
    {

        return json_decode($this->installment);
    }
    /**
     * Get purchase status.
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Globals\Status', 'purchase_status');
    }

    /**
     * Get dealer info
     */
    public function dealerInfo()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerInfo', 'dealer_id');
    }

    /**
     * Get largest purchase number.
     */
    public function scopeLargestPurchaseNumber($query)
    {
        return $query->max('purchase_number');
    }

    /**
     * Get largest receipt number.
     */
    public function scopeLargestReceiptNumber($query)
    {
        return $query->max('receipt_number');
    }

    /**
     * Get state.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\Globals\State', 'ship_state_id');
    }

    /**
     * Get city.
     */
    public function cityKey()
    {
        return $this->belongsTo('App\Models\Globals\City', 'ship_city_key', 'city_key');
    }

    public function fees()
    {
        return $this->hasMany('App\Models\ShippingInstallations\Fee', 'purchase_id', 'id');
    }

    public function getFeeAmount($type)
    {
        return $this->fees->where('type', $type)->pluck('amount')->first();
    }

    public function totalNoFee(){
        //

    }

    public function getFeeDetails($type)
    {
        if ($type == 'rebate_fee') {
            // dd($this->fees->where('type', $type));
            return $this->fees->where('type', $type);
        } else {
        return $this->fees->where('type', $type)->first();
    }
    }

    public function countryId()
    {
        return $this->hasOne('App\Models\Globals\Countries', 'country_id', 'country_id');
    }

    public function rbs()
    {
        return $this->hasOne('App\Models\Purchases\RBS', 'purchases_id', 'id');
    }

    // Seach
    public function scopesearchFilter($query, $searchFilter, $selectSearch)
    {

        if (!is_null($searchFilter)) {

            if ($selectSearch == 'PO') {
                return $query
                    ->leftJoin('orders', 'orders.purchase_id', '=', 'purchases.id')
                    ->select('orders.order_number', 'purchases.*')
                    ->where('orders.order_number', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'BDO') {
                return $query
                ->leftJoin('orders', 'orders.purchase_id', '=', 'purchases.id')
                ->select('orders.delivery_order', 'purchases.*')
                ->where('orders.delivery_order', 'like', '%' . $searchFilter . '%');
            }elseif ($selectSearch == 'PODO') {
                $orders = Order::find($searchFilter);
                return $query->where('id',$orders->purchase_id);
            } elseif ($selectSearch == 'inv_number') {
                return $query
                    ->where('purchases.inv_number', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'purchase_number') {
                return $query
                    ->where('purchases.purchase_number', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'NAME') {
                return $query
                    ->leftJoin('user_infos', 'user_infos.user_id', '=', 'purchases.user_id')
                    ->select('user_infos.full_name', 'purchases.*')
                    ->where('purchases.ship_full_name', 'like', '%' . $searchFilter . '%')
                    ->orWhere('user_infos.full_name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'RM') {
                return $query
                    ->where('purchases.purchase_amount', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'PT') {
                switch ($searchFilter) {
                    case 'Not Available':
                        return $query->whereNull('purchase_type');
                        break;
                    case 'Card':
                        return $query->whereIn('purchase_type', ['Card', 'card_cybersource']);
                        break;
                }
                return $query->where('purchase_type', $searchFilter);
            }
        }
        return $query;
    }

    public function scopestatusFilter($query, $statusFilter)
    {
        return $query->whereIn('purchase_status', $statusFilter);
    }

    //date filter from date to date
    public function scopedatebetweenFilter($query, $datefrom, $dateto)
    {
        if (!is_null($datefrom) && !is_null($dateto)) {
            return $query
                ->whereRaw("STR_TO_DATE(purchases.purchase_date, '%d/%m/%Y') BETWEEN '{$datefrom}' AND '{$dateto}'");
        } elseif (isset($datefrom) || isset($dateto)) {

            return $query
                ->where(DB::raw("(STR_TO_DATE(purchases.purchase_date,'%d/%m/%Y'))"), "$datefrom")
                ->orWhere(DB::raw("(STR_TO_DATE(purchases.purchase_date,'%d/%m/%Y'))"), "$dateto");
        }
    }

    public function shippingAddress()
    {
        return "{$this->ship_address_1}, {$this->ship_address_2}, {$this->ship_address_3}";
    }

    public function showStatus()
    {
        switch ($this->status->name) {
            case 'Refunded Voucher (Full)':
                $name = 'Full Refunded';
                break;

            case 'Refunded Voucher (Partial)':
                $name = 'Partial Refunded';
                break;

            default:
               $name = $this->status->name;
                break;
        }
        return $name;
    }
}
