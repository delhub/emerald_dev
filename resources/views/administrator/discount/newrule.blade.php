@extends('layouts.administrator.mainVue')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/discount/rule" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3" id="apps">
        <div class="card-body">
            <div class="row">

                <div class="col-12">
                    <h4>
                        Create New Rule
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.discount.storeRule') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Rule Type <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <select v-model="rule_type" name="rule_type" class="form-control" style="width: 100%;">
                                <option value="agent_registration" {{ old('rule_type') == 'agent_registration' ? 'selected' : ''}}>Agent Registration</option>
                                <option value="auto_apply" {{ old('rule_type') == 'auto_apply' ? 'selected' : ''}}>Auto Apply</option>
                                <option value="manual_apply" {{ old('rule_type') == 'manual_apply' ? 'selected' : ''}}>Manual Apply</option>
                                <option value="generate_checkout" {{ old('rule_type') == 'generate_checkout' ? 'selected' : ''}}>Generate After Checkout Status</option>
                                <option value="tradeIn" {{ old('rule_type') == 'tradeIn' ? 'selected' : ''}}>Trade In</option>
                                <option value="purchase" {{ old('rule_type') == 'purchase' ? 'selected' : ''}}>Purchase With Purchase</option>
                                <option value="refund" {{ old('rule_type') == 'refund' ? 'selected' : ''}}>refund</option>
                            </select>
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <select name="country_id" class="form-control" style="width: 100%;">
                                <option value="">Country..</option>
                                <option value="MY" selected>Malaysia</option>
                                <option value="SG">Singapore</option>
                            </select>

                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Name <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" name="name" class="form-control" placeholder="Rule Name" value="{{old('name')}}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Discount Calculation <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select v-model="coupon_type" name="coupon_type" class="form-control" style="width: 100%;">
                                    <option value="value" {{ old('coupon_type') == 'value' ? 'selected' : ''}}>Value</option>
                                    <option value="percentage" {{ old('coupon_type') == 'percentage' ? 'selected' : ''}}>Percentage</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Discount by <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select v-model="discount_type" name="discount_type" class="form-control" style="width: 100%;">
                                    <option value="quantity" {{ old('discount_type') == 'quantity' ? 'selected' : ''}}>Quantity</option>
                                    <option value="price" {{ old('discount_type') == 'price' ? 'selected' : ''}}>Price</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-if="(coupon_type == 'percentage' && rule_type !='auto_apply') || general_type =='pairing'">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Auto Generate Rule ID (Pairing)
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="generate_rule_id" class="form-control" style="width: 100%;">
                                    <option value="0">Select Auto Generate Rule</option>
                                    <option v-for="(m,i) in generate_id" :value="m.id">@{{m.id}} - @{{m.name}}</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Minimum <span v-if="discount_type =='quantity'">quantity</span><span v-else>cart total</span><small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" name="minimum" class="form-control" placeholder="Minimum" value="{{old('minimum')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Maximum <span v-if="discount_type =='quantity'">quantity</span><span v-else>cart total</span><small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" name="maximum" class="form-control" placeholder="Maximum" value="{{old('maximum')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Available for <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="available_for" class="form-control" style="width: 100%;">
                                    <option value="all" {{ old('available_for') == 'all' ? 'selected' : ''}}>All</option>
                                    <option value="personal" {{ old('available_for') == 'personal' ? 'selected' : ''}}>Personal</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    General Type <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select v-model="general_type" name="general_type" class="form-control" style="width: 100%;">
                                    <option value="normal" {{ old('general_type') == 'normal' ? 'selected' : ''}}>Normal</option>
                                    <option value="breakdown" {{ old('general_type') == 'breakdown' ? 'selected' : ''}}>Breakdown</option>
                                    <option value="pairing" {{ old('general_type') == 'pairing' ? 'selected' : ''}}>Pairing</option>
                                    <option value="stepBreakdown" {{ old('general_type') == 'stepBreakdown' ? 'selected' : ''}}>Step Breakdown</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row"  v-show="coupon_type !='percentage'">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    <span v-if="general_type =='breakdown'">Breakdown</span><span v-else>Step</span> Value<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" v-model="step" name="step" class="form-control" placeholder="Step" value="{{old('step')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    <span v-if="coupon_type =='percentage'">Percentage</span>  Value<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" name="value" class="form-control" placeholder="Value" value="{{old('value')}}">
                                <span class="form-text text-muted small" v-if="coupon_type =='value'">value generate for each step</span><span v-else>percentage value</span>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="rule_type !='auto_apply'">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Times Exceed<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" v-model="times_exceed" name="times_exceed" class="form-control" placeholder="Times Exceed" value="{{old('times_exceed')}}">
                                <span class="form-text text-muted small">Ex: vaucher value rm500 and times exceed = 2; 500 x 2 = RM 1000;</span>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="rule_type !='auto_apply'">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    <span v-if="general_type =='breakdown'">Monthly Max Usage</span><span v-else>Max Usage</span><small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" v-model="limit" name="limit" class="form-control" placeholder="Max Usage" value="{{old('limit')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Limit Type<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select v-model="limit_type" name="limit_type" @@change="getLimit(limit_type)" class="form-control" style="width: 100%;">
                                    <option value="na" selected="selected">Not Available</option>
                                    <option value="ex_category" {{ old('limit_type') == 'ex_category' ? 'selected' : ''}}>Exclude Category</option>
                                    <option value="in_category" {{ old('limit_type') == 'in_category' ? 'selected' : ''}}>Include Category</option>
                                    <option value="ex_product" {{ old('limit_type') == 'ex_product' ? 'selected' : ''}}>Exclude Product</option>
                                    <option value="in_product" {{ old('limit_type') == 'in_product' ? 'selected' : ''}}>Include Product</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="limit_hide">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Limit ID<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select id="multi" v-model="limit_id" name="limit_id[]" multiple style="width: 100%" >
                                    <option v-for="(l,i) in lid" :value="l.id">@{{l.name}}</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Start Date<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="date" name="start_date" {{-- id="start_date" --}} class="form-control" placeholder="Start Date" autocomplete="off" value="{{old('start_date')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    End Date<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="date" name="end_date" {{-- id="end_date" --}} class="form-control" placeholder="End Date" autocomplete="off" value="{{old('end_date')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Expiry Date Type<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select v-model="expiry_date_type" name="expiry_date_type" class="form-control" style="width: 100%;">
                                    <option value="0">Fixed</option>
                                    <option value="3" {{ old('expiry_date_type') == 3 ? 'selected' : ''}}>Daily Range</option>
                                    <option value="1" {{ old('expiry_date_type') == 1 ? 'selected' : ''}}>Monthly Range</option>
                                    <option value="2" {{ old('expiry_date_type') == 2 ? 'selected' : ''}}>Yearly Range</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="expiry_date_type ==0">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Expiry Date<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="date" name="expiry_date" {{-- id="expiry_date" --}} class="form-control" placeholder="Expiry Date" autocomplete="off" value="{{old('expiry_date') ?? date('Y-m-d')}}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="expiry_date_type ==3">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Daily Range Expiry Date<small class="text-danger"></small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="breakdown_exp_date" class="form-control">
                                    <option value="0">-- Select Daily Range --</option>
                                    @for ($i = 1; $i <= 30; $i++)
                                    <option value="{{$i}}">{{$i}} Day</option>
                                    @endfor
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="expiry_date_type ==1">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Monthly Range Expiry Date<small class="text-danger"></small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="breakdown_exp_date" class="form-control">
                                    <option value="0">-- Select Month Range --</option>
                                    @for ($i = 1; $i <= 12; $i++)
                                    <option value="{{$i}}">{{$i}} Month</option>
                                    @endfor
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-show="expiry_date_type ==2">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Yearly Range Expiry Date<small class="text-danger"></small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="breakdown_exp_date" class="form-control">
                                    <option value="0">-- Select Yearly Range --</option>
                                    @for ($i = 1; $i <= 10; $i++)
                                    <option value="{{$i}}">{{$i}} Year</option>
                                    @endfor
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Remarks
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" name="remarks" class="form-control" placeholder="remarks" value="{{old('remarks')}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <input type="hidden" name="generate_rule_id" value="0" v-if="coupon_type !='percentage' && rule_type =='auto_apply'">
                                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-tables-2@2.2.1/dist/vue-tables-2.min.js"></script>
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script>
    var app = new Vue({
    el: '#apps',
    data: {
        rule: [],
        lid: [],
        limit_type: 'na',
        limit_id: [],
        limit_hide: false,
        limit: 1,
        step: 1,
        name: '',
        expiry_date_type: 0,
        discount_type: 'price',
        general_type: 'normal',
        rule_type: 'auto_apply',
        times_exceed: 1,
        generate_id: [],
        coupon_type: 'value'
    },
    mounted() { //created // mounted //beforeMount
        this.getCategory()
        this.getRuleByType('manual_apply')
    },
    methods: {
        getGlobalProduct() {
        axios
            .get('/administrator/discount/get-global-product')
            .then(response => {
                this.lid = response.data
                })
            .catch(error => console.log(error))
        },
        getCategory() {
        axios
            .get('/administrator/discount/get-category')
            .then(response => {
                this.lid = response.data
                })
            .catch(error => console.log(error))
        },
        getRule() {
        axios
            .get('/administrator/discount/getRule')
            .then(response => {
                this.rule = response.data
                })
            .catch(error => console.log(error))
        },
        getRuleByType(type) {
        axios
            .get('/administrator/discount/getRule?type=' + type)
            .then(response => {
                this.generate_id = response.data
                })
            .catch(error => console.log(error))
        },
        getLimit(type) {
            switch(type) {
            case 'ex_category':
            case 'in_category':
                app.getCategory()
                this.limit_hide = true
            break;
            case 'ex_product':
            case 'in_product':
                app.getGlobalProduct()
                this.limit_hide = true
            break;
            default:
                this.limit_hide = false
                this.limit_id = []
            }
        }
    }
    });
</script>
<script>
    $('#start_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });

    $('#end_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });

    $('#expiry_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });

    setTimeout(function() {
        new SlimSelect({
            select: '#multi'
        })
    }, 300)
</script>
@endpush
