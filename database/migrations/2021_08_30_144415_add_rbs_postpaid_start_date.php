<?php

use App\Models\Rbs\RbsPostpaid;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRbsPostpaidStartDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rbs_postpaid', function (Blueprint $table) {
            $table->date('last_reminder')->after('months_id')->nullable();
            $table->date('start_reminder')->after('months_id');
            $table->integer('rbs_quantity')->after('attribute_id')->default(1);
        });

        $rbsPostpaids = RbsPostpaid::all();

        foreach ($rbsPostpaids as $rbsPostpaid) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $rbsPostpaid->created_at)->addMonth()->format('Y-m');
            $rbsPostpaid->start_reminder = $date.'-01';
            $rbsPostpaid->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rbs_postpaid', function (Blueprint $table) {
            $table->dropColumn('start_reminder');
            $table->dropColumn('last_reminder');
        });
    }
}
