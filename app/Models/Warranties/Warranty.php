<?php

namespace App\Models\Warranties;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    // Set table
    protected $table = 'panel_products_warranty';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

}
