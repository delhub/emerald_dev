<!DOCTYPE html>
<html>

<head>
 @include('layouts.favicon')
<style type="text/css">
* {
            padding: 0;
            margin: 0;
            font-family: 'Lato', sans-serif;
                font-family: 'Lato', sans-serif;
        }
   .topbar{
        position: absolute;
        margin-top: 0;
        width: 100%;
        border-top: 18px solid #ffcc00;
        left: 0;
    }
    .logo-barcode{
        display: inline-flex;
        width: 100%;
    }
    .logo-barcode .info{
        width: 50%;
        padding: 0px 15px;
    }
    .logo-barcode .barcode{
        width: 30%;
    }
    .logo-img,.logo-barcode .logo{
        width: 22%;
        border-right: 2px solid #ffcc00;
        padding: 0 15px;
    }
    .logo-img img{
        width: 190px;
        height: auto;
        margin-left: 20px;
        /* margin-right:40px; */
    }
    .myfloat-left{
        float: left;
    }
    .logo-barcode .info .brand{
        font-size: 1rem;
        font-weight: 600;
        margin-bottom: 3px;
    }
    .logo-barcode .info .numB{
        font-size: .75rem;
        margin-bottom: 0px;
    }
    .logo-barcode .address{
        font-size: .85rem;
        line-height: 1.5;
        margin-bottom: 0px;
        letter-spacing: .05em;
    }

    .topheader{
        margin: 0px;
        border-top: 18px solid #ffcc00;
        /* background: rgb(180, 180, 180); */
        width: 100%;
        padding-bottom: 10px;
    }
    .company-info{
        padding-left: 15px;
    }
    .company-info p{
        margin: 0px;
        font-size: 16px;
        letter-spacing: .08rem;
        font-weight: 600;
    }
    .company-info p+p{
        font-weight: normal;
        font-size: 12px;
        margin-top: 2px;
        color: rgb(70, 70, 70);
    }
    .company-info p+p+p{
        font-size: 14px;
        margin-top: 6px;
        line-height: 18px;
        margin-bottom: 6px;
    }
    .company-info .company-contact span{
        width: 33%;
        text-align: center;
        font-size: .8rem;
        margin-right: 18px;
        color: rgb(70, 70, 70);
    }
    .company-info .company-contact img{
        height: 9px;
        margin-right: 2px;
    }
    .mytable1{
        width: 100%;
        margin-top: 10px;
    }
    .mytable2,.mytable3{
        width: 100%;
        padding: 0px 36px;
    }
    .mytable3{
        margin-top: 10px;
        margin-bottom:-10px;
    }
    .mytable2 tr td.left-userinfo,.mytable2 tr td.center-userinfo{
        width: 28%;
        padding-top: 60px;
        vertical-align: top;
    }
    td.left-userinfo span,td.center-userinfo span,
    td.left-userinfo p,td.center-userinfo p{
        letter-spacing: .06rem;
    }
    td.left-userinfo span,td.center-userinfo span{
        color: #58595b;
        padding-left: 13px;
        font-size: 16px;
    }
    td.right-userinfo{
        padding-left: 13px;
    }
    td.right-userinfo span{
        font-size: 1.8rem;
        color: #ffcc00;
        font-weight: 700;
        margin: 0 10px;
    }
    td.left-userinfo p,td.center-userinfo p{
        padding-left: 13px;
        font-size: 14px;
        line-height: 1.2;
    }
    td.left-userinfo p.user-info,td.center-userinfo p.user-info{
        line-height: 1.2em;
        font-size: 14px;
    }
    td.left-userinfo p.user-info span,td.center-userinfo p.user-info span{
        font-weight: 600;
        font-size: 16px;
        line-height: 1.2em;
        letter-spacing: 0;
        color: #000;
    }
    .mytable2 tr td.right-userinfo{
        width: 44%;
        vertical-align: bottom;
        padding-top: 20px;
        padding-right: 0px;
        padding-left:20px;
    }
    td.right-userinfo .boxinfo{
        background-color: #fef5d6;
        border-radius: 20px;
        padding: 20px 0px 20px 20px;
    }
    .hry{
        margin-top: 8px;
        border-bottom: 2px solid #ffcc00;
    }
    .inner-table{
        width: 100%;
    }
    .inner-table tr td span{
        color:#000;
        font-size:16px;
        text-align: left;
        text-transform: capitalize;
        font-weight: normal;
    }
    .topBottomBorder {
        padding: 10px 0px;
        border-top: 1.5px solid #ffcc00;
        border-bottom: 1.5px solid #ffcc00;
        font-size: 16px;
        z-index: 1;
    }
    .dot-box{
        text-align: right;
        font-size: 14px;
        color: #000;
    }
    .text-grey {
        color: #58595b;
    }
    .page-style span{
        padding-left:-10px;
        color:#ffcc00
    }
</style>

</head>

<body class="topheader" onload="subst()">
        <div>
            <table @if($country != 'SG') style="margin-top: 10px" @else style="margin-top: 17px; height:100px;" @endif >
                <tr>
                    <td class="logo-img">
                        <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-logo.jpg') }}">
                    </td>

                    <td style="vertical-align: top; padding: 0 15px;">

                        <p style="font-size: 1.25rem; font-weight: 600; margin-bottom: 3px;">
                            {{$country->company_name}}
                        </p>

                        <p class="text-grey" style="font-size: .75rem; margin-bottom: 0px;">
                            @if($purchase->country_id != 'SG')
                            Company No: {{$country->company_reg}}
                            @else
                            GST Reg. No.: {{$country->company_reg}}
                            @endif
                        </p>

                        <p class="text-grey" style="font-size: .85rem; line-height: 1.5; margin-bottom: 0px;">
                            {!!$country->company_address!!}
                        </p>

                        <div style="width: 100%;">
                            <span class="text-grey"
                                style="width: 33%; text-align: center; font-size: .8rem; margin-right: 18px;">
                                <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-mail.jpg') }}"
                                    style="height: 9px; margin-right: 2px;">
                                    {{$country->company_email}}
                            </span>

                            <span class="text-grey"
                                style="width: 33%; text-align: center; font-size: .8rem; margin-right: 18px;">
                                <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-web.jpg') }}"
                                    style="height: 11px; margin-right: 2px;">
                                    {{$country->url}}
                            </span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

            <table class="mytable2" cellspacing="0" cellpadding="0">
                <tr style="height:100px">
                    <td class="left-userinfo" style="height:100%;border-bottom:2px solid #ffcc00;padding-bottom: 5px;">
                        <span style="font-weight: 600;">
                            PURCHASER:
                        </span>
                        <div class="hry"></div>
                            <p style="padding-top: 10px;line-height:14px;letter-spacing: .01em;font-size:12px;">
                                {{ $purchase->user->userInfo->shippingAddress->address_1 }},
                                <br>
                                {{ $purchase->user->userInfo->shippingAddress->address_2 }},
                                {{ $purchase->user->userInfo->shippingAddress->address_3 }},
                                <br>
                                {{ $purchase->user->userInfo->shippingAddress->postcode }},
                                @php
                                    if(isset($purchase->ship_address_1)){
                                        if ($purchase->user->userInfo->shippingAddress->cityKey != NULL) {
                                            echo $purchase->user->userInfo->shippingAddress->cityKey->city_name;
                                        }else {
                                            echo $purchase->user->userInfo->shippingAddress->city ;
                                        }
                                    }else{
                                        echo '' ;
                                        // echo $purchase->user->userInfo->shippingAddress->cityKey->city_name ;
                                    }
                                @endphp
                                <br>
                                {{ $purchase->user->userInfo->shippingAddress->state->name }},
                                Malaysia.
                            </p>
                            <p class="user-info">
                                Attn
                                <span>
                                    : {{ $purchase->user->userInfo->full_name }}
                                </span>
                            </p>
                            <p class="user-info">
                                Tel
                                <span style="padding-left: 22px">
                                    : {{ $purchase->user->userInfo->contacts->first()->contact_num }}
                                </span>
                            </p>
                        {{-- <div class="hry" style="padding-bottom: 5px;"></div> --}}
                    </td>
                    <td class="center-userinfo" style="height:100%; border-bottom:2px solid #ffcc00;padding-bottom: 5px;">
                        <span style="font-weight: 600;">
                            DELIVER TO:
                        </span>
                        <div class="hry"></div>
                            <p style="padding-top: 10px;line-height:14px;letter-spacing: .01em;font-size:12px;">
                                {{ isset($purchase->ship_address_1) ? $purchase->ship_address_1 : ''}}
                                <br>
                                {{ isset($purchase->ship_address_1) ? $purchase->ship_address_2 : ''}}
                                @if (isset($purchase->ship_address_1))
                                @if (isset($purchase->ship_address_3))
                                <br>
                                {{ $purchase->ship_address_3 }}
                                @endif
                                @else
                                {{-- @if (isset($purchase->user->userInfo->shippingAddress->address_3))
                                <br>
                                {{ $purchase->user->userInfo->shippingAddress->address_3 }}
                                @endif --}}
                                @endif
                                {{ isset($purchase->ship_address_1) ? $purchase->ship_postcode : '' }}
                                @php
                                    if(isset($purchase->ship_address_1)){
                                        if ($purchase->cityKey != NULL && $purchase->ship_city_key != 0) {
                                            echo $purchase->cityKey->city_name;
                                        }else {
                                            echo $purchase->ship_city ;
                                        }
                                    }else{
                                        echo '' ;
                                    }
                                @endphp
                                <br>
                                {{ isset($purchase->ship_address_1) ? $purchase->state->name : '' }},
                                Malaysia.
                            </p>
                            <p class="user-info">
                                Attn
                                <span>
                                    : {{ isset($purchase->ship_full_name) ? $purchase->ship_full_name : '' }},
                                </span>
                            </p>
                            <p class="user-info">
                                Tel
                                <span style="padding-left: 22px">
                                    : {{ isset($purchase->ship_contact_num) ? $purchase->ship_contact_num : '' }},
                                </span>
                            </p>
                        {{-- <div class="hry" style="padding-bottom: 5px;"></div> --}}
                    </td>
                    <td class="right-userinfo">
                            <span>
                                @if($purchase->country_id != 'SG')
                                INVOICE
                                @else
                                TAX INVOICE
                                @endif
                            </span>
                            <div class="boxinfo">
                                <table class="inner-table">
                                    <tr>
                                        <td style="width: 45%;">
                                            INVOICE NO.
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 50%;">
                                            {{ $purchase->getFormattedNumber() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ $purchase->purchase_date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Agent Code
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ ($purchase->user->userInfo->agentInformation) ? $purchase->user->userInfo->agentInformation->account_id : ''}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Credit Terms
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                                {{ $purchase->purchase_type }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{$purchase->user->userInfo->account_status == '1' ? 'DC Customer' : 'Bujishu Customer'}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Page
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{-- here is show the page number --}}
                                            <span style="margin-left: 0px" class="page"></span> of <span class="topage"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    </td>
                </tr>
            </table>

            <table class="mytable3" cellspacing="0" cellpadding="0">
                <tr class="form-text-center" style="font-size:16px;background-color: #fef5d6;font-weight:600;">
                    <th class="topBottomBorder" style="width: 50px;">No.</td>
                    {{-- <th class="topBottomBorder" style="width: 197px;">Product ID</td> --}}
                    <th class="topBottomBorder" style="width: 500px;">Description</td>
                    <th class="topBottomBorder" style="width: 50px;">Qty</td>
                    <th class="topBottomBorder" style="width: 157px;">
                        @if($purchase->country_id != 'SG')
                        Unit Price (RM)
                        @else
                        Unit Price (SGD)
                        @endif
                    </td>
                    <th class="topBottomBorder" style="width: 144px;">
                        @if($purchase->country_id != 'SG')
                        Amount (RM)
                        @else
                        Amount (SGD)
                        @endif
                    </td>
                </tr>

            </table>

    </thead>
    <script>
        function subst() {
            var vars = {};
            var query_strings_from_url = document.location.search.substring(1).split('&');
            for (var query_string in query_strings_from_url) {
                if (query_strings_from_url.hasOwnProperty(query_string)) {
                    var temp_var = query_strings_from_url[query_string].split('=', 2);
                    vars[temp_var[0]] = decodeURI(temp_var[1]);
                }
            }
            var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
            for (var css_class in css_selector_classes) {
                if (css_selector_classes.hasOwnProperty(css_class)) {
                    var element = document.getElementsByClassName(css_selector_classes[css_class]);
                    for (var j = 0; j < element.length; ++j) {
                        element[j].textContent = vars[css_selector_classes[css_class]];
                    }
                }
            }
        }
        </script>

</body>
