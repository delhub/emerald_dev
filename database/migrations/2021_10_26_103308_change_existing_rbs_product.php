<?php

use App\Models\Categories\PivCategory;
use App\Models\Products\ProductAttribute;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Products\Product;

class ChangeExistingRbsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $productCodes = ['RBS-SWSENX0005','RBS-BLMENX0051','RBS-NTFENX0019','RBS-LRPFWX0003','RBS-KDLIMX0020','RBS-DMVBWX0002','RBS-SWSENX0009','RBS-SWSENX0001'];

        foreach ($productCodes as $productCode) {


            $product = Product::where('product_code',$productCode)->first();
            $product->product_status = 2;
            $product->save();
        }

        $NTFENX0019 = ProductAttribute::where('product_code','NTFENX0019')->first();
        $NTFENX0019->allow_rbs = 1;
        $NTFENX0019->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[3]['price'] = 11890;
        $rbs_times_send[3]['qty'] = 1;
        $rbs_times_send[4]['price'] = 11890;
        $rbs_times_send[4]['qty'] = 1;
        $rbs_times_send[5]['price'] = 11890;
        $rbs_times_send[5]['qty'] = 2;

        $NTFENX0019->rbs_times_send = $rbs_times_send;
        $NTFENX0019->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $NTFENX0019->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();



        //------------------------------------
        //------------------------------------
        $SWSENX0009 = ProductAttribute::where('product_code','SWSENX0009')->first();
        $SWSENX0009->allow_rbs = 1;
        $SWSENX0009->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[4]['price'] = 2195;
        $rbs_times_send[4]['qty'] = 2;
        $rbs_times_send[5]['price'] = 2195;
        $rbs_times_send[5]['qty'] = 2;

        $SWSENX0009->rbs_times_send = $rbs_times_send;
        $SWSENX0009->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $SWSENX0009->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $SWSENX0001 = ProductAttribute::where('product_code','SWSENX0001')->first();
        $SWSENX0001->allow_rbs = 1;
        $SWSENX0001->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[4]['price'] = 2445;
        $rbs_times_send[4]['qty'] = 2;
        $rbs_times_send[5]['price'] = 2445;
        $rbs_times_send[5]['qty'] = 2;

        $SWSENX0001->rbs_times_send = $rbs_times_send;
        $SWSENX0001->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $SWSENX0001->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $DMVBWX0002 = ProductAttribute::where('product_code','DMVBWX0002')->first();
        $DMVBWX0002->allow_rbs = 1;
        $DMVBWX0002->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[4]['price'] = 2845;
        $rbs_times_send[4]['qty'] = 2;
        $rbs_times_send[5]['price'] = 2845;
        $rbs_times_send[5]['qty'] = 2;

        $DMVBWX0002->rbs_times_send = $rbs_times_send;
        $DMVBWX0002->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $DMVBWX0002->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $LRPFWX0003 = ProductAttribute::where('product_code','LRPFWX0003')->first();
        $LRPFWX0003->allow_rbs = 1;
        $LRPFWX0003->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[4]['price'] = 3695;
        $rbs_times_send[4]['qty'] = 2;
        $rbs_times_send[5]['price'] = 3695;
        $rbs_times_send[5]['qty'] = 2;

        $LRPFWX0003->rbs_times_send = $rbs_times_send;
        $LRPFWX0003->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $LRPFWX0003->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $SWSENX0005 = ProductAttribute::where('product_code','SWSENX0005')->first();
        $SWSENX0005->allow_rbs = 1;
        $SWSENX0005->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[3]['price'] = 7690;
        $rbs_times_send[3]['qty'] = 1;
        $rbs_times_send[4]['price'] = 7690;
        $rbs_times_send[4]['qty'] = 1;
        $rbs_times_send[5]['price'] = 7690;
        $rbs_times_send[5]['qty'] = 2;

        $SWSENX0005->rbs_times_send = $rbs_times_send;
        $SWSENX0005->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $SWSENX0005->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $BLMENX0051 = ProductAttribute::where('product_code','BLMENX0051')->first();
        $BLMENX0051->allow_rbs = 1;
        $BLMENX0051->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[3]['price'] = 13990;
        $rbs_times_send[3]['qty'] = 1;
        $rbs_times_send[4]['price'] = 13990;
        $rbs_times_send[4]['qty'] = 1;

        $BLMENX0051->rbs_times_send = $rbs_times_send;
        $BLMENX0051->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $BLMENX0051->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $KDLIMX0020 = ProductAttribute::where('product_code','KDLIMX0020')->first();
        $KDLIMX0020->allow_rbs = 1;
        $KDLIMX0020->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[4]['price'] = 2345;
        $rbs_times_send[4]['qty'] = 2;
        $rbs_times_send[5]['price'] = 2345;
        $rbs_times_send[5]['qty'] = 2;

        $KDLIMX0020->rbs_times_send = $rbs_times_send;
        $KDLIMX0020->save();

        $rbscategory = new PivCategory();
        $rbscategory->product_id = $KDLIMX0020->product2->parentProduct->id;
        $rbscategory->category_id = 143;
        $rbscategory->save();

        //------------------------------------
        //------------------------------------

        $KDLIMX0021 = ProductAttribute::where('product_code','KDLIMX0021')->first();
        $KDLIMX0021->allow_rbs = 1;
        $KDLIMX0021->rbs_frequency = ["1"];

        $rbs_times_send = array();
        $rbs_times_send[3]['price'] = 2345;
        $rbs_times_send[3]['qty'] = 1;
        $rbs_times_send[4]['price'] = 2345;
        $rbs_times_send[4]['qty'] = 1;

        $KDLIMX0021->rbs_times_send = $rbs_times_send;
        $KDLIMX0021->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_products', function (Blueprint $table) {
            //
        });
    }
}
