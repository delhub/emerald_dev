@extends('layouts.administrator.main')

@section('content')
@php

@endphp

    <h1>Import Data</h1>

    <div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 col-md-8 col-lg-{{$req->col}}">

                    @if(isset($data) && count($data) == 0)
                        <div class="container my-3">
                            <form action="{{route('fileUpload')}}" method="post" enctype="multipart/form-data">
                            <h3 class="text-center mb-5">Import Image/Data</h3>
                                @csrf
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="chooseFile" onchange="getFileData(this)">
                                        <label class="custom-file-label" for="chooseFile"><span id="fn">Select file</span></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                <div class="col-12" id="withHeading" style="display: none">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="withHead" id="withHead" type="checkbox" class="custom-control-input" aria-describedby="withHeadHelpBlock">
                                    <label for="withHead" class="custom-control-label">WithHeading</label>
                                    </div>
                                </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-12">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                        <input name="launching" id="launching" type="checkbox" class="custom-control-input" aria-describedby="launchingHelpBlock" value="1">
                                        <label for="launching" class="custom-control-label">Launching</label>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                                    Upload Files
                                </button>
                            </form>
                        </div>
                        @endif


                        @if(isset($data) && count($data) > 0)
                        <div class="my-3">
                        <form action="{{route('fileUpload')}}" class="form-check" method="post">
                            <h3 class="text-center mb-5">Select Import Data</h3>
                            @csrf
                        <div class="row justify-content-md-center mt-5 table-responsive">
                            <div class="col col-lg-2"></div>
                            <div class="col-md-auto col-lg-auto">
                                <div class="row my-4">
                                    <div class="col align-self-start">
                                        <a href="{{route('administrator.import.index')}}"><button type="button" class="btn btn-primary"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    </div>
                                    <div class="col align-self-center"></div>
                                    <div class="col align-self-end">
                                        <div class="text-right">
                                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Confirm... <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-sm table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center"><input name='selectAll' type="checkbox" onchange="countChecked()"></th>
                                        @for ($i = 0; $i < count($data[0]); $i++)
                                            @if($req->withHeading == 'on')
                                            <th scope="col">{{$data[0][$i]}}</th>
                                            @else
                                            <th scope="col">Field{{$i}}</th>
                                            @endif
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($req->withHeading == 'on')
                                        @php
                                            unset($data[0]);
                                            $key = 1;
                                        @endphp
                                    @else
                                        @php
                                            $key = 0;
                                        @endphp
                                    @endif
                                    @foreach ($data as $k=>$d)
                                    <tr>
                                        <th class="text-center"><input name='data[]' type="checkbox" value="{{ $k }}" onchange="countChecked()"></th>
                                        @for ($i = 0; $i < count($data[$key]); $i++)
                                        <td {{-- style="display: {{$i >= 6 ? 'none': ''}}" --}}>{{ $d[$i] }}</td>
                                        @endfor
                                    </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                                <hr>
                                <input type="hidden" name="import" value="{{$req->excel}}">
                                <div class="row my-4">
                                    <div class="col align-self-start">
                                        <a href="{{route('administrator.import.index')}}"><button type="button" class="btn btn-primary"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back</button></a>
                                    </div>
                                    <div class="col align-self-center"></div>
                                    <div class="col align-self-end">
                                        <div class="text-right">
                                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Confirm... <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-lg-2"></div>
                        </div>
                        </form>
                        </div>
                        @endif

                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('script')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

$(':checkbox[name=selectAll]').click (function () {
    $(':checkbox[type=checkbox]').prop('checked', this.checked);
});

//check checked
countChecked();

function countChecked() {
    var searchIDs = $('input:checked').map(function(){
     return $(this).val();
   });

   var size = Object.keys(searchIDs.get()).length;

   if(size == 0) { $('#submit').prop('disabled', true);  }
   else { $('#submit').prop('disabled', false); }

//    console.log(size);
}

function getFileData(myFile){
    var file = myFile.files[0];
    var filename = file.name;

    var ext = filename.split('.').pop();

    switch(ext) {
    case 'csv':
    case 'xlx':
    case 'xls':
    case 'xlsx':
    case 'ods':
        $('#withHeading').show();
      break;
    default:
        $('#withHeading').hide();
  }

    $('#fn').text(filename);
}
</script>
@endpush
