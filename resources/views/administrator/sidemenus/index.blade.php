@extends('layouts.administrator.main')

@section('content')

<h1>Menu</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/sidemenus/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Menu</a>
                </div>
            </div>
        </div>

        <!-- filter start -->
        {{-- <div class="row">
            <div class="col-12">
                <form action="" method="GET" class="form-inline">
                    <div class="form-row">
                        <div class="form-group mx-sm-3 mb-2">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option value="">Menu Id</option>
                            <option value="1">2</option>
                            <option value="1">3</option>
                            <option value="1">4</option>
                            <option value="1">5</option>
                            </select>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>Menu Name</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>Parent id</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>Menu Arrangement</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mx-sm-3 mb-2">Filter</button>
                    </div>
                </form>
            </div>
        </div> --}}
        <!-- filter end -->

        <div class="table-responsive m-2">
        @if(count($sidemenus) > 0)
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>Name</td>
                        <td>Group</td>
                        <td>Country Id</td>
                        <td>Link</td>
                        <td>menu icon</td>
                        <td>Parent id</td>
                        <td>Arrangement</td>
                        <td>Edit</td>
                    </tr>
                </thead>
                <tbody>
                
                    @foreach($sidemenus as $sidemenu)
                    <tr>
                        <td style="width: 5%;">{{$sidemenu->id}}</td>
                        <td style="width: 15%;">
                        {{$sidemenu->menu_title}}
                        <br>
                        <small>Created on : {{$sidemenu->created_at}}</small>
                        </td>
                        <td style="width: 5%;">
                            {{$sidemenu->menu_group}}
                            <br>
                            @if($sidemenu->menu_show == 1)
                            <small style="color: rgb(255 255 255);padding: 5px 5px;background: #ff4646;border-radius: 5px;text-align: center;">Active</small>
                            @else
                            <small style="color: rgb(255 255 255);padding: 5px 5px;background: #81cec6;border-radius: 5px;text-align: center;">No Active</small>
                            @endif
                        </td>
                        <td style="width: 5%">
                            <p>{{$sidemenu->country_id}}</p>
                        </td>
                        <td style="width: 32%">
                            <p>{{$sidemenu->menu_link}}</p>
                        </td>
                        <td style="width: 13%">
                            <img class="mw-100" src="{{asset('/storage/customer/sidebar-icons/' . $sidemenu->menu_icon)}}" alt="">
                            <p>{{$sidemenu->menu_icon}}</p>
                        </td>
                        <td style="width: 5%">
                            <p>{{$sidemenu->menu_parent}}</p>
                        </td>
                        <td style="width: 5%;">
                            {{$sidemenu->menu_arrangement}}
                        </td>
                        <td style="width: 20%;">
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left" 
                            href="/administrator/sidemenus/edit/{{$sidemenu->id}}" class="btn btn-primary shadow-sm">Edit</a>

                            <form action="{{ route('administrator.sidemenus.destroy',['id' => $sidemenu->id]) }}" id="delete-menu-form" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            {{-- <!-- @method('DELETE') --> --}}
                            @csrf

                            <!-- <input class="btn btn-danger" type="submit" value="Delete" /> -->

                            <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>

                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$sidemenus->links()}}
                    @else
                    <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Side Menu found</p>
            @endif
        </div>
    </div>
</div>

@endsection