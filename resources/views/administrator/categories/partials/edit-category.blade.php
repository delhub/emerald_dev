<div>
    <form name="modal-form" id="modal-form" enctype="multipart/form-data">
        @method('PUT')
        <div class="form-row">
            <div class="col-12 col-md-4 my-auto text-right">
                <p>Category Name</p>
            </div>
            <div class="col-12 col-md-6 form-group">
                <input type="text" name="category_name" id="category_name" class="form-control" value="{{ $category->name }}">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 my-auto text-right">
                <p>
                    Category Slug
                    <br>
                    <small>(Replaced white spaces with hyphen)</small>
                </p>
            </div>
            <div class="col-12 col-md-6 form-group">
                <input type="text" name="category_slug" id="category_slug" class="form-control" value="{{ $category->slug }}" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 my-auto text-right">
                <p>
                    Parent Category
                    <br>
                    <small>(Can only belong to one parent category)</small>
                </p>
            </div>
            <div class="col-12 col-md-6 form-group">
                <select name="parent_category_id" id="parent_category_id" class="select2 select2-edit form-control" style="width: 100%;">
                    <option value="0" {{ ($category->parent_category_id == 0) ? 'selected' : '' }}>None</option>
                    @foreach($categories as $item)
                    <option value="{{ $item->id}}" {{ ($item->id == $category->parent_category_id) ? 'selected' : ''}}>{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 my-auto text-right">
                <p>Category Icon</p>
            </div>
            <div class="col-12 col-md-6 form-group">
                {{-- {{ file_exists(public_path('/images/icons/category/'.$category->slug.'.png')) }} --}}
                @if($category->image && $category->image->filename)
                    <img id="category_icon_img" src="/images/icons/category/{{ $category->image->filename }}" alt="" class="w-100" style="border: 1px dotted black;">
                @else
                    <img id="category_icon_img" src="{{ asset('assets/images/errors/image-not-found.png') }}" alt="" class="w-100" style="border: 1px dotted black;">
                @endif
                <div class="form-group mt-1">
                    <input type="file" name="category_icon" id="category_icon" class="form-control-file">
                </div>
            </div>
        </div>

        {{-- <div class="form-row">
            <div class="col-12 col-md-6 offset-md-4 form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="featured_category" id="featured_category">
                    <label class="custom-control-label" for="featured_category">Featured Category</label>
                </div>
            </div>
        </div> --}}

        <div class="form-row">
            <div class="col-12 col-md-4 form-group text-right">
                Roles
            </div>
            <div class="col-12 col-md-6  form-group">
                <select  class="select2 select2-edit form-control" style="width: 100%;" multiple="multiple"
                id="rolesCategory" name="rolesCategory[]">
                    @foreach($roles as $key => $role)
                        <option value="{{ $key }}"
                            @if(in_array($key, $category->roles ?? [])) selected @endif>
                            {{ $key }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 form-group text-right">
                Homepage Section (Desktop) :
            </div>
            <div class="col-12 col-md-6  form-group">
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_category" id="featured_category0" value="0" {{ $category->featured == 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="featured_category0">None</label>
                  </div>
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_category" id="featured_category1" value="1" {{ $category->featured == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="featured_category1">Lead By Categories</label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_category" id="featured_category2" value="2" {{ $category->featured == 2 ? 'checked' : '' }}>
                    <label class="form-check-label" for="featured_category2">Shop By Categories (first)</label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_category" id="featured_category3" value="3" {{ $category->featured == 3 ? 'checked' : '' }}>
                    <label class="form-check-label" for="featured_category3">Shop By Categories (second)</label>
                  </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 col-md-4 form-group text-right">
                Homepage Section (Mobile):
            </div>
            <div class="col-12 col-md-6  form-group">
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_mobile" id="mobile_featured0" value="0" {{ $category->featured_mobile == 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="mobile_featured0">None</label>
                  </div>
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_mobile" id="mobile_featured1" value="1" {{ $category->featured_mobile == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="mobile_featured1">Lead By Categories</label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_mobile" id="mobile_featured2" value="2" {{ $category->featured_mobile == 2 ? 'checked' : '' }}>
                    <label class="form-check-label" for="mobile_featured2">Shop By Categories (first)</label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_mobile" id="mobile_featured3" value="3" {{ $category->featured_mobile == 3 ? 'checked' : '' }}>
                    <label class="form-check-label" for="mobile_featured3">Shop By Categories (second)</label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="featured_mobile" id="mobile_featured4" value="4" {{ $category->featured_mobile == 4 ? 'checked' : '' }}>
                    <label class="form-check-label" for="mobile_featured4">Shop By Categories (second)</label>
                  </div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 my-auto text-right">
                <p>
                    Exclude Country<small class="text-danger">*</small>
                </p>
            </div>
            <div class="col-12 col-md-6 form-group">
                <select  class="select2 select2-edit form-control" style="width: 100%;" multiple="multiple"
                id="excludeCountryCategory" name="excludeCountryCategory[]">
                    @foreach($excludeCountry as $key => $country)
                        <option value="{{ $key }}" {{ in_array($key,$dataArray) ? 'selected' : '' }}>{{ $key}}</option>
                    @endforeach
                </select>

                <input type="hidden" name="dataType" value="search">
                <input type="hidden" name="dataValue" value="exlcude">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 my-auto text-right">
                <p>Position</p>
            </div>
            <div class="col-12 col-md-6 form-group">
                <input type="number" name="category_position" id="category_position" class="form-control" value="{{ $category->position }}">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 col-md-4 form-group text-right">
                Public View:
            </div>
            <div class="col-12 col-md-6  form-group">
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="public_view" id="public_view1" value="1" {{ $category->public_view == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="public_view1">Yes</label>
                  </div>
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="public_view" id="public_view0" value="0" {{ $category->public_view == 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="public_view0">No</label>
                </div>
            </div>
        </div>

        <div class="form-row mt-2">
            <div class="col-12 col-md-6 offset-md-4">
                <button id="modalSubmitBtn" type="submit" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                    <span class="spinner-border spinner-border-sm" style="display: none;" role="status" aria-hidden="true"></span>
                    Save changes
                </button>
            </div>
        </div>
    </form>
</div>