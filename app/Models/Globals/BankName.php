<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;

class BankName extends Model
{
    // Set table
    protected $table = 'global_bank';
    public $incrementing = false;
    // Set timestamps


    // Set primary key
    protected $primaryKey = 'bank_code';

    // Set mass assignable columns
    protected $fillable = [];

    public static function getBank($code)
    {
        $bank = self::find($code);
        return $bank->bank_name;
    }
}
