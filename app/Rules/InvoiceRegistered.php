<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Registrations\Dealer\Registration;
class InvoiceRegistered implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $registrationRecord = Registration::where('invoice_number', $value)->first();
        return (isset($registrationRecord) && $registrationRecord->agent_id) ?  false : true ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This Invoice is already redeemed';
    }
}
