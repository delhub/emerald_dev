<?php

namespace App\Imports;

use App\Models\Globals\Image;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Filesystem\Filesystem;
use App\Models\Globals\Products\Product as GP;
use App\Models\Products\ProductAttribute as PA;

class ImageImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        \Log::info($row[1] . ' ' . $row[24]);
        if (empty($row[24])) return;

        if (strtolower($row[0]) == 'product') {
            $imgId = GP::where('product_code', $row[1])->first('id');

            if ($imgId) {
                $oImage = Image::where([['imageable_id', $imgId->id], ['imageable_type', 'App\Models\Globals\Products\Product']])->delete();
            }

            $storagePath  = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $tempFolder = $storagePath . 'public/uploads/temp/formula2u/' . $row[1];

            $publicPath = public_path();

            if (file_exists($tempFolder)) {
                $files = \File::files($tempFolder);

                if (count($files) > 0) {
                    foreach ($files as $val) {
                        $img[] = pathinfo($val);
                    }

                    // \Storage::move($tempFolder, 'OtherFolder');
                    \File::ensureDirectoryExists(public_path() . '/storage/uploads/images/products');

                    $file = new Filesystem();
                    $file->moveDirectory($tempFolder, public_path() . "/storage/uploads/images/products/" . $row[1], true);
                }

                if (count($img) > 0) {
                    foreach ($img as $v) {
                        $this->InsertImage($imgId->id, $v, $row[1]);
                    }
                }

                $this->setAttributeColorImages($row);
            }

        } else {
            $this->setAttributeColorImages($row);
        }
    }

    public function setAttributeColorImages($row)
    {
        $path = "uploads/images/products/" . $row[1] . "/";
        $getImgId = Image::where('path', $path)->get();

        if (count($getImgId) > 0) {
            foreach ($getImgId as $val) {
                $name = explode('.', $val['filename']);
                $val['fileid'] = $name[0];
                $data[] = $val;
            }

            $default_img = isset($row[30]) ? $row[30] : 1;

            $img = collect($data)->where('fileid', $default_img)->first();

            $pa = PA::where('product_code', $row[2])->first();

            $pa->color_images = $img->id;

            $pa->save();
        }


    }

    public function InsertImage($id, $data, $product_code)
    {
        $image = new Image;

        $image->path = "uploads/images/products/" . $product_code . "/";
        $image->filename = $data['basename'];
        $image->default = $data['filename'] == 1 ? 1 : 0;
        $image->brand = 0;
        $image->imageable_id = $id;
        $image->imageable_type = 'App\Models\Globals\Products\Product';

        $image->save();
    }
}
