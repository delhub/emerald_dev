<?php

namespace App\Http\Middleware\Authentication;

use App\Models\Users\User;
use Auth;
use Closure;
use Log;

class CheckInfoComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::find(Auth::user()->id);

        $userInfo = $user->userInfo;
        $userAddressShipping = $userInfo->shippingAddress;
        $userAddressMailing = $userInfo->mailingAddress;
        $userContactMobile = $userInfo->mobileContact;

        if ($user->hasRole('dealer')) {

            $dealerInfo = $user->dealerInfo;
            $dealerAddressShipping = $dealerInfo->shippingAddress;
            $dealerAddressMailing = $dealerInfo->billingAddress;
            $dealerContactMobile = $dealerInfo->dealerMobileContact;
            $dealerEmployment = $dealerInfo->employmentAddress;
            $dealerBank = $dealerInfo->dealerBankDetails;

            if ($dealerInfo && country()->country_id == 'MY') {
                if (!in_array('MY', $dealerInfo->dealership)) return redirect('/launch-package');
            }

            if (!$dealerInfo || $dealerInfo->full_name == null || $dealerInfo->nric == null || $dealerInfo->date_of_birth == null || $dealerInfo->gender_id == 0 ||
            $dealerInfo->race_id == 0 || $dealerInfo->marital_id == 0 || !$dealerAddressShipping || $dealerAddressShipping->address_1 == null ||  $dealerAddressShipping->city_key == null
            || $dealerAddressMailing->address_1 == null || $dealerAddressMailing->city_key == null || $dealerContactMobile->contact_num == null || !$dealerEmployment ||
            $dealerEmployment->employment_type == null || $dealerEmployment->company_name == null || $dealerEmployment->company_address_1 == null
            || $dealerEmployment->company_postcode == null || $dealerEmployment->company_state_id == '0' || $dealerEmployment->company_city_key == null || !$dealerBank  ||
            $dealerBank->bank_code == '' || $dealerBank->bank_acc == '0' || $dealerBank->bank_acc_name == '' || $dealerBank->account_regid == '') {
                $user->notes = 'dealer incomplete';
                $user->save();
                return redirect('/register-dealer/information');
            }

            if (!$userInfo || $userInfo->full_name == null || $userInfo->nric == null) {
                return redirect('/register-dealer/information');
            }
        }

        if ($user->hasRole('customer')) {
            if(!$userInfo || $userInfo->full_name == null || (($userInfo->passport_no == null) ? $userInfo->nric == null : false) || (($userInfo->nric == null) ? $userInfo->passport_no == null : false)
            || $userAddressShipping->address_1 == null ||  $userAddressShipping->city_key == null || $userAddressMailing->address_1 == null || $userAddressMailing->city_key == null || $userContactMobile->contact_num == null) {
                return redirect('/register-customer/information')->with(['incomplete' => true]);
            }
        }
        return $next($request);
    }
}
