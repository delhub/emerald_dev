@inject('controller', 'App\Http\Controllers\Warehouse\Location\LocationController')

@extends('layouts.administrator.main')

@section('content')
<h1>{{$controller::$header}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            @if(count($tables) > 0)
            <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        {{-- <th> Image </th> --}}
                        <th>Location Key</th>
                        <th>Location Name</th>
                        <th>Type</th>
                        <th>Country</th>
                        <th>Printer Email</th>
                        <th>Contact Number</th>
                        <th>Default Address</th>
                        <th>Post Code</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Operation Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($tables as $row)
                    <tr>
                        {{-- <td>
                            <img id="featuredPreview"
                            src="@if ($row->defaultImage) {{ asset('storage/' . $row->defaultImage->path . $row->defaultImage->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif"
                            alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                        </td> --}}
                        <td><b>{{$row->location_key }}</b></td>
                        <td>{{$row->location_name }}</td>
                        <td>
                            @switch($row->type )
                                @case('WH')
                                Warehouse
                                    @break
                                @case('SH')
                                Shop Outlet
                                    @break
                                @default
                            @endswitch
                        </td>
                        <td>{{$row->country->country_name }}</td>
                        <td>{{$row->printer_email }}</td>
                        <td>{{$row->contact_number }}</td>
                        <td>{{$row->default_address }}</td>
                        <td>{{$row->postcode }}</td>
                        <td>{{$row->city->city_name }}</td>
                        <td>{{$row->state->name }}</td>
                        <td>{{$row->operation_time }}</td>

                        <td>
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left" class="btn btn-primary shadow-sm"
                            href="{{ route($controller::$edit,['id' => $row->id]) }}" >Edit</a>

                            {{-- <form action="{{ route($controller::$destroy,['id' => $row->id]) }}" id="delete-form-id" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            @csrf
                                <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>
                            </form> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$tables->links()}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p>
            @endif
        </div>
    </div>
</div>
@endsection
