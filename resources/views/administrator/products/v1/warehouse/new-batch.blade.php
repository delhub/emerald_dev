@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Warehouse Batch</h1>
<div class="card shadow-sm">
    <div class="card-header">Warehouse Batch<small> - Add new</small>
        <div class="card-header-actions"><button class="btn btn-primary btn-sm" type="button" onclick="window.location.href = '{{ route('administrator.v1.products.warehouse.index')}}';">Batch List</button></div>
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-10">
                <form id="batch" action="{{ route('administrator.v1.products.warehouse.addBatch')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label" for="panel">Panel</label><small class="text-danger">*</small>
                        <select name="panel_id" class="form-control select2" required>
                            <option value="">Please choose a panel.</option>
                            @foreach($panels as $panel)
                            <option value="{{ $panel->account_id }}">{{ $panel->company_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="vendor_desc">Batch Type</label> <small class="text-danger">*</small>
                        <select name="batch_type" class="form-control select2 pc" required>
                            <option value="">Please choose a batch type.</option>
                            <option value="stock_in">Stock In</option>
                            <option value="qr_code_missing">QRC Missing</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="location_id">Location</label><small class="text-danger">*</small>
                        <select name="location_id" class="form-control select2" required>
                            @foreach($locations as $location)
                            {{-- <option value="{{ $location->id }}">{{ $location->location_key }} - {{ $location->location_name }} ({{ $location->default_address }})</option> --}}
                            <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="po_number">PO Number</label>
                        <input type="text" name="po_number" class="form-control" placeholder="PO Number" autocomplete="off" value="{{old('po_number')}}">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="inv_number">Invoice Number</label>
                        <input type="text" name="inv_number" class="form-control" placeholder="Invoice Number" autocomplete="off" value="{{old('inv_number')}}">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="inv_date">Invoice Date</label><small class="text-danger">*</small>
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                            <input type="text" name="inv_date" id="inv_date" class="form-control" placeholder="Invoice Date" autocomplete="off" value="{{old('inv_date')}}" data-parsley-error-message="This value is required." data-parsley-trigger="change" data-parsley-errors-container=".invdate-error-block" required>
                        </div>
                        <span class="invdate-error-block"></span>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="vendor_desc">Vendor Name</label>
                        <input type="text" name="vendor_desc" class="form-control" placeholder="Vendor Name" autocomplete="off" value="{{old('vendor_desc')}}">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="vendor_desc">Batch Notes</label>
                        <textarea name="notes" class="form-control" placeholder="Give Some Notes" autocomplete="off" value="{{old('notes')}}">{{old('notes')}}</textarea>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-12">
                          <label for="batch">Batch <span class="text-danger small">*(Naming cannot more than 12 character)</span></label>
                          <input type="text" name="batch" maxlength="12" class="form-control" placeholder="Batch" required>
                        </div>
                        {{-- <div class="form-group col-md-6">
                          <label for="lot">Lot</label>
                          <input type="text" name="lot" class="form-control" placeholder="Lot">
                        </div> --}}
                    </div>

                    <div class="form-group">
                        <label class="col-form-label" for="stock_in_date">Stock In Date</label><small class="text-danger">*</small>
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                            <input type="text" name="stock_in_date" id="stock_in_date" class="form-control" placeholder="Stock In Date" autocomplete="off" value="{{old('stock_in_date')}}" data-parsley-error-message="This value is required." data-parsley-trigger="change" data-parsley-errors-container="#stockin-error-block" required>
                        </div>
                        <span id="stockin-error-block"></span>
                    </div>
                    <hr>
                    <div class="form-row details" id="details">
                        <div class="form-group col-md-4">
                          <label for="product_code"><small>Product Code</small></label>
                          <select name="product_code[]" class="form-control select2 pc" required>
                            <option value="">Please choose product code.</option>
                                @foreach($products as $product)
                                <option value="{{ $product->product_code }}">{{ $product->product_code }} - {{ $product->product2->parentProduct->name }} ({{ $product->attribute_name }})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                          <label for="quantity"><small>Quantity</small></label>
                          <input type="text" name="quantity[]" class="form-control" placeholder="Quantity" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="quantity"><small>Repeatable</small></label>
                            <select name="repeatable[]" class="form-control select2 pc" required>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="quantity"><small>Lot</small></label>
                            <input type="text" name="lot[]" class="form-control" placeholder="Lot" required>
                        </div>
                        <div class="form-group col-md-2">
                          <label for="expiry_date"><small>Expiry Date</small></label>
                            <div class="input-group">
                                <input type="date" name="expiry_date[]" class="form-control expiry_date" placeholder="Expiry Date" data-parsley-error-message="" data-parsley-errors-container=".date-error-block" required>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger minus" onclick="removeBatchDetails(this);" disabled><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-success plus" onclick="cloneBatchDetails();"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            <span class="date-error-block"></span>
                        </div>
                      </div>
                    <hr>
                    <div class="form-group">
                        <p style="text-align: center;"><button class="btn btn-primary" type="submit" name="submit">Submit</button></p>
                    </div>
                </form>
            </div>



        </div>
    </div>
</div>
@endsection

@push('style')
<style>
    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da;
    }
    .select2-container .select2-selection--single {
        height: calc(1.6em + 0.75rem + 2px);
        padding: 0.375rem 0.75rem;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #595959;
        line-height: 20px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 5px;
        right: 3px;
    }
</style>
@endpush

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
<script>
    // Parsley plugin initialization with tweaks to style Parsley for Bootstrap 4
    $("#batch").parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    });

 // Parsley full doc is avalailable here : https://github.com/guillaumepotier/Parsley.js/
</script>
<script>
    $('#stock_in_date, #inv_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
    /* $('.expiry_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    }); */

    function removeBatchDetails(element) {
        // $("#details").remove("div.details:last");
        $(element).parent().parent().parent().parent().slideUp(300, function() {
            $(this).remove();
        });
        disableFirstMinus();
    }

    function cloneBatchDetails() {
        // $(".details:last").find('.pc').select2('destroy');
        $(".pc").select2('destroy');
        $("button.minus").attr("disabled", false);
        $("#details").clone().insertAfter("div.details:last");
        $("#details div > span").remove();
        disableFirstMinus();
        $('.pc').select2();
    }

    function disableFirstMinus() {
        $("button.minus:first").attr("disabled", true);
    }

    setTimeout(function() {
        new SlimSelect({
            select: '.slimselect'
        })
    }, 300)
</script>
@endpush
