<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Status;
use Illuminate\Support\Facades\DB;

class CreateWarehouseStockAdjustment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_stock_adjustment', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('location_id');
            $table->bigInteger('user');
            $table->string('product_code');
            $table->string('adjust_type');
            $table->integer('quantity');
            $table->text('remark');
            $table->timestamps();
        });


        $status = new Status;
        $status->id = 8017;
        $status->name = 'Stock Adjustment - IN';
        $status->description = 'Stock adjustment in type';
        $status->status_type = 'inventory_status';

        $status->save();

        $status = new Status;
        $status->id = 8018;
        $status->name = 'Stock Adjustment - OUT';
        $status->description = 'Stock adjustment out type';
        $status->status_type = 'inventory_status';

        $status->save();

        DB::statement("
        SELECT product_code, outlet_id,

        sum(case when inv_type = '8010' OR inv_type = '8000'OR inv_type = '8013'OR inv_type = '8014'OR inv_type = '8017'OR inv_type = '8018'
        then inv_amount
        else 0
        END)
        AS physical_stock,

        sum(case when inv_type = '8001'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved,

        sum(case when inv_type = '8011'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved_offline,

        sum(case when inv_type = '8012'
        then inv_amount
        else 0
        end)
        as pos_completed,

        sum(case when inv_type = '8013'
        then inv_amount
        else 0
        end)
        as total_in,

        sum(case when inv_type = '8014'
        then inv_amount
        else 0
        end)
        as total_out,

        sum(case when inv_type = '8017'
        then inv_amount
        else 0
        end)
        as total_adjustOut,

        sum(case when inv_type = '8018'
        then inv_amount
        else 0
        end)
        as total_adjustIn,

        sum(case when inv_type = '8000' OR inv_type = '8001' OR inv_type = '8011' OR inv_type = '8010' OR inv_type = '8012' OR inv_type = '8013'OR inv_type = '8014'OR inv_type = '8017'OR inv_type = '8018'
        then inv_amount
        else 0
        end)
        as virtual_stock

        FROM wh_inventories
        GROUP BY product_code, outlet_id
        ORDER BY outlet_id ASC
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_stock_adjustment');
    }
}
