<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateViewInventoryStockBundle18072022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock_bundle
        AS
        select `wh_inventories`.`product_code` AS `product_code`,`wh_inventories`.`outlet_id` AS `outlet_id`,`wh_order`.`delivery_order` AS `whDO`, `wh_inventories`.`created_at` AS createdAt,
        (case when isnull(`panel_attribute_bundle`.`active`) then 0 else 1 end) AS `bundle`,

        sum((case when ((`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8010') or (`wh_inventories`.`inv_type` = '8012') or (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014') or (`wh_inventories`.`inv_type` = '8017') or (`wh_inventories`.`inv_type` = '8018') or (`wh_inventories`.`inv_type` = '8019') or (`wh_inventories`.`inv_type` = '8020')) then `wh_inventories`.`inv_amount` else 0 end)) AS `physical_stock`,
        sum((case when (`wh_inventories`.`inv_type` = '8001') then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock_reserved`,
        sum((case when ((`wh_inventories`.`inv_type` = '8010') or (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014')) then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockIn`,
        sum((case when (`wh_inventories`.`inv_type` = '8011') then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock_reserved_offline`,
        sum((case when (`wh_inventories`.`inv_type` = '8012') then `wh_inventories`.`inv_amount` else 0 end)) AS `pos_completed`,
        sum((case when (`wh_inventories`.`inv_type` = '8013') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockTransferIn`,
        sum((case when (`wh_inventories`.`inv_type` = '8014') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_stockTransferOut`,
        sum((case when (`wh_inventories`.`inv_type` = '8017') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_adjustOut`,
        sum((case when (`wh_inventories`.`inv_type` = '8018') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_adjustIn`,
        sum((case when (`wh_inventories`.`inv_type` = '8019') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_b_bundle_add`,
        sum((case when (`wh_inventories`.`inv_type` = '8020') then `wh_inventories`.`inv_amount` else 0 end)) AS `total_b_bundle_minus`,
        sum((case when (`wh_inventories`.`inv_type` = '8000') then `wh_inventories`.`inv_amount` else 0 end)) AS `online_sold`,
        sum((case when ((`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8012')) then `wh_inventories`.`inv_amount` else 0 end)) AS `total_sold`,
        sum((case when ((`wh_inventories`.`inv_type` = '8000') or (`wh_inventories`.`inv_type` = '8010') or (`wh_inventories`.`inv_type` = '8012') or (`wh_inventories`.`inv_type` = '8013') or (`wh_inventories`.`inv_type` = '8014') or (`wh_inventories`.`inv_type` = '8017') or (`wh_inventories`.`inv_type` = '8018') or (`wh_inventories`.`inv_type` = '8019') or (`wh_inventories`.`inv_type` = '8020') or (`wh_inventories`.`inv_type` = '8001') or (`wh_inventories`.`inv_type` = '8011')) then `wh_inventories`.`inv_amount` else 0 end)) AS `virtual_stock`

        from ((`wh_inventories`
        join `wh_order` on((`wh_order`.`id` = `wh_inventories`.`model_id`)))
        left join `panel_attribute_bundle` on(((`panel_attribute_bundle`.`product_code` = `wh_inventories`.`product_code`) and (`panel_attribute_bundle`.`active` in (1,2)))))

        where (((`wh_inventories`.`outlet_id` <> '999') or ((`wh_inventories`.`outlet_id` = '999') and (`wh_inventories`.`model_type` = 'AppModelsWarehouseOrderWarehouseOrder')))
        and (`wh_inventories`.`created_at` > '2021-10-01'))

        group by `wh_inventories`.`product_code`,`wh_inventories`.`outlet_id`,`panel_attribute_bundle`.`active`,`wh_order`.`delivery_order`,`wh_order`.`id` ,  `wh_inventories`.`created_at`
        order by `wh_inventories`.`outlet_id`
    ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
