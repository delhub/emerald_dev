<?php

namespace App\Http\Middleware\Authentication;

use App\Models\Users\User;
use Auth;
use Closure;

class DealershipPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $dealership)
    {



        $user = Auth::user();

        //if ($user->hasRole([ 'administrator', 'management']))   return $next($request);

        if (!$user->hasRole(['dealer'])) return abort(403, 'Unauthorized');

        $dealership = is_array($dealership) ? $dealership : explode('|', $dealership);

        if (!in_array($dealership, ['default'])) $dealership = array(country()->country_id);

        $user_dealership = $user->dealerInfo->dealership;

        if( ! array_intersect ( $user_dealership, $dealership ) ) return abort(403, 'Unauthorized');

        return $next($request);
    }
}
