<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAcdView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        \DB::statement("
        CREATE OR REPLACE VIEW `view_agent_archimedes_project_data` AS
        SELECT
        p.purchase_number,
        p.user_id,
        p.created_at,
        p.purchase_date,
        p.ship_full_name,
        u.referrer_id,
        u.full_name,
        i.product_id,
        i.product_code,
        i.quantity,
        i.delivery_order,
        i.item_order_status,
        i.id AS item_table_id,
        o.panel_id,
        g.name_slug,
        g.id AS global_product_id,
        g.name AS product_name,
        i.prebuy_redemption AS prebuy_status,
        ds.prebuy_redemption_id

        FROM items i

        LEFT JOIN dealer_stockledger ds ON ds.item_id = i.id
        JOIN orders o ON o.order_number = i.order_number
        JOIN purchases p ON p.id = o.purchase_id
        JOIN user_infos u ON u.user_id = p.user_id
        JOIN panel_attribute_bundle b ON b.primary_product_code = i.product_code
        JOIN panel_product_attributes a ON a.product_code = b.product_code
        JOIN panel_products d ON d.id = a.panel_product_id
        JOIN global_products g ON g.id = d.global_product_id

        WHERE i.item_order_status IN (1001,1008,1009,1011)
        AND g.product_type = 'vqs'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
