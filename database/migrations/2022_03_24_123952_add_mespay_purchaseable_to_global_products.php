<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMespayPurchaseableToGlobalProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->tinyInteger('mespay_purchaseable')->default(1)->after('bluepoint_purchaseable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->dropColumn('mespay_purchaseable');
        });
    }
}
