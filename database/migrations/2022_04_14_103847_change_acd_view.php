<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeAcdView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("
        CREATE OR REPLACE VIEW `view_agent_archimedes_project_data` AS
        select DISTINCT `p`.`purchase_number` AS `purchase_number`,`p`.`user_id` AS `user_id`,`p`.`created_at` AS `created_at`,`p`.`purchase_date` AS `purchase_date`,`p`.`ship_full_name` AS `ship_full_name`,`u`.`referrer_id` AS `referrer_id`,`u`.`full_name` AS `full_name`,`i`.`id` AS `item_table_id`,`i`.`product_id` AS `product_id`,`i`.`product_code` AS `product_code`,`i`.`quantity` AS `quantity`,`i`.`subtotal_price` AS `subtotal_price`,`i`.`delivery_order` AS `delivery_order`,`i`.`item_order_status` AS `item_order_status`,`i`.`prebuy_redemption` AS `prebuy_redemption`,`o`.`panel_id` AS `panel_id`,`g`.`name_slug` AS `name_slug`,`g`.`id` AS `global_product_id`,`g`.`name` AS `product_name` from (((((((`items` `i` join `orders` `o` on((`o`.`order_number` = `i`.`order_number`))) join `purchases` `p` on((`p`.`id` = `o`.`purchase_id`))) join `user_infos` `u` on((`u`.`user_id` = `p`.`user_id`))) join `panel_attribute_bundle` `b` on((`b`.`primary_product_code` = `i`.`product_code`))) join `panel_product_attributes` `a` on((`a`.`product_code` = `b`.`product_code`))) join `panel_products` `d` on((`d`.`id` = `a`.`panel_product_id`))) join `global_products` `g` on((`g`.`id` = `d`.`global_product_id`))) where ((`i`.`item_order_status` in (1001,1008,1009,1011)) and (`g`.`product_type` = 'vqs'))
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
