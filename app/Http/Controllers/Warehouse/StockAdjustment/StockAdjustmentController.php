<?php

namespace App\Http\Controllers\Warehouse\StockAdjustment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Models\Warehouse\Location\Wh_location;
use App\Models\Warehouse\StockTransfer\StockTransfer;
use Illuminate\Support\Facades\Auth;
use App\Models\Users\User;
use App\Models\Warehouse\StockAdjustment\StockAdjustment;
use App\Models\Products\ProductAttribute;
use App\Models\Purchases\Order;
use App\Models\Products\ViewInventoriesStock;

class StockAdjustmentController extends Controller
{
    public static $home;
    public static $header;
    public static $transferStart;

    public static $headerTransferList;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        // self::$home = 'warehouse.location.index';
        self::$header = 'Stock Adjustment';

        self::$transferStart = 'warehouse.stockAdjustment.adjustStart';

        self::$headerTransferList = 'Stock Adjustment List';


        // self::$create = 'warehouse.location.create';
        // self::$store = 'warehouse.location.store';

        // self::$edit = 'warehouse.location.edit';
        // self::$update = 'warehouse.location.update';

        // self::$destroy = 'warehouse.location.destroy';

        // link to view page
        // view('warehouse.location.index');
    }

    public function index()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $tables = StockAdjustment::selectRaw('wh_stock_adjustment.*')
            ->orderBy('created_at', 'ASC')
            ->get();

        if (!$user->hasRole('wh_supervisor')) $tables = $tables->whereIn('location_id', $userLocationId);

        $tranfer = array();
        return view('warehouse.StockAdjustment.index', compact('tables', 'tranfer'));
    }

    public function adjust()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        $locations = wh_location::get();
        if (!$user->hasRole('wh_supervisor')) $locations = $locations->whereIn('id', $userLocationId);

        $products = ProductAttribute::selectRaw('panel_product_attributes.*')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->orderBy('panel_product_attributes.product_code', 'ASC')
            ->where('panel_product_attributes.active', 1)
            ->where('global_products.product_status', 1)
            ->get();

        return view('warehouse.StockAdjustment.start', compact('tables', 'user', 'locations', 'products'));
    }

    public function adjustStart(Request $request)
    {
        $request->validate([
            'product_code' => ['required'],
            'adjustType' => ['required'],
            'quantity' => ['required'],
            'remark' => ['required'],
        ]);

        $minus = '';
        $outlet_id = $request->input('location_id');
        $productCode = $request->input('product_code');
        $adjustType = $request->input('adjustType');
        $quantity = $request->input('quantity');
        $remark = $request->input('remark');



        if ($adjustType == 8018) {
            //checking if adjust quantity is more than totalInPickPack
            $minus = '-';
            $inventoryStock = ViewInventoriesStock::where('product_code', $productCode)->where('outlet_id', $outlet_id)->first();

            if ($inventoryStock == NULL) {
                $locationName = Wh_location::find($outlet_id);
                return back()->with('error', $productCode . ' not in ' . $locationName->location_name);
            }

            $ordersToCheck = Order::selectRaw('orders.*,orders.created_at, orders.order_number,orders.order_status,orders.pick_pack_status,orders.id,
            global_products.no_stockProduct,
            wh_order.location_id,wh_order.date,
            items.quantity
            ')
                ->join('wh_order', 'wh_order.delivery_order', '=', 'orders.delivery_order')
                ->join('items', 'orders.order_number', '=', 'items.order_number')
                ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
                ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')

                ->where('global_products.no_stockProduct', 0)
                ->whereIn('wh_order.location_id', [$outlet_id])
                ->whereIn('orders.pick_pack_status', [1, 2, 3, 4, 5])
                ->where('items.product_code', $productCode)

                ->orderBy('orders.pick_pack_status', 'DESC')
                ->orderBy('wh_order.date', 'ASC')
                ->orderBy('orders.order_status', 'DESC')
                ->groupBy('orders.id')
                ->get();

            $totalInPickPack = 0;
            foreach ($ordersToCheck as $key => $checked) $totalInPickPack += $checked->quantity;

            $balanceCanTransfer = $inventoryStock->physical_stock - $totalInPickPack;

            if ($quantity > $balanceCanTransfer) {
                return back()->with('error', 'Fail To Adjust Product Code : ' . $productCode . ' , Balance : ' . $balanceCanTransfer . ' ');
            }
        }

        //no problem then can proceed adjustment process
        $stockAdjustment = new StockAdjustment;
        $stockAdjustment->location_id = $outlet_id;
        $stockAdjustment->user = $request->input('user');
        $stockAdjustment->product_code = $productCode;
        $stockAdjustment->adjust_type = $adjustType;
        $stockAdjustment->quantity = $minus . $quantity;
        $stockAdjustment->remark = $remark;

        $stockAdjustment->save();

        $inventoryRecords[$productCode]['outlet_id'] = $outlet_id;
        $inventoryRecords[$productCode]['inv_type'] = $adjustType;
        $inventoryRecords[$productCode]['inv_amount'] = $minus . $quantity;
        $inventoryRecords[$productCode]['inv_user'] = 0;
        $inventoryRecords[$productCode]['model_type'] = StockAdjustment::class;;
        $inventoryRecords[$productCode]['model_id'] = $stockAdjustment->id;
        $inventoryRecords[$productCode]['product_code'] = $productCode;

        InventoryController::insertData($inventoryRecords);

        return back()->with('success', 'Success adjust Product Code : ' . $productCode . '');
    }
}
