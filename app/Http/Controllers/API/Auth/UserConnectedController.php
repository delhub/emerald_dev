<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Auth\ApiAuthController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Register\CustomerRegisterController;
use Illuminate\Http\Request;

use App\Models\Users\User;
use App\Models\Users\UserConnectedPlatform;
use App\Models\Users\UserInfo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Passport\ClientRepository;
use Illuminate\Support\Facades\DB;

class UserConnectedController extends Controller
{
    public function disconnect(Request $request){

        $data = $request->all();

        $connectedplatformUser = UserConnectedPlatform::where('email', $data['email'])
            ->where('platform_key', $data['platform_key']);

        // Check if any records match the given conditions
        if ($connectedplatformUser->doesntExist()) {
            return response()->json([
                'success' => false,
                'message' => 'No matching records found for the provided email and platform key.'
            ], 404);
        }

        // Delete the matching records
        $connectedplatformUser->delete();

        return response()->json([
            'success' => true,
            'message' => 'User disconnected successfully'
        ]);

    }

    public function reconnect(Request $request){
        $user = User::where('email', $request->email)->first();
        return (new ApiAuthController)->platformConnect($request, $user, true);
    }
}
