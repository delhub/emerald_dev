@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light"> Users / </span> List
    </h4>
<div class="card shadow-sm">
    
    <div class="card-body">
        <!-- <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="#" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Coming Soon</a>
                </div>
            </div>
        </div> -->

        <form action="{{ route('administrator.user') }}" method="GET">

            <div class="offset-md-3 d-flex" style="float:right;">
                <div class="" style="width: 15rem;">
                    <!-- Dropdown for selecting filter -->
                    <select class="custom-select" id="selectSearch" name="selectSearch" onchange="toggleInput()">
                        <option value="ALL" selected>Choose...</option>
                        <option value="name" {{ $request->selectSearch == 'name' ? 'selected' : '' }}>Name</option>
                        <option value="email" {{ $request->selectSearch == 'email' ? 'selected' : '' }}>Email Address</option>
                        <option value="account_type" {{ $request->selectSearch == 'account_type' ? 'selected' : '' }}>Roles</option>
                    </select>
                    <!-- Input field for search -->
                    <input type="text" class="form-control mt-2" id="searchBox" name="searchBox"
                        value="{{ ($request->searchBox) ? $request->searchBox : '' }}" 
                        style="display: {{ $request->selectSearch == 'account_type' ? 'none' : 'block' }};">

                    <!-- Select box for roles -->
                    <select class="custom-select mt-2" id="rolesBox" name="rolesBox" 
                        style="display: {{ $request->selectSearch == 'account_type' ? 'block' : 'none' }};">
                        <option value="">Select a Role</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->name }}"
                                 {{ ( $request->rolesBox == $role->name ) ? 'selected' : '' }}>{{ ucfirst($role->name) }}</option>
                            @endforeach
                    </select>
                </div>
                <div style="text-align:center;">
                    <div class="col-md-auto px-1">
                        <button type="submit" class="btn btn-warning" style="color:black;width:100%;"><a href={{ route('administrator.user') }} style="color:black;">Reset Filter</a></button>
                        <button type="submit" class="btn btn-warning mt-2" style="color:black; width:100%;">Search</button>
                    </div>
                </div>
            </div>
        </form>

        <div class="col-md-4 offset-md-4" style="float:right;">&nbsp;</div>
        <h2>Users Management</h2>
        <!-- <div class="col-5 mb-3">
            <p style="text-align:left;"><span style="font-size:20px;"> Total Users : <b>{{ count($forCountUsers)}}</b></span></p>
        </div> -->

        <div class="table-responsive m-2">
            <table id="users-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr style="font-weight: bold;">
                        <td style="width: 1%;">No.</td>
                        <td >Email Address</td>
                        <td >Roles</td>
                        <td style="width: 18%;">More Info</td>
                        <td>Action</td>                       
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $loop->iteration }}
                        </td>
                        <td>
                            <span class="d-inline-block mb-2">
                                {{ $user->email }} 
                                <br>
                                @if($user->userInfo)                                
                                    ({{ $user->userInfo->full_name }})
                                @endif
                            </span>                           
                        </td>
                        <td>
                            <!-- <ul> -->
                                @foreach($user->roles as $role)
                                <!-- <li class="text-capitalize mb-2"> -->
                                    <span class="badge badge-success">
                                        {{ $role->name }}
                                    </span>
                                <!-- </li> -->
                                @endforeach
                            </ul>
                        </td>
                        {{--<td>
                            @if($user->hasRole('customer'))
                            <button type="button" class="btn btn-secondary mb-2" data-toggle="modal" data-target="#customerInfoModal{{ $user->id }}">
                                Customer Information
                            </button>

                            <br>
                            @endif
                            @if($user->hasRole('dealer'))
                            <button class="btn btn-light mb-2" data-toggle="modal" data-target="#dealerInfoModal{{ $user->id }}">
                                Agent Information
                            </button>
                            <br>
                                @if(isset($user->dealerInfo->dealerBankDetails))
                                    <button class="btn btn-secondary mb-2" data-toggle="modal" data-target="#bankDetailsModal{{ $user->id }}">
                                        Bank Details
                                    </button>
                                    <br>
                                @endif
                            @endif
                            @if($user->hasRole('panel'))
                            <button class="btn btn-dark mb-2" data-toggle="modal" data-target="#panelInfoModal{{ $user->id }}">
                                Panel Information
                            </button>

                            <br>
                            @endif
                        </td>--}}
                        <td class="text-center">
                            <a class="btn btn-info" href="{{ route('administrator.users.show',$user->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('administrator.users.edit',$user->id) }}">Edit</a>
                        </td>
                        <td class="pos-rel">
                            <div class="form-switch">
                                <input class="form-check-input" type="checkbox" id="allow_offline{{ $user->id }}" data-user-id="{{ $user->id }}"
                                onclick="submit_setting(this, 'allow_offline')"
                                @if (($user->userInfo) && ($user->userInfo->acc_code == 'bypass')) checked @endif>
                                <label class="form-check-label" for="allow_offline">Allow Offline Payment</label>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-md-4-md-12" style="float:center;">
            {!! $users->render() !!}
        </div>

    </div>
</div>


@endsection

@push('style')
<style>
    /*  */
    td.pos-rel {
        position: relative;
    }
    .form-switch {
        padding-left: 2.5em;
    }
    .form-switch .form-check-input:checked {
        background-position: right center;
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='%23fff'/%3e%3c/svg%3e");
    }
    .form-switch .form-check-input {
        width: 2em;
        margin-left: -2.5em;
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='rgba%280, 0, 0, 0.25%29'/%3e%3c/svg%3e");
        background-position: left center;
        border-radius: 2em;
        transition: background-position .15s ease-in-out;
    }
    .form-check-input:checked {
        background-color: #0d6efd;
        border-color: #0d6efd;
    }
    .form-check-input {
        width: 1em;
        height: 1em;
        margin-top: 0.25em;
        vertical-align: top;
        background-color: #fff;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        border: 1px solid rgba(0,0,0,.25);
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        -webkit-print-color-adjust: exact;
        color-adjust: exact;
    }
</style>
@endpush

{{-- @push('script')
<script>
    $(document).ready(function() {
        $('#users-table').DataTable();
    });
</script>
@endpush --}}

<script>
    function changeSwitchLabel(id) {
        status = $('.form-switch #'+id).is(':checked')
        return (status == 'true') ? true : false;
    }

    function submit_setting(data, process) {
        var userID = $(data).attr('data-user-id');
        var id = $(data).attr('id');
        var switchData = $('.form-switch #'+id).is(':checked')

        console.log('switchData', switchData);

        $.ajax({
            url: '{{ route('administrator.update.user') }}',
            data: {
                _token: '{{ csrf_token() }}',
                id: userID,
                process: process,
                switchData: switchData,

            },
            method: 'POST',
            beforeSend: function() {
                $("#white-overlay").show();
            },
            complete: function() {
                $("#white-overlay").hide();
            },
            success: function(result) {
                toastr.options = {
                    "closeButton": true,
                    "progressBar": true
                }
                toastr.success("Successful Update");
            },
            error: function(result, status, message) {
                toastr.options = {
                    "closeButton": true,
                    "progressBar": true
                }

                messaged = (typeof  result.responseJSON != 'undefined' && result.responseJSON.message) ? result.responseJSON.message : message;
                toastr.error('Error: ' + messaged);
                $(".form-switch #" + id).prop('checked', !changeSwitchLabel(id));
            }
        })
    }
</script>