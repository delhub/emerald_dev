<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@200;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/monthly-statement.css') }}">
     @include('layouts.favicon')
    <title>Document</title>
</head>

<body>
    <div class="page-size-a4">
        <div class="inner-page">
            <table class="header">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr class="cpn-address">
                                    <td width="35%">
                                        <img class="cpn-logo" src="{{ asset('images/formula/post-img/logo.png') }}">
                                    </td>
                                    <td width="65%">
                                        <p>Formula Healthcare Sdn. Bhd.<span>(1318429-V)</p>
                                        <p>1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                        <p>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                        <table>
                                            <tr>
                                                <td>
                                                    <p class="cpn-email">
                                                        <img
                                                            src="{{ asset('images/formula/post-img/do-c-icon1.png') }}">
                                                        formula-cs@delhubdigital.com
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="cpn-weblink">
                                                        <img
                                                            src="{{ asset('images/formula/post-img/do-c-icon3.png') }}">
                                                        formula2u.com
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="page-title">
                                        <h4>MONTHLY STATEMENT</h4>
                                        <hr class="dbl-line">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="user-info">
                        <td width='60%'>
                            <p>
                                Tan Cheng Lok
                            </p>
                            <p>
                                012-3456789
                            </p>
                            <p>
                                1-26-05 Menara Bangkok Bank,
                            </p>
                            <p>
                                Berjaya Central Park,
                            </p>
                            <p>
                                No 105, Jalan Ampang,
                            </p>
                            <p>
                                50450 Kuala Lumpur.
                            </p>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p>
                                            Date
                                        </p>
                                    </td>
                                    <td>
                                        <span>: 01/06/2021</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Business Month
                                        </p>
                                    </td>
                                    <td>
                                        <span>: June 2021</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Agent Code
                                        </p>
                                    </td>
                                    <td>
                                        <span>: 1xxxxxxxxx</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Grade
                                        </p>
                                    </td>
                                    <td>
                                        <span>: AD</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="body">
                <tbody>
                    <tr class="tit-bg">
                        <td colspan="4">
                            <h4>EXECUTIVE SUMMARY</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Personal Sales</p>
                        </td>
                        <td>
                            <p>RM 10,800.00</p>
                        </td>
                        <td>
                            <p>Total Revenue</p>
                        </td>
                        <td>
                            <p>RM 1,800.00</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Group Sales</p>
                        </td>
                        <td>
                            <p>RM 200,800.00</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Performance Grade</p>
                        </td>
                        <td>
                            <p>A</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="spc-line">
                        <td colspan="4">
                            <h4>REVENUE BREAKDOWN</h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Personal Revenue</p>
                        </td>
                        <td>
                            <p>RM 450.00</p>
                        </td>
                        <td>
                            <p>Personal Order</p>
                        </td>
                        <td>
                            <p>6</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Group Revenue</p>
                        </td>
                        <td>
                            <p>RM 550.00</p>
                        </td>
                        <td>
                            <p>Group Order</p>
                        </td>
                        <td>
                            <p>24</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Business Development</p>
                        </td>
                        <td>
                            <p>RM 700.00</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Performance Bonus</p>
                        </td>
                        <td>
                            <p>RM 0.00</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr class="line-20">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="pd-l20">Total Revenue</p>
                        </td>
                        <td>
                            <p>RM 1,800.00</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="spc-line2">
                        <td colspan="4">
                            <h4>REVENUE BREAKDOWN</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table class="total-price">
                                <tbody>
                                    <tr class="line-bot-top">
                                        <th>Date</th>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th>Total Price (RM)</th>
                                        <th>SV</th>
                                    </tr>
                                    <tr class="line-bot">
                                        <td>
                                            31/06/2021
                                        </td>
                                        <td>
                                            La Roche Posay Effaclar Foaming Gel Cleanser 200ml
                                        </td>
                                        <td class="text-c">
                                            1
                                        </td>
                                        <td class="text-c">
                                            73.90
                                        </td>
                                        <td class="text-c bg-light">
                                            400
                                        </td>
                                    </tr>
                                    <tr class="line-bot">
                                        <td>
                                            24/06/2021
                                        </td>
                                        <td>
                                            Vitahealth Bilberry Plus
                                        </td>
                                        <td class="text-c">
                                            2
                                        </td>
                                        <td class="text-c">
                                            73.90
                                        </td>
                                        <td class="text-c bg-light">
                                            200
                                        </td>
                                    </tr>
                                    <tr class="line-bot">
                                        <td>
                                            20/06/2021
                                        </td>
                                        <td>
                                            Swisse Ultiboost Vitamin D3
                                        </td>
                                        <td class="text-c">
                                            2
                                        </td>
                                        <td class="text-c">
                                            40.90
                                        </td>
                                        <td class="text-c bg-light">
                                            350
                                        </td>
                                    </tr>
                                    <tr class="line-bot">
                                        <td>
                                            16/06/2021
                                        </td>
                                        <td>
                                            DermaVeen Daily Nourish Soap Free Wash
                                        </td>
                                        <td class="text-c">
                                            3
                                        </td>
                                        <td class="text-c">
                                            56.90
                                        </td>
                                        <td class="text-c bg-light">
                                            100
                                        </td>
                                    </tr>
                                    <tr class="line-bot">
                                        <td class="remove-line">

                                        </td>
                                        <td class="text-r">
                                            Total
                                        </td>
                                        <td class="text-c">
                                            6
                                        </td>
                                        <td class="text-c">
                                            245.60
                                        </td>
                                        <td class="text-c bg-dark">
                                            1,100
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="footer">
                <tbody>
                    <tr>
                        <td>
                            <hr class="dbl-line">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                This is computer generated statement no signature required.
                                The company must be notified of any discrepanct from the statement within 7 days upon
                                received,
                                otherwise the information of the statement will be considered accurate.
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
