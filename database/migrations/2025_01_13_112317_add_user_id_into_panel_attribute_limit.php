<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdIntoPanelAttributeLimit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('panel_attribute_limit', function (Blueprint $table) {
            $table->bigInteger('user_id')->after('amount')->default(-1)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('panel_attribute_limit', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
