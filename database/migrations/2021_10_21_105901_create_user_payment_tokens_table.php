<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPaymentTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payment_tokens', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('token_id');
            $table->string('card_number');
            $table->string('card_type',3);
            $table->timestamps();
        });
        Schema::dropIfExists('user_payment_info');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payment_tokens');
    }
}
