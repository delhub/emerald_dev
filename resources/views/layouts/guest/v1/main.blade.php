<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!--Validate register fields -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/register-page.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}?v=3.5" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    @stack('style')

    <style>
        .bg-md {
            background-image: url(/images/Bujishu_Landing_Page_Desktop_{{ $country }}.jpg);
            background-size: cover;

        }

        @media (max-width: 768px) {
            .bg-md {
                background: url(/images/Bujishu_Landing_Page_Desktop_{{ $country }}.jpg)no-repeat 51% 17%;
                background-size: 315%;

            }
        }

        /* @media (max-width: 576px) {
            .bg-sm {
                background-image: url(/images/Bujishu_Landing_Page_Desktop_{{ $country }}.jpg);
            }
        } */

        @media (max-width: 414px) {
            .bg-md {
                background: url(/images/Bujishu_Landing_Page_Desktop_{{ $country }}.jpg)no-repeat 61% 28%;
                background-size: cover;
            }

            /* .bg-sm {
                background-image: url(/images/bujishu-home-sm-{{ $country }}.jpg);
            } */
        }

    </style>
</head>

<body>
    @include('layouts.guest.v1.navigation.navigation')
    <main class="h-100">
        @yield('content')
    </main>
    @stack('script')
</body>

</html>
