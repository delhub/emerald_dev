<?php

namespace App\Models\Discount;

use Illuminate\Database\Eloquent\Model;

class Usage extends Model
{
    // Table Name
    protected $table = 'discount_retail_usage';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestemps = true;

}
