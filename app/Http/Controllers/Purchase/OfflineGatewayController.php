<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Models\Purchases\Payment;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use Illuminate\Support\Facades\Cookie;
use App\Models\Globals\Countries;
use App\Models\Purchases\RBS;
use App\Models\Products\ProductAttribute;

class OfflineGatewayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    // public function submitForm($request, $purchase, $user, $paymentOption, $cookie)
    public function submitForm($request, $purchase, $user, $paymentOption)
    {

        // dd($request, $purchase, $user, $paymentOption);
        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        $unique_id =  uniqid();

        $payment = new Payment;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->purchase_uuid = $purchase->purchase_number . '-' . $unique_id;
        $payment->gateway_response_code = 'PENDING';
        $payment->amount = $purchase->purchase_amount;
        $payment->point_amount = $purchase->point_amount;
        $payment->local_amount = $purchase->purchase_amount / $conversion_rate_markup;
        $payment->gateway_string_result = '-';
        $payment->auth_code = '-';
        $payment->last_4_card_number = '-';
        $payment->expiry_date = '-';
        $payment->gateway_eci = '-';
        $payment->gateway_security_key_res = '-';
        $payment->gateway_hash = '-';
        $payment->save();

        $paymentGatewayResponse = new PaymentGatewayController;

        if ($payment->point_amount != 0) {

            $points = array();
            $points['point_type'] = 'debit';
            $points['transaction_type'] = 'purchase';
            $points['status'] = 'pending';
            $points['balance'] = $user->userInfo->user_points - $payment->point_amount;

            $paymentGatewayResponse->saveUserPoints($payment,$points);
        }

       $this->processOfflinePayment($request, $purchase, $payment);

        // $paymentGatewayResponse->updateGroupSales($user, $purchase);

        foreach ($purchase->orders as $order) {
            $order->order_status = 1005;
            foreach ($order->items as $item) {
                $item->item_order_status = 1005;
                $item->save();
                $attributes = ProductAttribute::where('product_code', $item->product_code)->first();
                $currentSold = $attributes->sales_count;
                $attributes->sales_count = $currentSold + $item->quantity;
                $attributes->save();
            }

            $order->save();
        }

        // $check_registration = true;

        $purchase->increment('invoice_version');

        // $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration, $paymentOption,false );

        // GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);

        $gateway = 'offline';
        $transction_id = $purchase->reference_number;
        $request = $paymentGatewayResponse->sendToMarketingServer($user->userInfo->account_id, $purchase->getFormattedNumber(),  $purchase, $purchase->purchase_amount / 100, $gateway, $transction_id);
        // TODO: Move this to OfflinePaymentController later!
        // Admin is supposed to verify offline payment first before it is labeled success.
        // Temporary solution for deadlines on 17/04/2020.
        return $paymentGatewayResponse->markOrderComplete($purchase, $user, $cookie = null);
    }

    public function processOfflinePayment($request, $purchase, $payment)
    {
         $request;
        $file = $request->file('upload');
        $purchaseNumberFormatted = $purchase->getFormattedNumber();
         $fileExtension = $file->getClientOriginalExtension();
        $fileName = $purchaseNumberFormatted . '-payment' . '.' . $fileExtension;
        $destinationPath = public_path('/storage/uploads/images/users/' . $purchase->user->userInfo->account_id . '/payments');

        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        if (!File::isDirectory($destinationPath)) {
            File::makeDirectory($destinationPath, 0777, true);
        }
        $file->move($destinationPath, $fileName);

        $purchase->payment_proof =
            '/storage/uploads/images/users/' . $purchase->user->userInfo->account_id . '/payments' . '/' . $fileName;

        $purchase->offline_reference = $request->input('reference_number');
        $purchase->offline_payment_amount = $request->input('finalTotal');

        $purchase->purchase_status = 4000; // Pending Review - Offline

        $purchase->save();

        // $rbs = RBS::where('purchases_id',$purchase->id)->where('rbs_status','pending')->first();
        // if (isset($rbs)) {
        //     $rbs->rbs_status = 'active';
        //     $rbs->save();
        // }



        if ($purchase->installment_data) {
            $payment_amount = $purchase->installment_data->totalpayment * 100;
        } else {
            $payment_amount = $purchase->purchase_amount;
        };

        // TODO: Move this to OfflinePaymentController later!
        // Admin is supposed to verify offline payment first before it is labeled success.
        // Temporary solution for deadlines on 17/04/2020.


        $payment->gateway_response_code = '00';
        $payment->transaction_id = uniqid();
        $payment->auth_code = $request->input('reference_number');
        $payment->country_id = country()->country_id;
        $payment->save();
    }



    public function check_hash()
    {
        return;
    }

    public function check_duplicate()
    {
        return;
    }

    public function check_status()
    {
        return;
    }
}
