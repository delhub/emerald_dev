@inject('controller', 'App\Http\Controllers\Warehouse\StockTransfer\StockTransferController')

@extends('layouts.administrator.main')

@section('content')

<h1>{{$controller::$headerTransferList}}</h1>
<div class="card shadow-sm">
    <div class="card-body">

        <form action="{{ route('warehouse.StockTransfer.stockTransferList') }}" method="GET">
            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="row mt-2 ml-2">
                    <div class="col-md-auto px-4">
                        <select class="custom-select" id="from_location" name="from_location">
                                <option value="0" selected>All From Location...</option>
                            @foreach ($locations as $location)
                                <option value="{{$location->id}}" {{ $request->from_location == $location->id ? 'selected' : '' }}>{{$location->location_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-auto px-1">
                        <select class="custom-select" id="to_location" name="to_location">
                                <option value="0" selected>All To Location...</option>
                            @foreach ($toLocations as $locationb)
                                <option value="{{$locationb->id}}" {{ $request->to_location == $locationb->id ? 'selected' : '' }}>{{$locationb->location_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="row mt-2 ml-2">
                    <div class="col-md-auto px-5">
                        <select class="custom-select" id="transfer_type" name="transfer_type">
                            <option value="" selected>All Type...</option>
                            <option value="1" {{ $request->transfer_type == 1 ? 'selected' : '' }}>Auto</option>
                            <option value="2" {{ $request->transfer_type == 2 ? 'selected' : '' }}>Direct Transfer</option>
                            <option value="3" {{ $request->transfer_type == 3 ? 'selected' : '' }}>Batch Transfer</option>
                        </select>
                    </div>

                    <div class="col-md-auto px-1">
                        <select class="custom-select" id="transfer_status" name="transfer_status">
                            <option value="" selected>All status...</option>
                            <option value="-1" {{ $request->transfer_status == -1 ? 'selected' : '' }}>Canceled</option>
                            <option value="1" {{ $request->transfer_status == 1 ? 'selected' : '' }}>Transfering start</option>
                            <option value="3" {{ $request->transfer_status == 3 ? 'selected' : '' }}>Transfer Done</option>
                            <option value="4" {{ $request->transfer_status == 4 ? 'selected' : '' }}>Receiving Start</option>
                            <option value="5" {{ $request->transfer_status == 5 ? 'selected' : '' }}>Completed</option>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>

            <div class="col-md-4 offset-md-4" style="float:right;">
                <div style="text-align:center;">
                    <div class="col-md-auto px-1">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                        <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('warehouse.StockTransfer.stockTransferList') }} style="color:black;">Reset Filter</a></button>
                    </div>
                </div>
            </div>
        </form>
        {{$tables->links()}}
        <br>
        <br>
        <br>

        <div class="table-responsive m-2">
            @if(count($tables) > 0)

                <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User Transfer</th>
                            <th>User Receive</th>
                            <th>From Location</th>
                            <th>To Location</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                            <th>Transfer Type</th>
                            <th>Transfer Status</th>
                            <th>Transfer Item</th>
                            <th>Transfer Date</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($tables as $row)
                        <tr>
                            <td style="width:2%">{{$row->id}}</td>
                            <td style="width:2%">{{$row->theUser->userInfo->full_name}}</td>
                            <td style="width:2%">{{($row->theUserReceive) ? $row->theUserReceive->userInfo->full_name : '-'}}</td>
                            <td style="width:1%">{{$row->fromLocation->location_key}} - <b>{{$row->fromLocation->location_name}}</b><br>({{$row->fromLocation->default_address}})</td>
                            <td style="width:1%">{{$row->toLocation->location_key}} - <b>{{$row->toLocation->location_name}}</b><br>({{$row->toLocation->default_address}})</td>
                            <td style="width:2%">{{$row->session_start_time}}</td>
                            <td style="width:2%">{{$row->session_end_time}}</td>
                            <td style="width:2%"> {{showWarehouseTransferType($row->type)}}<br><b>{{($row->transferBox) ? 'Box : ('.$row->transferBox->box_name.')' : ''}}</b></td>
                            <td style="width:2%"> {{showWarehouseTransferstatus($row->status)}}</td>

                            <td style="width:60%">
                                @php
                                $tranfer = array();
                                    foreach ($row->stockTransferItems as $item){
                                        if(!isset($tranfer[$item->batchItem->batchDetail->product_code])) {
                                            $tranfer[$item->batchItem->batchDetail->product_code]['qty'] =0;
                                            $tranfer[$item->batchItem->batchDetail->product_code]['productName'] = $item->batchItem->batchDetail->productAttribute->product2->parentProduct->name;

                                        }
                                        $tranfer[$item->batchItem->batchDetail->product_code]['qty'] ++;
                                    }

                                    foreach ($tranfer as $productCode => $item) {
                                        echo '
                                            Transferred : <b>'.$item['qty'].'</b> - <b>'.$productCode. '</b> ('.$item['productName'].')
                                            <br>
                                        ';
                                    }
                                    unset($tranfer)
                                @endphp
                            </td>

                            <td style="width:2%">
                                {{$row->transfer_datetime}}
                            </td>

                            <td style="width:26%">
                                <textarea type="text" name="notes" id="notes{{$row->id}}" class="form-control" value="{{$row->notes}}">{{$row->notes}}</textarea>
                                <div class="ml-auto col-2 mr-4 mt-2">
                                        <button type="button" class="btn btn-danger"
                                            onclick="editNotes({{ $row->id }});">
                                            Edit
                                            <div class="spinner-border spinner-border-sm text-dark variationLoader{{ $row->id }}" role="status" style="display:none;">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </button>
                                    </div>
                                    <p class="variationThisEmpty{{ $row->id }}" style="color:red;display: none;">Please enter something to add!</p>
                                    <p class="variationRefreshPrice{{ $row->id }}" style="color:red;display: none;">Refresh to edit this notes</p>
                                </div>
                                @if ($row->cancel_notes != NULL)
                                    Cancel Note : {{$row->cancel_notes}}
                                @endif
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$headerTransferList}} Found</p>
            @endif
        </div>
        {{$tables->links()}}
    </div>
</div>
@endsection

@push('script')
<script>
    // $(document).ready(function() {
    //     $('#table-id').DataTable({
    //         "order": [[ 0, "desc" ]],
    //         "paging":   false,
    //         "info":     false
    //     });
    // });

function editNotes(batchID) {
    const notes =$("#notes"+batchID).val();

    if (confirm('Are you sure want to edit this notes? Any unsaved changes will be lost')) {
        if (notes=='' ) {
            $(".variationThisEmpty"+batchID).show();
        } else {
            $.ajax({
                // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
                url:"{{ route('warehouse.StockTransfer.stockTransferList.edit-notes', ['batchID' => "+batchID+"]) }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "batchID": batchID,
                    "notes": notes,
                },
                beforeSend: function () {
                    $(".variationLoader"+batchID).show();
                },
                success: function (success) {
                    $(".variationLoader"+batchID).hide();
                        $(".variationRefreshPrice"+batchID).show();
                },
                error: function () {
                    alert("error in loading");
                }
            });
        }
    }
}
</script>
@endpush
