<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserConnectedPlatforms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_connected_platforms', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->index();
            $table->string('platform_key', 4)->index()->nullable();
            $table->text('secret_key')->nullable();
            $table->unsignedBigInteger('account_id')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_connected_platforms');
    }
}
