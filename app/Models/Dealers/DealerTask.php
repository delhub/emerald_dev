<?php

namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerTask extends Model
{
    // Set table
    protected $table = 'view_agent_archimedes_project_data';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    // Cast column to another datatype.
    protected $casts = [];

    // public function stockLedger(){
    //      return $this->hasMany('App\Models\Dealers\DealerStockLedger','agent_id','dealer_id');

    //      // return $this->hasMany('Trip', 'route_name')->where('source_file', $this->source_file);
    //    }

    public function stockByAgent()
    {
        return $this->hasMany('App\Models\Dealers\DealerStockLedger', 'agent_id', 'referrer_id');
    }

    public function stockByProduct()
    {
        return $this->hasMany('App\Models\Dealers\DealerStockLedger', 'product_code', 'product_code');
    }

    public function items()
    {
        return $this->belongsTo('App\Models\Purchases\Item', 'item_table_id', 'id');
    }
}
