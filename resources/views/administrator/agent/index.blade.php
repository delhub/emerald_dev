
@extends('layouts.administrator.main')

@section('content')
@push('style')
<style>
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }
    
    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }
    
    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }
    
    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }
    
    input:checked + .slider {
      background-color: #ff9f43;
    }
    
    input:focus + .slider {
      box-shadow: 0 0 1px #ff9f43;
    }
    
    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }
    
    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }
    
    .slider.round:before {
      border-radius: 50%;
    }
</style>
@endpush
{{-- @php
    dd($categoryList);
@endphp --}}
<h1>Agent Categories Sales</h1>

<div class="card shadow-sm">
    <div class="card-body">
        <div class="table-responsive m-2">
          
        @if(count($categoryNames) > 0)
            <table id="categories-sales-report-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>id</td>
                        <td>Categories</td>
                        <td>Total Sale</td>
                        <td>Active</td>
                    </tr>
                </thead>
                <tbody>
                
                    @foreach($categoryNames as $categoryName)
                    <tr>
                        <td style="width: 10%;">{{$categoryName->id}}</td>
                        <td style="width: 35%;">{{$categoryName->catName}}</td>
                        <td style="width: 35%;">{{$categoryName->totalSum}}</td>
                        <td style="width: 20%;">
                            <form action="{{ route('administrator.agent.update',['id' => $categoryName->id]) }}" id="active-form" method="POST" >
                            @csrf
                            <label class="switch">
                              <input type="checkbox" value="1" id="category_status" onchange="$('#active-form').submit();" {{ ($categoryName->status == "1") ? "checked" : "" }}>
                              <span class="slider round"></span>
                            </label>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
                    @else
                    <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Categories found</p>
            @endif
        </div>
    </div>
</div>


@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#categories-sales-report-table').DataTable();
    });
</script>
@endpush