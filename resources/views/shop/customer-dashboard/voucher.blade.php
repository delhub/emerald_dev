@extends('layouts.management.main-customer')



@section('content')

@section('page_title')
{{ "Voucher Code List" }}
@endsection

<div class="container-fluid p-0">
  <div class="row mb-4">
    <div class="col-md-12 col-12">
      <div class="card" style="border-radius: 10px;box-shadow: #d8d8d8 0 0 8px 0px;">
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <h4 class="card-title  font-size my" style="font-weight:700;margin: 0;padding: 0;line-height: 30px;">Voucher Code</h4>
            </div>
            <div class="col-6">
                    <div class="form-group row">
                      <label for="text" class="col-4 col-form-label"></label>
                      <div class="mysearch01">
                        <div class="input-group">
                          <input id="search" name="text" placeholder="Search" type="text" class="form-control" autocomplete="off">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <i class="fa fa-search"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      {{-- <div class="col-8">
                        <div class="input-group dd">
                          <input id="search" name="text" placeholder="Search" type="text" class="form-control" autocomplete="off">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <i class="fa fa-search"></i>
                            </div>
                          </div>
                        </div>
                      </div> --}}
                    </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="overflow: auto;">
            <table class="table table-striped mt-3 voucher-tb" id="dtv" {{--style="overflow-x: scroll"--}}>
                <thead>
                  <tr>
                    <td class="text-muted text-bold small">EXPIRED DATE</td>
                    <td class="text-muted text-bold small">CREATED BY INVOICE</td>
                    <td class="text-muted text-bold small">VOUCHER</td>
                    {{-- <td scope="col">Remarks</td> --}}
                  </tr>
                </thead>
                <tbody>
                    @foreach($voucher as $code)
                    <tr class="text-secondary">
                        <th>{{$code->expiryf}}</th>
                        <th>{{$code->created_by_purchase}}</th>
                        <th>
                            <span class="small text-danger">{{ $code->rule['remarks'] }}</span>
                            <table class="table table-sm small">
                                <thead>
                                  <tr>
                                    <td>CODE</td>
                                    <td>AMOUNT</th>
                                    <td>USAGE</th>
                                    <td>Last Usage</td>
                                    <td>ACTION</td>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($code->voucher as $voucher)
                                  <tr>
                                    <td class="text-bold"><span id="{{ $voucher->coupon_code }}" data-clipboard-text="{{ $voucher->coupon_code }}" class="code" data-toggle="tooltip" data-placement="top" title="Code copied!" data-trigger="click">{{ $voucher->coupon_code }}</span></td>
                                    <td>{{ $voucher->coupon_amount }}</td>
                                    <td>{{ $voucher->usage_counter }}/{{ $voucher->max_voucher_usage }}</td>
                                    <td>{{ $voucher->lastUsage }}</td>
                                    <td>
                                        @if($voucher->coupon_status =='expired')
                                        <button type="button" class="btn vr-filt-btn" data-clipboard-target="#{{$voucher->id}}" disabled><strong>Expired</strong>
                                        @elseif($voucher->coupon_status =='used')
                                        <strong> USED </strong>
                                        @else
                                        {{-- <button type="button" class="btn vr-filt-btn" data-clipboard-target="#voucher{{$loop->iteration}}"><strong> Copy </strong></button> --}}
                                        <span data-clipboard-text="{{ $voucher->coupon_code }}" class="code"><strong> COPY </strong></span>
                                        @endif
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                        </th>
                        {{-- <td>{{ $code->rule['remarks'] }}</td> --}}
                      </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>

</div>

<style>
  .mysearch01{
    width: 200px;
  }
  .mobile-image {
    display: none;
  }

  .border-radius-card {
    border-radius: 15px;

  }

  .sub-border-color {
    border: 2pt solid #fbcc34;
  }

  @media(max-width:425px) {
    .font-size.my{
      line-height: 60px!important;
    }
    .mobile-image {
      display: block;
    }

    .web-image {
      display: none;
    }
  }

  @media(max-width:375px) {
    .font-size.my{
      font-size: 18px;
    }
  }

  @media(max-width:330px) {
    .font-size.my{
      font-size: 15px;
    }
  }

  @media(min-width:767px) {
    .hidden-md {
      display: none;
    }

    .font-size {
      font-size: 15pt;
    }
  }

  /*Laptop screen*/
  @media (max-width: 1800px) and (min-width: 1200px) {
    .font-size {
      font-size: 13pt;
    }

    .button-size-laptop {
      font-size: 7pt;
    }

  }
  .dataTables_filter { display: none; }
</style>

@endsection
@push('script')
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"></script>
<script>

$('#toast').toast('show');

oTable = $('#dtv').DataTable();
$('#search').keyup(function(){
      oTable.search($(this).val()).draw() ;
})

var clipboard = new ClipboardJS('.code');

clipboard.on('success', function(e) {
    $('#' + e.text).tooltip('show');
    setTimeout(function(){ $('#' + e.text).tooltip('hide'); }, 2000);
    // console.log(e.text);
/* console.info('Action:', e.action);
console.info('Text:', e.text);
console.info('Trigger:', e.trigger); */

e.clearSelection();
});

clipboard.on('error', function(e) {
console.error('Action:', e.action);
console.error('Trigger:', e.trigger);
});

</script>
@endpush
