<?php

namespace App\Mail\Registrations;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerUpgrade extends Mailable
{
    use Queueable, SerializesModels;

    public $url;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($url, $user)
    {
        $this->url = $url;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Account has been upgraded to Formula Advance Member!')
            ->markdown('emails.registrations.customer-auto-upgrade-email')
            ->with('url' . $this->url)
            ->with('user', $this->user);
    }
}
