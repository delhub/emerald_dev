<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BundleSubItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_bundle', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('item_id');
            $table->string('attribute_id');
            $table->string('primary_product_code');
            $table->bigInteger('primary_attribute_id');
            $table->integer('primary_quantity');
            $table->tinyInteger('active');
            $table->integer('sorted_qty')->default(0);
            $table->timestamps();
        });

        Schema::table('wh_product_b_unbundle', function (Blueprint $table) {
            $table->tinyInteger('bundle_type')->default(0)->after('panel_id')->comment('0 = manual, 1 = auto (pick)');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
