<?php

use App\Models\Purchases\Counters;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ModifiedCounterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Counters::truncate();

        DB::table('counters')->insert([
            [
                'counter_types' => 'invoice',
                'country_id' => 'MY',
                'counter_value' => 0,
                'counter_prefix' => 'FWS',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'counter_types' => 'receipt',
                'country_id' => 'MY',
                'counter_value' => 0,
                'counter_prefix' => 'FWR',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'counter_types' => 'po',
                'country_id' => 'MY',
                'counter_value' => 0,
                'counter_prefix' => 'FPO',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'counter_types' => 'do',
                'country_id' => 'MY',
                'counter_value' => 0,
                'counter_prefix' => 'FWD',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
