@extends('layouts.administrator.main')

@section('content')

<h1>Warehouse Expiry List</h1>

<form action="{{ route('administrator.v1.products.warehouse.batch-expiry') }}" method="GET">
    <div class="row mt-2 ml-2">
        <div class="col-xl-3 col-lg-4 ml-auto px-30">
            <div class="row">
                <div class="col-md-12 p-0">
                    <select name="location_id" class="form-control d-inline">
                        <option value="all">All Location</option>
                        @foreach ($locations as $location)
                        <option value="{{ $location->id }}" {{ $request->location_id == $location->id ? 'selected' : ''}}>
                            {{ $location->location_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 p-0 mt-2">
                    <select class="custom-select" id="selectSearch" name="selectSearch">
                        <option value="ALL" selected>Choose...</option>
                        <option value="months_left" {{ $request->selectSearch == 'months_left' ? 'selected' : '' }}>Months Left</option>
                        <option value="vendor_name" {{ $request->selectSearch == 'vendor_name' ? 'selected' : '' }}>Vendor Name</option>
                        <option value="product_code" {{ $request->selectSearch == 'product_code' ? 'selected' : '' }}>Product Code</option>
                        <option value="product_name" {{ $request->selectSearch == 'product_name' ? 'selected' : '' }}>Product Name</option>
                        <option value="batch_id" {{ $request->selectSearch == 'batch_id' ? 'selected' : '' }}>Batch ID</option>
                    </select>
                    <input type="text" class="form-control" id="searchBox" name="searchBox" value="{{ $request->input('searchBox') }}">
                    <select class="custom-select" id="monthsLeftSelect" name="monthsLeft" style="display: none;">
                        <option value="0" {{ $request->monthsLeft == '0'? 'selected' : '' }}>Expired </option>
                        @for ($i = 1; $i <= 12; $i++)
                            <option value="{{ $i }}" {{ $request->monthsLeft == $i ? 'selected' : '' }}>{{ $i }} Month{{ $i > 1 ? 's' : '' }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-0 text-right mt-2">
                    <button type="submit" class="btn btn-primary">Filter</button>
                    <button type="submit" class="btn btn-danger"><a href={{ route('administrator.v1.products.warehouse.batch-expiry') }} style="color:white;">Reset Filter</a></button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="row">
    <div class="legend-area mb-3">
        <div class="small-legend-box expired">
        </div>
        <div class="small-legend-text">
            <p>Expired</p>
        </div>
        <div class="small-legend-box less-than-3">
        </div>
        <div class="small-legend-text">
            <p>Less than 3 Months</p>
        </div>
        <div class="small-legend-box less-than-6">
        </div>
        <div class="small-legend-text">
            <p>Less than 6 Months</p>
        </div>
        <div class="small-legend-box less-than-9">
        </div>
        <div class="small-legend-text">
            <p>Less than 9 Months</p>
        </div>
        <div class="small-legend-box less-than-12">
        </div>
        <div class="small-legend-text">
            <p>Less than 12 Months</p>
        </div>
    </div>
</div>

<div class="card shadow-sm">
    <div class="card-body">

        <div class="d-flex top-header-flex">
            <h4>Total Item : {{ $totalItem }}</h4>
            @if ($showAllData)
            <button class="btn btn-primary mb-1 all-data-btn" onclick="window.location='{{ route('administrator.v1.products.warehouse.batch-expiry', ['showAllData' => true]) }}'">Show All Data</button>
            @else
            <button class="btn btn-primary mb-1 all-data-btn" onclick="window.location='{{ route('administrator.v1.products.warehouse.batch-expiry') }}'">Hide Old Data</button>
            @endif
        </div>

        <div class="batch-pagination">
            {{$tables->links()}}
        </div>

        <div class="table-responsive">
            <table id="table-id" class="table table-bordered table-responsive batch-expiry-table">
                <thead>
                    <tr>
                        <th style="width: 50px;" class="text-center">No</th>
                        <th>Batch ID</th>
                        <th style="width: 50px;" class="text-center">Lot</th>
                        <th>Vendor Name</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Product Attribute</th>
                        <th class="text-center">Qty</th>
                        <th>Location</th>
                        <th>Expiry Date</th>
                        <th class="action-row">Action</th>
                    </tr>
                </thead>

                @if(count($tables) > 0)
                <tbody>

                    @foreach($tables as $row)
                    <tr class="{{ $row->expiryClass }}">
                        <td class="text-center">{{ $listNumber }}</td>
                        <td>{{ $row->batch_id }}</td>
                        <td class="text-center">{{ $row->lot }}</td>
                        <td>{{ $row->mainBatch->vendor_desc }}</td>
                        <td>{{ $row->product_code }}</td>
                        <td>{{ optional($row->productAttribute)->product2->parentProduct->name }}</td>
                        <td>{{ optional($row->productAttribute)->attribute_name }}</td>
                        <td class="text-center">{{ $row->unusedItems->count() }}</td>
                        <td>{{ optional($row->mainBatch->location)->location_name }}</td>
                        <td>{{ date('d-M-Y', strtotime($row->expiry_date)) }}</td>
                        <td>
                            <div class="action-btn-flex">
                                <button class="btn btn-success modal-info-btn mb-1 action-btn" data-toggle="modal" data-target="#infoModal" data-row="{{ json_encode($row) }}">Info</button>
                                <button class="btn btn-warning action-btn">
                                    <a href="{{ route('administrator.v1.products.warehouse.batch-item-list', [
                                        'location_id' => 'all',
                                        'selectSearch' => 'item_batch_id',
                                        'searchBox' => $row->batch_id . '-' . $row->id
                                    ]) }}">Batch Items</a>
                                </button>
                            </div>
                        </td>
                    </tr>
                    @php
                        $listNumber++;
                    @endphp
                    @endforeach
                </tbody>
                @else
                <tbody>
                    <tr>
                        <td colspan="8" style="text-align:center">No Data ..</td>
                    </tr>
                </tbody>
                @endif
            </table>

            <div class="batch-pagination">
                {{$tables->links()}}
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" aria-labelledby="infoModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="infoModalLabel">Product Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<style>
.px-30 {
    padding: 0 30px;
}
th.action-row {
    width: 130px;
    text-align: center;
}
.action-btn {
    width: 105px;
}
.action-btn-flex {
    display: flex;
    flex-direction: column;
}
.table td {
    vertical-align: middle;
}
.btn-warning a {
    color: black;
}
.btn-success, .btn-warning {
    border: 1px solid #6f6f6f;
}
.btn-danger a:hover, .btn-warning a:hover{
    text-decoration: none;
}
.expired {
    background-color: #ff5c5c;
}
.less-than-3 {
    background-color: #ff724b;
}
.less-than-6 {
    background-color: #ff9f68;
}
.less-than-9 {
    background-color: #ffd071;
}
.less-than-12 {
    background-color: #fff480;
}
.pl-0 {
    padding-left: 0 !important;
}
.legend-area {
    display: flex;
    padding-left: 15px;
    padding-right: 15px;
    align-items: center;
}
.small-legend-box {
    width: 25px;
    height: 15px;
    border: 1px solid black;
}
.small-legend-text p {
    font-size: 12px;
    margin-bottom: 0;
    margin-left: 2px;
    margin-right: 10px;
}
.modal-body p {
    margin-bottom: 5px;
}
.t-red {
    color: red;
    font-size: 13px;
}
.modal-footer {
    padding: 10px;
}
.table-responsive.batch-expiry-table {
    display: table;
}
.top-header-flex {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 10px;
}
.top-header-flex h4 {
    margin-bottom: 0;
    font-weight: bold;
}
.all-data-btn {
    height: 40px;
    background-color: #204daf !important;
    border-color: #204daf;
}
.all-data-btn:hover {
    background-color: #083085;
}
.batch-pagination {
    overflow-x: scroll;
}
.batch-pagination::-webkit-scrollbar-thumb {
    background: #3490dc;
    opacity: 0;
}
.batch-pagination::-webkit-scrollbar-track {
    background: #6c6c6cf6;
}
.batch-pagination::-webkit-scrollbar {
    height: 4px;
    margin-bottom: 20px;
}
@media only screen and (max-width: 1400px) {
    .table-responsive.batch-expiry-table {
        display: block;
    }
}
</style>

@endpush
@push('script')
<script>
    $(document).ready(function() {
        // Search Filter JS
        $('#selectSearch').change(function() {
            if ($(this).val() === 'months_left') {
                $('#searchBox').hide();
                $('#monthsLeftSelect').show();
            } else {
                $('#searchBox').show();
                $('#monthsLeftSelect').hide();
            }
        });
        $('#selectSearch').trigger('change');

        // Dynamic Modal
        $('.modal-info-btn').click(function() {
            var rowData = $(this).data('row');
            var modal = $('#infoModal');

            $.ajax({
                url: '{{ route("get-batch-info", ":batch_id") }}'.replace(':batch_id', rowData.batch_id),
                type: 'GET',
                success: function(data) {
                    modal.find('.modal-title').html('<b>Batch Id :</b> ' + rowData.batch_id);
                    modal.find('.modal-body').html( '<p><b>Batch Type :</b> ' + (data.batch_type || '<span class="t-red">No Data ...</span>') + '</p>' +
                                                    '<p><b>Panel Name :</b> ' + (data.panel_name || '<span class="t-red">No Data ...</span>') + '</p>' +
                                                    '<p><b>PO Number :</b> ' + (data.po_number || '<span class="t-red">No Data ...</span>') + '</p>' +
                                                    '<p><b>Invoice Date :</b> ' + (data.invoice_date || '<span class="t-red">No Data ...</span>') + '</p>' +
                                                    '<p><b>Invoice Number :</b> ' + (data.invoice_number || '<span class="t-red">No Data ...</span>') + '</p>' +
                                                    '<p><b>Vendor Name :</b> ' + (data.vendor_name || '<span class="t-red">No Data ...</span>') + '</p>' +
                                                    '<p><b>Stock In Date :</b> ' + (data.stock_in_date || '<span class="t-red">No Data ...</span>') + '</p>');
                    modal.modal('show');
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
        });

    });
</script>
@endpush
