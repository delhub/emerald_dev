@extends('layouts.management.main-customer')

@section('content')

    @if (Session::has('successful_message'))
        <div class="alert alert-success">
            {{ Session::get('successful_message') }}
        </div>
    @endif

    @if (Session::has('error_message'))
        <div class="alert alert-danger">
            {{ Session::get('error_message') }}
        </div>
    @endif

    <div class="row">
        <div class="col-12 col-md-10">
            <div class="row col-12 mywebmenu2">
                <div class="my-profile-opt-btn active">
                    <i class="fa fa-user mr-1"></i> <a href="{{ route('shop.dashboard.customer.profile') }}"
                        class="text-color-header "><strong>My Profile</strong></a>
                </div>
                @hasrole('dealer')
                <div class="my-profile-opt-btn">
                    <i class="fa fa-address-book-o mr-1 "></i> <a href="{{ route('shop.dashboard.dealer.profile') }}"
                        class="text-color-header "><strong>Agent Profile</strong></a>
                </div>
                @endhasrole
                @hasrole('panel')
                {{-- <div class="my-profile-opt-btn">
        <i class="fa fa-building-o mr-1"></i><a href="{{route('management.company.profile')}}"
          class="text-color-header "><strong>Panel Profile</strong></a>
      </div> --}}
      @endhasrole
    </div>
    <div class="col-12 mymobilemenu2">
      <button class="btn btn-secondary mybut3 bjsh-btn-gradient" type="button" id="dropdownMenuButton"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
        style="background-color:rgb(250, 172, 24); color:black;">
        My Profile<i style="font-size: 10px;" class="fa fa-arrow-down"> </i>
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="#">My Profile</a>
        @hasrole('dealer')
        <a class="dropdown-item" href="{{route('shop.dashboard.dealer.profile')}}">Agent Profile</a>
        @endhasrole
        @hasrole('panel')
        <a class="dropdown-item" href="{{route('management.company.profile')}}">Panel Profile</a>
        @endhasrole
      </div>
    </div>
    <div class="card shadow-sm">
      <div class="card-body">
        <div class="form-group row p-form-box">
          <div class="col-md-12 m-0 profile-box" style="display: flex;">
            <h4>Profile Information</h4>
            <a style="margin-left:1rem;" href="{{route('shop.dashboard.customer.profile.edit')}}"><i
                class="fa fa-pencil-square-o bujishu-gold mytext-20px"></i></a>
          </div>
        </div>
        <hr>
        <div class="form-group row p-form-box">
          <label for="avatar" class="col-12 col-md-3 col-form-label">Avatar</label>
          <div class="col-4 col-md-1 m-0">
            <img src="{{asset('/images/formula/default-avatar.jpg')}}" style="max-width:80px; max-height:80px;"
              alt="avatar">
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="account_id" class="col-12 col-md-3 col-form-label">Formula ID</label>
          <div class="col-12 col-md-9 m-0">
            <input type="text" name="account_id" id="account_id" value="{{$customerInfo->userInfo->account_id}}"
              class="form-control  @error('account_id') is-invalid @enderror" value="{{ old('account_id') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="full_name" class="col-12 col-md-3 col-form-label">Full Name(NRIC)</label>
          <div class="col-12 col-md-4 m-0">
            <input type="text" name="full_name" id="full_name" value="{{$customerInfo->userInfo->full_name}}"
              class="form-control  @error('full_name') is-invalid @enderror" value="{{ old('full_name') }}" readonly>
          </div>
        </div>
        @if ($customerInfo->userInfo->nric)
          <div class="form-group row p-form-box">
            <label for="birth_date" class="col-12 col-md-3 col-form-label">{{ strlen($customerInfo->userInfo->nric) == 4 ? 'Last 4 Digits of NRIC' : 'Full NRIC Number' }}</label>
            <div class="col-12 col-md-4 m-0">
              <input type="text" name="birth_date" id="birth_date" value="{{$customerInfo->userInfo->nric}}"
                class="form-control  @error('birth_date') is-invalid @enderror" value="{{ old('birth_date') }}" readonly>
            </div>
          </div>
        @elseif($customerInfo->userInfo->date_of_birth)
          <div class="form-group row p-form-box">
            <label for="birth_date" class="col-12 col-md-3 col-form-label">Birth Of Date</label>
            <div class="col-12 col-md-4 m-0">
              <input type="date" name="birth_date" id="birth_date" value="{{$customerInfo->userInfo->date_of_birth}}"
                class="form-control  @error('birth_date') is-invalid @enderror" value="{{ old('birth_date') }}" readonly>
            </div>
          </div>
          <div class="form-group row p-form-box">
            <label for="birth_date" class="col-12 col-md-3 col-form-label">Passport No.</label>
            <div class="col-12 col-md-4 m-0">
              <input type="text" name="birth_date" id="birth_date" value="{{$customerInfo->userInfo->passport_no}}"
                class="form-control  @error('birth_date') is-invalid @enderror" value="{{ old('birth_date') }}" readonly>
            </div>
          </div>
          <div class="form-group row p-form-box">
            <label for="country" class="col-12 col-md-3 col-form-label">Country</label>
            <div class="col-12 col-md-4 m-0">
              <input type="text" name="country" id="country" value="{{$customerInfo->userInfo->country}}"
                class="form-control  @error('country') is-invalid @enderror" value="{{ old('country') }}" readonly>
            </div>
          </div>
        @endif
        <div class="form-group row p-form-box">
          <label for="billing_address_1" class="col-md-3 col-form-label">Billing Address</label>
          <div class="col-md-9 m-0">
            <input type="text" name="billing_address_1" id="billing_address_1"
              value="{{$customerInfo->userInfo->mailingAddress->address_1}}"
              class="form-control  @error('billing_address_1') is-invalid @enderror"
              value="{{ old('billing_address_1') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box pbox-2">
          <div class="offset-md-3 col-md-9  ">
            <input type="text" name="billing_address_2" id="billing_address_2"
              value="{{$customerInfo->userInfo->mailingAddress->address_2}}"
              class="form-control  @error('billing_address_2') is-invalid @enderror"
              value="{{ old('billing_address_2') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box pbox-2">
          <div class="offset-md-3 col-md-9 ">
            <input type="text" name="billing_address_3" id="billing_address_3"
              value="{{$customerInfo->userInfo->mailingAddress->address_3}}"
              class="form-control  @error('billing_address_3') is-invalid @enderror"
              value="{{ old('billing_address_3') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="billing_city" class="col-md-3 col-form-label">Billing City</label>
          <div class="col-md-4 m-0">
            <input type="text" name="billing_city" id="billing_city"
              value="{{ !empty($customerInfo->userInfo->mailingAddress->city_key) ? $customerInfo->userInfo->mailingAddress->cityKey->city_name : $customerInfo->userInfo->mailingAddress->city}}"
              class="form-control  @error('billing_city') is-invalid @enderror" value="{{ old('billing_city') }}"
              readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="billing_postcode" class="col-md-3 col-form-label">Billing Postcode</label>
          <div class="col-md-4 m-0">
            <input type="text" name="billing_postcode" id="billing_postcode"
              value="{{$customerInfo->userInfo->mailingAddress->postcode}}"
              class="form-control  @error('billing_postcode') is-invalid @enderror"
              value="{{ old('billing_postcode') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="billing_state" class="col-md-3 col-form-label">Billing State</label>
          <div class="col-md-9 m-0">
            <input type="text" name="billing_state" id="billing_state"
              value="{{$customerInfo->userInfo->mailingAddress->state->name}}"
              class="form-control @error('billing_state') is-invalid @enderror" value="{{ old('billing_state') }}"
              readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="shipping_address_1" class="col-md-3 col-form-label">Shipping Address</label>
          <div class="col-md-9 m-0">
            <input type="text" name="shipping_address_1" id="shipping_address_1"
              value="{{(isset($customerInfo->userInfo->cartShippingAddress->address_1)) ? $customerInfo->userInfo->cartShippingAddress->address_1 : ''}}"
              class="form-control  @error('shipping_address_1') is-invalid @enderror"
              value="{{ old('shipping_address_1') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box pbox-2">
          <div class="offset-md-3 col-md-9 ">
            <input type="text" name="shipping_address_2" id="shipping_address_2"
              value="{{(isset($customerInfo->userInfo->cartShippingAddress->address_2)) ? $customerInfo->userInfo->cartShippingAddress->address_2 : ''}}"
              class="form-control  @error('shipping_address_2') is-invalid @enderror"
              value="{{ old('shipping_address_2') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box pbox-2">
          <div class="offset-md-3 col-md-9 ">
            <input type="text" name="shipping_address_3" id="shipping_address_3"
              value="{{(isset($customerInfo->userInfo->cartShippingAddress->address_3)) ? $customerInfo->userInfo->cartShippingAddress->address_3 : ''}}"
              class="form-control  @error('shipping_address_3') is-invalid @enderror"
              value="{{ old('shipping_address_3') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="shipping_city" class="col-md-3 col-form-label">Shipping City</label>
          <div class="col-md-4 m-0">
            <input type="text" name="shipping_city" id="shipping_city"
              value="{{$shippingCity}}"
              class="form-control  @error('shipping_city') is-invalid @enderror" value="{{ old('shipping_city') }}"
              readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="shipping_postcode" class="col-md-3 col-form-label">Shipping Postcode</label>
          <div class="col-md-4 m-0">
            <input type="text" name="shipping_postcode" id="shipping_postcode"
              value="{{(isset($customerInfo->userInfo->cartShippingAddress->postcode)) ? $customerInfo->userInfo->cartShippingAddress->postcode : ''}}"
              class="form-control  @error('shipping_postcode') is-invalid @enderror"
              value="{{ old('shipping_postcode') }}" readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="shipping_state" class="col-md-3 col-form-label">Shipping State</label>
          <div class="col-md-9 m-0">
            <input type="text" name="shipping_state" id="shipping_state"
              value="{{$customerInfo->userInfo->cartShippingAddress->state->name}}"
              class="form-control @error('shipping_state') is-invalid @enderror" value="{{ old('shipping_state') }}"
              readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="mobile_phone" class="col-md-3 col-form-label">Mobile Phone</label>
          <div class="col-md-4 m-0">
            <input type="text" name="mobile_phone" id="mobile_phone"
              value="{{$customerInfo->userInfo->mobileContact->contact_num}}"
              class="form-control  @error('mobile_phone') is-invalid @enderror" value="{{ old('mobile_phone') }}"
              readonly>
          </div>
        </div>
        <div class="form-group row p-form-box">
          <label for="email" class="col-md-3 col-form-label">Email Address</label>
          <div class="col-md-4 m-0">
            <input type="text" name="email" id="email" value="{{$customerInfo->email}}"
              class="form-control  @error('email') is-invalid @enderror" value="{{ old('email') }}" readonly>
          </div>
        </div>

                    <div class="form-group row p-form-box">
                        <label for="id_information" class="col-md-3 col-form-label">Member Status</label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="id_information" id="id_information"
                                value="{{ $customerInfo->userInfo->showUserLevel() }}"
                                class="form-control  @error('id_information') is-invalid @enderror"
                                value="{{ old('id_information') }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .text-color-header {
            color: black;
            font-size: 12pt;
        }

    </style>

@endsection

@push('script')
    <script>
        //side menu disable and layout size
        const hideLeftNMenu = document.querySelector('.row.db-width.hide .col-md-3');
        const hideRightNMenu = document.querySelector('.row.db-width.hide .col-md-9');
        hideLeftNMenu.style.display = 'none';
        hideRightNMenu.className = 'col-md-12 my-box-pd';
    </script>
@endpush
