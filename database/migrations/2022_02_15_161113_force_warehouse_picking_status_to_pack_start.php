<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Pick\PickBatch;

class ForceWarehousePickingStatusToPackStart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $thoseBatch = PickBatch::whereIn(
            'batch_number',
            ['WHP21-000026', 'WHP21-000029', 'WHP21-000030', 'WHP21-000032', 'WHP21-000033', 'WHP21-000034', 'WHP21-000036', 'WHP21-000039', 'WHP21-000040', 'WHP21-000042', 'WHP21-000045', 'WHP21-000046', 'WHP21-000047', 'WHP21-000049', 'WHP21-000050', 'WHP21-000052', 'WHP21-000054', 'WHP21-000057', 'WHP21-000058', 'WHP21-000060', 'WHP21-000061', 'WHP21-000062', 'WHP21-000063', 'WHP21-000064', 'WHP21-000067', 'WHP21-000068', 'WHP21-000070', 'WHP21-000186', 'WHP21-000238', 'WHP21-000242', 'WHP21-000267', 'WHP21-000319', 'WHP21-000371']
        )->get();

        foreach ($thoseBatch as $key => $batch) {
            $batch->pick_pack_status = 5;
            $batch->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pack_start', function (Blueprint $table) {
            //
        });
    }
}
