@extends('layouts.guest.main')

@section('content')

    <div class="container return-align">
        <div class="return-o-box my-pt-7">

            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="form-group row ">
                        <div class="col-md-4 m-0">
                            <table class="return_form_title" style="border-collapse: collapse;">
                                <tr>
                                    <td>
                                        <span>RETURN FORM</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <hr>

                    <div class="personal-info-box">
                        <div id="personal_info" class="my-mt-3 my-mb-1">
                            <span> Personal Information </span>
                        </div>
                        <div class="return-info-box">
                            <div class="form-group row">
                                <label for="" class="col-md-2 col-form-label">Name
                                    <small class="text-danger">*</small>
                                </label>
                                <div class="col-md-9 m-0">
                                    <input type="text" name="" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-2 col-form-label">NRIC <small
                                        class="text-danger">*</small></label>
                                <div class="col-md-9 m-0">
                                    <input type="text" name="" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-2 col-form-label">Contact Number <small
                                        class="text-danger">*</small></label>
                                <div class="col-md-9 m-0">
                                    <input type="text" name="" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-2 col-form-label">Customer Address <small
                                        class="text-danger">*</small></label>
                                <div class="col-md-9 m-0">
                                    <input type="text" name="" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-2 col-form-label">Delivery Address <small
                                        class="text-danger">*</small></label>
                                <div class="col-md-9 m-0">
                                    <input type="text" name="" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-2 col-form-label">Order / Invoice Number <small
                                        class="text-danger">*</small></label>
                                <div class="col-md-9 m-0">
                                    <input type="text" name="" class="form-control form-control-sm">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="" class="col-md-2 col-form-label">Receiving Date <small
                                        class="text-danger">*</small></label>
                                <div class="col-md-9 m-0">
                                    <input type="date" name="receiving_date" id="receiving_date"
                                        class="form-control form-control-sm" value="" max="2021-7-8">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="detail-box">
                        <div id="detail-return" class="my-mt-3 my-mb-1">
                            <span> Details of Return </span>
                        </div>
                        <div class="return-info-box2 my-mb-3">
                            <table id="categories-sales-report-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <td>No.</td>
                                        <td>Product SKU</td>
                                        <td>Product Description </td>
                                        <td>Quantity Ordered</td>
                                        <td>Quantity Returned</td>
                                        <td>Reason</td>
                                        <td>Detail of Fault</td>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        @for ($i = 1; $i <= 10; $i++)
                                            <td>{{ $i . '.' }}</td>
                                            <td><input type="text" name="" class="form-control"></td>
                                            <td><input type="text" name="" class="form-control"></td>
                                            <td><input type="text" name="" class="form-control"></td>
                                            <td><input type="text" name="" class="form-control"></td>
                                            <td><select type="text" name="" class="form-control">
                                                    <option value="default"></option>
                                                    <option value="1">A</option>
                                                    <option value="2">B</option>
                                                    <option value="3">C</option>
                                                    <option value="4">D</option>
                                                    <option value="5">E</option>
                                                    <option value="6">F</option>
                                                    <option value="7">G</option>
                                                </select></td>
                                            <td><input type="text" name="" class="form-control">
                                                <form action="/" id="active-form" method="POST">
                                                    @csrf
                                                    {{-- <label class="switch">
                      <input type="checkbox" value="1" id="category_status" >
                      <span class="slider round"></span>
                    </label> --}}
                                                </form>
                                            </td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>

                        <div class="return-btm-info">
                            <div class="reason-left">
                                <span>Reason for Return:</span>
                            </div>
                            <div class="opt-right">
                                <p><span>A</span> Faulty</p>
                                <p><span>B</span> Damaged</p>
                                <p><span>C</span> Wrong Size / Colour / Variant</p>
                                <p><span>D</span> Parts Missing</p>
                                <p><span>E</span> Item Missing</p>
                                <p><span>F</span> Wrong Item</p>
                                <p><span>G</span> Others (Please Specify)</p>
                            </div>
                        </div>
                        <div class="return-notice">
                            * Items return will only be
                            accepted if complete with all accessories and GWP <br />* Health equipment will only be
                            accepted
                            if dismantled and boxed as received
                        </div>
                    </div>

                </div>
                <div class="text-right">
                    <div class="ri-btn pr-4 pb-3">
                        <input type="submit" class="btn r-color-btn" value="Submit">
                    </div>
                </div>

            </div>


        </div>
    </div>
