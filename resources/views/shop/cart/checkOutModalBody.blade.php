<div class="d-confirm modal-header">

    <h1 class="modal-title edit-tittle" id="checkoutcartLabel">
        @if ($deliveryMethod == 'Self-pickup')
            Self Collect At Store : {{ $outlet ? $outlet->outlet_name : '' }}
        @else
            Please Confirm The Delivery <span>Address Is Correct</span>
        @endif
    </h1>

    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>

<form>
    <fieldset disabled>
        <div class="mb-3 confirm-box2">
            <label for="disabledTextInput" class="form-label">
                @if ($deliveryMethod == 'Self-pickup') Store @endif
                Name
            </label>
            <div class="confirm-box1">
                <p>
                    @if ($deliveryMethod == 'Self-pickup')
                        {{ $outlet ? $outlet->outlet_name : '' }}
                    @else
                        {{ $cookie ? $cookie->name : '' }}
                    @endif
                </p>
            </div>
        </div>
        <div class="mb-3 confirm-box2">
            <label for="disabledTextInput" class="form-label">Contact
                (Mobile)</label>
            <div class="confirm-box1">
                <p>
                    @if ($deliveryMethod == 'Self-pickup')
                        {{ $outlet ? $outlet->contact_number : '' }}
                    @else
                        {{ $cookie ? $cookie->mobile : '' }}
                    @endif
                </p>
            </div>
        </div>
        <div class="mb-3 ">
            <label for="disabledTextInput" class="form-label">
            @if ($deliveryMethod == 'Self-pickup') Store @else Shipping
                @endif
                Address
            </label>
            <div class="confirm-box1">
                <p>
                    @if ($deliveryMethod == 'Self-pickup')
                        {{ $outlet->default_address ?? '' }}
                        {{ $outlet->postcode ?? '' }}
                        {{ $outlet->city->city_name ?? '' }}
                        {{ $outlet && $outlet->state_id ? $outlet->state->name : '' }}
                    @else
                        {{ $cookie->address_1 ?? '' }}
                        {{ $cookie->address_2 ?? '' }}
                        {{ $cookie->address_3 ?? '' }}
                        {{ $cookie->postcode ?? '' }}
                        {{ $cookie->city_name->city_name ?? '' }}
                        {{ $cookie && $cookie->state_name ? $cookie->state_name->name : '' }}
                    @endif
                </p>
            </div>
        </div>
    </fieldset>
</form>
