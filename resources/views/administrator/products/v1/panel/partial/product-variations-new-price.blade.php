@if ($user->hasAnyRole('administrator|management|admin_product|formula_pic'))
    <div class="col-md-2 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Add New Product Price</label>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="new_web_fixed_price" class="col-form-label font-weight-bold">Web Fixed Price (RM)<small class="text-danger">*</small></label>
                <input type="text" name="new_fixed_price" id="new_fixed_price{{$attributeID}}"
                class="form-control input-mask-price" value="">
            </div>
            <div class="col-md-3">
                <label for="new_web_offer_price" class="col-form-label font-weight-bold">Web Offer Price (RM) <small class="text-danger">*</small></label>
                <input type="text" name="new_web_offer_price" id="new_web_offer_price{{$attributeID}}"
                class="form-control input-mask-price" value="">
            </div>
            <div class="col-md-3">
                <label for="new_outlet_fixed_price" class="col-form-label font-weight-bold">Outlet Fixed Price (RM) <small class="text-danger">*</small></label>
                <input type="text" name="new_outlet_fixed_price" id="new_outlet_fixed_price{{$attributeID}}"
                class="form-control input-mask-price" value="">
            </div>
            <div class="col-md-3">
                <label for="new_outlet_offer_price" class="col-form-label font-weight-bold">Outlet Offer Price (RM) <small class="text-danger">*</small></label>
                <input type="text" name="new_outlet_offer_price" id="new_outlet_offer_price{{$attributeID}}"
                class="form-control input-mask-price" value="">
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="new_member_price" class="col-form-label font-weight-bold">Standard Member Price (RM) <small class="text-danger">*</small></label>
                <input  type="text" name="new_member_price" id="new_member_price{{$attributeID}}"
                class="form-control input-mask-price"  value="0">
            </div>
            <div class="col-md-3">
                <label for="new_offer_price" class="col-form-label font-weight-bold">Advance Member Price (RM)</label>
                <input type="text" name="new_offer_price" id="new_offer_price{{$attributeID}}"
                class="form-control input-mask-price"  value="0">
            </div>
            <div class="col-md-3">
                <label for="new_premier_price" class="col-form-label font-weight-bold">Premier Member Price (RM)</label>
                <input type="text" name="new_premier_price" id="new_premier_price{{$attributeID}}"
                class="form-control input-mask-price"  value="0">
            </div>
        </div>
    </div>

    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Product SV <small class="text-danger">*</small></label>
                <input type="text" name="new_product_sv" id="new_product_sv{{$attributeID}}"
                class="form-control input-mask-price" value="0">
            </div>
            <div class="col-md-9">
                <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Notes</label>
                <textarea type="text" name="new_notes" id="new_notes{{$attributeID}}"
                class="form-control" value="0"></textarea>
            </div>
        </div>
    </div>
@endif

    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-md-2 d-flex mr-md-5">
        <label for="point" class="my-auto col-form-label font-weight-bold">Product Point</label>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="fixed_price" class="col-form-label ">Point</label>
                <input type="number" name="new_point" id="new_point{{$attributeID}}"
                    class="form-control" value="0">
            </div>
            <div class="col-md-3">
                <label for="discount_percentage" class="col-form-label ">Discount Percentage (%)</label>
                <input type="number" id="new_discount_percentage{{$attributeID}}" name="new_discount_percentage"
                    class="form-control d-inline" value="0">
            </div>
            @php
                $pointCash = $attribute && $attribute->getPriceAttributes('point_cash') != NULL ? json_decode($attribute->getPriceAttributes('point_cash')) : null;
            @endphp
            <div class="col-md-3">
                <label for="point_cash_pointNpoint_cash_cash" class="col-form-label ">Point + Cash<small class="text-danger">*</small></label>
                <div class="input-group">
                    <input type="text" name="new_point_cash_point" id="new_point_cash_point{{$attributeID}}" class="select2 form-control available-in d-inline" value="0" placeholder="0">
                    <div class="input-group-text">
                        <span class="input-group-prepend">+</span>
                    </div>
                    <input type="text" name="new_point_cash_cash" id="new_point_cash_cash{{$attributeID}}" value="0" placeholder="0" class="form-control input-mask-price">
                </div>
            </div>
        </div>
    </div>

    <h2 class="variationPriceEmpty ml-auto col-4 mr-4 mt-2" style="color:red;display: none;">Please enter price to add!</h2>

    <div class="ml-auto col-2 mr-4 mt-2">

        <button type="button" class="btn btn-danger" style="width: 100%;"
            onclick="addNewPrice({{ $attributeID }}, 1);">
            Add + Activate New Price
            <div class="spinner-border spinner-border-sm text-dark variationLoaderPrice" role="status" style="display:none;">
                <span class="sr-only">Loading...</span>
                </div>
        </button>
        <small class="variationRefreshPriceActive" style="display: none;">Refresh to add + active this price</small>
    </div>

    <div class="ml-auto col-2 mr-4 mt-2">
        <button type="button" class="btn btn-danger" style="width: 100%;"
            onclick="addNewPrice({{ $attributeID }});">
            Add New Price
            <div class="spinner-border spinner-border-sm text-dark variationLoaderPrice" role="status" style="display:none;">
                <span class="sr-only">Loading...</span>
                </div>
        </button>
        <small class="variationRefreshPrice" style="display: none;">Refresh to add this price</small>

    </div>

