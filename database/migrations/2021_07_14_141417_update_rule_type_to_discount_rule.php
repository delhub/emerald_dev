<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRuleTypeToDiscountRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_rule', function (Blueprint $table) {
            $table->dropColumn(['rule_type']);
        });

        Schema::table('discount_rule', function (Blueprint $table) {
            $table->string('rule_type')->default('agent_registration')->after('country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_rule', function (Blueprint $table) {
            $table->dropColumn(['rule_type']);
        });

        Schema::table('discount_rule', function (Blueprint $table) {
            $table->string('rule_type')->nullable()->after('country_id');
        });
    }
}
