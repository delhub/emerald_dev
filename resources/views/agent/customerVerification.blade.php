@extends('layouts.management.main-agent')
@section('content')

    @if (Session::has('successful_message'))
        <div class="alert alert-success">
            {{ Session::get('successful_message') }}
        </div>
    @endif

    @if (Session::has('error_message'))
        <div class="alert alert-danger">
            {{ Session::get('error_message') }}
        </div>
    @endif

@section('page_title')
    {{ 'Welcome to Agent Dashboard' }}
@endsection

<div class="card shadow-sm">
    <div class="card-body">
        <h4>Customer Verification</h4>

        <div>
            <div class="row">
                {{-- <div class="col-12 text-right p-2">
            <a href="/administrator/courier/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Product</a>
        </div> --}}
            </div>
        </div>
        <div class="table-responsive m-2">
            @if (count($agentDownLineUser) > 0)
                <table id="global-products-table" class="table table-striped table-bordered" style="min-width: auto;">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Customer Information</td>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- {{ dd($agentDownLineUser->pluck('user')) }} --}}
                        @foreach ($agentDownLineUser as $downLineUser)
                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    Full Name : {{ $downLineUser->userInfo->full_name }}
                                    <br>
                                    Email : {{ isset($downLineUser) ? $downLineUser->email : '' }}
                                    <br>
                                    Register Date:
                                    {{ isset($downLineUser->created_at) ? $downLineUser->getFormattedtDate($downLineUser->created_at) : '' }}
                                    <hr>

                                    <div class="opt-btn-space">

                                        {{-- @if ($downLineUser->email_verified_at != null)

                        Approved : {{ $downLineUser->getFormattedtDate($downLineUser->email_verified_at) }}

                    @elseif ($downLineUser->email_verified_at == NULL && $downLineUser->userInfo->account_status == '9')

                        Disapproved : {{$downLineUser->notes}}

                    @else --}}

                                        <form action="{{ route('agent.customerVerification.action', [$downLineUser->id]) }}"
                                              method="POST">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="user_id" value="{{ $downLineUser->id }}">
                                            <input type="hidden" name="action" value="approve">


                                            <input type="submit" class="btn vr-filt-btn mr" value="Approve">
                                        </form>

                                        <hr>

                                        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Launch demo modal
                            </button> --}}

                                        {{-- <button type="button" class="btn cv-reject-btn br-none" data-bs-toggle="modal"
                                                data-bs-target="#exampleModal{{ $downLineUser->id }}">Reject</button> --}}
                                        <div class="btn cv-reject-btn br-none" data-bs-toggle="modal"
                                             data-bs-target="#exampleModal{{ $downLineUser->id }}"> Reject </div>

                                        <!-- Modal -->
                                        <div class="modal fade modal-downline-user"
                                             id="exampleModal{{ $downLineUser->id }}" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">


                                                <form action="{{ route('agent.customerVerification.action', [$downLineUser->id]) }}"
                                                      method="POST" value='desapprove'>
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Reason Reject
                                                            </h5>
                                                            <button type="button" class="close"
                                                                    data-bs-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="hidden" name="_method" value="POST">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <input type="hidden" name="user_id"
                                                                   value="{{ $downLineUser->id }}">
                                                            <input type="hidden" name="action" value="disapprove">
                                                            <input class="fill-reason" type="text" name="reason"
                                                                   value="" size="50"
                                                                   placeholder="Reason...">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="submit" class="btn vr-filt-btn br-none"
                                                                   value="Disapprove">
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>


                                        {{-- <form action="{{ route('agent.customerVerification.action',[$downLineUser->user->id]) }}"
                            method="POST" value='desapprove'>
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="user_id" value="{{ $downLineUser->user->id}}">
                            <input type="hidden" name="action" value="disapprove">

                            {{--  <input type="text" name ="reason" value="" size="50"> --}}
                                        {{-- <br> --}}
                                        {{-- <input type="submit" class="bjsh-btn-gradient ml-1" value="Disapprove"> --}}
                                        {{-- </form> --}}

                                        {{-- @endif --}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                {{-- {{$couriers->links()}} --}}
            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Customer found</p>
            @endif
        </div>
    </div>
</div>



@endsection
@push('style')
<style>
    .btn.cv-reject-btn {
        color: #ffffff;
        background: linear-gradient(89.82deg, #ff757c 0.13%, #ef3842 99.82%);
        width: 90px;
        height: 36px;
        border-radius: 30px;
        border: none;
        padding: 7px 15px;
    }

    .modal-open .modal {
        background: #000000b0;
    }

    .modal-backdrop {
        position: relative;
    }

    .hidenav {
        z-index: -1 !important;
    }
</style>
@endpush
@push('script')
<script type="text/javascript">
    $('.modal-downline-user').on('show.bs.modal', function(e) {
        $(".cv-reject-btn").click(function() {
            $('.fml-head.nav-height').addClass('hidenav');
        });
    })

    $('.modal-downline-user').on('hide.bs.modal', function(e) {
        $('.fml-head.nav-height').removeClass('hidenav');
    })
</script>
@endpush
