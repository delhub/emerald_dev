<?php
namespace App\Traits;

trait CommandHandle
{
  /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $function =  $this->argument('function');
            $is_function = false;

            while (!$is_function) {
                $function = str_replace('-', '_', $function);
                if (method_exists(__CLASS__, $function)) break;
                if ($function != '') $this->error("Function {$function} not found.");
                $function =  $this->choice(
                    'Function to run?',
                    $this->functions,
                    0
                );
            }

            $this->$function();
        } catch (\Throwable $th) {
            $this->error('Execution Error: ' . $th->getMessage());
        }
    }
    public static   function title($value){
        return str_replace('_', ' ', ucwords($value));
    }

    public function validate($field, $default, $callback)
    {
        $validate = false;
        $value = $this->option($field);

        $title = self::title($field);
        while (!$validate) {

            if ($value == $default || !$validate) $value = $this->ask("Key in {$title} (Default '{$default}'):") ?? $value;

            if ($callback) {
                $validate = $this->$callback($value);
            } else {
               $validate = true;
            }
          
        }
        return $value;
    }

}
