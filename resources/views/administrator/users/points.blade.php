@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

<h1>Points</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <h3 class="ml-3 mb-3">Global Minimum Points</h3>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="table-responsive m-2">
            <form class="form-inline" action="{{ route('administrator.user.updateMinimum') }}" method="POST">
                @csrf
                <div class="form-group mx-sm-3 mb-2">
                  <label for="inputPassword2" class="sr-only">Points</label>
                  <input name="minimumPoint" type="text" class="form-control" id="inputPassword2" placeholder="Points" value="{{ $currentPoint->description }}">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
              </form>
        </div>
    </div>
</div>

@endsection


@push('script')

@endpush
