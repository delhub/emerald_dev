<?php

namespace App\Http\Controllers\Administrator\Outlet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\Outlet;
use App\Models\Globals\City;
use App\Models\Globals\Countries;
use App\Models\Globals\State;
use App\Models\Warehouse\Location\Wh_location;

class OutletController extends Controller
{
    public static $home;
    public static $header;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$home = 'administrator.outlet.index';
        self::$header = 'Outlet';

        self::$create = 'administrator.outlet.create';
        self::$store = 'administrator.outlet.store';

        self::$edit = 'administrator.outlet.edit';
        self::$update = 'administrator.outlet.update';

        self::$destroy = 'administrator.outlet.destroy';

        // link to view page
        // view('administrator.outlet.index');
    }


    public function index()
    {
        $tables = Outlet::orderBy('outlet_name', 'ASC')->paginate(10);
        $header = self::$header;
        return view(self::$home, compact('tables', 'header'));
    }

    public function create()
    {
        $countries = Countries::orderBy('country_name', 'ASC')->get();
        $states = State::orderBy('name', 'ASC')->get();
        $warehouseLocation = Wh_location::where('country_id', country()->country_id)->get();

        return view(self::$create, compact('countries', 'states', 'warehouseLocation'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'outlet_key' => 'required',
            'outlet_name' => 'required',
            'printer_email' => 'required',
            'country_id' => 'required',
            'contact_number' => 'required',
            'default_address' => 'required',
            'postcode' => 'required',
            'city_key' => 'required',
            'state_id' => 'required',
            'operation_time' => 'required',
        ]);

        $existingdata  = Outlet::where('outlet_name', $request->input('outlet_name'))->first();

        if (!$existingdata) {
            $data = new Outlet;
            $data->outlet_key = $request->input('outlet_key');
            $data->outlet_name = $request->input('outlet_name');
            $data->printer_email = $request->input('printer_email');
            $data->country_id = $request->input('country_id');
            $data->contact_number = $request->input('contact_number');
            $data->default_address = $request->input('default_address');
            $data->postcode = $request->input('postcode');
            $data->city_key = $request->input('city_key');
            $data->state_id = $request->input('state_id');
            $data->operation_time = $request->input('operation_time');

            $data->save();

            $outlet = Outlet::find($data->id);

            if ($request->file('outletImage')) {
                // Get existing default image.
                $defaultImages = $data->images->where('default', 1);

                // Delete if default image exists.
                if ($defaultImages->count() != 0) {
                    foreach ($defaultImages as $defaultImage) {
                        $defaultImage->delete();
                    }
                }

                // Store new default image.
                $image = $request->file('outletImage');
                $imageDestination = public_path('/storage/uploads/images/outlet/' . $data->id . '/');
                $imageName = $data->id . '-' . 'default' . '.' . $image->getClientOriginalExtension();
                $image->move($imageDestination, $imageName);

                // Create record in DB.
                $data->images()->create([
                    'path' => 'uploads/images/outlet/' . $data->id . '/',
                    'filename' => $imageName,
                    'default' => 1
                ]);
            }


            return redirect()->route(self::$home)->with('success', 'submit done.');
        } else {
            return back()->with(['error' => self::$header . ' already exist  !!! ']);
        }
    }

    public function edit($id)
    {
        $data = Outlet::find($id);

        $countries = Countries::orderBy('country_name', 'ASC')->get();
        $states = State::orderBy('name', 'ASC')->get();
        $cities  = City::orderBy('city_name', 'ASC')->get();
        $warehouseLocation = Wh_location::where('country_id', country()->country_id)->get();

        return view(self::$edit, compact('data', 'countries', 'states', 'cities', 'warehouseLocation'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'outlet_key' => 'required',
            'outlet_name' => 'required',
            'printer_email' => 'required',
            'country_id' => 'required',
            'contact_number' => 'required',
            'default_address' => 'required',
            'postcode' => 'required',
            'city_key' => 'required',
            'state_id' => 'required',
            'operation_time' => 'required',
        ]);
        $data = Outlet::find($id);

        $data->outlet_key = $request->input('outlet_key');
        $data->outlet_name = $request->input('outlet_name');
        $data->printer_email = $request->input('printer_email');
        $data->country_id = $request->input('country_id');
        $data->contact_number = $request->input('contact_number');
        $data->default_address = $request->input('default_address');
        $data->postcode = $request->input('postcode');
        $data->city_key = $request->input('city_key');
        $data->state_id = $request->input('state_id');
        $data->operation_time = $request->input('operation_time');

        $data->save();

        if ($request->file('outletImage')) {
            // Get existing default image.
            $defaultImages = $data->images->where('default', 1);

            // Delete if default image exists.
            if ($defaultImages->count() != 0) {
                foreach ($defaultImages as $defaultImage) {
                    $defaultImage->delete();
                }
            }

            // Store new default image.
            $image = $request->file('outletImage');
            $imageDestination = public_path('/storage/uploads/images/outlet/' . $data->id . '/');
            $imageName = $data->id . '-' . 'default' . '.' . $image->getClientOriginalExtension();
            $image->move($imageDestination, $imageName);

            // Create record in DB.
            $data->images()->create([
                'path' => 'uploads/images/outlet/' . $data->id . '/',
                'filename' => $imageName,
                'default' => 1
            ]);
        }

        return redirect()->route(self::$home)->with('success', self::$header . ' Updates');
    }

    public function destroy($id)
    {
        $data = Outlet::find($id);

        $data->delete();

        return redirect()->route(self::$home)->with('success', self::$header . ' Remove');
    }
}
