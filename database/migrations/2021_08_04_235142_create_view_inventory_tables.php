<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewInventoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW view_inventories_stock
        AS
        SELECT product_code,
        sum(case when inv_type = '8001' OR inv_type = '8010' then inv_amount else 0 end) as virtual_stock,
        sum(inv_amount) as physical_stock
        from wh_inventories
        GROUP BY product_code
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_product_stock');
    }
}
