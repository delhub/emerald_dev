@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection
<div class="cart-home">
  <div class="fml-container">
      <div class="subtitle-namelist {{ isset($directPayTables) ? 'd-none' : '' }}">
          <ul>
              <li><a href="/">Home</a></li>
              <li><span>/</span></li>
              <li><a href="#">Cart Page</a></li>
          </ul>
      </div>
      <section>
        <div class="fml-container ">
            <div class="row footer-btm">
                <div class="col-md-5">
                    <img class="thankyou-3dimg" src="{{asset('/images/formula/thankyou-bg.png')}}" alt="">
                </div>
                <div class="col-md-7">
                    <div class="thank-info {{ $purchase->purchase_type == 'Fpx' ? 'pb-2' : '' }}">
                        <div class="thankyou-text">
                            <h5>Your order has been placed. Thank you for shopping with us.</h5>
                            <h5 class="p-0"> You order number is {{ $purchaseNumberFormatted }}</h5>
                        </div>

                    </div>
                    <div class="continue-shop">
                        <div class="edit-opt-btn">
                        @if (Auth::check() && (Auth::user()->dealerInfo && !Auth::user()->dealerInfo->nric ))

                            {{-- <button class="cancel-btn btn-space btn btn-primary"> --}}
                                <a class="cancel-btn btn-space btn" href="/shop">Continue Agent Registration</a>
                                <br><br>
                                <h4 class="text-left"> Please continue fill in the rest of information</h4>

                            {{-- </button> --}}
                        @elseif (isset($directPayTables))

                            @if ($directPayTables->purchase_product_type == 'rbs')
                            <a class="cancel-btn btn-space btn" href="/shop">Login</a>
                            @else
                            {{-- <h5>Please click register button to continue Formula2u customer registration.</h5> --}}

                            <a class="cancel-btn btn-space btn mt-md-2" href="/register/?invoice={{ $purchaseNumberFormatted }}&agent_id={{ $dealer->account_id }}&agent_name={{ $dealer->full_name }}&details={{ $directPayTables->customer_details }}">Click to continue customer registration</a>
                            @endif


                        @else
                        <a class="cancel-btn btn-space btn" href="/shop">Continue Shopping</a>
                            {{-- <button class="cancel-btn btn-space btn btn-primary">Download Invoice</button> --}}
                            {{-- <button class="savechg-btn btn-primary">View Orders</button> --}}
                        @endif
                        </div>

                        {{-- <div class="shop-for-text">
                            <h5>Continue <span>Shop For</span></h5>
                        </div> --}}
                        {{-- <div class="row">
                            <div class="continue-shop-category col-md-4">
                                <a class="thank-adult" href="#">
                                    <img src="{{asset('/images/icons/icon-eldest.png')}}" alt="">
                                    <p>Elderly</p>
                                </a>
                            </div>
                            <div class="continue-shop-category col-md-4">
                                <a class="thank-adult" href="#">
                                    <img src="{{asset('/images/icons/icon-adult.png')}}" alt="">
                                    <p>Adult</p>
                                </a>
                            </div>
                            <div class="continue-shop-category col-md-4">
                                <a class="thank-adult" href="#">
                                    <img src="{{asset('/images/icons/icon-kids.png')}}" alt="">
                                    <p>Kids</p>
                                </a>
                            </div>
                        </div> --}}

                    </div>
                    <br>
                    @if ($purchase->purchase_type == 'Fpx')
                    <div class="card bg-light p-md-3 col-md-10 shadow-sm p-3 mb-5 bg-body rounded mt-md-3">
                        <div class="row">
                            <div class="col-md-6 mt-2 mt-md-0">
                                <p class="m-0 fpxTitle">Transaction Date and Time</p>
                                <p class="m-0 fpxDesc">{{ date_format(date_create($fpx->fpx_fpxTxnTime),"d-M-Y H:i:s A") }}</p>
                            </div>
                            <div class="col-md-6 mt-2 mt-md-0">
                                <p class="m-0 fpxTitle">Transaction ID</p>
                                <p class="m-0 fpxDesc">{{ $fpx->fpx_fpxTxnId }}</p>
                            </div>
                        </div>
                        <div class="row my-md-3">
                            <div class="col-md-6 mt-2 mt-md-0">
                                <p class="m-0 fpxTitle">Amount</p>
                                <p class="m-0 fpxDesc">RM {{ number_format($fpx->fpx_txnAmount,2) }}</p>
                            </div>
                            <div class="col-md-6 mt-2 mt-md-0">
                                <p class="m-0 fpxTitle">Bank</p>
                                <p class="m-0 fpxDesc">{{ $banks ? $banks->bank_name : $fpx->fpx_buyerBankId }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mt-2 mt-md-0">
                                <p class="m-0 fpxTitle">Invoice Number</p>
                                <p class="m-0 fpxDesc">{{ $purchaseNumberFormatted }}</p>
                            </div>
                            <div class="col-md-6 mt-2 mt-md-0">
                                <p class="m-0 fpxTitle">Status</p>
                                <p class="m-0 fpxDesc">{{ $responses ? $responses->description : $fpx->fpx_debitAuthCode }}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
    </section>
  </div>
</div>
@endsection



@push('style')
<style>
    .fpxTitle{
        font-size:16px;
        font-weight: 500;
    }

    .fpxDesc{
        font-size:14px;
        color: #000;
    }
</style>
@endpush


@push('script')
<script>

</script>
@endpush
