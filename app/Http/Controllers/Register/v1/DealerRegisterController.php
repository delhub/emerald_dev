<?php

namespace App\Http\Controllers\Register\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Registrations\Dealer\Registration;
use App\Rules\InvoiceRegistered;
use App\Rules\InvoiceCountry;
use App\Models\Globals\Employment;
use App\Models\Globals\Gender;
use App\Models\Globals\Marital;
use App\Models\Users\User;
use App\Models\Globals\PaymentGateway\MerchantID;
use App\Models\Globals\Race;
use App\Models\Globals\State;
use App\Models\Purchases\Order;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use App\Models\Globals\BankName;
use App\Models\Users\Customers\PaymentInfo;
use App\Models\Users\Dealers\DealerAddress;
use App\Models\Users\Dealers\DealerContact;
use App\Models\Users\Dealers\DealerEmployment;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\Dealers\DealerSpouse;
use App\Models\Users\Dealers\DealerBankDetails;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use App\Models\Users\UserInfo;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use App\Models\Dealers\DealerGroup;
use App\Http\Controllers\Register\CustomerRegisterController;
use App\Http\Controllers\BujishuAPI;
use App\Http\Controllers\Purchase\FpxGatewayController;
use App\Http\Controllers\Purchase\PurchaseController;
use App\Http\Controllers\Shop\CartController;
use App\Models\Categories\Category;
use App\Rules\CheckCustomerUpline;
use App\Rules\CheckIfCustomerAgent;
use App\Rules\CheckCustomerPassword;
use App\Models\Users;
use App\Models\Users\Dealers;
use Log;
use App\Models\Globals\City;
use App\Models\Globals\Products\Product;
use App\Models\Users\Dealers\DealerCountry;
use Cookie;
use App\Models\Globals\Countries;
use App\Http\Controllers\XilnexAPI;
use App\Models\Dealers\DealerDirectPay;
use App\Models\Globals\Cards\Issuer;
use App\Models\Users\Customers\Cart;
use App\Models\Users\UserCountry;
use App\Traits\FreeGiftraits;
use App\Traits\Voucher;
use BaconQrCode\Encoder\QrCode;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode as FacadesQrCode;
use stdClass;

class DealerRegisterController extends Controller
{
    use Voucher;
    use FreeGiftraits;
    /**
     * Show registration form.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {

        if (Auth::check()) {
            Auth::logout();
        }
        $country = country()->country_id;

        $packageCategory = Category::where('slug', 'agent-registration')->where('type', 'shop')->first();

        $packages = $packageCategory->products;

        $inv_placehold = ($country == 'MY') ? 'FWS'.date('y').'XX-XXXXXX' : '';

        Cookie::queue(Cookie::forget('addressCookie'));

        // if ($country == 'SG') {

        //launching
        $cities = City::where('country_id', 'MY')->get();
        $states = State::where('country_id', 'MY')->get();
        // return view('auth.register.v2.dealerSG', compact('country', 'packages', 'cities', 'states'));
        // } else {
        //     return view('auth.register.v2.dealer', compact('country', 'packages'));
        // }
        return view('auth.register.v2.dealer', compact('country', 'packages', 'inv_placehold'));
        //return abort(404);

    }

    public function showRegistrationFormSG(Request $request)
    {
        if (Auth::check()) {
            Auth::logout();
        }
        Cookie::queue(Cookie::forget('addressCookie'));
        $country = country()->country_id;

        $packageCategory = Category::where('slug', 'agent-registration')->where('type', 'shop')->first();

        $packages = $packageCategory->products;

        $inv_placehold = ($country == 'MY') ? 'FWS'.date('y').'XX-XXXXXX' : '';



        // if ($country == 'SG') {

        //launching
        $cities = City::where('country_id', 'MY')->get();
        $states = State::where('country_id', 'MY')->get();
        return view('auth.register.v2.dealerSG', compact('country', 'packages', 'cities', 'states'));
        // } else {
        //     return view('auth.register.v2.dealer', compact('country', 'packages'));
        // }
        // return view('auth.register.v2.dealer', compact('country', 'packages', 'inv_placehold'));
        //return abort(404);

    }

    // connect with deler info, dealer group

    public function getInvoiceRegister(Request $request)
    {
        //return $request->invoice;

        return $dealerRegistration = Registration::with(['dealerInfo.agentGroup'])
            ->where('invoice_number', $request->invoice)
            ->first();
    }
    /**
     * as
     */

    public function checkCustomerID(Request $request)
    {

        $customer_email = $request->id;
        $customer =  User::where('email', $customer_email)->first();

        if (!$customer) return response()->json(['message' => "Customer Email Not Found"], 500);

        $agent = DealerInfo::where('user_id', $customer->userInfo->user_id)->first();
        if ($agent && $agent->is_dealership) return response()->json(['message' => "Customer already Agent"], 500);

        //$name = $customer->userInfo->full_name;
        $data['status'] = 'OK';
        $data['message'] = 'Update Successful';
        $data['data'] = 'Email Correct';

        return response()->json($data, 200);
    }

    public function dealerRegister(Request $request)
    {
        $registrationRecord = Registration::where('invoice_number', $request->input('invoice_number'))->first();

        if ($request->bjs_customer == 0) {
            $request->validate([
                'email' => ['required', 'unique:users', 'max:255'],
                'password' => ['required', 'min:8'],
                'referrer_name' => ['required', 'max:255'],
                'invoice_number' => ['required', 'max:255', 'exists:dealer_registrations,invoice_number', new InvoiceRegistered, new InvoiceCountry],
                'referrer_id' => ['required', 'min:10', 'max:10', 'exists:dealer_infos,account_id'],
                'agent_group' => ['required'],
                // 'bjs_customer' => ['exists:user_infos,account_id', 'max:10'],
                // 'package' => ['required']
            ]);

            $user = CustomerRegisterController::createCustomer($request, true, $registrationRecord->country_id);

            $this->createDealer($request, $user, $registrationRecord);

            $credentials = array(
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            );
            $user->notes = null;
            $user->save();
            Auth::attempt($credentials);
        } else {

            $user = User::where('email', $request->bjs_customer_id)->first();
            if ($user) {
                $user->notes = 'incomplete';
                $user->save();
                // dd($user);

                $request->validate([
                    // 'email' => ['required', 'unique:users', 'max:255'],
                    // 'password' => ['required', 'min:8'],
                    'referrer_name' => ['required', 'max:255'],
                    'invoice_number' => ['required', 'max:255', 'exists:dealer_registrations,invoice_number', new InvoiceRegistered, new InvoiceCountry],
                    'bjs_customer_id' => ['required', 'exists:users,email', new CheckIfCustomerAgent($user)],
                    'referrer_id' => ['required', 'min:10', 'max:10', 'exists:dealer_infos,account_id', new CheckCustomerUpline($user)],
                    'password' => ['required', 'min:8', new CheckCustomerPassword($user)],

                    // 'package' => ['required']
                ]);

                $this->createDealer($request, $user, $registrationRecord);

                $credentials = array(
                    'email' => $request->input('bjs_customer_id'),
                    'password' => $request->input('password'),
                );
                //            dd('asd');
                $user->notes = null;
                $user->save();
                Auth::attempt($credentials);
            }

        }

        return redirect('/register-dealer/information');

    }

    public function dealerRegisterSG(Request $request)
    {

        // return $request;
        $registrationRecord = new Registration;
        $registrationRecord->package_id = '0';
        $registrationRecord->agent_id = '0';
        $registrationRecord->country_id = 'MY';

        if ($request->bjs_customer == 0) {
            $request->validate([
                'full_name' => ['required', 'max:255'],
                'contact_number_mobile' => [
                    'required',
                    'min:10'
                ],
                'email' => ['required', 'unique:users', 'max:255'],
                'password' => ['required', 'min:8'],
                'referrer_id' => ['numeric']
                // 'agent_group' => ['required'],
            ]);


            // xilnex data create client
            $data = [
                'name' => $request->input('full_name'),
                'email' => $request->input('email'),
                'phone' => $request->input('contact_number_mobile'),
                'category' => 'Personal'
            ];

            //XilnexAPI POS create client
            // $xilnex = XilnexAPI::createClient($data);
            // $request->xilnex_client_id = $xilnex->data->client->id;
            $user = CustomerRegisterController::createCustomer($request, true, 'MY');

            $this->createDealer($request, $user, $registrationRecord, true);

            $credentials = array(
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            );
            $user->notes = null;
            $user->save();
            Auth::attempt($credentials);
        } else {

            $user = User::where('email', $request->bjs_customer_id)->first();
            $user->notes = 'incomplete';
            $user->save();
            $request->validate([
                'bjs_customer_id' => ['required', 'exists:users,email', new CheckIfCustomerAgent($user)],
                'password' => ['required', 'min:8', new CheckCustomerPassword($user)],
            ]);

            $checkDealerExisting = DealerInfo::where('user_id', $user->id)->first();

            if ($checkDealerExisting == NULL) {
                $this->createDealer($request, $user, $registrationRecord, true);
            }

            $credentials = array(
                'email' => $request->input('bjs_customer_id'),
                'password' => $request->input('password'),
            );
            $user->notes = null;
            $user->save();
            Auth::attempt($credentials);
        }

        // $city = NULL;
        // if ($request->input('city') != NULL && $request->input('city_key') == 0) $city = $request->input('city');

        // $new_shipCookie = json_encode(array(
        //     'address_1' => $request->input('address_1'),
        //     'address_2' => $request->input('address_2'),
        //     'address_3' => $request->input('address_3'),
        //     'postcode' => $request->input('postcode'),
        //     'city' => $city,
        //     'city_key' => 'SGP_AMK',
        //     'city_name' => City::where('city_key', 'SGP_AMK')->first(),
        //     'state_name' => State::find(22),
        //     'state_id' => 22,
        //     'name' => $request->input('full_name'),
        //     'mobile' => $request->input('contact_number_mobile')
        // ));

        // Cookie::queue('addressCookie', $new_shipCookie, 3600);

        return redirect('/');
    }

    public function createDealer($request, $user, $registrationRecord, $launching = false)
    {
        $dealerInfo = $user->dealerInfo;

        // Generating new dealer account id.
        $largestDealerId = 0;
        if (DealerInfo::all()->count() == 1) {
            $largestDealerId = 3911000501;
        } else {
            $largestDealerId = DealerInfo::largestDealerId() + 1;
        }

        // Dealer_infos table.
        $dealerInfo = new DealerInfo;
        $dealerInfo->user_id = $user->id;
        $dealerInfo->account_id = $largestDealerId;
        $dealerInfo->account_status = 3;
        $dealerInfo->full_name = ($request->input('full_name') != null) ? $request->input('full_name') : NULL;
        $dealerInfo->gender_id = 0;
        $dealerInfo->race_id = 0;
        $dealerInfo->marital_id = 0;
        $dealerInfo->referrer_id = ($request->input('referrer_id') != null) ? $request->input('referrer_id') : 1010;
        $dealerInfo->referrer_name = ($request->input('referrer_name') != null) ? $request->input('referrer_name') : '';
        $dealerInfo->group_id = ($request->input('agent_group') != null) ? $request->input('agent_group') : '0';
        $dealerInfo->package_type = $registrationRecord->package_id;
        $dealerInfo->save();

        if (!$launching) {

            $dealerCountry = new DealerCountry;
            $dealerCountry->account_id = $largestDealerId;
            $dealerCountry->country_id = $registrationRecord->country_id;
            $dealerCountry->dealer_id = $registrationRecord->country_id . $largestDealerId;
            $dealerCountry->referrer_id = ($request->input('referrer_id') != null) ? $registrationRecord->country_id . $request->input('referrer_id') : $registrationRecord->country_id . '1010';
            $dealerCountry->group_id = ($request->input('agent_group') != null) ? $request->input('agent_group') : '0';
            $dealerCountry->save();


            $registrationRecord->agent_id = $largestDealerId;
            $registrationRecord->save();

            // GENERATE VOUCHER for agent registration 14-07-2021
            $gen_voucher = $this->generateVoucherAgentRegistration($registrationRecord);
        }
        $user->userInfo->group_id = ($request->input('agent_group') != null) ? $request->input('agent_group') : '0';
        $user->userInfo->account_status = 1;
        $user->userInfo->user_level = 6001;
        $user->userInfo->referrer_id = $largestDealerId;
        $user->userInfo->save();
        $userCountry = $user->userInfo->userCountries->where('country_id', country()->country_id)->first();
        if (!$userCountry ){
            $userCountry = new UserCountry();
            $userCountry->account_id = $user->userInfo->account_id;
            $userCountry->country_id = country()->country_id;
        }
        $userCountry->group_id = $user->userInfo->group_id;
        $userCountry->referrer_id = country()->country_id . $largestDealerId;
        $userCountry->save();

        // Dealer_address table. (Shipping Address)
        $dealerAddressShipping = new DealerAddress;
        $dealerAddressShipping->account_id = $dealerInfo->account_id;
        $dealerAddressShipping->address_1 = ($request->input('address_1') != null) ? $request->input('address_1') : NULL;
        $dealerAddressShipping->address_2 = ($request->input('address_2') != null) ? $request->input('address_2') : NULL;
        $dealerAddressShipping->address_3 = ($request->input('address_3') != null) ? $request->input('address_3') : NULL;
        $dealerAddressShipping->postcode =  ($request->input('postcode') != null) ? $request->input('postcode') : NULL;
        $dealerAddressShipping->city = ($request->input('city') != NULL && $request->input('city_key') == 0) ? $request->input('city_key') : NULL;
        $dealerAddressShipping->city_key = ($request->input('city_key') != null) ? $request->input('city_key') : 0;
        $dealerAddressShipping->state_id = ($state = $request->input('state')) ? $state : 14;
        $dealerAddressShipping->is_shipping_address = 1;
        $dealerAddressShipping->is_residential_address = 0;
        $dealerAddressShipping->is_mailing_address = 0;
        $dealerAddressShipping->save();

        // Dealer_address table. (Mailing Address)
        $dealerAddressMailing = new DealerAddress;
        $dealerAddressMailing->account_id = $dealerInfo->account_id;
        $dealerAddressMailing->state_id = 14;
        $dealerAddressMailing->is_shipping_address = 0;
        $dealerAddressMailing->is_residential_address = 0;
        $dealerAddressMailing->is_mailing_address = 1;
        $dealerAddressMailing->save();

        // Dealer_contacts table (Mobile).
        $dealerContactMobile = new DealerContact;
        $dealerContactMobile->account_id = $largestDealerId;
        $dealerContactMobile->contact_num = ($request->input('contact_number_mobile') != null) ? $request->input('contact_number_mobile') : NULL;
        $dealerContactMobile->is_mobile = 1;
        $dealerContactMobile->save();

        // Dealer_contacts table (Home).
        $dealerContactHome = new DealerContact;
        $dealerContactHome->account_id = $largestDealerId;
        $dealerContactHome->is_home = 1;
        $dealerContactHome->save();

        // Dealer_spouse table.
        $dealerSpouse = new DealerSpouse;
        $dealerSpouse->account_id = $largestDealerId;
        $dealerSpouse->save();

        // Dealer_employment table
        $dealerEmployment = new DealerEmployment;
        $dealerEmployment->account_id = $largestDealerId;
        $dealerEmployment->company_state_id = 0;
        $dealerEmployment->save();


        $user->assignRole('dealer');
    }
    /**
     * Dealer information GET.
     */
    public function dealerInformationRegisterView(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $message = $request->message ?? '';
        // Redirect to filling in more info.
        $genders = Gender::all();
        $races = Race::all();
        $maritals = Marital::all();
        $employments = Employment::all();
        $country = country()->country_id;

        $bankNames = BankName::where('type','agent')->where('country_id','MY')->get();
        $cities = City::where('country_id', $country)->where('state_id', $user->userInfo->shippingAddress->state_id)->get();
        $states = State::where('country_id', $country)->get();


        return view('auth.register.v1.dealer-info')
            ->with('genders', $genders)
            ->with('races', $races)
            ->with('maritals', $maritals)
            ->with('country', $country)
            ->with('states', $states)
            ->with('employments', $employments)
            ->with('bankNames', $bankNames)
            ->with('user', $user)
            ->with('cities', $cities)
            ->with('message', $message);
    }

    /**
     * Dealer information POST.
     */
    public function dealerInformationRegister(Request $request)
    {
        Validator::make($request->all(),
        [
            'employment_id' => ['required'],
        ],[
            'employment_id.required' => 'Please select employment status'
        ])->validate();
        $user = User::find(Auth::user()->id);
        $dealer_info = DealerInfo::find($user->id);
        $dealer_infos = DealerInfo::all();
        foreach ($dealer_infos as $dealerinfo) {
            if($dealerinfo->nric == $request->input('nric') && $dealerinfo->account_id != $dealer_info->account_id) {
                $request->validate([
                    'nric' => ['required', 'unique:dealer_infos'],
                ]);
            }
        }
        // $request->validate([
        //     'employment_id' => 'required',
        // ]);

        //  xilnex data create client
        $data = [
            'name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('contact_number_mobile'),
            'category' => 'Personal'
        ];
        $user = User::find(Auth::user()->id);


        // User
        $userInfo = UserInfo::find($user->id);
        $userInfo->full_name = $request->input('full_name');
        $userInfo->nric = $request->input('nric');
        $userInfo->referrer_name = $request->input('full_name');
        $userInfo->save();

        $userAddressBilling = $userInfo->shippingAddress;
        $userAddressBilling->address_1 = $request->input('address_1');
        $userAddressBilling->address_2 = $request->input('address_2');
        $userAddressBilling->address_3 = $request->input('address_3');
        $userAddressBilling->postcode = $request->input('postcode');
        $userAddressBilling->city = $request->input('city');
        $userAddressBilling->city_key = $request->input('city_key');
        $userAddressBilling->state_id = ($request->input('state')) ? $request->input('state') : 14;
        $userAddressBilling->save();

        $userAddressMailing = $userInfo->mailingAddress;
        $userAddressMailing->address_1 = $request->input('address_1');
        $userAddressMailing->address_2 = $request->input('address_2');
        $userAddressMailing->address_3 = $request->input('address_3');
        $userAddressMailing->postcode = $request->input('postcode');
        $userAddressMailing->city = $request->input('city');
        $userAddressMailing->city_key = $request->input('city_key');
        $userAddressMailing->state_id =  ($request->input('state')) ? $request->input('state') : 14;
        $userAddressMailing->save();

        if ($request->input('contact_number_home') != null) {
            $userContactHome = $userInfo->homeContact;
            $userContactHome->contact_num = $request->input('contact_number_home');
            $userContactHome->save();
        }

        $userContactMobile = $userInfo->mobileContact;
        $userContactMobile->contact_num = $request->input('contact_number_mobile');
        $userContactMobile->save();
        // End User

        // Dealer
        $dealerInfo = DealerInfo::find($user->id);
        $dealerInfo->full_name = $request->input('full_name');
        $dealerInfo->nric = $request->input('nric');
        $dealerInfo->date_of_birth = $request->input('date_of_birth');
        $dealerInfo->gender_id = $request->input('gender_id');
        $dealerInfo->race_id = $request->input('race_id');
        $dealerInfo->marital_id = $request->input('marital_id');
        $dealerInfo->save();

        $dealerInfo->shippingAddress ? $dealerAddressShipping = $dealerInfo->shippingAddress : $dealerAddressShipping = new DealerAddress;
        $dealerInfo->shippingAddress ? : $dealerAddressShipping->account_id = $dealerInfo->account_id and $dealerAddressShipping->is_shipping_address = 1;
        $dealerAddressShipping->address_1 = $request->input('address_1');
        $dealerAddressShipping->address_2 = $request->input('address_2');
        $dealerAddressShipping->postcode = $request->input('postcode');
        $dealerAddressShipping->city = $request->input('city');
        $dealerAddressShipping->city_key = ($request->input('city_key') != NULL) ? $request->input('city_key') : 0;
        $dealerAddressShipping->state_id =  ($request->input('state')) ? $request->input('state') : 14;
        $dealerAddressShipping->save();

        $dealerInfo->billingAddress ? $dealerAddressMailing = $dealerInfo->billingAddress : $dealerAddressMailing = new DealerAddress;
        $dealerInfo->billingAddress ? : $dealerAddressMailing->account_id = $dealerInfo->account_id and $dealerAddressMailing->is_mailing_address = 1;
        $dealerAddressMailing->address_1 = $request->input('address_1');
        $dealerAddressMailing->address_2 = $request->input('address_2');
        $dealerAddressMailing->postcode = $request->input('postcode');
        $dealerAddressMailing->city = $request->input('city');
        $dealerAddressMailing->city_key = $request->input('city_key');
        $dealerAddressMailing->state_id =  ($request->input('state')) ? $request->input('state') : 14;
        $dealerAddressMailing->save();

        if ($request->input('contact_number_home')) {
            $dealerInfo->dealerHomeContact ? $dealerContactHome = $dealerInfo->dealerHomeContact : $dealerContactHome = new DealerContact;
            $dealerInfo->dealerHomeContact ? : $dealerContactHome->account_id = $dealerInfo->account_id and $dealerContactHome->is_home = 1;
            $dealerContactHome->contact_num = $request->input('contact_number_home');
            $dealerContactHome->save();
        }

        $dealerInfo->dealerMobileContact ? $dealerContactMobile = $dealerInfo->dealerMobileContact : $dealerContactMobile = new DealerContact;
        $dealerInfo->dealerMobileContact ? : $dealerContactMobile->account_id = $dealerInfo->account_id and $dealerContactMobile->is_mobile = 1;
        $dealerContactMobile->contact_num = $request->input('contact_number_mobile');
        $dealerContactMobile->save();

        if ($dealerInfo->marital_id == 2) {
            $dealerInfo->dealerSpouse ? $dealerSpouse = $dealerInfo->dealerSpouse : $dealerSpouse = new DealerSpouse;
            $dealerInfo->dealerSpouse ? : $dealerSpouse->account_id = $dealerInfo->account_id;
            $dealerSpouse->spouse_name = $request->input('spouse_full_name');
            $dealerSpouse->spouse_nric = $request->input('spouse_nric');
            $dealerSpouse->spouse_date_of_birth = $request->input('spouse_date_of_birth');
            $dealerSpouse->spouse_occupation = $request->input('spouse_occupation');
            $dealerSpouse->spouse_contact_office = $request->input('spouse_contact_office');
            $dealerSpouse->spouse_contact_mobile = $request->input('spouse_contact_mobile');
            $dealerSpouse->spouse_email = $request->input('spouse_email');
            $dealerSpouse->save();
        }

        $dealerInfo->employmentAddress ? $dealerEmployment = $dealerInfo->employmentAddress : $dealerEmployment = new DealerEmployment;
        $dealerInfo->employmentAddress ? : $dealerEmployment->account_id = $dealerInfo->account_id;
        $dealerEmployment->employment_type = ($request->input('employment_id')) ? $request->input('employment_id') : 1;
        $dealerEmployment->company_name = $request->input('employment_name');
        $dealerEmployment->company_address_1 = $request->input('company_address_1');
        $dealerEmployment->company_address_2 = $request->input('company_address_2');
        $dealerEmployment->company_postcode = $request->input('company_postcode');
        $dealerEmployment->company_city = $request->input('company_city');
        $dealerEmployment->company_city_key = $request->input('company_city_key');
        $dealerEmployment->company_state_id =  ($request->input('company_state')) ? $request->input('company_state') : 14;
        $dealerEmployment->save();

        $dealer_bank_details = $dealerInfo->dealerBankDetails;




        $bjs_user_id = $user->userInfo->account_id;
        $name = addslashes($request->input('full_name'));
        $upline_agent_id = $dealerInfo->referrer_id;
        $ic = $request->input('nric');
        $email = $user->email;
        $referral_name = $dealerInfo->referrer_name;
        $group_id = (isset($dealerInfo->group_id)) ? $dealerInfo->group_id : 0;
        $create_bjs_agent = 'BHA';
        $agent_id = $dealerInfo->account_id;
        $bp_award = (isset($dealerInfo->agentPackage)) ? $dealerInfo->agentPackage->package_bp : 0;

        $user->notes !== null ? : BujishuAPI::createUser($bjs_user_id, $name, $upline_agent_id, $ic, $email, $referral_name, $group_id, $create_bjs_agent, $agent_id, $bp_award);


        if (!$dealer_bank_details) {
            $dealer_bank_details = new DealerBankDetails();
            $dealer_bank_details->account_id = $dealerInfo->account_id;
        }

        $dealer_bank_details->bank_code = ($bank_Code = $request->input('bank_name')) ? $bank_Code : '';
        $dealer_bank_details->bank_acc = ($bank_acc = $request->input('bank_account_number')) ? $bank_acc : 0;
        $dealer_bank_details->bank_acc_name = ($bank_acc_name = $request->input('account_holder_name')) ? $bank_acc_name : "";
        $dealer_bank_details->account_regid = ($account_regid = $request->input('account_regid')) ? $account_regid : "";
        $dealer_bank_details->save();

        $user->notes = null;
        $user->save();

        // return 'ok';

        $request->message = 'success';
        return $this->dealerInformationRegisterView($request);
    }

    public function directPayRegistration(Request $request)
    {
        $directPayTables = DealerDirectPay::where('uuid_link', $request->directID)->first();

        $address = json_decode($directPayTables->customer_details);


        $shippingAddress = new stdClass();
        $shippingAddress->name = $address->name;
        $shippingAddress->mobile  = $address->mobile;
        $shippingAddress->address_1 = $address->address_1;
        $shippingAddress->address_2 = $address->address_2;
        $shippingAddress->address_3 = $address->address_3;
        $shippingAddress->postcode = $address->postcode;
        $shippingAddress->city = $address->city;
        $shippingAddress->city_key = $address->city_key;
        // $shippingAddress->city_name = $address->city;
        $shippingAddress->state_name =  $address->state_name;
        $shippingAddress->state_id =  $address->state_id;
        $shippingAddress->country_id =  'MY';

        // dd($address,$shippingAddress);

        $session = Session::get('bjs_cart_discount');

        // return $shippingAddress;
        $voucher = (array)json_decode($directPayTables->voucher);

        $session = Session::put('bjs_cart_discount', $voucher);

        Session::put('bjs_cart_delivery_method', $directPayTables->delivery_method);
        Session::put('bjs_cart_delivery_outletId', $directPayTables->outlet_id);

        $cartsItems['issuer'] = Issuer::all();

        $cookie = array();
        $cookie["name"] = $address->name;
        $cookie["mobile"] = $address->mobile;
        $cookie["address_1"] =  $address->address_1;
        $cookie["address_2"] =  $address->address_2;
        $cookie["address_3"] =  $address->address_3;
        $cookie["postcode"] =  $address->postcode;
        $cookie["city"] = $address->city;
        $cookie["city_key"] =  $address->city_key;
        $cookie["city_name"] =  $address->city;
        $cookie["state_name"] =  $address->state_name;
        $cookie["state_id"] =  $address->state_id;
        $cookie["country_id"] =  'MY';

        $directpayData = array();
        $directpayData['cookie'] = $cookie;
        $directpayData['shippingAddress'] = $shippingAddress;


        $purchaseController = new PurchaseController();
        return $purchaseController->paymentOption($request,$directpayData);

    }

    public function savedirectPayData(Request $request)
    {

        //
        $user = Auth::user()->id;
        $cookies = json_decode(Cookie::get('addressCookie'));

        $customerAddress = array();
        $customerAddress['address_1'] = $cookies->address_1;
        $customerAddress['address_2'] = $cookies->address_2;
        $customerAddress['address_3'] = $cookies->address_3;
        $customerAddress['city'] = $cookies->city;
        $customerAddress['city_key'] = $cookies->city_key;
        $customerAddress['mobile'] = $cookies->mobile;
        $customerAddress['name'] = $cookies->name;
        $customerAddress['postcode'] = $cookies->postcode;
        $customerAddress['state_id'] = $cookies->state_id;
        $customerAddress['state_name'] = $cookies->state_name->name;
        // $customerAddress['shipping_fee'] = number_format(($request->cartItems['shippingFee']['amount'] / 100), 2);
        // $customerAddress['rebate_fee'] = 0;

        // $customerAddress = [
        //     'address_1' => $cookies->address_1,
        //     'address_2' => $cookies->address_2,
        //     'address_3' => $cookies->address_3,
        //     'city' => $cookies->city,
        //     'city_key' => $cookies->city_key,
        //     'mobile' => $cookies->mobile,
        //     'name' => $cookies->name,
        //     'postcode' => $cookies->postcode,
        //     'state_id' => $cookies->state_id,
        //     'state_name' => $cookies->state_name->name,
        // ];

        $selectedCarts = array_keys($request->cartItems['items']);


        // foreach ($selectedCarts as $selectedCart) {
        //     $cart = Cart::where('id',$selectedCart)->first();
        //     $cart->status = 2004;
        //     $cart->save();
        // }

        $sessionDelivery = Session::get('bjs_cart_delivery_method');
        $sessionOutlet = Session::get('bjs_cart_delivery_outletId');

        $uuid = self::globalSaveDirectPay($selectedCarts, $customerAddress, $request, $user, $sessionDelivery, $sessionOutlet);
        // }

        $result = array();
        $url = URL::signedRoute('agent.direct.pay', ['directID' => $uuid]);
        $parts = parse_url($url);
        parse_str($parts['query'], $query);

        $result['paymentLink'] = '<img src="data:images/png;base64,' . base64_encode(FacadesQrCode::format('png')->size(100)->generate($url)) . '">';
        $result['inputLink'] = $request->root() . '/' . 'direct-pay/' . $uuid . '/?signature=' . $query['signature'];

        return $result;
    }

    public static function globalSaveDirectPay($selectedCarts, $customerAddress, $request, $user, $sessionDelivery, $sessionOutlet, $purchase_product_type = 'normal')
    {
        $uuid = uniqid();
        //
        $directPayTable = new DealerDirectPay();
        $directPayTable->uuid_link = $uuid;
        $directPayTable->user_id = $user;
        $directPayTable->delivery_method = $sessionDelivery;
        $directPayTable->outlet_id = $sessionOutlet ? $sessionOutlet : 0;
        $directPayTable->purchase_product_type = $purchase_product_type;
        $directPayTable->cart_id = json_encode($selectedCarts);
        $directPayTable->customer_details = json_encode($customerAddress);
        $directPayTable->shipping_fee = json_encode($request->cartItems['shippingFee']);
        $directPayTable->voucher = Session::get('bjs_cart_discount') != null ? json_encode(Session::get('bjs_cart_discount')) : null;
        $directPayTable->save();

        return $uuid;
    }

    public function checkDirectPay(Request $request)
    {

        $request->directID;

        $directPayTables = DealerDirectPay::where('uuid_link', $request->directID)->first();

        $carts = Cart::whereIn('id', json_decode($directPayTables->cart_id))->whereIn('selected', [0, 1])->get();

        if (in_array(2003, $carts->pluck('status')->toArray()) || in_array(2002, $carts->pluck('status')->toArray())) {
            $expiredLink = true;
        } else {
            $expiredLink = false;
        }

        if ($directPayTables->purchase_id == null || !$expiredLink) {
            return 'true';
        } else {
            return 'false';
        }
    }
}
