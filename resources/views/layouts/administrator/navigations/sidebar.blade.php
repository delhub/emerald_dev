@php
    $panels = \App\Models\Users\Panels\PanelInfo::all();
@endphp

<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav" style="margin-bottom: 200px;">
            <li class="nav-item">
                <a class="nav-link" href="/administrator">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-title">Shop</li>
            {{-- <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-drop"></i> Products</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/products" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Products
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-drop"></i> Products By Panel</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/products/panels" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Products
                        </a>
                    </li>
                </ul>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{route('administrator.v1.products.panels.price')}}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Products Prices
                        </a>
                    </li>
                </ul>
            </li> --}}

            @hasanyrole('admin_product|administrator|formula_pic')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-drop"></i>Product</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.product') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Product
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.product.shippingCategory') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Attribute Shipping Category
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.v1.products.panels.price') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Attribute Prices
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.rbs.index') }}" class="nav-link">
                                <i class="nav-icon icon-drop"></i>Product RBS
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.product.product-restriction') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Product Restriction
                            </a>
                        </li>
                    </ul>
                </li>
            @endhasanyrole

            @role('administrator|formula_pic')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-drop"></i> Categories</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/administrator/categories" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Categories
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.product.brand') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Brand
                            </a>
                        </li>
                    </ul>
                    @role('management')
                        {{-- @if (!\App::environment('prod')) --}}
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="{{ route('administrator.products.reports') }}" class="nav-link">
                                    <i class="nav-icon icon-puzzle"></i> Brand/Product Report
                                </a>
                            </li>
                        </ul>
                        {{-- @endif --}}
                    @endrole
                </li>

                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Shipping
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.zones.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Zones / Rates
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.cities.index') }}" class="nav-link">
                                <i class="nav-icon icon-drop"></i>Cities
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.shipping-installations.ship_category.index') }}"
                                class="nav-link">
                                <i class="nav-icon icon-drop"></i>Ship Categories
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Outlet
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.outlet.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Outlet
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('administrator.courier.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>Courier
                    </a>
                </li>
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Users
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/administrator/users" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Users
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/administrator/users/membership" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Membership
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/administrator/users/points" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Points
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Panel
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.panels.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Panel
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole
            @hasanyrole('administrator|account_manager|formula_pic')
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Payments
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.payment.summary') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Summary Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> All Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=pending" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Pending Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=created" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Record Created
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=failed" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Failed Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=on-hold" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> On Hold Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=paid" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Paid Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=comm-cpcb" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> CP/CB Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=cancel" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Cancelled Payments
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/administrator/payments?paymentStatus=refund" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Refunded Payments
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Account Operation
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.account.sql.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> SQL Export
                            </a>
                        </li>

                    </ul>
                </li>
            @endhasanyrole
            @role('administrator|formula_pic')
                <li class="nav-item">
                    <a href="{{ route('administrator.sidemenus.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>Side Menu
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('administrator.testbanners.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>Banner
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('administrator.faq.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>FAQ
                    </a>
                </li>

                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>
                        Virtual Quantity Stock (VQS)
                    </a>
                    {{-- <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.vqs.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Agent
                            </a>
                        </li>
                    </ul> --}}

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.vqs.pending.approval') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pending Approval
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole

            @hasanyrole('admin_orderTracking|administrator|formula_pic')
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Order Tracking
                    </a>
                    {{-- <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a href="{{ route('administrator.ordertracking.summary') }}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Order Tracking Summary (Panel)
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.summary.customer') }}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Order Tracking Summary (Customer)
                            </a>
                        </li>
                    </ul> --}}

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.orders') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pick
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.old-detail') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Order Tracking Detail
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.rbs-prepaid') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>RBS - Prepaids
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.rbs-postpaid') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>RBS - Postpaid
                            </a>
                        </li>
                    </ul>
                    @hasrole('management')
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="{{ route('administrator.ordertracking.logisticReport') }}" class="nav-link">
                                    <i class="nav-icon icon-puzzle"></i>Logistic report
                                </a>
                            </li>
                        </ul>
                    @endhasrole

                </li>
            @endhasanyrole
            @hasanyrole('admin_orderTracking|administrator')
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Orders Pick
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> ALL Batch
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBin.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Bin Number
                            </a>
                        </li>
                    </ul>

                    {{-- <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.orders-pick') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Orders Pick
                            </a>
                        </li>
                    </ul> --}}

                    {{-- <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.old-detail') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Order Tracking Detail
                            </a>
                        </li>
                    </ul> --}}
                </li>
            @endhasanyrole

            @role('administrator')
                {{-- <li class="nav-item">
                <a href="{{ route('administrator.tax.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>Tax
                </a>
            </li> --}}

                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Discount
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.discount.selector') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Product Selector
                            </a>
                        </li>
                    </ul>
                    {{-- code --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.discount.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Code-list
                            </a>
                        </li>
                    </ul>
                    {{-- rule --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.discount.rule') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Rule-list
                            </a>
                        </li>
                    </ul>

                    {{-- <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.discount.summary') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Summary
                        </a>
                    </li>
                </ul> --}}
                </li>
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Country
                    </a>

                    {{-- Country --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.countries.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Country
                            </a>
                        </li>
                    </ul>

                    {{-- Mark Up Category --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.products.v1.panel.markup-category.index') }}"
                                class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Mark Up Category
                            </a>
                        </li>
                    </ul>

                </li>

                <li class="nav-item">
                    <a href="{{ route('administrator.tax.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>Tax
                    </a>
                </li>

                <li class="nav-item nav-dropdown">
                    <a href="{{ route('administrator.import.index') }}" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Import
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.import.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Import Data
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.import.stock') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Import WH Stock
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.import.weight') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Import Weight
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.import.manufacture') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Import Manufacture
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.import.price') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> Import Price
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Freegift
                    </a>

                    {{-- index --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('freegift.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>index
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('freegift.create') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Create New
                            </a>
                        </li>
                    </ul>

                </li>
            @endrole

            @role('redemption')
                <li class="nav-item">
                    <a href="{{ route('administrator.redemption') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>
                        Redemption
                    </a>
                </li>
            @endrole

            @role('voucher_summary')
                {{-- <li class="nav-item nav-dropdown">
            <a href="#" class="nav-link nav-dropdown-toggle">
                <i class="nav-icon icon-drop"></i> Discount
            </a>

            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a href="{{ route('administrator.discount.summary') }}" class="nav-link">
                    <i class="nav-icon icon-puzzle"></i> Summary
                    </a>
                </li>
            </ul>
        </li> --}}
            @endrole

            @role('wh_warehouse||wh_supervisor|formula_pic')
                {{-- <hr> --}}
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Warehouse
                    </a>

                    {{-- New Batch --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.v1.products.warehouse.newBatch') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Add New Batch
                            </a>
                        </li>
                    </ul>

                    {{-- Batch List --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.v1.products.warehouse.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Batch List
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.v1.products.warehouse.batch-expiry') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Batch Expiry
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.v1.products.warehouse.label') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Batch Label
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.warehouse.inventory.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Inventories
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.warehouse.inventory.overall') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Inventories Overall
                            </a>
                        </li>
                    </ul>
                    @role('wh_supervisor')
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="{{ route('administrator.warehouse.inventory.overall-management') }}"
                                    class="nav-link">
                                    <i class="nav-icon icon-puzzle"></i> FML Inventory Summary
                                </a>
                            </li>

                            {{-- <li class="nav-item">
                        <a href="{{route('administrator.warehouse.inventory.overall-management-v2')}}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> FML Actual Stock
                        </a>
                    </li> --}}
                        </ul>
                    @endrole

                    {{-- Batch List --}}
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.v1.products.warehouse.batch-item-list') }}"
                                class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Batch Item List
                            </a>
                        </li>
                    </ul>

                </li>

                {{-- Warehouse Order --}}
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Warehouse Admin
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.orders') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Order Tracking
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.ordertracking.detail') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Order Tracking Detail
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pick Pack Batch
                            </a>
                        </li>
                    </ul>
                </li>

                @role('wh_supervisor')
                    {{-- Location --}}
                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="nav-icon icon-drop"></i>Location
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="{{ route('warehouse.location.index') }}" class="nav-link">
                                    <i class="nav-icon icon-puzzle"></i>Location
                                </a>
                            </li>
                        </ul>
                    </li>

                    {{-- User --}}
                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="nav-icon icon-drop"></i>User
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="{{ route('warehouse.user.index') }}" class="nav-link">
                                    <i class="nav-icon icon-puzzle"></i>Warehouse User
                                </a>
                            </li>
                        </ul>
                    </li>
                @endrole

                {{-- Stock Adjustment --}}
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Stock Adjustment
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.stockAdjustment.adjust') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Adjustment
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.stockAdjustment.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i> List
                            </a>
                        </li>
                    </ul>
                </li>

            @endrole

            @role('wh_transferrer||wh_supervisor')
                {{-- Stock Transfer --}}
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Stock Transfer
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.StockTransfer.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Stock Transfer
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.StockTransfer.stockTransferList') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Transfer List
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.StockTransfer.create-box') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Create New Box
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.StockTransfer.box-list') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Transfer Box List
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole

            @role('wh_receiver||wh_supervisor')
                {{-- Stock Transfer --}}
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Stock Receive
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.StockTransfer.receive') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Stock Receive
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole



            @role('wh_picker||wh_supervisor')
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Orders Pick
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.start') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pick Start
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.end') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pick END
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole


            @role('wh_sorter||wh_supervisor')
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Orders Sort
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.sortStart') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Sort Start
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.sortEnd') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Sort END
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole


            @role('wh_packer||wh_supervisor')
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i> Orders Pack
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.pack-start') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pack Start
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('administrator.pickBatch.packEnd') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Pack End
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole

            @role('wh_b_unbundle||wh_supervisor')
                {{-- Bundle/Unbundle --}}
                <li class="nav-item nav-dropdown">
                    <a href="#" class="nav-link nav-dropdown-toggle">
                        <i class="nav-icon icon-drop"></i>Bundle/Unbundle
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.b_unbundle.index') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>Bundle Unbundle
                            </a>
                        </li>
                    </ul>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route('warehouse.b_unbundle.list') }}" class="nav-link">
                                <i class="nav-icon icon-puzzle"></i>B Unbundle List
                            </a>
                        </li>
                    </ul>
                </li>
            @endrole
            @hasanyrole('export_manager|account_manager|administrator|formula_pic')
            <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i>Export Manager
                </a>
            @role('export_manager|account_manager|administrator')
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                    <a href="{{ route('csv.main.list') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i>Formula Web
                        </a>
                    </li>
                </ul>
            @endrole
            @role('administrator|formula_pic')
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                    <a href="{{ route('adminexport') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i>Egg Export
                        </a>
                    </li>
                </ul>
            @endrole
            @endhasanyrole

            @role('wh_cashier|wh_supervisor')
                <li class="nav-item">
                    <a href="{{ route('warehouse.redemption.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>
                        Redemption
                    </a>
                </li>
            @endrole
            @role('administrator')
                <li class="nav-item">
                    <a href="{{ route('administrator.settings') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>
                        Global Settings
                    </a>
                </li>
            @endrole

            {{-- All dashboard --}}
            {{-- <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Dashboard
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/agent" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Agent Categories Sales
                        </a>
                    </li>
                </ul>
            </li> --}}

            {{-- currently not using
            <li class="nav-item nav-dropdown">

                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Report
                </a>

                <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/administrator/report" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Panel Report Summary
                            </a>
                        </li>
                </ul>

            </li> --}}



            <li class="divider"></li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
