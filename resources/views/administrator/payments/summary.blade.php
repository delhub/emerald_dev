@extends('layouts.administrator.main')

@section('pageTitle', 'Summary')

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light"> Payments / </span> Summary
    </h4>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <form action="" method="GET">
                        <div class="d-flex justify-content-between mb-3">
                            <h2 class="d-inline-block">Summary ({{ $summary_date }})</h2>

                            <div class="d-flex align-items-center">
                                <span>Date From</span>
                                <input class="form-control col text-center mx-3" name="date_before" type="date" value="{{ request('date_before') }}" required>
                                <span>Date To</span>
                                <input class="form-control col text-center mx-3" name="date_after" type="date" value="{{ request('date_after') }}" required>
                                <a href="{{ route('administrator.payment.summary') }}">
                                    <button class="btn btn-warning" type="button">
                                        Reset
                                    </button>
                                </a>
                                <button class="btn btn-primary" type="submit">
                                    Filter
                                </button>
                            </div>
                        </div>
                    </form>

                    <table class="table table-bordered table-striped">
                        <thead>
                            <th width="12%">Status</th>
                            @foreach ($purchase_types as $purchase_type)
                            <th class="text-center">{{$purchase_type}} (RM)</th>
                            @endforeach
                        </thead>
                        <tbody>
                            @foreach($purchases as $status_name => $amounts)
                            <tr>
                                <td>{{ showGloabalStatus($status_name) }}</td>
                                @foreach ($amounts as $amount)
                                <td class="text-center">{{ number_format($amount / 100, 2) }}</td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot style="border-top: 2px solid #00000020">
                            <tr>
                                <td>Purchase Paid Total (RM)</td>
                                @foreach ($purchase_totals['paid'] as $purchase_total)
                                <td class="text-center">{{ number_format($purchase_total / 100, 2) }}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Case Paid Total</td>
                                @foreach ($purchase_cases['paid'] as $purchase_case)
                                <td class="text-center">{{ $purchase_case }}</td>
                                @endforeach
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
        $(document).ready(function () {
        @if (request('date_before'))
            $('[name=date_after]').prop('min', '{{ request("date_before") }}')
        @endif
        @if (request('date_after'))
            $('[name=date_before]').prop('max', '{{ request("date_after") }}')
        @endif
    })
    $('[name=date_before]').on('change', function() {
        $('[name=date_after]').prop('min', $(this).val())
    })

    $('[name=date_after]').on('change', function() {
        $('[name=date_before]').prop('max', $(this).val())
    })
</script>
@endpush
