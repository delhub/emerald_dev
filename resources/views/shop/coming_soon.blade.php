@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Coming Soon</a></li>
            </ul>
        </div>
    </div>
    <!-- sample for test dynamic banner Mobile  end -->
    <div class="fml-container">
        <img src="{{asset('/images/coming_soon.png')}}" alt="" style="width: 100%;">
    </div>
</div>
@endsection

