<?php

namespace App\Models\Faq;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
     //use HasFactory;

     // Table Name
     protected $table = 'faq';
     // Primary Key
     public $primaryKey = 'id';
     // Timestamps
     public $timestemps = true;

     public function faq_type() {
          return $this->hasMany(Faq_type::class);
     }

     public $incrementing = false; 
}
