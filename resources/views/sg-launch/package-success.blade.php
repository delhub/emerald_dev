@extends('layouts.guest.v1.sg-launch-main')

@section('content')


<div class="bg-md bg-sm ">

    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 mb-0 pt-2 pb-3">
            <img class="mw-100" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
        </div>
    </div>
    <div>
        @include('shop.payment.success-child')
    </div>
</div>
</div>

@endsection

@push('style')
<style>
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .error {
        color: red;
    }
</style>
@endpush