@extends('layouts.shop.main')

@section('content')

    <div class="privacy-policy-bg-img">

        <div class="mt-5 d-flex"></div>
        <div class="d-md-none p-0">
            <img src="/images/membership-tnc/membership-tnc-1.jpg" style="margin-top: -22px;object-fit: cover;width: 100%;">
        </div>
        <div class="mt-md-5">

            <div class="d-flex justify-content-center">
                <div class="overflow-auto scrollbar privacy-policy" style="border-radius: 10px;">
                    <h1>Membership Terms and Conditions</h1>
                    <div class="info-hrline-red privacy"></div>
                    <div class="mt-md-2"></div>
                    <ul>
                        <li>
                            <p class="paragraph">
                                MEMBERSHIP
                            </p>
                        </li>
                        <p class="paragraph">
                            Membership Program is an exclusive members’ service provided by Formula Healthcare Sdn Bhd
                            and its affiliate companies (hereinafter referred to as “FHSB”). By applying for membership,
                            you hereby AGREE to be bound by the following terms and conditions: -
                        </p>
                        <li>
                            <p class="paragraph">
                                Customers are required to register as member before they can make purchase via
                                FHSB’s website. All members’ ID will be recorded in membership account.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                Application is open to customers who are aged 18 years and above.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                Membership are divided to Standard Member, Advance Member and Premier Member, each
                                category of membership will enjoy different types of benefits and offers from FHSB.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                Membership for both Advance and Premier categories will be renewed automatically after
                                spending minimum amount annually. Member who has failed to meet the minimum transaction
                                amount will be converted to become a Standard Member. There is no special transaction
                                requirement for Standard Member.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                A member may only hold one membership account at one time.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                Membership will be suspended after one year of zero transaction.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                POINTS
                            </p>
                            <ul>
                                <li>
                                    <p class="paragraph">
                                        ONE (1) membership point will be awarded for every RM1 spend in a single
                                        receipt. Every 2000 points can be exchangeable for RM20 cash voucher to purchase
                                        selected merchandise.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Any unused cash voucher cannot be exchanged for cash.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Earning of points will be based on the total amount purchase in a single
                                        receipt. Accumulation/Combination of receipts is prohibited.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        All points earned can be used to offset any purchases at FHSB’s official website and its physical store.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB has the right to cancel the points awarded if products are returned and
                                        refunded in full or partially. This will also apply to exchange of products,
                                        unless it is for exchange of products with equivalent value.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Points earned cannot be exchanged for cash and can only be used to offset
                                        purchases according to terms and conditions set by FHSB.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Points earned are strictly non-transferable and non-assignable.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Points earned can only be valid for 36 months from the date of points were
                                        earned.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB shall have the absolute discretion to refuse to process any member’s
                                        redemption request if fraudulent transactions/ activities are suspected/
                                        involved.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB reserves the right to determine number of points can be used to offset the
                                        purchase of any merchandise. Any member of FHSB who commits a misconduct,
                                        engages in fraud, abuses of FHSB benefits and rewards, and fails to adhere to
                                        these Terms and Conditions, will have his/her membership terminated. All of
                                        his/her benefits and privileges shall cease forthwith and their points will
                                        become void immediately.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Any transaction using the accumulated points will not be awarded with points.
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <p class="paragraph">
                                MEMBERSHIP TERMINATION
                            </p>
                            <ul>
                                <li>
                                    <p class="paragraph">
                                        Members who wish to discontinue their membership must submit written notice or
                                        notification via email to {{--fhsb.cs@gmail.com--}}. Membership account will be
                                        terminated
                                        within 30 days upon receipt of such notice and all benefits and privileges of
                                        the
                                        member shall cease and all outstanding points of such member will become void.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB reserves the right to terminate any membership at its sole and absolute
                                        discretion, at which point all unused points, benefits and privileges will be
                                        cancelled. Such termination shall be without prejudice to the accrued rights and
                                        remedies of FHSB, its participating partners and affiliates.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        If a Member has obtained redemption awards or other products or services through
                                        fraud, dishonesty or deceit then the member shall without limitation be liable
                                        to
                                        FHSB and its participating partners or affiliates for the full price of the
                                        redemption awards or other products or services obtained together with all costs
                                        and
                                        damages incurred or suffered by FHSB and its participating partners, affiliates
                                        as a
                                        result thereof.
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <p class="paragraph">
                                GENERAL
                            </p>
                            <ul>
                                <li>
                                    <p class="paragraph">
                                        The membership shall remain as property of FHSB at all times.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        By applying for and using FHSB membership, members must consent to the
                                        collection, storage, use and disclosure of the personal information in
                                        accordance with FHSB’s Privacy Policy and Privacy notice. Members can find the
                                        Privacy Policy online at <a href="/" class="paragraph pl-0">{{ url('/') }}</a>.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        In addition to the matters set out in the Privacy Policy, member’s personal
                                        information is collected so that the Company can continuously update its members
                                        of new range of products and services and other promotional materials or notices
                                        from time to time.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Members can at any one-time email to <a href="mailto:{{country()->company_email}}"
                                        class="paragraph pl-0">{{country()->company_email}}</a> of their wish to
                                        terminate the membership and thereafter they will not receive any promotional
                                        material or notices from FHSB or its participating partners and/or affiliates.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB shall hold no liability if a membership application is rejected for any
                                        reason whatsoever.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Any dispute related to points and services must be directed to
                                        <a href="mailto:{{country()->company_email}}" class="paragraph pl-0">
                                        {{country()->company_email}}</a> within seven (7) days of the said dispute arises.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        The Terms and conditions are governed by laws of @if(country()->country_id == 'MY')Malaysia. @else Singapore. @endif
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB has the final authority as to the interpretation of the Terms and
                                        Conditions and as to any other disputes regarding the membership.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        FHSB reserves the right to amend any of these Terms and Conditions from time to
                                        time as and when it deems necessary. Reasonable notice will be posted on FHSB’s
                                        official website as notification of the said amendment(s). Your continuous usage
                                        of the membership after any amendment(s) to the Terms and Conditions shall
                                        constitute your acceptance to such amendment(s).
                                    </p>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>




    @push('style')
        <style>
            p + ul {
                /*list-style-type: none;*/
                /*counter-reset: item;*/
                /*margin: 0;*/
                padding-left: 2rem;
            }
            ol > li {
                display: table;
                counter-increment: item;
                /*margin-bottom: 0.6em;*/
            }
            ol > li::before {
                /*content: counters(item, ".") ".";*/
                display: table-cell;
                padding-right: 0.5em;
            }
            /*li ol > li {*/
            /*    margin: 0;*/
            /*}*/
            /*li ol > li::before {*/
            /*    content: counters(item, ".");*/
            /*}*/
            .info-hrline-red.privacy {
                position: relative;
                width: 27px;
                margin: auto;
            }
            h1 {
                font-family: "Roboto", sans-serif;
                font-weight: 700;
                font-size: 28px;
                margin-bottom: 0px;
            }
            .privacy-policy-bg-img {
                margin-top: 82px;
                /*background-size: cover;*/
                object-fit: cover;
                background-size: 100% 100%;
                /* width: 100%; */
                /*height: 67.3vh;*/
            }

            .fml-footer {
                margin-top: 0;
            }

            .privacy-policy {
                background-color: white;
                border: 2px solid whitesmoke;
                padding: 0.75rem;
                max-height: 50vh;

            }

            /*Ultra large screen*/
            @media(min-width:2000px) {
                .privacy-policy {
                    max-width: 150vh;
                }
            }

            /*Desktop screen*/
            @media(min-width:1600px) and (max-width: 1999px){
                .privacy-policy {
                    max-width: 135vh;
                }
            }
            @media(min-width:768px) {
                .info-hrline-red {
                    right: 26.1%;
                }
                .privacy-policy-bg-img {
                    background: url('/images/membership-tnc/membership-tnc.jpg')no-repeat center;
                    background-size: 100% 100%;
                    /* width: 100%; */
                    height: 71.3vh;
                }
            }

            /*Large laptop screen*/
            @media(min-width:1200px) and (max-width:1600px) {
                .privacy-policy {
                    max-width: 100vh;
                }
            }

            /*Laptop screen*/
            @media(min-width:800px) and (max-width:1200px) {
                .privacy-policy {
                    max-width: 70vh;
                }
            }

            /*Tablet screen*/
            @media(max-width:799px) {
                .info-hrline-red {
                    left: 29.2%;
                }
                /*.privacy-policy-bg-img {
                    height: 73.9vh;
                }*/
                .privacy-policy {
                    max-height: 100%;
                    max-width: 50vh;
                    border: none;
                }
            }

            .header {
                font-weight: 700;
                text-decoration: underline;

            }

            .paragraph {
                font-family: "Roboto", sans-serif;
                font-size: 16px;
                text-align: justify;
                line-height: 1.3;
                font-weight: revert;
            }

            .sub-list {
                counter-reset: list;
            }

            .sub-list>li {
                list-style: none;
                position: relative;
            }

            .sub-list>li:before {
                counter-increment: list;
                content: "("counter(list, lower-alpha) ") ";
                position: absolute;
                left: -1.7em;
            }


            .scrollbar::-webkit-scrollbar {
                width: 15px;



            }

            .scrollbar::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
                border-radius: 10px;

            }

            .scrollbar::-webkit-scrollbar-thumb {
                border-radius: 10px;

                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
                background: #ef3842c9;

            }
        </style>
    @endpush

@endsection
