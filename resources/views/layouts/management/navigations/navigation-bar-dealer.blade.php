@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp

<header class="app-header navbar navbar-bg-color navbar-border-bottom-color nav-height">
    {{--Navbar icon mobile--}}
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <i class="fa fa-bars navigation-icon"></i>
    </button>

    <!-- Navbar icon desktop -->
    {{--<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show"><i class="fa fa-bars navigation-icon"></i></button>--}}

    <a class="navbar-brand" href="/shop">
        <img class="navbar-brand-full margin-logo-mobile img-logo-resize" src="{{asset("storage/logo/bujishu-logo-$country.png")}}" width="40" height="40" alt="Bujishu Logo" />
        <img class="navbar-brand-minimized" src="{{asset("storage/logo/bujishu-logo-$country.png")}}" width="30" height="30" alt="Bujishu Logo" />
    </a>
    <h4 class="toptitle hidden-sm" style="color: #e0b555;">Welcome to Agent Dashboard</h4>
    <ul class="nav navbar-nav ml-auto">
        <!--<h5 style="margin-right:10px; color:#ffcc00;" class="welcome-text">{{Auth::user()->userInfo->full_name}}</h5>-->
        <!-- Points -->
        <li class="nav-item category-icon">
            <a class="test" href="/shop/category/bedsheets-and-mattresses/bedsheets" data-toggle="tooltip" data-placement="bottom" title="Bed Sheet">
                <img class="" src="{{ asset('assets/images/icons/categories/Heading Icon_Bedsheet.png') }}" alt="" />
            </a>
            <a href=" /shop/category/curtains" data-toggle="tooltip" data-placement="bottom" title="Curtain">
                <img class="" src="{{ asset('assets/images/icons/categories/Heading Icon_Curtain.png') }}" alt="" />
            </a>
            <a href="/shop/category/lightings" data-toggle="tooltip" data-placement="bottom" title="Lighting">
                <img class="" src="{{ asset('assets/images/icons/categories/Heading Icon_Lighting.png') }}" alt="" />
            </a>
            <a href="/shop/category/cabinets" data-toggle="tooltip" data-placement="bottom" title="Cabinet">
                <img class="" src="{{ asset('assets/images/icons/categories/Heading Icon_Cabinet.png') }}" alt="" />
            </a>
            <a href="/shop/category/carpets" data-toggle="tooltip" data-placement="bottom" title="Carpet">
                <img class="" src="{{ asset('assets/images/icons/categories/Heading Icon_Carpet.png') }}" alt="" />
            </a>
            <a href="/shop/category/products-and-services/paints" data-toggle="tooltip" data-placement="bottom" title="Paint">
                <img class="" src="{{ asset('assets/images/icons/categories/Heading Icon_Paint.png') }}" alt="" />
            </a>
        </li>
        <!-- Points -->
        <li class="nav-item">
            <span class="points">{{ Auth::user()->userInfo->blue_points }}</span>
            <img class="point-icon" src="{{ asset('assets/images/icons/blue-point-01.png') }}" alt="" />
        </li>
        <!-- Wish List -->
        <li class="nav-item">
            <a href="{{route('shop.wishlist.home')}}">
                <i class="far fa-heart"></i>
                <span class="amount" id="wishlist-amount"></span>
            </a>
        </li>
        <!-- Shopping Cart amount -->
        <li class="nav-item">
            <a href="/shop/cart">
                <i class="fas fa-shopping-cart"></i>
                <span class="amount" id="cart-quantity"></span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="image-avatar" src="{{asset('storage/avatar/default-avatar.jpg')}}" alt="{{ Auth::user()->userInfo->full_name }}" />
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                {{--
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>
                <a class="dropdown-item" href="#"> <i class="fa fa-user"></i> Profile </a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-usd"></i> Payments
                    <span class="badge badge-secondary">42</span>
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-file"></i> Projects
                    <span class="badge badge-primary">42</span>
                </a>
                <div class="divider"></div>
                <a class="dropdown-item" href="#"> <i class="fa fa-shield"></i> Lock Account</a> --}} {{-- <a class="dropdown-item" href="#"> <i class="fa fa-lock"></i> Logout</a> --}}
                <a class="dropdown-item profile-menu" href="{{ route('shop.dashboard.dealer.profile') }}" style="padding: 10px 0px 10px 20px;border-radius: 30px 30px 0 0;">
                    <img src="{{ asset('assets/images/icons/profile_b.png') }}" alt="Profile" style="height:23px; padding:0 10px 0 0px;"> Profile
                </a>

                <a class="dropdown-item myDashboard-menu" href="{{ route('shop.dashboard.customer.home') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="My Dashboard" style="height:23px; padding:0 10px 0 0px !important;">
                    My Dashboard
                </a>

                @hasrole('dealer')
                <a class="dropdown-item agentDashboard-menu" href="{{ route('agent.overview') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Agent Dashboard" style="height:23px; padding:0 10px 0 0px !important;"> Agent Dashboard
                </a>
                @endhasrole

                @hasrole('panel')
                <a class="dropdown-item panelDashboard-menu" href="{{ route('management.panel.home') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Panel Dashboard" style="height:23px; padding:0 10px 0 0px !important;"> Panel Dashboard
                </a>
                @endhasrole
                @hasrole('redemption')
                <a href="{{ route('administrator.redemption') }}" class="dropdown-item redemptionDashboard-menu"><img
                        src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Administrator"
                        style="height:23px; padding:0 10px 0 0px !important;"> Redemption</a>
                <!-- <a href="/administrator/sales" class="dropdown-item saleSummary-menu"><img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Sale Summary" style="height:23px; padding:0 10px 0 0px !important;"> Sale Summary</a> -->
                @endhasrole

                <a class="dropdown-item logout-menu" href="{{ route('logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();" style="border-radius: 0 0 30px 30px;">
                    <img src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout" style="height:23px; padding:0 10px 0 0px !important;"> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
    {{--
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    --}}
</header>

<style>
    .profile-menu:active img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    .profile-menu:hover img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    /* .myDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .myDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .agentDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .agentDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .panelDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .panelDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .logout-menu:active, img{
    content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }*/

    .logout-menu:hover img {
        content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }
</style>