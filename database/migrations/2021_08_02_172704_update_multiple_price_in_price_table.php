<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Products\ProductPrices;

class UpdateMultiplePriceInPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('carts', function (Blueprint $table) {
            $table->integer('price_key')->after('product_code')->default(1);
        });

        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->integer('price_key')->after('product_code')->default(1);
            $table->text('notes')->after('product_sv')->nullable();
            $table->bigInteger('premier_price')->after('offer_price')->default(0);
        });

        Schema::table('items', function (Blueprint $table) {
            $table->integer('price_key')->after('product_code')->default(1);
        });

        $productPrices = ProductPrices::get();
        foreach ($productPrices as $key => $price) {
            $price->active = 1;
            $price->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('price_key');
        });

        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->dropColumn('price_key');
            $table->dropColumn('notes');
            $table->dropColumn('premier_price');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('price_key');
        });
    }
}
