<?php

namespace App\Http\Controllers\Administrator\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\Image;
use App\Models\Users\User;
use App\Models\Users\UserInfo;
use App\Models\Globals\Status;
use App\Models\Users\UserMembershipLogs;
use App\Models\Users\UserPoints;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Hash;
use App\Models\Globals\State;
use App\Models\Globals\City;
use App\Models\Globals\Race;
use App\Models\Globals\Gender;
use App\Models\Globals\Marital;
use App\Models\Globals\BankName;
use App\Models\Globals\Employment;
use App\Models\Dealers\DealerGroup;
use App\Models\Registrations\Dealer\Packages;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request);
        $users = User::with('userInfo', 'dealerInfo', 'panelInfo', 'roles')
            ->searchFilter(
                $request->selectSearch === 'account_type' ? $request->rolesBox : $request->searchBox, 
                $request->selectSearch
            );

        $forCountUsers = $users->get();
        $roles = Role::get();

        $users = $users->paginate(10);
        $users->appends(request()->query());;

        return view('administrator.users.index', compact('users', 'request', 'forCountUsers', 'roles'));
    }

    public function updateUser(Request $request)
    {
        if ($request->process == 'allow_offline') {

            $acc_code = $request->input('switchData') == 'true' ? 'bypass' : null;

            UserInfo::where('user_id', $request->id)->update([
                'acc_code' => $acc_code
            ]);

        }

        $arr = [];

        return response()->json($arr, 200);
    }

    public function membership(Request $request)
    {

        // $memberships = UserMembershipLogs::searchFilter($request->searchBox, $request->selectSearch)->get();
        $users = User::has('userInfo')->rankFilter($request->r,$request->searchBox)->searchFilter($request->searchBox, $request->selectSearch)->paginate(20);
        $userLevels = Status::where('status_type','user_level')->get();

        // if (count($memberships) > 0) {
        //     foreach ($memberships as $key => $value) {
        //         $value->description = json_decode($value->description);
        //         $data[] = $value;
        //     }
        // } else {
        //     $memberships = [];
        // }
        $users->appends(request()->query());

        return view('administrator.users.membership', compact('users','userLevels'));
    }
    public function upgradeMembership(Request $request){
        //
        $user = User::where('id',$request->user_id)->first();

        // upload image
        $file = $request->file('supporting_doc');
        $fileName = str_replace(' ','-',strtolower($file->getClientOriginalName()));
        $folderD = 'uploads/images/users/'.$user->userInfo->account_id.'/upgrade/';
        $destinationPath = public_path('/storage/'.$folderD);
        if (!File::isDirectory($destinationPath)) {
            File::makeDirectory($destinationPath, 0777, true);
        }
        $file->move($destinationPath, $fileName);

        // update membership logs
        $membershipLog = UserMembershipLogs::firstOrNew(['user_id' =>  $request->user_id],
        ['date_start' => Carbon::now(), 'date_end' => Carbon::now(), 'last_checked' => Carbon::now(), 'total_expenses' => 0]);
        $membershipLog->reason = $request->reason;
        $membershipLog->admin_id = auth()->user()->id;
        $membershipLog->checking_status = 'upgraded';
        $membershipLog->save();

        $image = Image::firstOrNew(['imageable_type' =>  UserMembershipLogs::class,'imageable_id' =>  $membershipLog->id]);
        $image->path = $folderD;
        $image->filename = $fileName;
        $image->default = 0;
        $image->brand = 0;
        $image->save();

        $membershipLog->image_id = $image->id;
        $membershipLog->save();
      
        $user->userInfo->user_level = 6001;
        $user->userInfo->save();

        return redirect()->back();
    }

    public function updateMembership(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'user_level' => 'required',
            'amount' => 'required',
            'discount' => 'required',
            'range_yearly' => 'required',
            'maintenance_fee' => 'required'
        ]);

        $membership = Status::where('id', $request->user_level)->first();
        $description = [
            'amount' => $request->amount,
            'discount' => $request->discount,
            'range_yearly' => $request->range_yearly,
            'maintenance_fee' => $request->maintenance_fee
        ];

        $membership->name = $request->name;
        $membership->description = json_encode($description);
        $membership->save();

        return redirect()->route('administrator.user.membership')
            ->with('success', $request->name . ' Membership data updated..');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function points()
    {
        $userPoints = UserPoints::where('point_type', 'credit')->where('status', 'pending')->where('created_at', '<=', Carbon::now()->subDays(7)->toDateTimeString())->get();

        if ($userPoints) {
            foreach ($userPoints as $userPoint) {
                $userPoint->status = 'approved';
                $userPoint->save();

                $user = User::where('id', $userPoint->user_id)->first();
                $user->userInfo->user_points = $userPoint->balance;
                $user->userInfo->save();
                dd($userPoints, $user->userInfo);
            }
        }
        $currentPoint = Status::where('status_type', 'minimum_point')->first();
        return view('administrator.users.points', compact(['currentPoint']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePoints(Request $request)
    {
        $currentPoint = Status::where('status_type', 'minimum_point')->first();
        $currentPoint->description = $request->minimumPoint;
        $currentPoint->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $states = State::get();
        $cities = City::get();
        $genders = Gender::get();
        $maritals = Marital::get();
        $races = Race::get();
        $dealerGroups  = DealerGroup::get();
        $packages = Packages::get();
        $banks = BankName::where('type', 'agent')->get();
        $employments = Employment::get();

        return view('administrator.users.show', compact('user', 'states', 'cities', 'maritals', 'races', 'genders', 'dealerGroups', 'packages', 'banks', 'employments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::orderBy('id')->pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();


        return view('administrator.users.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $form_type = null)
    {
        $user = User::find($id);
        $addressRules = [
            'address_1' => 'required|string|max:255',
            'address_2' => 'nullable|string|max:255',
            'address_3' => 'nullable|string|max:255',
            'postalCode' => 'required|regex:/^\d{4,10}$/',
            'state' => 'required',
            'city' => 'nullable',
        ];

        switch ($form_type) {
            case 'customer_info':
                $rules = [
                    'full_name' => 'required|string|max:255',
                    'nric' => 'required|digits_between:6,12',
                    'email' => 'required|email|unique:users,email,' . $id,
                    'contact_num' => 'required|regex:/^\d{8,15}$/',
                ];
    
                foreach (['shipping_address', 'mailingAddress'] as $prefix) {
                    foreach ($addressRules as $field => $rule) {
                        $rules["{$prefix}.{$field}"] = $rule;
                    }
                }
    
                // Validate the request
                $this->validate($request, $rules);

                $user->email = $request->email;
                $user->save();

                $user->userInfo->full_name = $request->full_name;
                $user->userInfo->nric = $request->nric;
                $user->userInfo->save();

                $user->userInfo->mobileContact->contact_num = $request->contact_num;
                $user->userInfo->mobileContact->save();
                
                if (isset($user->userInfo->shippingAddress)) {
                    
                    $user->userInfo->shippingAddress->address_1 = $request->shipping_address['address_1'];
                    $user->userInfo->shippingAddress->address_2 = $request->shipping_address['address_2'];
                    $user->userInfo->shippingAddress->address_3 = $request->shipping_address['address_3'];
                    $user->userInfo->shippingAddress->postcode = $request->shipping_address['postalCode'];
                    $user->userInfo->shippingAddress->state_id = $request->shipping_address['state'] ?? 0;
                    $user->userInfo->shippingAddress->city_key = $request->shipping_address['city'] ?? 0;
                    if(isset($request->shipping_address['city'])) {
                        $city = City::where('city_key', $request->shipping_address['city'])->first();
                        $user->userInfo->shippingAddress->city = $city->city_name;
                    } else {
                        $user->userInfo->shippingAddress->city = null;
                    }
                }
                $user->userInfo->shippingAddress->save();
                
                if (isset($user->userInfo->mailingAddress)) {
                    $user->userInfo->mailingAddress->address_1 = $request->mailingAddress['address_1'];
                    $user->userInfo->mailingAddress->address_2 = $request->mailingAddress['address_2'];
                    $user->userInfo->mailingAddress->address_3 = $request->mailingAddress['address_3'];
                    $user->userInfo->mailingAddress->postcode = $request->mailingAddress['postalCode'];
                    $user->userInfo->mailingAddress->state_id = $request->mailingAddress['state'] ?? 0;
                    $user->userInfo->mailingAddress->city_key = $request->mailingAddress['city'] ?? 0;
                    if(isset($request->mailingAddress['city'])) {
                        $city = City::where('city_key', $request->mailingAddress['city'])->first();
                        $user->userInfo->mailingAddress->city = $city->city_name;
                    } else {
                        $user->userInfo->mailingAddress->city = null;
                    }
                }
            
                $user->userInfo->mailingAddress->save();
                

                break;
            case 'dealer_info':
                $rules = [
                    'full_name' => 'required|string|max:255',
                    'nric' => 'required|digits_between:6,12',
                    'contact_num' => 'required|regex:/^\d{8,15}$/',
                    'date_of_birth' => 'required|date',
                    'gender' => 'required|numeric|min:1',
                    'race' => 'required|numeric|min:1',
                    'maritalStatus' => 'required|numeric|min:1',

                    'employment' =>'required|string',
                    'company_name' => 'required|string',
                ];
    
                foreach (['shippingAddress', 'billingAddress', 'employmentAddress'] as $prefix) {
                    foreach ($addressRules as $field => $rule) {
                        $rules["{$prefix}.{$field}"] = $rule;
                    }
                }

                $this->validate($request, $rules);



                if(isset($user->dealerInfo)){
                    $user->dealerInfo->full_name = $request->full_name;
                    $user->dealerInfo->nric = $request->nric;
                    $user->dealerInfo->payment_proof = $request->payment_proof;
                    $user->dealerInfo->referrer_name = $request->referrer_name;
                    $user->dealerInfo->referrer_id = $request->referrer_id;
                    $user->dealerInfo->package_type = $request->package_name;
                    $user->dealerInfo->date_of_birth = $request->date_of_birth;
                    $user->dealerInfo->gender_id = $request->gender;
                    $user->dealerInfo->race_id = $request->race;
                    $user->dealerInfo->marital_id = $request->maritalStatus;
                    $user->dealerInfo->group_id = $request->agentGroup;
                    $user->dealerInfo->save();
                }
                
                if(isset($user->dealerInfo->dealerMobileContact)) {
                    $user->dealerInfo->dealerMobileContact->contact_num = $request->contact_num;
                    $user->dealerInfo->dealerMobileContact->save();
                }

                if (isset($user->dealerInfo->shippingAddress)) {
                    
                    $user->dealerInfo->shippingAddress->address_1 = $request->shippingAddress['address_1'];
                    $user->dealerInfo->shippingAddress->address_2 = $request->shippingAddress['address_2'];
                    $user->dealerInfo->shippingAddress->address_3 = $request->shippingAddress['address_3'];
                    $user->dealerInfo->shippingAddress->postcode = $request->shippingAddress['postalCode'];
                    $user->dealerInfo->shippingAddress->state_id = $request->shippingAddress['state'] ?? 0;
                    $user->dealerInfo->shippingAddress->city_key = $request->shippingAddress['city'] ?? 0;
                    if(isset($request->shippingAddress['city'])) {
                        $city = City::where('city_key', $request->shippingAddress['city'])->first();
                        $user->dealerInfo->shippingAddress->city = $city->city_name;
                    } else{
                        $user->dealerInfo->shippingAddress->city = null;
                    }
                    $user->dealerInfo->shippingAddress->save();
                }
                
                if (isset($user->dealerInfo->billingAddress)) {
                    $user->dealerInfo->billingAddress->address_1 = $request->billingAddress['address_1'];
                    $user->dealerInfo->billingAddress->address_2 = $request->billingAddress['address_2'];
                    $user->dealerInfo->billingAddress->address_3 = $request->billingAddress['address_3'];
                    $user->dealerInfo->billingAddress->postcode = $request->billingAddress['postalCode'];
                    $user->dealerInfo->billingAddress->state_id = $request->billingAddress['state'] ?? 0;
                    $user->dealerInfo->billingAddress->city_key = $request->billingAddress['city'] ?? 0;
                    if(isset($request->billingAddress['city'])) {
                        $city = City::where('city_key', $request->billingAddress['city'])->first();
                        $user->dealerInfo->billingAddress->city = $city->city_name;
                    } else{
                        $user->dealerInfo->billingAddress->city = null;
                    }
                    $user->dealerInfo->billingAddress->save();
                }

                if (isset($user->dealerInfo->employmentAddress)) {
                    $user->dealerInfo->employmentAddress->employment_type = $request->employment;
                    $user->dealerInfo->employmentAddress->company_name = $request->company_name;
                    $user->dealerInfo->employmentAddress->company_address_1 = $request->employmentAddress['address_1'];
                    $user->dealerInfo->employmentAddress->company_address_2 = $request->employmentAddress['address_2'];
                    $user->dealerInfo->employmentAddress->company_address_3 = $request->employmentAddress['address_3'];
                    $user->dealerInfo->employmentAddress->company_postcode = $request->employmentAddress['postalCode'];
                    $user->dealerInfo->employmentAddress->company_state_id = $request->employmentAddress['state'] ?? 0;
                    $user->dealerInfo->employmentAddress->company_city_key = $request->employmentAddress['city'] ?? 0;
                    if(isset($request->employmentAddress['city'])) {
                        $city = City::where('city_key', $request->employmentAddress['city'])->first();
                        $user->dealerInfo->employmentAddress->company_city = $city->city_name;
                    } else{
                        $user->dealerInfo->employmentAddress->company_city = null;
                    }
                    $user->dealerInfo->employmentAddress->save();
                }

                if (isset($user->dealerInfo->dealerSpouse)) {
                    $user->dealerInfo->dealerSpouse->spouse_name = $request->spouse_name;
                    $user->dealerInfo->dealerSpouse->spouse_date_of_birth = $request->spouse_date_of_birth;
                    $user->dealerInfo->dealerSpouse->spouse_contact_office = $request->spouse_contact_office;
                    $user->dealerInfo->dealerSpouse->spouse_email = $request->spouse_email;
                    $user->dealerInfo->dealerSpouse->spouse_nric = $request->spouse_nric;
                    $user->dealerInfo->dealerSpouse->spouse_occupation = $request->spouse_occupation;
                    $user->dealerInfo->dealerSpouse->spouse_contact_mobile = $request->spouse_contact_mobile;
                    $user->dealerInfo->dealerSpouse->save();
                }

                break;
            case 'bank_info':
                $this->validate($request, [
                    'bank_code' => 'required',
                    'account_regid' => 'required',
                    'bank_acc' => 'required',
                    'bank_acc_name' => 'required'
                ]);
                $user->dealerInfo->dealerBankDetails->bank_code = $request->bank_code;
                $user->dealerInfo->dealerBankDetails->account_regid = $request->account_regid;
                $user->dealerInfo->dealerBankDetails->bank_acc = $request->bank_acc;
                $user->dealerInfo->dealerBankDetails->bank_acc_name = $request->bank_acc_name;
                $user->dealerInfo->dealerBankDetails->save();

                break;
            default:
                $this->validate($request, [
                    'password' => 'same:confirm-password',
                    'roles' => 'required'
                ]);
        
                $input = $request->all();
                if (!empty($input['password'])) {
                    $input['password'] = Hash::make($input['password']);
                } else {
                    $input = Arr::except($input, array('password'));
                }
        
                $user = User::find($id);
                $user->update($input);
                DB::table('model_has_roles')->where('model_id', $id)->delete();
        
                $user->assignRole($request->input('roles'));
                $acc_code = ($request->allow_bypass == 'on') ? 'bypass' : null;
    
                UserInfo::where('user_id', $id)->update([
                    'acc_code' => $acc_code
                ]);

                break;
        }


        return redirect()->route('administrator.user')
        ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
