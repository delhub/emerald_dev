<?php

namespace App\Models\Users\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\WEB\Shop\v1\CartController;
use App\Traits\AttributeImage;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\ShippingInstallations\Rates;

class Cart extends Model
{
    use AttributeImage;
    // Set table
    protected $table = 'carts';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    // Cast column to another datatype.
    protected $casts = [
        'product_information' => 'array'
    ];

    /**
     * Get product in the cart.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Products\Product', 'product_id');
    }

    // Show error message in cart page
    public function getDisabledMessageAttribute()
    {
        $min = 0;

        if ($this->disabled == 1 || $this->disabled == 7) {

            $zone = CartController::maybeCreateCookieAddress();
            $zoneId = CartController::getZone($zone['state_id'], $zone['city_key']);

            if ($this->disabled == 1) {
                $categoryId = $this->product->shipping_category;
            } elseif ($this->disabled == 7) {
                if ((array_key_exists('product_installation', $this->product_information) && $this->product_information['product_installation']) != 0) {
                    $categoryId = $this->product_information['product_installation'];
                }
            }

            if (!empty($zoneId)) {
                foreach ($zoneId as $key => $zone_id) {
                    $zoneid[] = $zone_id->id;
                }

                $minRates = Rates::where('active', 'Y')
                    ->select('condition', 'min_rates')
                    ->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')
                    ->whereIn('zone_id', $zoneid)
                    ->where('category_id', $categoryId)
                    ->min('min_rates')
                    ->first();


                $conditionRates = Rates::where('active', 'Y')
                    ->select('condition', 'min_rates')
                    ->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')
                    ->whereIn('zone_id', $zoneid)
                    ->where('category_id', $categoryId)
                    ->pluck('condition')
                    ->first();
            }

            if (isset($minRates) && isset($conditionRates)) {
                if ($conditionRates == 'quantity') {
                    $min = '' . $minRates  . ' Qty';
                } else {
                    $min = country()->country_currency . number_format(($minRates / 100), 2) . '';
                }
            }


            //     if ($min == 0) {
            //         $min = country()->country_currency . number_format(($this->product->panel->categoriesWithMinPrice->first()['max_price'] / 100), 2);
            //     }
        }

        $message = array(
            0 => null,
            // 1 => 'Minimum purchase is ' . $min . '.', //shipping
            1 => 'Product not reach minimum requirements', //shipping
            2 => 'Product delivery to the address provided is not available.',
            3 => 'Before delivery, company will email to ask for top up delivery fee if needed.',
            4 => 'Product no longer available.',
            5 => 'Product no longer available. Please delete and buy again.',
            6 => 'Product installation to the address provided is not available.',
            7 => 'This product does not meet the minimum quantity/price for installation',
            8 => '',
            9 => 'This product is not eligible for discount',
            10 => 'Recurring Booking Systems (RBS) can only be purchased individually'
        );

        return $message[$this->disabled];
    }

    /**
     * Get shipping / installation category
     *      */
    public function installationCategories()
    {
        if (array_key_exists('product_installation', $this->product_information)) {
            return Ship_category::where('id', $this->product_information['product_installation'])->first();
        }
    }

    /**
     * Get item's unit price formatted with 2 decimal points.
     */
    public function getDecimalUnitPrice()
    {
        return number_format(($this->unit_price / 100), 2);
    }

    /**
     * Get item's total price formatted with 2 decimal points.
     */
    public function getDecimalTotalPrice()
    {
        return number_format(($this->total_price / 100), 2);
    }

    /**
     * Get item's shipping fee formatted with 2 decimal points.
     */
    public function getDecimalShippingFee()
    {
        return number_format(($this->shipping_fee / 100), 2);
    }
}
