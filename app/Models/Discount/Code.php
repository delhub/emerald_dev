<?php

namespace App\Models\Discount;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    // Table Name
    protected $table = 'discount_code';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestemps = true;

    public static function generateCode($length = 8)
    {
        if (function_exists('random_bytes')) {
            $bytes = random_bytes($length / 2);
        } else {
            $bytes = openssl_random_pseudo_bytes($length / 2);
        }

        $code = bin2hex($bytes);

        $code = str_replace(str_split('oO01l'), '', $code);
        while (strlen($code) < $length) $code .= self::generateCode(22);

        $code = strtoupper($code);

        return substr($code, 0, $length);
    }

    public function rule()
    {
        return $this->belongsTo('App\Models\Discount\Rule', 'coupon_rule_id', 'id');
    }
}
