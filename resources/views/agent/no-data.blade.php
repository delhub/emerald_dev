
@extends('layouts.management.main-agent')
@section('content')

<div class="container-fluid">
    <div class="row myagentpage-row">
        <div class="ad-my-box col-md-12">
            <div class="d-style-box card">
                <div class="card-body">
                    <div class="card-text">
                        <div class="yearly-title-group">
                            <div class="yearly-title-1">
                                <h4>No Data Available</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('style')
<style>
    .d-style-box.card {
        background: #FFFFFF;
        border: 1px solid #D6D6D6;
        box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.15);
        border-radius: 15px;
    }
    .card-body {
        padding: 40px 20px;
    }
    .yearly-title-group {
        display: flex;
        padding: 0px 10px;
        width: 100%;
        flex-direction: row;
    }
    .yearly-title-group h4 {
        margin-bottom: 0px;
        font-style: normal;
        font-weight: 500;
        font-size: 20px;
        line-height: 20px;
        color: #F44A3B;
        width: auto;
        text-align: left;
    }
    .yearly-title-1 {
        display: flex;
        flex-direction: row;
        align-items: center;
    }
    @media only screen and (max-width: 767px) {
        .yearly-title-group {
            flex-direction: column;
        }
        .yearly-title-group h4 {
            font-size: 16px;
            text-align: center;
        }
    }
</style>
    
@endpush
