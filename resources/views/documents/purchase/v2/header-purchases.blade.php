<!-- Header -->
<style>
    .topbar{
        position: absolute; 
        top: 0; 
        width: 100%; 
        border-top: 18px solid #ffcc00;
        left: 0;
    }
    .logo-barcode{
        display: inline-flex;
        width: 100%;
    }
    .logo-barcode .info{
        width: 50%;
        padding: 0px 15px;
    }
    .logo-barcode .barcode{
        width: 30%;
    }
    .logo-img,.logo-barcode .logo{
        width: 19.5%;
        border-right: 2px solid #ffcc00;
        padding: 0 15px;
    }
    .logo-img img,.logo-barcode .logo img{
        width: 190px; 
        height: auto;
        opacity:1;
    }
    .myfloat-left{
        float: left;
    }
    .logo-barcode .info .brand{
        font-size: 1rem; 
        font-weight: 600; 
        margin-bottom: 3px;
    }
    .logo-barcode .info .numB{
        font-size: .75rem; 
        margin-bottom: 0px;
    }
    .logo-barcode .address{
        font-size: .85rem; 
        line-height: 1.5; 
        margin-bottom: 0px;
        letter-spacing: .05em;
    }
</style>
<div class="topbar"></div>

<!-- Letterhead -->
<div style="padding: 0px 20px;margin-top: 20px;margin-bottom: 18px;height: 104px;">
    {{-- Old table code --}}
    <table>
        <tr>
            <td class="logo-img">
                <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-logo.jpg') }}">
            </td>

            <td style="vertical-align: top; padding: 0 15px;">

                <p style="font-size: 1.25rem; font-weight: 600; margin-bottom: 3px;">
                    {{$country->company_name}}
                </p>

                <p class="text-grey" style="font-size: .75rem; margin-bottom: 0px;">
                    @if(isset($purchase) && $purchase->country_id != 'SG')
                    Company No: {{$country->company_reg}}
                    @else
                    GST Reg. No.: {{$country->company_reg}}
                    @endif
                </p>

                <p class="text-grey" style="font-size: .85rem; line-height: 1.5; margin-bottom: 0px;">
                    {!!$country->company_address!!}
                </p>

                <div style="width: 100%;">
                    <!-- <span class="text-grey" style="width: 33%; text-align: center; font-size: .8rem; margin-right: 18px;">
                        <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-tel.jpg') }}"
                    style="height: 11px; margin-right: 2px;"> 603-2181 8821
                    </span> -->

                    <span class="text-grey"
                        style="width: 33%; text-align: center; font-size: .8rem; margin-right: 18px;">
                        <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-mail.jpg') }}"
                            style="height: 9px; margin-right: 2px;">
                            {{$country->company_email}}
                    </span>

                    <span class="text-grey"
                        style="width: 33%; text-align: center; font-size: .8rem; margin-right: 18px;">
                        <img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-web.jpg') }}"
                            style="height: 11px; margin-right: 2px;">
                            {{$country->url}}
                    </span>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- End Letterhead -->