<?php

namespace App\Models\Warehouse\Bunbundle;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;


class WarehouseBunbundleItem extends Model
{
    use TableLogs;

    protected $table = 'wh_product_b_unbundle_item';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    /**
     * Get purchase of the WarehouseOrder.
     */
    public function batchItem()
    {
        return $this->belongsTo('App\Models\Products\WarehouseBatchItem', 'batch_item_id', 'id');
    }

    public function theUser()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'user');
    }
}
