<?php

namespace App\Traits;

use App\Models\Globals\Countries;
use Illuminate\Support\Facades\Cookie;
use stdClass;

trait PriceCalculation
{
    public $priceTypes = [
        'fixed_price', 'outlet_price', 'standard_price', 'offer_price', 'advance_price', 'premier_price', 'web_offer_price', 'outlet_offer_price', 'point', 'point_cash', 'discount_percentage', 'product_sv','member_price',
        'id', 'price_key', 'notes', 'active'
    ];

    // show active price
    public function getPriceAttributes($price_type)
    {
        $country_id = country()->country_id;

        $prices = $this->getPricingModel($country_id);

        return isset($prices) ? $prices->$price_type : 0;
    }

    public function getPricingModel($country_id)
    {

        $MyPrice = $this->morphOne('App\Models\Products\ProductPrices', 'model')
            ->select($this->priceTypes)
            ->where('country_id', 'MY')
            ->where('active', 1)
            ->first();

        if ($country_id == "MY") return $MyPrice;

        // $CountryPrice = $this->morphOne('App\Models\Products\ProductPrices', 'model')
        //     ->select($this->priceTypes)
        //     ->where('country_id', $country_id)
        //     ->first();
        // $defaultPrice = new stdClass;

        // foreach ($this->priceTypes as $priceType) {

        //     $thisPrice = $CountryPrice[$priceType];
        //     $malaysiaPrice = $MyPrice[$priceType];
        //     if ($thisPrice == 0) {
        //         $thisPrice = $this->defaultPrice($malaysiaPrice, $country_id);
        //         $CountryPrice->$priceType = $thisPrice['new_price'];
        //         $defaultPrice->$priceType = $thisPrice['default_price'];
        //     } else {
        //         $CountryPrice->$priceType =   $defaultPrice->$priceType = $thisPrice;
        //     }
        // }

        // if ($CountryPrice->member_price >= $CountryPrice->price) {
        //     $CountryPrice->member_price = $defaultPrice->member_price;
        //     $CountryPrice->price = $defaultPrice->price;
        // }

        // if ($CountryPrice->fixed_price >= $CountryPrice->member_price) {
        //     $CountryPrice->fixed_price = $defaultPrice->fixed_price;
        // }

        // return $CountryPrice;
    }


    public function defaultPrice($malaysiaPrice, $country_id)
    {
        if ($malaysiaPrice == null) return ['new_price' => 0, 'default_price' => 0];

        $countryTable =  Countries::where('country_id', $country_id)->first();

        $panelProduct = (self::class == 'App\Models\Products\Product') ? $this : $this->product2;

        $markupCategory = $panelProduct->markupCategories->first();

        $markup_rate = ($markupCategory != NULL) ? $markupCategory->markup_rate : $countryTable->mark_up;

        $defaultPrice = ceil($malaysiaPrice * ($markup_rate / 100));

        $customPrice = $this->calculatePriceCustom($malaysiaPrice, $defaultPrice);

        return ['new_price' => (int) $customPrice['firm'], 'default_price' => (int) $customPrice['loose']];
    }

    public function calculatePriceCustom($price_my, $price_sg)
    {

        //$length_my = strlen(round($price_my));
        $length_sg = strlen(round($price_sg));

        switch ($length_sg) {
            case 7:
                $last5 = substr($price_my, -5);
                $first = substr($price_sg, 0, 2);
                $firm = $loose =  $first . $last5;

                $last5 = substr($price_my, -5);
                $first = substr($price_sg, 0, 2);

                if ($last5 == 98000 || $last5 == 99000 || $last5 == '09000') {
                    $firm =  $first . $last5;
                }
                break;
            case 6:
                $last4 = substr($price_my, -4);
                $first = substr($price_sg, 0, 2);
                $firm = $loose = $first . $last4;

                $last5 = substr($price_my, -5);
                $first = substr($price_sg, 0, 1);

                if ($last5 == 98000 || $last5 == 99000 || $last5 == '09000') {
                    $firm =  $first . $last5;
                }
                break;
            case 5:
                $last2 = substr($price_my, -2);
                $first = substr($price_sg, 0, 3);
                $firm = $loose = $first . $last2;

                $last3 = substr($price_my, -3);
                $first = substr($price_sg, 0, 2);

                if ($last3 == 890 || $last3 == 990) {
                    $firm =  $first . $last3;
                }
                break;
            case 4:
                $last2 = substr($price_my, -2);
                $first = substr($price_sg, 0, 2);

                $firm =  $loose = $first . $last2;

                break;
            case 3:
                $last2 = substr($price_my, -1);
                $first = substr($price_sg, 0, 2);

                $firm = $loose = $first . $last2;
                break;
            default:
                $firm = $loose = 0;
        }

        return compact('firm', 'loose');
    }

    // show inactive price
    public function getInactivePriceAttributes()
    {
        $country_id = country()->country_id;

        $prices = $this->getInactivePricingModel($country_id);
        return $prices;
    }

    public function getInactivePricingModel($country_id)
    {
        $MyPrice = $this->morphMany('App\Models\Products\ProductPrices', 'model')
            ->select($this->priceTypes)
            ->where('country_id', 'MY')
            ->where('active', 0)
            ->get();

        if ($country_id == "MY") return $MyPrice;
    }
}
