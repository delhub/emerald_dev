@inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>{{$controller::$packHeaderEnd}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div> --}}
        </div>
        <div class="table-responsive m-2">

            <div class="col-12">
                <form action="{{route($controller::$packEndWork)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12 col-md-4 text-md-right my-auto">
                        <p>
                            Select Bin<small class="text-danger">*</small>
                        </p>
                    </div>

                    <div class="col-12 col-md-8 form-group">
                        <select name="selected_bin" class="form-control">
                            <option value="" >Please Select...</option>

                            @foreach($pickBins as $pickBin)
                                <option value="{{ $pickBin->id }}" >{{ $pickBin->bin_number}} - {{$pickBin->packEndBatch->batch_number}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-danger"
                            onclick="clickSubmit({{ $user->id }});">Pack Done</button>
                        </div>
                    </div>
                </form>
            </div>

           {{-- <p style="text-align: center"> Please selct Bin</p>
           <select name="bin_select" class="form-control">
               @foreach ($pickBins as $pickBin)
                <option value="{{$pickBin->id}}">{{$pickBin->pick_number}}<option>
               @endforeach
           </select> --}}
            {{-- <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p> --}}
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit(userId) {
    if (confirm('Are you sure completed pack?')) {
        document.getElementById("create-form-id").submit();
    }
}
</script>
@endpush
