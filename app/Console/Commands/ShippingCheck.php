<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\ShippingInstallations\Ship_category;

class ShippingCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipping:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check shipping start/end date and update status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel("schedule")->info('Shipping checking Start');

        $shippingCategorySpecial = Ship_category::where('cat_type','shipping_special')->get(); 

        $now = Carbon::now()->startOfDay();
        $tz = 'Asia/Kuala_Lumpur';

        $startDate = NULL;

        foreach ($shippingCategorySpecial as $key => $category) {
           
            $startDate = $category->start_date;
            $endDate = $category->end_date;

            if ($startDate == $now) {

                $category->active = 1;

                Log::channel("schedule")->info("'.$category->cat_name.' update to active. 
                Category ID : '.$category->id.', Start Date - '.$category->start_date.', Start End - '.$category->end_date.' has been SET");
            }

            if ($endDate == $now) {

                $category->active = 0;
                
                Log::channel("schedule")->info("'.$category->cat_name.' update to Inactive. 
                Category ID : '.$category->id.', Start Date - '.$category->start_date.', Start End - '.$category->end_date.' has been SET");
            }

            $category->save();
        }

        Log::channel("schedule")->info('Shipping checking End');
    }
}
