<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Models\Globals\Cards\Issuer;
use App\Models\Globals\PaymentGateway\MerchantID;
use App\Models\Globals\PaymentGateway\Response;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use App\Models\Users\Customers\PaymentInfo;
use App\Models\Users\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Cookie;


class CardGatewayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function submitForm($request, $purchase, $user)
    {

        $merchantId = MerchantID::where('card_type', $request->input('card_type'))->first();

        $cardIssuer = Issuer::where('issuer_name', $request->input('card_type'))->first();


        $paymentInfo = new PaymentInfo;
        $paymentInfo->account_id = $user->userInfo->account_id;

        $paymentInfo->card_number = $request->input('card_number');
        $paymentInfo->issuer_id = $cardIssuer->id;
        $paymentInfo->name_on_card = $request->input('name_on_card');
        $paymentInfo->expiry_date = $request->input('expiry_date');

        $cvv2 = $request->input('cvv');

        if ($purchase->installment_data) {
            $payment_amount = $purchase->installment_data->totalpayment * 100;
        } else {
            $payment_amount = $purchase->purchase_amount;
        };

        $type = 'cc';

        $responseUrl = URL::to('/payment/gateway-response?process=card');


        return view('shop.payment.card.post-to-gateway')
            ->with('purchase', $purchase)
            ->with('paymentInfo', $paymentInfo)
            ->with('cvv2', $cvv2)
            ->with('merchantId', $merchantId)
            ->with('responseUrl', $responseUrl)
            ->with('payment_amount', $payment_amount);
    }

    public function gatewayResponse($request)
    {

        $paymentGatewayResponse = new PaymentGatewayController;

        // Card payment gateway

        $payment = Payment::where('purchase_number', $request->input('invoiceNo'))->first();

        Log::channel('transactions')->info($request->input());
        if (isset($payment) && in_array($payment->gateway_response_code, ['00', 'BP'])) {


            $errorMessage = new \stdClass();
            $errorMessage->id = 0;
            $errorMessage->response_code = 'FAILED';
            $errorMessage->description = "Duplicated Payment Detected.";

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $purchase = Purchase::where('purchase_number', $request->input('invoiceNo'))->firstOrFail();
        $user = User::find($purchase->user_id);
        $response = $request->input('response');


        if ($response == '00') {

            // check
            $authCode = $request->input('authCode');
            $invoiceNo = $request->input('invoiceNo');
            $PAN = $request->input('PAN');
            $expiryDate = $request->input('expiryDate');
            $amount =  sprintf('%012d', $purchase->purchase_amount);
            $secretCode = 'DCSGLVP01';
            $checkhash = base64_encode(hash('sha256', $authCode . $response . $invoiceNo . $secretCode . $PAN . $expiryDate . $amount, true));
            $hash = $request->input('hash');

            if ($checkhash != $hash) {
                $success = false;
                $failed_message = $this->processCardPayment($request, $purchase, $success);

                $errorMessage = new \stdClass();
                $errorMessage->id = 0;
                $errorMessage->response_code = 'FAILED';
                $errorMessage->description = "Hash Validation Failed.";

                return view('shop.payment.failed')
                    ->with('errorMessage', $errorMessage);
            }

            // Success
            $success = true;
            $this->processCardPayment($request, $purchase, $success);

            $paymentGatewayResponse->updateGroupSales($user, $purchase);


            $check_registration = true;

            $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration, false, false);

            GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);

            $purchaseNumberFormatted = $purchase->getFormattedNumber();
            $cookie = json_decode(Cookie::get('bjs_cart'));

            $gateway = 'cc';
            $transaction_id = 'Card ' .  $request->input('PAN') . ' - ' .  $request->input('authCode');

            $paymentGatewayResponse->sendToMarketingServer($user->userInfo->account_id, $purchase->getFormattedNumber(),  $purchase, $purchase->purchase_amount / 100, $gateway, $transaction_id);
            return $paymentGatewayResponse->markOrderComplete($purchase, $user, $cookie);
        } else {

            $success = false;
            $failed_message = $this->processCardPayment($request, $purchase, $success);

            $errorMessage = $failed_message;

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }
    }

    public function processCardPayment($request, $purchase, $success)
    {

        $payment = new Payment;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->gateway_string_result = $request->input('result');
        $payment->gateway_response_code = $request->input('response');
        $payment->auth_code = $request->input('authCode');
        $payment->last_4_card_number = $request->input('PAN');
        $payment->expiry_date = $request->input('expiryDate');
        $payment->amount = $request->input('amount');
        $payment->gateway_eci = $request->input('ECI');
        $payment->gateway_security_key_res = $request->input('securityKeyRes');
        $payment->gateway_hash = $request->input('hash');
        $payment->country_id = country()->country_id;
        if ($success) $payment->save();
        $response = Response::find($payment->gateway_response_code);

        $payment->response_message = ($response) ? $payment->response_code . ' - ' . $response->description : $payment->response_code . ' - Not Specified.';
        $purchase->purchase_status = ($success) ? (in_array($purchase->payment_type,['7003','7004']) ? 4002 : 4007) : 4006;
        $purchase->save();


        return $response;
    }

    public function check_hash()
    {
        return;
    }

    public function check_duplicate()
    {
        return;
    }

    public function check_status()
    {
        return;
    }
}
