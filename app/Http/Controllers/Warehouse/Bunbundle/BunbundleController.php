<?php

namespace App\Http\Controllers\Warehouse\Bunbundle;

use App\Http\Controllers\Administrator\v1\Product\WarehouseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Models\Warehouse\Location\Wh_location;
use Illuminate\Support\Facades\Auth;
use App\Models\Users\User;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundle;
use App\Models\Products\ProductAttribute;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Purchases\Counters;
use App\Models\Products\WarehouseBatchItem;

class BunbundleController extends Controller
{
    public static $home;
    public static $header;
    public static $transferStart;

    public static $headerTransferList;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$header = 'Product Bundle Unbundle Process';

        self::$transferStart = 'warehouse.b_unbundle.start';

        self::$headerTransferList = 'Bundle / Unbundle';

        // link to view page
        // view('warehouse.Bunbundle.index');
    }

    public function index()
    {
        $tables = WarehouseBunbundle::orderBy('id', 'ASC')->paginate(10);

        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location; 
        $locations = wh_location::get();
        if (!$user->hasRole('wh_supervisor')) $locations = $locations->whereIn('id', $userLocationId);

        $panels = PanelInfo::get();
        $puchongWarehouse = Wh_location::where('id', 1)->first();
        $products = ProductAttribute::selectRaw('panel_product_attributes.*')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->orderBy('panel_product_attributes.product_code', 'ASC')
            ->where('panel_product_attributes.active', 1)
            ->where('global_products.product_status', 1)
            ->where('global_products.no_stockProduct', 0)
            ->get();

        $process = WarehouseBunbundle::selectRaw('wh_product_b_unbundle.*')
            ->where('user_id', $user->id)
            ->where('completed', 0)
            ->first();

        if ($process != NULL) {
            return view('warehouse.Bunbundle.scanProduct', compact('process'));
        } else {
            return view('warehouse.Bunbundle.index', compact('tables', 'user', 'locations', 'panels', 'puchongWarehouse', 'products'));
        }
    }

    public function start(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $request->validate([
            'product_code' => ['required'],
            'type' => ['required'],
            'quantity' => ['required'],
            'remark' => ['required'],
        ]);

        $warehouseBunbundle = new WarehouseBunbundle;
        $warehouseBunbundle->user_id = $user->id;
        $warehouseBunbundle->location_id = $request->input('location_id');
        $warehouseBunbundle->type = $request->input('type');
        $warehouseBunbundle->from_product_code = $request->input('from_product_code');
        $warehouseBunbundle->product_code = $request->input('product_code');
        $warehouseBunbundle->quantity = $request->input('quantity');
        $warehouseBunbundle->expiry_date = $request->input('expiry_date');
        $warehouseBunbundle->panel_id = $request->input('panel_id');
        $warehouseBunbundle->remark = $request->input('remark');
        $warehouseBunbundle->save();

        return redirect()->route('warehouse.b_unbundle.scanProduct')->with('success', 'Start Scan Product for ' . $request->input('type') . '');
    }

    public function scanProduct()
    {
        $user = User::find(Auth::user()->id);
        $process =  WarehouseBunbundle::where('wh_product_b_unbundle.user_id', $user->id)
            ->where('completed', 0)
            ->first();

        return view('warehouse.Bunbundle.scanProduct', compact('process'));
    }

    public static function end(Request $request, $id = NULL)
    {
        if (Auth::user()) {
            $user_id = User::find(Auth::user()->id)->id;

        }else{
            $user_id = 0;
        }

        $bUnbundle = WarehouseBunbundle::selectRaw('wh_product_b_unbundle.*')
            ->join('wh_product_b_unbundle_item', 'wh_product_b_unbundle_item.b_unbundle_key', '=', 'wh_product_b_unbundle.id')
            ->where('wh_product_b_unbundle.user_id', $user_id)
            ->where('wh_product_b_unbundle.id', $request->stockTransferId)
            ->where('completed', 0)
            ->first();

        if ($bUnbundle == NULL) return back()->with('error', 'Please scan product!');

        foreach ($bUnbundle->bUnbundleItems as $key => $item) {

            if (!isset($data[$item->batchItem->batchDetail->product_code])) {
                $data[$item->batchItem->batchDetail->product_code] = array();
                $data[$item->batchItem->batchDetail->product_code]['qty'] = 0;
            }

            $data[$item->batchItem->batchDetail->product_code]['qty']++;

            $item->batchItem->picking_order_id = $bUnbundle->id;
            $item->batchItem->type = $bUnbundle->type;
            $item->batchItem->save();
        }

        //Plus, selected product code
        $productCodeAdd = $bUnbundle->product_code;
        $inventoryRecordsPlus[$productCodeAdd]['outlet_id'] = $bUnbundle->location_id;
        $inventoryRecordsPlus[$productCodeAdd]['inv_type'] = 8019;
        $inventoryRecordsPlus[$productCodeAdd]['inv_amount'] = $bUnbundle->quantity;
        $inventoryRecordsPlus[$productCodeAdd]['inv_user'] = 0;
        $inventoryRecordsPlus[$productCodeAdd]['model_type'] = WarehouseBunbundle::class;;
        $inventoryRecordsPlus[$productCodeAdd]['model_id'] = $bUnbundle->id;
        $inventoryRecordsPlus[$productCodeAdd]['product_code'] = $productCodeAdd;

        foreach ($data as $productCode => $value) {
            //Minus,  scanned product
            $inventoryRecordsMinus[$productCode]['outlet_id'] = $bUnbundle->location_id;
            $inventoryRecordsMinus[$productCode]['inv_type'] = 8020;
            $inventoryRecordsMinus[$productCode]['inv_amount'] = - ($value['qty']);
            $inventoryRecordsMinus[$productCode]['inv_user'] = 0;
            $inventoryRecordsMinus[$productCode]['model_type'] = WarehouseBunbundle::class;;
            $inventoryRecordsMinus[$productCode]['model_id'] = $bUnbundle->id;
            $inventoryRecordsMinus[$productCode]['product_code'] = $productCode;
        }

        InventoryController::insertData($inventoryRecordsPlus);
        InventoryController::insertData($inventoryRecordsMinus);

        //add warehouse batch
        $request->product_code = array($bUnbundle->product_code);
        $request->quantity = array($bUnbundle->quantity);
        $request->repeatable = array(0);
        $request->expiry_date = array($bUnbundle->expiry_date);
        $request->lot = array(null);

        $request->panel_id = $bUnbundle->panel_id;
        $request->company_name = NULL;
        $request->batch = Counters::autoIncrement(['b_unbundle'], 4, '.')['b_unbundle'];;
        $request->stock_in_date = NULL;
        $request->po_number = NULL;
        $request->inv_number = NULL;
        $request->vendor_desc = NULL;
        $request->batch_type = $bUnbundle->type;
        $request->notes = $bUnbundle->remark;

        $request->location_id = $bUnbundle->location_id;

        WarehouseController::addBatch($request);
        //end add warehouse batch

        if (isset($request->imAuto)) {
            $updates = WarehouseBatchItem::where('batch_id', $request->batch)->get();
            foreach ($updates as $update) {
                $update->type = $request->process;
                $update->picking_order_id = $request->picking_order_id;
                $update->save();
            }
        }

        //complete process
        $bUnbundle->completed = 1;
        $bUnbundle->save();

        return redirect()->route('warehouse.b_unbundle.index')->with('success', $bUnbundle->type . ' process completed! Please print product qr code in warehouse');
    }

    public function list()
    {
        $tables = WarehouseBunbundle::with([
            'theUser.userInfo',
            'location',
            'productAttributeFrom.product2.parentProduct',
            'bUnbundleItems.batchItem.batchDetail.productAttribute.product2.parentProduct',
            'productAttribute.product2.parentProduct',
            'panel'
        ])->get();

        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        if (!$user->hasRole('wh_supervisor')) $tables = $tables->whereIn('location_id', $userLocationId);

        $tranfer = array();

        return view('warehouse.Bunbundle.list', compact('tables', 'tranfer'));
    }

    // public function editNotes(Request $request)
    // {
    //     $batch = StockTransfer::where('id', $request->batchID)->first();
    //     $batch->notes = $request->notes;
    //     $batch->save();

    //     return true;
    // }
}
