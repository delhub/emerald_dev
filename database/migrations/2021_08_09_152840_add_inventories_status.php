<?php

use App\Models\Globals\Status;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddInventoriesStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_statuses', function (Blueprint $table) {
            //
        });

        DB::table('global_statuses')->insert([
            [
                'id' => '8000',
                'name' => 'Purchase Order - Stock Out',
                'description' => 'Product out process complete for purchase order',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8001',
                'name' => 'Purchase Order - Stock Hold',
                'description' => 'Product for purchase order to blocked / hold stock',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8002',
                'name' => 'Purchase Order - Release Hold Stock',
                'description' => 'Release from blocked stock for the purchase order',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8003',
                'name' => 'Inventory Adjustment',
                'description' => 'Subsequent adjustment for subcontracting',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8004',
                'name' => 'Return Deliveries',
                'description' => 'Return deliveries to vendor',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8005',
                'name' => 'Return From Hold Stock',
                'description' => 'Return deliveries to vendor from hold/blocked stock',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8006',
                'name' => 'Return From Customer',
                'description' => 'Return from customer (without shipping)',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8007',
                'name' => 'Return Stock Transfer',
                'description' => 'Return stock transfer',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8008',
                'name' => 'Stock In - In Process',
                'description' => 'Stock In - In Process',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8009',
                'name' => 'Stock In - On-Hold',
                'description' => 'Stock In - On-Hold',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '8010',
                'name' => 'Stock In - Completed',
                'description' => 'Stock In - Completed',
                'status_type' => 'inventory_status',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('global_statuses', function (Blueprint $table) {
        //     //
        // });
        $status = Status::where('id', '>=', 8000)->where('id', '<=', 8010)->delete();
    }
}
