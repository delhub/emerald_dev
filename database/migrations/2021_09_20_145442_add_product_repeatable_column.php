<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Products\WarehouseBatchItem;

class AddProductRepeatableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function ($table) {
            $table->tinyInteger('repeatable')->default(0)->after('shipping_category_special');
        });

        Schema::table('wh_batch_item', function ($table) {
            $table->string('type')->nullable()->after('picking_order_id');
            $table->tinyInteger('repeatable')->default(0)->after('type');
        });

        Schema::table('wh_inventories', function ($table) {
            $table->tinyInteger('repeatable')->default(0)->after('inv_user');
        });

        $warehouseBatchItem = WarehouseBatchItem::whereNotNuLL('picking_order_id')->get();

        foreach ($warehouseBatchItem as $key => $item) {
            $item->type = 'pick';
            $item->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('repeatable');
        });

        Schema::table('wh_batch_item', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('repeatable');
        });

        Schema::table('wh_inventories', function (Blueprint $table) {
            $table->dropColumn('repeatable');
        });
    }
}
