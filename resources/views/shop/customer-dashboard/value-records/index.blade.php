@extends('layouts.management.main-customer')
@section('content')
@section('page_title')
{{ "Welcome to My Profile" }}
@endsection

<div class="container-fluid px-0 px-md-5">
    <div class="row align-items-end">
        <div class="col-6">
            <div class="text-header">
                <p class="headerText myfont_weight700">Value Records</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid px-0 px-md-5">
    <div style="font-size:small;">
        <form action="{{ route('shop.customer.orders') }}" method="GET">
            <div class="row mt-2">

                <!-- Date filter from date to date-->
                <div class="col-md-auto">
                    <div class="input-group mb-3">
                        <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{ ($request->datefrom) ? $request->datefrom : $request->dateto }}" autocomplete="off">
                    </div>
                </div>

                <div class="col-md-auto">
                    <div class="input-group mb-3">
                        <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                            aria-label="Username" aria-describedby="basic-addon1"
                            value="{{ ($request->dateto) ? $request->dateto : $request->datefrom }}" autocomplete="off">
                    </div>
                </div>

                <div class="col-md-auto">
                    <button type="submit" class="btn btn-warning btn-block mb-3" style="color:black;" border="2px solid">Filter</button>
                </div>
                <div class="col-md-auto">
                    <button type="submit" class="btn btn-warning btn-block mb-3" style="color:black;"><a href={{ route('shop.customer.orders') }} style="color:black;">Reset Filter</a></button>
                </div>


            </div>
        </form>
    </div>
</div>

<div class="container-fluid px-0 px-md-5">
    <div class="row align-items-end">
        <div class="col-12 mywebmenu">
            <div class="row">
                <div class="col-6">
                    <a href="/shop/dashboard/orders/index" class="my_order_text active">
                        <strong>All Orders</strong>
                    </a>
                    <a href="#" class="my_order_text">
                        <strong>Pending Star Ratings</strong>
                    </a>
                </div>
                <div class="col-6 d-flex justify-content-end">
                    <strong>
                        {{$purchases->sum('orders_count')}}
                    </strong> orders placed
                </div>
            </div>
        </div>
        <div class="col-6 mymobilemenu">
            <button class="btn btn-secondary  bjsh-btn-gradient" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                style="background-color:rgb(250, 172, 24); color:black;">
                All Orders <i style="font-size: 10px;" class="fa fa-arrow-down"> </i>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                {{-- <a class="dropdown-item" href="#">All Orders</a> --}}
                {{-- <a class="dropdown-item" href="/shop/dashboard/orders/open-orders">Open Orders</a> --}}
                <!-- <a class="dropdown-item" href="/shop/dashboard/orders/order-status">Order Status</a> -->
                {{-- <a class="dropdown-item" href="#">Return Orders</a> --}}
                <a class="dropdown-item" href="#">Pending Star Ratings</a>
            </div>
        </div>
    </div>

    @if(!$purchases->isEmpty())
    <div class="row">
        <div class="col-12 bundle my0padding">
            @foreach($purchases as $purchase)

            <div class="card">
                <div class="card-header-first myholizontalline" style="padding: 20px 20px 10px 20px;">
                    <div class="row">
                        <!--<div class="col-md-7">
                            <h4 class="mywidth0 float-left" style="font-weight:bold; color:rgb(250, 172, 24);">
                                Purchase #: {{ $purchase->getFormattedNumber()}}
                            </h4>
                        </div> -->
                        <div class="col-md-12 resp01">
                            <div class="mymobilepad">
                                @if (isset($purchase->inv_number))
                                    <h4 class="mywidth0">
                                        Invoice #: {{ $purchase->inv_number}}
                                    </h4>
                                    <br>
                                @endif
                                <h4 class="mywidth0">
                                    Order #: {{ $purchase->getFormattedNumber()}}
                                </h4>
                                <span>
                                    Order Date:{{ $purchase->purchase_date }}
                                </span>
                            </div>
                            <div class="mymobilepad">
                                <span class="float-left">
                                    Order Date:{{ $purchase->purchase_date }}
                                </span>
                                @if ($purchase->installment_data)
                                @php if (!isset($purchase->installment_data->productdescription)){
                                    $installment_data =  $purchase->installment_data;
                                    $installment_data->productdescription = 'Bujishu Installment';
                                    $purchase->installment = json_encode($installment_data);
                                } @endphp


                                <img src="/storage/icons/payments/DC-Credit.png" alt="" class="px-1" style="height: 35px;"> <a class="btn bjsh-btn-gradient"
                                href="@dc_credit_url($purchase)">Apply DC Credit</a>
                                <span class="pl-1"></span>

                                @endif
                                <button
                                    class="text-uppercase mybut03 mywidth0 float-left {{$purchase->purchase_status}}"
                                    disabled>{{$status[$purchase->purchase_status]}}</button>

                                @if (!in_array($purchase->purchase_status, [3000, 4005, 4006,4004]))
                                <div class="mywidth0 float-right text-capitalize mybut02">
                                    <a
                                        href="{{asset('/storage/documents/invoice/'.$purchase->getFormattedNumber().(($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/').$purchase->getFormattedNumber().'-receipt.pdf')}}">
                                        Receipt
                                    </a>
                                </div>
                                <div class="mywidth0 float-right text-capitalize mybut02">
                                    <a
                                        href="{{asset('/storage/documents/invoice/'.$purchase->getFormattedNumber().(($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/').$purchase->getFormattedNumber().'.pdf')}}">
                                        Invoice
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($purchase->sortItems() as $item)

                    @if ($item->bundle_id == 0)
                    <hr class="my-hr">
                    @endif
                    <div class="card-body myordercard @if ($item->bundle_id != 0) bundle @endif">
                        @if ($item->bundle_id != 0)
                            <div class="bundle-tag">
                                <P>IN BUNDLE</P>
                            </div>
                        @endif
                        <div class="row px-md-5 pt-md-2 pb-md-1">
                            <div class="row col-md-7 my0padding @if ($item->bundle_id != 0) mx-auto @endif">

                                <div class="col-md-2 my0padding  @if (!$item->product) border-1 @endif">
                                    {{-- <a
                                        href="/shop/product/{{ $item->product->parentProduct->name_slug}}?panel={{$item->product->panel_account_id}}" class="mb-size"> --}}

                                        <!-- New Version latest 1 -->

                                        @if(array_key_exists('product_color_img', $item->product_information) && isset($item->product))

                                        @foreach($item->product->parentProduct->images as $value)

                                            @if ($value['id'] == $item->product_information['product_color_img'] )
                                                <img class=" @if ($item->bundle_id == 0) responsive-img  main-img @else img-box @endif" style="background:@if (array_key_exists('product_color_code', $item->product_information)){{$item->product_information['product_color_code']}}@endif;" src="{{ asset('storage/' . $value['path'] . '' . $value['filename']) }}" >
                                            @endif

                                        @endforeach

                                        @elseif(isset($item->product->parentProduct))
                                        <img class="responsive-img main-img"
                                            src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename) }}"
                                            alt="Product Image" />
                                        @endif

                                    </a>
                                </div>

                                <div class="col-md-5 my-al-lc myorderlist my-auto">
                                    @if (isset($item->product->parentProduct))
                                    <a class="ordername text-capitalize pb-3"
                                    href="/shop/product/{{ $item->product->parentProduct->name_slug}}?panel={{$item->product->panel_account_id}}">{{ $item->product->parentProduct->name }}</a>

                                    @endif
                                    {{-- <a class="ordername text-capitalize pb-3"
                                        href="/shop/product/{{ $item->product->parentProduct->name_slug}}?panel={{$item->product->panel_account_id}}">{{ $item->product->parentProduct->name }}</a> --}}

                                    {{-- <p style="display:none;" class="text-capitalize">Sold by:
                                        {{ ($item->product->display_panel_name != "default") ? $item->product->display_panel_name : $item->product->panel->company_name }}
                                    </p> --}}

                                    @if(array_key_exists('product_miscellaneous', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">
                                        <strong>{{ $item->product_information['product_miscellaneous'] }}</strong>
                                        <br>
                                    </p>
                                    @if(array_key_exists('invoice_number', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">
                                        Invoice Number:
                                        <strong>{{ $item->product_information['invoice_number'] }}</strong>
                                        <br>
                                    </p>
                                    @endif
                                    @else
                                    <p style="font-size:14px;line-height: 23px;">Quantity:
                                        <strong>{{$item->quantity}}</strong></p>
                                    <!-- <p style="display:none;" class="text-capitalize">Unit Price:
                                        <?php /* echo 'RM ' . number_format(($item->product->price / 100), 2); */?>
                                    </p>-->
                                    @endif
                                    @if(array_key_exists('product_color_name', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">Color:
                                        <strong> {{ $item->product_information['product_color_name'] }} </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_size', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">Size:
                                        <strong> {{ $item->product_information['product_size'] }} </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_curtain_size', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">Curtain Model:
                                        <strong> {{ $item->product_information['product_curtain_size'] }} </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_temperature', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">Color
                                        Temperature:
                                        <strong> {{ $item->product_information['product_temperature'] }} </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_order_remark', $item->product_information))
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">
                                        Remarks:
                                        <strong> {{ $item->product_information['product_order_remark'] }} </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_order_selfcollect', $item->product_information) &&
                                    $item->product_information['product_order_selfcollect'])
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">
                                        Self Collect:
                                        <strong> Yes </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_order_tradein', $item->product_information) &&
                                    $item->product_information['product_order_tradein'])
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">
                                        Rebate:
                                        <strong> -
                                            RM
                                            {{ number_format($item->product_information['product_order_tradein'] / 100),2 }}
                                        </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_installation', $item->product_information) &&
                                    $item->product_information['product_installation'])
                                    <p style="font-size:14px;    line-height: 23px;" class="text-capitalize">
                                        Installation:
                                        <strong>
                                            {{($item->installationCategories()!=NULL) ? $item->installationCategories()->cat_name : ''}}
                                        </strong>
                                        <br>
                                    </p>
                                    @endif
                                    @if ($item->bundle_id != 0)
                                    <p style="font-size:14px;line-height: 23px;">In Bundle :
                                        <strong>({{$item->getParentName($item->bundle_id)}})</strong>
                                    </p>
                                    @endif
                                </div>
                            </div>

                            @if (!$item->childItem->count())
                            <div class="col-md-5 resp01 pt-md-2 pt-3 s99">
                                <div class="mymobilepad float-right">
                                    <div class="mymobile-mt">
                                        <div class="processiconbox">
                                            <div class="myprocessstepbox">
                                                <div class="mystep">
                                                    <img class="img-process active" title="Order Placed"
                                                        src="{{ asset('assets/images/icons/shop_order/cart_g.svg') }}">
                                                </div>
                                                <div class="midline"></div>

                                                <!-- Show ship out date -->

                                                @php

                                                /* if ($item->ship_date !='Pending' || $item->actual_ship_date !='Pending') {
                                                $shipDate =($item->actual_ship_date !='Pending') ? $item->actual_ship_date :
                                                $item->ship_date;
                                                }else {
                                                $shipDate = ($item->order->delivery_date != 'Pending') ? date("d-m-Y",
                                                strtotime($item->order->delivery_date)) : $item->order->delivery_date ;
                                                } */

                                                $shipDate = $item->ship_date;

                                                // if ($item->collected_date != 'Pending') {
                                                if (($item->collected_date == 'Pending') ||
                                                (is_null($item->collected_date))) {
                                                $collectedDate = date("d-m-Y", strtotime($item->order->received_date));
                                                } else {
                                                $collectedDate = date("d-m-Y", strtotime($item->collected_date));
                                                }

                                                 switch ($item->item_order_status ) {
                                                case '1002': // Order Shipped
                                                case '1008': // ACD Delivered
                                                case '1010': // ACD Shipout
                                                case '1011': // ACD Self Delivered
                                                $icon = 'lorry';
                                                break;

                                                case '1009': // ACD Collected
                                                case '1003': // Order Delivered
                                                $icon = 'collected';
                                                break;

                                                default:
                                                $icon = 'none';
                                                    break;

                                            }


                                                @endphp


                                                <div class="mystep">
                                                    <img id="lorry"
                                                        class=" img-process
                                                        {{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending')  ? 'active' : '' }}"
                                                        src="{{ asset('assets/images/icons/shop_order/ship_g.svg') }}"
                                                        data-toggle="tooltip"
                                                        title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? 'Estimate ship out date : ' . $shipDate : '' }}">

                                                </div>
                                                <div class="midline"></div>
                                                <div class="mystep">
                                                    <img class="img-process
                                                        {{  ( $icon == 'collected') ? 'active' : '' }}"
                                                        src="{{ asset('assets/images/icons/shop_order/receive_g.svg') }}">
                                                </div>
                                                <div class="midline"></div>
                                                <div class="mystep">
                                                    <img class="img-process {{( $icon == 'collected') ? 'active' : ''}}"
                                                        src="{{ asset('assets/images/icons/shop_order/complete_g.svg') }}"
                                                        data-toggle="tooltip"
                                                        title="{{ ($item->item_order_status == '1003') || ($item->order->order_status == '1003') || ($item->item_order_status == '1009') || ($item->order->order_status == '1009') ? 'Order Complete : ' . $collectedDate : '' }}">
                                                </div>

                                            </div>

                                            {{-- shipdate - {{ $item->ship_date }}
                                            <br>
                                            order - {{ $order->delivery_date }}
                                            <br>
                                            {{ $shipDate }}
                                            <br>
                                            item - {{ $item->collected_date }}
                                            <br>
                                            order - {{ $order->received_date }}
                                            <br>
                                            {{ $collectedDate }} --}}

                                            <br>
                                            <br>
                                            <div class="text-center pt-2">
                                                @if($item->courier_name && $item->tracking_number)
                                                    @php $tracking_link = \App\Models\Courier\Courier::trackingURL($item->courier_name, $item->tracking_number); @endphp

                                                    <a class="btn btn-sm bjsh-btn-gradient" @if($tracking_link != "#") target="_blank" href="{{$tracking_link}}"@endif >{{$item->courier_name}} Tracking: {{$item->tracking_number}}</a>

                                                @else
                                                    {{-- <button class="mybut03 mywidth0 btn-sm" disabled="">Tracking Not Available</button> --}}
                                                @endif
                                             </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>

                @endforeach
            </div>

            @endforeach
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-12">
            <div class="card text-center">
                <div class="card-header">
                    <h6> There are no orders found.</h6>
                </div>
                <div class="card-body">
                    <h5 class="card-title">
                        <a class="btn bjsh-btn-gradient" href="/shop">Continue Shopping</a>
                    </h5>

                </div>
            </div>
        </div>
        @endif
    </div>

    <style>
        .card .my-hr{
            display: none;
        }
        .card .my-hr ~ .my-hr{
            display:block;
        }
        .my-hr{
            width: 90%;
            align-self: center;
            border-top: 0.01em solid #ffcc006e;
        }
        .img-box{
            width: 70%;
            height: 100px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
            margin-left: 30%;
            color: #fff;
        }
        .my-ml{
            margin-left:0;
        }
        .mb-size{
            padding-left: 0;
        }
        .button-status {
            font-size: small;
            border-radius: 5px;
            padding: 5px 8px;
            font-weight: bold;
            color: #666;
        }

        .fa.fa-star-o {
            color: rgb(250, 172, 24);
        }
        .my0padding{
            padding:0;
        }
        .bundle-sub{
            object-fit: cover;
            max-width: 80%;
            float: right;
        }
        .bundle.my0padding{
            padding-right: 15px;
            padding-left: 15px;
        }
        .bundle-tag{
            display: none;
        }
        .bundle-title{
            display: block;
        }
        .bundle-activeicon{
            display:none;
        }
        @media (max-width: 425px) {
            .img-box {
                height: auto;
                width: 80%;
                margin: 0 10%!important;
            }
            .myordercard.bundle .midline{
                width: 12px;
            }
            .bundle-title{
                display: none;
            }
            .bundle-tag{
                display: block;
                background: #ffcc00;
                position: absolute;
                top: 0;
                right: 0;
                padding: 10px 15px;
                z-index: 99;
                border-bottom-right-radius: 10px;
                border-bottom-left-radius: 10px;
            }
            .bundle-tag p{
                line-height: 12px;
                margin: 0;
                font-weight: bold;
            }
            .bundle.my0padding{
                padding:0;
            }
            .myordercard {
                padding: 10px 1.25em;
            }
            .myordercard.bundle{
                margin: 10px 1.25em;
                box-shadow: 0px 0px 7px #00000036;
                padding-top: 40px;
                position: relative;
            }
            .midline {
                width: 16px;
                padding-top: 22px;
            }
            .mystep {
                width: 46px;
                float: left;
                margin: 0 7px;
            }
            .bundle-sub{
                max-width: 80%;
                float: none;
                margin: 0 10%!important;
            }
            .main-img{
                object-fit: cover;
                width: 80%;
                margin: 0 10%!important;
            }
        }

        @media (max-width: 414px) {
            .mb-size{
                padding-left: 0%;
            }

        }

        @media (max-width: 375px) {
            .midline {
                width: 26px;
                padding-top: 16px;
            }
        }

        @media (max-width: 320px) {
            .midline {
                width: 20px;
                padding-top: 14px;
            }
        }

        ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 7px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: rgba(0, 0, 0, .5);
            box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }
    </style>

    @push('script')
    <script>
        $( function() {
            $( document ).tooltip();
        } );

        $(".filterDate").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            // changeMonth: true,
            // changeYear: true
            });
    </script>

    @endpush



    @endsection
