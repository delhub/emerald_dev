@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Warehouse Batch</h1>
<div class="card shadow-sm">
    <div class="card-header">Warehouse Batch<small> - Add new</small>
        <div class="card-header-actions"><a class="card-header-action" href="#" target="_blank"><small class="text-muted">docs</small></a></div>
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-6">
                <form action="{{ route('administrator.v1.products.warehouse.add')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="col-form-label" for="panel">Panel</label>
                        <select class="form-control" name="panel" aria-readonly="true" readonly>
                            <option value="0">-- All Product --</option>
                            {{-- <option v-for="(c,i) in categories" :value="c.id">@{{c.name}}</option> --}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="stock_in_date">Stock In Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                            <input type="text" name="stock_in_date" id="stock_in_date" class="form-control" placeholder="Stock In Date" autocomplete="off" value="{{old('stock_in_date')}}">
                          </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="po_number">PO Number</label>
                        <input type="text" name="po_number" class="form-control" placeholder="PO Number" autocomplete="off" value="{{old('po_number')}}">
                    </div>
                    <hr>
                    <div class="form-row details" id="details">
                        <div class="form-group col-md-4">
                          <label for="product_code">Product Code</label>
                          <input type="text" name="product_code[]" class="form-control" placeholder="Product Code">
                        </div>
                        <div class="form-group col-md-3">
                          <label for="quantity">Quantity</label>
                          <input type="text" name="quantity[]" class="form-control" placeholder="Quantity">
                        </div>
                        <div class="form-group col-md-5">
                          <label for="expiry_date">Expiry Date</label>

                            <div class="input-group">
                                <input type="text" name="expiry_date[]" class="form-control expiry_date" placeholder="Expiry Date">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                      </div>
                    <hr>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </form>
            </div>



        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $('#stock_in_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
    $('.expiry_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });

    function cloneBatchDetails () {
        $("#details").clone().insertAfter("div.details:last");
    }
</script>
@endpush
