<div class="modal fade" id="purchaseAgreementModal" tabindex="-1" role="dialog" aria-labelledby="purchaseAgreementModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-12 mb-0">
                        <div class="overflow-auto" style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                             @if (country()->country_id == 'MY')
                                @include('shop.payment.service-terms-conditions-my')
                            @else
                                @include('shop.payment.service-terms-conditions-sg')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
