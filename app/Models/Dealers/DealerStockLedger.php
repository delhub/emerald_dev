<?php

namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerStockLedger extends Model
{
     // Set table
     protected $table = 'dealer_stockledger';

     // Set timestamps
     public $timestamps = true;

     // Set primary key
     protected $primaryKey = 'id';

     // Set mass assignable columns
     protected $fillable = [
        'ref_id',
        'type',
        'product_code',
        'quantity',
        'item_id',
        'agent_id',
        'date'
     ];

     // Cast column to another datatype.
     protected $casts = [];

     public function item()
     {
       return $this->belongsTo('App\Models\Purchases\Item', 'item_id', 'id');
     }
}
