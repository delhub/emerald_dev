<?php

use App\Models\Categories\Category;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SeedCategoryCareProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $shopCategories = Category::where('type','shop')->get();
        // foreach ($shopCategories as $shopCategory) {
        //     $shopCategory->delete();
        // }
        Category::truncate();

        DB::table('categories')->insert(
            array(
                array(
                    'name' => 'Oral',
                    'slug' => 'oral',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Kidney',
                    'slug' => 'kidney',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Liver',
                    'slug' => 'liver',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Lung',
                    'slug' => 'lung',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Hair',
                    'slug' => 'hair',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Men's Care",
                    'slug' => 'men-care',
                    'type' => 'product_care',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Women's Care",
                    'slug' => 'women-care',
                    'type' => 'product_care',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Baby's Care",
                    'slug' => 'baby-care',
                    'type' => 'product_care',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Kids's Care",
                    'slug' => 'kid-care',
                    'type' => 'product_care',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Aging Care",
                    'slug' => 'aging-care',
                    'type' => 'product_care',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Health Care",
                    'slug' => 'health-care',
                    'type' => 'product_care',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Oral",
                    'slug' => 'vitamin-oral',
                    'type' => 'shop',
                    'parent_category_id' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Oral 2",
                    'slug' => 'vitamin-oral-2',
                    'type' => 'shop',
                    'parent_category_id' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Kidney",
                    'slug' => 'vitamin-kidney',
                    'type' => 'shop',
                    'parent_category_id' => '2',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Kidney 2",
                    'slug' => 'vitamin-kidney-2',
                    'type' => 'shop',
                    'parent_category_id' => '2',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Kidney 3",
                    'slug' => 'vitamin-kidney-3',
                    'type' => 'shop',
                    'parent_category_id' => '2',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Liver",
                    'slug' => 'vitamin-liver',
                    'type' => 'shop',
                    'parent_category_id' => '3',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Lung",
                    'slug' => 'vitamin-lung',
                    'type' => 'shop',
                    'parent_category_id' => '4',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Vitamin Hair",
                    'slug' => 'vitamin-hair',
                    'type' => 'shop',
                    'parent_category_id' => '5',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Agent Report",
                    'slug' => 'agent-report',
                    'type' => 'agent',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => "Accounting Report",
                    'slug' => 'accounting-report',
                    'type' => 'accounting',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array('name' => 'Adult', 'slug' => 'adult', 'type' => 'suitable_for', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Elderly', 'slug' => 'elderly', 'type' => 'suitable_for', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Kids', 'slug' => 'kids', 'type' => 'suitable_for', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),

                array('name' => 'Malaysia', 'slug' => 'malaysia', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Australia', 'slug' => 'australia', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'Indonesia', 'slug' => 'indonesia', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
                array('name' => 'India', 'slug' => 'india', 'type' => 'origin_country', 'parent_category_id' => '0', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );


        Schema::table('panel_products', function (Blueprint $table) {
            $table->text('product_content_3')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
