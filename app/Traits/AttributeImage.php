<?php

 namespace App\Traits;

use App\Models\Globals\Image;
use App\Models\Globals\Products\Product;
use App\Models\Users\User;
 use App\Models\Users\Customers\Cart;
 use Illuminate\Support\Facades\Auth;

 trait AttributeImage
 {
     public function getImageUrlAttribute($id)
     {
 
        $parentProduct = Product::find($id);
         
         $img = $parentProduct->images->where('brand','!=',1)->first();        

         return isset($img) ? $img->id : 0;
    }
     public function getImageUrlFromID($id)
     {
        $parentProduct = Product::find($id);

        if($parentProduct) {
            $img = $parentProduct->images->first();

            return $img;
        }     
     }
 }