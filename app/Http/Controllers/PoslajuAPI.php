<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class PoslajuAPI extends Controller
{
    public static $client;
    public static $apiServer;

    public static function apiServer()
    {
        if (\App::environment('prod')) {
            return 'https://send.pos.com.my';
        } else {
            return 'http://sendparcel-test.ap-southeast-1.elasticbeanstalk.com';
        }
    }

    public static function apiKey()
    {
        return config('app.poslaju_api_key');
    }

    //guzzle get api
    public static function guzzleGet($url)
    {
        $client = new Client();

        $response = $client->get($url, [
            "headers" => [
                "appid" => env,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        } else {
            return $response;
        }
    }

    //guzzle post api
    public static function guzzlePost($url, $data, $type = 'json')
    {


        $client = new Client();

        $response = $client->post($url, [
            'form_params' => $data,
            'http_errors' => false
        ]);

        if ($type == 'pdf') {
            Log::channel("poslaju")->info($response->getStatusCode());
        }

        if ($response->getStatusCode() == 200) {

            switch ($type) {
                case 'pdf':
                    // header("Content-type:application/pdf");
                    $pdf = $response->getBody()->getContents();
                    $result = time() . '_consignment_note.pdf';
                    Storage::disk('public')->put('/uploads/poslaju/' . $result, $pdf);
                    Log::channel("poslaju")->info($result);
                    break;
                default:
                    $response = $response->getBody();
                    $result = json_decode($response);
            }
            return $result;
        } else {
            return $response;
        }
    }

    // To get user details which belong to the api_key.
    public static function getMe()
    {
        $body = [
            'api_key' => self::apiKey()
        ];

        $url = self::apiServer() . '/apiv1/me';
        $result = self::guzzlePost($url, $body);

        return $result->data;
    }

    public static function create_shipment($data)
    {
        // $me = self::getMe();

        $body = [
            'api_key' => self::apiKey(),
            'send_method' => 'pickup',
            'send_date' => Carbon::now()->toDateString(),
            'type' => 'parcel',
            'declared_weight' => '0.5', // 1KG
            'size' => 'box',
            'width' => '10',
            'length' => '10',
            'height' => '10',
            'provider_code' => 'poslaju',
            'content_type' => 'general',
            'content_description' => $data['content_description'],
            'content_value' => $data['content_value'],
            'sender_name' => 'FORMULA HEALTHCARE',
            'sender_phone' => '03-5975 8613',
            'sender_company_name' => 'FORMULA HEALTHCARE SDN. BHD',
            'sender_email' => 'formula2u@gmail.com',
            'sender_address_line_1' => 'No 26, Jalan Sierra 10/2',
            'sender_address_line_2' => 'Sierra Zentro I,',
            'sender_address_line_3' => 'Bandar 16 Sierra,',
            'sender_address_line_4' => 'Puchong, Selangor',
            'sender_postcode' => '47110',
            'receiver_name' => $data['receiver_name'],
            'receiver_phone' => $data['receiver_phone'],
            'receiver_email' => $data['receiver_email'],
            'receiver_address_line_1' => $data['receiver_address_line_1'],
            'receiver_address_line_2' => $data['receiver_address_line_2'],
            'receiver_address_line_3' => $data['receiver_address_line_3'],
            'receiver_address_line_4' => $data['receiver_address_line_4'],
            'receiver_postcode' => $data['receiver_postcode'],
            'receiver_country_code' => 'MY',
        ];

        $url = self::apiServer() . '/apiv1/create_shipment';
        $result = self::guzzlePost($url, $body);

        return $result->data;
    }

    // public static function createShipmentCheckoutCN($id)
    // {
    //     $itemTables = Item::find($id);

    //    $client = new Client();
    //    $form_args = [

    //     'form_params' => [
    //         'api_key' => self::apiKey(),
    //         'send_method' => 'pickup',
    //         'send_date' => Carbon::now()->toDateString(),
    //         'type' => 'parcel',
    //         'declared_weight' => '0.5', // 1KG
    //         'size' => 'box',
    //         'width' => '10',
    //         'length' => '10',
    //         'height' => '10',
    //         'provider_code' => 'poslaju',
    //         'content_type' => 'general',
    //         'content_description' => $itemTables->product->parentProduct ? $itemTables->product->parentProduct->name : 'Product',
    //         'content_value' => ($itemTables->subtotal_price / 100), // RM 00.00
    //         'sender_name' => 'FORMULA HEALTHCARE (' .$itemTables->delivery_order. ')',
    //         'sender_phone' => '03-5975 8613',
    //         'sender_company_name' => $itemTables->delivery_order,
    //         'sender_email' => 'formula2u@gmail.com',
    //         'sender_address_line_1' => 'No 26, Jalan Sierra 10/2',
    //         'sender_address_line_2' => 'Sierra Zentro I,',
    //         'sender_address_line_3' => 'Bandar 16 Sierra,',
    //         'sender_address_line_4' => 'Puchong, Selangor',
    //         'sender_postcode' => '47110',
    //         'receiver_name' => $itemTables->order->purchase->ship_full_name,
    //         'receiver_phone' => $itemTables->order->purchase->ship_contact_num,
    //         'receiver_email' => $itemTables->order->purchase->user->email,
    //         'receiver_address_line_1' => $itemTables->order->purchase->ship_address_1,
    //         'receiver_address_line_2' => $itemTables->order->purchase->ship_address_2 ?: '',
    //         'receiver_address_line_3' => $itemTables->order->purchase->ship_address_3 ?: '',
    //         'receiver_address_line_4' => '',
    //         'receiver_postcode' => $itemTables->order->purchase->ship_postcode,
    //         'receiver_country_code' => 'MY',
    //     ]
    //     ];

    //     $response = $client->post(self::apiServer() . '/apiv1/create_shipment',    $form_args);
    //     $responseStatus = $response->getStatusCode();
    //     $request = json_decode($response->getBody(), true);

    //     if ($responseStatus == 200 && $request['status'] == 'true') {

    //         $itemTables->shipment_key = $request['data']['key'];

    //         $checkout = self::checkout($itemTables->shipment_key);

    //         $itemTables->tracking_number = $checkout[0]->tracking_no;

    //         $cnote = self::get_consignment_note([$itemTables->tracking_number]);

    //         $itemTables->save();
    //         $message = $request['message'];

    //     } else {
    //         $message = $request['message'];
    //     }

    //     return response()->json( $message );
    // }

    public static function posLajuCheckout($id, $key)
    {
        $checkout = self::checkout($key);

        $dt = Carbon::now();
        $time = $dt->toTimeString(); //14:15:16

        if ($time > '11:45:00') {
            $dt->addDays(1);
            $actual_ship_date = $dt->format("Y-m-d");
        } else {
            $actual_ship_date = $checkout[0]->declared_send_at;
        }

        if (count($checkout) > 0) {
            $item = Item::find($id);

            $item->tracking_number = $checkout[0]->tracking_no;
            $item->actual_ship_date = $actual_ship_date;
            $item->save();
        }
    }

    public static function checkout($key)
    {
        $body = [
            'api_key' => self::apiKey(),
            'shipment_keys' => $key
        ];

        $url = self::apiServer() . '/apiv1/checkout';
        $result = self::guzzlePost($url, $body);

        return $result->data->shipments;
    }

    public static function get_consignment_note($tracking_no)
    {
        $body = [
            'api_key' => self::apiKey(),
            'tracking_no' => $tracking_no
        ];

        $url = self::apiServer() . '/apiv1/get_consignment_note';

        $result = self::guzzlePost($url, $body, 'pdf');

        return $result;
    }

    public static function generate_consignment_note($data)
    {
        $shipment = self::create_shipment($data);

        if ($shipment) {
            $checkout = self::checkout($shipment->key);

            if ($checkout && count($checkout) > 0) {
                foreach ($checkout as $val) {
                    $tracking_no[] = $val->tracking_no;
                }

                $cnote = self::get_consignment_note($tracking_no);

                $result = ['consignment_note' => $cnote, 'shipment' => $shipment, 'checkout' => $checkout];

                return $result;
            }
        }
    }

    public static function get_all_cart_items()
    {
        $body = [
            'api_key' => self::apiKey()
        ];

        $url = self::apiServer() . '/apiv1/get_cart_items';
        $result = self::guzzlePost($url, $body);

        return $result;
    }

    public static function get_postcode_details(Request $request)
    {
        $body = [
            'api_key' => self::apiKey(),
            'postcode' => $request->postcode
        ];

        $url = self::apiServer() . '/apiv1/get_postcode_details';
        $result = self::guzzlePost($url, $body);
        // dd($result->data);
        return response()->json($result);

        return $result->data;
    }

    public static function get_shipment_statuses()
    {
        $body = [
            'api_key' => self::apiKey()
        ];

        $url = self::apiServer() . '/apiv1/get_shipment_statuses';
        $result = self::guzzlePost($url, $body);

        return $result;
    }
}
