@inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')
@php
use setasign\Fpdi\Fpdi;

    // if ($request->files){
    // $fname =array();
    //         // initiate FPDI
    //     $pdf = new FPDI('l');
    //     $pageCount = 0;

    //     // iterate over array of files and merge
    //     foreach ($request->files as $file) {
    //         //if no file, dont combine
    //         if (!empty($file)) {
    //             $pageCount = $pdf->setSourceFile($file);
    //             for ($i = 0; $i < $pageCount; $i++) {
    //                 $tpl = $pdf->importPage($i + 1, '/MediaBox');
    //                 $pdf->addPage();
    //                 $pdf->useTemplate($tpl);
    //             }
    //         }
    //     }

        // if (!empty($files)) {
            // $filename = 'combined' . implode(",", $fname) . '.pdf';

            // $pdf->Output($filename, 'I');
        // }

        // {{-- <a id="showPdf" target="_blank"
        //     href="{{ URL::asset('storage/documents/pick/' . $request->batch_number . '/' . $request->batch_id . '.pdf') }}">
        // </a>

        // @push('script')
        // <script>
        //     $(function(){
        //         window.open($("#showPdf").attr("href"));
        //     });
        // </script> --}}
    // }
@endphp


<h1>{{$controller::$sortStartHeader}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        {{-- <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div>
        </div> --}}
        <div class="table-responsive m-2">

            <div class="col-12">
                <form action="{{route($controller::$sortStartWork)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12 col-md-4 text-md-right my-auto">
                        <p>
                            Select Bin<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="selected_bin" class="form-control">
                            <option value="" >Please Select...</option>
                            @foreach($pickBins as $pickBin)
                                <option value="{{ $pickBin->id }}" >{{ $pickBin->bin_number}} - {{$pickBin->sortStartBatch->batch_number}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-danger"
                            onclick="clickSubmit({{ $user->id }});">Start Sorting</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@endsection

@push('script')
<script>
function clickSubmit(userId) {
    if (confirm('Are you sure start sorting? Will auto create shipment if those orders are delivery by company.')) {
        document.getElementById("create-form-id").submit();
    }
}
</script>
@endpush
