<?php
namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Purchases\Item;
use Auth;
use App\Models\Users\Dealers\DealerInfo;
use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Dealers\DealerStockLedger;
use App\Models\Dealers\DealerData;

class AgentStockController extends Controller
{
    public static function recordSales($item_id){

         //get agent_id
         $dealerID = User::find(Auth::user()->id)->dealerInfo->account_id;

        // return $item_id;
        $item = Item::find($item_id);

        $stock = new DealerStockLedger;
        $stock->ref_id = $item->order->purchase->getFormattedNumber();
        $stock->type = $item->item_order_status == 1008 ? 5001 : 5003;
        $stock->product_code = self::replaceProductCode($item->product_code);
        $stock->item_id = $item->id;
        // $stock->quantity =  - $item->quantity;
        $stock->quantity =  - self::agentStockQuantity($item->product_code , $item->quantity);

        $stock->balance = ($stock->type == 5003) ?  self::agentStockBalance($item->product_code,$dealerID) + $stock->quantity : $stock->quantity;

        $stock->agent_id = $item->order->purchase->user->userInfo->referrer_id;
        $stock->date = Carbon::now();

        $stock->save();

    }
    public static function agentStockBalance($product_code,$dealerID){

        // find all ledger with the productcode and agent-id
        // $dealerLedger = DealerStockLedger::where('product_code',$product_code)->where('agent_id',$dealerID)->where('type','!=',5001);
        $dealerLedger = DealerStockLedger::where('product_code',$product_code)->where('agent_id',$dealerID);

        //sum quantity
        $sumQuantity = $dealerLedger->sum('quantity');

        return $sumQuantity ;
    }

    public static function agentStockQuantity($product_code , $getQuantity){

        if ((stripos($product_code,"0103-0006-K-2") == true) || (stripos($product_code,"0103-0006-Q-2") == true) || (stripos($product_code,"0103-0021-PK2") == true )) {
            $quantity = 2 * $getQuantity;

        } else {
            $quantity = $getQuantity;
        }

        return $quantity ;
    }

    public static function replaceProductCode($product_code){

            if ($product_code == "BU0920-0103-0006-K-2") {
                $new_product_code= str_replace("BU0920-0103-0006-K-2","BU0920-0103-0006-K",$product_code);
            }
            else if ($product_code == "BU0920-0103-0006-Q-2") {
                $new_product_code= str_replace("BU0920-0103-0006-Q-2","BU0920-0103-0006-Q",$product_code);
            }
            else if ($product_code == "BU1220-0103-0021-PK2") {
                $new_product_code= str_replace("BU1220-0103-0021-PK2","BU1220-0103-0020",$product_code);
            }
            else {
                $new_product_code= $product_code;
            }

            return $new_product_code;
    }

    public static function agentProductStock($product_code,$dealerID){

        // find all ledger with the productcode and agent-id
        $dealerData = DealerData::getAllAgentData($dealerID); //get data from tss-queen etc

        $records = array();

        $records['archimedes_details'] = DealerStockLedger::with('item')->where('agent_id',$dealerID)->get()->groupBy('product_code');

            // $records['date_and_id'] =  $dealerData->where('name','archimedes_project')->where('value','!=','0')->first();
            $records['date_and_id'] =  $dealerData ? $dealerData->first() : 0;

            foreach ($records['archimedes_details'] as $key => $value) {
                    $value['archimedes_value'] = $dealerData->where('name',$key)->first() ?? 0;

                    // $selectedDealerData = $dealerData->where('name',$key)->first();
                    // $value['archimedes_value'] = $selectedDealerData ? $selectedDealerData->stockLedger->where('type',5002)->sum('quantity') : 0;


            }
        return $records;

    }


}
