<?php

namespace App\Http\Controllers\Shop;

use Auth;
use Illuminate\Http\Request;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use App\Models\Categories\ProductType;
use App\Models\Categories\SubCategory;
use App\Models\Globals\Products\Product;
use App\Models\Globals\State;
use App\Models\Globals\City;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Users\User;
use App\Models\PromotionPage\Banner;
use App\Models\Products\Product as PanelProduct;
use App\Models\Users\Customers\Cart;
use App\Models\Testbanners\Testbanner;
use App\Models\Sidemenus\Sidemenu;
use App\Models\Faq\Faq;
use App\Models\Globals\Countries;
use App\Models\Globals\Image;
use App\Models\Globals\Status;
use App\Models\Products\ProductAttribute;
use Cookie;
use App\Models\Products\ProductBundle;
use App\Models\Purchases\Item;
use App\Models\ShippingInstallations\Ship_category;
use stdClass;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Rbs\RbsOptions;
use App\Models\Rbs\RbsPostpaid;
use Carbon\Carbon;

class ShopController extends Controller
{
    protected $cart = null;
    protected $categories = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // Check if user is authenticated or not.
            if (Auth::check()) {
                // If authenticated, then get their cart.
                $this->cart = Auth::user()->carts->where('status', 2001);
            }
            // Get all categories, with subcategories and its images.
            $categories = Category::orderBy('position', 'ASC')->topLevelCategory();

            // Share the above variable with all views in this controller.
            // View::share('categories', $categories);
            View::share('cart', $this->cart);

            // Return the request.
            return $next($request);
        });
    }

    // for bank test
    public function indexthemebank()
    {
        return view('shop.product_pagebank');
    }

    // coming soon
    public function comingsoon()
    {
        return view('shop.coming_soon');
    }

    // cart page
    public function cartpage()
    {
        return view('shop.cart_page');
    }

    //payment page
    public function paymentpage()
    {
        return view('shop.payment_page');
    }

    // new theme product page
    public function indextheme()
    {
        $pageName = 'product_page';
        return view('shop.product_page')
            ->with('pageName', $pageName);
    }

    // new theme product multineed
    public function productmultineed()
    {
        return view('shop.product_multineed');
    }
    /**
     * Handles /shop
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);
        switch ($user) {
            case $user->hasRole('wh_warehouse');
                return redirect()->route('administrator.v1.products.warehouse.newBatch');
            case $user->hasRole('wh_picker');
                return redirect()->route('administrator.pickBatch.start');
            case $user->hasRole('wh_sorter');
                return redirect()->route('administrator.pickBatch.sortStart');
            case $user->hasRole('wh_packer');
                return redirect()->route('administrator.pickBatch.pack-start');
            case $user->hasRole('wh_transferrer');
                return redirect()->route('warehouse.StockTransfer.index');
            case $user->hasRole('wh_receiver');
                return redirect()->route('warehouse.StockTransfer.receive');
            case $user->hasRole('wh_b_unbundle');
                return redirect()->route('warehouse.b_unbundle.index');
            case $user->hasRole('admin_orderTracking');
                return redirect()->route('administrator.ordertracking.orders');
            case $user->hasRole('administrator');
                break;
            case $user->hasRole('account_manager');
                return redirect()->route('csv.main.list');
        }

        // dd('asda');
        $banner_county = 'MY';
        $country = country()->country_id;

        if ($country == 'SG') {
            $banner_county = 'SG';
        }

        $testbanners = Testbanner::orderBy('created_at', 'desc')->get();
        $data = Banner::all();
        $categories = Category::where('type', 'shop')->where('active', 1)->where('parent_category_id', 0)->whereNotIn('slug', ['registrations', 'shop', 'agent-registration'])
            ->with(['products' => function ($q) {
                $q->validPrice();
            }])
            ->get()->sortBy('position');
        $cats = array();
        foreach ($categories as $category) {
            $cats[$category->slug] = $category;
        }
        $desktop_seq = [
            1 => 'oral-care', 2 => 'brain-care', 3 => 'cosmetic',
            4 => 'body-wash', 5 => 'mouth-and-throat-care', 6 => 'weight-management',
            7 => 'hair', 8 => 'blood-sugar', 9 => 'relaxing-sleeping',
            10 =>  'infant-care', 11 => 'blood-pressure', 12 => 'anti-oxidant-anti-inflammation',
            13 => 'children-care', 14 => 'cholesterol', 15 => 'muscle-care',
            16 =>  'stomach-and-guts-care', 17 => 'women-care', 18 => 'kidney-care',
            19 =>  'skin-care', 20 => 'men-care', 21 => 'liver-care',
            22 =>  'vision-care', 23 => 'intimate-wash', 24 => 'bone-and-joint-care',
            25 =>  'immunity-care', 26 => 'body-protection', 27 => 'lung-and-respiratory-care',
            28 =>  'energy-multi-vitamins-and-nutrition', 29 => 'facial', 30 => 'heart-care'
        ];

        $mobile_seq = [
            1 => 'energy-multi-vitamins-and-nutrition', 2 => 'immunity-care', 3 => 'vision-care',
            4 => 'skin-care', 5 => 'stomach-and-guts-care', 6 => 'children-care',
            7 => 'infant-care', 8 => 'hair', 9 => 'body-wash',
            10 =>  'oral-care', 11 => 'facial', 12 => 'body-protection',
            13 => 'intimate-wash', 14 => 'men-care', 15 => 'women-care',
            16 =>  'cholesterol', 17 => 'blood-pressure', 18 => 'blood-sugar',
            19 =>  'mouth-and-throat-care', 20 => 'brain-care', 21 => 'heart-care',
            22 =>  'lung-and-respiratory-care', 23 => 'bone-and-joint-care', 24 => 'liver-care',
            25 =>  'kidney-care', 26 => 'muscle-care', 27 => 'anti-oxidant-anti-inflammation',
            28 =>  'relaxing-sleeping', 29 => 'weight-management', 30 => 'cosmetic'
        ];

        $popularCategories = $cats;
        $menusides = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $menudata = Sidemenu::all();

        $menus = Sidemenu::where('menu_parent', '=', 0)->get();
        $menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $allMenus = Sidemenu::pluck('menu_title', 'id')->all();
        $country = country()->country_id;

        $products = Product::where('key_selection', 1)->where('product_status', 1)->validPrice()->limit(88)->get();
        // $products = PanelProduct::limit(10)->get();

        // $categories = Category::where('type','shop')->get();

        return view('shop.index')
            ->with('menudata', $menudata)
            ->with('products', $products)
            ->with('data', $data)
            ->with('categories', $categories)
            ->with('popularCategories', $popularCategories)
            ->with('desktop_seq', $desktop_seq)
            ->with('mobile_seq', $mobile_seq)
            ->with('menusides', $menusides)
            ->with('allMenus', $allMenus)
            ->with('country', $country)
            ->with('menus', $menus)
            ->with('banner_county', $banner_county)
            ->with('testbanners', $testbanners);
    }

    /****
     * Show About Us
     */
    public function aboutUs()
    {
        return view('shop.footer-pages.about-us');
    }

    /****
     * Show Return Form
     */
    public function returnForm()
    {
        return view('shop.footer-pages.return-form');
    }

    /****
     * Show Return and Refund Policy Page
     */
    public function returnRefundPolicy()
    {
        return view('shop.footer-pages.return-and-refund-policy');
    }

    /****
     * Show Shipping and Delivery Policy Page
     */
    public function shippingDeliveryPolicy()
    {
        return view('shop.footer-pages.shipping-and-delivery-policy');
    }

    /****
     * Show Membership Terms and Condition Page
     */
    public function membershipTermsNCondition()
    {
        return view('shop.footer-pages.membership-terms-and-condition');
    }

    /**Show Workforce Page***/
    public function workforce()
    {
        return view('shop.footer-pages.workforce');
    }

    /*Show Bujishu Service Page**/
    public function bujishuService()
    {
        return view('shop.footer-pages.bujishu-service');
    }

    /*Show FAQ Page**/
    public function faq()
    {
        //return view('shop.footer-pages.faq');
        //return view('shop.footer-pages.faq2',compact('faqs'));

        $faqs = Faq::orderBy('id', 'DESC')->where('faq_status', 1)->get();

        // $statuses = Status::whereIn('id', [10, 11, 12, 13, 14])->get();

        $statuses = Status::where('status_type', 'faq_type')->get();

        return view('shop.footer-pages.faq3', compact(['faqs', 'statuses']));
    }

    /***Return privacy policy**/

    public function privacyPolicy()
    {
        return view('shop.footer-pages.privacy-policy');
    }

    /***Our Vision,Culture and Value***/
    public function visionCultureValue()
    {
        return view('shop.footer-pages.vision-culture-value');
    }

    /*****Work in Progress***/
    public function workInProgress()
    {
        return view('shop.work-in-progress');
    }

    public function findQuality($categorySlug)
    {

        $products = new Product;

        $products = $products->whereHas('categories', function ($query) use ($categorySlug) {
            $query->where('slug', $categorySlug);
        });

        $qualities = $products->orderBY('quality_id', 'desc')->pluck('quality_id')->toArray();
        $qualities = array_unique($qualities);
        if (count($qualities) < 1) $qualityVal = ['Domestic'];

        foreach ($qualities as $key => $quality) {
            switch ($quality) {
                case '1':
                    $qualityVal[] = 'Domestic';
                    break;
                case '2':
                    $qualityVal[] = 'Import';
                    break;
                case '3':
                    $qualityVal[] = 'Exclusive';
                    break;
                default:
                    $qualityVal[] = 'Exclusive';
                    break;
            }
        }
        return $qualityVal;
    }

    /**
     * Handles /shop/product/{product-slug}
     */
    // public function product(Request $request, $slug)
    // {
    //     $panelId = $request->query('panel');

    //     $product = Product::where('name_slug', $slug)
    //         ->with('images')
    //         ->firstOrFail();

    //     $panelProduct = PanelProduct::where('global_product_id', $product->id)
    //         ->where('panel_account_id', $panelId)
    //         ->firstOrFail();

    //     $category = $product->categories->first();

    //     $relatedProducts = $category->products->where('name_slug', '!=', $slug)->where('product_status', 1)->take(6);

    //     $user = User::find(Auth::user()->id);

    //     $customer = $user->userInfo;

    //     $states = State::all();


    //     return view('shop.product')
    //         ->with('product', $product)
    //         ->with('panelProduct', $panelProduct)
    //         ->with('relatedProducts', $relatedProducts)
    //         ->with('customer', $customer)
    //         ->with('states', $states);
    //     // ->with('products', $products);
    // }

    /**
     * Not used in cart page
     * Handles editing address on shop page.
     */
    public function productChangeAddress(Request $request)
    {
        // return State::find($request->input('state_id'));
        $user = User::find(Auth::user()->id);
        $userAddress = $user->userInfo->shippingAddress;
        $state = State::all();

        $shipCookie = json_decode(Cookie::get('addressCookie'));

        $new_shipCookie = json_encode(array(
            'address_1' => $request->input('address_1'),
            'address_2' => $request->input('address_2'),
            'address_3' => $request->input('address_3'),
            'postcode' => $request->input('postcode'),
            'city' => $request->input('city'),
            'city_key' => $request->input('city_key'),
            'state_name' => State::find($request->input('state_id')),
            'state_id' => $request->input('state_id'),
            'name' => $request->input('name'),
            'mobile' => $request->input('mobile')
        ));

        // $userAddress->address_1 = $request->input('address_1');
        // $userAddress->address_2 = $request->input('address_2');
        // $userAddress->address_3 = $request->input('address_3');
        // $userAddress->postcode = $request->input('postcode');
        // $userAddress->city = $request->input('city');
        // $userAddress->state_id = $request->input('state_id');
        // $userAddress->save();

        $cartItems = $user->carts->where('status', 2001);

        foreach ($cartItems as $cartItem) {
            $product = $cartItem->product;
            if ($product->deliveries->count() > 0) {
                $deliveryFee = $product->deliveries->where('state_id', $userAddress->state_id)->first();

                if ($deliveryFee) {
                    $cartItem->disabled = 0;
                } else {
                    $cartItem->delivery_fee = 0;
                    $cartItem->disabled = 2;
                }
            } else {
                $cartItem->delivery_fee = 0;
                $cartItem->disabled = 2;
            }

            $cartItem->save();
        }

        return redirect()->back()->withCookie(Cookie::make('addressCookie', $new_shipCookie, 3600))
            // ->withCookie(Cookie::make('name', $request->input('name'), 3600))
            // ->withCookie(Cookie::make('mobile', $request->input('mobile'), 3600))
        ;
    }

    /**
     * Handles /shop/shopping-cart
     * An empty view will be returned first. The view will then submit an AJAX request.
     */
    public function shoppingCart(Request $request)
    {
        // If ajax request..
        if ($request->ajax()) {
            // Find user
            $user = User::find(Auth::user()->user_id);
            // Get items in user's cart.
            $cartItems = $user->cartItems->where('status', 2001);
            // Return a partial view (can be treated as a component), the view will loop through all the items.
            return view('shop.cart.partials.shopping-cart-item')->with('cartItems', $cartItems);
        }

        // Return view.
        // After finished loading, the view will submit an AJAX request that will be handled by the statement above.
        return view('shop.shopping-cart');
    }

    /**
     * Handles /shop/category/[category-name]
     */

    public function topLevelCategory(Request $request, $topLevelSlug)
    {
        // Get users
        $user = Auth::check() ? User::find(Auth::user()->id) : null;

        $category = Category::where('slug', $topLevelSlug)->where('type', 'shop')->where('active', 1)->first();

        if (!$category) {
            abort(404);
        }

        $userRoles = $user ? $user->roles->pluck('name')->all() : [];

        if (!empty($category->roles)) {
            if (!$user || empty(array_intersect($userRoles, $category->roles))) {
                abort(401);
            }
        }

        if ($category->public_view == 0 && !Auth::check()) {
            return redirect('/')->with('error', 'This page is only allowed for logged-in users. Please log in to view.');
        }

        $qualities = $request->query('quality') ?: $this->findQuality($category->slug);
        foreach ($qualities as $key => $quality) {
            $products[] = self::getProducts($category->slug, $quality)->toArray();
        }
        $products = array_merge(...$products);
        foreach ($products as $key => $product) {
            $products[$key] = (object) $product;
        }
        // $originlocal = Category::where('type', 'origin_country')
        //     ->whereIn('name', ['Malaysia', 'MAL'])->get('id');


        // if (count($originlocal)) {
        //     foreach ($originlocal as $k => $v) {
        //         $local[] = $v['id'];
        //     }
        // }

        $categoryProducts = $category->products()->validPrice()->get();

        $hotSelections = $categoryProducts->where('hot_selection', 1)->where('product_status', 1)->take(16);
        $recommendationProducts = $categoryProducts->where('hot_selection', 0)->where('product_status', 1);
        $originDomestic = $originImport = $originExclusive = array();

        foreach ($products as $key => $product) {
            switch ($product->quality_id) {
                case '1':
                    $originDomestic[] = $product;
                    break;

                case '2':
                    $originImport[] = $product;
                    break;

                case '3':
                    $originExclusive[] = $product;
                    break;

                default:
                    $originExclusive[] = $product;
                    break;
            }
        }
        // foreach ($category->products->where('product_status', 1) as $product) {
        //     $originCountry = json_decode($product->panelProduct->place_of_origin);

        //     if (in_array($originCountry[0], $local)) {
        //         $originDomestic[] = $product->panelProduct;
        //     } else {
        //         $originImport[] = $product->panelProduct;
        //     }
        // }

        /* $originDomestic = collect($allProduct)->whereIn('origin',$local);
        $originImport = collect($allProduct)->whereNotIn('origin',$local); */

        // local
        $domestic = [];
        if (count($originDomestic)) {
            foreach ($originDomestic as $val) {
                $gp[] = $val->id;
            }

            if (count($gp) > 0) {
                $domestic = $category->products->whereIn('id', $gp);
            }
        }

        // import
        $import = [];
        if (count($originImport)) {
            foreach ($originImport as $val) {
                $gpi[] = $val->id;
            }

            if (count($gpi) > 0) {
                $import = $category->products->whereIn('id', $gpi);
            }
        }

        // exclusive
        $exclusive = [];
        if (count($originExclusive)) {
            foreach ($originExclusive as $val) {
                $gpe[] = $val->id;
            }

            if (count($gpe) > 0) {
                $exclusive = $category->products->whereIn('id', $gpe);
            }
        }

        $show = 3;

        if (count($exclusive) > 0) {
            $show = 3;
        } elseif (count($import) > 0) {
            $show = 2;
        } elseif (count($domestic) > 0) {
            $show = 1;
        } else {
            //
        }

        // dd($domestic);
        // Check if the exact item is already in the cart..
        // $getCartQuantity = new Cart;
        // dd( $this->findQuality($request));

        // $getCartQuantity = $getCartQuantity->where('user_id', $user->id)->where('status', 2001)->sum('quantity');

        // $category = Category::with(['childCategories'])
        //     ->topFilter($topLevelSlug)
        //     //->qualityFilter($request->query('quality'))
        //     ->take(6)
        //     ->get();

        // $ctgrSub = Category::with(['childCategories'])
        //     ->topFilter($topLevelSlug)
        //     ->get();

        // if(empty($products)){
        //     $quality = 'premium' ;
        // } else {
        //     $quality = 'moderate' ;
        // }

        // $mainCtgr = $category->first();

        // $article = $mainCtgr->getArticles($mainCtgr->id);
        /* $articlec = [];
        $articles = [];  */

        // if (isset($article)) {
        //     if ($article->type == 'child_cat') {
        //         $articlec = $mainCtgr->getChildArticle($article->cat_id);
        //     }
        // } else {
        //     $article = new stdClass;
        //     $article->type = 'null';
        //     $article->title = '';
        //     $article->data = '';
        // }

        // $mainCtgrSubtit = $ctgrSub->first()->childCategories;
        // $quality = $request->query('quality') ?: $this->findQuality($mainCtgr->slug);
        // $products = self::getProducts($mainCtgr->slug, $quality);
        // $categoryLevel = 1;
        // $qltName =  $quality;
        // $parentCategory = $topLevelSlug;
        // $secondLvlName = $mainCtgr->slug;



        // dd($mainCtgrSubtit);

        // dd($allCategories,$mainCtgr);

        return view('shop.catalog.catalog')
            ->with('hotSelections', $hotSelections)
            ->with('domestic', $domestic)
            ->with('import', $import)
            ->with('exclusive', $exclusive)
            ->with('show', $show)
            // /* ->with('articlec', $articlec)
            // ->with('articles', $articles) */
            ->with('recommendationProducts', $recommendationProducts)
            // ->with('mainCtgr', $mainCtgr)
            // ->with('request', $request)
            // // ->with('childCategories', $childCategories)
            // ->with('categoryLevel', $categoryLevel)
            // ->with('qltName', $qltName)
            // // ->with('getCartQuantity', $getCartQuantity)
            // ->with('products', $products)
            ->with('topLevelSlug', $topLevelSlug)
            // ->with('secondLvlName', $secondLvlName)
            // ->with('mainCtgrSubtit', $mainCtgrSubtit)
        ;
    }

    // public function secondLevelCategory(Request $request, $topLevelSlug, $secondLevelCategory)
    // {

    //     // Get user
    //     $user = User::find(Auth::user()->id);

    //     $quality = $request->query('quality') ?  $request->query('quality')  : 'premium';

    //     $category = Category::with(['childCategories'])
    //         ->topFilter($secondLevelCategory)
    //         ->take(6)
    //         ->get();

    //     $ctgrSub = Category::with(['childCategories'])
    //         ->topFilter($topLevelSlug)
    //         ->get();

    //     $mainCtgr = $category->first();
    //     $mainCtgrSubtit = $ctgrSub->first() ? $ctgrSub->first()->childCategories : null;

    //     $quality = $request->query('quality') ?: $this->findQuality($mainCtgr->slug);
    //     $products = self::getProducts($mainCtgr->slug, $quality);
    //     $categoryLevel = 2;
    //     $qltName =  $quality;
    //     $secondlvlslug = $category->first()->parentCategory->slug;
    //     $parentCategory = $topLevelSlug;
    //     $secondLvlName = $mainCtgr->parentCategory->slug;

    //     // $productsTwo = self::getProducts($mainCtgr->slug);

    //     // dd($products);



    //     return view('shop.catalog.catalog')
    //         ->with('category', $category)
    //         ->with('mainCtgr', $mainCtgr)
    //         ->with('request', $request)
    //         ->with('categoryLevel', $categoryLevel)
    //         ->with('qltName', $qltName)
    //         ->with('products', $products)
    //         ->with('secondlvlslug', $secondlvlslug)
    //         ->with('parentCategory', $parentCategory)
    //         ->with('secondLvlName', $secondLvlName)
    //         ->with('mainCtgrSubtit', $mainCtgrSubtit);
    // }

    // public function thirdLevelCategory(Request $request, $topLevelSlug, $secondLevelCategory, $thirdlevel)
    // {
    //     // dd($request);
    //     // Get user
    //     $user = User::find(Auth::user()->id);

    //     $quality = $request->query('quality') ?  $request->query('quality')  : 'premium';

    //     $category = Category::with(['childCategories'])
    //         ->topFilter($thirdlevel)
    //         ->take(6)
    //         ->get();


    //     $ctgrSub = Category::with(['childCategories'])
    //         ->topFilter($topLevelSlug)
    //         ->get();

    //     $mainCtgr = $category->first();
    //     $quality = $request->query('quality') ?: $this->findQuality($mainCtgr->slug);
    //     $products = self::getProducts($mainCtgr->slug, $quality);
    //     $mainCtgrSubtit = $ctgrSub->first()->childCategories;
    //     $categoryLevel = 3;
    //     $qltName =  $quality;
    //     $secondlvlslug = $category->first()->parentCategory->slug;
    //     $parentCategory = $topLevelSlug;
    //     $secondLvlName = $mainCtgr->parentCategory->slug;

    //     // $productsdd = new Product;

    //     // $products = $products->whereHas('categories', function ($query) use ($categorySlug) {
    //     //     $query->where('slug', $categorySlug);
    //     // });

    //     // dd($products);



    //     return view('shop.catalog.catalog')
    //         ->with('category', $category)
    //         ->with('mainCtgr', $mainCtgr)
    //         ->with('request', $request)
    //         ->with('categoryLevel', $categoryLevel)
    //         ->with('qltName', $qltName)
    //         ->with('products', $products)
    //         ->with('secondlvlslug', $secondlvlslug)
    //         ->with('parentCategory', $parentCategory)
    //         ->with('secondLvlName', $secondLvlName)
    //         ->with('mainCtgrSubtit', $mainCtgrSubtit)
    //         ->with('ctgrSub', $ctgrSub);
    // }

    public static function getProducts($categorySlug, $quality)
    {

        // $allProduct =
        $products = new Product;

        $products = $products->whereHas('categories', function ($query) use ($categorySlug) {
            $query->where('slug', $categorySlug);
            $query->where('type', 'shop');
        });

        $products->whereHas('quality', function ($query) use ($quality) {
            $query->where('name', $quality);
        });
        $products = $products->has('productSoldByPanels', '>', 0);

        $products = $products->validPrice();

        $products = $products->where('product_status', 1)->get();
        // dd($products);
        return  $products;

        // dd($product);
    }

    public function productPage(Request $request, $category, $productSlug)
    {
        // dd($category);
        $user = Auth::check() ? User::find(Auth::user()->id) : null;
        $userLevel = $user ? $user->userInfo->user_level : null;
        $tableCategories = Category::where('slug', $category)->where('type', 'shop')->first();

        if (!$tableCategories) {
            abort(404);
        }

        $userRoles = $user ? $user->roles->pluck('name')->all() : [];

        if (!empty($tableCategories->roles)) {
            if (!$user || empty(array_intersect($userRoles, $tableCategories->roles))) {
                abort(401);
            }
        }

        if ($tableCategories->public_view == 0 && !Auth::check()) {
            return redirect('/')->with('error', 'This page is only allowed for logged-in users. Please log in to view.');
        }

        $products = $tableCategories->products->where('name_slug', $productSlug)->where('product_status', 1)->first();

        if (is_null($products)) {
            abort(404);
        }

        $suitableFor = Category::where('slug', $category)->first();
        $images = (isset($products)) ? $products->images->sortByDesc('default') : array();
        $pageName = 'product_page';

        $cookie = json_decode(Cookie::get('addressCookie'));
        // dd($cookie);
        $states = State::all();
        $state_id = isset($cookie) ? $cookie->state_id : ($user ? $user->userInfo->shippingAddress->state_id : null);
        $cities = $state_id ? City::where('state_id', $state_id)->get() : collect();

        $getCats = DB::table('piv_category_product')
            ->where('category_id', '=', $tableCategories->id)
            ->get();

        $rbsReminders = RbsOptions::get();

        if (count($getCats) > 0) {
            foreach ($getCats as $k => $v) {
                $pk[] = $v->product_id;
            }

            $widgetBlock = Product::whereIn('id', $pk)->where('product_status', 1)->get();
        } else {
            $widgetBlock = isset($products) ? $products->getSameBrand($products->brand_id) : collect();
        }

        return view('shop.product')
            ->with('userLevel', $userLevel)
            ->with('states', $states)
            ->with('cookie', $cookie)
            ->with('cities', $cities)
            ->with('products', $products)
            ->with('widgetBlock', $widgetBlock)
            ->with('images', $images)
            ->with('rbsReminders', $rbsReminders)
            ->with('pageName', $pageName)
            ->with('category', $category);
    }

    public function delhubdigital()
    {
        return view('shop.delhub-digital');
    }

    //customer shop page search bar function
    public function search(Request $request)
    {
        $products = 0;
        $categories  = 0;
        $result = array();
        // $slug = $request->query('query');
        $slug = $request->input('query');

        $country = country()->country_id;
        $excludeCountry = ($country == 'MY') ? 'my' : 'sg';

        if ($slug != NULL) {
            //By category
            $categories = Category::where('categories.slug', 'like', '%' . $slug . '%')
                ->where('categories.active', 1)
                ->where('categories.type', 'shop')
                ->when(!Auth::check(), function ($query) {
                    // For not logged-in users
                    $query->where('categories.public_view', 1)
                    ->whereNull('categories.roles');
                })
                ->when(Auth::check(), function ($query) {
                    // For logged-in users
                    $userRoles = Auth::user()->roles->pluck('name')->all();
                    $query->where(function ($query) use ($userRoles) {
                        $query->whereNull('categories.roles');
                        foreach ($userRoles as $role) {
                            $query->orWhereJsonContains('categories.roles', $role);
                        }
                    });
                })
                ->whereDoesntHave('categoryData', function (Builder $queryy) use ($excludeCountry) {
                    $queryy->where('categories_data.country', 'like', $excludeCountry);
                })
                ->get();

            if (isset($categories) && $categories->isNotEmpty() && !empty($categories) && $categories->count() > 0) {
                foreach ($categories as $category) {
                    $result[] = array(
                        'name' => $category->name,
                        'url' => $category->getUrlAttribute(),
                        'image' => isset($category->image) ? asset('/' . $category->image->path . $category->image->filename) : '',
                    );
                }
            }

            // $result[] = array(
            //     'name' => '',
            //     'url' => '',
            //     'image' => '',
            //     '_category' => 'By Cateogry',
            //     'separator' => 'CategoryYeah',
            //     'category' => 'By Product top',
            // );

            //By global Product
            $products = Product::where('product_status', 1)
                ->whereDoesntHave('categories', function (Builder $query) use ($excludeCountry) {
                    $query->where('categories.slug', 'like', $excludeCountry)
                        ->orWhereHas('categoryData', function (Builder $subQuery) use ($excludeCountry) {
                            $subQuery->where('categories_data.country', 'like', $excludeCountry);
                        });
                })
                ->where('global_products.name', 'like', '%' . $slug . '%')
                ->when(Auth::check(), function ($query) {
                    // If the user is logged in
                    $userRoles = Auth::user()->roles->pluck('name')->all();
                    $query->whereHas('categories', function (Builder $categoryQuery) use ($userRoles) {
                        $categoryQuery->where('categories.type', 'shop')
                            ->where(function ($query) use ($userRoles) {
                                $query->whereNull('categories.roles');
                                foreach ($userRoles as $role) {
                                    $query->orWhereJsonContains('categories.roles', $role);
                                }
                        });
                    });
                })
                ->when(!Auth::check(), function ($query) {
                    // If the user is not logged in
                    $query->whereHas('categories', function (Builder $categoryQuery) {
                        $categoryQuery->where('categories.type', 'shop')
                            ->whereNull('categories.roles');
                    });
                })
                ->get();

            if (isset($products) && $products->isNotEmpty() && !empty($products) && $products->count() > 0) {
                foreach ($products as $product) {
                    if (isset($product->productSoldByPanels[0]->panel_account_id) != NULL) {
                        $result[] = array(
                            'name' => $product->name,
                            'url' => ($product->name_slug != NULL) ? $product->getUrlAttribute() : '',
                            'image' => isset($product->images[0]) ? asset('storage/' . $product->images[0]->path . '/' . $product->images[0]->filename) : '',
                        );
                    }
                }
            }
        }

        return response()->json($result);
    }

    public function addRbsPostpaid(Request $request)
    {
        // return $request;
        // unserialize form for cookies
        foreach ($request->address as $keys => $values) {
            $formInput[$keys] = $values;
        }
        // return $formInput;
        $user = User::find(Auth::user()->id);

        $data = RbsPostpaid::where('user_id', $user->id)
            ->where('attribute_id', $request->product_attributes)
            ->where('active', 1)
            ->where('start_reminder', date("Y-m-d"))
            ->where('months_id', $request->monthReminder)
            ->where('shipping_address', json_encode($formInput))
            ->first();

        if (!isset($data)) {
            $data = new RbsPostpaid;
            $quantity = 0;
        } else {
            $quantity = $data->rbs_quantity;
        }

        $data->user_id = $user->id;
        $data->attribute_id = $request->product_attributes;
        $data->rbs_quantity = $quantity + $request->productQuantity;
        $data->months_id = $request->monthReminder;
        $data->start_reminder = Carbon::now()->addMonth(1)->startOfMonth();
        $data->shipping_address = $formInput;
        $data->save();

        $shipCookie = json_decode(Cookie::get('addressCookie'));

        $cityRequest = City::where('city_key', $formInput['city'])->first();

        // return $formInput;

        $new_shipCookie = json_encode(array(
            'address_1' => $formInput['address1'],
            'address_2' => $formInput['address2'],
            'address_3' => $formInput['address3'],
            'postcode' => $formInput['postcode'],
            'city' => $cityRequest ? $cityRequest->city_name : '',
            'city_key' => $formInput['city'],
            'city_name' => $cityRequest,
            'state_name' => State::find($formInput['state']),
            'state_id' => $formInput['state'],
            'name' => $formInput['name'],
            'mobile' => $formInput['phone']
        ));


        return back()->with('success', 'add or updated!')->withCookie(Cookie::make('addressCookie', $new_shipCookie, 3600));
    }
}
