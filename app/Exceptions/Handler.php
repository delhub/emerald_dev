<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {

        if (!$this->shouldntReport($exception)) {

            Log::info(
                'URL: ' . Request::url() .
                    ' - USERID: ' . (Auth::user() ? Auth::user()->id : 0) .
                    ' - INPUT: ' . json_encode(Request::all())
            );
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return abort(403, 'You do not have the right permission to access this page.');
        } else if ($exception instanceof TokenMismatchException || $exception instanceof  AuthenticationException) {

            if (!request()->is('api*')) {
                return redirect('/login?pageExpired=true');
            }
        }
        Log::channel("authentication")->info($exception);
        return parent::render($request, $exception);
    }
}
