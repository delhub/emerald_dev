<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserMembership extends Model
{
     // Set table
     protected $table = 'user_membership';
         // Set timestamps
    public $timestamps = true;
}
