<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    @include('layouts.favicon')
    <title>File Upload</title>
    <style>
        .container {
            max-width: 500px;
        }
        dl, ol, ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }
    </style>
</head>

<body>
    @if(isset($data) && count($data) == 0)
    <div class="container mt-5">
        <form action="{{route('fileUpload')}}" method="post" enctype="multipart/form-data">
          <h3 class="text-center mb-5">Upload File</h3>
            @csrf
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
            <div class="form-group">
                <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="chooseFile" onchange="getFileData(this)">
                    <label class="custom-file-label" for="chooseFile"><span id="fn">Select file</span></label>
                </div>
            </div>

            <div class="form-group">
            <div class="col-12" id="withHeading" style="display: none">
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="withHead" id="withHead" type="checkbox" class="custom-control-input" aria-describedby="withHeadHelpBlock">
                  <label for="withHead" class="custom-control-label">WithHeading</label>
                </div>
              </div>
            </div>


            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                Upload Files
            </button>
        </form>
    </div>
    @endif


    @if(isset($data) && count($data) > 0)
    <div class="mt-5">
    <form action="{{route('fileUpload')}}" class="form-check" method="post">
        <h3 class="text-center mb-5">Select Import Data</h3>
        @csrf
      <div class="row justify-content-md-center mt-5">
        <div class="col col-lg-2"></div>
        <div class="col-md-auto col-lg-auto">
            <table class="table table-sm table-hover">
                <thead>
                  <tr>
                    <th class="text-center"><input name='selectAll' type="checkbox"></th>
                    @for ($i = 0; $i < count($data[0]); $i++)
                        @if($req->withHeading == 'on')
                        <th scope="col">{{$data[0][$i]}}</th>
                        @else
                        <th scope="col">Field{{$i}}</th>
                        @endif
                    @endfor
                  </tr>
                </thead>
                <tbody>
                @if($req->withHeading == 'on')
                    @php
                        unset($data[0]);
                        $key = 1;
                    @endphp
                @else
                    @php
                        $key = 0;
                    @endphp
                @endif
                @foreach ($data as $k=>$d)
                  <tr>
                    <th class="text-center"><input name='data[]' type="checkbox" value="{{ $k }}"></th>
                    @for ($i = 0; $i < count($data[$key]); $i++)
                    <td>{{ $d[$i] }}</td>
                    @endfor
                  </tr>
                </tbody>
                @endforeach
              </table>
              <hr>
              <input type="hidden" name="import" value="1">              
              <div class="col-lg-4 mb-5 float-right">                
                <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                    Confirm...
                </button></div>
        </div>
        <div class="col col-lg-2"></div>
      </div>
    </form>
    </div>
    @endif

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

$(':checkbox[name=selectAll]').click (function () {
  $(':checkbox[type=checkbox]').prop('checked', this.checked);
});

function getFileData(myFile){
    var file = myFile.files[0];
    var filename = file.name;

    var ext = filename.split('.').pop();

    switch(ext) {
    case 'csv':
    case 'xlx':
    case 'xls':
    case 'xlsx':
    case 'ods':
        $('#withHeading').show();
      break;
    default:
        $('#withHeading').hide();
  }

    $('#fn').text(filename);
}
</script>

</body>
</html>
