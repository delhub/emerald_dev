<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Products\Product;

class UpdateGlobalProductsNameSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $globalProducts = Product::get();

        foreach ($globalProducts as $key => $product) {

            $product->name_slug = $product->id . '-' . $product->name_slug;

            $product->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
