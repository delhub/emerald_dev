<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse\Location\Wh_location;

class WarehouseBatchItem extends Model
{
    // Set table
    protected $table = 'wh_batch_item';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function batchDetail()
    {
        return $this->belongsTo('App\Models\Products\WarehouseBatchDetail', 'detail_id', 'id');
    }

    // Search Bar
    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {
            if (($selectSearch == 'location_id')) {
                return $query
                    ->join('wh_location', 'wh_location.id', '=', 'wh_batch_item.location_id')
                    ->where('wh_location.location_name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'item_batch_id') {

                return $query
                    ->where('items_id', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'product_code') {

                return $query
                    ->join('wh_batch_detail', 'wh_batch_detail.id', '=', 'wh_batch_item.detail_id')
                    ->where('wh_batch_detail.product_code', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'product_name') {

                $query
                    ->join('wh_batch_detail', 'wh_batch_detail.id', '=', 'wh_batch_item.detail_id')
                    ->join('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'wh_batch_detail.product_code')
                    ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
                    ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')

                    ->where('global_products.name', 'LIKE', '%' . $searchFilter . '%');


                return $query;
            }
        }

        return $query;
    }

    public function scopeLocationFilter($query, $location_id)
    {
        if ($location_id != 'all' && $location_id != NULL) {
            return $query->where('location_id', $location_id);
        } else {
            // return $query;
        }
    }

    public function location()
    {
        return $this->hasOne(Wh_location::class, 'id', 'location_id');
    }
}
