<div id="exampleModal" class="d-none col-12 p-1 m-0">
    <div class="card -radius-0 mb-2 p-2 -danger">
        <div class="row no-gutters py-2">
            <div class="col-12 px-1 text-center">
                <p class="m-2 h5">Oops, please select at least 1 item to checkout.</p>
            </div>

        </div>
    </div>
</div>
<!-- If there's no item in cart, show message -->
@if($cartItems->count() == 0)
    <div class="col-12 p-1 m-0">
        <div class="card -radius-0 mb-2 p-2">
            <div class="row no-gutters py-2">
                <div class="col-12 px-1 text-center">
                    <p class="m-2 h5">There's no item in your cart.</p>
                    <a href="/shop" class="btn bjsh-btn-gradient ">Continue Shopping</a>
                </div>

            </div>
        </div>
    </div>
@else
{{-- {{dd($zoneId)}} --}}
    <div id="cartDetails" class="cartDetails" name="cartDetails"
        data-subtotal="{{ $cartTotals['subtotal'] }}"
        data-shipping-total="{{ $cartTotals['fee']['shipping']['amount'] }}"
        data-installation-total="{{ $cartTotals['fee']['installation']['amount'] }}"
        data-rebate-total="{{$cartTotals['fee']['rebate']['amount']}}"
        data-total="{{ $cartTotals['total'] }}"

        data-tax-rate="{{ $cartTotals['tax_rate'] }}"
        data-tax-name="{{ $cartTotals['tax_name'] }}"

        {{-- data-zoneid="{{$zoneId}}" --}}
        data-shipping-total-old="{{ $cartTotals['shipping_new'] }}"
        data-total-old="{{ $cartTotals['total_old'] }}"
    ></div>
    @foreach ($cartItems as $panelid => $cartItem)
    {{-- {{$cartItem->id}} and {{$cartItem->product->shipping_category}} --}}
        @if ($cartItem->bundle_id == 0)
            <div class="card -radius-0 mb-4 px-4 py-2 bundleMain">
                {{-- {{$cartItem->id}} and {{$cartItem->disabled}} --}}
                <div class="row no-gutters">
                    <div class="col-12 mb-1 px-1 my-2">
                        @if ($cartItem->disabled != 0)
                                <span class="text-danger ml-2" style="font-size: .8rem;">
                                    {{$cartItem->getDisabledMessageAttribute()}}
                                </span>
                            @endif
                    </div>
                </div>
                {{-- <hr class="m-0" style="-top: 0.01em solid #ffcc00ad;"> --}}
                {{-- shipping = {{ $cartItem->product->shipping_category }},  install = {{ json_encode($cartItem->product->installation_category) }},  qty = {{ $cartItem->quantity }}
                @if(array_key_exists('product_installation', $cartItem->product_information))
                    <br>from ProductInformation. : {{ $cartItem->product_information['product_installation'] }}
                    <br>
                @endif --}}
                <div class="container my-2">
                    <div class="row d-md-block ">
                        <div class="col-4 col-md-2 float-left ">
                            <div class="row pt-md-2">
                                <div class="col-2 my-auto mx-0 pr-0">
                                    <input type="checkbox" name="cartItemId[]"
                                        class="custom-control-input item-checkbox {{ ($cartItem->disabled == 4) ? 'disabled' : '' }}"
                                        id="item-{{ $cartItem->id }}" value="{{ $cartItem->id }}"
                                        data-unit-price="{{ $cartItem->unit_price }}" data-quantity="{{ $cartItem->quantity }}"
                                        {{-- data-tax-rate="{{ $cartItem->tax_rate }}"
                                        data-tax-name="{{ $cartItem->tax_name }}"
                                        data-shipping-price="{{ $cartItem->delivery_fee }}"
                                        data-installation-price="{{ $cartItem->installation_fee }}"
                                        data-tradein-price="{{ $cartItem->rebate != 0 ? $cartItem->rebate : 0 }}" --}}
                                        data-panel-id="{{ $cartItem->product->panel_account_id }}"
                                        data-bundle-id="{{ $cartItem->bundle_id }}"
                                        {{ (($cartItem->selected == 1) && ($cartItem->disabled != 1) && ($cartItem->disabled != 2) && ($cartItem->disabled != 4)&& ($cartItem->disabled != 5)&& ($cartItem->disabled != 6)&& ($cartItem->disabled != 7)  && ($cartItem->product->parentProduct->product_status != 2) )  ? 'checked' : '' }}
                                        {{ ($cartItem->disabled != 0 && $cartItem->disabled != 3) || ($cartItem->product->parentProduct->product_status == 2) ? 'disabled' : '' }}>
                                    <label class="custom-control-label" for="item-{{ $cartItem->id }}"></label>
                                </div>
                                <div class="col-10 p-0">
                                    @if (isset($cartItem->product_information['product_color_img']))

                                    @php
                                        $value = $cartItem->product->parentProduct->images->where('id',$cartItem->product_information['product_color_img'])->first();
                                        if (! $value ) $value =  $cartItem->product->parentProduct->images[0];

                                    @endphp

                                    <img class="mw-100 rounded d-inline " style="background:{{isset($cartItem->product_information['product_color_code']) ? $cartItem->product_information['product_color_code'] : ''}};object-fit: cover;width: 100%;" src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                    @elseif(isset($cartItem->product_information['product_color_code']))

                                    <img class="mw-100 rounded d-inline " style="background:{{$cartItem->product_information['product_color_code']}};width: 100%;height: 50px;object-fit: cover;">

                                    @else

                                    <img class="mw-100 rounded d-inline "
                                    src="{{ asset('storage/' . $cartItem->product->parentProduct->images[0]->path. $cartItem->product->parentProduct->images[0]->filename)}}"
                                    alt="">

                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-8 col-md-4 float-left px-0">
                            <div class="row">
                                <div class="col-12 col-md-auto float-left">
                                    @if ($cartItem->disabled != 4)
                                        <a href="/shop/product/{{ $cartItem->product->parentProduct->name_slug}}?panel={{ $cartItem->product->panel_account_id }}">
                                    @endif
                                        <p class="m-0 pl-2 text-dark text-left" style="font-size: 15px;">
                                            <b>{{ $cartItem->product->parentProduct->name }}</b>
                                        </p>
                                    </a>
                                </div>
                                <div class="col-12 col-md-12 float-right">
                                    <p class="text-left pl-2 m-0 text-muted" style="font-size: 12px;">

                                        @if(array_key_exists('product_temperature', $cartItem->product_information))
                                        Color Temp. : {{ $cartItem->product_information['product_temperature'] }}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_size', $cartItem->product_information))
                                        Size : {{ $cartItem->product_information['product_size'] }}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_curtain_size', $cartItem->product_information))
                                        Curtain Model : {{ $cartItem->product_information['product_curtain_size'] }}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_color_name', $cartItem->product_information))
                                        Color : {{ $cartItem->product_information['product_color_name']}}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_miscellaneous', $cartItem->product_information))
                                        {{ $cartItem->product_information['product_miscellaneous'] }}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_preorder_date', $cartItem->product_information))
                                        Pre Order Delivery: {{ $cartItem->product_information['product_preorder_date']}}
                                        <br>
                                        @endif
                                        @if(array_key_exists('invoice_number', $cartItem->product_information))
                                        Invoice Number: {{ $cartItem->product_information['invoice_number'] }}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_order_remark', $cartItem->product_information))
                                        Remarks: {{ $cartItem->product_information['product_order_remark'] }}
                                        <br>
                                        @endif
                                        @if(array_key_exists('product_order_selfcollect', $cartItem->product_information) && $cartItem->product_information['product_order_selfcollect'])
                                        Self Collection: Yes
                                        </span>
                                        <br>
                                        @endif
                                         <!-- Trade In -->
			                            @if(array_key_exists('product_order_tradein', $cartItem->product_information) &&
			                            $cartItem->product_information['product_order_tradein'])
			                            Rebate: - {{$countryCurrency->country_currency}}
			                            {{ number_format(($cartItem->product_information['product_order_tradein'] * $cartItem->quantity)/100),2 }}
			                            {{ $cartItem->quantity > 1 ? '( Trade-in '. $cartItem->quantity . ' sofa )' : ''}}
			                            </span>
                                        <br>
			                            @endif
                                        <!-- Discount Auto Apply -->
			                            @if(array_key_exists('product_order_discount', $cartItem->product_information) &&
			                            $cartItem->product_information['product_order_discount'])
			                            Rebate: - {{$countryCurrency->country_currency}}
			                            {{ number_format(($cartItem->product_information['product_order_discount']/100),2) }}			                           
			                            </span>
                                        <br>
			                            @endif
                                        <!-- Installation -->
                                        @if(array_key_exists('product_installation', $cartItem->product_information) &&
			                            $cartItem->product_information['product_installation'])
                                        Installation:
                                        {{($cartItem->installationCategories()!=NULL) ? $cartItem->installationCategories()->cat_name : ''}}
			                            </span>
			                            @endif

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 ml-3 ml-md-0 col-md-6 float-left">
                            <div class="row pt-md-2">
                                <div class="col-4 p-0 ">
                                    <div class="input-group qty-field mx-auto py-2 my-auto">
                                        <span class="input-group-btn minus">
                                            <button type="button" class="btn btn-default btn-number p-lg-1 px-0 cart" data-type="minus"
                                                data-field="quant[{{ $cartItem->id }}]">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                        </span>
                                        <div class="d-inline p-0 mx-1 totNum">
                                            <input type="text" name="quant[{{ $cartItem->id }}]"
                                                class="p-lg-1 form-control input-number text-center"
                                                value="{{ $cartItem->quantity }}" min="1" max="1000"
                                                data-item-id="{{ $cartItem->id }}" size="4">
                                        </div>
                                        <span class="input-group-btn plus">
                                            <button type="button" class="btn btn-default btn-number p-lg-1 px-0 cart" data-type="plus"
                                                data-field="quant[{{ $cartItem->id }}]">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-6  my-auto pr-0">
                                    <span style="font-size: 16px; color: #f0c230;" class="font-weight-bold my-1">
                                        {{country()->country_currency}}  {{ number_format(($cartItem->subtotal_price / 100), 2) }}
                                    </span>
                                </div>
                                <div class="col-1  my-auto pr-0">
                                    <span class="">
                                        <a href="javascript:void()" id="DeleteCartItem" class="text-decoration-none"
                                            data-value="{{ $cartItem->id }}">
                                            <i class="fa fa-trash text-muted"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        @else

            <!-- Bundle subproduct show here -->
            <div class="card -radius-0 mb-4 px-4 py-2 subbundle">
                <div class="row d-md-block sub">
                    <div class="col-4 col-md-2 float-left ">
                        <div class="row pt-md-2">
                            <div class="col-1 my-auto mx-0 pr-0">
                                <input type="checkbox" name="cartItemId[]"
                                    class="custom-control-input item-checkbox {{ ($cartItem->disabled == 4) ? 'disabled' : '' }}"
                                    id="item-{{ $cartItem->id }}" value="{{ $cartItem->id }}"
                                    data-unit-price="{{ $cartItem->unit_price }}" data-quantity="{{ $cartItem->quantity }}"
                                    data-shipping-price="{{ $cartItem->delivery_fee }}"
                                    {{-- data-installation-price="{{ $cartItem->installation_fee }}" --}}
                                    data-tradein-price="{{ $cartItem->rebate != 0 ? $cartItem->rebate : 0 }}"
                                    data-panel-id="{{ $cartItem->product->panel_account_id }}"
                                    data-bundle-id="{{ $cartItem->bundle_id }}"
                                    {{ (($cartItem->selected == 1) && ($cartItem->disabled != 4))  ? 'checked' : '' }}
                                    {{-- {{ ($cartItem->disabled != 0 && $cartItem->disabled != 3) || ($cartItem->disabled == 4) ? 'disabled' : '' }} --}}
                                    >
                            </div>

                            <div class="col-8 p-0">

                                @if (isset($cartItem->product_information['product_color_img']))
                                @php
                                    $value = $cartItem->product->parentProduct->images->where('id',$cartItem->product_information['product_color_img'])->first();
                                    if (! $value ) $value =  $cartItem->product->parentProduct->images[0];
                                @endphp
                                {{-- {{$cartItem->product_information['product_color_img']}} --}}

                                <img class="mw-60 rounded d-inline bundle" style="background:{{isset($cartItem->product_information['product_color_code']) ? $cartItem->product_information['product_color_code'] : ''}};width: 100%;object-fit: cover;" src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                @elseif(isset($cartItem->product_information['product_color_code']))

                                <img class="mw-60 rounded d-inline " style="background:{{$cartItem->product_information['product_color_code']}};width: 100%;height: 50px;object-fit: cover;">

                                @else
                                {{-- <p class="no-image">no images</p> --}}
                                <img class="mw-60 rounded d-inline " style="width: 100%;height: 50px;object-fit: cover;"
                                src="{{ asset('storage/' . $cartItem->product->parentProduct->images[0]->path. $cartItem->product->parentProduct->images[0]->filename) }}"
                                alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-md-4 float-left px-0">
                        <div class="row">
                            {{-- @if ($cartItem->disabled == 4)
                            <div class="col-12  col-md-auto float-left text-left ">
                                <span class="text-danger" style=" font-size: .8rem;">
                                    Product no longer available.
                                </span>
                            </div>
                            @endif --}}
                            <div class="col-12 pl-0  col-md-auto float-left">

                                @if ($cartItem->disabled == 0 || $cartItem->disabled == 3)
                                    <a href="/shop/product/{{ $cartItem->product->parentProduct->name_slug}}?panel={{ $cartItem->product->panel_account_id }}">
                                @endif
                                    <p class="m-0 pl-2 text-dark text-left" style="font-size: 15px;">
                                        <b>{{ $cartItem->product->parentProduct->name }} x <span>{{ $cartItem->quantity }}</span></b>
                                    </p>

                                @if ($cartItem->disabled == 0 || $cartItem->disabled == 3)
                                    </a>
                                @endif

                                <p class="text-left pl-2 m-0 text-muted" style="font-size: 12px;">

                                    @if(array_key_exists('product_temperature', $cartItem->product_information))
                                    Color Temp. : {{ $cartItem->product_information['product_temperature'] }}
                                    <br>
                                    @endif
                                    @if(array_key_exists('product_size', $cartItem->product_information))
                                    Size : {{ $cartItem->product_information['product_size'] }}
                                    <br>
                                    @endif
                                    @if(array_key_exists('product_curtain_size', $cartItem->product_information))
                                    Curtain Model : {{ $cartItem->product_information['product_curtain_size'] }}
                                    <br>
                                    @endif
                                    @if(array_key_exists('product_color_name', $cartItem->product_information))
                                    Color : {{ $cartItem->product_information['product_color_name']}}
                                    <br>
                                    @endif
                                    @if(array_key_exists('invoice_number', $cartItem->product_information))
                                    Invoice Number: {{ $cartItem->product_information['invoice_number'] }}
                                    @endif
                                    @if(array_key_exists('product_miscellaneous', $cartItem->product_information))
                                    {{ $cartItem->product_information['product_miscellaneous'] }}
                                    @endif
                                    @if(array_key_exists('product_preorder_date', $cartItem->product_information))
                                    Pre Order Delivery: {{ $cartItem->product_information['product_preorder_date']}}
                                    <br>
                                    @endif
                                    @if(array_key_exists('product_order_remark', $cartItem->product_information))
                                    Remarks: {{ $cartItem->product_information['product_order_remark'] }}
                                    <br>
                                    @endif
                                    @if(array_key_exists('product_order_selfcollect', $cartItem->product_information) && $cartItem->product_information['product_order_selfcollect'])
                                    Self Collection: Yes
                                    </span>
                                    @endif
                                        <!-- Trade In -->
                                    @if(array_key_exists('product_order_tradein', $cartItem->product_information) &&
                                    $cartItem->product_information['product_order_tradein'])
                                    Rebate: - {{country()->country_currency}}
                                    {{ number_format(($cartItem->product_information['product_order_tradein'] * $cartItem->quantity)/100),2 }}
                                    {{ $cartItem->quantity > 1 ? '( Trade-in '. $cartItem->quantity . ' sofa )' : ''}}
                                    </span>
                                    @endif
                                    <!-- Installation -->
                                    @if(array_key_exists('product_installation', $cartItem->product_information) &&
                                    $cartItem->product_information['product_installation'])
                                    Installation: {{ $cartItem->installationCategories()->cat_name }}
                                    </span>
                                    @endif

                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        @endif

    @endforeach

@endif


{{-- {{ dd($cartPanels) }} --}}
{{-- @foreach ($cartPanels as $panelid => $cartPanel)

@foreach ($cartPanel as $cart)

{{ $cart }}

@endforeach


@endforeach --}}
