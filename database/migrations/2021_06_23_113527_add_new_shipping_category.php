<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ShippingInstallations\Ship_category;

class AddNewShippingCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $shipping_category = new Ship_category;
        $shipping_category->cat_name = 'Default';
        $shipping_category->cat_desc = 'Default';
        $shipping_category->cat_type = 'shipping';

        $shipping_category->active = '1';
        $shipping_category->for_user = '["6000"]';
        $shipping_category->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
