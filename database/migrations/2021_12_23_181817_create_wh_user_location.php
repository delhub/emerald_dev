    <?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Support\Facades\DB;
    use App\Models\Users\User;
    use App\Models\Warehouse\User\WarehouseUserLocation;

    class CreateWhUserLocation extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('wh_user_location', function (Blueprint $table) {
                $table->id();
                $table->bigInteger('account_id');
                $table->bigInteger('location_id');
                $table->timestamps();

                $table->index(['account_id', 'location_id']);
            });

            $emails = [
                'warehouse1@email.com',
                'pick1@email.com',
                'sort1@email.com',
                'pack1@email.com',
                'transferrer1@email.com',
                'receiver1@email.com',
                'receiver2@email.com',
                'receiver3@email.com',
                'receiver4@email.com',
                'receiver5@email.com',
                'receiver6@email.com',
                'receiver7@email.com',
                'receiver8@email.com',
                'receiver9@email.com',
                'receiver10@email.com',
                'b_unbundle1@email.com',
            ];

            $users = User::whereIn('email', $emails)->get();

            foreach ($users as $user) {
                $userLocation = new WarehouseUserLocation;
                $userLocation->account_id = $user->userInfo->account_id;
                $userLocation->location_id = 1;
                $userLocation->save();
            }

            Schema::table('wh_picking', function (Blueprint $table) {
                $table->bigInteger('location_id')->after('bin_id');
            });

            DB::table('wh_picking')->update(['location_id' => 1]);

            Schema::table('pick_bin', function (Blueprint $table) {
                $table->bigInteger('location_id')->after('bin_number');
            });

            DB::table('pick_bin')->update(['location_id' => 1]);

            Schema::table('wh_box', function (Blueprint $table) {
                $table->bigInteger('location_id')->after('box_name');
            });

            DB::table('wh_box')->update(['location_id' => 1]);
        }


        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('wh_user_location');
        }
    }
