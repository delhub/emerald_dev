<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @include('layouts.favicon')
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> --}}
    <link href="{{ asset('css/register-page.css') }}?v=2.5" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}?v=3.5" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine&display=swap" rel="stylesheet">

</head>
@if(session('error'))
    <div id="error-message" class="alert alert-danger position-fixed top-0 m-3" 
        style="z-index: 9999; right: 0px; opacity: 1; transition: opacity 1s;">
        {{ session('error') }}
    </div>
    <script>
        setTimeout(function() {
            let errorMessage = document.getElementById('error-message');
            if (errorMessage) {
                errorMessage.style.opacity = 0;

                setTimeout(function() {
                    errorMessage.style.display = 'none';
                }, 1000);
            }
        }, 2000);
    </script>
@endif
<body>
    <div class="r-3d-bg">
        <div class="my-pb-12"></div>
        <div class="container my-mb-3">
            <div class="row">
                <div class="my col-10 col-md-4 register-logo">
                    <img class="my-mx-register" src="{{ asset('images/logo/formula-logo.png') }}" alt="Bujishu">
                </div>
            </div>
        </div>
        <div class="container r-wlc-text">
            <div class="row my-mb-3">
                <div class="col-12 register-title my-pb-1">
                    <h2>
                        Your health means the
                        <p>
                        <p>world</p>
                        to us.
                        </p>
                    </h2>
                    <h2>
                        We provide the
                        <p>
                        <p>best</p>
                        service for the best customers!
                        </p>
                    </h2>
                </div>
            </div>
            <div class="r-btn-opt">
                <div class=" btn-register1">
                    <a href="/login " class="r-btn">
                        <b>{{ __('LOGIN') }}</b>
                    </a>

                    <a href="/register" class="r-btn">
                        <b> {{ __('BE CUSTOMER') }}</b>
                    </a>

                    <a href="/register-dealer " class="r-btn">
                        <b>{{ __('BE AN AGENT') }}</b>
                    </a>

                    {{-- <a href="/register-dealer-launching " class="r-btn">
                        <b>{{ __('BE AN AGENT') }}</b>
                    </a> --}}
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>
