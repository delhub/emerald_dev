<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_batch', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('batch_id',24)->nullable();
            $table->bigInteger('panel_id')->nullable();
            $table->string('po_number',24)->nullable();
            $table->timestamp('stock_in_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_batch');
    }
}
