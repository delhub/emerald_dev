@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Product Article</a></li>
            </ul>
        </div>
    </div>
    <section class="fml-multineed">
        <div class="fml-container">
            <div class="multineed-header my-mb-3">
                <h1>
                    Health
                    <span>
                        Article
                        <div class="info-hrline-red"></div>
                    </span>
                </h1>
            </div>
        </div>
        
        <div class="hr-row">
            <div class="fml-container">
                <div class="d-flex align-items-center">
                    <div class="flex-shrink-0">
                        <a href="#" class="btn-left btn-link p-2 toggleLeft"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    </div>
                    <div class="flex-grow-1 position-relative overflow-hidden" id="outer">
                        <ul class="nav nav-pills text-uppercase position-relative flex-nowrap" id="pills-tab" role="tablist">
                            
                            <li class="nav-item active">
                                <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info" role="tab" aria-controls="pills-contact" aria-selected="false">
                                  <img src="{{asset('/images/icons/multi01.png')}}" alt="">
                                  Testimonials</a>
                                  <div class="downarrow">
                                    <img src="{{asset('/images/icons/multidownarrow.svg')}}" alt="">
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="flex-shrink-0">
                        <a href="#" class="btn-right btn-link toggleRight p-2"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            
            </div>
        </div>
        
        <div class="full-hrline-grey hr-margin"></div>
        <div class="fml-container">
            <div class="row">
                    <div class="outer tab-content my-mt-5" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="testimonials-info" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="">
                                <div class="row multi-inner">
                                    <div class="col-md-12">
                                       
                                        <nav>
                                            <div class="multi-subinfo nav nav-tabs" id="nav-tab" role="tablist">
                                                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">
                                                    <img src="{{asset('/images/icons/icon-adult.png')}}" alt="">
                                                    <p>Adult</p>
                                                    <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                                                </button>

                                                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">
                                                    <img src="{{asset('/images/icons/icon-elderly.png')}}" alt="">
                                                    <p>Elderly</p>
                                                    <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                                                </button>

                                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">
                                                    <img src="{{asset('/images/icons/icon-kids.png')}}" alt="">
                                                    <p>Kids</p>
                                                    <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                                                </button>

                                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">
                                                    <img src="{{asset('/images/icons/icon-kids.png')}}" alt="">
                                                    <p>Man</p>
                                                    <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                                                </button>

                                                <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">
                                                    <img src="{{asset('/images/icons/icon-kids.png')}}" alt="">
                                                    <p>Women</p>
                                                    <div class="downarrow"><img src="{{asset('/images/icons/multidownarrow.svg')}}" alt=""></div>
                                                </button>
                                            </div>
                                        </nav>
                                        
                                        <div class="row">
                                            <div class="col-12">
                                                <h1 class="my-mb-1 text-center">
                                                    How to improve memory?
                                                    <div class="info-hrline-red"></div>
                                                </h1>
                                                <div class="inner tab-content  height-limit-scroll" id="nav-tabContent">
                                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                        <div class="row">
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                        <div class="row">
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                                        <div class="row">
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mult-info">
                                                                <div class="mult-info-6">
                                                                    <img src="{{asset('/images/formula/image-human-brain.png')}}" alt="">
                                                                </div>
                                                                <div class="mult-info-6">
                                                                    <p>
                                                                        According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. Being overweight puts you at risk for many serious health conditions such as hypertension, diabetes, fatty liver disease, osteoarthritis, etc. According to National Health Morbidity Health Survey 2019, 50.1% of adults in Malaysia were overweight or obese. Obesity is more than just a cosmetic issue. 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row multi-inner my-mt-5">
                                <div class="col-md-12">
                                    <h1 class="my-mb-1 text-center">
                                        Here are the recommended products
                                        <span>Brand</span>
                                        <div class="info-hrline-red"></div>
                                    </h1>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 item414">
                                            <div class="product-item-list">
                                                <a href="{{ route('shop.product_pageitem') }}">
                                                    <img src="{{asset('/images/product_item/sample_item.png')}}" alt="">
                                                    <h5>Blackmores Dietary Supplement</h5>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">

            </div>
        </div>
    </section>
</div>
@endsection


@push('script')
<script>
    $(document).ready(function(){
        

    });
</script>
@endpush