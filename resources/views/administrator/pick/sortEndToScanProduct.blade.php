@inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>Scan Product Now</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div> --}}
        </div>
        <div class="table-responsive m-2">

            @if($pickBatchDetail)
            <p> <b>DO Number</b> : {{$pickBatchDetail->delivery_order }}</p>
            <p> <b>Batch Number</b> : {{$pickBatchDetail->batch->batch_number }}</p>

                <table id="table-id"  class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Attribute</th>
                            <th>Qty</th>
                            <th>Sorted Qty</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pickBatchDetail->items as $item)
                            @if ($item->item_order_status != 1011 && $item->product->parentProduct->no_stockProduct != 1)
                            <tr>
                                <td>
                                    {{$item->product_code }}
                                    @if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2)
                                    <hr>

                                        Bundle Item :
                                        @foreach ($item->bundleChild as $key => $bundle)
                                        <br>
                                            {{$bundle->primary_product_code}} @if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    {{$item->product->parentProduct->name }}

                                    @if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2)
                                    <hr>

                                        Product name :
                                        @foreach ($item->bundleChild as $key => $bundle)
                                        <br>
                                            {{ $bundle->productAttribute->product2->parentProduct->name}} @if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    @php
                                        if(array_key_exists('product_size', $item->product_information)){
                                            echo '(Size:'.$item->product_information['product_size'].')';
                                        }
                                    @endphp

                                    @if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2)
                                    <hr>

                                        Product Attribute :
                                        @foreach ($item->bundleChild as $key => $bundle)
                                        <br>
                                            {{ $bundle->productAttribute->attribute_name}} @if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                                </td>

                                <td>
                                    {{$item->quantity }}

                                    @if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2)
                                    <hr>

                                        Quantity :
                                        @foreach ($item->bundleChild as $key => $bundle)
                                        <br>
                                            {{($bundle->primary_quantity *$item->quantity) }}@if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    {{$item->sorted_qty }}

                                    @if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2)
                                    <hr>

                                        Sorted Qty :
                                        @foreach ($item->bundleChild as $key => $bundle)
                                        <br>
                                            {{$bundle->sorted_qty}}@if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-12 text-center">
                        <form action="{{route('administrator.pickBatch.sortEnd-toScanDo-redirect', ['again' => 1,'batch_id'=>$pickBatchDetail->id])}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                            @csrf
                        <button type="button" class="btn btn-success"
                        onclick="clickSubmit();"> Back</button>
                        </form>
                    </div>
                </div>
                {{-- {{$pickBatchDetail->links()}} --}}
            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Found</p>
            @endif
        </div>
    </div>
</div>

<div class="my-input-warehouse input-group input-group-lg mb-3">
    <input type="text" ref="scancode" v-model="scancode" name="scancode" class="bg-form2 form-control" id="my-input-warehouse"
        placeholder="Use Scanner Here" autofocus autocomplete="off" @@change="qrProduct(scancode)">
    <div class="pop-btn">
        <button type="button" class="qrbtn btn-primary">Submit</button>
    </div>
</div>

@endsection

@push('script')
<script>
function clickSubmit() {
    if (confirm('Are you checked product?')) {
        document.getElementById("create-form-id").submit();
    }
}
</script>
@endpush

