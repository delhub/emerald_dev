@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Product List One</a></li>
            </ul>
        </div>
    </div>
    <!-- sample for test dynamic banner Mobile  end -->
    <section>
        <div class="clickme-info web">
            <a data-bs-toggle="modal" href="#waringInfo" role="button">
                <img src="{{ asset('/images/icons/clickme-icon.png') }}" alt="">
            </a>
        </div>
        <div class="container">

            <div class="side-stickinfo active">
                <div class="open-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                {{-- <div class="close-icon"><i class="fa fa-angle-left" aria-hidden="true"></i></div> --}}
                <ul>
                    <li>
                        <div class="side-info ">
                            <p>1000</p>
                            <span>ml</span>
                        </div>
                    </li>
                    <li>
                        <div class="side-info like">
                            <img src="{{ asset('/images/icons/side02.png') }}" alt="">
                            <div class="like-bg">
                                <p>543</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="side-info">
                            <img src="{{ asset('/images/icons/side01.png') }}" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="side-info">
                            <img src="{{ asset('/images/icons/side03.png') }}" alt="">
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-12 product-item-bg pr">
                    <div class="slider slider-for tp">
                        <div>
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/test_productitem.png') }}" alt="">
                        </div>
                        <div>
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                        </div>
                        <div>
                            <img class="product-itemimg" src="{{ asset('/images/product_item/test_mask.png') }}"
                                alt="">
                        </div>
                        <div>
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/test_productitem.png') }}" alt="">
                        </div>
                        <div>
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                        </div>
                    </div>
                    {{-- slider nav --}}
                    <div class="slider slider-nav slick-dotted bot">
                        <div class="addpd">
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/test_productitem.png') }}" alt="">
                        </div>
                        <div class="addpd">
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                        </div>
                        <div class="addpd">
                            <img class="product-itemimg" src="{{ asset('/images/product_item/test_mask.png') }}"
                                alt="">
                        </div>
                        <div class="addpd">
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/test_productitem.png') }}" alt="">
                        </div>
                        <div class="addpd">
                            <img class="product-itemimg"
                                src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12">
                    <div class="productitem-info my-mb-3">
                        <h3>Blackmores Executive B Stress Formula</h3>
                        <p>
                            A specifically formulated combination of nutrients which helps reduce the symptoms of
                            stress. A specifically formulated combination of nutrients.
                        </p>
                        <div class="item-pricelist">
                            <div class="offer-price">
                                <img src="{{ asset('/images/icons/huangmao.png') }}" class="" alt="">
                                <p><span>MYR</span>1099.00</p>
                                <p>Offer Price</p>
                            </div>
                            <div class="member-price">
                                <img src="{{ asset('/images/icons/huangmao.png') }}" class="" alt="">
                                <p><span>MYR</span>1099.00</p>
                                <p>Member Price</p>
                            </div>
                            <div class="non-member-price">
                                <p><span>MYR</span>1099.00</p>
                                <p>Non Member Price</p>
                            </div>
                        </div>

                        {{-- footer menu at mobile --}}
                        <div class="ft-cart-content-body">
                            @include('layouts.shop.footer-menu.product-ft-cart')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="full-hrline-grey">

        <div class="container">
            <div class="row">
                <ul class="nav nav-pills my-pb-5" id="pills-tab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#detial-info" role="tab"
                            aria-controls="pills-home" aria-selected="true">
                            <img src="{{ asset('/images/icons/icon041.png') }}" alt="">
                            Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#ingredients-info"
                            role="tab" aria-controls="pills-profile" aria-selected="false">
                            <img src="{{ asset('/images/icons/icon042.png') }}" alt="">
                            Ingredients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#video-info" role="tab"
                            aria-controls="pills-contact" aria-selected="false">
                            <img src="{{ asset('/images/icons/icon043.png') }}" alt="">
                            Articles & Video</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info"
                            role="tab" aria-controls="pills-contact" aria-selected="false">
                            <img src="{{ asset('/images/icons/icon044.png') }}" alt="">
                            Testimonials</a>
                    </li>
                </ul>
                <div class="fml tab-content my-mt-5" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="detial-info" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1">
                                        Product Description
                                        <div class="info-hrline-red"></div>
                                    </h3>

                                    <p>
                                        A specifically formulated combination of nutrients which helps reduce the
                                        symptoms of stress.

                                        Blackmores Executive B Stress Formula contains a combination of ingredients
                                        which are beneficial in times of ongoing stress. It can temporarily relieve the
                                        symptoms of stress and support energy production and the healthy function of the
                                        nervous system. Blackmores Executive B Stress Formula also contains nutrients
                                        which are necessary for the heathy functioning of adrenal glands and which can
                                        also support the body's response to stress.
                                    </p>
                                    <ul class="product-inner-info">
                                        <li>
                                            <p>Place Origin :</p>
                                        </li>
                                        <li>
                                            <img src="{{ asset('/images/icons/country_icon/cAus.png') }}" alt="">
                                        </li>
                                        <li>
                                            <p><span>Australia</span></p>
                                        </li>
                                    </ul>
                                </div>

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1">
                                        Suitable For
                                        <div class="info-hrline-red"></div>
                                    </h3>

                                    <ul class="product-inner-info">
                                        <li>
                                            <div class="categories-smartlist">
                                                <a href="#" class="img-icon">
                                                    <img src="{{ asset('/images/icons/icon-adult.png') }}" alt="">
                                                </a>
                                                <p>Adult</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="categories-smartlist">
                                                <a href="#" class="img-icon">
                                                    <img src="{{ asset('/images/icons/icon-eldest.png') }}" alt="">
                                                </a>
                                                <p>Elderly</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="categories-smartlist">
                                                <a href="#" class="img-icon">
                                                    <img src="{{ asset('/images/icons/icon-kids.png') }}" alt="">
                                                </a>
                                                <p>Kids</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1">
                                        Ussage & Dosage
                                        <div class="info-hrline-red"></div>
                                    </h3>

                                    <ul class="product-inner-info ussage">
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <img src="{{ asset('/images/icons/icon-adult.png') }}" alt="">
                                                </div>
                                                <div class="ussage-group">
                                                    <h5>Adults </h5>
                                                    <p>Take 1 tablet with breakfast and 1 tablet with lunch, or as
                                                        professionally prescribed.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <img src="{{ asset('/images/icons/icon-adult.png') }}" alt="">
                                                </div>
                                                <div class="ussage-group">
                                                    <h5>Adults </h5>
                                                    <p>Take 1 tablet with breakfast and 1 tablet with lunch, or as
                                                        professionally prescribed.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <img src="{{ asset('/images/icons/icon-adult.png') }}" alt="">
                                                </div>
                                                <div class="ussage-group">
                                                    <h5>Adults </h5>
                                                    <p>Take 1 tablet with breakfast and 1 tablet with lunch, or as
                                                        professionally prescribed.</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1">
                                        Suitable For
                                        <div class="info-hrline-red"></div>
                                    </h3>
                                    <p>
                                        Audited and Certified by Health Sciences Authority of Malaysia. We supply
                                        government/ private hospitals, general practitioners, pharmacies, wholesalers,
                                        supermarkets, health and beauty retailers.
                                        <br>
                                        Audited and Certified by Health Sciences Authority of Malaysia. We supply
                                        government/ private hospitals, general practitioners.
                                    </p>

                                    <ul class="product-inner-info right-columns">
                                        <li>
                                            <img src="{{ asset('/images/icons/sijir01.png') }}" alt="">
                                        </li>
                                        <li>
                                            <img src="{{ asset('/images/icons/sijir02.png') }}" alt="">
                                        </li>
                                        <li>
                                            <img src="{{ asset('/images/icons/sijir01.png') }}" alt="">
                                        </li>
                                        <li>
                                            <img src="{{ asset('/images/icons/sijir02.png') }}" alt="">
                                        </li>
                                        <li>
                                            <img src="{{ asset('/images/icons/sijir01.png') }}" alt="">
                                        </li>
                                        <li>
                                            <img src="{{ asset('/images/icons/sijir02.png') }}" alt="">
                                        </li>
                                    </ul>
                                </div>

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1">
                                        Why Use
                                        <div class="info-hrline-red"></div>
                                    </h3>

                                    <ul class="product-inner-info ussage">
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="ussage-group">
                                                    <p>Supports healthy stress response in the body</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="ussage-group">
                                                    <p>Supports healthy stress response in the body</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="ussage-group">
                                                    <p>Supports healthy stress response in the body</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                    <img class="info-detials-img"
                                        src="{{ asset('/images/product_item/test_mask.png') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ingredients-info" role="tabpanel"
                        aria-labelledby="pills-profile-tab">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1">
                                        Ingredients
                                        <div class="info-hrline-red"></div>
                                    </h3>
                                    <h5>Each vegetable capsule contains:</h5>
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Vitamin A (60% as Retinyl Acetate and 40% Beta Carotene)
                                                </td>
                                                <td>
                                                    3500.00 iu
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Vitamin B3 (Nicotinamide)
                                                </td>
                                                <td>
                                                    3244.00 iu
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Vitamin B1
                                                </td>
                                                <td>
                                                    534.14 iu
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <div class="col-md-4 col-sm-12">

                                <div class="info-detials my-mb-3">
                                    <img class="ingredients-img"
                                        src="{{ asset('/images/product_item/test_mask.png') }}">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="video-info" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <a id="play-video" class="play-vedio-btn" href="#">
                                    <img src="{{ asset('/images/icons/play_img.png') }}" alt="">
                                </a>
                                <div class="info-detials video-box my-mb-3">
                                    <iframe id="top-video" width="100%" height="300"
                                        src="https://www.youtube.com/embed/uie9R6fjcc0?rel=0"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials video my-mb-3">
                                    <h3 class="my-mb-1">
                                        Product
                                    </h3>
                                    <span>
                                        Introduction
                                        <div class="info-hrline-red"></div>
                                    </span>
                                    <p>
                                        A specifically formulated combination of nutrients which helps reduce the
                                        symptoms of stress.

                                        Blackmores Executive B Stress Formula contains a combination of ingredients
                                        which are beneficial in times of ongoing stress. It can temporarily relieve the
                                        symptoms of stress and support energy production and the healthy function of the
                                        nervous system. Blackmores Executive B Stress Formula also contains nutrients
                                        which are necessary for the heathy functioning of adrenal glands and which can
                                        also support the body's response to stress.
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials video my-mb-3">
                                    <h3 class="my-mb-1">
                                        Scientific Doctor
                                    </h3>
                                    <span>
                                        Recommend
                                        <div class="info-hrline-red"></div>
                                    </span>
                                    <p>
                                        A specifically formulated combination of nutrients which helps reduce the
                                        symptoms of stress.

                                        Blackmores Executive B Stress Formula contains a combination of ingredients
                                        which are beneficial in times of ongoing stress. It can temporarily relieve the
                                        symptoms of stress and support energy production and the healthy function of the
                                        nervous system. Blackmores Executive B Stress Formula also contains nutrients
                                        which are necessary for the heathy functioning of adrenal glands and which can
                                        also support the body's response to stress.
                                    </p>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <a id="play-video02" class="play-vedio-btn" href="#">
                                    <img src="{{ asset('/images/icons/play_img02.png') }}" alt="">
                                </a>
                                <div class="info-detials video-box my-mb-3">
                                    <iframe id="top-video02" width="100%" height="300"
                                        src="https://www.youtube.com/embed/ErPsyBUCijM?rel=0"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="testimonials-info" role="tabpanel"
                        aria-labelledby="pills-contact-tab">
                        <div class="user-bg">
                            <div class="row ">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="rate-start-result my-mb-1">
                                                <h3>5.0</h3>
                                                <span>5 out of 5 stars</span>
                                                <div class="star-rate">
                                                    <img src="{{ asset('/images/icons/star_bg.png') }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="rate-line">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td class="rate01"><span>5</span></td>
                                                            <td class="rate02"><img
                                                                    src="{{ asset('/images/icons/small_star.png') }}"
                                                                    alt=""></td>
                                                            <td class="rate03">
                                                                <div class="line-clr active"></div>
                                                            </td>
                                                            <td class="rate04"><span>(100)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rate01"><span>0</span></td>
                                                            <td class="rate02"><img
                                                                    src="{{ asset('/images/icons/small_star.png') }}"
                                                                    alt=""></td>
                                                            <td class="rate03">
                                                                <div class="line-clr"></div>
                                                            </td>
                                                            <td class="rate04"><span>(100)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rate01"><span>0</span></td>
                                                            <td class="rate02"><img
                                                                    src="{{ asset('/images/icons/small_star.png') }}"
                                                                    alt=""></td>
                                                            <td class="rate03">
                                                                <div class="line-clr"></div>
                                                            </td>
                                                            <td class="rate04"><span>(100)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rate01"><span>0</span></td>
                                                            <td class="rate02"><img
                                                                    src="{{ asset('/images/icons/small_star.png') }}"
                                                                    alt=""></td>
                                                            <td class="rate03">
                                                                <div class="line-clr"></div>
                                                            </td>
                                                            <td class="rate04"><span>(100)</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rate01"><span>0</span></td>
                                                            <td class="rate02"><img
                                                                    src="{{ asset('/images/icons/small_star.png') }}"
                                                                    alt=""></td>
                                                            <td class="rate03">
                                                                <div class="line-clr"></div>
                                                            </td>
                                                            <td class="rate04"><span>(100)</span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 user-cm-list user-hr">
                                    <ul>
                                        <li>
                                            <div class="user-cm">
                                                <div class="user-avatar">
                                                    <img src="{{ asset('/images/user_photo/user01.png') }}" alt="">
                                                </div>
                                                <div class="cm-list">

                                                    <h6>Tan Yau Sheng</h6>
                                                    <ul>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                    </ul>
                                                    <span>Blackmores Executive B Stress Formula</span>
                                                    <p>
                                                        A specifically formulated combination of nutrients which helps
                                                        reduce the symptoms of stress. A specifically formulated
                                                        combination of nutrients which helps reduce the symptoms of
                                                        stress.A specifically formulated combination of nutrients which
                                                        helps reduce the symptoms of stress.A specifically formulated
                                                        combination of nutrients which helps reduce the symptoms of
                                                        stress.A specifically formulated combination of nutrients which
                                                        helps.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user-cm">
                                                <div class="user-avatar">
                                                    <img src="{{ asset('/images/user_photo/user01.png') }}" alt="">
                                                </div>
                                                <div class="cm-list">

                                                    <h6>Tan Yau Sheng</h6>
                                                    <ul>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                    </ul>
                                                    <span>Blackmores Executive B Stress Formula</span>
                                                    <p>
                                                        A specifically formulated combination of nutrients which helps
                                                        reduce the symptoms of stress. A specifically formulated
                                                        combination of nutrients which helps reduce the symptoms of
                                                        stress.A specifically formulated combination of nutrients which
                                                        helps reduce the symptoms of stress.A specifically formulated
                                                        combination of nutrients which helps reduce the symptoms of
                                                        stress.A specifically formulated combination of nutrients which
                                                        helps.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user-cm">
                                                <div class="user-avatar">
                                                    <img src="{{ asset('/images/user_photo/user01.png') }}" alt="">
                                                </div>
                                                <div class="cm-list">

                                                    <h6>Tan Yau Sheng</h6>
                                                    <ul>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('/images/icons/small_star.png') }}"
                                                                alt="">
                                                        </li>
                                                    </ul>
                                                    <span>Blackmores Executive B Stress Formula</span>
                                                    <p>
                                                        A specifically formulated combination of nutrients which helps
                                                        reduce the symptoms of stress. A specifically formulated
                                                        combination of nutrients which helps reduce the symptoms of
                                                        stress.A specifically formulated combination of nutrients which
                                                        helps reduce the symptoms of stress.A specifically formulated
                                                        combination of nutrients which helps reduce the symptoms of
                                                        stress.A specifically formulated combination of nutrients which
                                                        helps.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="fml-special-pro my-mb-3 my-pt-7">
        <div>
            <div class="row special-pro">
                <div class="col-md-12 text-center mb-5 mt-5">
                    <h1>
                        Immune Booster & Against<span>Covid-19</span>
                        <div class="info-hrline-red"></div>
                    </h1>
                </div>

                <div class="center my responsive slider">
                    <div class="special-box">
                        <div class="special-item-box">
                            <div class="special-left">
                                <img class="imgproduct"
                                    src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                            </div>
                            <div class="special-right">
                                <div class="orfer-special">
                                    <div class="sale-special">
                                        <p>30% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="name-special">
                                    <p class="special-tittle">Blackmores Dietary Supplement</p>
                                    <p class="special-price">RM100</p>
                                </div>
                                <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/category/vitamin-c" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="special-box">
                        <div class="special-item-box">
                            <div class="special-left">
                                <img class="imgproduct"
                                    src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                            </div>
                            <div class="special-right">
                                <div class="orfer-special">
                                    <div class="sale-special">
                                        <p>30% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="name-special">
                                    <p class="special-tittle">Blackmores Dietary Supplement</p>
                                    <p class="special-price">RM100</p>
                                </div>
                                <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/category/vitamin-c" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="special-box">
                        <div class="special-item-box">
                            <div class="special-left">
                                <img class="imgproduct"
                                    src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                            </div>
                            <div class="special-right">
                                <div class="orfer-special">
                                    <div class="sale-special">
                                        <p>30% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="name-special">
                                    <p class="special-tittle">Blackmores Dietary Supplement</p>
                                    <p class="special-price">RM100</p>
                                </div>
                                <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/category/vitamin-c" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="special-box">
                        <div class="special-item-box">
                            <div class="special-left">
                                <img class="imgproduct"
                                    src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                            </div>
                            <div class="special-right">
                                <div class="orfer-special">
                                    <div class="sale-special">
                                        <p>30% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="name-special">
                                    <p class="special-tittle">Blackmores Dietary Supplement</p>
                                    <p class="special-price">RM100</p>
                                </div>
                                <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/category/vitamin-c" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="special-box">
                        <div class="special-item-box">
                            <div class="special-left">
                                <img class="imgproduct"
                                    src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                            </div>
                            <div class="special-right">
                                <div class="orfer-special">
                                    <div class="sale-special">
                                        <p>30% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="name-special">
                                    <p class="special-tittle">Blackmores Dietary Supplement</p>
                                    <p class="special-price">RM100</p>
                                </div>
                                <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/category/vitamin-c" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="special-box">
                        <div class="special-item-box">
                            <div class="special-left">
                                <img class="imgproduct"
                                    src="{{ asset('/images/product_item/colgate_toothpaste.png') }}" alt="">
                            </div>
                            <div class="special-right">
                                <div class="orfer-special">
                                    <div class="sale-special">
                                        <p>30% <span>OFF</span></p>
                                    </div>
                                </div>
                                <div class="name-special">
                                    <p class="special-tittle">Blackmores Dietary Supplement</p>
                                    <p class="special-price">RM100</p>
                                </div>
                                <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/category/vitamin-c" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="waringInfo" aria-hidden="true" aria-labelledby="waringInfoLabel" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered warnings-popup-bg">
        <div class="modal-content edit-info-modal-box notice-border">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row warning-box">
                    <div class="col-md-4">
                        <img class="notice-img-icon" src="{{ asset('images/formula/warnings.png') }}" alt="">
                    </div>
                    <div class="col-md-8 warnings-details">
                        <div class="warning-text">
                            <h5>WARNINGS</h5>
                            <p>Always read the label. Follow the directions for use. If symptoms persist talk to your
                                health professional. Supplements may only be of assistance if dietary intake is
                                inadequate. This medicine may not be right for you. Read the warnings before purchase.
                            </p>
                        </div>
                        <div class="caution-text">
                            <h5>CAUTION</h5>
                            <p>Contains gluten.
                                If you are pregnant or breastfeeding talk to your health professional before use.</p>
                        </div>
                        <div class="edit-opt-btn">
                            <button class="cancel-btn btn-primary">Proceed</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
<!-- mobile footer menu bar -->
@include('layouts.shop.navigation.product-ft-menu')

@push('script')
{{-- <script>
    $(document).ready(() => {

        //add to cart footer menu only for product page
        const pageId = @Json($pageName);
        const proftmenu = document.querySelector('.footer-menu');
        // const winWith = window.innerWidth;
        if(pageId === "product_page"){
            proftmenu.style.display = 'block';
        } else {
            proftmenu.style.display = 'none';
        }

    });

</script> --}}
@endpush
