<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bujishu - Purchase Order {{ $order->order_number}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
     @include('layouts.favicon')
    <style>
        * {
            padding: 0;
            margin: 0;
            font-family: 'Lato', sans-serif;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        .text-grey {
            color: #58595b;
        }

        .topBottomBorder {
            padding: 13px 0px;
            border-top: 1.5px solid rgba(255, 204, 0, 0.6);
            border-bottom: 1.5px solid #ffcc00
        }

        .topGold {
            border-top: 1px solid #ffcc00;
        }

        .totalAmount {
            border-top: 1px solid #ffcc00;
            width: 134px;
            text-align: center;
            padding: 12px 10px;
            background-color: #fef5d6;
            font-weight: 600;
            font-size: 16px;

        }

        .totalTitle {
            border-top: 1px solid #ffcc00;
            width: 151px;
            padding: 12px 10px;
            text-align: right;
            font-weight: 600;
            font-size: 16px;
        }

        .paymentReceived {
            border-top: 1px solid #ffcc00;
            width: 605px;
            padding: 12px 10px;
        }

        td img.my-img1{
            object-fit: contain!important;
            width: 110px!important;
            /* height: 80px!important; */
        }
        .sty-span{
            width: 241px;
            text-align: left;
        }
        .sty-span{
            font-size: .8rem;
        }
        .sty-span .span600 {
            font-weight: 600;
            font-size: 1rem;
        }


    </style>
</head>

<body style="margin: 0; padding: 0; height: 100vh;">
    <?php



    // Declare batch
    $batchs = array();
    $batchCount = 1;
    $productCount = 0;
    $batchs[$batchCount] = collect();
    $key = 0;
    $qtyTotal = 0;

    // Subtotal
    $subtotalPrice = 0;
        // dd($order);
        foreach ($order->items as $item) {

            $qtyTotal += $item->quantity;

            // Start batch loop
            $batchs[$batchCount]->add($item);
            $productCount++;

            if ($productCount > 5 * $batchCount )
            {
                $batchCount++;
                $batchs[$batchCount] = collect();
            };

        }

    ?>


    @foreach( $batchs as $batch_id => $batch)
    <!-- First Page -->
    <div
        style="page-break-inside: avoid; width: 100%; height: 1385px; position: relative; padding-top: 1%; padding-bottom: 1%;">
        <!-- Header -->
        @include('documents.purchase.v2.header-purchases')

        <!-- Second Header -->
        <div style="margin: 5px 40px 30px 40px;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 56%; vertical-align: bottom; padding-top: 20px; padding-right: 5px;">
                        <span class="text-grey" style="display: inline-block; width: 43%; padding: 0 10px;">
                            PANEL:
                        </span>
                        <span class="text-grey" style="display: inline-block; width: 46%; padding: 0 10px;">
                            DELIVER TO:
                        </span>
                        <div style="margin-top: 8px; border-bottom: 2px solid #ffcc00;"></div>

                        <div style="width: 100%;">
                            <table style="display: inline-block; width: 48%; padding: 5px 10px;">
                                <tr>
                                    <td>
                                        <p style="font-size: .75rem; line-height: 1.2;">
                                            {{ $order->panel->correspondenceAddress->address_1}}
                                            <br>
                                            {{ $order->panel->correspondenceAddress->address_2}}
                                            {{ $order->panel->correspondenceAddress->address_3 }}
                                            <br>
                                            {{ $order->panel->correspondenceAddress->postcode }},
                                            @php
                                                if($order->panel->correspondenceAddress->cityKey != NULL && !empty($order->panel->correspondenceAddress->cityKey->id)){
                                                    echo $order->panel->correspondenceAddress->cityKey->city_name ;
                                                }else{
                                                    echo $order->panel->correspondenceAddress->city;
                                                }
                                            @endphp
                                            <br>
                                            {{ $order->panel->correspondenceAddress->state->name }},
                                            Malaysia.
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Attn :
                                            <span>
                                                {{ $order->panel->company_name }}
                                            </span>
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Tel :
                                            <span>
                                                {{ $order->panel->company_phone }}
                                            </span>
                                        </p>
                                    </td>
                                </tr>
                            </table>

                            <table style="display: inline-block; width: 48%; padding: 5px 10px;vertical-align: top;">
                                <tr>
                                    <td>
                                        <p style="font-size: .75rem; line-height: 1.2;">
                                            {{ isset($order->purchase->ship_address_1) ? $order->purchase->ship_address_1 : '' }}
                                            <br>
                                            {{ isset($order->purchase->ship_address_1) ? $order->purchase->ship_address_2 : ''}}
                                            @if (isset($order->purchase->ship_address_1))
                                            @if (isset($order->purchase->ship_address_3))
                                            <br>
                                            {{ $order->purchase->ship_address_3 }}
                                            @endif
                                            @else
                                            {{-- @if (isset($order->purchase->user->userInfo->shippingAddress->address_3))
                                            <br>
                                            {{ $order->purchase->user->userInfo->shippingAddress->address_3 }}
                                            @endif --}}
                                            @endif
                                            {{ isset($order->purchase->ship_address_1) ? $order->purchase->ship_postcode : '' }}
                                            @php
                                            if(isset($order->purchase->ship_address_1)){
                                                if ($order->purchase->cityKey != NULL && $order->purchase->ship_city_key != 0) {
                                                    echo $order->purchase->cityKey->city_name;
                                                }else {
                                                    echo $order->purchase->ship_city  ;
                                                }
                                            }else{
                                                echo '' ;
                                            }
                                            @endphp
                                            <br>
                                            {{ isset($order->purchase->ship_address_1) ? $order->purchase->state->name : '' }},
                                            Malaysia.
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Attn :
                                            <span>
                                                {{ ($order->purchase->ship_full_name != null) ? $order->purchase->ship_full_name : '' }},
                                            </span>
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Tel :
                                            <span>
                                                {{ ($order->purchase->ship_contact_num != null) ? $order->purchase->ship_contact_num : '' }},
                                            </span>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div style="margin-top: 8px; border-bottom: 1px solid rgba(255, 204, 0, 0.6);"></div>
                    </td>
                    <td style="width: 2%;"></td>
                    <td style="width: 42%; vertical-align: top;">
                        <p style="font-size: 1.8rem; color: #ffcc00; font-weight: 700;">
                            <span style="margin: 0 10px;">PURCHASE ORDER</span>
                            <div style="background-color: #fef5d6; border-radius: 20px; padding: 20px 0px 20px 20px;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 50%;">
                                            PURCHASE ORDER NO.
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 45%;">
                                            {{ $order->getOrderNumberFormatted() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ $order->purchase->getFormattedDate() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Agent Code
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ ($order->purchase->user->userInfo->agentInformation) ? $order->purchase->user->userInfo->agentInformation->account_id : ''}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Credit Terms
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <span style="text-transform: capitalize;">
                                                {{ $order->purchase->purchase_type }}
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Page
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ $batch_id }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <!-- End Second Header -->

        <!-- Content -->
        <div style="margin: 10px 40px;">
            <table cellspacing="0" cellpadding="0">
                <tr style="text-align: center;font-size:16px;background-color: #fef5d6;font-weight:600;">
                    <td class="topBottomBorder" style="width: 76.5px;">No.</td>
                    <td class="topBottomBorder" style="width: 236.5px;">Product ID</td>
                    <td class="topBottomBorder" style="width: 446.5px;">Description</td>
                    <td class="topBottomBorder" style="width: 146.5px;">Qty</td>
                </tr>
                @foreach ($batch as $item)
                    <tr style="vertical-align: top;" class="op">
                        <td class="topGold" style="text-align: center;padding:10px;">
                            @php
                            $batchs[$key] = $item;
                            $key++;
                            @endphp
                            {{ $key }}
                        </td>
                        <td class="topGold" style="padding:10px;text-align: center;">
                            {{ $item->product_code }}
                        </td>
                        <td class="topGold" style="padding:10px;word-wrap: break-word;">
                            <table cellspacing=" 5" >
                                <tr style="vertical-align: top;">
                                    <td style="width: 110px">
                                        @if (isset($item->product_information['product_color_img']) && $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first() != NULL)

                                        @php
                                            $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first()
                                        @endphp

                                        <img class="mw-100 rounded d-inline my-img1" style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};display: inline-block; margin-right: 12px;" src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                        @elseif(isset($item->product_information['product_color_code']) && $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first() !=NULL)

                                        @php
                                            $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first()
                                        @endphp

                                        <img class="mw-100 rounded d-inline my-img1" style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};display: inline-block; margin-right: 12px;">

                                        @elseif($item->product->parentProduct->images->isnotEmpty())

                                        <img class="mw-100 rounded d-inline my-img1"
                                        src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}"
                                        alt="">

                                        @else

                                        <img class="mw-100 rounded d-inline my-img"
                                            src="{{ asset('assets/images/errors/image-not-found.png') }}" style="display: inline-block;float:right;background-size:contain;object-fit: contain;"
                                            alt="">

                                        @endif

                                        {{-- @foreach($item->product->parentProduct->images as $value)
                                            @if ($value['id'] == $item->product_information['product_color_img'] )

                                                <img class="mw-100 rounded d-inline " style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};width: 110px; display: inline-block; margin-right: 12px;" src="{{ asset('storage/' . $value['path'] . '' . $value['filename']) }}" >

                                            @endif
                                        @endforeach --}}
                                    </td>
                                    <td class="sty-span">
                                        {{-- <p style=" vertical-align: top; text-align: left;"> --}}
                                            <!-- Product Name -->
                                            <span class="span600">{{ $item->product->parentProduct->name }}</span>
                                            @if($item->product_id != 175 || $item->product_id != 176)
                                            @if(array_key_exists('product_color_name', $item->product_information))
                                            <br>
                                            <span>
                                                Color: {{ $item->product_information['product_color_name'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_size', $item->product_information))
                                            <br>
                                            <span >
                                                Size: {{ $item->product_information['product_size'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_curtain_size', $item->product_information))
                                            <br>
                                            <span>
                                                Curtain Model: {{ $item->product_information['product_curtain_size'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_temperature', $item->product_information))
                                            <br>
                                            <span>
                                                Color Temperature:
                                                <br />{{ $item->product_information['product_temperature'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_miscellaneous', $item->product_information))
                                            <br>
                                            <span>
                                                {{ $item->product_information['product_miscellaneous'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('invoice_number', $item->product_information))
                                            <br>
                                            <span>
                                                Invoice Number: {{ $item->product_information['invoice_number'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_preorder_date', $item->product_information))
                                            <br>
                                            <span>
                                                Pre Order
                                                Delivery:<br />{{ $item->product_information['product_preorder_date'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_order_remark', $item->product_information))
                                            <br>
                                            <span>
                                                Remarks:<br />{{ $item->product_information['product_order_remark'] }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_order_selfcollect', $item->product_information) &&
                                            $item->product_information['product_order_selfcollect'])
                                            <br>
                                            <span>
                                                Self Collection: Yes
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_order_tradein', $item->product_information) &&
                                            $item->product_information['product_order_tradein'])
                                            <br>
                                            <span>
                                                Rebate: - RM
                                                {{ number_format($item->product_information['product_order_tradein'] / 100),2 }}
                                            </span>
                                            @endif
                                            @if(array_key_exists('product_installation', $item->product_information) &&
                                            $item->product_information['product_installation'])
                                            <br>
                                            <span>
                                                {{($item->installationCategories()!=NULL) ? $item->installationCategories()->cat_name : ''}}
                                            </span>
                                            @endif
                                            @endif
                                        {{-- </p> --}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="topGold" style=" text-align: center;padding:10px;background-color: #fef5d6;">
                            {{ $item->quantity }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- End Content -->

        <!-- Total Footer -->
        <div style="margin: 10px 40px; position: fixed; bottom: 48; width: 904px; z-index:-1;">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 605px;">&nbsp;</td>
                    <td style="width: 151px;">&nbsp;</td>
                    <td style="background-color: #fef5d6;height:780px;">&nbsp;</td>
                </tr>
                <tr>
                    <td class="paymentReceived text-grey" rowspan="6" style="padding: 20px;">
                        <p style="font-size: 1.15rem; font-weight: 600; margin-bottom: 6px;">
                            REMARK
                            <p>
                                <div style="background-color: #fef5d6; border-radius: 10px; width: 45%;">

                                </div>
                                <p style="font-size: .75rem; color: #000000; margin-top: 6px;">
                                    The issuance of this Purchase Order shall be demeed as your
                                    acceptance of the terms and conditions of this Disclaimer. The Bujishu Agent Agreement shall only become effective after full payment is received by the Company.
                                </p>
                    </td>
                    <td class="totalTitle" style=" border-bottom: 1px solid #ffcc00;color: #ffcc00;">
                        Total
                    </td>
                    <td class=" totalAmount" style="background-color: #ffcc00;">
                        {{ $qtyTotal }}
                    </td>
            </table>
        </div>
        <!-- End Total Footer -->

        <!-- Footer -->
        @include('documents.purchase.v2.footer')
    </div>
    <!-- End First Page -->
    @endforeach


</html>
