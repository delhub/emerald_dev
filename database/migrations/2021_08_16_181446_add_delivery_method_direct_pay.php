<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeliveryMethodDirectPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            $table->integer('outlet_id')->nullable()->after('purchase_id');
            $table->string('delivery_method')->nullable()->after('purchase_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            //
        });
    }
}
