<!-- Footer -->
<!DOCTYPE html>
<html>

<head>
    @include('layouts.favicon')
    <style type="text/css">
    .footer{
        width: 100%;
        margin: 0;
        height: 50px;
    }
    .bottomBorder{
        width: 100%;
        border-top: 18px solid #ffcc00;
    }
    .footer-info{
        width: 100%;
        text-align: center;
        margin: 0;
        font-size: .85rem;
        line-height: 40px;
    }
    .text-grey {
        color: #58595b;
    }
    </style>
</head>

<body class="footer">
    <tfoot style="width: 991px;float:left;height:49px;">
        <p class="text-grey footer-info">This is a computer generated invoice, no signature is required.</p>
        <div class="bottomBorder"></div>
    </tfoot>

</body>

</html>

<!-- End Footer -->
