@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Warehouse - Reprint QR Item</h1>
<div class="card shadow-sm">
    <div class="card-header">Warehouse <small> - Reprint QR Item</small>
        {{-- <div class="card-header-actions"><button class="btn btn-primary btn-sm" type="button" onclick="window.location.href = '{{ route('administrator.v1.products.warehouse.index')}}';">Batch List</button></div> --}}
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-12">
                <form id="batch" action="{{ route('administrator.v1.products.warehouse.reprintQrItem')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-1">
                          <label class="form-label">Start After</label>
                          <input type="text" name="start" max="111" class="form-control" placeholder="Start row" value="0">
                        </div>
                        <div class="form-group col-md-1">
                          <label class="form-label">Row</label>
                          <input type="text" name="row" max="15" class="form-control" placeholder="row" value="0">
                        </div>
                      </div>
                    <div class="form-group">
                        <label for="batchItem">Batch Item</label>
                        <select name="batchItem[]" class="slimselect" multiple>
                            <option value="">Please choose batch Item.</option>
                                @foreach($batch as $k => $v)
                                <option value="{{ $v->items_id }}">{{ $v->items_id }} </option>
                                @endforeach
                            </select>
                    </div>
            </div>
                    <hr>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit">Reprint</button>
                    </div>
                </form>
            </div>



        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
<script>
    // Parsley plugin initialization with tweaks to style Parsley for Bootstrap 4
    $("#batch").parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid',
        errorsWrapper: '<span class="form-text text-danger"></span>',
        errorTemplate: '<span></span>',
        trigger: 'change'
    });

 // Parsley full doc is avalailable here : https://github.com/guillaumepotier/Parsley.js/
</script>
<script>
    setTimeout(function() {
        new SlimSelect({
            select: '.slimselect'
        })
    }, 300)
</script>
@endpush
