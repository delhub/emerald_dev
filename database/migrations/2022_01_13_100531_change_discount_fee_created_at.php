<?php

use App\Models\Purchases\Purchase;
use App\Models\ShippingInstallations\Fee;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDiscountFeeCreatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_fee', function (Blueprint $table) {
            //
        });
        $discountFees = Fee::where('type','rebate_fee')->get();
        foreach ($discountFees as $discountFee) {
            $purchases = Purchase::where('id',$discountFee->purchase_id)->first();
            if ($purchases) {
                $discountFee->created_at =$purchases->created_at;
            $discountFee->save();
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_fee', function (Blueprint $table) {
            //
        });
    }
}
