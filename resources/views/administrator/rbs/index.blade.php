@extends('layouts.administrator.main')

@section('content')
@php

@endphp
@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif
<div class="row">
    <div class="col-6">
        <h1>RBS Options</h1>
    </div>
    <div class="offset-4 col-2">
        @if (env('APP_ENV') == 'local')
        <a href="{{ route('administrator.rbs.send.email') }}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark rbs-email">
            <span class="spinner-border spinner-border-sm spinner-rbs" style="display:none;" role="status" aria-hidden="true"></span>
            Send Email RBS</a>
        @endif
    </div>
</div>


<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-md-6 p-2">
                    <h3>RBS Frequency</h3>
                </div>
                <div class="col-md-6 text-right p-2">
                    <a href="{{ route('administrator.rbs.createEdit',['type'=>'frequency']) }}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New RBS Frequency</a>
                </div>
            </div>
        </div>

        <div class="table-responsive m-2">
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Frequency Label</td>
                        <td>Frequency Month</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                    @foreach($rbsFrequency as $frequency)

                    <tr>
                        <td style="width: 5%;">{{ $loop->iteration }}</td>

                        <td style="width: 20%;">{{ $frequency->rbs_label }}</td>

                        <td style="width: 20%;">{{  $frequency->rbs_data }}</td>

                        <td style="width: 20%;">

                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;"
                            href="{{ route('administrator.rbs.createEdit',['type'=>'frequency','id'=>$frequency->id]) }}" class="btn btn-primary shadow-sm">Edit</a>

                            @if($frequency->option_status == 'inactive')
                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;"
                            href="/administrator/rbs/enableRbs/{{$frequency->id}}" class="btn btn-info shadow-sm">Enable</a>
                            @endif

                            @if($frequency->option_status == 'active')
                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;"
                            href="/administrator/rbs/disableRbs/{{$frequency->id}}" class="btn btn-danger shadow-sm">Disable</a>
                            @endif

                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-md-6 p-2">
                    <h3>Times to send</h3>
                </div>
                <div class="col-md-6 text-right p-2">
                    <a href="{{ route('administrator.rbs.createEdit',['type'=>'times']) }}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create "New Times To Send" Data</a>
                </div>
            </div>
        </div>

        <div class="table-responsive m-2">
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>RBS Name</td>
                        <td>RBS Month</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                    @foreach($rbsTimeToSends as $rbsTimeToSend)

                    <tr>
                        <td style="width: 5%;">{{ $loop->iteration }}</td>

                        <td style="width: 20%;">{{ $rbsTimeToSend->rbs_label }}</td>

                        <td style="width: 20%;">{{  $rbsTimeToSend->rbs_data }}</td>

                        <td style="width: 20%;">

                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;"
                            href="{{ route('administrator.rbs.createEdit',['type'=>'times','id'=>$rbsTimeToSend->id]) }}" class="btn btn-primary shadow-sm">Edit</a>

                            @if($rbsTimeToSend->option_status == 'inactive')
                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;"
                            href="/administrator/rbs/enableRbs/{{$rbsTimeToSend->id}}" class="btn btn-info shadow-sm">Enable</a>
                            @endif

                            @if($rbsTimeToSend->option_status == 'active')
                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;"
                            href="/administrator/rbs/disableRbs/{{$rbsTimeToSend->id}}" class="btn btn-danger shadow-sm">Disable</a>
                            @endif

                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $( ".rbs-email" ).click(function() {
            $('.spinner-rbs').show();
        });
    </script>
@endpush