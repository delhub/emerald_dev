@extends('layouts.guest.main')

@section('content')


<div class="r-bg">

    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo" src="{{ asset("images/logo/formula-logo.png") }}" alt="">
        </div>
    </div>

    <div>

        <div class="my-register-box guests-card">
            <div class="my-r-title-box">
                <h5>Agent Registration</h5>
            </div>

            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link register-tab-active active " id="home-tab" data-toggle="tab" href="#registration"
                        role="tab" aria-controls="registration" aria-selected="true">Registration</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link register-tab-active " id="profile-tab" data-toggle="tab" href="#information"
                        role="tab" aria-controls="profile" aria-selected="false">Introducer Information</a>
                </li>
            </ul>

            <div class="card-body ">
                <!-- Dealer Registration Form -->
                <form method="POST" action="{{ route('guest.register.agent.post-sg') }}" id="register-form" enctype="multipart/form-data">
                    {{--<div class="tab-content" id="myTabContent">
                                            <!-- Registration  Tab-->
                                            <div class="r-subt-box tab-pane fade show active" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                                                <h5>Account Particulars</h5>
                                                <div class="register-info form-row">

                                                    --}}{{-- <div class="form-group col-md-12"> --}}{{--
                                                        --}}{{-- <label for="validate-customer">Existing Bujishu Customer/Agent?</label> --}}{{--
                                                        --}}{{-- <div class="validate-customer"> --}}{{--
                                                            --}}{{-- <div class="form-check form-check-inline"> --}}{{--
                                                                <input class="form-check-input" type="hidden" name="bjs_customer"
                                                                    id="bjs_customer_1" value="1" @if(old('bjs_customer')=='1' ) checked
                                                                    @endif>
                                                                --}}{{-- <label class="form-check-label" for="bjs_customer">Yes</label> --}}{{--
                                                            --}}{{-- </div> --}}{{--
                                                            --}}{{-- <div class="form-check form-check-inline"> --}}{{--
                                                                <input class="form-check-input" type="hidden" name="bjs_customer"
                                                                    id="bjs_customer_0" value="0" @if(old('bjs_customer')==null ||
                                                                    old('bjs_customer')=='0' ) checked @endif>
                                                                --}}{{-- <label class="form-check-label" for="bjs_customer">No</label> --}}{{--
                                                            --}}{{-- </div> --}}{{--
                                                        --}}{{-- </div> --}}{{--
                                                    --}}{{-- </div> --}}{{--


                                                    --}}{{-- <div class="form-group col-md-12">
                                                        <label for="invoice_number">Agent Package Invoice Number <small
                                                                class="text-danger">*</small></label>
                                                        <input type="text" name="invoice_number"
                                                            onchange="checkInvoice();"
                                                            class="form-control
                                                              @error('invoice_number') is-invalid @enderror"
                                                            id="invoice_number" placeholder="BJN2020XXXXXXX"
                                                            value="{{ old('invoice_number') }}">
                                                         @error('invoice_number')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror --
                                                    </div> --}}{{--

                                                    <div class="form-group  col-md-12 if-bjs" style="display:none">
                                                        <label for="bjs_customer_id"> Customer Email <small
                                                                class="text-danger">*</small></label>
                                                        <div class="input-group mb-0">
                                                            <input type="text" name="bjs_customer_id"
                                                                class="form-control @error('bjs_customer_id') is-invalid @enderror"
                                                                id="bjs_customer_id"
                                                                value="{{ old('bjs_customer_id') }}">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-outline-secondary" id="check_customer_id"
                                                                    onclick="checkCustomerID();" type="button">Check ID <span
                                                                        class="spinner-border spinner-border-sm" style="display: none;"
                                                                        role="status" aria-hidden="true"></span></button>

                                                            </div>
                                                        </div>

                                                        <div id="bjs_info">
                                                            @error('bjs_customer_id')
                                                            <small class="form-text text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="full_name">Full Name<small class="text-danger">*</small></label>
                                                        <input type="text" name="full_name"
                                                            class="form-control @error('full_name') is-invalid @enderror" id="full_name"
                                                            required placeholder="Full Name" value="{{ old('full_name') }}">
                                                        @error('full_name')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="contact_number_mobile">Contact Number (Mobile)<small class="text-danger">*</small></label>
                                                        <input type="text" name="contact_number_mobile" id="contact_number_mobile"
                                                            class="form-control @error('contact_number_mobile') is-invalid @enderror"
                                                            placeholder="Mobile Contact Number" value="{{ old('contact_number_mobile') }}">
                                                        @error('contact_number_mobile')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="address_1">Address Line 1<small class="text-danger">*</small></label>
                                                        <input type="text" name="address_1" id="address_1"
                                                            class="form-control @error('address_1') is-invalid @enderror"
                                                            placeholder="Residential Address Line 1" value="{{ old('address_1') }}">
                                                        @error('address_1')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="address_1">Address Line 2<small class="text-danger">*</small></label>
                                                        <input type="text" name="address_2" id="address_2"
                                                            class="form-control @error('address_2') is-invalid @enderror"
                                                            placeholder="Residential Address Line 2" value="{{ old('address_2') }}">
                                                        @error('address_2')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="address_1">Address Line 3<small class="text-danger">*</small></label>
                                                        <input type="text" name="address_3" id="address_3"
                                                            class="form-control @error('address_3') is-invalid @enderror"
                                                            placeholder="Residential Address Line 3" value="{{ old('address_3') }}">
                                                        @error('address_3')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="postcode">Postcode<small class="text-danger">*</small></label>
                                                        <input type="text" name="postcode" id="postcode"
                                                            class="form-control @error('postcode') is-invalid @enderror"
                                                            placeholder="Postcode" value="{{ old('postcode') }}">
                                                        @error('postcode')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-12 if-notbjs" id="class_city_key">
                                                        <label for="city_key">City<small class="text-danger">*</small></label>
                                                        <select name="city_key" id="city_key" class="form-control @error('city_key') is-invalid @enderror">
                                                            <option disabled selected>Choose your city..</option>
                                                            @foreach($cities as $city)
                                                            <option class="text-capitalize" value="{{ $city->city_key }}"
                                                                @if(old('city_key')==$city->city_key) selected @endif>{{ $city->city_name }}</option>
                                                            @endforeach
                                                            <option value="0">Others</option>
                                                        </select>
                                                        @error('city_key')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-6 if-notbjs" id="city">
                                                        <label for="city">Others City</label>
                                                        <input type="text" name="city"  class="form-control" value="" placeholder="Entry Your City Here">
                                                    </div>

                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="state">State</label>
                                                        <select name="state" id="state"
                                                            class="form-control @error('state') is-invalid @enderror">
                                                            @foreach($states as $state)
                                                            <option class="text-capitalize" value="{{ $state->id }}"
                                                                @if(old('state')==$state->id) selected @endif>{{ $state->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="email">Email <small class="text-danger">*</small></label>
                                                        <input type="email" name="email"
                                                            class="form-control @error('email') is-invalid @enderror" id="email"
                                                            placeholder="Your Email Address" value="{{ old('email') }}">
                                                        @error('email')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label for="password">Password <small class="text-danger">*</small></label>
                                                        <input type="password" name="password" class="form-control" id="password">
                                                        @error('password')
                                                        <small class="form-text text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-12 if-notbjs">
                                                        <label for="password-confirm">Confirm Password <small
                                                                class="text-danger">*</small></label>
                                                        <input id="password-confirm" type="password" class="form-control"
                                                            name="password_confirmation" autocomplete="new-password">
                                                    </div>
                                                </div>

                                                <!-- Next Button -->
                                                <div class="r-next-btn">
                                                    <a class="btn r-color-btn "id="next-btn"><b>Next</b></a>
                                                </div>

                                                <div id="error-div-1" class="row mt-2" style="display:none">
                                                    <div class="col-12">
                                                        <p class="text-danger">Please make sure all fields are filled in.</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Information Tab -->
                                            <div class="tab-pane fade" id="information" role="tabpanel" aria-labelledby="information-tab">
                                                <!-- Personal Particulars -->
                                                <div class="r-subt-box tab-pane fade show active" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                                                    <h5>Introducer Information</h5>
                                                </div>
                                                --}}{{-- <h5 class="text-center"
                                                    style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;"> --}}{{--

                                                <div class="form-row">
                                                    <div class="col-12 col-md-6 form-group">
                                                        <label for="referrer_name">Introducer Name</label>
                                                        <input type="text" name="referrer_name" id="referrer_name" placeholder="(Optional)" class="form-control" value="{{ old('referrer_name') }}">

                                                    </div>
                                                    <div class="col-12 col-md-6 form-group">
                                                        <label for="referrer_id">Introducer ID </label>
                                                        <input type="text" name="referrer_id" id="referrer_id" placeholder="(Optional)"
                                                            class="form-control  @error('referrer_id') is-invalid @enderror "
                                                            value="{{ old('referrer_id') }}">

                                                        @error('referrer_id')
                                                        <small class="form-text text-danger"><i class="fa fa-times"> </i>
                                                            {{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-12 form-group">
                                                        <label for="agent_group">Agent Group<small class="text-danger">*</small></label>
                                                        <select name="agent_group" id="agent_group" class="form-control">
                                                            <option value="">Select a group..</option>
                                                            <optgroup label="Kuala Lumpur">
                                                                <option value="1" @if ("1"==old('agent_group')) selected="selected" @endif>
                                                                    Chiew Wei Ping @ Alex</option>
                                                                <option value="2" @if ("2"==old('agent_group')) selected="selected" @endif>
                                                                    Chow Siok Koon @ Tiffany</option>
                                                                <option value="3" @if ("3"==old('agent_group')) selected="selected" @endif>
                                                                    Kong Kim Leng @ June</option>
                                                                <option value="4" @if ("4"==old('agent_group')) selected="selected" @endif>
                                                                    Liew Wei Chyan @ Dave</option>
                                                                <option value="5" @if ("5"==old('agent_group')) selected="selected" @endif>
                                                                    Yeap Lay Yan</option>
                                                            </optgroup>
                                                            <optgroup label="Penang">
                                                                <option value="6" @if ("6"==old('agent_group')) selected="selected" @endif>
                                                                    Lee Kean Guan @ Sampson</option>
                                                                <option value="7" @if ("7"==old('agent_group')) selected="selected" @endif>
                                                                    Ch'ng Saw Boay @ May</option>
                                                                <option value="8" @if ("8"==old('agent_group')) selected="selected" @endif>
                                                                    Khong Bee Kim @ Kim</option>
                                                                <option value="9" @if ("9"==old('agent_group')) selected="selected" @endif>
                                                                    Tan Teng Siew @ Darren</option>
                                                                <option value="10" @if ("10"==old('agent_group')) selected="selected"
                                                                    @endif>Tang Hui Chin</option>
                                                                <option value="11" @if ("11"==old('agent_group')) selected="selected"
                                                                    @endif>NDV</option>
                                                            </optgroup>
                                                             <optgroup label="Singapore">
                                                                <option value="6" @if ("12"==old('agent_group')) selected="selected" @endif>
                                                                    Singapore Group</option>

                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <!--
                                                <div class="card p-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="package1" name="package" value="1" class="custom-control-input" checked>
                                                        <label class="custom-control-label" for="package1">
                                                            <p class="font-weight-bold">配套1 (DC客戶申請代理)</p>
                                                            <div class="row no-gutters">
                                                                <div class="col-4 col-md-3 px-1">
                                                                    <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-1.jpg') }}" alt="Package 2">
                                                                </div>
                                                                <div class="col-8 col-md-9 px-1">
                                                                    <ol class="px-3 py-0 list-unstyled">
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                                                RM 6,090
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                                                1,000
                                                                                <span>
                                                                                    <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                                                </span>
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: .8rem; color: #ffffff;">
                                                                                RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX
                                                                            </p>
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="card p-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="package2" name="package" value="2" class="custom-control-input">
                                                        <label class="custom-control-label" for="package2">
                                                            <p class="font-weight-bold">配套2 (PJA申請代理)</p>
                                                            <div class="row no-gutters">
                                                                <div class="col-4 col-md-3 px-1">
                                                                    <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-2.jpg') }}" alt="Package 2">
                                                                </div>
                                                                <div class="col-8 col-md-9 px-1">
                                                                    <ol class="px-3 py-0 list-unstyled">
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                                                RM 6,090
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                                                3,000
                                                                                <span>
                                                                                    <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                                                </span>
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: .8rem;">
                                                                                *特別奬勵：90天完成 RM29,000 指定生意
                                                                                <br>
                                                                                額外獲得 RM1,000 現金花紅
                                                                            </p>
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="card p-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="package3" name="package" value="3" class="custom-control-input">
                                                        <label class="custom-control-label" for="package3">
                                                            <p class="font-weight-bold">配套3 A級創利方案 (DC或POR代理)</p>
                                                            <div class="row no-gutters">
                                                                <div class="col-4 col-md-3 px-1">
                                                                    <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-3.jpg') }}" alt="Package 3">
                                                                </div>
                                                                <div class="col-8 col-md-9 px-1">
                                                                    <ol class="px-3 py-0 list-unstyled">
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                                                RM 7,090
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                                                7,000
                                                                                <span>
                                                                                    <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                                                </span>
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: .8rem;">
                                                                                *特別奬勵：60天內完成 RM19,000指定生意
                                                                                <br>
                                                                                額外獲得 RM1,000 現金花紅
                                                                            </p>
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="card p-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="package4" name="package" value="4" class="custom-control-input">
                                                        <label class="custom-control-label" for="package4">
                                                            <p class="font-weight-bold">配套3 A+級擢昇方案 (DC或POR代理)</p>
                                                            <div class="row no-gutters">
                                                                <div class="col-4 col-md-3 px-1">
                                                                    <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-4.jpg') }}" alt="Package 4">
                                                                </div>
                                                                <div class="col-8 col-md-9 px-1">
                                                                    <ol class="px-3 py-0 list-unstyled">
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                                                RM 17,090
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                                                17,000
                                                                                <span>
                                                                                    <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                                                </span>
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: .8rem;">
                                                                                *特別奬勵：90天完成 RM39,000 指定生意
                                                                                <br>
                                                                                額外獲得 RM2,500 現金花紅
                                                                                <br>
                                                                                <span class="text-danger">僅限99個位子</span>
                                                                            </p>
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="card p-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="package5" name="package" value="5" class="custom-control-input">
                                                        <label class="custom-control-label" for="package5">
                                                            <p class="font-weight-bold">配套3 A++領導方案 (DC或POR代理)</p>
                                                            <div class="row no-gutters">
                                                                <div class="col-4 col-md-3 px-1">
                                                                    <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-5.jpg') }}" alt="Package 5">
                                                                </div>
                                                                <div class="col-8 col-md-9 px-1">
                                                                    <ol class="px-3 py-0 list-unstyled">
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                                                RM 38,090
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                                                38,000
                                                                                <span>
                                                                                    <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                                                </span>
                                                                            </p>
                                                                        </li>
                                                                        <li>
                                                                            <p class="mb-0" style="font-size: .8rem;">
                                                                                *特別奬勵：90天完成 RM59,000 指定生意
                                                                                <br>
                                                                                額外獲得 RM4,000 現金花紅
                                                                                <br>
                                                                                <span class="text-danger">僅限50個位子</span>
                                                                            </p>
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                    -->

                                                <!-- <div class="form-row mt-2" id="two-checkbox"> -->
                                                 <div class="form-row mt-2">
                                                    <div class="col-12" style="padding-left: .7rem;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="credit-debit-agree">
                                                            <label class="custom-control-label" for="credit-debit-agree">
                                                                I have read, understand and accepted
                                                                the <a href="" data-toggle="modal"
                                                                    data-target="#purchaseAgreementModal">terms and conditions</a>.
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row mt-2">
                                                    <div class="col-12" style="padding-left: .7rem;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="compliance">
                                                            <label class="custom-control-label" for="compliance">
                                                                I will comply and ensure strict compliance with guidelines as set by
                                                                Formula Healthcare Sdn Bhd.
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- </div> -->

                                                 <div class="r-next-btn">
                                                    <input type="hidden" name="package" value="10" id="package">

                                                    <a class="btn r-color-btn "id="next-btn"><b>Next</b></a>
                                                </div>
                                                <br>
                                                <div class="edit-opt-btn" style="text-align: right;">
                                                    <input type="hidden" name="package" value="10" id="package">
                                                    <button type="submit" class="cancel-btn btn-primary" id="pay-now-button" disabled>Sign Up</button>
                                                </div>

                                                 <div class="r-next-btn">
                                                    <!-- Submit button -->
                                                    <input type="hidden" name="package" value="10" id="package">
                                                    <button type="submit"
                                                        class="btn r-color-btn"
                                                        disabled>Sign Up</button>
                                                </div>

                                                <div id="error-div-2" class="row mt-2" style="display:none">
                                                    <div class="col-12">
                                                        <p class="text-danger">Please make sure all fields are filled in.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--}}
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <!-- Registration  Tab-->
                        <div class="r-subt-box tab-pane fade show active" id="registration" role="tabpanel"
                             aria-labelledby="registration-tab">

                            <h5>Account Particulars</h5>
                            <div class="form-row">

                                <div class="form-group col-md-12">
                                    <label for="validate-customer">Existing Formula customer?</label>
                                    <div class="validate-customer">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="bjs_customer"
                                                   id="bjs_customer_1" value="1" @if(old('bjs_customer')=='1' ) checked
                                                @endif>
                                            <label class="form-check-label" for="bjs_customer">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="bjs_customer"
                                                   id="bjs_customer_0" value="0" @if(old('bjs_customer')==null ||
                                                old('bjs_customer')=='0' ) checked @endif>
                                            <label class="form-check-label" for="bjs_customer">No</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group col-md-12">
                                    <label for="invoice_number">Agent Package Invoice Number <small
                                            class="text-danger">*</small></label>
                                    <input type="text" name="invoice_number"
                                           onchange="checkInvoice();"
                                           class="form-control @error('invoice_number') is-invalid @enderror"
                                           id="invoice_number" placeholder="FWSXX-XXXXXX"
                                           value="{{ old('invoice_number') }}">
                                    @error('invoice_number')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group  col-md-12 if-bjs" style="display:none">
                                    <label for="bjs_customer_id">Formula Customer Email <small
                                            class="text-danger">*</small></label>
                                    <div class="input-group mb-0">
                                        <input type="text" name="bjs_customer_id"
                                               class="form-control @error('bjs_customer_id') is-invalid @enderror"
                                               id="bjs_customer_id"
                                               value="{{ old('bjs_customer_id') }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" id="check_customer_id"
                                                    onclick="checkCustomerID();" type="button">Check ID <span
                                                    class="spinner-border spinner-border-sm" style="display: none;"
                                                    role="status" aria-hidden="true"></span></button>

                                        </div>
                                    </div>

                                    <div id="bjs_info">
                                        @error('bjs_customer_id')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group col-md-12 if-notbjs">
                                    <label for="email">Email <small class="text-danger">*</small></label>
                                    <input type="email" name="email"
                                           class="form-control @error('email') is-invalid @enderror" id="email"
                                           placeholder="Your Email Address" value="{{ old('email') }}">
                                    @error('email')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="password">Password <small class="text-danger">*</small></label>
                                    <input type="password" name="password" class="form-control" id="password">
                                    @error('password')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12 if-notbjs">
                                    <label for="password-confirm">Confirm Password <small
                                            class="text-danger">*</small></label>
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" autocomplete="new-password">
                                </div>


                            <div class="form-group col-md-6 if-notbjs">
                                <label for="full_name">Full Name<small class="text-danger">*</small></label>
                                <input type="text" name="full_name"
                                       class="form-control @error('full_name') is-invalid @enderror" id="full_name"
                                       required placeholder="Full Name" value="{{ old('full_name') }}">
                                @error('full_name')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-md-6 if-notbjs">
                                <label for="contact_number_mobile">Contact Number (Mobile)<small class="text-danger">*</small></label>
                                <input type="text" name="contact_number_mobile" id="contact_number_mobile"
                                       class="form-control @error('contact_number_mobile') is-invalid @enderror"
                                       placeholder="Mobile Contact Number" value="{{ old('contact_number_mobile') }}">
                                @error('contact_number_mobile')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-md-12 if-notbjs">
                                <label for="address_1">Address Line 1<small class="text-danger">*</small></label>
                                <input type="text" name="address_1" id="address_1"
                                       class="form-control @error('address_1') is-invalid @enderror"
                                       placeholder="Residential Address Line 1" value="{{ old('address_1') }}">
                                @error('address_1')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12 if-notbjs">
                                <label for="address_1">Address Line 2<small class="text-danger">*</small></label>
                                <input type="text" name="address_2" id="address_2"
                                       class="form-control @error('address_2') is-invalid @enderror"
                                       placeholder="Residential Address Line 2" value="{{ old('address_2') }}">
                                @error('address_2')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12 if-notbjs">
                                <label for="address_1">Address Line 3<small class="text-danger">*</small></label>
                                <input type="text" name="address_3" id="address_3"
                                       class="form-control @error('address_3') is-invalid @enderror"
                                       placeholder="Residential Address Line 3" value="{{ old('address_3') }}">
                                @error('address_3')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-md-12 if-notbjs">
                                <label for="postcode">Postcode<small class="text-danger">*</small></label>
                                <input type="text" name="postcode" id="postcode"
                                       class="form-control @error('postcode') is-invalid @enderror"
                                       placeholder="Postcode" value="{{ old('postcode') }}">
                                @error('postcode')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-md-12 if-notbjs" id="class_city_key">
                                <label for="city_key">City<small class="text-danger">*</small></label>
                                <select name="city_key" id="city_key" class="form-control @error('city_key') is-invalid @enderror">
                                    <option disabled selected>Choose your city..</option>
                                    @foreach($cities as $city)
                                        <option class="text-capitalize" value="{{ $city->city_key }}"
                                                @if(old('city_key')==$city->city_key) selected @endif>{{ $city->city_name }}</option>
                                    @endforeach
                                    <option value="0">Others</option>
                                </select>
                                @error('city_key')
                                <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-md-6 if-notbjs" id="city">
                                <label for="city">Others City</label>
                                <input type="text" name="city"  class="form-control" value="" placeholder="Entry Your City Here">
                            </div>

                            <div class="form-group col-md-12 if-notbjs">
                                <label for="state">State</label>
                                <select name="state" id="state"
                                        class="form-control @error('state') is-invalid @enderror">
                                    @foreach($states as $state)
                                        <option class="text-capitalize" value="{{ $state->id }}"
                                                @if(old('state')==$state->id) selected @endif>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>

                            <!-- Next Button -->
                            <div class="r-next-btn">
                                <a class="btn r-color-btn" id="next-btn"><b>Next</b></a>
                            </div>

                            <div id="error-div-1" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Information Tab -->
                        <div class="r-subt-box tab-pane fade" id="information" role="tabpanel" aria-labelledby="information-tab">
                            <!-- Personal Particulars -->
                            <h5>Introducer Information</h5>
                            <div class="form-row">
                                <div class="col-12 col-md-6 form-group">
                                    <label for="referrer_name">Introducer Name <small
                                            class="text-danger">*</small></label>

                                    <input type="text" name="referrer_name" id="referrer_name" class="form-control" value="{{ old('referrer_name') }}">

                                </div>
                                <div class="col-12 col-md-6 form-group">
                                    <label for="referrer_id">Introducer ID <small class="text-danger">*</small></label>
                                    <input type="text" name="referrer_id" id="referrer_id"
                                           class="form-control  @error('referrer_id') is-invalid @enderror "
                                           value="{{ old('referrer_id') }}">

                                    @error('referrer_id')
                                    <small class="form-text text-danger"><i class="fa fa-times"> </i>
                                        {{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-12 form-group">
                                    <label for="agent_group">Agent Group <small class="text-danger">*</small></label>
                                    <select name="agent_group" id="agent_group" class="form-control">
                                        <option value="">Select a group..</option>
                                        <optgroup label="Kuala Lumpur">
                                            <option value="1" @if ("1"==old('agent_group')) selected="selected" @endif>
                                                Chiew Wei Ping @ Alex</option>
                                            <option value="2" @if ("2"==old('agent_group')) selected="selected" @endif>
                                                Chow Siok Koon @ Tiffany</option>
                                            <option value="3" @if ("3"==old('agent_group')) selected="selected" @endif>
                                                Kong Kim Leng @ June</option>
                                            <option value="4" @if ("4"==old('agent_group')) selected="selected" @endif>
                                                Liew Wei Chyan @ Dave</option>
                                            <option value="5" @if ("5"==old('agent_group')) selected="selected" @endif>
                                                Yeap Lay Yan</option>
                                        </optgroup>
                                        <optgroup label="Penang">
                                            <option value="6" @if ("6"==old('agent_group')) selected="selected" @endif>
                                                Lee Kean Guan @ Sampson</option>
                                            <option value="7" @if ("7"==old('agent_group')) selected="selected" @endif>
                                                Ch'ng Saw Boay @ May</option>
                                            <option value="8" @if ("8"==old('agent_group')) selected="selected" @endif>
                                                Khong Bee Kim @ Kim</option>
                                            <option value="9" @if ("9"==old('agent_group')) selected="selected" @endif>
                                                Tan Teng Siew @ Darren</option>
                                            <option value="10" @if ("10"==old('agent_group')) selected="selected"
                                                @endif>Tang Hui Chin</option>
                                            <option value="11" @if ("11"==old('agent_group')) selected="selected"
                                                @endif>NDV</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <hr>
                        <!--
                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package1" name="package" value="1" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="package1">
                                        <p class="font-weight-bold">配套1 (DC客戶申請代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-1.jpg') }}" alt="Package 2">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 6,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            1,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem; color: #ffffff;">
                                                            RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package2" name="package" value="2" class="custom-control-input">
                                    <label class="custom-control-label" for="package2">
                                        <p class="font-weight-bold">配套2 (PJA申請代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-2.jpg') }}" alt="Package 2">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 6,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            3,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM29,000 指定生意
                                                            <br>
                                                            額外獲得 RM1,000 現金花紅
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package3" name="package" value="3" class="custom-control-input">
                                    <label class="custom-control-label" for="package3">
                                        <p class="font-weight-bold">配套3 A級創利方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-3.jpg') }}" alt="Package 3">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 7,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            7,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：60天內完成 RM19,000指定生意
                                                            <br>
                                                            額外獲得 RM1,000 現金花紅
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package4" name="package" value="4" class="custom-control-input">
                                    <label class="custom-control-label" for="package4">
                                        <p class="font-weight-bold">配套3 A+級擢昇方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-4.jpg') }}" alt="Package 4">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 17,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            17,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM39,000 指定生意
                                                            <br>
                                                            額外獲得 RM2,500 現金花紅
                                                            <br>
                                                            <span class="text-danger">僅限99個位子</span>
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package5" name="package" value="5" class="custom-control-input">
                                    <label class="custom-control-label" for="package5">
                                        <p class="font-weight-bold">配套3 A++領導方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-5.jpg') }}" alt="Package 5">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 38,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            38,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM59,000 指定生意
                                                            <br>
                                                            額外獲得 RM4,000 現金花紅
                                                            <br>
                                                            <span class="text-danger">僅限50個位子</span>
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                -->

                            <!-- <div class="form-row mt-2" id="two-checkbox"> -->
                            <div class="form-row mt-2">
                                <div class="col-12" style="padding-left: .7rem;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="credit-debit-agree">
                                        <label class="custom-control-label" for="credit-debit-agree">
                                            I have read, understand and accepted
                                            the <a href="" data-toggle="modal"
                                                   data-target="#purchaseAgreementModal">terms and conditions</a>.
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mt-2">
                                <div class="col-12" style="padding-left: .7rem;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="compliance">
                                        <label class="custom-control-label" for="compliance">
                                            I will comply and ensure strict compliance with guidelines as set by
                                            Formula.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->

                            {{--<div class="col-12 edit-opt-btn text-right">
                                <!-- Submit button -->
                                <input type="hidden" name="package" value="10" id="package">
                                <button type="submit" id="pay-now-button"
                                        class="cancel-btn btn-primary"
                                        disabled>Sign Up</button>
                            </div>--}}
                            <div class="edit-opt-btn text-right">
                                <input type="hidden" name="package" value="10" id="package">
                                <button type="submit" class="cancel-btn btn-primary" id="pay-now-button" disabled>
                                    Sign Up
                                </button>
                            </div>

                            <div id="error-div-2" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="purchaseAgreementModal" tabindex="-1" role="dialog" aria-labelledby="purchaseAgreementModal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-12 mb-0">
                        <div class="overflow-auto"
                            style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                            @include('shop.payment.service-terms-conditions-sg')
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="emailErrorModal" tabindex="-1" role="dialog" aria-labelledby="emailErrorModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="emailErrorModalLabel">Email Is Currently Used</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.1rem;">
                    The email you entered has been used.
                    <br>
                    <br>
                    Please send an email to
                    <br>
                    <a style="font-size: 1.15rem; font-weight: 600;"
                        href="mailto:formula2u-cs@delhubdigital.com">formula2u-cs@delhubdigital.com</a> for further
                    assistance.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>

    /* City */
    const cityKey = document.getElementById("city_key");
    // var city = document.getElementsByClassName("city");
    const city = document.getElementById("city");

    const classCityKey = document.getElementById("class_city_key");

    city.style.display = "none";
    displayCity();

    $('#city_key').on('change', function( ) {
        displayCity();
    });

    $("#state").on("change",function(){
        const variableID = $(this).val();

        if(variableID){
            $.ajax({
                type:"POST",
                url:'{{route("guest.register.customer.state-filter-city")}}',
                data:$("#register-form").serialize(),
                success:function(toajax){
                    $("#city_key").html(toajax);
                    displayCity();
                }
            });
        }

    });

    function displayCity(){

        if(cityKey.value === '0'){
            city.style.display = "block";
            classCityKey.className ="col-12 col-md-6 form-group if-notbjs"
        }else {
            city.style.display = "none";
            classCityKey.className ="col-12 col-md-12 form-group if-notbjs";
        }
    }
    /* End City */


    bjsCustomerSelect();

    function bjsCustomerSelect() {

    bjs_customer = $('input[name="bjs_customer"]:checked').val();

        if (bjs_customer === '1') {

            $('.if-bjs').show();
            $('.if-notbjs').hide();
            displayCity();
            $('label[for="password"]').html('Existing  Password<small class="text-danger">*</small>');
        } else {

            $('.if-bjs').hide();
            $('.if-notbjs').show();
            displayCity();
             $('label[for="password"]').html('Password<small class="text-danger">*</small>');
        }

    }
    $('input[name="bjs_customer"]').click(bjsCustomerSelect);

    @error('email')
    $('#emailErrorModal').modal('show');
    @enderror

    let priceSelectors = document.getElementsByClassName('input-mask-price');

    let priceInputMask = new Inputmask({
        'mask': '99999.99',
        'numericInput': true,
        'digits': 2,
        'digitsOptional': false,
        'placeholder': '0'
    });

    for (var i = 0; i < priceSelectors.length; i++) {
        priceInputMask.mask(priceSelectors.item(i));
    }

    $('#introducer_name').on('keyup', function() {
        if ($(this).val().length === 0 || $(this).val() === '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#introducer_id').on('keyup', function() {
        if ($(this).val().length < 10 || $(this).val() === '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#agent_group').on('change', function() {
        if ($(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    let currentTab = $('.nav-tabs > .active');
    let nextTab = currentTab.next('li');

    // Handles tabs click.
    $('.nav-link').click(function() {
        currentTab = $(this).parent();
        $('.nav-tabs > .active').removeClass('active');
        currentTab.addClass('active');
        nextTab = currentTab.next('li');
    });

    // Form Validation
    $("#register-form").validate({
        rules: {
            full_name: {
                required: true,
            },
            address_1: {
                required: true,
                minlength: 3
            },

            postcode: {
                required: true,
                // postcode: true
            },
            city_key: {
                required: true
            },
            city: {
                required: true
            },
            state: {
                required: true
            },
            contact_number_mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 15
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8,
            },
            password_confirmation: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            },
            referrer_id: {
                digits:true
            }
        },
        messages: {
            // invoice_number: {
            //     required: "Please enter an invoice number.",
            //     email: "The invoice number is not valid."
            // },
            full_name: {
                required: "Please enter an full name.",
                email: "wrong name."
            },
            address_1: {
                required: "Please enter your address"
            },

            postcode: {
                required: "Please enter your postcode"
            },
            city_key: {
                required: "Please select your city"
            },
            city: {
                required: "Please select your city"
            },
            state: {
                required: "Please select your state"
            },
            contact_number_mobile: {
                required: "Please enter your mobile number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            },
            email: {
                required: "Please enter an email.",
                email: "The email is not valid."
            },
            password: {
                required: "Please enter a password.",
                minlength: "Password must be minimum of 8 characters."
            },
            password_confirmation: {
                required: "Please confirm your password",
                minlength: "Password must must be minimum of 8 characters",
                equalTo: "Password must be same as above"
            },
            agent_group:{
                 required: "Please select your group"
            }
        }
    });

    // Validate fields in 1st tab.
    $('#next-btn').click(function() {
        if (
            // $("#register-form").validate().element('#invoice_number') &&
            $("#register-form").validate().element('#full_name') &&
            $("#register-form").validate().element('#contact_number_mobile') &&
            $("#register-form").validate().element('#address_1') &&
            $("#register-form").validate().element('#postcode') &&
            $("#register-form").validate().element('#city_key') &&
            // $("#register-form").validate().element('#city') &&
            $("#register-form").validate().element('#state') &&
            $("#register-form").validate().element('#email') &&
            $("#register-form").validate().element('#password') &&
            $("#register-form").validate().element('#password-confirm')
        ) {
            $('#error-div-1').hide();
            nextTab.find('a').trigger('click');
        } else {
            $('#error-div-1').show();
        }
    });

    // Validate fields in 2nd tab.
    $('#next-btn2').click(function() {
        if (
            $('#introducer_name').val() != '' &&
            $('#agent_group').val() != 'default'
        ) {
            $('#error-div-2').hide();
            nextTab.find('a').trigger('click');
        } else {
            $('#error-div-2').show();
            $('#introducer_name').addClass('is-invalid');
            $('#agent_group').addClass('is-invalid');
        }
    });



    // Validate form before submit.
    $('#register-form').on('submit', function(e) {
        let error = 0;

        // if (
        //     $("#register-form").validate().element('#invoice_number')
        // ) {
        //     error = error;
        // } else {
        //     error = error + 1;
        // }

        if (
            $("#register-form").validate().element('#full_name')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#register-form").validate().element('#email')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#register-form").validate().element('#password')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (

            $("#register-form").validate().element('#password-confirm')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $('#introducer_name').val().length == 0 || $('#introducer_name').val() == ''
        ) {
            error = error + 1;
            $('#introducer_name').addClass('is-invalid');
        }

        if ($('#agent_group').val() == 'default') {
            error = error + 1;
            $('#agent_group').addClass('is-invalid');
        }

        if (error == 0) {
            $('#error-div-3').hide();
            return true;
        } else {
            $('#error-div-3').show();
            return false;
        }
    });
    // End Form Validation

    //1
    // $('#credit-debit-agree').on('change', function() {
        // if ($('#credit-debit-agree').prop('checked', true)) {
            //$('#pay-now-button').prop('disabled', false);

            $('#compliance').on('change', function() {
             if ($('#compliance').prop('checked', true)) {
                 $('#pay-now-button').prop('disabled', false);
             } else {
                 $('#pay-now-button').prop('disabled', true);
             }
            });

        // } else {
        //     $('#pay-now-button').prop('disabled', true);
        // }
    // });

    // $('#compliance').on('change', function() {
    //     if ($('#compliance').prop('checked', true)) {
    //         //$('#pay-now-button').prop('disabled', false);

    //         $('#credit-debit-agree').on('change', function() {
    //          if ($('#credit-debit-agree').prop('checked', true)) {
    //              $('#pay-now-button').prop('disabled', false);
    //          } else {
    //              $('#pay-now-button').prop('disabled', true);
    //          }
    //         });

    //     } else {
    //         $('#pay-now-button').prop('disabled', true);
    //     }
    // });


    function checkCustomerID(obj){

        let button = $(obj);
        let customer_id =  $('#bjs_customer_id').val();
        let loader = $('.spinner-border');

        //let count = button.find('.liked-btn');
        $.ajax({
            async: true,
            beforeSend: function() {
                loader.show();
            },
            complete: function() {
                loader.hide();
            },
            url: '{{route("guest.check.customerid")}}',
            type: "POST",
            data: {
                id: customer_id,
                _token: "{{ csrf_token() }}"
            },
            success: function(result) {
                $('#bjs_info').html('<small class="text-uppercase text-primary">' + result.data + '</small>');
                $('#bjs_customer_id').removeClass('is-invalid');
            },
            error: function(result) {

                $('#bjs_info').html('<small class="form-text text-danger" >'+result.responseJSON.message+'.</small>');
                $('#bjs_customer_id').addClass('is-invalid');

            }
        });
    }



    function checkInvoice(obj){

    let input = $(obj);
    let invoice_number =  $('#invoice_number').val();
    let loader = $('.spinner-border');

    //let count = button.find('.liked-btn');
    $.ajax({
            async: true,
            beforeSend: function() {
                loader.show();
            },
            complete: function() {
                loader.hide();
            },
            url: '{{route("guest.get.invoice")}}',
            type: "POST",
            data: {
                invoice: invoice_number,
                _token: "{{ csrf_token() }}"
            },
            success: function(result) {

                const a = result.dealer_info;
                const b = a.agent_group;

                // console.log(result);
                // console.log(result.buyer_id);
                // console.log(a.full_name);
                // console.log(a.referrer_id);
                // console.log(b.group_name);
                // console.log(a.group_id);

                // js get array
                // $('#bjs_info').html('<small class="text-uppercase text-primary">' + result.data + '</small>');
                // $('#bjs_customer_id').removeClass('is-invalid');

                //add introducer_id & agent_group

                // document.getElementById("referrer_name").value = result['buyer_id'];
                document.getElementById("referrer_name").value = a.full_name;
                document.getElementById("referrer_id").value = a.account_id;
                document.getElementById("agent_group").value =  a.group_id;

            }
        });
    }
</script>
@endpush

@push('style')
<style>
    .btn.btn-outline-secondary {
        height: auto;
        margin-left: 0.1rem;
        width: auto;
        border: 1px solid #6c757d;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        font-size: smaller;
    }
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .error {
        color: red;
    }
</style>
@endpush

@push('script')

@endpush
