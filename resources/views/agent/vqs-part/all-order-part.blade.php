<!-- Orders -->
@php
use App\Models\Purchases\Order;

$allTaskLists = [];
foreach ($taskLists as $taskList) {
    $order = Order::where('delivery_order', $taskList->delivery_order)->first();

    if ($taskList->prebuy_redemption == 0 && count($order->items) < 2) {
        $allTaskLists[] = $taskList;
    } else {
        $show = 1;

        foreach ($order->items as $key => $value) {
            $archimedes = DB::table('items as i')
                ->join('panel_attribute_bundle as b', 'b.primary_product_code', '=', 'i.product_code')
                ->join('panel_product_attributes as a', 'a.product_code', '=', 'b.product_code')
                ->join('panel_products as d', 'd.id', '=', 'a.panel_product_id')
                ->join('global_products as g', 'g.id', '=', 'd.global_product_id')
                ->where('i.product_code', $value->product_code)
                ->whereIn('i.item_order_status', [1001, 1008, 1009, 1011])
                ->first();

            if (!isset($archimedes)) {
                $show = 0;
                break;
            }
        }

        if ($show == 1 && $taskList->prebuy_redemption == 0) {
            $allTaskLists[] = $taskList;
        }
    }
}

@endphp

<div class="card">
    <h4 class="mt-3 ml-3">Redeem available :</h4>

    <div class="card-body">
        @foreach ($dealerDatas as $dealerData)
            {{ $loop->iteration }})
            @if ($productAttribute->where('product_code', $dealerData->name)->first())
                <img style="width: 15%;margin: 10px 8px;"
                    src="{{ asset('storage/' .$productAttribute->where('product_code', $dealerData->name)->first()->product2->parentProduct->images[0]->path .$productAttribute->where('product_code', $dealerData->name)->first()->product2->parentProduct->images[0]->filename) }}" />
            @else
                <img class="responsive-img p-1" src="" alt="Product Image" />
            @endif

            {{ $productAttribute->where('product_code', $dealerData->name)->first()->product2->parentProduct->name }}
            &nbsp;&nbsp;&nbsp; <b> X {{ $dealerData->value }}</b> quantity available.
            <br>
        @endforeach
    </div>

</div>

@if (count($allTaskLists) < 1)
    <div class="card my-2 px-lg-5  px-2">
        <div class="card-body row">
            <div class="col-auto mx-auto">
                <strong>
                    No Order Found.
                </strong>
            </div>
        </div>
    </div>
@else
    @foreach ($allTaskLists as $allTaskList)
        <div class="vr-or-dt mb-4">
            <div class="vr-box">
                <div class="purchase-list-t">
                    <h4>Purchase : <span>{{ $allTaskList->purchase_number }}</span></h4>
                </div>

                <div class="oderdate-t">
                    <h5>Order Date : <span>{{ $allTaskList->purchase_date }}</span></h5>
                </div>
                <div class="vr-ir-btn">
                    <input type="checkbox" class="cb-element largerCheckBox" style="width: 30px;height: 30px;"
                        name="itemsid[]" {{ $allTaskList->prebuy_redemption == 1 ? 'disabled' : '' }}
                        value="{{ $allTaskList->item_table_id }}" {{-- onclick="checkBalance(this);"
            {{ 'data-productCode=' . $taskList->product_code }}
            data-quantity="{{ $taskList->quantity }}" --}} />
                    {{-- {{ $taskList->item_table_id }}
            - {{ $taskList->product_code }} -
            {{ $taskList->subtotal_price }} --}}
                </div>
            </div>
            {{-- @foreach ($purchase->sortItems() as $key => $item)
    @if ($item->bundle_id == 0) --}}

            <div class="row vr-order-lst">
                <div class="col-md-7 vr-pt-m0">
                    <div class="row body-inner vr-i-align">
                        <div class="col-md-3 col-3">
                            @if ($allTaskList->items->product->parentProduct->defaultImage)
                                <img class="vr-p-img"
                                    src="{{ asset('storage/' .$allTaskList->items->product->parentProduct->images[0]->path .$allTaskList->items->product->parentProduct->images[0]->filename) }}" />
                            @else
                                <img class="imgproduct" src="{{ asset('/images/product_item/default-p.png') }}"
                                    alt="">
                            @endif
                        </div>
                        <div class="col-md-9 col-9 vr-p-d">
                            <a class="text-dark"
                                href="{{ $allTaskList->items->product->parentProduct->getUrlAttribute() }}">
                                <strong style="font-size:15px;" class="font-weight-bold">
                                    {{ $allTaskList->items->product->parentProduct->name }}
                                </strong>
                            </a>
                            <br>
                            Quantity : {{ $allTaskList->quantity }}
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-3 ">
                    <div class="vr-o-step row">
                        <div class="col-md-3 p-0">Purchaser :</div>
                        <div class="col-md-8 p-0">{{ $allTaskList->full_name }}</div>
                        <div class="col-md-3 p-0">Recipient :</div>
                        <div class="col-md-8 p-0">{{ $allTaskList->ship_full_name }}
                        </div>
                    </div>
                </div>

            </div>
            {{-- @endif

    @endforeach --}}
        </div>
    @endforeach

    {{-- select bulk action --}}
    <form id="bulkactionform" action={{ route('agent.vqs.createDealerPreBuyRedemption') }} method="POST">
        <div class="row mt-2 ml-2">
            <div class="col-md-auto px-1">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="action_ids" id="action_ids" value="">

                <div class="input-group mb-1">
                    <div class="input-group-append">
                        <input type="hidden" name="goSummaryPage" value="yeah">
                        <input type="submit" class="buy-btn link-product" value="Submit" id="btnid">
                    </div>
                </div>
            </div>
        </div>
    </form>
@endif
