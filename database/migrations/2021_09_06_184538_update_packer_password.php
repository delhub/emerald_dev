<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Users\User;
use Illuminate\Support\Facades\Hash;

class UpdatePackerPassword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $packer = User::where('email', 'packer1@email.com')->first();
        $packer->email = 'pack1@email.com';
        $packer->password = Hash::make('pack1234');
        $packer->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
