<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class MoveDiscountColumnToPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('discount_percentage');
        });

        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->integer('discount_percentage')->default(0)->after('point_cash');
        });


        DB::table('categories')
            ->where('name', 'Kids')
            ->where('type', 'suitable_for')
            ->update(
                ['name' => 'Kid'],
                ['slug' => 'kid']

            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
