<?php

namespace App\Models\Purchases;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class Counters extends Model
{
    // Set table
    protected $table = 'counters';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    //    protected $primaryKey = '';

    // Set incremeneting to false.
    public $incrementing = false;

    // Set mass assignable columns
    protected $fillable = [];

    /**
     * Get all items belongin to the order.
     */
    public static function autoIncrement($counterTypes, $length = 6, $dash = '-')
    {
        $country_id = country()->country_id;

        foreach ($counterTypes as $counterType) {

            $counterTable = self::where('counter_types', $counterType)
                ->where('country_id', $country_id)
                ->whereYear('updated_at', Carbon::now())
                ->first();

            if (!$counterTable) {
                $counterTable = self::setNew($counterType);
            }

            do {
                $currentValue = $counterTable->increment('counter_value');
            } while ($counterTable->counter_value < $currentValue);

            $purchaseNumber[$counterType] = $counterTable->counter_prefix . Carbon::now()->format('y') . $dash . str_pad($counterTable->counter_value, $length, "0", STR_PAD_LEFT);
        }

        return $purchaseNumber;
    }

    public static function setNew($counterType)
    {
        $country_id = country()->country_id;
        $newCounter = self::where('counter_types', $counterType)->where('country_id', $country_id)->first();
        $newCounter->counter_value = 0;
        $newCounter->save();
        return $newCounter;
    }

    public static function autoIncrementInv($counterTypes, $purchase)
    {
        if ($purchase->inv_date){
            $carbonDate = Carbon::createFromFormat('Y-m-d', $purchase->inv_date);
        } else {
            $carbonDate = Carbon::createFromFormat('d/m/Y', $purchase->purchase_date);
        }
        $counter_date = $carbonDate->format('ym');
        $country_id = $purchase->country_id;

        foreach ($counterTypes as $counterType) {

            $counterTable = self::where('counter_types', $counterType)
                ->where('country_id', $country_id)
                ->where('date_month', $counter_date)
                ->first();

            if (!$counterTable) {
                $counterTable = self::createNewInv($counterType, $country_id, $counter_date);
            }

            do {
                $currentValue = $counterTable->increment('counter_value');
            } while ($counterTable->counter_value <= $currentValue);

            switch ($counterType) {
                case 'invoice':
                    $purchaseNumber[$counterType] = $counterTable->counter_prefix . '-' . str_pad($counterTable->counter_value, 6, "0", STR_PAD_LEFT);
                    break;
            }
        }

        return $purchaseNumber;
    }

    public static function createNewInv($counterType, $country_id, $counter_date)
    {
        //create new
        $newCounter = new Counters();
        $newCounter->counter_types = $counterType;
        $newCounter->date_month = $counter_date;
        $newCounter->country_id = $country_id;
        $newCounter->counter_value = 0;
        $newCounter->counter_prefix = 'FWS' . $counter_date;
        $newCounter->counter_types = $counterType;
        $newCounter->save();

        return $newCounter;
    }
}
