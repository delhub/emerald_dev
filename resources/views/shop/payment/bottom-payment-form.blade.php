{{-- <div class="py-2 text-left text-danger" style="display: {{ strtolower($type) == 'voucher' ? '' : 'none'}}">
  This order will be paid using voucher. Important! Any unused value will be burnt.
</div> --}}
<div class="col-12 row">
    <span class="form-text text-left ml-3 text-danger aggreement{{ strtolower($type) }}" style="display:none;">This field is
        required</span>
    <div class="form-check col-12 checkbox-style form-submit-list padding-zero">
        <input class="form-check-input radio{{ strtolower($type) }}" type="radio" name="tncRadio" id="gridCheck{{ $type }}" >
        <label class="form-check-label" for="gridCheck{{ $type }}" ></label>

        <span> I have read, understand and accepted the <a href="#termsNconditions" id="DeleteCartItem"
                data-bs-toggle="modal" role="button">return and refund policy</a>.
            <br>

        </span>
    </div>
    @if ($type == 'Fpx')
    <br>
    <span class="form-text text-danger aggree{{ strtolower($type) }}" style="display:none;">This field is
        required</span>
    <div id="fpx-tnc" class="col-12 fpx-tnc form-check checkbox-style form-submit-list padding-zero "
        style="">

        <input class="form-check-input radio{{ strtolower($type) }}" type="radio" name="fpx_tnc"
            id="fpx_tnc{{ $type }}">
        <label class="form-check-label" for="fpx_tnc{{ $type }}" ></label>

        <span> By clicking on the "Submit" button, you hereby agree with <a
                href="https://www.mepsfpx.com.my/FPXMain/termsAndConditions.jsp" target="_blank" id="DeleteCartItem">FPX's
                Terms & Conditions</a>.
             </span>
    </div>
    @endif
</div>

<div class="col-12">
    <input type="hidden" name="payment_option" value="{{ $type }}">
<input type="hidden" name="product" id="listProduct" value="{{ json_encode(array_keys($cartsItems['items'])) }}">
<input type="hidden" name="address" value="{{ json_encode($cookie) }}">
<input type="hidden" id="finalTotal" name="finalTotal" value="{{ $cartsItems['finalTotal'] }}" readonly>
<input type="hidden" id="finalDiscount" name="finalDiscount" value="{{ $cartsItems['discount'] }}" readonly>
<input type="hidden" id="finalTotalPoint" name="finalTotalPoint" value="{{ $cartsItems['finalTotalPoint'] }}"
    readonly>
<input type="hidden" id="shippingFee" name="shippingFee" value="{{ json_encode($cartsItems['shippingFee']) }}"
    readonly>
    <br>
<button @if ($type == 'Metapoint') id="submit_button" @endif  type="submit" class="btn red-btn float-right clickOnce" data-form="{{ strtolower($type) }}">Submit</button>
</div>




