@extends('layouts.management.main-customer')



@section('content')

@section('page_title')
{{ "Welcome to My Dashboard" }}
@endsection
<div class="tab-content my-box-pd" id="v-pills-tabContent">
    <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
            <div class="row">
                <div class="col-lg-4 dashboard-pt-b">
                    <div class="checkout-box-s2 checkout-box-height">
                        <div class="dashboard-box">
                            <div class="dashboard-title">
                                <p>Voucher Code</p>
                            </div>
                        </div>
                        <div class="my-voucher-code-body">
                            <div class="body-inner">
                                <div class="col-lg-4 col-4 voucher-icon">
                                    <img class="db-icon1" src="{{asset('/images/icons/dashboard-icon1.png')}}">
                                </div>
                                <div class="col-lg-8 col-8 voucher-text">
                                    <h1>2</h1>
                                    <h6>Valid Voucher(s)</h6>
                                </div>
                            </div>

                        </div>
                        <div class="dashboard-card-bottom">
                            <a href="#" id="voucher-btn" class="btn voucher-btn">Redeem Vouchers</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 dashboard-pt-b">
                    <div class="checkout-box-s3 checkout-box-height">
                        <div class="dashboard-box">
                            <div class="dashboard-title">
                                <div class="title-width">
                                    <p>Value Records</p>
                                </div>

                                <div class="icon-special2">
                                    <div class="icon-viewmore2">
                                        <a href="#" data-original-title="" title="" tabindex="0">
                                            <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="my-value-record-body">
                            <div class="myv-img">
                                <img class="db-img1" src="{{asset('/images/formula/product2.png')}}">
                            </div>
                            <h4>
                                Almond Shower Gel 1000ml
                            </h4>

                        </div>
                        <div class="dashboard-card-bottom">
                            <h5>Order Placed</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 dashboard-pt-b">
                    <div class="checkout-box-s3 checkout-box-height">
                        <div class="dashboard-box">
                            <div class="dashboard-title">
                                <div class="title-width">
                                    <p>Perfect List</p>
                                </div>

                                <div class="icon-special2">
                                    <div class="icon-viewmore2">
                                        <a href="#"data-original-title="" title="" tabindex="0">
                                            <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                        </a>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="my-perfect-list-body">
                            <div class="myp-img">
                                <img class="db-img2" src={{asset('/images/formula/product.png')}}>
                            </div>
                            <h4>
                                Cetaphil Moisturiser Daily Use Bundle Set (Moisturiser)
                            </h4>


                        </div>
                        <div class="dashboard-card-bottom">
                            <a href="#" id="cart-btn" class="btn voucher-btn">Add to Cart</a>
                        </div>
                    </div>
                </div>

            </div>
    </div>    
</div>        
@endsection