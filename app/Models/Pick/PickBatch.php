<?php

namespace App\Models\Pick;

use Illuminate\Database\Eloquent\Model;

class PickBatch extends Model
{
    protected $table = 'wh_picking';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    public function batchDetail()
    {
        return $this->hasMany('App\Models\Pick\PickBatchDetail', 'key');
    }

    public function bin()
    {
        return $this->hasone('App\Models\Pick\PickBin', 'id', 'bin_id');
    }

    public function pick_user()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'picker');
    }

    public function sort_user()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'sorter');
    }

    public function pack_user()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'packer');
    }
}
