<?php

namespace App\Traits;

use Carbon\Carbon;
use stdClass;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Gift\Freegift;
use App\Models\Products\ProductAttribute;
use Session;

trait FreeGiftraits
{
    public static function freeGift_AutoApply($carts,$outlet_id=1) {

        $item_product_code = self::getProductCode($carts);
        $collection = collect($item_product_code);

        //check if customer is using voucher
        $session = Session::get('bjs_cart_discount');

        if(isset($session['voucher'])) {
            $freegift = [];
        }
        else {
            $freegift = self::getFreeGiftRules($outlet_id);
        }

        if(count($freegift)) {

            foreach ($freegift as $k => $v) {
                switch ($v->gifted) {
                    case 'product':
                        $contains = json_decode($v->contains);

                        if($total_pc = count($contains)) {
                            foreach ($contains as $a => $b) {
                                $pdata[$a]['pc'] = $collection->contains($b);
                            }
                        }

                        $isContains = collect($pdata);

                        $checkContains = $isContains->where('pc', true)->count();

                        $quantity = collect($carts)->whereIn('product_code', $contains)->sum('quantity');

                        if($checkContains == $total_pc) {
                            if($v->gift_type == 'product') {
                                $data[] = [
                                    'status' => true,
                                    'gift_type' => $v->gift_type,
                                    'freegift' => $v->gift_product,
                                    'quantity' => floor($quantity/count($contains)),
                                    'product' => ProductAttribute::where('product_code',$v->gift_product)->first()
                                ];
                            }
                            else {
                                $mgift = json_decode($v->gift_product);

                                if(count($mgift) > 0) {
                                    foreach ($mgift as $key => $value) {
                                        $product_info[] = ProductAttribute::where('product_code',$value)->first();
                                    }
                                }

                                $data[] = [
                                    'status' => true,
                                    'gift_type' => $v->gift_type,
                                    'freegift' => $mgift,
                                    'quantity' => floor($quantity/count($contains)),
                                    'product' => $product_info
                                ];
                            }
                        }
                        else {
                            // not meet conditions
                            $data[] = [
                                'status' => false,
                                'gift_type' => '',
                                'freegift' => ''
                            ];
                        }

                        break;
                    case 'quantity':
                        $contains = $v->contains;
                        $isContains = $collection->contains($contains);

                        if($isContains == true) {
                            $cartItem = collect($carts)->where('product_code', $contains)->first();

                            if($cartItem['quantity'] >= $v->quantity) {

                                if($v->gift_type == 'product') {
                                    $data[] = [
                                        'status' => true,
                                        'gifted' => $v->gifted,
                                        'gift_type' => $v->gift_type,
                                        'freegift' => $v->gift_product,
                                        'quantity' => floor($cartItem['quantity']/$v->quantity),
                                        'product' => ProductAttribute::where('product_code',$v->gift_product)->first()
                                    ];
                                }
                                else {
                                    $mgift = json_decode($v->gift_product);

                                    if(count($mgift) > 0) {
                                        foreach ($mgift as $key => $value) {
                                            $quantity_info[] = ProductAttribute::where('product_code',$value)->first();
                                        }
                                    }

                                    $data[] = [
                                        'status' => true,
                                        'gift_type' => $v->gift_type,
                                        'freegift' => $mgift,
                                        'quantity' => floor($cartItem['quantity']/$v->quantity),
                                        'product' => $quantity_info
                                    ];
                                }

                            }
                            else {
                                $data[] = [
                                    'status' => false,
                                    'gift_type' => '',
                                    'freegift' => ''
                                ];
                            }
                        }
                        else {
                            $data[] = [
                                'status' => false,
                                'gift_type' => '',
                                'freegift' => ''
                            ];
                        }
                        break;
                    case 'brand':
                        dd($v);
                    // case 'category':
                        break;
                    default:
                }
            }

            return $data;
        }
        else {
            return [];
        }

        // dd($collection,$isContains);


    }

    public static function getFreeGiftRules($outlet_id) {
        $dt1 = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->startOfDay()->toDateTimeString();
        $dt2 = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->endOfDay()->toDateTimeString();

        $freegift = Freegift::where([['outlet_id',$outlet_id],['start_date','<=', $dt1],['end_date','>',$dt2],['status',1]])->get();

        if($freegift->isNotEmpty()) {
            return $freegift;
        }
        else {
            return [];
        }
    }

    public static function getProductCode($cartItem) {
        if(count($cartItem)) {
            $cartItem = collect($cartItem)->where('selected',1);

            if(count($cartItem)) {
                foreach ($cartItem as $k => $v) {
                    $product_code[] = $v->product_code;
                }

                return $product_code;
            }
        }
    }


}
