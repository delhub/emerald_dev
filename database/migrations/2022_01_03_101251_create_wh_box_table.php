<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhBoxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_box', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('box_name')->unique();;
            $table->string('using')->default(0);
            $table->bigInteger('pic_1')->default(0);
            $table->bigInteger('pic_2')->default(0);
            $table->bigInteger('pic_3')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_box');
    }
}
