<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.favicon')
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine&display=swap" rel="stylesheet">

    <style>
        /* Hide scrollbar for Chrome, Safari and Opera */
        .scrollbar-hidden::-webkit-scrollbar {
            display: none;
        }

        /* Hide scrollbar for IE and Edge */
        .scrollbar-hidden {
            -ms-overflow-style: none;
        }
    </style>
</head>

<body>

    <div>
        <img src="{{ asset('assets/images/background-image/bujishu-launch.jpg') }}" alt="" style="max-width: 100%; position: relative;">

        <div style="position: absolute; top: 450px; left: 295px; width: 120px; height: 70px; color: #000000; font-size: 2rem; font-weight: 600; overflow: hidden; text-align: right;">
            <p style="margin-bottom: 0;">
                {{ $agentRegistrationSum }}
            </p>
        </div>

        <div style="position: absolute; top: 450px; left: 590px; width: 120px; height: 55px; color: #000000; font-size: 2rem; font-weight: 600; overflow: hidden; text-align: right;">
            <p style="margin-bottom: 0;">
                {{ $customerRegistrationSum }}
            </p>
        </div>

        <div style="position: absolute; top: 430px; left: 810px; width: 550px; height: 70px; color: #000000; font-size: 3rem; font-weight: 600; overflow: hidden; text-align: right;">
            <p style="margin-bottom: 0;">
                RM {{ $totalSales }}
            </p>
        </div>

        <div style="position: absolute; top: 635px; left: 165px; width: 550px; height: 165px; color: #000000; font-size: 2rem; font-weight: 600; overflow: hidden;">
            <div id="latest-agents" class="overflow-auto scrollbar-hidden" style="height: 100%;">
                <ul style="list-style-type: none; text-align: center;">
                    @foreach($latestThreeAgent as $agent)
                    <li>
                        {{ $agent->full_name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div style="position: absolute; top: 635px; left: 810px; width: 550px; height: 165px; color: #000000; font-size: 2rem; font-weight: 600; overflow: hidden;">
            <div id="sales-performance" class="overflow-auto scrollbar-hidden" style="height: 100%;">
                <ul style="list-style-type: none; text-align: left;">
                    @foreach($topThreeSales as $group)
                    <li>
                        <p class="mb-0" style="display: inline-block; width: 50%;">
                            {{ $group->group_name }}
                        </p>
                        <p class="mb-0" style="display: inline-block;">
                            RM {{ number_format(($group->sales_amount / 100), 2) }}
                        </p>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function() {
            $("#latest-agents").animate({
                scrollTop: $('#latest-agents').prop("scrollHeight")
            }, 30000);

            $("#sales-performance").animate({
                scrollTop: $('#sales-performance').prop("scrollHeight")
            }, 30000);
        });
    </script>

</body>






</html>