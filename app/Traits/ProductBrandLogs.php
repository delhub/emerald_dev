<?php

namespace App\Traits;

use App\Models\Products\ProductBrandLogs as ProductsProductBrandLogs;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait ProductBrandLogs
{
    // public $priceTypes = ['fixed_price', 'price', 'member_price', 'offer_price', 'point', 'point_cash', 'discount_percentage', 'product_sv'];
    // show price for product

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            // dd($model->table);
            $logs = ProductsProductBrandLogs::whereMonth('log_month', Carbon::now()->month)->first();

            if ($logs) {
                switch ($model->table) {
                    case 'global_products':
                        $logs->increment('new_product_count');
                        $logs->increment('product_count');
                        $logs->save();
                        break;

                    case 'product_brand':
                        $logs->increment('new_brand_count');
                        $logs->increment('brand_count');
                        $logs->save();
                        break;

                    default:
                        break;
                }

            }

        });

        self::updated(function ($model) {

            foreach ($model->getChanges() as $key => $changes) {
                $logs = ProductsProductBrandLogs::whereMonth('log_month', Carbon::now()->month)->first();


                if ($logs) {

                    $terminate = $logs->product_terminate;

                    switch ($key) {
                        case 'product_status':
                            // if ($model->display_only == 0) {
                                if ($changes == 2) {
                                    $logs->product_terminate = $terminate + 1;
                                } else {
                                    $logs->product_terminate = $terminate - 1;
                                }
                            // }
                            $logs->save();

                            break;

                        case 'display_only':
                            $suspended = $logs->product_suspended;
                            if ($changes == 1) {
                                $logs->product_suspended = $suspended + 1;
                            } else {
                                $logs->product_suspended = $suspended - 1;

                                if ($model->product_status == 1) {
                                    $logs->product_terminate = $terminate + 1;
                                } else {
                                    $logs->product_terminate = $terminate - 1;
                                }
                            }
                            $logs->save();

                            break;

                        default:
                            break;
                    }
                }
            }
        });
    }
}
