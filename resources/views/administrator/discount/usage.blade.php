@extends('layouts.administrator.mainVue')

@section('content')
@php

@endphp

<h1>Discount Usage</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div class="table-responsive m-2">
            <v-client-table :columns="columns" :data="code" :options="options">

                <span slot="coupon_status" slot-scope="p">
                    <span class="badge" :class="{ 'badge-success': p.row.coupon_status == 'valid', 'badge-secondary': p.row.coupon_status == 'expired', 'badge-dark': p.row.coupon_status == 'used', 'badge-warning': p.row.coupon_status == 'inactive' }">@{{p.row.coupon_status.toUpperCase()}}</span>
                </span>

                <span slot="__action" slot-scope="p">
                    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#codeEdit" @@click="getCodeById(p.row.id)">Edit</button>
                    <button type="button" class="btn btn-danger" @@click="deleteCode(p.row.id)">Delete</button>  --}}
                </span>

            </v-client-table>

        </div>
    </div>
</div>
@endsection


@push('script')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-tables-2@2.2.1/dist/vue-tables-2.min.js"></script>
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script>
Vue.use(VueTables.ClientTable);

    var app = new Vue({
    el: '#app',
    data: {
        code: [],
        id: '',
        user_id: '',
        country_id: '',
        coupon_code: '',
        coupon_status: '',
        coupon_type: '',
        coupon_amount: '',
        columns: ['id','user_id','coupon_code','purchase_created','purchase_redeemed','redeem_date','created_at','__action'],
        options: {
          headings: {
            id: '#',
            user_id: 'User ID',
            country_id: 'Country ID',
            __action: 'Action'
          },
          perPage: 10,
          cellClasses:{
            /* coupon_status: [{ class:'text-center', condition: row => row.coupon_status }],
            usage_counter: [{ class:'text-center', condition: row => row.usage_counter }] */
          }
        }
    },
    mounted() { //created // mounted //beforeMount
        this.getUsage()
    },
    methods: {
        getUsage() {
        axios
            .get('/administrator/discount/usage-list')
            .then(response => {
                this.code = response.data
                })
            .catch(error => console.log(error))
        }
    }
    });
</script>
@endpush
