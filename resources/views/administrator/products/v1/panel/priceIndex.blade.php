@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

<!-- Empty Modal -->
<div class="modal fade" id="ModalId" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Ajax response loaded here -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-3">
        <h1 style="display:inline;">Panel Product Price</h1>
    </div>
</div>

<form action="{{ route('administrator.v1.products.panels.price') }}" method="GET">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-3">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.v1.products.panels.price') }} style="color:black;">Reset Filter</a></button>
                </div>
                <select class="custom-select" id="selectSearch" name="categories">
                    <option value="" selected>Choose Product Category...</option>
                        @foreach ($categories as $cat)
                        <option value="{{$cat->id}}" {{ $request->categories == $cat->id ? 'selected' : '' }}>{{$cat->name}}</option>
                        @endforeach
                </select>
              </div>
        </div>
        <div class="col-lg-2">
            <select class="custom-select" id="selectSearch" name="selectSearch">
                <option value="ALL" selected>Choose Attribute...</option>
                <option value="NAME" {{ $request->selectSearch == 'NAME' ? 'selected' : '' }}>Product/Attribute Name</option>
                <option value="CODE" {{ $request->selectSearch == 'CODE' ? 'selected' : '' }}>Product/Attribute Code</option>
            </select>
        </div>
        <div class="col-lg-3">
            <div class="input-group mb-3">
                <input type="text" name="searchBox" class="form-control" placeholder="Search">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
        </div>
    </div>
</form>

    {{-- <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT:  <b>{{ count($count)}} </b></span></p> --}}
    <div class="row">
        <div class="col-auto ml-auto">
            {!! $productPrices->render() !!}
        </div>
    </div>

<div class="table-responsive">
    <table id="panel-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
        <thead>
            <tr>
                <th>No.</th>
                <th>ProductIMG</th>
                <th>Product</th>
                {{-- <th>Markup Category</th> --}}
                <th>Price</th>
            </tr>
        </thead>

        <tbody>
            @foreach($lists as $productKey => $list)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                    @if($list['product_image'])
                    <img style="border-radius: 5%; width: 100px" src="{{ asset('storage/' . $list['product_image']['path']. $list['product_image']['filename']) }}" alt="">
                    @endif
                </td>
                <td>
                    Price Type: <strong>{{$list['model_type']}}</strong> <br>
                    Product Code: <strong>{{$list['product_code']}}</strong> <br>
                    Product Name / Attribute Name: <strong>{{$list['product_name']}}</strong>

                </td>

                {{-- <td>
                    @if ($user->hasRole('administrator') || $user->hasRole('management'))
                        @if ($list['markup_category'] != 'AtrributePrice')
                            <button type="button" class="btn btn-dark edit-markup" data-toggle="modal" data-target="#newProductModaledit"
                            data-price-id="{{ $list['panelProductID'] }}" >{{$list['markup_category']}}
                            </button>
                        @endif
                    @endif
                </td> --}}

                <td>
                    <table>
                        <thead>
                            <tr>
                                <th>Version</th>
                                <th>Country</th>
                                <th>Fixed Price</th>
                                <th>Offer Price</th>
                                <th>Standard Member Price</th>
                                <th>Advance Member Price</th>
                                <th>Premier Member Price</th>
                                <th>Notes</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach ($list['cPrice'] as $price)
                        @if ($price->country_id == 'MY')
                        <tr>
                            <td>{{$price->price_key}} @if ($price->active == 1) (Current) @endif </td>
                            <td>{{$price->country_id}}</td>
                            <td>{{$price->fixed_price}}
                                {{-- @if($price->country_id != 'MY') ({{$price->model->getPricingModel('SG')->fixed_price}}) @endif --}}
                            </td>
                            <td>{{$price->price}}
                                {{-- @if($price->country_id != 'MY') ({{$price->model->getPricingModel('SG')->price}}) @endif --}}
                            </td>
                            <td>{{$price->member_price}}
                                {{-- @if($price->country_id != 'MY') ({{$price->model->getPricingModel('SG')->member_price}}) @endif --}}
                            </td>
                            <td>{{$price->offer_price}}
                                {{-- @if($price->country_id != 'MY') ({{$price->model->getPricingModel('SG')->offer_price}}) @endif --}}
                            </td>
                            <td>{{$price->product_sv}}  </td>

                            <td>{{$price->notes}}  </td>

                            {{-- <td style="width: 20%;">
                                @if ($user->hasRole('administrator') || $user->hasRole('management'))
                                    <button type="button" class="btn btn-dark edit-price" data-toggle="modal" data-target="#newProductModaledit" data-price-id="{{ $price->id }}">Edit</button>
                                @endif
                            </td> --}}
                        </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="col-auto ml-auto py-3">
        {!! $productPrices->render() !!}
    </div>
</div>

@endsection

@push('script')
<script>
    $(document).ready(function() {

        let editURL;

        //edit price modal
        $('#app').on('click', '.edit-price', function() {
            // Price Edit URL - GET
            editURL = "{{ route('administrator.v1.products.panels.price.edit', ['id' => 'id']) }}";

            let id = $(this).data('price-id');

            editURL = editURL.replace('id', id);

            $.ajax({
                async: true,
                beforeSend: function() {
                },
                complete: function() {
                },
                url: editURL,
                type: 'GET',
                success: function(response) {
                    $('#ModalId .modal-body').empty();

                    // Change modal title
                    $('#ModalId .modal-title').text('Edit Price')
                    // Add response in Modal body
                    $('#ModalId .modal-body').html(response);

                    // Display Modal
                    $('#ModalId').modal('show');

                    $('.select2-edit').select2({
                        dropdownParent: $('#ModalId')
                    });

                },
                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON.message);
                }
            });

        });

        //edit markup modal
        $('#app').on('click', '.edit-markup', function() {
            // Price Edit URL - GET
            editURL = "{{ route('administrator.v1.products.panels.price.edit-markup', ['id' => 'id']) }}";

            let id = $(this).data('price-id');

            editURL = editURL.replace('id', id);

            let outsideMarkupButton = this;

            $.ajax({
                async: true,
                beforeSend: function() {
                },
                complete: function() {
                },
                url: editURL,
                type: 'GET',
                success: function(response) {
                    $('#ModalId .modal-body').empty();

                    // Change modal title
                    $('#ModalId .modal-title').text('Edit Price')
                    // Add response in Modal body
                    $('#ModalId .modal-body').html(response);

                    // Display Modal
                    $('#ModalId').modal('show');

                    $('.select2-edit').select2({
                        dropdownParent: $('#ModalId')
                    });

                    $('#modalSubmitBtn').on('click', function() {

                        const markupCategry = document.getElementById('markup_category');
                        const categoryId = markupCategry.options[markupCategry.selectedIndex].value;

                        const markupName = $(markupCategry).find(':selected').data('markup-name')

                        $.ajax({
                            async: true,
                            beforeSend: function() {
                                // Show loading spinner.
                                // loading.show();
                            },
                            complete: function() {
                                // Hide loading spinner.
                                // loading.hide();
                            },
                            url: '/administrator/products/v1/panels/price-update-markup/' + $(this).data('item-id'),
                            type: "GET",
                            data:{
                                // POST data.
                                category_id: categoryId,
                            },
                            success: function(result) {
                                // change button text
                                outsideMarkupButton.innerHTML = markupName;
                                //hiden modal
                                $("#ModalId").modal('hide');
                            },
                            error: function(result) {
                                // Log into console if there's an error.
                                console.log(result.status + ' ' + result.statusText);
                            }
                        });

                    });

                },
                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON.message);
                }
            });

        });

    });
</script>
@endpush
