@extends('layouts.administrator.mainVue')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/discount" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">

                <div class="col-12">
                    <h4>
                        Create New Code
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.discount.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <select name="country_id" class="form-control" style="width: 100%;">
                                <option value="">Country..</option>
                                <option value="MY">Malaysia</option>
                                <option value="SG">Singapore</option>
                            </select>

                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Coupon Code <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <div class="input-group mb-3">
                                    <input type="text" v-model="coupon_code" name="coupon_code" class="form-control" placeholder="Coupon Code">
                                    <div class="input-group-append">
                                      <button class="btn btn-primary" type="button" @@click="generateCode()">Generate</button>
                                    </div>
                                  </div>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p> Generate<small class="text-danger">*</small></p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="number" name="generate_amount" class="form-control" placeholder="Amount code to generate" autocomplete="off" value="1">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Coupon Type <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="coupon_type" class="form-control" style="width: 100%;">
                                    <option value="value">Value</option>
                                    <option value="percentage">Percentage</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Coupon Amount <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" name="coupon_amount" class="form-control" placeholder="Coupon Amount">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Coupon Rule ID <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="coupon_rule_id" class="form-control" style="width: 100%;">
                                    <option v-for="(r,i) in rule" :value="r.id">@{{'' + r.id + '. ' + r.country_id + ' | ' + r.name + ' - ' + r.rule_type}}</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p> Coupon Max Usage<small class="text-danger">*</small></p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="number" name="max_usage" class="form-control" placeholder="Coupon Max Usage" autocomplete="off">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p> Platform<small class="text-danger">*</small></p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="platform" class="form-control" style="width: 100%;">
                                    <option value="1">Web</option>
                                    <option value="2">Retail Shop</option>
                                    <option value="3">Web + Retail</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p> Outlet<small class="text-danger">*</small></p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="outlet_id" class="form-control" style="width: 100%;">
                                    <option value="1">Formula2u, MBB</option>
                                    <option value="2">Wisma Formula, Puchong</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Coupon Status <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <select name="coupon_status" class="form-control" style="width: 100%;">
                                    <option value="valid">Valid</option>
                                    <option value="invalid">Invalid</option>
                                    <option value="inactive">Inactive</option>
                                    <option value="expired">Expired</option>
                                    <option value="used">Used</option>
                                </select>
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Expiry<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="date" name="expiry" class="form-control" placeholder="Expiry Date" autocomplete="off">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-tables-2@2.2.1/dist/vue-tables-2.min.js"></script>
<script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
<script>
    var app = new Vue({
    el: '#app',
    data: {
        coupon_code: '',
        rule: []
    },
    mounted() { //created // mounted //beforeMount
        this.generateCode()
        this.getRule()

    },
    methods: {
        generateCode() {
        axios
            .get('/administrator/discount/generateCode')
            .then(response => {
                this.coupon_code = response.data.code
                })
            .catch(error => console.log(error))
        },
        getRule() {
        axios
            .get('/administrator/discount/getRule')
            .then(response => {
                this.rule = response.data
                })
            .catch(error => console.log(error))
        }
    }
    });
</script>
<script>
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
</script>
@endpush
