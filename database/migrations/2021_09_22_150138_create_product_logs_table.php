<?php

use App\Models\Globals\Products\Product;
use App\Models\Products\ProductBrand;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_brand_logs', function (Blueprint $table) {
            $table->id();
            $table->date('log_month');
            $table->integer('brand_count')->default(0);
            $table->integer('product_count')->default(0);
            $table->integer('new_brand_count')->default(0);
            $table->integer('new_product_count')->default(0);
            $table->integer('product_suspended')->default(0);
            $table->integer('product_terminate')->default(0);
            $table->timestamps();
        });

        DB::table('product_brand_logs')->insert([
            'log_month' => Carbon::now(),
            'brand_count' => ProductBrand::count(),
            'product_count' => Product::count(),
            'new_brand_count' => 0,
            'new_product_count' => 0,
            'product_suspended' => Product::where('display_only',1)->count(),
            'product_terminate' => Product::where('product_status',2)->count(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_brand_logs');
    }
}
