@extends('layouts.administrator.main')

@section('content')


    <h1>Export - Formula Web</h1>

    @hasrole('account_manager')
        <div class="card">
            <div class="card-header">
                Download

            </div>

            <div class="card-body">
                <!-- Date filter from date to date-->
                <form class="row" action="{{ route('csv.payments', ['country_id' => $country]) }}" method="get">
                    <div class="col-md-auto px-1">
                        <div class="input-group mb-3">
                            <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Date"
                                aria-label="Username" aria-describedby="basic-addon1" value="{{-- ($request->datefrom)?$request->datefrom:'' --}}"
                                autocomplete="off" required>
                        </div>
                    </div>

                    <div class="col-md-auto px-1">
                        <div class="input-group mb-3">
                            <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Date"
                                aria-label="Username" aria-describedby="basic-addon1" value="{{-- ($request->dateto)?$request->dateto:'' --}}"
                                autocomplete="off" required>
                        </div>
                    </div>
                    <div class="col-md-auto px-1">
                        <div class="input-group mb-3">
                            <button type="submit" name="downloadType" value="payment" class="btn btn-primary mx-2">Payment
                                Report</button>
                            <button type="submit" name="downloadType" value="sale" class="btn btn-primary mx-2">Sale
                                Report</button>
                            <button type="submit" name="downloadType" value="invoice" class="btn btn-primary mx-2">Invoices
                                Report</button>
                        </div>
                    </div>
                </form>
                @if ($errors->any())
                    <br>
                    <p class="text-danger">{{ $errors->first() }}</p>
                @endif
            </div>
        </div>
    @endhasrole

    @hasrole('export_manager')
        <div class="card">
            <div class="card-header">

            </div>
            <div class="card-body">
                <div class="row">
                    <a href="{{ route('csv.agent', ['country_id' => $country]) }}">
                        <button type="button" class="btn btn-info mx-2">Agents</button>
                    </a>
                    <a href="{{ route('csv.customer', ['country_id' => $country]) }}">
                        <button type="button" class="btn btn-primary mx-2">Customers</button>
                    </a>
                    <a href="{{ route('csv.orders', ['country_id' => $country]) }}">
                        <button type="button" class="btn btn-info mx-2">Orders</button>
                    </a>
                    <a href="{{ route('csv.products', ['country_id' => $country]) }}">
                        <button type="button" class="btn btn-primary mx-2">Products</button>
                    </a>
                </div>
            </div>
        </div>
    @endhasrole

@endsection

@push('script')
    <script>
        // $('[name=datefrom]').on('change', function() {
        //     console.log('[name="dateto"]');
        //     $('[name=dateto]').prop('min', this.value)
        // })

        // $('[name=dateto]').on('change', function() {
        //     $('[name="datefrom"]').prop('max', this.value)
        // })
        //

        // set minimum date value for 'dateto' input
        $('[name=datefrom]').on('change', function() {
            var selectedDate = $(this).datepicker('getDate');
            $('[name=dateto]').datepicker('option', 'minDate', selectedDate);
        });

        // set maximum date value for 'datefrom' input
        $('[name=dateto]').on('change', function() {
            var selectedDate = $(this).datepicker('getDate');
            $('[name=datefrom]').datepicker('option', 'maxDate', selectedDate);
        });



        $(function() {
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });

            $(".estimatedateonly").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                changeMonth: true,
                changeYear: true
            });

            $(".filterDate").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
@endpush
