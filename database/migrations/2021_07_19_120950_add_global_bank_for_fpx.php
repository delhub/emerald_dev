<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGlobalBankForFpx extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('global_bank');

            Schema::create('global_bank', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('bank_code');
                $table->string('bank_name');
                $table->string('country_id', 2)->default('MY');
                $table->string('type')->default('agent');
            });


            DB::table('global_bank')->insert(
                array(
                    array("bank_code" => "AIBBMYKL","bank_name" => "Affin Islamic Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "AISLMYKL","bank_name" => "AmIslamic Bank (M) Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "ALSRMYKL","bank_name" => "Alliance Islamic Bank (M) Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "ARBKMYKL","bank_name" => "AmBank","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "BARCGB22","bank_name" => "Barclays Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "BIMBMYKL","bank_name" => "Bank Islam Malaysia","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "BKCHSGSG","bank_name" => "Bank of China","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "BKIDSGSG","bank_name" => "Bank of India","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "BKKBSGSG","bank_name" => "Bangkok Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "BKRMMYKL","bank_name" => "Bank Kerjasama Rakyat","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "BMMBMYKL","bank_name" => "Bank Muamalat (Malaysia) Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "BNMAMY2K","bank_name" => "Bank Simpanan Nasional Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "CHASSGSG","bank_name" => "J.P. Morgan Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "CIBBMYKL","bank_name" => "CIMB Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "CIBBSGSG","bank_name" => "CIMB Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "CITIMYKL","bank_name" => "CitiBank Malaysia","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "CITISGSG","bank_name" => "Citibank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "CTBBMYKL","bank_name" => "CIMB Islamic Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "DBSSSGSG","bank_name" => "Developmental Bank of Singapore","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "HBMBMYKL","bank_name" => "HSBC Bank Malaysia Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "HLBBMYKL","bank_name" => "Hong Leong Bank","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "HLIBMYKL","bank_name" => "Hong Leong Islamic Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "HMABMYKL","bank_name" => "HSBC Amanah Malaysia Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "HSBCSGSG","bank_name" => "HSBC Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "ICICSGSG","bank_name" => "ICICI Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "IOBASGSG","bank_name" => "Indian Overseas Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "MBBEMYKL","bank_name" => "Maybank","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "MBBESGS2","bank_name" => "Maybank Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "MBISMYKL","bank_name" => "Maybank Islamic Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "MFBBMYKL","bank_name" => "Alliance Bank","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "OCBCMYKL","bank_name" => "OCBC Bank (Malaysia) Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "OCBCSGSG","bank_name" => "OCBC Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "PBBEMYKL","bank_name" => "Public Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "PHBMMYKL","bank_name" => "Affin Bank","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "PUIBMYKL","bank_name" => "Public Islamic Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "RHBAMYKL","bank_name" => "RHB Islamic Bank Berhad","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "RHBBMYKL","bank_name" => "RHB Bank","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "RHBBSGSG","bank_name" => "RHB Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "SBINSGSG","bank_name" => "State Bank of India","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "SCBLMYKX","bank_name" => "Standard Chartered Bank Malaysia","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "SCBLSGSG","bank_name" => "Standard Chartered Bank","country_id" => "SG",'type' => 'agent'),
                    array("bank_code" => "UOVBMYKL","bank_name" => "United Overseas Bank Berhad   ","country_id" => "MY",'type' => 'agent'),
                    array("bank_code" => "UOVBSGSG","bank_name" => "United Overseas Bank","country_id" => "SG",'type' => 'agent'),
                    array('bank_code' => 'AIBBMYKL','bank_name' => 'Affin Islamic Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'AISLMYKL','bank_name' => 'AmIslamic Bank (M) Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'ALSRMYKL','bank_name' => 'Alliance Islamic Bank (M) Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'ARBKMYKL','bank_name' => 'AmBank','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'BIMBMYKL','bank_name' => 'Bank Islam Malaysia','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'BKRMMYKL','bank_name' => 'Bank Kerjasama Rakyat','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'BMMBMYKL','bank_name' => 'Bank Muamalat (Malaysia) Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'BNMAMY2K','bank_name' => 'Bank Simpanan Nasional Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'CIBBMYKL','bank_name' => 'CIMB Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'CITIMYKL','bank_name' => 'CitiBank Malaysia','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'CTBBMYKL','bank_name' => 'CIMB Islamic Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'HBMBMYKL','bank_name' => 'HSBC Bank Malaysia Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'HLBBMYKL','bank_name' => 'Hong Leong Bank','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'HLIBMYKL','bank_name' => 'Hong Leong Islamic Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'HMABMYKL','bank_name' => 'HSBC Amanah Malaysia Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'MBBEMYKL','bank_name' => 'Maybank','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'MBISMYKL','bank_name' => 'Maybank Islamic Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'MFBBMYKL','bank_name' => 'Alliance Bank','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'OCBCMYKL','bank_name' => 'OCBC Bank (Malaysia) Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'PBBEMYKL','bank_name' => 'Public Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'PHBMMYKL','bank_name' => 'Affin Bank','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'PUIBMYKL','bank_name' => 'Public Islamic Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'RHBAMYKL','bank_name' => 'RHB Islamic Bank Berhad','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'RHBBMYKL','bank_name' => 'RHB Bank','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'SCBLMYKX','bank_name' => 'Standard Chartered Bank Malaysia','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'UOVBMYKL','bank_name' => 'United Overseas Bank Berhad   ','type' => 'agent','country_id' => 'MY'),
                    array('bank_code' => 'ABB0234','bank_name' => 'Affin B2C - Test ID','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'ABB0233','bank_name' => 'Affin Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'ABMB0212','bank_name' => 'Alliance Bank (Personal)','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'AGRO01','bank_name' => 'AGRONet','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'AMBB0209','bank_name' => 'AmBank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'BIMB0340','bank_name' => 'Bank Islam','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'BMMB0341','bank_name' => 'Bank Muamalat','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'BKRM0602','bank_name' => 'Bank Rakyat','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'BSN0601','bank_name' => 'BSN','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'BCBB0235','bank_name' => 'CIMB Clicks','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'CIT0219','bank_name' => 'Citibank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'HLB0224','bank_name' => 'Hong Leong Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'HSBC0223','bank_name' => 'HSBC Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'KFH0346','bank_name' => 'KFH','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'MBB0228','bank_name' => 'Maybank2E','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'MB2U0227','bank_name' => 'Maybank2U','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'OCBC0229','bank_name' => 'OCBC Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'PBB0233','bank_name' => 'Public Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'RHB0218','bank_name' => 'RHB Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'TEST0021','bank_name' => 'SBI Bank A','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'TEST0022','bank_name' => 'SBI Bank B','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'TEST0023','bank_name' => 'SBI Bank C','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'SCB0216','bank_name' => 'Standard Chartered','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'UOB0226','bank_name' => 'UOB Bank','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'UOB0229','bank_name' => 'UOB Bank - Test ID','type' => 'b2c_staging','country_id' => 'MY'),
                    array('bank_code' => 'ABB0232','bank_name' => 'Affin Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'ABB0235','bank_name' => 'AFFINMAX','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'ABMB0213','bank_name' => 'Alliance Bank (Business)','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'AGRO02','bank_name' => 'AGRONetBIZ','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'AMBB0208','bank_name' => 'AmBank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'BIMB0340','bank_name' => 'Bank Islam','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'BMMB0342','bank_name' => 'Bank Muamalat','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'BNP003','bank_name' => 'BNP Paribas','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'BCBB0235','bank_name' => 'CIMB Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'CIT0218','bank_name' => 'Citibank Corporate Banking','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'DBB0199','bank_name' => 'Deutsche Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'HLB0224','bank_name' => 'Hong Leong Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'HSBC0223','bank_name' => 'HSBC Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'BKRM0602','bank_name' => 'i-bizRAKYAT','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'KFH0346','bank_name' => 'KFH','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'MBB0228','bank_name' => 'Maybank2E','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'OCBC0229','bank_name' => 'OCBC Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'PBB0233','bank_name' => 'Public Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'PBB0234','bank_name' => 'PB Enterprise','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'RHB0218','bank_name' => 'RHB Bank','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'TEST0021','bank_name' => 'SBI Bank A','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'TEST0022','bank_name' => 'SBI Bank B','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'TEST0023','bank_name' => 'SBI Bank C','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'SCB0215','bank_name' => 'Standard Chartered','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'UOB0228','bank_name' => 'UOB Regional','type' => 'b2b1_staging','country_id' => 'MY'),
                    array('bank_code' => 'ABB0233','bank_name' => 'Affin Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'ABMB0212','bank_name' => 'Alliance Bank (Personal)','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'AGRO01','bank_name' => 'AGRONet','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'AMBB0209','bank_name' => 'AmBank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'BIMB0340','bank_name' => 'Bank Islam','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'BMMB0341','bank_name' => 'Bank Muamalat','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'BKRM0602','bank_name' => 'Bank Rakyat','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'BSN0601','bank_name' => 'BSN','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'BCBB0235','bank_name' => 'CIMB Clicks','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'HLB0224','bank_name' => 'Hong Leong Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'HSBC0223','bank_name' => 'HSBC Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'KFH0346','bank_name' => 'KFH','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'MBB0228','bank_name' => 'Maybank2E','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'MB2U0227','bank_name' => 'Maybank2U','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'OCBC0229','bank_name' => 'OCBC Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'PBB0233','bank_name' => 'Public Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'RHB0218','bank_name' => 'RHB Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'SCB0216','bank_name' => 'Standard Chartered','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'UOB0226','bank_name' => 'UOB Bank','type' => 'b2c_prod','country_id' => 'MY'),
                    array('bank_code' => 'ABB0232','bank_name' => 'Affin Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'ABB0235','bank_name' => 'AFFINMAX','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'ABMB0213','bank_name' => 'Alliance Bank (Business)','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'AGRO02','bank_name' => 'AGRONetBIZ','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'AMBB0208','bank_name' => 'AmBank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'BIMB0340','bank_name' => 'Bank Islam','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'BMMB0342','bank_name' => 'Bank Muamalat','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'BNP003','bank_name' => 'BNP Paribas','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'BCBB0235','bank_name' => 'CIMB Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'CIT0218','bank_name' => 'Citibank Corporate Banking','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'DBB0199','bank_name' => 'Deutsche Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'HLB0224','bank_name' => 'Hong Leong Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'HSBC0223','bank_name' => 'HSBC Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'BKRM0602','bank_name' => 'i-bizRAKYAT','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'KFH0346','bank_name' => 'KFH','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'MBB0228','bank_name' => 'Maybank2E','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'OCBC0229','bank_name' => 'OCBC Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'PBB0233','bank_name' => 'Public Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'PBB0234','bank_name' => 'PB Enterprise','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'RHB0218','bank_name' => 'RHB Bank','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'SCB0215','bank_name' => 'Standard Chartered','type' => 'b2b1_prod','country_id' => 'MY'),
                    array('bank_code' => 'UOB0228','bank_name' => 'UOB Regional','type' => 'b2b1_prod','country_id' => 'MY')
                )
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_banks', function (Blueprint $table) {
            //
        });
    }
}
