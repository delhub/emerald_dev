<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Products\ProductAttribute;
use App\Models\Users\UserInfo;
use App\Models\Purchases\Purchase;
use Carbon\Carbon;

use function GuzzleHttp\json_decode;

class XilnexAPI extends Controller
{
    const APPID = 'Doqb6FDkLvCuqgF6S4kkimzaEPeS4eWs';
    const TOKEN = 'v5_LhY1hFP7I1RUWZjYR0EJ8zUuvw1lLCMNMGhJMm0eD9Y=';
    const AUTH = '5';

    public static $client;
    public static $apiServer;

    public static function apiServer()
    {
        return "https://api.xilnex.com";
    }

    //guzzle get api
    public static function guzzleGet($url) {
        $client = new Client(); 
        
        $response = $client->get($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }        
    }
    
    //guzzle post api
    public static function guzzlePost($url,$data) {
        $client = new Client(); 
        
        $response = $client->post($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ],
            'form_params' => $data/* ,
            'http_errors' => false */
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }        
    }

    //guzzle put api
    public static function guzzlePut($url,$data) {
        $client = new Client(); 
        
        $response = $client->put($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ],
            'json' => $data
            // 'http_errors' => false
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }        
    }

    //guzzle delete api
    public static function guzzleDelete($url) {
        $client = new Client(); 
        
        $response = $client->delete($url, [
            "headers" => [
                "appid" => self::APPID,
                'token' => self::TOKEN,
                'auth' => self::AUTH
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $response = $response->getBody();
            $result = json_decode($response);

            return $result;
        }
        else {
            return $response;
        }        
    }

    //get outlet
    public static function getOutlet($id)
    {
        $url = self::apiServer() . '/logic/v2/outlets/' . $id;
        $result = self::guzzleGet($url);

        return $result->data->outlet;
    }

    //create outlet
    public static function CreateOutlet($data)
    {
        $url = self::apiServer() . '/logic/v2/outlets';
        $result = self::guzzlePost($url,$data);

        return $result;
    }

    //syn stock
    public static function synStock()
    {
        $url = self::apiServer() . '/apps/v2/sync/stocks';
        $result = self::guzzleGet($url);

        $rest = collect($result->data->stocks)
                ->where('outletId',1)
                ->where('availableQuantity', '>', 0)
                ->where('onhandQuantity', '>', 0);

        return $rest;
    }

    public static function getStockByItemId($id)
    {
        $url = self::apiServer() . '/logic/v2/stocks/Item/' . $id . '?outlet=1';
        $result = self::guzzleGet($url);

        return $result->data->stocks;
    }

    public static function getAllItem()
    {
        $url = self::apiServer() . '/logic/v2/items';
        $result = self::guzzleGet($url);

        return $result->data->items;
    }

    public static function getItemByItemId($id)
    {
        $url = self::apiServer() . '/logic/v2/items/' . $id;
        $result = self::guzzleGet($url);

        return $result->data->item;
    }

    public static function SynAllStockItemOnWeb() {
        $allProduct = ProductAttribute::get('product_code');

        if(count($allProduct) > 0) {
            foreach($allProduct as $val) {
                $x[] = $val->product_code;
            }

            $allitem = self::getAllItem();
            $rest = collect($allitem)->whereIn('itemCode',$x);

            foreach($rest as $k=>$val) {
                $item = self::getStockByItemId($val->id);
                //$price = collect($allitem)->where('id',$val->id);
                self::UpdateStock($item[0]);
                $y[$k]['id'] = $val->id;
                $y[$k]['itemCode'] = $val->itemCode;
            }            

            // dd($y);

            return $allitem;
        }        
    }

    public static function UpdateStock($item) {

        $price = self::getItemByItemId($item->itemId);

        $pp = ProductAttribute::where('product_code',$item->itemCode)->first();

        $pp->stock = $item->availableQuantity;
        $pp->onhand_stock = $item->onhandQuantity;
        $pp->fixed_price = $price->suggestedPrice*100;
        $pp->price = $price->salesPrice*100;
        $pp->member_price = $price->employeePrice*100;
        $pp->offer_price = $price->wholesalesPrice*100;

        $pp->save();
    }

    //get all client
    public static function getAllClient()
    {
        $url = self::apiServer() . '/logic/v2/clients';
        $result = self::guzzleGet($url);

        return $result->data->clients;
    }
    
    //delete client
    public static function deleteClient($id)
    {
        $url = self::apiServer() . '/logic/v2/clients/client/' . $id;
        $result = self::guzzleDelete($url);

        return $result;
    }

    //cancel sales order 
    public static function cancelSalesOrder($id) // invoice id
    {
        $body = [
            'sale' => [
                'cancelOutlet' => "MAIN BRANCH",
                'cancelRemark' => "cancel."
            ]
        ];
        
        $url = self::apiServer() . "/logic/v2/sales/invoices/{$id}/cancel";
        $result = self::guzzlePut($url,$body);

        return $result;
    }

    //create client
    public static function createClient($data)
    {
        $outlet = self::getOutlet(1);

        $body = [
            'client' => [
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'category' => $data['category'],
                'active' => 'true',
                'createdOutlet' => $outlet->name,
                'verified' => 'true',
            ]
        ];

        $url = self::apiServer() . '/logic/v2/clients';        

        return self::guzzlePost($url,$body);
    }

    //guzzle not working api ## `400 Bad Request` response: {"ok":false,"status":"Sales cannot be created without sales items
    public static function createSalesOrder($userId,$cartItems,$purchase)
    {
        $userInfo = UserInfo::where('user_id',$userId)->first(); 

        $today = Carbon::now()->isoFormat('YYYY-MM-DD');

        if(count($cartItems) > 0) {
            foreach($cartItems as $k=>$val) {
                $cart[$k]['itemCode'] = $val->product_code;
                $cart[$k]['quantity'] = $val->quantity;
                $cart[$k]['enterPrice'] = $val->unit_price / 100;  
            }
        }

        dd($cart);

        $body = [
            'sale' => [
              'cashier' => 'shu fung',
              'dateTime' => $today . 'T00:00:00.000Z',
              'salesPerson' => 'xilnex api',
              'shippingRemark' => '',
              'status' => 'CONFIRMED',
              'confirmOutlet' => 'MAIN BRANCH',
              'outlet' => 'MAIN BRANCH',
              'discountPercentage' => 0,
              'billDiscountAmount' => 0,
              'remark' => '',
              'recipientContact' => NULL,
              'salesOrderId' => $purchase->purchase_number,
              'items' => $cart,
              'collections' => [
                0 => [
                  'method' => 'cash',
                  'amount' => '100',
                ],
              ],
              'client' => [
                'id' => $userInfo->xilnex_client_id,
              ]
            ]
        ];

        $url = self::apiServer() . "/logic/v2/sales/salesinvoice/MAIN%20BRANCH"; 

        return self::guzzlePost($url,$body);
    } 

    public static function curlCreateSalesOrder($userId,$cartItems,$purchase) {  

        $userInfo = UserInfo::where('user_id',$userId)->first(); 

        $today = Carbon::now()->isoFormat('YYYY-MM-DD');


        if(count($cartItems) > 0) {
            foreach($cartItems as $k=>$val) {
                $cart[$k]['itemCode'] = $val->product_code;
                $cart[$k]['quantity'] = $val->quantity;
                $cart[$k]['enterPrice'] = $val->unit_price / 100;  
            }
        }
        
        $body = [
            'sale' => [
              'cashier' => 'shu fung',
              'dateTime' => $today . 'T00:00:00.000Z',
              'salesPerson' => 'xilnex api',
              'shippingRemark' => '',
              'status' => 'CONFIRMED',
              'confirmOutlet' => 'MAIN BRANCH',
              'outlet' => 'MAIN BRANCH',
              'discountPercentage' => 0,
              'billDiscountAmount' => 0,
              'remark' => '',
              'recipientContact' => NULL,
              'salesOrderId' => $purchase->purchase_number,
              'items' => $cart,
              'collections' => [
                0 => [
                  'method' => 'cash',
                  'amount' => number_format(($purchase->purchase_amount / 100),2),
                ],
              ],
              'client' => [
                'id' => $userInfo->xilnex_client_id,
              ]
            ]
        ];
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => self::apiServer() . "/logic/v2/sales/salesinvoice/MAIN%20BRANCH",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => array(
            "appid: Doqb6FDkLvCuqgF6S4kkimzaEPeS4eWs",
            "auth: 5",
            "content-type: application/json",
            "token: v5_LhY1hFP7I1RUWZjYR0EJ8zUuvw1lLCMNMGhJMm0eD9Y="
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $inv = Purchase::where('purchase_number',$purchase->purchase_number)->first();

            $invoice = json_decode($response);

            if($invoice->status == 'SuccessInsert') {
                $inv->xilnex_invoice_id = $invoice->data->sale->id;
                $inv->save();
            }
            else {
                dd($invoice);
            }          

            return $response;
        }
    }

    public function test() {
        //
    }






}
