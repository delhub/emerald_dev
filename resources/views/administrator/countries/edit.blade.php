@inject('controller', 'App\Http\Controllers\Administrator\Countries\GlobalCountriesController')

@extends('layouts.administrator.main')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{route($controller::$home)}}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
    </div>
</div>
<div class="card shadow-sm mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <h4>
                    Edit {{$controller::$header}}
                </h4>
                @include('inc.messages')
            </div>
            <div class="col-12">
                <form action="{{ route($controller::$update,['id' => $data->country_id]) }}" id="edit-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country ID <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="country_id" name="country_id" class="form-control" value="{{ $data->country_id }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country Name <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="country_name" name="country_name" class="form-control" value="{{ $data->country_name }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country Currency <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="country_currency" name="country_currency" class="form-control" value="{{ $data->country_currency }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Conversion Rate <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="conversion_rate" name="conversion_rate" class="form-control" value="{{ $data->conversion_rate }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Mark Up <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="mark_up" name="mark_up" class="form-control" value="{{ $data->mark_up }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Company Name
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="company_name" name="company_name" class="form-control" value="{{ $data->company_name }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Company Address
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="company_address" name="company_address" class="form-control" value="{{ $data->company_address }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Company Reg
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="company_reg" name="company_reg" class="form-control" value="{{ $data->company_reg }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Company Phone
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="company_phone" name="company_phone" class="form-control" value="{{ $data->company_phone }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Company Email
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="company_email" name="company_email" class="form-control" value="{{ $data->company_email }}">
                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
