@extends('layouts.administrator.main')

@section('content')
@php

@endphp

    <h1>Import Stock</h1>

    <div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 col-md-8 col-lg-{{$req->col}}">

                        <div class="container my-3">
                            <form action="{{route('administrator.import.upload-stock')}}" method="post" enctype="multipart/form-data">
                            <h3 class="text-center mb-5">Import Stock</h3>
                                @csrf
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="chooseFile" onchange="getFileData(this)">
                                        <label class="custom-file-label" for="chooseFile"><span id="fn">Select file</span></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                <div class="col-12" id="withHeading" style="display: none">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                    <input name="withHead" id="withHead" type="checkbox" class="custom-control-input" aria-describedby="withHeadHelpBlock">
                                    <label for="withHead" class="custom-control-label">WithHeading</label>
                                    </div>
                                </div>
                                </div>

                                <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                                    Upload Files
                                </button>
                            </form>
                        </div>


                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@push('script')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
function getFileData(myFile){
    var file = myFile.files[0];
    var filename = file.name;

    var ext = filename.split('.').pop();

    $('#fn').text(filename);
}
</script>
@endpush
