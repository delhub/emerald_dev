<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewToGlobalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_statuses')
            ->where('id', 6000)
            ->update(['description' => json_encode(['amount' => 0,'range_yearly' => 1,'max_year' => 0, 'discount' => 0])]
        );

        DB::table('global_statuses')
            ->where('id', 6001)
            ->update(['description' => json_encode(['amount' => 500,'range_yearly' => 1,'max_year' => 1, 'discount' => 5])]
        );

        DB::table('global_statuses')
            ->where('id', 6002)
            ->update(['description' => json_encode(['amount' => 1000,'range_yearly' => 1,'max_year' => 3, 'discount' => 10])]
        );

        Schema::table('user_infos', function (Blueprint $table) {
            $table->timestamp('membership_date')->after('product_used')->nullable();
            $table->timestamp('last_checked')->after('membership_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_infos', function (Blueprint $table) {
            $table->dropColumn('membership_date');
            $table->dropColumn('last_checked');
        });
    }
}
