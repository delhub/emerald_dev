<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Status;
use Illuminate\Support\Facades\DB;
use App\Models\Users\User;
use App\Models\Users\UserInfo;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use Illuminate\Support\Facades\Hash;
use App\Models\Users\UserCountry;
use Illuminate\Support\Carbon;


class CreateWhTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wh_transfer', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user');
            $table->dateTime('session_start_time')->nullable();
            $table->dateTime('session_end_time')->nullable();
            $table->bigInteger('from_location_id');
            $table->bigInteger('to_location_id');
            $table->dateTime('transfer_datetime');
            $table->timestamps();
        });

        Schema::create('wh_transfer_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('key');
            $table->Integer('batch_item_id');
            $table->timestamps();
        });

        $status = new Status;
        $status->id = 8013;
        $status->name = 'Stock Transfer - IN';
        $status->description = 'Stock transfer in type';
        $status->status_type = 'inventory_status';

        $status->save();

        $status = new Status;
        $status->id = 8014;
        $status->name = 'Stock Transfer - OUT';
        $status->description = 'Stock transfer out type';
        $status->status_type = 'inventory_status';

        $status->save();

        DB::table('roles')->insert(
            array(
                array('name' => 'wh_transferrer', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        $role = 'transferrer';
        $this->createCustomer($role, $verify = true, 'MY');

        DB::statement("
        SELECT product_code, outlet_id,

        sum(case when inv_type = '8010' OR inv_type = '8000'OR inv_type = '8013'OR inv_type = '8014'
        then inv_amount
        else 0
        END)
        AS physical_stock,

        sum(case when inv_type = '8001'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved,

        sum(case when inv_type = '8011'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved_offline,

        sum(case when inv_type = '8012'
        then inv_amount
        else 0
        end)
        as pos_completed,

        sum(case when inv_type = '8013'
        then inv_amount
        else 0
        end)
        as total_in,

        sum(case when inv_type = '8014'
        then inv_amount
        else 0
        end)
        as total_out,

        sum(case when inv_type = '8000' OR inv_type = '8001' OR inv_type = '8011' OR inv_type = '8010' OR inv_type = '8012' OR inv_type = '8013'OR inv_type = '8014'
        then inv_amount
        else 0
        end)
        as virtual_stock

        FROM wh_inventories
        GROUP BY product_code, outlet_id
        ORDER BY outlet_id ASC
        ");
    }

    public static function createCustomer($role, $verify = true, $country_id)
    {
        // Users table.
        $user = new User;
        $user->password = Hash::make($role . '1234');
        $user->email = $role . '1@email.com';

        if ($verify) $user->email_verified_at = Carbon::now();
        $user->save();

        // Generating new customer account id.
        $largestCustomerId = UserInfo::largestCustomerId() + 1;

        // User_infos table.
        $userInfo = new UserInfo;
        $userInfo->user_id = $user->id;
        $userInfo->account_id = $largestCustomerId;
        $userInfo->user_level = 6001;

        $userInfo->account_status  = 1;

        $userInfo->full_name = $role;
        $userInfo->nric = 1234;
        $userInfo->referrer_id = 1;
        $userInfo->group_id = 13;
        $userInfo->save();

        // Users table.
        $userCountry = new UserCountry;
        $userCountry->account_id = $largestCustomerId;
        $userCountry->country_id = $country_id;
        $userCountry->referrer_id = $country_id . '1010';
        $userCountry->group_id = 13;
        $userCountry->save();

        // User_addresses table(two records - billing address and shipping address)
        $userAddress_billing_address = new UserAddress;
        $userAddress_billing_address->account_id = $userInfo->account_id;
        $userAddress_billing_address->address_1 = 1;
        $userAddress_billing_address->city_key = 0;
        $userAddress_billing_address->state_id = 14;
        $userAddress_billing_address->country_id = $country_id;
        $userAddress_billing_address->is_shipping_address = 1;
        $userAddress_billing_address->is_residential_address = 1;
        $userAddress_billing_address->is_mailing_address = 1;
        $userAddress_billing_address->save();

        // User_contacts table (Home).
        $userContactHome = new UserContact;
        $userContactHome->account_id = $userInfo->account_id;
        $userContactHome->contact_num = 1;
        $userContactHome->is_home = 1;
        $userContactHome->is_mobile = 1;
        $userContactHome->is_office = 1;

        $userContactHome->save();

        $user->assignRole('wh_' . $role);

        return $user;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_transfer');
        Schema::dropIfExists('wh_transfer_item');
    }
}
