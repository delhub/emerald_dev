@extends('layouts.management.main-management')
@section('content')
    <style>
        html * {
            font-family: 'Lato', sans-serif;
        }


        .box {
            box-shadow: 0px 1px 5px 0px #ccc;
            margin: 0px 0px 40px 0px;
            background-color: #fff;
            border-radius: 10px;
            padding: 15px 25px;

        }

        /* .half-box {
          height: 49vh;
        } */

        .box-header {
            background-color: transparent;
            border: none;

        }

        .box-subtitle {
            text-align: right;
            color: #ffcc00;
            font-size: 15px;
            font-weight: 500;

            vertical-align: bottom;
        }

        .box-title {
            margin-bottom: 5px;
            font-size: 16px;
            font-weight: 800;

        }

        .box-body {
            padding: 0px 20px 10px 20px;
        }

        .bar-legend,
        .donut-legend ul li {
            list-style: none;
            text-align: left;
            line-height: 1.9;

        }

        .agent-name,
        .group-name {
            text-align: start;
            font-size: 21px;
        }

        .agent-sales,
        .group-sales {
            text-align: end;
            font-size: 21px;
            font-weight: 800;
        }

        .bar-legend,
        .donut-legend ul li span {
            width: 15px;
            height: 15px;
            display: inline-block;
            margin-right: 5px;
            list-style-position: inside;
        }

        .apexcharts-toolbar {
            display: none;
        }



        .middle-box {
            /* height: 100vh; */
            box-shadow: 0px 1px 20px 0px grey;
            margin: 0px 0px 20px 0px;
            width: 95%;
            border-radius: 20px;
            padding: 20px;
        }

        .amountSale {
            line-height: 1.5rem;
            font-weight: 900;
            font-size: 18px;
            margin-bottom: 0;
        }

        .imgTotal {
            width: 85px;
        }

        @media screen and (max-width: 600px) {
            .sales-data .sales-product .col-md-4 {

                text-align: left;
            }

            .donut-legend {
                visibility: hidden;
                clear: both;
                float: left;
                margin: 10px auto 5px 20px;
                width: 28%;
                display: none;
            }

            .sale-list tr {
                border-bottom: 1px solid #d3d3d37a;
            }

            .amountSale {
                font-size: 25px;
            }

            .imgTotal {
                width: 65px;
                px
            }

            .app-body {
                margin-top: 65px;
            }
        }

        .numberCircle {
            border-radius: 100%;
            min-width: 20px;
            min-width: 20px;
            padding: 4px 6px;
            display: inline-block;
            background: #ffcc00;
            border: 2px solid #ffcc00;
            color: #000;
            text-align: center;
            line-height: 10px;
            margin-right: 5px;
            font: 10px Arial, sans-serif;
            font-weight: bolder
        }

        .itemTitle {

            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .uppercase {
            color: #ffcc00;
            text-transform: uppercase;
        }

        .dateTitle {
            font-size: 18px;
            color: #999;
        }

        .sales-data .sales-product .col-md-4,
        .sales-data .col-12 {
            margin-bottom: 0px !important;
        }

        .date {


            text-align: center;
            font-size: 120%;
            font-weight: bold;
        }

        .input-group {
            margin: 0 auto;

        }

    </style>

    <div class="sales-data">
        <div class="row">
            <div class="col-12 text-center">
                <h2><strong>Executive Sales Report Summary</strong></h3>
                    <div class="col-12 col-md-4 btn-group pb-3">
                        <input type="text" name="sales_date" id="sales_date" class="form-control date"
                            data-date="{{ $currentday }}">
                        <input type="submit" id="date_submit" class="btn btn-primary" value="Change Date">
                    </div>

                    <br />
            </div>
        </div>
        <div class="row">

            <div class="col-md-3">
                <div class="box">
                    <div class="row">
                        <div class="col-12">
                            <h4><strong>Sales Summary</strong>
                            </h4>
                            <hr />
                        </div>
                        <div class="col-12">
                            <p class="box-title uppercase">Daily Sales</p>
                            <p><span class="amountSale">{{ $currency }}
                                    {{ number_format($daydata['saledata']['total'], 2) }}</span>
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    ({{ $daydata['saledata']['case'] }} Case) </span><br />
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    (-{{ $currency }} {{ number_format($daydata['voucherdata']['total'], 2) }} -
                                    {{ $daydata['voucherdata']['case'] }} Discounted) </span>
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    ({{ $currency }} {{ number_format($daydata['mpdata']['total'], 2) }} -
                                    {{ $daydata['mpdata']['case'] }} Mes Pay) </span>

                            </p>

                        </div>
                        <div class="col-12">
                            <p class="box-title uppercase">Weekly Sales</p>
                            <p><span class="amountSale">{{ $currency }}
                                    {{ number_format($weekdata['saledata']['total'], 2) }}</span>
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    ({{ $weekdata['saledata']['case'] }} Case) </span><br />
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    (-{{ $currency }} {{ number_format($weekdata['voucherdata']['total'], 2) }}  -
                                    {{ $weekdata['voucherdata']['case'] }} Discounted) </span>
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    ({{ $currency }} {{ number_format($weekdata['mpdata']['total'], 2) }} -
                                    {{ $weekdata['mpdata']['case'] }} Mes Pay) </span>

                            </p>
                        </div>
                        <div class="col-12">
                            <p class="box-title uppercase">Monthly Sales</p>
                            <p><span class="amountSale">{{ $currency }}
                                    {{ number_format($monthdata['saledata']['total'], 2) }}</span>
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    ({{ $monthdata['saledata']['case'] }} Case) </span><br />
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    (-{{ $currency }} {{ number_format($monthdata['voucherdata']['total'], 2) }} -
                                    {{ $monthdata['voucherdata']['case'] }} Discounted) </span>
                                <span class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                                    ({{ $currency }} {{ number_format($monthdata['mpdata']['total'], 2) }} -
                                    {{ $monthdata['mpdata']['case'] }} Mes Pay) </span>
                            </p>
                        </div>

                    </div>
                </div>
                <div class="box">
                    <div class="row">
                        <div class="col-12">
                            <h4><strong>Account Registrations</strong></h4>
                            <hr />
                        </div>
                        <div class="col-12">

                            <p class="amountSale">{{ $registrations['agents'] }} Total Agents
                                <br />
                                {{ $registrations['custs'] }} Total Customers
                            </p>
                            <br />
                            <p class="box-title uppercase">Today</p>
                            <p class="subSale">{{ $daydata['agents'] }} New Agents
                                <br />
                                {{ $daydata['custs'] }} New Customers
                            </p>
                            <p class="box-title uppercase">This Month</p>
                            <p class="subSale">{{ $monthdata['agents'] }} New Agents
                                <br />
                                {{ $monthdata['custs'] }} New Customers
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="row">
                        <div class="col-12">
                            <h4><strong>Weekly Top 20 Products</strong></h4>
                            <hr />
                        </div>



                        @foreach ($weekdata['actual_data'] as $datakey => $salesdata)
                            <div class="col-md-6 sales-product">
                                <p style='font-size:14px' class="itemTitle"><span
                                        class="numberCircle">{{ $salesdata }}</span>
                                    <strong>{{ $datakey }}</strong>
                                </p>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="box">
                    <div class="row">
                        <div class="col-12">
                            <h4><strong>Weekly Area Performance</strong></h4>
                            <hr />
                        </div>


                        @foreach ($weekdata['area'] as $datakey => $areadata)
                            <div class="col-md-6 sales-product">
                                <p style='font-size:14px' class="itemTitle"><span
                                        class="numberCircle">{{ $areadata['case'] }}</span> <strong>
                                        {{ $states[$datakey] }} -
                                        {{ $currency }}{{ number_format($areadata['total'], 2) }} -
                                        {{ $areadata['case'] }} Cases
                                    </strong>
                                </p>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="row">
                        <div class="col-12">
                            <h4><strong>Weekly Group Performance</strong></h4>
                            <hr />
                        </div>
                        <div class="col-12 sales-product">


                            @foreach ($weekdata['group'] as $datakey => $groupdata)
                                <p style='font-size:14px' class="itemTitle"><span
                                        class="numberCircle">{{ $groupdata['case'] }}</span> <strong>
                                        <a href="/management/sales/group/{{ $datakey }}/">{{ $groups[$datakey] }}
                                            Group</a> - {{ $currency }}{{ number_format($groupdata['total'], 2) }}
                                        - {{ $groupdata['new'] }} New Agent</strong>
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    @push('script')
        <script>
            $(function() {
                $(".date").datepicker({

                    dateFormat: 'dd/mm/yy',

                    onSelect: function(dateText) {
                        location.href = '/management/sales/' + dateText;
                    }
                });


                date = $(".date").data("date");
                $(".date").val(date);

                $("#date_submit").click(function() {

                    $(".date").datepicker("show");

                });
            });
        </script>
    @endpush
@endsection
