<?php

namespace App\Traits;

use App\Models\Globals\Image;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Filesystem\Filesystem;
use App\Models\Globals\Products\Product as GP;
use App\Models\Products\ProductAttribute as PA;

trait ImportImage
{
    public function importImage($data)
    {
        if (count($data)) {
            foreach ($data as $k => $v) {
                if (!empty($v['web_fixed_price'])) {
                    if (strtolower($v['type']) == 'product') {
                        $imgId = GP::where('product_code', $v['product_code_main'])->first('id');

                        if ($imgId) {
                            $oImage = Image::where([['imageable_id', $imgId->id], ['imageable_type', 'App\Models\Globals\Products\Product']])->delete();
                        }

                        $storagePath  = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
                        $tempFolder = $storagePath . 'public/uploads/temp/formula2u/' . $v['product_code_main'];

                        $publicPath = public_path();

                        if (file_exists($tempFolder)) {
                            $files = \File::files($tempFolder);

                            if (count($files) > 0) {
                                $img = [];
                                foreach ($files as $val) {
                                    $img[] = pathinfo($val);
                                }

                                // \Storage::move($tempFolder, 'OtherFolder');
                                \File::ensureDirectoryExists(public_path() . '/storage/uploads/images/products');

                                $file = new Filesystem();
                                $file->moveDirectory($tempFolder, public_path() . "/storage/uploads/images/products/" . $v['product_code_main'], true);
                            }

                            if (count($img) > 0) {
                                foreach ($img as $vi) {
                                    $this->InsertImage($imgId->id, $vi, $v['product_code_main']);
                                }
                            }

                            $this->setAttributeColorImages($v);
                        }
                    } else {
                        $this->setAttributeColorImages($v);
                    }
                }
            }
        }
    }

    public function setAttributeColorImages($data)
    {
        $path = "uploads/images/products/" . $data['product_code_main'] . "/";
        $getImgId = Image::where('path', $path)->get();

        if (count($getImgId) > 0) {
            foreach ($getImgId as $val) {
                $name = explode('.', $val['filename']);
                $val['fileid'] = $name[0];
                $data[] = $val;
            }

            $default_img = isset($data['attribute_image_id']) ? $data['attribute_image_id'] : 1;

            $img = collect($data)->where('fileid', $default_img)->first();

            $pa = PA::where('product_code', $data['product_code'])->first();

            $pa->color_images = $img->id;

            $pa->save();
        }
    }

    public function InsertImage($id, $data, $product_code)
    {
        $image = new Image;

        $image->path = "uploads/images/products/" . $product_code . "/";
        $image->filename = $data['basename'];
        $image->default = $data['filename'] == 1 ? 1 : 0;
        $image->brand = 0;
        $image->imageable_id = $id;
        $image->imageable_type = 'App\Models\Globals\Products\Product';

        $image->save();

        return $data;
    }
}
