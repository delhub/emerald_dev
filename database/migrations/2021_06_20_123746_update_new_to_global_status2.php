<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewToGlobalStatus2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_statuses')
            ->where('id', 6000)
            ->update(['description' => json_encode(['amount' => 0,'range_yearly' => 1,'maintenance_fee' => 0, 'discount' => 0])]
        );

        DB::table('global_statuses')
            ->where('id', 6001)
            ->update(['description' => json_encode(['amount' => 500,'range_yearly' => 1,'maintenance_fee' => 699, 'discount' => 5])]
        );

        DB::table('global_statuses')
            ->where('id', 6002)
            ->update(['description' => json_encode(['amount' => 1000,'range_yearly' => 3,'maintenance_fee' => 799, 'discount' => 10])]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
