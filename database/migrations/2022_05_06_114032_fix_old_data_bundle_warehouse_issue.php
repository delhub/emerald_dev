<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Purchases\Order;
use App\Models\Pick\PickBatch;
use App\Models\Pick\PickBatchDetail;
use App\Http\Controllers\Warehouse\Bunbundle\BunbundleController;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundle;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundleItem;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Warehouse\Order\WarehouseOrder;

class FixOldDataBundleWarehouseIssue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $theDate = Carbon::createFromFormat('Y-m-d H:i:s', '2022-04-29 16:12:00');
        $theDate = Carbon::parse($theDate);

        $pickBatch = PickBatch::orderBy('created_at', 'desc')
            ->where('pick_pack_status', 6)
            ->where('created_at', '<', $theDate)
            ->get();

        foreach ($pickBatch as $key => $batch) {

            //update orders
            $batchDetail = PickBatchDetail::where('key', $batch->id)->get('delivery_order');

            $orders = Order::whereIn('delivery_order', $batchDetail)->get();

            foreach ($orders as $orderKey => $order) {

                $fromProducts = $toProduct = array();
                foreach ($order->items as $key => $item) {

                    if (isset($item->bundleChild) && ($item->bundleChild->first() != NULL) && $item->bundleChild->first()->active == 2) {
                        foreach ($item->bundleChild as $key => $bundle) {
                            $fromProducts[$bundle->primary_product_code] = ($bundle->primary_quantity * $item->quantity);
                        }
                        $toProduct['product_code'] = $item->product_code;
                        $toProduct['type'] = 'bundle';
                        $toProduct['quantity'] = $item->quantity;
                        $toProduct['remark'] = 'fix' . $item->delivery_order;

                        $batchDetailsss = PickBatchDetail::where('delivery_order', $item->delivery_order)->first();
                    }

                    if (isset($fromProducts) && $toProduct && isset($batchDetailsss)) {
                        foreach ($fromProducts as $code => $vv) {
                            $bundleProductCodeOnly[] = $code;
                        }
                        $batchItemss = WarehouseBatchItem::selectRaw('wh_batch_item.*')
                            ->join('wh_batch_detail', 'wh_batch_detail.id', '=', 'wh_batch_item.detail_id')
                            ->where('wh_batch_item.type', 'pick')
                            ->where('wh_batch_item.picking_order_id', $batchDetailsss->id)
                            ->whereIn('wh_batch_detail.product_code', $bundleProductCodeOnly)
                            ->get();

                        if ($batchItemss->isNotEmpty()) {
                            $warehouseOrders = WarehouseOrder::whereIn('delivery_order', $batchDetail)->first();

                            $warehouseBunbundle = new WarehouseBunbundle;
                            $warehouseBunbundle->user_id = 0;
                            $warehouseBunbundle->location_id = $warehouseOrders->location_id;
                            $warehouseBunbundle->type = $toProduct['type'];
                            $warehouseBunbundle->from_product_code = '';
                            $warehouseBunbundle->product_code = $toProduct['product_code'];
                            $warehouseBunbundle->quantity = $toProduct['quantity'];
                            $warehouseBunbundle->expiry_date = '';
                            $warehouseBunbundle->panel_id = 2011000000;
                            $warehouseBunbundle->bundle_type = 1;
                            $warehouseBunbundle->remark = $toProduct['remark'] . json_encode($fromProducts);

                            $warehouseBunbundle->save();
                            //check why empty
                            foreach ($batchItemss as $key => $bItem) {
                                $transferItem = new WarehouseBunbundleItem;

                                $transferItem->b_unbundle_key = $warehouseBunbundle->id;
                                $transferItem->batch_item_id = $bItem->id;
                                $transferItem->save();
                            }

                            //auto create bundle/batch/mark batch item
                            $theRequest = new Request();
                            $theRequest->stockTransferId = $warehouseBunbundle->id;
                            $theRequest->picking_order_id = $batchDetailsss->id;
                            $theRequest->process = 'pick';
                            $theRequest->imAuto = 'yes';

                            $bundleResult = BunbundleController::end($theRequest);
                        }
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
