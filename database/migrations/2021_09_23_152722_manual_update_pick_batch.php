<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Pick\PickBatch;
use Illuminate\Support\Carbon;


class ManualUpdatePickBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // manual update those 2021-09-21 batch to sort end as warehouse lost track
        $start = Carbon::now()->subDays(30)->startOfDay();
        $end = Carbon::createFromFormat('Y-m-d H:i:s', '2021-09-21 23:59:59');

        $allPickBatch = PickBatch::whereBetween('created_at', [$start, $end])->get();

        foreach ($allPickBatch as $key => $batch) {
            $batch->pick_pack_status = 4;
            $batch->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
