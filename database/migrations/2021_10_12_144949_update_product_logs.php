<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Globals\Products\Product;
use App\Models\Products\ProductBrand;
use App\Models\Products\ProductBrandLogs;
use Carbon\Carbon;

class UpdateProductLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('product_brand_logs')->insert([
            'log_month' => Carbon::now(),
            'brand_count' => ProductBrand::count(),
            'product_count' => Product::whereIn('product_status',[1,2])->where('brand_id','!=',0)->count(),
            'new_brand_count' => 0,
            'new_product_count' => 0,
            'product_suspended' => Product::where('display_only',1)->whereIn('product_status',[1,2])->where('brand_id','!=',0)->count(),
            'product_terminate' => Product::where('product_status',2)->where('display_only',0)->where('brand_id','!=',0)->count(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
