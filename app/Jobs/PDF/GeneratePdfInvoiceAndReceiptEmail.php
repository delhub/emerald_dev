<?php

namespace App\Jobs\PDF;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Emails\SendInvoiceAndReceiptEmail;
use App\Models\Globals\Countries;
use PDF;
use App;
use Illuminate\Support\Facades\File;

class GeneratePdfInvoiceAndReceiptEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    protected $emailAddress;
    public $purchase;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($purchase, $emailAddress = null)
    {
        $this->purchase = $purchase;
        $this->emailAddress = $emailAddress;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        self::createInvoiceReceiptPDF($this->purchase);

        // Queue Invoice email to customer.
        if ($this->emailAddress) {
            //SendInvoiceAndReceiptEmail::dispatch($this->emailAddress, $this->purchase);
        }
    }

    public static function createInvoiceReceiptPDF($purchase)

    {
        if (!App::environment('localhost')) {
            // if ($purchase->invoice_version == 0) {
            // } else {
            //     $purchase->increment('invoice_version');
            // }
            // $countries = Countries::getAll();

            // $country = $countries[$purchase->country_id];

            // $headerHtml = view()->make('documents.purchase.v2.header-mysg',  compact('purchase','country'))->render();
            // $footerHtml = view()->make('documents.purchase.v2.footer-sg',  compact('purchase'))->render();
            // $fileName = $purchase->getFormattedNumber();

            // $optionsMy = [
            //     'footer-html'   => $footerHtml,
            //     'header-html'      => $headerHtml,
            //     'margin-bottom'   => '12.5mm',
            //     'margin-top'   => '86.5mm'
            // ];

            // $optionsSg = [
            //     'footer-html'   => $footerHtml,
            //     'header-html'      => $headerHtml,
            //     'margin-bottom'   => '12.5mm',
            //     'margin-top'   => '91mm'
            // ];

            //Create our PDF with the main view and set the options
            $pdf = PDF::loadView('documents.purchase.v2.invoice-v2',  compact('purchase'))->setPaper('a4');

            // switch ($purchase->country_id) {

            //     case "SG":
            //         $pdf->setOptions($optionsSg);
            //         break;
            //     case "MY":
            //     default:
            //         $pdf->setOptions($optionsMy);
            //         break;
            // }
            // return $pdf->setPaper('a4')->stream($fileName . '.pdf');

            // Generate Invoice PDF.
            // Generate PDF.
            // $pdf = PDF::loadView('documents.purchase.v2.invoice', compact('purchase'));
            // Get PDF content.
            $contentTax = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . '/v'.$purchase->invoice_version . '/');
            // Set PDF file name.
            $pdfNameTax = $purchase->getFormattedNumber();
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfNameTax . '.pdf', $contentTax);

            // Generate Receipt PDF.
            // Generate PDF.
            // $countries = Countries::getAll();

            // $country = $countries[$purchase->country_id];

            $pdf = PDF::loadView('documents.purchase.v2.receipt-v2', compact('purchase'))->setPaper('a4');

            $pdf->setOptions([
                'footer-html'   => '',
                'header-html'      => '',
                'margin-bottom'   => 0,
                'margin-top'   => 0
            ]);

            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->getFormattedNumber() . '/v'.$purchase->invoice_version.'/');
            // Set PDF file name.
            $pdfName = $purchase->getFormattedNumber() . '-receipt';
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }
            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);
        } else {
            \Log::info('simulate.invoice.receipt.pdf.created');
        }
    }
}
