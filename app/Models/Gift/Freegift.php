<?php

namespace App\Models\Gift;

use Illuminate\Database\Eloquent\Model;

class Freegift extends Model
{
    // Table Name
    protected $table = 'freegift';
    // Primary Key
    public $primaryKey = 'id';
    protected $fillable = [
        'gift_name',
        'outlet_id',
        'gifted',
        'contains',
        'quantity',
        'gift_type',
        'gift_product',
        'gift_price_value',
        'gift_price_percentage',
        'start_date',
        'end_date'
    ];
}
