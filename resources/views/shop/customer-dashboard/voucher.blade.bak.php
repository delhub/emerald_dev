@extends('layouts.management.main-customer')



@section('content')

@section('page_title')
{{ "Voucher Code List" }}
@endsection

<div class="container-fluid p-0">
  <div class="row mb-4">
    <div class="col-md-12 col-12">
      <div class="card" style="border-radius: 10px;box-shadow: #d8d8d8 0 0 8px 0px;">
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <h4 class="card-title  font-size my" style="font-weight:700;margin: 0;padding: 0;line-height: 30px;">Voucher Code</h4>
            </div>
            <div class="col-6">
                    <div class="form-group row">
                      <label for="text" class="col-4 col-form-label"></label>
                      <div class="mysearch01">
                        <div class="input-group">
                          <input id="search" name="text" placeholder="Search" type="text" class="form-control" autocomplete="off">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <i class="fa fa-search"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      {{-- <div class="col-8">
                        <div class="input-group dd">
                          <input id="search" name="text" placeholder="Search" type="text" class="form-control" autocomplete="off">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <i class="fa fa-search"></i>
                            </div>
                          </div>
                        </div>
                      </div> --}}
                    </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="overflow: auto;">
            <table class="table table-striped mt-3 voucher-tb" id="dtv" {{--style="overflow-x: scroll"--}}>
                <thead>
                  <tr>
                    <td class="text-muted text-bold small">EXPIRED DATE</td>
                    <td class="text-muted text-bold small">CREATED BY INVOICE</td>
                    <td class="text-muted text-bold small">DISCOUNT AMOUNT</td>
                    <td class="text-muted text-bold small">VOUCHER CODE</td>
                    <td class="text-muted text-bold small">Usage</td>
                    <td class="text-muted text-bold small">Last Usage</td>
                    <td scope="col">Action</td>
                  </tr>
                </thead>
                <tbody>
                    @foreach($voucher as $code)
                    <tr class="{{$code->coupon_status == 'valid' ? 'text-primary' : 'text-secondary'}}">
                        <th>{{$code->expiryf}}</th>
                        <th>{{$code->created_by_purchase}}</th>
                        <th>RM {{$code->coupon_amount}}</th>
                        <th id="voucher{{$loop->iteration}}">{{$code->coupon_code}}</th>
                        <th>{{$code->usage_counter}}/{{$code->max_voucher_usage}}</th>
                        <td>{{$code->lastUsage}}</td>
                        <td>
                            @if($code->coupon_status =='expired')
                              <button type="button" class="btn vr-filt-btn" data-clipboard-target="#voucher{{$loop->iteration}}" disabled><strong>Expired</strong>
                            @elseif($code->coupon_status =='used')
                              <strong> Used </strong>
                            @else
                              <button type="button" class="btn vr-filt-btn" data-clipboard-target="#voucher{{$loop->iteration}}"><strong> Copy </strong>
                            @endif
                        </button></td>
                      </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>

</div>

<style>
  .mysearch01{
    width: 200px;
  }
  .mobile-image {
    display: none;
  }

  .border-radius-card {
    border-radius: 15px;

  }

  .sub-border-color {
    border: 2pt solid #fbcc34;
  }

  @media(max-width:425px) {
    .font-size.my{
      line-height: 60px!important;
    }
    .mobile-image {
      display: block;
    }

    .web-image {
      display: none;
    }
  }

  @media(max-width:375px) {
    .font-size.my{
      font-size: 18px;
    }
  }

  @media(max-width:330px) {
    .font-size.my{
      font-size: 15px;
    }
  }

  @media(min-width:767px) {
    .hidden-md {
      display: none;
    }

    .font-size {
      font-size: 15pt;
    }
  }

  /*Laptop screen*/
  @media (max-width: 1800px) and (min-width: 1200px) {
    .font-size {
      font-size: 13pt;
    }

    .button-size-laptop {
      font-size: 7pt;
    }

  }
  .dataTables_filter { display: none; }
</style>

@endsection
@push('script')
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"></script>
<script>

$('#toast').toast('show');

oTable = $('#dtv').DataTable();
$('#search').keyup(function(){
      oTable.search($(this).val()).draw() ;
})

var clipboard = new ClipboardJS('.btn');

clipboard.on('success', function(e) {
console.info('Action:', e.action);
console.info('Text:', e.text);
console.info('Trigger:', e.trigger);

e.clearSelection();
});

clipboard.on('error', function(e) {
console.error('Action:', e.action);
console.error('Trigger:', e.trigger);
});

</script>
@endpush
