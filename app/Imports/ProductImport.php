<?php

namespace App\Imports;

use App\Models\Globals\Products\Product as GP;
use App\Models\Products\Product as PP;
use App\Models\Products\ProductAttribute as PA;
use App\Models\Products\ProductPrices;
use App\Models\Globals\Countries;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Users\Panels\PanelAddress;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Categories\Category;
use App\Models\Categories\PivCategory;


class ProductImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // return;
        // dd($row);

        //if fixed_price != 0 import
        if (!empty($row[24])) {

            switch ($row[0]) {
                case 'attribute':
                    $gp = GP::where('product_code', $row[1])->first();

                    if ($gp) {

                        $pp = PP::where('global_product_id', $gp->id)->first('id');

                        // panel product attribute
                        $pa = PA::where([['panel_product_id', $pp->id], ['product_code', $row[2]]])->first();

                        if (!$pa) {
                            $pa = new PA;
                        } else {
                            //$pa->delete();
                        }

                        $pa->panel_product_id = $pp->id;
                        $pa->attribute_type = 'size';
                        $pa->attribute_name = $row[4];
                        $pa->product_sv = isset($row[23]) ? $row[23] : 0;
                        // $pa->color_images = $row[30];
                        $pa->product_code = $row[2];
                        $pa->product_measurement_weight = isset($row[39]) ? $row[39] / 1000  : 0;

                        $pa->save();

                        $this->InsertPanelProductPrice($row, $pa->id, 'attribute');
                    }

                    break;
                    // case 2:
                    //     break;
                default:
                    // Global product
                    $gp = GP::where('product_code', $row[1])->first();

                    if (!$gp) {
                        $gp = new GP;
                    }

                    if (isset($row[34])) {
                        $video_link = \explode(',', $row[34]);

                        $product_data = [
                            'video' => [
                                'link' => $video_link,
                                'introduction' => $row[35],
                                'recommend' => $row[36],
                            ]
                        ];
                    } else {
                        $product_data = [];
                    }

                    $gp->product_code = $row[1];
                    $gp->moh_code = isset($row[33]) ? $row[33] : null;
                    $gp->name = $row[3];
                    $gp->name_slug = str_slug($row[3]);
                    $gp->quality_id = isset($row[13]) ? $row[13] : 1;
                    $gp->product_status = isset($row[14]) ? $row[14] : 1;
                    $gp->eligible_discount = isset($row[15]) ? $row[15] : 0;
                    $gp->generate_discount = isset($row[16]) ? $row[16] : 0;
                    $gp->can_remark = isset($row[17]) ? $row[17] : 0;
                    $gp->can_selfcollect = isset($row[18]) ? $row[18] : 0;
                    $gp->can_preorder = isset($row[19]) ? $row[19] : 0;
                    $gp->can_installment = '{"loanAmount":null,"firstPayment":null,"monthlyPayment":null,"interestRate":null,"tenure":null,"processingFee":null,"productDescription":""}';
                    $gp->data = json_encode($product_data);
                    $gp->hot_selection = isset($row[37]) ? $row[37] : 0;
                    $gp->key_selection = isset($row[38]) ? $row[38] : 0;

                    $gp->save();

                    // Insert/update Panel Info
                    $this->InsertPanelInfo($row);
                    // Insert/update Panel Address
                    $this->InsertPanelAddress($row);

                    // Panel Product
                    $pp = PP::where('global_product_id', $gp->id)->first();

                    if (!$pp) {
                        $pp = new PP;
                    }

                    $account_id = (isset($row[20]) ? $row[20] : 2011000000);

                    $data_origin = isset($row[7]) ? $row[7] : 'Malaysia';

                    $origin = Category::where([['name', $data_origin], ['type', 'origin_country']])->first();

                    $pivMultiple = PivCategory::where([['product_id', $gp->id], ['category_id', $origin->id]])->get();

                    if (count($pivMultiple) > 1) {
                        PivCategory::where([['product_id', $gp->id], ['category_id', $origin->id]])->delete();
                    }

                    //insert piv origin
                    if ($origin) {
                        $pivCat = PivCategory::where([['product_id', $gp->id], ['category_id', $origin->id]])->first();

                        if (!$pivCat) {
                            $pivCat = new PivCategory;
                        }

                        $pivCat->product_id = $gp->id;
                        $pivCat->category_id = $origin->id;
                        $pivCat->save();
                    }

                    // dd($pivCat = PivCategory::where([['product_id', $gp->id], ['category_id', $category->id]])->first());

                    $pp->global_product_id = $gp->id;
                    $pp->panel_account_id = $account_id;
                    $pp->display_panel_name = isset($row[21]) ? $row[21] : 'FORMULA HEALTHCARE SDN. BHD';
                    $pp->short_description =  isset($row[5]) ? $row[5] : NULL;
                    $pp->product_description = isset($row[6]) ? $row[6] : NULL;
                    $pp->place_of_origin = json_encode([$origin->id]);
                    $pp->product_content_1 = isset($row[8]) ? $row[8] : NULL;
                    $pp->product_content_2 = isset($row[9]) ? $row[9] : NULL;
                    $pp->product_content_3 = isset($row[10]) ? $row[10] : NULL;
                    $pp->product_content_4 = isset($row[11]) ? $row[11] : NULL;
                    $pp->product_content_5 = isset($row[12]) ? $row[12] : NULL;
                    // $pp->fixed_showitem = isset($row[24]) ? $row[24] : 0;
                    $pp->fixed_price = isset($row[24]) ? ($row[24] * 100) : 0;
                    $pp->outlet_price = isset($row[25]) ? ($row[25] * 100) : 0;
                    $pp->standard_price = isset($row[26]) ? ($row[26] * 100) : 0;
                    $pp->estimate_ship_out_date = isset($row[22]) ? $row[22] : 0;
                    $pp->product_sv = isset($row[23]) ? $row[23] : 0;
                    $pp->origin_state_id = 0;
                    // $pp->shipping_category = 0;

                    $pp->save();

                    // $this->InsertPanelProductPrice($row,$pp->id,'product');

                    // panel product attribute
                    $pa = PA::where([['panel_product_id', $pp->id], ['product_code', $row[2]]])->first();

                    if (!$pa) {
                        $pa = new PA;
                    } else {
                        // $dpa = PA::where([['panel_product_id',$pp->id],['product_code',$row[2]]])->delete();
                    }

                    $pa->panel_product_id = $pp->id;
                    $pa->attribute_type = 'size';
                    $pa->attribute_name = $row[4];
                    $pa->product_sv = isset($row[23]) ? $row[23] : 0;
                    // $pa->color_images = $row[30];
                    $pa->product_code = $row[2];
                    $pa->product_measurement_weight = isset($row[39]) ? $row[39] / 1000  : 0;

                    $pa->save();

                    $this->InsertPanelProductPrice($row, $pa->id, 'attribute');

                    $category = Category::where([['name', $row[27]], ['type', 'shop']])->first();

                    //insert piv category
                    if (isset($row[27])) {
                        $pivCats = PivCategory::where([['product_id', $gp->id], ['category_id', $category->id]])->first();

                        if (!$pivCats) {
                            $pivCats = new PivCategory;
                        }

                        $pivCats->product_id = $gp->id;
                        $pivCats->category_id = $category->id;
                        $pivCats->save();
                    }
            }
        }
    }

    public function InsertPanelProductPrice($row, $id, $attribute)
    {

        $country = Countries::whereIn('country_id', ['MY', 'SG'])->get('country_id');

        // $model_type = $attribute == 'product' ? 'App\Models\Products\Product' : 'App\Models\Products\ProductAttribute';
        $model_type = 'App\Models\Products\ProductAttribute';

        if (count($country) > 0) {

            foreach ($country as $val) {

                $fixed_price = ($val['country_id'] == 'MY') ? $row[24] * 100 : 0;
                $price = ($val['country_id'] == 'MY') ? $row[25] * 100 : 0;
                $member_price = ($val['country_id'] == 'MY') ? $row[26] * 100 : 0;

                $pPrice = ProductPrices::where([
                    ['model_id', $id], ['model_type', $model_type], ['country_id', $val['country_id']],
                    ['fixed_price', $fixed_price], ['price', $price], ['member_price', $member_price]
                ])->first();

                if (!$pPrice) {
                    $inactiveThis  = ProductPrices::where('model_id', $id)->where('country_id', $val['country_id'])->get();

                    if (count($inactiveThis)) {
                        foreach ($inactiveThis as $key => $inactive) $inactive->active = 0;
                        $inactive->save();
                    }

                    $maxPricekey = ProductPrices::where('model_type', $model_type)->where('country_id', $val['country_id'])->where('model_id', $id)->max('price_key');

                    $pPrice = new ProductPrices;
                    $pPrice->price_key = $maxPricekey + 1;
                }

                $pPrice->model_type = $model_type;
                $pPrice->model_id = $id;
                $pPrice->product_code = $row[2];
                $pPrice->country_id = $val['country_id'];
                $pPrice->product_sv = isset($row[23]) ? $row[23] : 0;
                $pPrice->fixed_price = $fixed_price;
                $pPrice->outlet_price = $price;
                $pPrice->standard_price = $member_price;

                $pPrice->save();
            }
        }
    }

    public function InsertPanelInfo($row)
    {
        $account_id = (isset($row[20]) ? $row[20] : 2011000000);

        $pi = PanelInfo::where('account_id', $account_id)->first();

        if (!$pi) {
            $pi = new PanelInfo;
        }
        $pi->user_id = time();
        $pi->account_id = $account_id;
        $pi->account_status = 1;
        $pi->company_name = (isset($row[21]) ? $row[21] : 'Formula2u');
        $pi->ssm_number = 1;
        $pi->company_email = 1;
        $pi->company_phone = 1;
        $pi->pic_name = 1;
        $pi->pic_nric = 1;
        $pi->pic_contact = 1;
        $pi->pic_email = 1;
        $pi->name_for_display = (isset($row[21]) ? $row[21] : 'Formula2u');

        $pi->save();
    }

    public function InsertPanelAddress($row)
    {
        $account_id = (isset($row[20]) ? $row[20] : 2011000000);
        $pAdd = PanelAddress::where('account_id', $account_id)->first();

        if (!$pAdd) {
            $pAdd = new PanelAddress;
        }

        $pAdd->account_id = $account_id;
        $pAdd->address_1 = 'address1';
        $pAdd->address_2 = 'address2';
        $pAdd->address_3 = 'address3';
        $pAdd->address_1 = 'address1';
        $pAdd->postcode = 12345;
        $pAdd->city = 'city';
        $pAdd->city_key = 0;
        $pAdd->state_id = 14;
        $pAdd->is_correspondence_address = 1;
        $pAdd->is_billing_address = 1;

        $pAdd->save();
    }
}
