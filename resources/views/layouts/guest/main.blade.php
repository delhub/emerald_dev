<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/register-page.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}?v=3.5" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style2.css') }}?v=3.7">
    <link rel="stylesheet" href="{{ asset('css/responsive2.css') }}?v=3.3">


    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <!-- Expanded Side Menu -->
    <link rel="stylesheet" href="{{ asset('assets/css/expandedsidemenu/expandedsidemenu.css') }}">
    <!-- Custom Scrollbar CDN -->
    <!-- TODO: Import using mix. -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    @stack('style')

    <style>
        body {
            font-family: 'Lato', sans-serif;
        }

        @media (max-width: 576px) {
            .bg-sm {
                background-image: url(/images/bujishu-home-sm-{{ country()->country_id }}.jpg);
            }
        }

        @media (min-width: 768px) {
            .bg-md {

                background-image: url(/images/bujishu-home-lg-{{ country()->country_id }}.jpg);

            }
        }

    </style>


    <script>
        jQuery(function() {
            expandedsidemenu.init({
                menuid: 'mysidebarmenu'
            })
        })
    </script>
</head>

<body class="r-3d-bg">


    @if (!in_array(Request::path(),['account-error','register','register-dealer','launch-package','shop/return-form','register-dealer-sg','email/verify','register-customer','register-customer/resend-email']) && !in_array(request()->route()->uri,['member-info/{agentID}','member-info','register-customer/information']))
        @include('layouts.guest.navigation.navigation')
    @elseif (in_array(Request::path(),['launch-package','account-error']))
        @include('layouts.guest.v1.sg-launch-main')
    @endif

    <main id="body-content-collapse-sidebar">
        <div id="app">
            <div class="fml-container my-pb-3">
                @yield('content')
            </div>
        </div>
    </main>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <!-- Signature Pad -->
    <!-- TODO: Import using mix. -->
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <!--Validate register fields -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    @stack('script')

</body>

</html>
