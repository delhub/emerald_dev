<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Insert1009GlobalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_statuses')->insert(array('id' => 1009, 'name' => 'Order Self Collect Delivered', 'description' => 'Status for order status.'));

        Schema::table('wh_batch', function (Blueprint $table) {
            $table->text('notes')->nullable()->after('batch_type')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
