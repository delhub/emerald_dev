<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;

class CategoryData extends Model
{
    // Set table
    protected $table = 'categories_data';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        // 'name',
        // 'slug',
        // 'type',
        // 'parent_category_id'
    ];

    // public function category()
    // {
    //     return $this->belongsToMany('App\Models\Categories\Category', 'category_id', 'id');
    // }
}