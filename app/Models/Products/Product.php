<?php

namespace App\Models\Products;

use App\Models\Categories\Category;
use Illuminate\Database\Eloquent\Model;
use Auth;
use  App\Models\Globals\Image;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Cookie;
use App\Traits\PriceCalculation;

class Product extends Model
{
    use PriceCalculation;
    // Set table
    protected $table = 'panel_products';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    // Casts
    protected $casts = [
        'product_rating' => 'double',
        'installation_category' => 'array'
    ];

    public function parentProduct()
    {
        return $this->belongsTo('App\Models\Globals\Products\Product', 'global_product_id');
    }

    /**
     * Get shipping / installation category
     *      */
    public function installationCategories()
    {

        if (is_array($this->installation_category)) {
            return Ship_category::with('categoriesRates')->whereIn('id', $this->installation_category)->get();
        }
    }

    public function markupCategories()
    {
        return $this->belongsToMany(
            'App\Models\Products\ProductMarkupCategory',
            'piv_product_markup',
            'product_id',
            'category_id'
        );
    }

    /**
     * Get all attribute of a product.
     */
    public function attributes()
    {
        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')->where('active', 1);
    }
    public function inactiveAttributes()
    {
        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('active', 0);
    }

    public function ratings()
    {
        return $this->hasMany('App\Models\Purchases\Rating', 'panel_id', 'panel_account_id');
    }

    public function bundles()
    {
        return $this->hasMany('App\Models\Products\ProductBundle', 'panel_product_id');
    }

    public function sortBundles()
    {
        $bundles = $this->bundles;

        $sorted = array();

        foreach ($bundles as $bundle) {

            if (!isset($sorted[$bundle->bundle_id])) {
                $sorted[$bundle->bundle_id]['items'] = array();
                $sorted[$bundle->bundle_id]['type'] = $bundle->bundle_type;
                $sorted[$bundle->bundle_id]['title'] = $bundle->bundle_title;
                $sorted[$bundle->bundle_id]['quantity'] = $bundle->bundle_qty;
                $sorted[$bundle->bundle_id]['id'] = $bundle->bundle_id;
            }

            $sorted[$bundle->bundle_id]['items'][] = $bundle;
        }

        return $sorted;
    }


    /**
     * Get all color attributes of a product.
     */
    public function colorAttributes()
    {
        $colors = $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('attribute_type', 'color')->get();

        foreach ($colors as $color) {
            // dd($this);

            if ($color->color_images == null) {

                $imagesColor = Image::where('imageable_id', $this->global_product_id)->where('imageable_type', 'App\Models\Globals\Products\Product')->first();
                //    dd($imagesColor);

                $color->color_images = isset($imagesColor->id) ? $imagesColor->id : null;

                $color->save();
            }
        }

        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('attribute_type', 'color');
    }

    /**
     * Get all size attributes of a product.
     */
    public function sizeAttributes()
    {
        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('attribute_type', 'size');
    }

    /**
     * Get all miscellaneous attributes of a product.
     */
    public function miscellaneousAttributes()
    {
        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('attribute_type', 'miscellaneous');
    }

    /**
     * Get all curtain size attributes of a product.
     */
    public function curtainSizeAttributes()
    {
        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('attribute_type', 'curtain-size');
    }

    /**
     * Get all light temperature of a product.
     */
    public function lightTemperatureAttributes()
    {
        return $this->hasMany('App\Models\Products\ProductAttribute', 'panel_product_id')
            ->where('attribute_type', 'light-temperature');
    }

    /**
     * Get origin state of the product.
     */
    public function originPlace()
    {
        $origins = json_decode($this->place_of_origin);
        return Category::where('type', 'origin_country')->whereIn('id', $origins)->get();
    }

    /**
     * Get the states and delivery fee the product is available in.
     */
    public function deliveries()
    {
        return $this->hasMany('App\Models\Products\ProductDelivery', 'panel_product_id');
    }

    /**
     * Get panel info of a product.
     */
    public function panel()
    {
        return $this->belongsTo('App\Models\Users\Panels\PanelInfo', 'panel_account_id', 'account_id');
    }

    // public function getPrice()
    // {
    //     return $this->morphMany('App\Models\Products\ProductPrices', 'model');
    // }


    /**
     * Get the formatted product's price.
     */
    public function getDecimalPrice()
    {
        return number_format(($this->price / 100), 2);
    }

    /**
     * Get the formatted product's delivery price.
     */
    public function getDecimalDeliveryFee()
    {
        return number_format(($this->delivery_fee / 100), 2);
    }

    /**
     * Get the formatted product's installation price.
     */
    public function getDecimalInstallationFee()
    {
        return number_format(($this->installation_fee / 100), 2);
    }

    public function favorite()
    {
        return $this->hasMany('App\Models\Users\Customers\Favorite', 'product_id');
    }

    public function getLikesAttribute()
    {
        return count($this->favorite);
    }

    public function getIsLikedAttribute()
    {
        //$liked = Favorite::where('user_id', Auth::id())->where('product_id', $this->id)->first();
        if (Auth::id()) {

            $userLiked =  count($this->favorite->where('user_id', Auth::id())) >= 1;
        } else {

            $userLiked = false;
        }


        return $userLiked;
    }

    public function defaultImage()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable')
            // ->where('default', 1)
        ;
    }

    /**
     * product image attribute.
     */
    public function getImageAttributes()
    {
        return $this->hasOneThrough(
            'App\Models\Globals\Image', //owner
            'App\Models\Products\ProductAttribute', //car
            'color_images', // Foreign key on cars table...
            'id', // Foreign key on owners table...
            'id', // Local key on mechanics table...
            'panel_product_id' // Local key on cars table...
        );
    }

    // public function userLevelPrice()
    // {
    //     $user = User::find(Auth::user()->id);
    //     switch ($user->userInfo->user_level) {
    //         case '6000':
    //             $price = $this->getPriceAttributes('fixed_price');
    //             break;
    //         case '6001':
    //             $price = $this->getPriceAttributes('web_offer_price');
    //             break;
    //         case '6002':
    //             $price = $this->getPriceAttributes('member_price');
    //             break;
    //     }
    //     return $price;
    // }
}
