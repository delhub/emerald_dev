@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection
<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Cart Page</a></li>
            </ul>
        </div>
        <section>
            <div class="fml-container">
                <div class="card-home">
                    <div class="modal fade" id="address_edit_form" aria-hidden="true"
                        aria-labelledby="address_edit_formLabel" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box">
                                <div class="modal-header">
                                    <h1 class="modal-title edit-tittle" id="address_edit_formLabel">Edit Your
                                        <span>Details</span></h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                                            <input type="email" class="checkout-box2 form-control"
                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                placeholder="example@gmail.com">
                                            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleInputEmail1" class="form-label">Contact (Mobile)</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                placeholder="Contact Number">
                                            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                                        </div>
                                        <div class="cart-edit-location">
                                            <div class="mb-3 cart-form3">
                                                <label for="">State</label>
                                                <select class="form-select checkout-box3"
                                                    aria-label="Default select example">
                                                    <option selected>Select</option>
                                                    <option value="1">Pahang</option>
                                                    <option value="2">Perlis</option>
                                                    <option value="3">Sabah</option>
                                                </select>
                                            </div>
                                            <div class="mb-3 cart-form2">
                                                <label for="">City</label>
                                                <select class="form-select checkout-box3"
                                                    aria-label="Default select example">
                                                    <option selected>Select</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput1" class="form-label">Address 1</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleFormControlInput1" placeholder="Address 1">
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput1" class="form-label">Address 2</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleFormControlInput1" placeholder="Address 2">
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput1" class="form-label">Address 3</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleFormControlInput1" placeholder="Address 3">
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput1" class="form-label">Postcode</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleFormControlInput1" placeholder="Postcode">
                                        </div>
                                    </form>
                                </div>
                                <div class="edit-opt-btn modal-footer">
                                    <button class="cancel-btn  btn btn-primary">Back</button>
                                    <button class="savechg-btn btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="findastore" aria-hidden="true" aria-labelledby="findastoreLabel"
                        tabindex="-1">
                        <div class="modal-dialog modal-xl modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box find-store-tittle">
                                <div class="modal-header top-findshop-tittle">
                                    <h1 class="modal-title edit-tittle2" id="findastoreLabel">Find A <span>Store</span>
                                    </h1>

                                    <div class="search-store">
                                        <input type="text" class="bg-form2 form-control" id="formGroupExampleInput"
                                            placeholder="Location">
                                        <a href="#" id="DeleteCartItem" class="link-product">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body find-store-box">
                                    <div class="store-scroll">

                                        <div class="store-details-box active">
                                            <div class="shoplot">
                                                <img class="shop-img1" src="{{ asset('/images/formula/shotlot1.png') }}"
                                                    alt="">
                                            </div>
                                            <div class="shoplot-box shotlot-detail">
                                                <ul>
                                                    <li>
                                                        <h5>Formula, Uptown Selangor</h5>
                                                    </li>
                                                    <li>
                                                        <p>No. 15-G, Jalan Dinar G U3/G Taman Subang Perdana, Seksyen
                                                            U3, Selangor 40150</p>
                                                    </li>
                                                    <li class="shoplot-contact">
                                                        <p><i class="fa fa-phone" aria-hidden="true"></i>019-9867908</p>

                                                        <p><i class="fa fa-clock-o" aria-hidden="true"></i>9:30am -
                                                            9:30pm Daily</p>
                                                    </li>
                                                </ul>

                                            </div>
                                            <div class="icon-stock">
                                                <li class="shoplot-instock">
                                                    <p><i class="fa fa-check" aria-hidden="true"></i>In Stock</p>
                                                </li>
                                            </div>
                                        </div>

                                        <div class="store-details-box">
                                            <div class="shoplot">
                                                <img class="shop-img1" src="{{ asset('/images/formula/shotlot1.png') }}"
                                                    alt="">
                                            </div>
                                            <div class="shoplot-box shotlot-detail">
                                                <ul>
                                                    <li>
                                                        <h5>Formula, Uptown Selangor</h5>
                                                    </li>
                                                    <li>
                                                        <p>No. 15-G, Jalan Dinar G U3/G Taman Subang Perdana, Seksyen
                                                            U3, Selangor 40150</p>
                                                    </li>
                                                    <li class="shoplot-contact">
                                                        <p><i class="fa fa-phone" aria-hidden="true"></i>019-9867908</p>

                                                        <p><i class="fa fa-clock-o" aria-hidden="true"></i>9:30am -
                                                            9:30pm Daily</p>
                                                    </li>
                                                </ul>

                                            </div>
                                            <div class="icon-stock">
                                                <li class="shoplot-lowstock">
                                                    <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Low
                                                        Stock</p>
                                                </li>
                                            </div>
                                        </div>

                                        <div class="store-details-box">
                                            <div class="shoplot">
                                                <img class="shop-img1" src="{{ asset('/images/formula/shotlot1.png') }}"
                                                    alt="">
                                            </div>
                                            <div class="shoplot-box shotlot-detail">
                                                <ul>
                                                    <li>
                                                        <h5>Formula, Uptown Selangor</h5>
                                                    </li>
                                                    <li>
                                                        <p>No. 15-G, Jalan Dinar G U3/G Taman Subang Perdana, Seksyen
                                                            U3, Selangor 40150</p>
                                                    </li>
                                                    <li class="shoplot-contact">
                                                        <p><i class="fa fa-phone" aria-hidden="true"></i>019-9867908</p>

                                                        <p><i class="fa fa-clock-o" aria-hidden="true"></i>9:30am -
                                                            9:30pm Daily</p>
                                                    </li>
                                                </ul>

                                            </div>
                                            <div class="icon-stock">
                                                <li class="shoplot-outstock">
                                                    <p><i class="fa fa-times" aria-hidden="true"></i>Out of Stock</p>
                                                </li>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="edit-opt-btn modal-footer">
                                    <button class="cancel-btn  btn btn-primary">Back</button>
                                    <button class="savechg-btn btn-primary">Select</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="checkoutcart" aria-hidden="true" aria-labelledby="checkoutcartLabel"
                        tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box confirm-info">
                                <div class="modal-header">
                                    <h1 class="modal-title edit-tittle" id="checkoutcartLabel">Please Confirm The
                                        Delivery <span>Address Is Correct</span></h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <fieldset disabled>
                                            <div class="mb-3 confirm-box2">
                                                <label for="disabledTextInput" class="form-label">Name</label>
                                                <div class="confirm-box1">
                                                    <p>Jane Tan</p>
                                                </div>
                                            </div>
                                            <div class="mb-3 confirm-box2">
                                                <label for="disabledTextInput" class="form-label">Contact
                                                    (Mobile)</label>
                                                <div class="confirm-box1">
                                                    <p>0199765672</p>
                                                </div>
                                            </div>
                                            <div class="mb-3 ">
                                                <label for="disabledTextInput" class="form-label">Shipping
                                                    Address</label>
                                                <div class="confirm-box1">
                                                    <p>B-10-11, forest hill, Taman Desa, 58100 Kuala Lumpur, Wilayah
                                                        Persekutuan Kuala Lumpur
                                                        B-10-11, forest hill, Taman Desa, 58100 Kuala Lumpur, Wilayah
                                                        Persekutuan Kuala Lumpur
                                                    </p>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="edit-opt-btn modal-footer">
                                    <button class="cancel-btn  btn btn-primary">Back</button>
                                    <button class="savechg-btn btn-primary">Proceed</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="errorshow" aria-hidden="true" aria-labelledby="checkoutcartError"
                        tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box confirm-info">
                                <div class="modal-header">
                                    <h1 class="modal-title edit-tittle" id="checkoutcartLabel">Please Confirm The
                                        Delivery <span>Address Is Correct</span></h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <fieldset disabled>
                                            <div class="mb-3 confirm-box2">
                                                <label for="disabledTextInput" class="form-label">Name</label>
                                                <div class="confirm-box1">
                                                    <p>Jane Tan</p>
                                                </div>
                                            </div>
                                            <div class="mb-3 confirm-box2">
                                                <label for="disabledTextInput" class="form-label">Contact
                                                    (Mobile)</label>
                                                <div class="confirm-box1">
                                                    <p>0199765672</p>
                                                </div>
                                            </div>
                                            <div class="mb-3 ">
                                                <label for="disabledTextInput" class="form-label">Shipping
                                                    Address</label>
                                                <div class="confirm-box1">
                                                    <p>B-10-11, forest hill, Taman Desa, 58100 Kuala Lumpur, Wilayah
                                                        Persekutuan Kuala Lumpur
                                                        B-10-11, forest hill, Taman Desa, 58100 Kuala Lumpur, Wilayah
                                                        Persekutuan Kuala Lumpur
                                                    </p>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="edit-opt-btn modal-footer">
                                    <button class="cancel-btn  btn btn-primary">Close</button>
                                    <button class="savechg-btn btn-primary">Proceed</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="checkout-page content">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="checkout-box">

                                    <div class="shopping-tittle">
                                        <p>Shopping Cart</p>
                                    </div>

                                    <div class="checkout-subtittle">
                                        <p>Subtotal</p>
                                        <p>MYR 100.00</p>
                                    </div>
                                    <div class="cart-scroll">

                                        <div class="checkout-cart active">
                                            <div class="checkout-tick">
                                                <input type="checkbox" name="rt" value="33" id="rt" /><label
                                                    for="rt"></label>
                                            </div>
                                            <div class="checkout-product">
                                                <img class="checkout-p1"
                                                    src="{{ asset('/images/product_item/test_mask.png') }}" alt="">
                                            </div>
                                            <div class="checkout-text product-detail">
                                                <p>Blackmores Executive B Stress Formula</p>
                                                <p>Twin Pack (250 Tablets)</p>
                                                <p>MYR10000.00</p>
                                                <div class="product-quantity product-detail">
                                                    <button class="product-add-btn" type="button" name="fml-add-btn">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <input type="text" name="name" value="1">
                                                    <button class="product-minus-btn " type="button"
                                                        name="fml-minus-btn">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="icon-special icon-checkout">
                                                <div class="icon-checkout-2">
                                                    <a href="#" id="DeleteCartItem" class="link-product">
                                                        <i class="fa fa-trash icon-chekout-1" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="check-bg">
                                                <img class="checkout-3dbg"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                        </div>

                                        <div class="checkout-cart">
                                            <div class="checkout-tick">
                                                <input type="checkbox" name="rt1" value="33" id="rt1" /><label
                                                    for="rt1"></label>
                                            </div>
                                            <div class="checkout-product">
                                                <img class="checkout-p1"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                            <div class="checkout-text product-detail">
                                                <p>Blackmores Executive B Stress Formula</p>
                                                <p>Twin Pack (250 Tablets)</p>
                                                <p>MYR10000.00</p>
                                                <div class="product-quantity">
                                                    <button class="product-add-btn" type="button" name="fml-add-btn">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <input type="text" name="name" value="1">
                                                    <button class="product-minus-btn " type="button"
                                                        name="fml-minus-btn">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                            </div>
                                            <div class="icon-special icon-checkout">
                                                <div class="icon-checkout-2">
                                                    <a href="#" id="DeleteCartItem" class="link-product">
                                                        <i class="fa fa-trash icon-chekout-1" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="check-bg">
                                                <img class="checkout-3dbg"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                        </div>

                                        <div class="checkout-cart">
                                            <div class="checkout-tick">
                                                <input type="checkbox" name="rt3" value="33" id="rt3" /><label
                                                    for="rt3"></label>
                                            </div>
                                            <div class="checkout-product">
                                                <img class="checkout-p1"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                            <div class="checkout-text product-detail">
                                                <p>Blackmores Executive B Stress Formula</p>
                                                <p>Twin Pack (250 Tablets)</p>
                                                <p>MYR10000.00</p>
                                                <div class="product-quantity">
                                                    <button class="product-add-btn" type="button" name="fml-add-btn">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <input type="text" name="name" value="1">
                                                    <button class="product-minus-btn " type="button"
                                                        name="fml-minus-btn">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                            </div>
                                            <div class="icon-special icon-checkout">
                                                <div class="icon-checkout-2">
                                                    <a href="#" id="DeleteCartItem" class="link-product">
                                                        <i class="fa fa-trash icon-chekout-1" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="check-bg">
                                                <img class="checkout-3dbg"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                        </div>

                                        <div class="checkout-cart">
                                            <div class="checkout-tick">
                                                <input type="checkbox" name="rt4" value="33" id="rt4" /><label
                                                    for="rt4"></label>
                                            </div>
                                            <div class="checkout-product">
                                                <img class="checkout-p1"
                                                    src="{{ asset('/images/product_item/test_mask.png') }}" alt="">
                                            </div>
                                            <div class="checkout-text product-detail">
                                                <p>Blackmores Executive B Stress Formula</p>
                                                <p>Twin Pack (250 Tablets)</p>
                                                <p>MYR10000.00</p>
                                                <div class="product-quantity">
                                                    <button class="product-add-btn" type="button" name="fml-add-btn">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <input type="text" name="name" value="1">
                                                    <button class="product-minus-btn " type="button"
                                                        name="fml-minus-btn">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                            </div>
                                            <div class="icon-special icon-checkout">
                                                <div class="icon-checkout-2">
                                                    <a href="#" id="DeleteCartItem" class="link-product">
                                                        <i class="fa fa-trash icon-chekout-1" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="check-bg">
                                                <img class="checkout-3dbg"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                        </div>

                                        <div class="checkout-cart">
                                            <div class="checkout-tick">
                                                <input type="checkbox" name="rt5" value="33" id="rt5" /><label
                                                    for="rt5"></label>
                                            </div>
                                            <div class="checkout-product">
                                                <img class="checkout-p1"
                                                    src="{{ asset('/images/product_item/test_mask.png') }}" alt="">
                                            </div>
                                            <div class="checkout-text product-detail">
                                                <p>Blackmores Executive B Stress Formula</p>
                                                <p>Twin Pack (250 Tablets)</p>
                                                <p>MYR10000.00</p>
                                                <div class="product-quantity">
                                                    <button class="product-add-btn" type="button" name="fml-add-btn">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>
                                                    <input type="text" name="name" value="1">
                                                    <button class="product-minus-btn " type="button"
                                                        name="fml-minus-btn">
                                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                                    </button>
                                                </div>

                                            </div>
                                            <div class="icon-special icon-checkout">
                                                <div class="icon-checkout-2">
                                                    <a href="#" id="DeleteCartItem" class="link-product">
                                                        <i class="fa fa-trash icon-chekout-1" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="check-bg">
                                                <img class="checkout-3dbg"
                                                    src="{{ asset('/images/formula/cart-bg1.png') }}" alt="">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="checkout-bg"></div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="checkout-box">
                                    <div class="order-box">
                                        <div class="shopping-tittle">
                                            <p>Order Summary</p>
                                        </div>
                                    </div>
                                    <div class="total-price">
                                        <table class="table table-hover">
                                            <tbody>
                                                <tr>
                                                    <td class="price-details-1">Subtotal (Items)</td>
                                                    <td class="price-details-2">MYR13000.00</td>
                                                </tr>
                                                <tr>
                                                    <td class="price-details-1">Shipping Fee</td>
                                                    <td class="price-details-2">MYR30.00</td>
                                                </tr>
                                                <tr>
                                                    <td class="price-details-1">Discount</td>
                                                    <td class="price-details-2">MYR30.00</td>
                                                </tr>
                                                <tr>
                                                    <td class="price-details-red1">Total </td>
                                                    <td class="price-details-red2">MYR13030.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="checkout-box checkout-box-space">
                                    <div class="ship-box">
                                        <div class="shopping-tittle textbox-width">
                                            <p>Ship To</p>
                                        </div>
                                        <div class="icon-btn">
                                            <div class="icon-edit-info">
                                                <a href="#address_edit_form" id="DeleteCartItem" data-bs-toggle="modal"
                                                    role="button" class="link-product">
                                                    <i class="fa fa-pencil-square-o icon-edit" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="total-price">
                                        <div class="info-checkout">
                                            <p>Name</p>
                                            <div class="bg-form">
                                                <p class="">0199765672</p>
                                            </div>
                                        </div>
                                        <div class="info-checkout">
                                            <p>Contact (Mobile)</p>
                                            <div class="bg-form">
                                                <p class="">0199765672</p>
                                            </div>

                                        </div>
                                        <div class="info-checkout">
                                            <p>Shipping Address</p>
                                            <div class="bg-form">
                                                <p class="">B-10-11, forest hill, Taman Desa, 58100 Kuala Lumpur,
                                                    Wilayah Persekutuan Kuala Lumpur</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkout-box checkout-box-space">
                                    <div class="order-box">
                                        <div class="shopping-tittle">
                                            <p>Voucher</p>
                                        </div>
                                    </div>
                                    <div class="total-price voucher">
                                        <form>
                                            <div class="form-group info-checkout">
                                                <label for="formGroupExampleInput">Coupon Code</label>
                                            </div>
                                            <div class="col-md-12 form-row my-mb-2 coupon-box">
                                                <div class="col-7 col-md-7">
                                                    <input type="text" class="form-control" placeholder="Coupon Code">
                                                </div>
                                                <div class="col-5 col-md-5">
                                                    <a href="#" id="checkout-btn"
                                                        class="btn link-product apply-btn">Apply</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="checkout-box checkout-box-space delivery-tittle">
                                    <div class="delivery-box">
                                        <div class="shopping-tittle">
                                            <p>Delivery Method</p>
                                        </div>
                                    </div>
                                    <div class="delivery-choice">
                                        <ul class="nav nav-pills mb-3 delivery-option" id="pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="choose-delivery-tab" data-toggle="pill"
                                                    href="#choose-delivery" role="tab" aria-controls="choose-delivery"
                                                    aria-selected="true">
                                                    <div class="red-dot"></div>
                                                    <img class="icon-size"
                                                        src="{{ asset('/images/icons/side03.png') }}" alt="">
                                                    <p>Delivery By Company</p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="choose-selfpick-tab" data-toggle="pill"
                                                    href="#choose-selfpick" role="tab" aria-controls="choose-selfpick"
                                                    aria-selected="false">
                                                    <div class="red-dot"></div>
                                                    <img class="icon-size"
                                                        src="{{ asset('/images/icons/side01.png') }}" alt="">
                                                    <p>Self-pickup</p>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="choose-delivery" role="tabpanel"
                                                aria-labelledby="choose-delivery-tab">
                                                <div class="find-store">
                                                    {{-- <div class="checkout-tick">
                                                                <input type="checkbox" name="current-add01" value="current-add01" id="current-add01"/>
                                                                <label for="current-add01" id="myCheck"></label>
                                                                <p>Use Current Location</p>
                                                            </div> --}}
                                                    <a data-bs-toggle="modal" href="#checkoutcart" role="button"
                                                        class="link-product checkout-btn">Checkout</a>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="choose-selfpick" role="tabpanel"
                                                aria-labelledby="choose-selfpick-tab">
                                                <div class="find-store">
                                                    <label for="formGroupExampleInput">Find A Store</label>
                                                    <div class="voucher-box">
                                                        <input type="text" class="bg-form2 form-control"
                                                            id="formGroupExampleInput" placeholder="Location">
                                                        <a href="#findastore" id="DeleteCartItem" class="link-product"
                                                            data-bs-toggle="modal" role="button">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="checkout-tick">
                                                        <input type="checkbox" name="current-add01"
                                                            value="current-add01" id="current-add01" />
                                                        <label for="current-add01" id="myCheck"></label>
                                                        <p>Use Current Location</p>
                                                    </div>
                                                    <a data-bs-toggle="modal" href="#checkoutcart" role="button"
                                                        class="link-product checkout-btn">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@push('script')
<script>
    const numInput = document.querySelectorAll('.product-quantity input');

    function runMycode(e) {
        console.log(e.target.value);
    }

    getNum.forEach(numVue => {
        numVue.addEventListener('keydown', runMycode);
    });
</script>
@endpush
