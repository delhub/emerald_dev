@inject('controller', 'App\Http\Controllers\Administrator\v1\Product\PanelProductMarkupCategoryController')

@extends('layouts.administrator.main')

@section('content')

<h1>{{$controller::$header}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            @if(count($tables) > 0) 
            <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Category Name</td>
                        <td>Category Currency</td>
                        <td>Markup Rate</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                    @foreach($tables as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{$row->cat_name}}</td>
                        <td>{{$row->cat_desc}}</td>
                        <td>{{$row->markup_rate}}</td>
                        <td>
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left" class="btn btn-primary shadow-sm"
                            href="{{ route($controller::$edit,['id' => $row->id]) }}" >Edit</a>

                            {{-- <form action="{{ route($controller::$destroy,['id' => $row->id]) }}" id="delete-form-id" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            @csrf
                                <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>
                            </form> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$tables->links()}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p>
            @endif
        </div>
    </div>
</div>
@endsection
