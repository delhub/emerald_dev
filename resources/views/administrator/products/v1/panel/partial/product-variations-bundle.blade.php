<div class="{{ ($loop && $loop->last) || (isset($bundle) && !$bundle) ? 'default-item2' : '' }}"
    style="border: 1px solid #b3b3b3; border-radius: 5px;">
    <input {{ $inactive ? 'readonly' : '' }} type="hidden" class="product_bundle_id"
        name="product_bundle_id[{{ $attribute ? $attribute->id : old('newProductID') }}][]" id="productBundleID"
        value="{{ isset($bundle) && $bundle ? $bundle->id : old('product_bundle_id') }}" readonly>

    <div class="row" style="padding: 5px 10px 10px;">
        <div class="col-12 col-sm-12 col-md-10">
            <label for="product_bundle_primary_product_id" class="col-form-label font-weight-bold">Primary
                Product</label>
            <select
                name="product_bundle_primary_product_id[{{ $attribute ? $attribute->id : old('newProductID') }}][]"
                class="select2 pc product-bundle-select" {{ $inactive ? 'disabled' : '' }}>
                <option value="">Please choose primary product code.</option>
                @foreach ($productAttributes as $productAttribute)
                    <option value="{{ $productAttribute->id }}"
                        {{ isset($bundle) && $bundle->primary_product_code == $productAttribute->product_code && $bundle->active >= 1? 'selected': '' }}>
                        {{ $productAttribute->product_code }}
                        ({{ $productAttribute->product_status == 1 ? 'active' : 'inactive' }})
                        -
                        {{ $productAttribute->name }} ({{ $productAttribute->attribute_name }})
                        ({{ $productAttribute->attribute_active == 1 ? 'active' : 'inactive' }})
                    </option>
                @endforeach
            </select>
        </div>
        {{-- <div class="col-12 col-sm-12 col-md-2"></div> --}}
        <div class="col-12 col-sm-12 col-md-2">
            <label for="product_bundle_primary_quantity" class="col-form-label font-weight-bold">Primary Quanity</label>
            <input {{ $inactive ? 'readonly' : '' }} type="number" id="product_bundle_primary_quantity"
                name="product_bundle_primary_quantity[{{ $attribute ? $attribute->id : old('newProductID') }}][]"
                class="form-control input-mask-price d-inline" style="width: 100%;"
                value="{{ isset($bundle) && $bundle->active >= 1 ? $bundle->primary_quantity : '' }}">
        </div>
    </div>
    @if (isset($bundle) && !$loop->first)
        <button type="button" class="btn btn-danger" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
            <i class="fa fa-minus"></i>
        </button>
    @endif
</div>
