<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bujishu - Invoice {{ $purchase->purchase_number }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
     @include('layouts.favicon')
    <style>
        * {
            padding: 0;
            margin: 0;
            font-family: 'Lato', sans-serif;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        .text-grey {
            color: #58595b;
        }
        td img.my-img1{
            object-fit: contain!important;
            width: 100px!important;
            /* height: 80px!important; */
        }
        td img.my-img{
            object-fit: contain!important;
            width: 70px!important;
            float:right;
            margin-right:10px;
            /* height:50px!important; */
        }
        .img-box{
            width: 70%;
            height: 70px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            margin-left: 20%;
            margin-right: 10%;
        }

        /* table,
        td,
        tr {
            border: black 1px solid;
        } */

        .topBottomBorder {
            padding: 13px 0px;
            border-top: 1.5px solid rgba(255, 204, 0, 0.6);
            border-bottom: 1.5px solid #ffcc00
        }

        .topGold {
            border-top: 1px solid #ffcc00;
        }

        .totalAmount {
            border-top: 1px solid #ffcc00;
            width: 146px;
            text-align: right;
            padding: 12px 10px;
            background-color: #fef5d6;
            font-weight: 600;
            font-size: 16px;

        }

        .totalTitle {
            border-top: 1px solid #ffcc00;
            width: 151px;
            padding: 12px 10px;
            text-align: right;
            font-weight: 600;
            font-size: 16px;
        }

        .paymentReceived {
            border-top: 1px solid #ffcc00;
            width: 605px;
            padding: 12px 10px;
        }
    </style>
</head>

<body style="margin: 0; padding: 0; height: 100vh;">
    <?php

    $batchs = array();
    // $subBatchs = array();
    $batch_count = 1;
    $item_count = 0;
    $batchs[$batch_count] = collect();
    // Calculate Subtotal
    $subtotalPrice = 0;
    $deliveryFee = 0;
    $installationFee = 0;
    $rebateFee = 0;
    $balanceDue = 0;

    $grandTotal = 0;

    $key = 0;

    foreach ($purchase->sortItems() as $item){

            $subtotalPrice = $subtotalPrice + $item->subtotal_price;
            $deliveryFee = ($purchase->getFeeAmount('shipping_fee') != NULL) ? $purchase->getFeeAmount('shipping_fee') : 0;
            $installationFee = ($purchase->getFeeAmount('installation_fee') != NULL) ? $purchase->getFeeAmount('installation_fee') : 0;
            $rebateFee = ($purchase->getFeeAmount('rebate_fee') != NULL ) ? $purchase->getFeeAmount('rebate_fee') : 0;


            $batchs[$batch_count]->add($item);
            $item_count++;


            if ($item_count >= 6 * $batch_count ){
                $batch_count++;
                $batchs[$batch_count] = collect();
            };


        $grandTotal = ($subtotalPrice   - $rebateFee) + ($deliveryFee + $installationFee);
        $successPayment = isset($purchase->successfulPayment) ? $purchase->successfulPayment->amount : 0;
        $balanceDue = $grandTotal - $successPayment;

    }
    // $itemCount = count($purchase->sortItems());
     ?>

    @foreach( $batchs as $batch_id => $batch)
    <!-- First Page -->
    <div
        style="page-break-inside: avoid; width: 100%; height: 1385px; position: relative; padding-top: 1%; padding-bottom: 1%;">
        <!-- Header -->
        @include('documents.purchase.v2.header')

        <!-- Second Header -->
        <div style="margin: 10px 40px;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 56%; vertical-align: bottom; padding-top: 20px; padding-right: 0px;">
                        <span class="text-grey" style="display: inline-block; width: 43%; padding: 0 10px;">
                            PURCHASER:
                        </span>
                        <span class="text-grey" style="display: inline-block; width: 46%; padding: 0 10px;">
                            DELIVER TO:
                        </span>
                        <div style="margin-top: 8px; border-bottom: 2px solid #ffcc00;"></div>

                        <div style="width: 100%;">
                            <table style="display: inline-block; width: 46%; padding: 5px 10px;">
                                <tr>
                                    <td>
                                        <p style="font-size: .75rem; line-height: 1.2;">
                                            {{ $purchase->user->userInfo->shippingAddress->address_1 }},
                                            <br>
                                            {{ $purchase->user->userInfo->shippingAddress->address_2 }},
                                            {{ $purchase->user->userInfo->shippingAddress->address_3 }},
                                            <br>
                                            {{ $purchase->user->userInfo->shippingAddress->postcode }},
                                            @php
                                            if (!empty($purchase->user->userInfo->shippingAddress->cityKey->id)) {
                                                echo $purchase->user->userInfo->shippingAddress->cityKey->city_name;
                                            }else{
                                                echo $purchase->user->userInfo->shippingAddress->city;
                                            }
                                            @endphp
                                            <br>
                                            {{ $purchase->user->userInfo->shippingAddress->state->name }},
                                            Malaysia.
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Attn :
                                            <span>
                                                {{ $purchase->user->userInfo->full_name }}
                                            </span>
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Tel :
                                            <span>
                                                {{ $purchase->user->userInfo->contacts->first()->contact_num }}
                                            </span>
                                        </p>
                                    </td>
                                </tr>
                            </table>

                            <table style="display: inline-block; width: 46%; padding: 5px 10px;vertical-align: top;">
                                <tr>
                                    <td>
                                        <p style="font-size: .75rem; line-height: 1.2;">
                                            {{ isset($purchase->ship_address_1) ? $purchase->ship_address_1 : $purchase->user->userInfo->shippingAddress->address_1 }}
                                            <br>
                                            {{ isset($purchase->ship_address_1) ? $purchase->ship_address_2 : $purchase->user->userInfo->shippingAddress->address_2}}
                                            @if (isset($purchase->ship_address_1))
                                            @if (isset($purchase->ship_address_3))
                                            <br>
                                            {{ $purchase->ship_address_3 }}
                                            @endif
                                            @else
                                            @if (isset($purchase->user->userInfo->shippingAddress->address_3))
                                            <br>
                                            {{ $purchase->user->userInfo->shippingAddress->address_3 }}
                                            @endif
                                            @endif
                                            {{ isset($purchase->ship_address_1) ? $purchase->ship_postcode : $purchase->user->userInfo->shippingAddress->postcode }}
                                            @php
                                                if(isset($purchase->ship_address_1)){
                                                    if ($purchase->cityKey != NULL) {
                                                        echo $purchase->cityKey->city_name;
                                                    }else {
                                                        echo $purchase->user->userInfo->shippingAddress->city ;
                                                    }
                                                }else{
                                                    echo $purchase->user->userInfo->shippingAddress->cityKey->city_name ;
                                                }
                                            @endphp
                                            <br>
                                            {{ isset($purchase->ship_address_1) ? $purchase->state->name : $purchase->user->userInfo->shippingAddress->state->name }},
                                            Malaysia.
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Attn :
                                            <span>
                                                {{ isset($purchase->ship_full_name) ? $purchase->ship_full_name : $purchase->user->userInfo->full_name }},
                                            </span>
                                        </p>
                                        <p style="font-size: .78rem; margin-top: 2px;">
                                            Tel :
                                            <span>
                                                {{ isset($purchase->ship_contact_num) ? $purchase->ship_contact_num : $purchase->user->userInfo->contacts->first()->contact_num }},
                                            </span>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="margin-top: 8px; border-bottom: 1px solid rgba(255, 204, 0, 0.6);"></div>
                    </td>
                    <td style="width: 2%;"></td>
                    <td style=" width: 42%; vertical-align: bottom;">
                        <p style="font-size: 1.8rem; color: #ffcc00; font-weight: 700;">
                            <span style="margin: 0 10px;">INVOICE</span>
                            <div style="background-color: #fef5d6; border-radius: 20px; padding: 20px 0px 20px 20px;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 45%;">
                                            INVOICE NO.
                                        </td>
                                        <td style="width: 5%">
                                            :
                                        </td>
                                        <td style="width: 50%;">
                                            {{ $purchase->getFormattedNumber() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ $purchase->purchase_date }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Agent Code
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{ ($purchase->user->userInfo->agentInformation) ? $purchase->user->userInfo->agentInformation->account_id : ''}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Credit Terms
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <span style="text-transform: capitalize;">
                                                {{ $purchase->purchase_type }}
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Status
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{$purchase->user->userInfo->account_status == '1' ? 'DC Customer' : 'Bujishu Customer'}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Page
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{$batch_id}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <!-- End Second Header -->

        <!-- Content -->
        <div style="margin: 10px 40px;">
            <table cellspacing="0" cellpadding="0">
                <tr style="text-align: center;font-size:16px;background-color: #fef5d6;font-weight:600;">
                    <td class="topBottomBorder topGold" style="width: 51px;">No.</td>
                    <td class="topBottomBorder topGold" style="width: 151px;">Product ID</td>
                    <td class="topBottomBorder topGold" style="width: 351px;">Description</td>
                    <td class="topBottomBorder topGold" style="width: 51px;">Qty</td>
                    <td class="topBottomBorder topGold" style="width: 151px;">Unit Price (RM)</td>
                    <td class="topBottomBorder topGold" style="width: 151px;">Amount (RM)</td>
                </tr>
                @foreach ($batch as $item)
                    <tr class=" @if ($item->bundle_id == 0) op @else non-op @endif ">
                        <td @if ($item->bundle_id == 0) class="topGold" @endif style="text-align: center;padding:10px;">
                            @php
                                if (($item->bundle_id == 0)) {
                                    $batchs[$key] = $item;
                                    $key++;
                                    echo $key;
                                }
                            @endphp
                        </td>

                        <td @if ($item->bundle_id == 0) class="topGold" @endif style="padding:10px;">
                            @if ($item->bundle_id == 0)
                                {{ $item->product_code }}
                            @endif
                        </td>

                        <td @if ($item->bundle_id == 0) class="topGold" @endif style="padding:10px;">
                            <table cellspacing=" 5">
                                <tr>
                                    <td style="width: 110px">
                                        @if (isset($item->product_information['product_color_img']))

                                            @php
                                                $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first()
                                            @endphp

                                            <img class=" mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif " style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};display: inline-block; margin-right: 12px;" src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                        @elseif(isset($item->product_information['product_color_code']))

                                            @php
                                                $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first()
                                            @endphp

                                            <img class="mw-100 rounded d-inline" style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};width: 80px; display: inline-block; margin-right: 12px;">

                                        @else

                                            <img class="mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif "
                                            src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}" style="display: inline-block;float:right;background-size:contain;object-fit: contain;"
                                            alt="">

                                        @endif
                                    </td>

                                    <td style="width: 241px">
                                        <p style=" vertical-align: top; text-align: left;">
                                            <!-- Product Name -->
                                            <span style="font-weight: 600;">{{ $item->product->parentProduct->name }}</span>
                                            @if($item->product_id != 175 || $item->product_id != 176)
                                                @if(array_key_exists('product_color_name', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Color: {{ $item->product_information['product_color_name'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_size', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Size: {{ $item->product_information['product_size'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_curtain_size', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Curtain Model: {{ $item->product_information['product_curtain_size'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_temperature', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Color Temperature:
                                                    <br />{{ $item->product_information['product_temperature'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_miscellaneous', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    {{ $item->product_information['product_miscellaneous'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('invoice_number', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Invoice Number: {{ $item->product_information['invoice_number'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_preorder_date', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Pre Order
                                                    Delivery:<br />{{ $item->product_information['product_preorder_date'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_order_remark', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Remarks:<br />{{ $item->product_information['product_order_remark'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_order_selfcollect', $item->product_information) &&
                                                $item->product_information['product_order_selfcollect'])
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Self Collection: Yes
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_order_tradein', $item->product_information) &&
                                                $item->product_information['product_order_tradein'])
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Rebate: - RM
                                                    {{ number_format($item->product_information['product_order_tradein'] / 100),2 }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_installation', $item->product_information) &&
                                                $item->product_information['product_installation'])
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    {{($item->installationCategories()!=NULL) ? $item->installationCategories()->cat_name : ''}}
                                                </span>
                                                @endif
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td @if ($item->bundle_id == 0) class="topGold" @endif style="text-align: center;padding:10px;">
                            {{ $item->quantity }}
                        </td>

                        <td @if ($item->bundle_id == 0) class="topGold" @endif style="text-align: center;padding:10px;">
                            @if ($item->bundle_id == 0) {{ number_format(($item->unit_price / 100), 2) }} @endif
                        </td>

                        <td @if ($item->bundle_id == 0) class="topGold" @endif style=" text-align: right;padding:10px;background-color: #fef5d6;">
                            @if ($item->bundle_id == 0) {{ number_format(($item->subtotal_price / 100), 2) }} @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- End Content -->

        <!-- Total Footer -->
        <div style="margin: 10px 40px; position: fixed; bottom: 80; width: 910px; z-index:-1;">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 605px;">&nbsp;</td>
                    <td style="width: 151px;">&nbsp;</td>
                    <td style="width: 144px;background-color: #fef5d6;height:660px;">&nbsp;</td>
                </tr>
                <tr>
                    <td class="paymentReceived" rowspan="{{ $rebateFee != 0 ? '7' : '6' }}">
                        <table cellspacing="0" cellpadding="0" {{-- style="border:1pxsolidblack; --}}">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="text-grey" style="font-size: 1.15rem">
                                                PAYMENT RECEIVED
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding:10px 0px;">
                                                <table
                                                    style="background-color: #fef5d6;padding:20px;width:380px;border-radius:10px;">
                                                    <tr>
                                                        <td style="padding:5px 0;width: 190px;">Payment Method</td>
                                                        <td style="width: 10px;">:</td>
                                                        <td style="width: 200px;">{{ $purchase->purchase_type }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px 0;">Reference Number</td>
                                                        <td>:</td>
                                                        <td>{{ $purchase->successfulPayment->auth_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:5px 0;">Amount Paid</td>
                                                        <td>:</td>
                                                        <td>RM
                                                            {{ number_format(($purchase->successfulPayment->amount / 100), 2) }}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:12px;">
                                                The issuance of this invoice shall be deemed as your acceptance of the
                                                terms and conditions of this Disclaimer. The Bujishu Agent Agreement shall only become
                                                effective after full payment is received by the Company.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="totalTitle">
                        Sub Total
                    </td>
                    <td class="totalAmount">{{ number_format(($subtotalPrice / 100), 2)}}</td>
                </tr>
                <tr>
                    <td class="totalTitle">
                        Delivery Fee
                    </td>
                    <td class="totalAmount">{{ number_format(($deliveryFee / 100), 2)}}</td>
                </tr>
                <tr>
                    <td class="totalTitle">
                        Installation Fee
                    </td>
                    <td class="totalAmount">{{ number_format(($installationFee / 100), 2)}}</td>
                </tr>
                @if($rebateFee != 0)
                <tr>
                    <td class="totalTitle">
                        Rebate
                    </td>
                    <td class="totalAmount"> - {{ number_format(($rebateFee / 100), 2)}}
                    </td>
                </tr>
                @endif
                <tr>
                    <td class="totalTitle" style="color: #ffcc00;">
                        Grand Total
                    </td>
                    <td class="totalAmount" style="background-color:#ffcc00;">
                        {{ number_format(($grandTotal / 100), 2)}}</td>
                </tr>

                <tr>
                    <td class="totalTitle">
                        Amount Paid
                    </td>
                    <td class="totalAmount">{{ number_format(($purchase->successfulPayment->amount / 100), 2) }}</td>
                </tr>
                <tr>
                    <td class="totalTitle" style=" border-bottom: 1px solid #ffcc00;">
                        Balance Due
                    </td>
                    <td class="totalAmount" style=" border-bottom: 1px solid #ffcc00;">
                        {{ number_format(($balanceDue / 100), 2) }}</td>
                </tr>
            </table>
        </div>
        <!-- End Total Footer -->

        <!-- Footer -->
        @include('documents.purchase.v2.footer')


    </div>
    @endforeach
    <!-- End First Page -->
</body>

</html>
