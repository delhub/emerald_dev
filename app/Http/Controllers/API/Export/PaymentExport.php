<?php

namespace App\Http\Controllers\API\Export;

use App\Http\Controllers\InvoicesPerMonthSheet;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PaymentExport implements WithMultipleSheets
{
    use Exportable;

    protected $request;

    public const ONLINE_OFFLINE = 1;
    public const NO_PAYMENT = 2;
    public const CANCEL_REFUND = 3;
    public const CN_DN = 4;
    public const ITEMISED = 5;
    // public const SALES_ITEMISED = 6;
    public const SALES = 6;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $total = $this->paymentQuery($counter = false);

        if ($this->request->downloadType == 'payment') {
            $sheet = 1;
            $totals = count($total);
        }else{
            $sheet = 5;
            $totals = 6;
        }

        for ($counter = $sheet; $counter <= $totals; $counter++) {
            $query = $this->paymentQuery($counter);
            $sheets[] = new PaymentPerSheets($query, $counter);
        }
        return $sheets;
    }

    public function paymentQuery($counter)
    {
        $datefrom = date('d/m/Y', strtotime($this->request->datefrom));
        $dateto = date('d/m/Y', strtotime($this->request->dateto));

        // special for CN/DN
        $period = \Carbon\CarbonPeriod::create(date('Y-m-d',strtotime($this->request->datefrom)), '1 month', date('Y-m-d',strtotime($this->request->dateto)));
        foreach ($period as $dt) {
            $newDate[] = $dt->format("ym");
        }
        $rangeDate = implode('|',$newDate);


        if ($this->request->downloadType == 'payment') {

            $query[self::ONLINE_OFFLINE]['data'] = DB::select(DB::raw("
                SELECT a.purchase_number AS 'Purchase Number',
                a.inv_number as 'Invoice Number',
                d.`name` AS 'Purchase Status',
                c.full_name AS 'Purchaser Name',
                a.ship_full_name AS 'Receiver Name',
                a.purchase_date AS 'Invoice Date',
                a.purchase_type AS 'Payment Method',
                a.purchase_amount/100 AS 'Purchase Amount',
                order_amt/100 AS 'Order Amount',
                ifnull(ship_fee/100,0) as 'Ship Fee',
                ifnull(rebates/100,0) AS 'Rebate',
                b.gateway_string_result AS 'Gateway String Result',
                b.auth_code AS 'Auth Code',
                b.amount/100 AS 'Online Amount',
                b.local_amount/100 AS 'Local Amount',
                b.purchase_applied  AS 'Purchase Applied'
                FROM purchases a
                INNER JOIN payments b ON a.purchase_number = b.purchase_number
                INNER JOIN user_infos c ON a.user_id = c.user_id
                INNER JOIN global_statuses d ON a.purchase_status = d.id
                JOIN (SELECT purchase_id, SUM(order_amount) AS order_amt FROM orders GROUP BY purchase_id) AS e ON a.id = e.purchase_id
                left JOIN (SELECT purchase_id, SUM(amount) AS ship_fee FROM shipping_fee WHERE `type` in ('shipping_fee') GROUP BY purchase_id) AS f ON a.id = f.purchase_id
                left JOIN (SELECT purchase_id, SUM(amount) AS rebates FROM shipping_fee WHERE `type` in ('rebate_fee') GROUP BY purchase_id) AS g ON a.id = g.purchase_id
                WHERE a.purchase_status IN (3001,3002,3003,3013,4000,4001,4002,4007) AND
                (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
                ORDER  BY a.purchase_date, a.purchase_number;
                "));
            $query[self::ONLINE_OFFLINE]['title'] = 'Online Offline';
            $query[self::ONLINE_OFFLINE]['header'] = array('Purchase Number', 'Invoice Number', 'Purchase Status', 'Purchaser Name', 'Receiver Name', 'Invoice Date', 'Payment Method', 'Purchase Amount','Order Amount', 'Ship Fee', 'Rebate', 'Gateway String Result', 'Auth Code', 'Online Amount', 'Local Amount', 'Purchase Applied');

            $query[self::NO_PAYMENT]['data'] = DB::select(DB::raw("
                SELECT a.purchase_number AS 'Purchase Number',
                a.inv_number as 'Invoice Number',
                d.`name` AS 'Purchase Status',
                c.full_name AS 'Purchaser Name',
                a.ship_full_name AS 'Receiver Name',
                a.purchase_date AS 'Invoice Date',
                a.purchase_type AS 'Payment Method',
                a.purchase_amount/100 AS 'Purchase Amount',
                b.gateway_string_result AS 'Gateway String Result',
                b.auth_code AS 'Auth Code',
                b.amount/100 AS 'Online Amount',
                b.local_amount/100 AS 'Local Amount',
                b.purchase_applied AS 'Purchase Applied'
                FROM purchases a
                LEFT  JOIN payments b ON a.purchase_number = b.purchase_number
                INNER JOIN user_infos c ON a.user_id = c.user_id
                INNER JOIN global_statuses d ON a.purchase_status = d.id
                WHERE a.purchase_status IN (3000,3023,4006) AND
                (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
                ORDER  BY a.purchase_date, a.purchase_number;
                "));

            $query[self::NO_PAYMENT]['title'] = 'No Payment';
            $query[self::NO_PAYMENT]['header'] = array('Purchase Number', 'Invoice Number', 'Purchase Status', 'Purchaser Name', 'Receiver Name', 'Invoice Date', 'Payment Method', 'Purchase Amount', 'Gateway String Result', 'Auth Code', 'Online Amount', 'Local Amount', 'Purchase Applied');

            $query[self::CANCEL_REFUND]['data'] = DB::select(DB::raw("
                SELECT a.purchase_number AS 'Purchase Number',
                a.inv_number as 'Invoice Number',
                d.`name` AS 'Purchase Status',
                c.full_name AS 'Purchaser Name',
                a.ship_full_name AS 'Receiver Name',
                a.purchase_date AS 'Invoice Date',
                a.purchase_type AS 'Payment Method',
                a.purchase_amount/100 AS 'Purchase Amount',
                b.gateway_string_result AS 'Gateway String Result',
                b.auth_code AS 'Auth Code' ,
                b.amount/100 AS 'Online Amount',
                b.local_amount/100 AS 'Local Amount',
                b.purchase_applied  AS 'Purchase Applied'
                FROM purchases a
                LEFT JOIN payments b ON a.purchase_number = b.purchase_number
                INNER JOIN user_infos c ON a.user_id = c.user_id
                INNER JOIN global_statuses d ON a.purchase_status = d.id
                WHERE a.purchase_status IN (4004,4005,4003) AND
                (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
                ORDER  BY a.purchase_date, a.purchase_number;
                "));
            $query[self::CANCEL_REFUND]['title'] = 'Cancel Refund';
            $query[self::CANCEL_REFUND]['header'] = array('Purchase Number', 'Invoice Number', 'Purchase Status', 'Purchaser Name', 'Receiver Name', 'Invoice Date', 'Payment Method', 'Purchase Amount', 'Gateway String Result', 'Auth Code', 'Online Amount', 'Local Amount', 'Purchase Applied');

            $query[self::CN_DN]['data'] = DB::select(DB::raw("
                SELECT a.txn_date,
                a.user_id, a.payment_mode,
                a.payment_id,
                a.purchase_number,
                a.adj_type,
                a.adj_id,
                a.adj_amount/100,
                a.remark,
                a.created_at
                FROM account_ledger a
                WHERE a.adj_id REGEXP '{$rangeDate}';
                "));
            $query[self::CN_DN]['title'] = 'CN DN';
            $query[self::CN_DN]['header'] = array('Transaction Date', 'User ID', 'Payment Mode', 'Payment ID', 'Purchase Number', 'Adj Type', 'Adj ID', 'Adj Amount', 'Remark', 'Created At');

            $query[self::ITEMISED]['data'] = DB::select(DB::raw("
                SELECT a.purchase_number AS 'Purchase Number',
                a.inv_number as 'Invoice Number',
                a.purchase_date AS 'Purchase Date',
                c.product_code AS 'Product Code',
                e.`name` AS 'Name',
                c.quantity AS 'Quantity',
                c.unit_price/100 AS 'Unit Price',
                c.subtotal_price/100 AS 'Subtotal Price'
                FROM purchases a
                INNER JOIN orders b ON a.id = b.purchase_id
                INNER JOIN items c ON b.order_number = c.order_number
                INNER JOIN panel_products d ON c.product_id = d.id
                INNER JOIN global_products e ON d.global_product_id = e.id
                WHERE a.purchase_status IN (3001,3002,3003,3013,4000,4001,4002,4007) AND
                (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
                ORDER  BY a.purchase_date, a.purchase_number;
                "));
            $query[self::ITEMISED]['title'] = 'Itemised';
            $query[self::ITEMISED]['header'] = array('Purchase Number', 'Invoice Number', 'Purchase Date', 'Product Code', 'Name', 'Quantity', 'Unit Price', 'Subtotal Price');

        } else {

            $query[self::SALES]['data'] = DB::select(DB::raw("
            SELECT
                a.purchase_number AS 'Purchase Number',
                a.inv_number as 'Invoice Number',
                a.purchase_date AS 'Invoice Date',
                d.`name` AS 'Purchase Status',
                c.full_name AS 'Purchaser By',
                a.ship_full_name AS 'Ship To',
                a.purchase_type AS 'Payment Method',
                b.gateway_string_result AS 'Gateway String',
                b.auth_code AS 'Auth Code',
                order_amt/100 AS 'Order Amount',
                ifnull(ship_fee/100,0) AS 'Shipping Fee',
                ifnull(rebates/100,0) AS 'Rebate',
                a.purchase_amount/100 AS 'Subtotal',
                b.amount/100 AS 'Online Amount',
                b.local_amount/100 AS 'Local Amount'
                FROM purchases a
                INNER JOIN payments b ON a.purchase_number = b.purchase_number
                INNER JOIN user_infos c ON a.user_id = c.user_id
                INNER JOIN global_statuses d ON a.purchase_status = d.id
                JOIN (
                SELECT purchase_id, SUM(order_amount) AS order_amt
                FROM orders
                GROUP BY purchase_id) AS e ON a.id = e.purchase_id
                left JOIN (
                SELECT purchase_id, SUM(amount) AS ship_fee
                FROM shipping_fee
                WHERE `type` in ('shipping_fee')
                GROUP BY purchase_id) AS f ON a.id = f.purchase_id
                left JOIN (
                SELECT purchase_id, SUM(amount) AS rebates
                FROM shipping_fee
                WHERE `type` in ('rebate_fee')
                GROUP BY purchase_id) AS g ON a.id = g.purchase_id
                WHERE a.purchase_status IN (3001,3002,3003,3013,4000,4001,4002,4007) AND
                (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
                ORDER BY a.purchase_date, a.purchase_number;
                "));
            $query[self::SALES]['title'] = 'Sales (Website)';
            $query[self::SALES]['header'] = array('Purchase Number', 'Invoice Number', 'Invoice Date', 'Purchase Status', 'Purchaser By', 'Ship To', 'Payment Method', 'Gateway String', 'Auth Code', 'Purchase Amount', 'Shipping Fee', 'SubTotal', 'Rebates', 'Purchase Amount', 'Payment Amount');

            $query[self::ITEMISED]['data'] = DB::select(DB::raw("
                SELECT a.purchase_number AS 'Purchase Number',
                a.inv_number as 'Invoice Number',
                a.purchase_date AS 'Purchase Date',
                c.product_code AS 'Product Code',
                e.`name` AS 'Name',
                c.quantity AS 'Quantity',
                c.unit_price/100 AS 'Unit Price',
                c.subtotal_price/100 AS 'Subtotal Price'
                FROM purchases a
                INNER JOIN orders b ON a.id = b.purchase_id
                INNER JOIN items c ON b.order_number = c.order_number
                INNER JOIN panel_products d ON c.product_id = d.id
                INNER JOIN global_products e ON d.global_product_id = e.id
                WHERE a.purchase_status IN (3001,3002,3003,3013,4000,4001,4002,4007) AND
                (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
                ORDER  BY a.purchase_date, a.purchase_number;
                "));
            $query[self::ITEMISED]['title'] = 'Itemised (Website)';
            $query[self::ITEMISED]['header'] = array('Purchase Number', 'Invoice Number', 'Purchase Date', 'Product Code', 'Name', 'Quantity', 'Unit Price', 'Subtotal Price');
        }

        if ($counter) {
            return $query[$counter];
        } else {
            return $query;
        }
    }
}
