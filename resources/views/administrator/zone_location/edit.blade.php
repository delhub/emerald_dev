@extends('layouts.administrator.main')

@section('content')

                        {{-- @php
                        dd($new_zone);
                        @endphp --}}

    <div class="row">
        <div class="col-12">
            {{-- href="/administrator/faq" --}}
            <a href="{{ route('administrator.zone_location.index') }}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>

    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">

                    <h4>
                        Edit Zones Locations
                    </h4>
                    {{-- @include('inc.messages') --}}
                </div>

                <div class="col-12">
                    <form action="{{ route('administrator.zone_location.update', ['id' => $zone_locations->id]) }}"
                        id="edit-global-zones-form" method="POST" enctype="multipart/form-data">

                        {{-- <form 
                            id="edit-global-zones-form" method="POST" enctype="multipart/form-data"> --}}

                        @csrf
                        @method('POST')

                        {{-- @if(count($zone2) > 0)

                        @foreach($zone2 as $zone) --}}

                        <div class="row">
                            
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Location Type
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('location_type') ? ' has-error' : '' }}">
                                <input type="text" id="location_type" name="location_type" class="form-control" value="{{ $zone_locations->location_type }}">

                                {{-- <input type="text" id="location_type" name="location_type" class="form-control" value=""> --}}

                                <small class="text-danger">{{ $errors->first('location_type') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Location Id
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
                                <input type="text" id="location_id" name="location_id" class="form-control" value="{{ $zone_locations->location_id }}">

                                {{-- <input type="text" id="location_id" name="location_id" class="form-control" value=""> --}}

                                <small class="text-danger">{{ $errors->first('location_id') }}</small>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Zone Id
                                </p>
                            </div>

                            {{-- <div class="col-12 col-md-3 form-group">

                                @if(count($zones) > 0)
                                <select name="zone_id" id="zone_id" class="select2 form-control form-group{{ $errors->has('zone_id') ? ' has-error' : '' }}" style="width: 100%;">
                                      
                                    @foreach($zones as $zone)
    
                                    <option value="{{ $zone->zone_id }}">{{ $zone->zone_id }}</option>
    
                                    @endforeach
                                        
                                </select>	

                                <small class="text-danger">{{ $errors->first('zone_id') }}</small>

                                @endif
                              
                            </div> --}}

                            <div class="col-12 col-md-3 form-group{{ $errors->has('zone_id') ? ' has-error' : '' }}">
                                <input type="text" id="zone_id" name="zone_id" class="form-control" value="{{ $zone_locations->zone_id }}">

                                <small class="text-danger">{{ $errors->first('zone_id') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection