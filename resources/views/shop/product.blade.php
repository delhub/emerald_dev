@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection

<div class="product-home">

    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="/shop/category/{{ $category }}">{{ $category }}</a></li>
                {{-- <li><span>/</span></li>
                <li>{{ $products->name }}</li> --}}

            </ul>
        </div>

    </div>
    <!-- sample for test dynamic banner Mobile  end -->
    <section>
        {{-- <div class="clickme-info">
            <a data-bs-toggle="modal" href="#waringInfo" id="warning-modal" data-button="Back" role="button">
                <img src="{{asset('/images/icons/clickme-icon.png')}}" alt="">
            </a>
        </div> --}}
        <div class="container my-mb-5">

            <div class="side-stickinfo active">
                <div class="open-icon"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                {{-- <div class="close-icon"><i class="fa fa-angle-left" aria-hidden="true"></i></div> --}}
                <ul>
                    <li>
                        <div class="side-info sales-count label-p-1">
                            <p>0</p>
                            <span>Sold</span>
                        </div>
                    </li>
                    <li>
                        <a class="likeme" onclick="toggleUpdateLike(this)"
                            data-liked="{{ $products->panelProduct->id }}"
                            data-count="{{ $products->panelProduct->total_like }}">
                            <div
                                class="side-info like likeBubble {{ $products->panelProduct->total_like > 0 ? 'initialLiked' : 'notLiked' }}">
                                @if (Auth::check())
                                <div id="talkbubble">
                                    <span>Click me to like</span>
                                </div>
                                @endif

                                <div class="like-icon {{ $products->panelProduct->total_like > 0 ? 'active' : '' }}">

                                    <img src="{{ asset('/images/icons/defalut-likes-icon.png') }}" class="one like"
                                        alt=""
                                        style="{{ $products->panelProduct->getIsLikedAttribute() ? 'display:none' : '' }}">
                                    <img src="{{ asset('/images/icons/likes-icon.png') }}" class="two" alt=""
                                        style="{{ $products->panelProduct->getIsLikedAttribute() ? '' : 'display:none' }}">
                                </div>
                                <div class="like-bg">
                                    <p id="newLike">{{ $products->panelProduct->total_like }}</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <div class="side-info">
                            <img src="{{ asset('/images/icons/h-delivery-icon.png') }}" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="side-info">
                            <img src="{{ asset('/images/icons/selfpickup-icon.png') }}" alt="">
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-12 pr height-fit">
                    <div class="slider slider-for tp">

                        @foreach ($images as $image)
                            <div class='ex1'>
                                <img class="product-itemimg  product-item-bg"
                                    src="{{ asset('storage/' . $image->path . $image->filename) }}" alt="">
                            </div>
                        @endforeach
                        {{-- <div>
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/1-default.png')}}" alt="">

                        </div>
                        <div>
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/3-default.png')}}" alt="">

                        </div>
                        <div>
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/4-default.png')}}" alt="">

                        </div>
                        <div>
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/5-default.png')}}" alt="">

                        </div> --}}
                    </div>
                    {{-- slider nav --}}
                    <div class="slider slider-nav slick-dotted bot">
                        @foreach ($images as $image)
                            <div class="addpd">
                                <img class="product-itemimg"
                                    src="{{ asset('storage/' . $image->path . $image->filename) }}" alt="">

                            </div>
                        @endforeach
                        {{-- <div class="addpd">
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/1-default.png')}}" alt="">

                        </div>
                        <div class="addpd">
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/3-default.png')}}" alt="">

                        </div>
                        <div class="addpd">
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/4-default.png')}}" alt="">

                        </div>
                        <div class="addpd">
                            <img class="product-itemimg" src="{{asset('storage/uploads/images/products/2/5-default.png')}}" alt="">

                        </div> --}}
                    </div>
                </div>
                <div class="col-md-7 col-sm-12">
                    <div class="productitem-info my-mb-3">
                        {{-- <p> --}}
                        {{-- {{$images[0]}}
                            {{$images[0]->path}}
                            {{$images[0]->filename}} --}}

                        {{-- <img src="{{asset('storage/'.$images->path.$images->filename)}}" alt=""> --}}
                        {{-- </p> --}}
                        <h3>{{ $products->name }}</h3>
                        <div class="p-detail">
                            <p>
                                {{-- product_content_1 --}}
                                {!! nl2br($products->panelProduct->product_content_1) !!}
                            </p>
                        </div>
                        {{-- Offer price shift to fixed price temporary --}}
                        <div class="item-pricelist">
                            <div class="non-member-price">
                                <p id="fixed_price" class="fixed_price">
                                    <span>{{ country()->country_currency }}</span>
                                    {{ number_format($products->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                </p>
                                <p>Fixed Price</p>
                            </div>

                            <div class="offer-price">
                                <img src="{{ asset('/images/icons/huangmao.png') }}" class="" alt="">
                                <p id="offer_price" class="offer_price">
                                    <span>{{ country()->country_currency }}</span>
                                    {{ number_format($products->panelProduct->attributes->first()->getPriceAttributes('web_offer_price') / 100, 2) }}
                                </p>

                                <p>Offer Price</p>
                            </div>

                            {{-- @php
                                if ($userLevel == 6001) {
                                    $priceType = 'Advance Price';
                                    $price = number_format(($products->panelProduct->attributes->first()->getPriceAttributes('price') / 100),2);
                                } else {
                                    $priceType = 'Premium Price';
                                    $price = number_format(($products->panelProduct->attributes->first()->getPriceAttributes('member_price') / 100),2);
                                }

                            @endphp --}}

                            <!-- @if ($userLevel == 6001) <div class="member-price">
                                <img src="{{ asset('/images/icons/huangmao.png') }}" class="" alt="">
                                <p id="advance_price"><span>{{ country()->country_currency }}</span>{{ number_format($products->panelProduct->attributes->first()->getPriceAttributes('price') / 100, 2) }}</p>
                                <p>Advance Price</p>
                            </div> @endif -->
                            @if (Auth::check())
                            @if ($userLevel == 6002)
                                <div class="member-price">
                                    <img src="{{ asset('/images/icons/huangmao.png') }}" class="" alt="">
                                    <p id="premium_price">
                                        <span>{{ country()->country_currency }}</span>{{ number_format($products->panelProduct->attributes->first()->getPriceAttributes('member_price') / 100, 2) }}
                                    </p>
                                    <p>Premium Price</p>
                                </div>
                            @endif
                            @endif

                        </div>
                    <div class="item-pricelist">
                            @if ($products->panelProduct->attributes->first()->getPriceAttributes('point') != 0)
                                <div class="non-member-price">
                                    <p id="point">
                                        {{ number_format($products->panelProduct->attributes->first()->getPriceAttributes('point')) }}
                                    </p>
                                    <p>Points</p>
                                </div>
                            @endif
                            {{-- @php
                                $point_cash = $products->panelProduct->attributes->first()->getPriceAttributes('point_cash');
                            @endphp
                            @if ($point_cash)
                                <div class="non-member-price">
                                    <p id="cash-point">{{ number_format(json_decode($point_cash)->point) }}<span> + </span><span> MYR</span>{{ number_format(json_decode($point_cash)->cash) }}</p>
                                    <p>Points + Cash</p>
                                </div>
                            @endif --}}
                        </div>

                        {{-- footer menu at mobile --}}
                        <div class="ft-cart-content-body">
                            @include('layouts.shop.footer-menu.product-ft-cart')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="full-hrline-grey">

        <div class="container">
            <div class="row">
                <ul class="nav nav-pills my-pb-5" id="pills-tab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#detial-info" role="tab"
                            aria-controls="pills-home" aria-selected="true">
                            <img src="{{ asset('/images/icons/icon041.png') }}" alt="">
                            Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#ingredients-info"
                            role="tab" aria-controls="pills-profile" aria-selected="false">
                            <img src="{{ asset('/images/icons/icon042.png') }}" alt="">
                            Ingredients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-article-tab" data-toggle="pill" href="#video-info" role="tab"
                            aria-controls="pills-contact" aria-selected="false">
                            <img src="{{ asset('/images/icons/icon043.png') }}" alt="">
                            Articles & Video</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#testimonials-info"
                            role="tab" aria-controls="pills-contact" aria-selected="false">
                            <img src="{{ asset('/images/icons/icon044.png') }}" alt="">
                            Testimonials</a>
                    </li>
                </ul>
                <div class="fml tab-content my-mt-5" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="detial-info" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials my-mb-3 mb-5">
                                    <h3 class="my-mb-1 tit">
                                        Product Description
                                        <div class="info-hrline-red"></div>
                                    </h3>

                                    <p>
                                        {{-- product_description --}}
                                        {!! nl2br($products->panelProduct->product_description) !!}
                                    </p>
                                    <ul class="product-inner-info">
                                        <li>
                                            <p>Place Origin :</p>
                                        </li>
                                        @foreach ($products->panelProduct->originPlace() as $origin)
                                            {{-- <li>
                                                <img src="{{asset('/images/icons/country_icon/'.$origin->slug.'.png')}}" alt="">
                                            </li> --}}
                                            <li>
                                                <p><span>{{ $origin->name }}</span></p>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1 tit">
                                        Usage & Dosage
                                        <div class="info-hrline-red"></div>
                                    </h3>
                                    <ul class="product-inner-info ussage">
                                        <h5>{!! $products->panelProduct->product_content_3 !!}</h5>

                                        <p>{!! nl2br($products->panelProduct->product_content_2) !!}</p>

                                        {{-- @foreach (json_decode($products->panelProduct->product_content_3) as $key => $item)
                                        @if ($item != null)
                                        <li>
                                            <div class="categories-ussage">
                                                <div class="ussage-group">
                                                    <img src="{{asset('/images/icons/icon-'.$key.'.png')}}" alt="">
                                                </div>
                                                <div class="ussage-group">
                                                    <h5>{{ ucfirst($key) }}</h5>
                                                    <p>{{ $item }}</p>
                                                </div>
                                            </div>
                                        </li>
                                        @endif
                                        @endforeach --}}
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials my-mb-3 mb-5">
                                    <h3 class="my-mb-1 tit">
                                        Trust And Reliability
                                        <div class="info-hrline-red"></div>
                                    </h3>
                                    <p>
                                        {{-- Trust and Reliability(4) --}}
                                        Coming Soon
                                    </p>
                                </div>

                                <div class="info-detials use my-mb-3">
                                    <h3 class="my-mb-1 tit">
                                        Why Use
                                        <div class="info-hrline-red"></div>
                                    </h3>

                                    <p>
                                        {{-- Why use(5) --}}
                                        {!! $products->panelProduct->short_description !!}
                                    </p>

                                    {{-- <img class="info-detials-img" src="{{asset('/images/product_item/test_mask.png')}}"> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="ingredients-info" role="tabpanel"
                        aria-labelledby="pills-profile-tab">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">

                                <div class="info-detials my-mb-3">
                                    <h3 class="my-mb-1 tit">
                                        Ingredients
                                        <div class="info-hrline-red"></div>
                                    </h3>
                                    <h5>Product Contains:</h5>
                                    <table class="table table-borderless">
                                        <tbody>
                                            @if (isset($products->ingredients))
                                                @foreach (json_decode($products->ingredients) as $key => $ingredient)
                                                    <tr>
                                                        <td>
                                                            {{ $key }}
                                                        </td>
                                                        <td>
                                                            {{ $ingredient }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            <div class="col-md-4 col-sm-12">

                                <div class="info-detials my-mb-3">
                                    <img class="ingredients-img"
                                        src="{{ asset('/images/product_item/test_mask.png') }}">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="video-info" role="tabpanel" aria-labelledby="pills-article-tab">
                        <div class="info-detials my-mb-3">
                            @if (empty($products->panelProduct->product_content_4))
                                <h3 class="my-mb-1">
                                    Coming Soon
                                    <div class="info-hrline-red"></div>
                                </h3>
                            @else
                            <h3 class="my-mb-1">
                                Article :
                                <br>
                                <div class="info-hrline-red"></div>
                            </h3>
                                {!! ($products->panelProduct->product_content_4) !!}
                            @endif

                        </div>
                        {{-- <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <a id="play-video" class="play-vedio-btn" href="#">
                                    <img src="{{asset('/images/icons/play_img.png')}}" alt="">
                                </a>
                                <div class="info-detials video-box my-mb-3">
                                    <iframe id="top-video" width="100%" height="300" src="https://www.youtube.com/embed/uie9R6fjcc0?rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials video my-mb-3">
                                    <h3 class="my-mb-1">
                                        Product
                                    </h3>
                                    <span>
                                        Introduction
                                        <div class="info-hrline-red"></div>
                                    </span>
                                    <p>
                                        A specifically formulated combination of nutrients which helps reduce the symptoms of stress.

                                        Blackmores Executive B Stress Formula contains a combination of ingredients which are beneficial in times of ongoing stress. It can temporarily relieve the symptoms of stress and support energy production and the healthy function of the nervous system. Blackmores Executive B Stress Formula also contains nutrients which are necessary for the heathy functioning of adrenal glands and which can also support the body's response to stress.
                                    </p>
                                </div>

                            </div>

                            </div>
                            <div class="row">

                            <div class="col-md-6 col-sm-12">

                                <div class="info-detials video my-mb-3">
                                    <h3 class="my-mb-1">
                                        Scientific Doctor
                                    </h3>
                                    <span>
                                        Recommend
                                        <div class="info-hrline-red"></div>
                                    </span>
                                    <p>
                                        A specifically formulated combination of nutrients which helps reduce the symptoms of stress.

                                        Blackmores Executive B Stress Formula contains a combination of ingredients which are beneficial in times of ongoing stress. It can temporarily relieve the symptoms of stress and support energy production and the healthy function of the nervous system. Blackmores Executive B Stress Formula also contains nutrients which are necessary for the heathy functioning of adrenal glands and which can also support the body's response to stress.
                                    </p>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <a id="play-video02" class="play-vedio-btn" href="#">
                                    <img src="{{asset('/images/icons/play_img02.png')}}" alt="">
                                </a>
                                <div class="info-detials video-box my-mb-3">
                                    <iframe id="top-video02" width="100%" height="300" src="https://www.youtube.com/embed/ErPsyBUCijM?rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>

                            </div>

                        </div> --}}
                    </div>
                    <div class="tab-pane fade" id="testimonials-info" role="tabpanel"
                        aria-labelledby="pills-contact-tab">
                        <div class="info-detials my-mb-3">
                            <h3 class="my-mb-1">
                                Coming Soon
                                <div class="info-hrline-red"></div>
                            </h3>
                        </div>
                        {{-- <div class="user-bg">
                            <div class="row ">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="rate-start-result my-mb-1">
                                                <h3>5.0</h3>
                                                <span>5 out of 5 stars</span>
                                                <div class="star-rate">
                                                    <img src="{{asset('/images/icons/star_bg.png')}}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="rate-line">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                      <tr>
                                                        <td class="rate01"><span>5</span></td>
                                                        <td class="rate02"><img src="{{asset('/images/icons/small_star.png')}}" alt=""></td>
                                                        <td class="rate03"><div class="line-clr active"></div></td>
                                                        <td class="rate04"><span>(100)</span></td>
                                                      </tr>
                                                      <tr>
                                                        <td class="rate01"><span>0</span></td>
                                                        <td class="rate02"><img src="{{asset('/images/icons/small_star.png')}}" alt=""></td>
                                                        <td class="rate03"><div class="line-clr"></div></td>
                                                        <td class="rate04"><span>(100)</span></td>
                                                      </tr>
                                                      <tr>
                                                        <td class="rate01"><span>0</span></td>
                                                        <td class="rate02"><img src="{{asset('/images/icons/small_star.png')}}" alt=""></td>
                                                        <td class="rate03"><div class="line-clr"></div></td>
                                                        <td class="rate04"><span>(100)</span></td>
                                                      </tr>
                                                      <tr>
                                                        <td class="rate01"><span>0</span></td>
                                                        <td class="rate02"><img src="{{asset('/images/icons/small_star.png')}}" alt=""></td>
                                                        <td class="rate03"><div class="line-clr"></div></td>
                                                        <td class="rate04"><span>(100)</span></td>
                                                      </tr>
                                                      <tr>
                                                        <td class="rate01"><span>0</span></td>
                                                        <td class="rate02"><img src="{{asset('/images/icons/small_star.png')}}" alt=""></td>
                                                        <td class="rate03"><div class="line-clr"></div></td>
                                                        <td class="rate04"><span>(100)</span></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 user-cm-list user-hr">
                                    <ul>
                                        <li>
                                            <div class="user-cm">
                                                <div class="user-avatar">
                                                    <img src="{{asset('/images/user_photo/user01.png')}}" alt="">
                                                </div>
                                                <div class="cm-list">

                                                    <h6>Tan Yau Sheng</h6>
                                                    <ul>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                    </ul>
                                                    <span>Blackmores Executive B Stress Formula</span>
                                                    <p>
                                                        A specifically formulated combination of nutrients which helps reduce the symptoms of stress. A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user-cm">
                                                <div class="user-avatar">
                                                    <img src="{{asset('/images/user_photo/user01.png')}}" alt="">
                                                </div>
                                                <div class="cm-list">

                                                    <h6>Tan Yau Sheng</h6>
                                                    <ul>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                    </ul>
                                                    <span>Blackmores Executive B Stress Formula</span>
                                                    <p>
                                                        A specifically formulated combination of nutrients which helps reduce the symptoms of stress. A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="user-cm">
                                                <div class="user-avatar">
                                                    <img src="{{asset('/images/user_photo/user01.png')}}" alt="">
                                                </div>
                                                <div class="cm-list">

                                                    <h6>Tan Yau Sheng</h6>
                                                    <ul>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                        <li>
                                                            <img src="{{asset('/images/icons/small_star.png')}}" alt="">
                                                        </li>
                                                    </ul>
                                                    <span>Blackmores Executive B Stress Formula</span>
                                                    <p>
                                                        A specifically formulated combination of nutrients which helps reduce the symptoms of stress. A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps reduce the symptoms of stress.A specifically formulated combination of nutrients which helps.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="fml-special-pro my-mb-3 my-pt-7">
        <div>
            <div class="row special-pro">
                <div class="col-md-12 text-center mb-5 mt-5">
                    <h1>
                        You Might Be <span>Interested</span>
                        <div class="info-hrline-red"></div>
                    </h1>
                </div>

                <div class="center my responsive slider">
                    @foreach ($widgetBlock as $product)
                        @php
                            $categorySlug = $product->categories
                                ->where('parent_category_id', 0)
                                ->where('type', 'shop')
                                ->first()->slug;
                            $slug = $categorySlug ?? '';
                        @endphp
                        <div class="special-box">
                            <div class="special-item-box">
                                <div class="special-left">
                                    @if ($product->defaultImage)
                                        <img class="imgproduct"
                                            src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                            alt="">
                                    @else
                                        <img class="imgproduct"
                                            src="{{ asset('/images/product_item/default-p.png') }}" alt="">
                                    @endif
                                </div>
                                <div class="special-right">
                                    @if (!empty($product->panelProduct->attributes->first()->discount_percentage))
                                        <div class="orfer-special">
                                            <div class="sale-special">
                                                <p>{{ $product->panelProduct->attributes->first()->discount_percentage }}%
                                                    <span>OFF</span>
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="name-special">
                                        <p class="special-tittle">{{ $product->name }}</p>
                                        <p class="special-price">{{ country()->country_currency }}
                                            {{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                        </p>
                                    </div>
                                    <a href="/shop/product/{{ $slug }}/{{ $product->name_slug }}?panel={{ $product->panelProduct->panel_account_id }}"
                                        class="fml-btn buy-btn link-product" data-bs-original-title="" title=""> Buy
                                        Now</a>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>

</div>

<div class="modal fade" id="waringInfo" aria-hidden="true" aria-labelledby="waringInfoLabel" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered warnings-popup-bg">
        <div class="modal-content edit-info-modal-box notice-border">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="col-md-8 warnings-details">
                <div class="warning-text">
                    <h5>WARNINGS</h5>
                    @if ($products->panelProduct->product_content_5)
                        <p>{!! $products->panelProduct->product_content_5 !!}</p>
                    @else
                        <p>Coming Soon</p>
                    @endif
                </div>
                <div class="showlink"></div>

                {{-- <div class="caution-text">
                <h5>CAUTION</h5>
                <p>{!! $products->caution !!}</p>
              </div> --}}
                <div class="edit-opt-btn">
                    <button class="cancel-btn btn-primary" id="modal-button">Proceed</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="waringInfo-buy-cart" aria-hidden="true" aria-labelledby="waringInfoLabel" tabindex="-1">
    <div class="modal-dialog modal-xl modal-dialog-centered warnings-popup-bg">
        <div class="modal-content edit-info-modal-box notice-border">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row warning-box">
                    <div class="col-md-4">
                        <img class="notice-img-icon" src="{{ asset('images/formula/warnings.png') }}" alt="">
                    </div>
                    <div class="col-md-8 warnings-details">
                        <div class="warning-text">
                            <h5>WARNINGS</h5>
                            @if ($products->panelProduct->product_content_5)
                                <p>{!! $products->panelProduct->product_content_5 !!}</p>
                            @else
                                <p>Coming Soon</p>
                            @endif
                        </div>
                        <div class="showlinkCart">
                            <div id="showCart">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-quantity-ft"
                                    class="ftitemCount">0</span>
                            </div>
                            <a id="DeleteCartItem"
                                onclick="{{ $products->display_only == 1 ? '' : 'ajaxSaveCart(false)' }}"
                                href="{{ $products->display_only == 1 ? '#' : '' }}" class="link-product add_cart">
                                <div class="p-m-add-btn"
                                    style="{{ $products->display_only == 1 ? 'background:darkgrey;cursor:pointer;' : '' }}">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                        <div class="showlinkBuy">
                            <a class="in-warning buy-btn link-product buy_now"
                                onclick="{{ $products->display_only == 1 ? '' : "ajaxSaveCart(true)" }}"
                                style="{{ $products->display_only == 1 ? 'background:darkgrey;cursor:pointer;' : '' }}">
                                Buy Now
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="remindedModal" aria-hidden="true" aria-labelledby="remindedModal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered edit-popup-bg modal-lg">
        <div class="modal-content edit-info-modal-box">
            <div class="modal-header">
                <h1 class="modal-title edit-tittle" id="address_edit_formLabel">Reminder
                </h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body reminded-packages">

                @include('layouts.shop.footer-menu.product-reminder')
            </div>
        </div>
    </div>
</div>

<!-- ok Modal (after submit rbs month) -->
<div class="modal fade" id="okModal" aria-hidden="true" aria-labelledby="okModal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered edit-popup-bg">
        <div class="modal-content edit-info-modal-box">
            <div class="modal-header">
                <h5 class="modal-title" id="okModalLabel">Added Reminder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>You may view/edit settings at “My Dashboard”-> Recurring Booking System(RBS)</p>
            </div>
            <div class="modal-footer">
                    <button class="btn rbssubmit" type="button" data-bs-dismiss="modal" onClick="window.location.reload();">OK</button>
            </div>
        </div>
    </div>
</div>
{{-- if item quantity is over limit --}}
<div class="modal fade" id="abc" aria-hidden="true" aria-labelledby="limitProduct" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered edit-popup-bg">
        <div class="modal-content edit-info-modal-box">
            <div class="modal-header">
                <h1 class="modal-title edit-tittle" id="limitProduct"><span>Warning</span></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body limit-over">
                <p>Each person limit to 50 units</p>
            </div>
        </div>
    </div>
</div>
{{-- if item quantity is over limit end--}}

{{-- if item purchase is over limit --}}
<div class="modal fade" id="itemPurchaseLimit" aria-hidden="true" aria-labelledby="limitProduct" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered edit-popup-bg">
        <div class="modal-content edit-info-modal-box">
            <div class="modal-header">
                <h1 class="modal-title edit-tittle" id="limitProduct"><span>Warning</span></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body limit-over">
            </div>
        </div>
    </div>
</div>
{{-- if item purchase is over limit end--}}

<!-- mobile footer menu bar -->
@include('layouts.shop.navigation.product-ft-menu')
@endsection

@push('style')
<style>
    #DeleteCartItem {
        cursor: pointer;
    }

    a.fml-btn.buy-btn:hover {
        cursor: pointer;
        color: white;
        user-select: none;
    }

    .pointerEvents{
        pointer-events:none;
    }
    .purchase-limit p {
        text-align: justify;
    }
    .purchase-limit p .purchase-limit-red {
        font-weight: bold;
        color: #ef3842;
    }
    .purchase-limit p .purchase-limit-red2 {
        font-weight: bold;
        color: #ef3842;
        display: inline-block
    }
    .purchase-limit p .purchase-limit-bold {
        font-weight: bold;
    }
    p.limit-text-variation {
        color: #ef3842;
        font-size: 12px;
        line-height: 13px;
        font-weight: 500;
    }

    .view-product-without-login-box {
        display: flex;
        width: 100%;
        background: #f7f7f7;
        border-radius: 9px;
        border: 1px solid #dbdbdb;
        padding: 15px 15px;
        margin-top: 25px;
        position: relative;
        height: fit-content;
        align-items: center;
        gap: 30px;
        text-align: left;
        justify-content: space-around;
    }
    .view-product-without-login-box-mb {
        display: none;
    }
    @media screen and (max-width: 458px){
        .view-product-without-login-box {
            display:none;
        }
        .view-product-without-login-box-mb {
            z-index: 9999;
            display: flex;
            align-items: center;
            background: linear-gradient(177deg, #70abff 0%, #3d5fb5 96.99%);
            color: white;
            position: fixed;
            bottom:0px;
            width: 100%;
        }
        .view-product-without-login-box-mb .text-messages {
            text-align:center; 
            padding: 15px;
            background: #e9e9e9;
            color: #263a8b !important;
        }
        .view-product-without-login-box-mb .login-button {
            padding: 15px; 
            font-size:15px;
        }
    }
</style>

@endpush
@push('script')
<script>

    let getAllQlyTot = '';
    let val = '';
    let rmval = '';
    let x = false;
    let rbstimeVal = 0;
    let rbsqltValue = 0;
    let totalRBS = 0;
    let rbsqltPricePer = 0;
    let rbsqltShippingPer = 0;
    let rbsQtyValue = 0;
    let qtyOnChange = 0;
    let maxTotrbs = 0;
    let getRbsBuyQlt = 0;
    let rbsValue = 0;
    let getSlcID = '';
    let rBuy = false;

    $('#varqty').change(function() {
            qtyOnChange =  $(this).val()
        });

    $('.sales-count').html('<p>' + $('input[name=attributesPrice]').data('sales-count') +
        '</p><span>Sold</span>');

    $('input[name=attributesPrice]').change(function() {

        $('input[type="radio"][name=attributesPrice]').removeAttr('checked');
        $('.offer_price').html('<span>MYR</span>' + $(this).data('offer-price'));
        $('.fixed_price').html('<span>MYR</span>' + $(this).data('fixed-price'));
        $('#advance_price').html('<span>MYR</span>' + $(this).data('advance-price'));
        $('#premium_price').html('<span>MYR</span>' + $(this).data('premium-price'));
        $('.sales-count').html('<p>' + $(this).data('sales-count') + '</p><span>Sold</span>');
        $('.rbs-col-hide').hide();
        $('.popupReminder').hide();
        $('#rbsDrop'+$(this).data('variation-id')).show();
        $('#popupReminder-'+$(this).data('variation-id')).show();

        if ($(this).data('allow-reminder') == 1) {
            $('.remind-bell').show();
        } else {
            $('.remind-bell').hide();
        }

        getSlcID = $(this).attr('id');

        getValVariationId(getSlcID);

    });

    function getValVariationId(getSlcID) {
        const y = document.querySelectorAll('input[name=attributesPrice]');
        y.forEach(itemPrice =>{

            if(itemPrice.id === getSlcID){
                itemPrice.checked = true;
            }
        })


        //console.log(getSlcID);
    }



    // $('#DeleteCartItem').click(function() {
    //     ajaxSaveCart();
    // });
    function changeRBSmonth(months){
        const selectedProduct =  $('input[name=attributesPrice]');
        selectedProduct.data('rbs-months',months);
    }

    function addFtCart(g) {
        const cartQuantityFt = document.querySelectorAll('#cart-quantity-ft');
        cartQuantityFt.forEach(item => {
            item.innerHTML = g;
        })
    }

    function ajaxSaveRbsPostpaid(){
        const selectedProduct =  $('input[name=attributesPrice]').filter(":checked");

        $.ajax({
            url: '/shop/addRbsPostpaid',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "id": selectedProduct.data('variation-id'),
                "rbs_month": selectedProduct.data('rbs-months'),
            },
            beforeSend: function() {
            },
            success: function(success) {
                $('#okModal').modal('show');
            },
            error: function() {
                location.reload();
            }
        });
    }

    //triggered when modal is about to be shown
    $('#waringInfo').on('show.bs.modal', function(e) {
        //    console.log($('#warning-modal').data('button'));
        var buttonText = $(e.relatedTarget).data('button');
        $('#modal-button').text(buttonText);
        if (buttonText == 'Back') {
            $('#modal-button').attr('data-bs-dismiss', 'modal');
        } else {
            $('#modal-button').click(function() {
                ajaxSaveCart(true);
                setTimeout(function() {
                    window.location.href = "/shop/cart";
                }, 1000);
            });
        }
    });

    let totlike = @JSON($products->panelProduct->total_like);

    function likeifTrue() {
        const likeinner = document.querySelector('.like-icon');
        const likebox = document.querySelector('.side-info.like.likeBubble');
        if (totlike > 0) {
            likeinner.classList.add('active');
            likebox.classList.add('liked');
        } else {
            likeinner.classList.remove('active');
            likebox.classList.remove('liked');
        }
    }
    //likeifTrue();

    function toggleUpdateLike(obj) {
        let button = $(obj);
        let product_id = button.data('liked');
        let oldLike = button.data('count');
        let likeIcon = $('.like-icon');
        let imgOne = $('.one.like');
        let imgTwo = $('.two');
        const likeinner = document.querySelector('.like-icon');
        const likebox = document.querySelector('.side-info.like.likeBubble');

        // let loader = button.find('.spinner-border');
        // let label = button.find('.fa-heart');
        //let count = button.find('.liked-btn');

        $.ajax({
            // async: true,
            beforeSend: function() {

            },
            complete: function() {
                // loader.hide();
            },
            url: '{{ route('shop.toggle-perfect-list') }}',
            type: 'POST',
            data: {
                '_token': '{{ csrf_token() }}',
                'id': product_id
            },
            success: function(result) {

                button.find('#newLike').html(result.data.likes);

                if (result.data.liked) {
                    imgOne.hide();
                    imgTwo.show();
                    likeinner.classList.add('active');
                    likebox.classList.add('liked');
                    likebox.classList.remove('initialLiked');
                    likebox.classList.remove('notLiked');
                } else {
                    imgOne.show();
                    imgTwo.hide();
                    likeinner.classList.remove('active');
                    likebox.classList.remove('liked');
                    likebox.classList.remove('initialLiked');
                    likebox.classList.add('notLiked');
                }
                //    likeIcon.addClass('active');

                //button.data('test' ,result.data.likes);
                // button.attr('data-count', result.data.likes);
            },
            error: function(result) {
                label.html('error');
            }
        });
    };

    window.onload = () => {

        $('.rbs-col.rbs-col-hide.active.pointerEvents').removeClass('pointerEvents');

        //click buy or add to cart if Tick Warning

        const cartTick = document.querySelector('.showlinkCart');
        const buyTick = document.querySelector('.showlinkBuy');
        const buyBtnClick = document.querySelectorAll('.link-product.buy_now.warning');
        const cartBtnClick = document.querySelectorAll('.link-product.add_cart');
        const modalBox = document.querySelector('#waringInfo-buy-cart');

        //if click buy dont show the notice word
        $( ".buy_now" ).on( "click", function() {
            rBuy = true;
        });


        cartBtnClick.forEach(cartitem => {
            //console.log(cartitem);
            cartitem.addEventListener('click', (e) => {
                cartTick.classList.add('active');

                if (buyTick.className == 'showlinkBuy active') {
                    buyTick.classList.remove('active')
                }
                e.preventDefault();
            })
        })

        buyBtnClick.forEach(item => {
            console.log(item);
            item.addEventListener('click', (e) => {
                buyTick.classList.add('active');

                if (cartTick.className == 'showlinkCart active') {
                    cartTick.classList.remove('active')
                }
                e.preventDefault();

            });

        });

        //Product Images add Zoom effect
        $('.ex1').zoom();

        //RBS checkbox function add new start

        const rbsBox = document.querySelectorAll('.rbs-col.active');
        const rbscheckbox = document.querySelectorAll('.price-box input[type="checkbox"]');
        const showSlcItem = document.querySelectorAll('.rbs-note');
        const showMbfrequency = document.querySelector('.ft-price-img .rbs-price .price.month span');
        const showMBtimes = document.querySelector('.ft-price-img .rbs-price .price.time span');
        const showRbstotPri = document.querySelector('.rbs-active');
        const rbsNotslc = document.querySelector('.ft-price');
        const rbsvarintion = document.querySelectorAll('.item-variation .variation-box label');
        const rbsvariationMobile = document.querySelector('.item-variation.mobile');
        const rbsQltSetValue = document.querySelectorAll('.ft-cart-content .item-buy-section-quantity');
        const bellSlc = document.querySelectorAll('.remind-bell');
         //note rbs removed
        const noteRbsRmv = document.querySelector('.item-buy-section .noteRbs-add');
        const noteRbsRmvmobile = document.querySelector('.item-buy-section .noteRbs-add-mobile');

        rbsBox.forEach(rbsBoxEach =>{
            const openRbs = rbsBoxEach.querySelector('button.accordion-button');
            const rbsSlcTime = rbsBoxEach.querySelectorAll('.accordion-body .rbs-time-slc input[type="radio"]');
            const rbsSlcQly = rbsBoxEach.querySelectorAll('.accordion-body .rbs-qlt-slc input[type="radio"]');
            const showTotalSlcItem = document.querySelectorAll('.rbs_total');
            const rbsPrice = document.querySelectorAll('rbs-col.mb');

            //when RBS open tab function
            openRbs.addEventListener('click', changeImg);

            rbsSlcTime.forEach(t =>{
                t.addEventListener('click', getRbsVal);
            })

            rbsSlcQly.forEach(q =>{
                q.addEventListener('click', getRbsVal);
            })

            //if click variation
            rbsvarintion.forEach(variationItem =>{
                variationItem.addEventListener('click', ()=>{
                    clsAllRbc();
                });
            })

            //if click reminder bell
            bellSlc.forEach(bellitem =>{
                bellitem.addEventListener('click', ()=>{
                    clsAllRbc();
                })
            })

            function clsAllRbc(){
                if(openRbs.ariaExpanded == 'true'){
                    $(openRbs).click();
                    rbsGetSlc();
                } else{
                    return true;
                }
            }

            //when rbs button click function
            function changeImg() {
                const openRbsImg = openRbs.ariaExpanded;
                const imgOpen = rbsBoxEach.querySelector('.icon-rbs1 img');

                imgOpen.src = '/images/icons/rbs-slc.png';
                x = openRbsImg;

                if(x == 'true'){
                    rbsSlcTime[0].checked = true;
                    rbsSlcQly[0].checked = true;
                    imgOpen.src = '/images/icons/rbs-imgshow.png';
                    rbsvariationMobile.style.display = 'none';
                    //rbsPrice.style.display = 'block';
                    rbsPrice.forEach(item=>{
                        console.log('item');
                    })

                    getRbsVal();
                    $('.rbsQuantity').html('Quantity<br>Per Delivery');
                    $('.remind-bell').hide();

                } else {
                    $('.remind-bell').show();

                    noteRbsRmvmobile.style.display = 'block';
                    noteRbsRmv.style.display = 'block';
                    rbsvariationMobile.style.display = 'block';
                    setTimeout(function() {
                        noteRbsRmv.style.display = 'none';
                        noteRbsRmvmobile.style.display = 'none';
                    },1500);
                    getAllQlyTot =1;
                    rbsGetSlc();
                    $('.rbsQuantity').html('Quantity');
                    return true;
                }
            }

            //rbs get value by group
            function getRbsVal() {
                rbsSlcTime.forEach(itemt =>{

                    if(itemt.checked == true) {
                        rbstimeVal = parseInt(itemt.value);
                        if(getRbsBuyQlt < rbstimeVal){
                            //console.log('get' + getRbsBuyQlt + maxTotrbs);
                        } else {}
                    } else {}

                });
                rbsSlcQly.forEach(itemq =>{

                    if(itemq.checked == true) {
                        rbsqltValue = parseInt(itemq.value);
                        maxTotrbs = parseInt(itemq.getAttribute("data-quantity"));
                        rbsqltPricePer = parseFloat(itemq.getAttribute('data-pricePer'));
                        rbsqltShippingPer = parseFloat(itemq.getAttribute('data-shippingCat'));
                        getRbsBuyQlt = maxTotrbs;
                        if(getRbsBuyQlt < rbsqltValue){
                            //console.log('get' + getRbsBuyQlt + maxTotrbs);
                        } else {}
                    } else {};

                });

                rbsQltSetValue.forEach(qlyVal =>{
                        const ff = qlyVal.querySelector('.product-add-btn');
                        const ss = qlyVal.querySelector('.product-minus-btn');
                        const vv = qlyVal.querySelector('input#varqty');

                        vv.value = maxTotrbs;

                        ff.addEventListener('click', ()=>{
                            getRbsBuyQlt = parseInt(vv.value);
                            rbsValue = parseInt(vv.value);
                            console.log(rbsValue);
                            getTotSendItem();

                        });

                        ss.addEventListener('click', ()=>{
                            getRbsBuyQlt = parseInt(vv.value);
                            rbsValue = parseInt(vv.value);
                            console.log(rbsValue);
                            getTotSendItem();

                        });

                    })

                showValRbsSlc();

            }

            //rbs if click close tab
            function rbsGetSlc() {
                getRbsBuyQlt = 1;
                rbsqltValue = 0;
                rbsqltPricePer = 0;
                rbstimeVal = 0;
                rbsSlcTime.forEach(itemt=>{
                    itemt.checked = false;
                });
                rbsSlcQly.forEach(itemq =>{
                    itemq.checked = false;
                });

                rbsQltSetValue.forEach(itemTot =>{
                    const h = itemTot.querySelector('input#varqty');
                    h.value = 1;
                    getRbsBuyQlt = 1;
                })

                // setTimeout(function() {
                    //noteRbsRmvmobile.style.display = 'block';
                    noteRbsRmvmobile.innerHTML = `
                    *RBS have been removed!
                    `;
                    //noteRbsRmv.style.display = 'block';
                    noteRbsRmv.innerHTML = `
                    *RBS have been removed!
                    `;
                // },1500);

                showValRbsSlc();
            }

            function getTotSendItem(){

                if(getRbsBuyQlt < maxTotrbs){
                    getRbsBuyQlt = maxTotrbs;
                    //console.log("getTotSend",getRbsBuyQlt,maxTotrbs,rbsValue);
                    rbsQltSetValue.forEach(itemTot =>{
                        const h = itemTot.querySelector('input#varqty');
                        h.value = maxTotrbs;
                        getRbsBuyQlt = maxTotrbs;
                    });
                } else {
                    console.log(getRbsBuyQlt,maxTotrbs,rbsValue);

                }
                showValRbsSlc();
            }

            //rbs show value
            function showValRbsSlc(){

                showSlcItem.forEach(noteVal => {
                    if(rbsqltValue > 0 || rbstimeVal > 0){

                        noteVal.innerHTML = `
                        <p>Every <span>${rbstimeVal} month(s)</span>, we will deliver <span>${getRbsBuyQlt} item(s)</span> to you for <span>${rbsqltValue} time(s).</span></p> `;
                        rbsNotslc.style.display = 'none';
                        showRbstotPri.style.display = 'block';
                        showMbfrequency.innerHTML = `${rbsqltValue}`;
                        showMBtimes.innerHTML = `${rbstimeVal}`;

                        showTotalSlcItem.forEach(rbsPriceitem =>{
                            totalRBS = (((rbsqltPricePer*rbsqltValue)/100)*getRbsBuyQlt).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            rbsPriceitem.innerHTML= `MYR ${totalRBS}* <small>(Price per item: MYR ${((rbsqltPricePer)/100).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")})</small>`;
                        })

                        noteRbsRmvmobile.style.display = 'block';
                        noteRbsRmvmobile.innerHTML = `
                        Minimum purchase quantity is ${maxTotrbs}
                        `;

                        noteRbsRmv.style.display = 'block';
                        noteRbsRmv.innerHTML = `
                        Minimum purchase quantity is ${maxTotrbs}!
                        `;

                        // console.log('maxTotrbs',maxTotrbs);
                        if (getAllQlyTot < maxTotrbs) {
                            getAllQlyTot = maxTotrbs;
                        }
                        //

                    } else {
                        maxTotrbs = 0;
                        // getAllQlyTot = 1;
                        document.querySelectorAll('.varqty').forEach(item => {
                            item.value = getAllQlyTot;
                        })

                        // console.log('maxTotrbs2',maxTotrbs);
                        // setTimeout(function() {
                            noteVal.innerHTML = ``;
                            showMbfrequency.innerHTML = `0`;
                            showMBtimes.innerHTML = `0`;
                            showRbstotPri.style.display = 'none';
                            rbsNotslc.style.display = 'flex';
                            // noteRbsRmvmobile.style.display = 'none';
                            // noteRbsRmvmobile.innerHTML = ``;
                            // noteRbsRmv.style.display = 'none';
                            // noteRbsRmv.innerHTML = ``;
                        // },100)

                    }
                })
            }

        })

        //RBS checkbox function add new end

        $(rbscheckbox).change(function() {//suggest to use change
            //const isUnchecked = this.checked
            $('input[value="' + this.value + '"]:checkbox').prop('checked', this.checked);
            val = this.value;
            //console.log(val);
            rbscheckbox.forEach(item =>{
                if(item.value != val){
                    item.checked = false;
                } else {

                }
            })
        });

    };

    // console.log(maxTotrbs);

    function ajaxSaveCart(redirect){

        const selectedProduct =  $('input[name=attributesPrice]').filter(":checked");
        const prductQuantity =  getAllQlyTot;
        const cartQuantity =  $('#cart-quantity').text();

        let itemName = selectedProduct.data('product-name');
        let itemAttribute = selectedProduct.data('variation-name');
        let eligiblePurchase = selectedProduct.data('eligible-purchase');
        let eligibleAmount = selectedProduct.data('purchase-limit-amount');
        let eligiblePeriod = selectedProduct.data('purchase-limit-period');
        let overLimit = false;

        if (prductQuantity > eligiblePurchase) overLimit = true;
        console.log('prductQuantity', prductQuantity);
        console.log('overLimit', overLimit);
        console.log('eligiblePurchase', eligiblePurchase);
        console.log('eligibleAmount', eligibleAmount);
        console.log('eligiblePeriod', eligiblePeriod);
        //const prductQuantity =  $('.varqty').val();

        if (eligibleAmount && overLimit) {
            $('#itemPurchaseLimit .modal-body').html('<ul><li class="purchase-limit"><p>Purchase limit for <span class="purchase-limit-red"> ' + itemName + '</span> <span class="purchase-limit-red2">(' + itemAttribute + ')</span> ' + eligiblePeriod + '</span> is <span class="purchase-limit-bold"> ' + eligibleAmount + '</span> unit(s). You have a remaining balance of <span class="purchase-limit-bold">' + eligiblePurchase + '</span> unit(s) for purchase. Contact your agent or cashier for additional purchases</p></li></ul>');
            $('#itemPurchaseLimit').modal('show');
        } else {
            $.ajax({
                url: '/shop/cart/add-item',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": selectedProduct.data('variation-id'),
                    "rbs_frequency": rbstimeVal,
                    "rbs_times": rbsqltValue,
                    "rbs_price": (rbsqltPricePer*rbsqltValue)/100,
                    "rbs_minimum":maxTotrbs,
                    "rbs_price_per": rbsqltPricePer,
                    "rbs_shipping_category": rbsqltShippingPer,

                    // "process": process,

                    "quantity": prductQuantity,

                },
                beforeSend: function() {
                    //console.log('this is before' + prductQuantity);
                },
                success: function(success) {

                    //prductQuantity = productQlt;

                    //newCartQuantity = parseFloat(cartQuantity) + parseFloat(prductQuantity);
                    newCartQuantity = parseFloat(cartQuantity) + parseFloat(prductQuantity);
                    $('#cart-quantity').text(newCartQuantity);
                    addFtCart(newCartQuantity);

                    console.log(getAllQlyTot,maxTotrbs,prductQuantity);

                    if (maxTotrbs != 0) {
                        // getAllQlyTot = maxTotrbs;
                        // showValRbsSlc();
                    } else {

                        getAllQlyTot =1;
                    }

                    document.querySelectorAll('.varqty').forEach(item => {
                        //const cartNoteMgs = document.querySelector('.item-buy-section.mobile .noteRbs-add');
                        const cartNoteMgs = document.querySelectorAll('.noteRbs-add-cart');

                        item.value = getAllQlyTot;

                        console.log(getAllQlyTot);

                        cartNoteMgs.forEach(itemg => {
                            console.log('this is r ' + rBuy,typeof rBuy);
                            if( rBuy !== true){//if buy button on click = true else false
                                itemg.style.display = 'block';
                                itemg.innerHTML = `
                                    Your (${prductQuantity} item) Added to cart successfully.
                                `;
                                setTimeout(function(){
                                    itemg.style.display = 'none';
                                    itemg.innerHTML = ``;
                                },1400);
                            } else {

                            }
                        })

                    })
                    //console.log(productQlt + 'get after value');
                },
                error: function() {
                    // return false;
                    // if (confirm('Something went wrong, Please reload page')) {
                    location.reload();
                    // }
                }
            });

            if (redirect == true) {
                setTimeout(function() {
                    window.location.href = "/shop/cart";
                }, 100);
            }

        }

    }

    function funcRemind(){
            $('#remindedModal').modal('show');
            $('input[name="monthReminder"]:first').attr('checked', true );
        }

     function changeRBSmonth(value) {
            $('.inputRBS').attr('data-rbs-months',value);
        }

    function editAddressForm(select) {
        var stateID = $(select).val();

        $.ajax({
            url: '/shop/cart/get-city/' + stateID,
            type: 'GET',
            data: {
                // "_token": "{{ csrf_token() }}",
                "id": stateID
            },
            success: function(html) {
                $('#cityChoices').html(html)
            },
            error: function() {
                alert("There's error. Please reload page");
            }

        });
    }
</script>
@endpush
