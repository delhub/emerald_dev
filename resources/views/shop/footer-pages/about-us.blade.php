@extends('layouts.shop.main')

@section('content')

    <div class="backgroundImg-about-us">
        <div class="row m-0">
            <div class="d-md-none p-0">
                <img src="/images/about-us/about-us-1.jpg" style="object-fit: cover; width: 100%;">
            </div>
            <div class="offset-md-3 col-md-9 mt-3 mt-md-5">
                <h1 class="title mb-0">About us</h1>
                <div class="info-hrline-red about-us"></div>
            </div>
            <div class="row mt-md-3 mt-2 m-0">
                <div class="pl-md-4 ml-md-2">
                    <div class="ml-md-5 pl-md-5 col-md-7 text-paragraph-font">
                        <p>
                            Formula治方閣為保健良品零售公司，過去20年，創辦團隊在提昇客戶健康的事項上，有了許多重要的發現與實證，明確知道，要保持長期健康體質，並非單單依靠保健品，而是準確適量的攝取關鍵營養補助品，從而獲得最高的健康效益保障。<br>
                            Formula Healthcare is a retailer of healthcare products. The founding team had accumulated
                            many
                            important findings and testimonials on matters relating to raising one's health over the
                            past 20
                            years.
                            We are certain that in order to maintain one's health and well-being, one should not rely
                            solely
                            on
                            supplements or health products, but proper and balanced nutritional food intakes and
                            supplements.
                        </p>
                        <p>
                            市面上有著琳瑯滿目的保健產品，治方閣的專業團隊，將從中挑選最適宜的產品，讓客戶參考。我們認為，保持身體健康，無須多吃保健品，依不同年齡及身體需求，決定適宜份量，比亂吃一通，更將明智，所得效果更好。<br>
                            There is a dizzying array of health care products on the market, and the professional team
                            at
                            Formula
                            Healthcare will select the most appropriate products for customers' reference. We believe
                            that
                            there is
                            no need to consume at random to maintain good health. It would be more prudent to calculate
                            and
                            decide
                            the appropriate amount to consume based on different ages and physical needs.
                        </p>
                        <p>
                            我們決定採用大數據及高科技，協助客戶買到最佳的保健良品，經過長達半年多的準備與籌劃，治方閣順利於2021年正式營運，客戶不只可在治方閣網站上購買所需保健良品，還可以在全馬各地的實體店面採購，滿足一家老少所需的營養品。<br>
                            We have decided to use big data and cutting-edge technology to assist customers in
                            purchasing
                            the best
                            health-care products. After more than half a year of preparation and planning Formula
                            Healthcare
                            will
                            officially begin operations in 2021, Customers can now purchase products not only online
                            from
                            Formula
                            Healthcare website, but also in our physical stores throughout Malaysia, to meet the
                            nutritional
                            needs
                            of both young and old.
                        </p>
                        <p>
                            面向九紫未來，客戶將對營養品有更高的需求標準，為此，未來五年，治方閣將訓練多名營養顧問，依據廠家或專家的研究報告，分享最新最好的保健知識供客戶參考，我們相信，跟營養廠家合作，提昇客戶的保健知識，有助於避免或減少，濫吃或誤吃營養品引發不良後果的案例。<br>
                            Embracing the future of Ninth period, public will have higher or more stringent requirement
                            towards
                            nutritional products. Therefore, in the next five years, FORMULA HEALTHCARE will train teams
                            of
                            nutrition consultants to share most up-to-date and best health-care information with
                            customers
                            based on
                            research published by manufacturers or experts. We believe by collaborating with nutrition
                            manufacturers
                            , We are able to increase customer awareness of general health knowledge, thereby reducing
                            cases
                            of
                            unfavourable consequences caused by overeating nutritional products.
                        </p>
                        <p>
                            防範勝於治療，人的健康，不能單靠營養品，家居環境整潔，良好生活習慣，保持運動等，都是健康不可或缺的因素，治方閣矢言為客戶選用的營養品嚴格把關，給大家有所依據，服用真正所需，同時減少無謂浪費。<br>
                            Prevention is better than cure. One should never rely solely on nutritional supplements to
                            improve one's
                            health. A clean home environment, healthy living habits, and regular exercise are all
                            necessary
                            for good
                            health. Formula Healthcare promises to maintain strict control over the selection of
                            nutritional
                            products, and Formula Healthcare will advise customers to take only what they truly require,
                            reducing
                            unnecessary purchases.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-md-none p-0">
            <img src="/images/about-us/about-us-2.jpg" style="object-fit: cover; width: 100%;">
        </div>
        <div class="my-md-0">
            <div class="col-md-6 special-box blink_me">
                <h1 class="title mb-0" style="text-align: center;">Our Vision</h1>
                <div class="info-hrline-red vision"></div>
                <div class="row mt-md-3 mt-2 m-0 text-paragraph-font">
                    <p>
                        Raising public awareness and educating the public on proper healthcare concepts, improved public
                        health, and living a healthy lifestyle.<br>
                        治方閣願景：運用知識，提昇消費者正確健保觀念，減少病害風險，擁有健康人生。
                    </p>
                </div>
            </div>
        </div>
        <div class="d-none d-md-block">
            <div class="my-pb-3">
            </div>
        </div>
        <div class="d-md-none">
            <div class="pb-4    "></div>
        </div>
    </div>
@endsection

@push('style')
    <style>
        .fml-footer {
            margin-top: 0;
        }
        h1 {
            font-family: "Roboto", sans-serif;
            font-weight: 700;
            font-size: 28px;
        }

        .text-paragraph-font {
            text-align: justify;
            font-family: "Roboto", sans-serif;
            font-size: 16px;
            line-height: 1.3;
        }

        @media (max-width: 768px) {
            .info-hrline-red.about-us {
                position: absolute;
                left: 28%;
                width: 28px;
            }

            .info-hrline-red.vision {
                position: absolute;
                left: 59.8%;
                width: 32px;
            }

            .special-box {
                text-align: left;
                border-radius: 0px;
                position: relative;
                margin-right: 0px;
                padding: 33px 24px;
                height: auto;
            }

            .backgroundImg-about-us {
                margin-top: 50px;
                background-image: none;
                background-repeat: no-repeat;
                background-position: center;
                /*margin-top: 15%;*/
                background-size: cover;
                /*background-size: 100vw 30vh;*/
                /*width: 100vw;*/
                /*height: 30vh;*/
            }

            .text-paragraph-font {
                font-size: 0.7rem;
            }
        }

        @media (min-width: 768px) {
            .my-md-0 {
                margin-left: 12.7%;
            }
            .info-hrline-red.about-us {
                position: absolute;
                left: 7.35%;
                width: 29px;
            }

            .info-hrline-red.vision {
                position: absolute;
                left: 54.4%;
                width: 31px;
            }

            .special-box {
                text-align: left;
                position: relative;
                margin-right: 0px;
                padding: 33px 24px;
                height: auto;
            }

            .backgroundImg-about-us {
                margin-top: 80px;
                background-image: url(/images/about-us/about-us.jpg);
                background-repeat: no-repeat;
                background-position: initial;
                object-fit: cover;
                /*margin-top: 5%;*/
                background-size: cover;
                background-size: 100% 100%;
                /*width: 100vw;*/
                /*height: 100vh;*/
            }

            .col-md-7.text-paragraph-font {
                max-width: 60%;
            }
        }
    </style>
@endpush

@push('script')
    <script>
        (function blink() {
            $('.blink_me').fadeOut(250).fadeIn(250, blink).delay(4000);
        })();
    </script>
@endpush
