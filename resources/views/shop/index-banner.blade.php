@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection

<!-- desktop -->
<div style="width: 100% !important;">

    <section id="slideshow" class="hidden-sm">
        <div class="slick">
            <div>
                <a href="/shop/category/new-launching">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/1018-banner.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/packages">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/packagepanner.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/mattresses">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/R1-Banner.jpg')}}" alt="">
                </a>
            </div>
            <div class="mysliderbox">
                <a href="/shop/category/mattresses">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/W1-Banner.jpg')}}" alt="">
                </a>
            </div>
            <div class="mysliderbox">
                <a href="/shop/category/doors">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Door-Banner.jpg')}}" alt="">
                </a>
            </div>
            <div class="mysliderbox">
                <a href="/shop/category/lightings">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Light-Banner.jpg')}}" alt="">
                </a>
            </div>
            <div class="mysliderbox">
                <a href="/shop/category/bedsheets-and-mattresses/bedsheets">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Bedsheet-P-Banner.jpg')}}" alt="">
                </a>
            </div>
            <div class="mysliderbox">
                <a href="/shop/category/bedsheets-and-mattresses/bedsheets">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Bedsheet-M-Banner.jpg')}}" alt="">
                </a>
            </div>
    </section>

    <!-- <section id="slideshow" class="hidden-sm">
        <div class="slick">
            <div>
            <a href="/shop/category/mattresses">
                <img class="image-slideshow ml-5" src="{{asset('/storage/banner/banner7.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Banner1.jpg')}}" alt="">
            </div>
            <div>
                <a href="/shop/category/cabinets">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Banner2.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/bedsheets-and-mattresses/bedsheets">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Banner3.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/lightings">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Banner4.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/curtains">
                    <img class="image-slideshow ml-5" src="{{asset('/storage/banner/Banner5.jpg')}}" alt="">
                </a>
            </div>
        </div>
    </section> -->


    <!-- phone -->
    <section id="slideshow" class="hidden-md" style="margin-top:75px;">
        <div class="slick ">

            <div>
                <a href="/shop/category/new-launching">
                    <img class="image-slideshow" src="{{asset('/storage/banner/1018-bannerm.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/packages">
                    <img class="image-slideshow" src="{{asset('/storage/banner/packagebanner_m.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/mattresses">
                    <img class="image-slideshow" src="{{asset('/storage/banner/R1-Banner_M.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/mattresses">
                    <img class="image-slideshow" src="{{asset('/storage/banner/W1-Banner_M.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/doors">
                    <img class="image-slideshow" src="{{asset('/storage/banner/Door-Banner_M.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/lightings">
                    <img class="image-slideshow" src="{{asset('/storage/banner/Light-Banner_M.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/bedsheets-and-mattresses/bedsheets">
                    <img class="image-slideshow" src="{{asset('/storage/banner/Bedsheet-P-Banner_M.jpg')}}" alt="">
                </a>
            </div>
            <div>
                <a href="/shop/category/bedsheets-and-mattresses/bedsheets">
                    <img class="image-slideshow" src="{{asset('/storage/banner/Bedsheet-M-Banner_M.jpg')}}" alt="">
                </a>
            </div>
    </section>

    <section>
        <div class="container">
            <div class="row mx-auto">
                <div class="col-md-12">
                    <h1 class="m-0 mb-3 myholizontalline myh1" style="text-align: left;"><b>Hot Selections</b></h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="mycol text-alignment my-pw mx-md-1 mx-2">
                        <a style="color:#e0b555; " href="/shop/category/bedsheets-and-mattresses/bedsheets"><img class="icon-image" src="{{asset('/storage/icons/bedsheet-icon.png')}}" alt="Icon">
                            <h5 class="text-center" style="font-size:15px;"><b>BEDSHEETS</b></h5>
                        </a>

                    </div>
                    
                </div>
            </div>
        </div>

    </section>
</div>

<br>



@endsection

@push('style')
<style>
    .icon-image {
        width: 100px;
    }

    .text-alignment {
        text-align: center;
    }



    .promo-page-background-color {
        background-color: black;
    }

    @media(max-width:767px) {

        .image-slideshow {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 100%;
            width: 100%;
        }

        .icon-image {
            width: 100px;
            height: 100px;
        }

        .margin-left-text {
            /* margin-left: 10px; */
        }
    }

    @media (max-width: 767px) {
        .hidden-sm {
            display: none;
        }

        .w-100-sm {
            width: 100%;
        }
    }

    @media (min-width: 768px) {
        .hidden-md {
            display: none;
        }

        .w-50-md {
            width: 50%;
        }

        .margin-bottom-md {
            margin-bottom: 20px;
        }

        .image-slideshow {
            width: 93%;
            height: auto;
        }
    }


    @media(max-width:325px) {
        .icon-image {
            width: 90px;
            height: 90px;
        }

        .margin-left-text {
            margin-left: 10px;
        }
    }

    .slick-dots {
        position: absolute;
        display: block;
        width: 100%;
        padding: 10;
        margin: 0;
        list-style: none;
        text-align: center;
    }

    .slick-dots li button:before {
        color: white;
    }

    .slick-dots li.slick-active button:before {
        font-size: 10px;
        color: #ffcc00;
    }
</style>
@endpush



@push('script')
<script>
    $(document).ready(() => {
        $('#slideshow .slick').slick({
            autoplay: true,
            fade: false,
            autoplaySpeed: 2000,
            speed: 1000,
            dots: true,
            prevArrow: false,
            nextArrow: false
        });
    });

    // document.querySelector("#myhomepage").style.display = "block";
    // document.querySelector(
    document.querySelector("#app").classList.remove('app-body');
</script>
@endpush
