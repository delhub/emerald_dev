@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Product multineed</a></li>
            </ul>
        </div>
    </div>

    <section>
        <div class="fml-container">
            <div class="row">
                <div class="col-12">
                    <div class="multineed-header my-mb-3">
                        <h1>
                            Hot
                            <span>
                                Selections
                                <div class="info-hrline-red-multi"></div>
                            </span>
                        </h1>
                    </div>
                </div>
            </div>

            <div class="row multineed-box">
                <div class="col-md-12 col-sm-12 multi-info">
                    <div class="r-p-list">
                        <div class="row">
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 item414">
                                <div class="product-item-list">
                                    <a href="{{ route('shop.product_pageitem') }}">
                                        <img src="{{ asset('/images/formula/1.jpg') }}" alt="">
                                        <h5>Blackmores Dietary Supplement</h5>
                                        <p>MYR 100.00</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="multi-1">
                    <ul>                            
                        <li>
                            <a href="#" data-bs-original-title="" title="">
                                <img src="{{asset('/images/icons/bone-care-icon.png')}}" alt="">
                              <h4>Bone and Joint Care</h4> 
                            </a>                          
                        </li>
                        <li>
                            <a href="#" data-bs-original-title="" title="">
                                <img src="{{asset('/images/icons/bone-care-icon.png')}}" alt="">
                              <h4>Bone and Joint Care</h4> 
                            </a>                          
                        </li>
                        <li>
                            <a href="#" data-bs-original-title="" title="">
                                <img src="{{asset('/images/icons/bone-care-icon.png')}}" alt="">
                              <h4>Bone and Joint Care</h4> 
                            </a>                          
                        </li>  
                        <li>
                            <a href="#" data-bs-original-title="" title="">
                                <img src="{{asset('/images/icons/bone-care-icon.png')}}" alt="">
                              <h4>Bone and Joint Care</h4> 
                            </a>                          
                        </li>    
                    </ul>  
                </div> --}}

                </div>
            </div>
        </div>

        <div class="full-hrline-grey hr-margin2"></div>
        <div class="fml-container">
            <div class="row">
                <div class="outer my-mt-5" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="testimonials-info" role="tabpanel"
                        aria-labelledby="pills-contact-tab">
                        <div class="col-md-12">
                            <div class="multineed-header my-mb-2">
                                <h1>Recommendation
                                    <span>Products
                                        <div class="info-hrline-red-multi"></div>
                                    </span>
                                </h1>
                            </div>

                            <nav>
                                <div class="multi-inner nav nav-tabs rp-opt-list" id="nav-tab" role="tablist">
                                    <button class="mp-icon1 active" id="nav-home-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
                                        aria-selected="true">
                                        <img src="{{ asset('/images/icons/icon-adult.png') }}" alt="">
                                        <p>Adult</p>
                                        <div class="downarrow"><img
                                                src="{{ asset('/images/icons/multidownarrow.svg') }}" alt=""></div>
                                    </button>

                                    <button class="mp-icon1" id="nav-profile-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-profile" type="button" role="tab"
                                        aria-controls="nav-profile" aria-selected="false">
                                        <img src="{{ asset('/images/icons/icon-elderly.png') }}" alt="">
                                        <p>Elderly</p>
                                        <div class="downarrow"><img
                                                src="{{ asset('/images/icons/multidownarrow.svg') }}" alt=""></div>
                                    </button>

                                    <button class="mp-icon1" id="nav-contact-tab" data-bs-toggle="tab"
                                        data-bs-target="#nav-contact" type="button" role="tab"
                                        aria-controls="nav-contact" aria-selected="false">
                                        <img src="{{ asset('/images/icons/icon-kids.png') }}" alt="">
                                        <p>Kids</p>
                                        <div class="downarrow"><img
                                                src="{{ asset('/images/icons/multidownarrow.svg') }}" alt=""></div>
                                    </button>
                                </div>
                            </nav>

                            <div class="row">
                                <div class="col-12">
                                    <div class="multi inner tab-content height-limit-scroll" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                            aria-labelledby="nav-home-tab">
                                            <div class="row">
                                                <div class="col-12 mult-info">
                                                    1
                                                    <div class="r-p-list">
                                                        <div class="row">
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                            aria-labelledby="nav-profile-tab">
                                            <div class="row">
                                                <div class="col-12 mult-info">
                                                    2
                                                    <div class="r-p-list">
                                                        <div class="row">
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                            aria-labelledby="nav-contact-tab">
                                            <div class="row">
                                                <div class="col-12 mult-info">
                                                    3
                                                    <div class="r-p-list">
                                                        <div class="row">
                                                            <div class="col-md-3 item414">
                                                                <div class="product-item-list">
                                                                    <a href="{{ route('shop.product_pageitem') }}">
                                                                        <img src="{{ asset('/images/formula/1.jpg') }}"
                                                                            alt="">
                                                                        <h5>Blackmores Dietary Supplement</h5>
                                                                        <p>MYR 100.00</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection


@push('script')
<script>
    $(document).ready(function() {


    });
</script>
@endpush
