@inject('controller', 'App\Http\Controllers\Warehouse\StockTransfer\StockTransferController')

@extends('layouts.administrator.main')

@section('content')

<h1>Transfer Box list</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <form action="{{ route('warehouse.StockTransfer.box-list') }}" method="GET">
            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="col-auto ml-auto">
                    <select class="custom-select" id="location" name="location">

                        <option value="0" selected>All Location...</option>
                        @foreach ($locations as $location)
                        <option value="{{$location->id}}" {{ $request->location == $location->id ? 'selected' : '' }}>{{$location->location_name}}</option>

                        @endforeach
                    </select>
                </div>
            </div>
            <br>
            <br>

            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="col-auto ml-auto">
                    <select class="custom-select" id="selectSearch" name="selectSearch">
                        <option value="ALL" selected>Choose...</option>
                        <option value="name" {{ $request->selectSearch == 'name' ? 'selected' : '' }}>Box Name</option>
                    </select>

                    <input type="text" class="form-control" name="searchBox"
                    value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                </div>
                <div style="text-align:center;">
                    <div class="col-md-auto px-1">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                        <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('warehouse.StockTransfer.box-list') }} style="color:black;">Reset Filter</a></button>
                    </div>
                </div>
            </div>
        </form>

        {{$tables->links()}}
        <div class="table-responsive m-2">
            @if(count($tables) > 0)

                <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Location</th>
                            <th>Box Name</th>
                            <th>Use By Transfer ID</th>
                            <th>User Created</th>
                            <th>User Received</th>
                            <th>Notes</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($tables as $row)
                        <tr>

                            <td style="width:2%">{{$row->id}}</td>
                            <td style="width:2%">{{$row->location->location_name}}</td>
                            <td style="width:2%">{{$row->box_name}}</td>
                            <td style="width:2%">{{$row->using}}</td>
                            <td style="width:2%">{{$row->theUser1->userInfo->full_name}}</td>
                            <td style="width:2%">{{ $row->theUser2->userInfo->full_name ?? ''}}</td>
                            {{--<td style="width:2%">{{($row->theUserReceive) ? $row->theUserReceive->userInfo->full_name : '-'}}</td>
                             <td style="width:1%">{{$row->fromLocation->location_key}} - <b>{{$row->fromLocation->location_name}}</b><br>({{$row->fromLocation->default_address}})</td>
                            <td style="width:1%">{{$row->toLocation->location_key}} - <b>{{$row->toLocation->location_name}}</b><br>({{$row->toLocation->default_address}})</td>
                            <td style="width:2%">{{$row->session_start_time}}</td>
                            <td style="width:2%">{{$row->session_end_time}}</td>
                            <td style="width:2%"> {{showWarehouseTransferType($row->type)}}</td>
                            <td style="width:2%"> {{showWarehouseTransferstatus($row->status)}}</td>

                            <td style="width:60%">
                                @php
                                $tranfer = array();
                                    foreach ($row->stockTransferItems as $item){
                                        if(!isset($tranfer[$item->batchItem->batchDetail->product_code])) {
                                            $tranfer[$item->batchItem->batchDetail->product_code]['qty'] =0;
                                            $tranfer[$item->batchItem->batchDetail->product_code]['productName'] = $item->batchItem->batchDetail->productAttribute->product2->parentProduct->name;

                                        }
                                        $tranfer[$item->batchItem->batchDetail->product_code]['qty'] ++;
                                    }

                                    foreach ($tranfer as $productCode => $item) {
                                        echo '
                                            Transferred : <b>'.$item['qty'].'</b> - <b>'.$productCode. '</b> ('.$item['productName'].')
                                            <br>
                                        ';
                                    }
                                    unset($tranfer)
                                @endphp
                            </td>

                            <td style="width:2%">
                                {{$row->transfer_datetime}}
                            </td>--}}

                            <td style="width:80%">
                                <textarea type="text" name="notes" id="notes{{$row->id}}" class="form-control" value="{{$row->notes}}" >{{$row->notes}}</textarea>
                                <div class="ml-auto col-2 mr-4 mt-2">
                                    <button type="button" class="btn btn-danger"
                                        onclick="editNotes({{ $row->id }});">
                                        Edit
                                        <div class="spinner-border spinner-border-sm text-dark variationLoader{{ $row->id }}" role="status" style="display:none;">
                                            <span class="sr-only">Loading...</span>
                                        </div>

                                    </button>
                                    <div class="variationRefreshPrice{{ $row->id }}" style="display:none;">Edited </div>
                                </div>
                            </td>

                            <td>
                                {{-- @if ($row->using == 0) --}}
                                    <form action="{{ route('warehouse.StockTransfer.print-label')}}" method="GET">
                                        <input type="hidden" name="id" value="{{$row->id}}">
                                        <input type="hidden" name="method" value="reprint">
                                        <button class="btn-gradient" type="submit" formtarget="_blank">Print</button>
                                    </form>
                                {{-- @endif --}}
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Data Found</p>
            @endif
        </div>
        {{$tables->links()}}
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable({
            "order": [[ 0, "desc" ]],
            "paging":   false,
            "info":     false
        });
    });

function editNotes(batchID) {
    const notes =$("#notes"+batchID).val();

    if (confirm('Are you sure want to edit this notes? Any unsaved changes will be lost')) {
        if (notes=='' ) {
            $(".variationThisEmpty"+batchID).show();
        } else {
            $.ajax({
                // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
                url:"{{ route('warehouse.StockTransfer.stockTransferList.edit-box-notes', ['batchID' => "+batchID+"]) }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "batchID": batchID,
                    "notes": notes,
                },
                beforeSend: function () {
                    $(".variationLoader"+batchID).show();
                },
                success: function (success) {
                    $(".variationLoader"+batchID).hide();
                        $(".variationRefreshPrice"+batchID).show();
                },
                error: function () {
                    alert("error in loading");
                }
            });
        }
    }
}
</script>
@endpush
