<?php

namespace App\Models\Dealers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Users\Dealers\DealerInfo;
class DealerGroup extends Model
{
    protected $table = 'dealer_groups';


    static function getManagerGroupID(){
        $dealer = DealerInfo::find(Auth::user()->id);
        $account_id  =  $dealer->account_id;

        $group = self::where('manager_id', $account_id )->firstOrFail();

        return $group->id;
    }
}


