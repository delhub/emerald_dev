{{-- @inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController') --}}

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')
<h1>Scan Product Now ({{$process->type}})</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="col-8">

            </div>
        </div>
        <div class="table-responsive m-2">

            @if($process)
                <p> <b>Location</b> : {{ $process->location->location_name}}</p>
                <p> <b>Type</b> : {{ $process->type}}</p>
                <p>
                    <b>Product code (To)</b> : {{ $process->product_code}} - {{$process->productAttribute->product2->parentProduct->name}} ({{$process->productAttribute->attribute_name}})
                </p>

                <p> <b>Quantity</b> : {{ $process->quantity}}</p>
                <p> <b>Remark</b> : {{ $process->remark}}</p>

                {{-- <div class="col-8">
                    <textarea type="text" name="notes" id="notes{{$stockTransferCheck->id}}" class="form-control" value="{{$stockTransferCheck->notes}}">{{$stockTransferCheck->notes}}</textarea>
                    <div class="ml-auto col-2 mr-4 mt-2">
                        <button type="button" class="btn btn-danger"
                            onclick="editNotes({{ $stockTransferCheck->id }});">
                            Edit
                            <div class="spinner-border spinner-border-sm text-dark variationLoader{{ $stockTransferCheck->id }}" role="status" style="display:none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </button>
                    </div>
                    <p class="variationThisEmpty{{ $stockTransferCheck->id }}" style="color:red;display: none;">Please enter something to add!</p>
                    <p class="variationRefreshPrice{{ $stockTransferCheck->id }}" style="color:red;display: none;">Refresh to edit this notes</p>
                </div> --}}

                <br>
                <hr>
                <br>
                <h3>Product Scanned (From) - ({{$process->from_product_code}})</h3>

                <table id="table-id"  class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <th>Batch Item Id</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Product Attribute</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $array =array();
                        @endphp
                        @if ($process->bUnbundleItems)
                            @foreach($process->bUnbundleItems as $row)
                            @php
                                if (!isset($array[$row->batchItem->batchDetail->product_code])) {
                                    $array[$row->batchItem->batchDetail->product_code]['qty'] = 0;
                                }
                                $array[$row->batchItem->batchDetail->product_code]['qty'] ++;
                            @endphp
                            <tr>
                                <td>{{$row->batchItem->items_id}}</td>
                                <td>{{$row->batchItem->batchDetail->product_code}}</td>
                                <td>{{$row->batchItem->batchDetail->productAttribute->product2->parentProduct->name}}</td>
                                <td>{{$row->batchItem->batchDetail->productAttribute->attribute_name}}</td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>

                <hr>

                <h3>Summary</h3>
                <table class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Total Quantity</th>
                        </tr>

                @if ($array)
                    @foreach ($array as $productCode => $total)
                        <tbody>
                            <td> {{$productCode}} </td>
                            <td> {{$total['qty']}}</td>
                        </tbody>
                    @endforeach
                @endif

                </table>

                <div class="row">
                    <div class="col-12 text-center">
                        <form action="{{route('warehouse.b_unbundle.end', ['stockTransferId'=>$process->id])}}" id="create-form-id" method="GET" enctype="multipart/form-data">
                            @csrf
                        <button type="button" class="btn btn-success"
                        onclick="clickSubmit();"> Complete {{$process->type}}</button>
                        </form>
                    </div>
                </div>
            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Found</p>
            @endif

            <div class="my-input-warehouse input-group input-group-lg mb-3">
                <input type="text" ref="scancode" v-model="scancode" name="scancode" class="bg-form2 form-control" id="my-input-warehouse"
                    placeholder="Use Scanner Here" autofocus autocomplete="off" @@change="qrProduct(scancode)">
                <div class="pop-btn">
                    <button type="button" class="qrbtn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit() {
    if (confirm('Are you sure complete {{$process->type}}?')) {
        document.getElementById("create-form-id").submit();
    }
}

// function editNotes(batchID) {
//     const notes =$("#notes"+batchID).val();

//     if (confirm('Are you sure want to edit this notes? Any unsaved changes will be lost')) {
//         if (notes=='' ) {
//             $(".variationThisEmpty"+batchID).show();
//         } else {
//             $.ajax({
//                 // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
//                 url:"{{ route('warehouse.StockTransfer.stockTransferList.edit-notes', ['batchID' => "+batchID+"]) }}",
//                 type: 'POST',
//                 data: {
//                     "_token": "{{ csrf_token() }}",
//                     "batchID": batchID,
//                     "notes": notes,
//                 },
//                 beforeSend: function () {
//                     $(".variationLoader"+batchID).show();
//                 },
//                 success: function (success) {
//                     $(".variationLoader"+batchID).hide();
//                         $(".variationRefreshPrice"+batchID).show();
//                 },
//                 error: function () {
//                     alert("error in loading");
//                 }
//             });
//         }
//     }
// }
</script>
@endpush
