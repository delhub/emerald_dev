@extends('layouts.administrator.main')

@section('content')

<div class="container->fluid">
    <div class="row">
        <div class="col-12 my-3">
            <h3>All Agent</h3>
        </div>
    </div>
    <div class="row m-2 ">
        <div class="col-md-6 ml-auto">
            <form action="{{ route('administrator.vqs.index') }}" method="GET">
                <div class="row">
                    <div class="input-group col-md-5 p-0 ml-auto mx-4">
                        <div class="input-group-append col-md-8 p-0">
                            <input type="text" class="form-control text-left" id="search" name="search"
                                value="{{ ($request->search)?$request->search:'' }}">
                        </div>
                        <div class="input-group-append col-md-2 p-0">
                            <button type="submit" class="btn btn-outline-warning"
                                style="color:black;">Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-auto ml-md-auto">
            {{ $archimedesDatas->links() }}

        </div>
    </div>
</div>

<div class="container-fluid table-responsive m-2">
    <table id="summaryTables"  class="table table-striped table-bordered" style="min-width: 1366px;">
        <thead>
            <tr>
                <td>Agent ID</td>
                <td>Agent Name</td>
                <td>Deposits</td>
                @foreach ($vqsProducts as $vqsProduct)
                    <td>{!! wordwrap($vqsProduct->name,25,"<br>") !!}</td>
                @endforeach
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($archimedesDatas as $key => $archimedesData)

            @php
            $eachProductCodes = $archimedesData['stock_ledger']->groupBy('product_code');
            @endphp


            <tr>
                <td>
                    {{ $archimedesData['acd_main']->dealer_id }}
                </td>
                <td>
                    {{ $dealerName->where('account_id',$archimedesData['acd_main']->dealer_id)->first()->full_name }}
                </td>
                <td>
                    -
                    {{-- RM {{ number_format(($archimedesData['acd_main']->value / 100),2) }} --}}

                </td>
                @foreach ($vqsProducts as $vqsProduct)
                <td>
                    @if (!$archimedesData['acd_product']->where('name',$vqsProduct->product_code)->isEmpty())
                    @php $sum = 0 @endphp
                    @foreach ($eachProductCodes as $eachProductCode)
                    @php $sum = $eachProductCode->where('type','!=',5001)->sum('quantity') @endphp
                    @endforeach
                        {{ $sum }}
                     /
                    {{ $archimedesData['acd_product']->where('name',$vqsProduct->product_code)->first()->value }}

                    @else

                    {{ '-' }}

                    @endif

                </td>
                @endforeach


                <td>
                    <div class="row">
                        <div class="col-auto my-1 mx-auto">
                            <a href="{{ route('administrator.vqs.details',[$archimedesData['acd_main']->dealer_id]) }}"
                                target="_blank">
                                <button class="btn my-auto btn-small-screen font-weight-bold bjsh-btn-gradient"
                                    type="button">Details</button>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-auto my-1 mx-auto">
                            <button id="topupButton" onclick="passData({{ $archimedesData['acd_main']->dealer_id }})"
                                class="btn my-auto btn-small-screen font-weight-bold bjsh-btn-gradient" type="button"
                                data-toggle="modal" data-target="#topupModal"
                                data-dealer="{{ $archimedesData['acd_main']->dealer_id }}"
                                data-product="{{ $archimedesData['acd_product'] }}" data-backdrop="static"
                                data-keyboard="false">Topup</button>

                        </div>
                    </div>
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="topupModal" tabindex="-1" role="dialog" aria-labelledby="topupModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title">Agent ID = <span id="dealerID"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container p-4">
                    <form action="{{ route('administrator.vqs.topup') }}" id="topup-acd" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="dealer_id" id="dealer_id" value="">
                        <p id="acdProduct"></p>
                        <table class="table text-center table-striped">
                            <thead>
                              <tr>
                                <th class="text-center" colspan="2">Positive for refill &nbsp;&nbsp;/&nbsp;&nbsp; Negative for refund</th>
                            </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>tss-king</td>
                                <td>
                                    <button type="button" id="sub-tssking" class="sub">-</button>
                                    <input type="text" name="topup[tss-king]" value="0" id="1" class="field" />
                                    <button type="button" id="add-tssking" class="add">+</button>
                                </td>
                              </tr>
                              <tr>
                                <td>tss-queen</td>
                                <td>
                                    <button type="button" id="sub-tssqueen" class="sub">-</button>
                                    <input type="text" name="topup[tss-queen]" value="0" id="1" class="field" />
                                    <button type="button" id="add-tssqueen" class="add">+</button>
                                </td>
                              </tr>
                              <tr>
                                <td>silkvenus</td>
                                <td>
                                    <button type="button" id="sub-silkvenus" class="sub">-</button>
                                    <input type="text" name="topup[silkvenus]" value="0" id="1" class="field" />
                                    <button type="button" id="add-silkvenus" class="add">+</button>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                    <button type="button" class="btn my-auto btn-small-screen font-weight-bold btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn my-auto btn-small-screen font-weight-bold bjsh-btn-gradient" value="submit">Save changes</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>


    @endsection

    @push('style')
    <style>
        button {
            margin: 4px;
            cursor: pointer;
        }

        input {
            text-align: center;
            width: 40px;
            margin: 4px;
            color: black;
        }
    </style>
    @endpush

    @push('script')
    <script>

        // pass agent id to popup
        function passData(id){
            $("#dealerID").html(id);
            document.getElementById("dealer_id").value = id;
            console.log(id);
        }

        // refill or refund
        $('.add').click(function () {
            $(this).prev().val(+$(this).prev().val() + 1);
        });
        $('.sub').click(function () {
            $(this).next().val(+$(this).next().val() - 1);
        });

    </script>

    @endpush

