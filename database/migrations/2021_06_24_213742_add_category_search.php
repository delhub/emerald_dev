<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddCategorySearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('categories')->insert(
            array(
                array('name' => 'Malaysia', 'slug' => 'my', 'type' => 'exclude_country', 'parent_category_id' => '0', 'featured' => '0', 'position' => '9999', 'active' => '0'),
                array('name' => 'Singapore', 'slug' => 'sg', 'type' => 'exclude_country', 'parent_category_id' => '0', 'featured' => '0', 'position' => '9999', 'active' => '0')
            )
        );

        Schema::create('categories_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id');
            $table->string('type');
            $table->string('value');
            $table->string('country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}