<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    {{-- <meta charset="UTF-8"> --}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formula - Delivery Order {{ $order->delivery_order }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@200;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/delivery-order.css') }}">
     @include('layouts.favicon')
</head>
<body >
    @php
        $url = URL::signedRoute(
    'guest.order-received',
    ['orderNum' => $order->delivery_order]
    );
    @endphp
    <div class="a4">
        <table style="width: 100%;height:100vh !important;vertical-align: top !important;">
            <tr>
                <td style="width: 100%; background: white; height:960px !important;vertical-align: top !important;">
                        <div class="header-invoice" style="position: relative">
                            <table width="100%">
                                <tr >
                                    <td width="25%">
                                        <img class="company-width" src="{{asset('images/formula/post-img/logo.png')}}" style="width: 90%;margin: auto;">
                                    </td>
                                    <td class="company-name" width="55%">
                                        <p style="font-size: 15px;font-weight:600;">Formula Healthcare Sdn. Bhd.<br><span>201901009101 (1318429-V)</span></p>
                                        <p>1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                        <p >No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                        <table>
                                            <tr class="company-details">
                                                <td>
                                                    <div class="contact-info">
                                                        <p class="mt-contact">
                                                        <img class="ci-width" src="{{asset('images/formula/post-img/do-c-icon1.png')}}">
                                                        formula2u-cs@delhubdigital.com</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="contact-info">
                                                        <p class="mt-contact">
                                                        <img class="ci-width" src="{{asset('images/formula/post-img/do-c-icon3.png')}}">
                                                        formula2u.com</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                    <td width="20%">
                                        <div class="do-qrcode2">
                                            <img style="position: absolute;width: 120px;top: 0px;right:0;" src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($url)) }} ">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="invoice-body">
                            <div class="invoice-dt">
                                <h1 style="padding: 0;margin: 0;margin-bottom: 10px;">DELIVERY ORDER</h1>
                            </div>
                        </div>

                        <div class="-do-dd">
                            <div class="invoice-dt-box">
                                <table width="100%">
                                    <tr>
                                        <td width="45%">
                                            <table width="100%">
                                                <tr>
                                                    <td class="invoice-name invoice-d-tb" width="32%">
                                                    @if ($order->delivery_method == 'Self-pickup' && !empty($order->delivery_outlet))
                                                        COLLECT BY
                                                    @else
                                                        DELIVER TO
                                                    @endif
                                                    </td>
                                                    <td class="w-1">
                                                        :
                                                    </td>
                                                    <td class="invoice-name" width="66%">
                                                        {{($order->purchase->ship_full_name) ? $order->purchase->ship_full_name : ''}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="invoice-name invoice-d-tb" width="40%">
                                                        CONTACT NO.
                                                    </td>
                                                    <td class="w-1">
                                                        :
                                                    </td>
                                                    <td class="invoice-name" width="66%">
                                                        {{ ($order->purchase->ship_contact_num) ? $order->purchase->ship_contact_num : '' }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    @php
                                                        $lines = array();
                                                        $lines[] = ucwords(rtrim($order->purchase->ship_address_1,",").',');
                                                        if (str_replace(',', '', $order->purchase->ship_address_2)) $lines[] = ucwords(rtrim($order->purchase->ship_address_2,",").',');
                                                        if (str_replace(',', '', $order->purchase->ship_address_3)) $lines[] = ucwords(rtrim($order->purchase->ship_address_3,",").',');

                                                        if ($order->purchase->ship_postcode) $postcode =  $order->purchase->ship_postcode;
                                                        if ($order->purchase->cityKey != NULL) {
                                                            $city = $order->purchase->cityKey->city_name;
                                                        }else {
                                                            $city = $order->purchase->ship_city ;
                                                        }
                                                        if ($order->purchase->cityKey != NULL ||$order->purchase->cityKey) $lines[]=  $postcode . ' ' . $city . ',';
                                                        if ($order->purchase->state->name) $lines[] = $order->purchase->state->name . ',';

                                                        $address = $lines;
                                                    @endphp

                                                    {{-- <td> --}}
                                                        <td class="invoice-add2" colspan="3">
                                                            @foreach ($lines as $line)
                                                                <p>{{$line}}<p>
                                                            @endforeach
                                                            <p>Malaysia.</p>
                                                        </td>
                                                    {{-- </td> --}}
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="55%">
                                            <div class="do-name2">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="invoice-name invoice-d-tb" width="50%">DELIVERY ORDER NO.</td>
                                                        <td class="w-1">:</td>
                                                        <td class="invoice-name">{{$order->delivery_order}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="invoice-name invoice-d-tb" width="42%">ORDER NO. </td>
                                                        <td class="w-1">:</td>
                                                        <td class="invoice-name">{{$order->purchase->purchase_number}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="invoice-name invoice-d-tb" width="42%">Purchase Date </td>
                                                        <td class="w-1">:</td>
                                                        <td class="invoice-name">{{$order->purchase->purchase_date}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="invoice-name invoice-d-tb" width="42%">Delivery Method </td>
                                                        <td class="w-1">:</td>
                                                        <td class="invoice-name">
                                                            @if ($order->delivery_method == 'Self-pickup' && !empty($order->delivery_outlet))
                                                                {{$order->delivery_method}}
                                                            @else
                                                                Delivery By Company
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if ($order->delivery_method == 'Self-pickup' && !empty($order->delivery_outlet))
                                                    <tr>
                                                        <td class="invoice-name invoice-d-tb" width="42%">Store Location </td>
                                                        <td class="w-1">:</td>
                                                        <td class="invoice-name">{{$order->outlet->outlet_name}}</td>
                                                    </tr>
                                                    @endif
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="invoice-description">
                            <table class="table-width">
                                <thead class="dark-clr2">
                                    <tr>
                                        <th width="10%" style="font-size: 15px;">NO.</th>
                                        <th width="70%" style="font-size: 15px;">Description</th>
                                        <th width="20%" style="font-size: 15px;">QTY</th>
                                    </tr>
                                </thead>
                                <tbody style="margin:0">

                                    @php
                                    $no = 0;
                                    $total = 0;

                                    foreach ($order->items->where('item_order_status','!=',1011) as $item){
                                        if ($item->attributes->product2->parentProduct->no_stockProduct == 0) {
                                            $no ++;
                                            $size = NULL;

                                            if(array_key_exists('product_size', $item->product_information)){
                                                $size ='<br>(Size:'.$item->product_information['product_size'].')';
                                            }
                                            if(array_key_exists('product_rbs_frequency', $item->product_information)){
                                                $size ='<br><span style="color:red;">*</span> Deliver every '. $item->product_information['product_rbs_frequency'] .' month(s) for '.$item->product_information['product_rbs_times'] .' times.';
                                            }
                                            if(array_key_exists('product_rbs_id', $item->product_information)){
                                                $size ='<br>('.$item->product_information['product_rbs'].')';
                                                // $size ='<br><span style="color:red;">*</span> Deliver every '. $item->product_information['product_rbs_frequency'] .' month(s) for '.$item->product_information['product_rbs_times'] .' times.';
                                            }

                                            echo'
                                                <tr>
                                                    <td class="i-fw" style="font-size: 15px;">'.$no.'</td>
                                                    <td class="table-line2">
                                                        <div class="table-p-bold2">
                                                            <h4 style="font-size: 15px;">'.$item->product_code.'
                                                                ';
                                                                if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {
                                                                        echo 'x'.($item->quantity).'';
                                                                }
                                                        echo'
                                                            </h4>
                                                        </div>
                                                        <p style="font-size: 15px;">'.$item->product->parentProduct->name.$size.'</p>
                                                        ';

                                                        if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {
                                                            foreach ($item->attributes->productBundle as $key => $bundle) {
                                                                echo '
                                                                    <br>
                                                                    <div class="table-p-bold2">
                                                                        <h4 style="font-size: 15px;">'.$bundle->primary_product_code.'</h4>
                                                                        <p style="font-size: 15px;">'.$bundle->productAttribute->product2->parentProduct->name.'<br>('.$bundle->productAttribute->attribute_name.')</p>
                                                                    </div>
                                                                ';
                                                            }
                                                        }

                                                        if (!empty($item->subtotal_point)) {
                                                            echo'
                                                                <div class="i-promo-tag">
                                                                    <p style="font-size: 15px;">Point applied: '.$item->subtotal_point.'</p>
                                                                </div>
                                                ';
                                                            }
                                                echo'

                                                        </td>
                                                        <td class="i-fw" style="font-size: 15px;">
                                                            ';

                                                            if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {
                                                                foreach ($item->attributes->productBundle as $key => $bundle) {
                                                                    echo '
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <br>
                                                                        <p class="i-fw" style="font-size: 15px;text-align: center;"> '.($bundle->primary_quantity*$item->quantity).'</p>
                                                                    ';
                                                                }
                                                            }else{
                                                                echo ''.$item->quantity.'';
                                                            }

                                                    echo'
                                                        </td>
                                                    </tr>
                                                ';

                                            if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {
                                                foreach ($item->attributes->productBundle as $key => $bundle) {
                                                    $total += ($bundle->primary_quantity*$item->quantity);
                                                }
                                            }else{
                                                $total += $item->quantity;
                                            }
                                        }
                                    }
                                    @endphp
                                </tbody>
                            </table>

                            <table style="margin-top:0;width: 100%;margin-bottom: 15px;">
                                <tbody>
                                    <tr class="table-line-top">
                                        <td class="i-fw" width="60%">
                                            <p style="font-size: 15px;font-weight:400;">
                                                The issuance of this Delivery Order shall be deemed as your acceptance of the terms and
                                                conditions
                                                of this Disclaimer.
                                            </p>
                                        </td>
                                        <td class="i-fw" width="20%" style="font-size: 15px;border-bottom: 1px solid #00269A;border-top: 1px solid #00269A;">
                                            Total
                                        </td>
                                        <td class="i-total" width="20%" style="border-bottom: 1px solid #00269A;font-size: 15px;border-top: 1px solid #00269A;">
                                            {{$total}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="invoice-footer">
                            <div style="background: #dedede;padding: 1px 10px;border-radius: 10px;font-weight: 400;font-family: 'Noto Serif SC', serif;font-size: 13px;">
                                <p>尊敬的客户，<br>
                                    感谢您在Formula2u.com购买心儀之品。收到货物，確認無誤後，请扫描送货订单右上角的QR码，并给于评分或评语。
                                    您寶貴的回饋，深具价值与意义，將讓我們團隊精益求精，力求為客至善。谢谢。<br>
                                    Dear Customer,<br>
                                    Thank you for purchasing at Formula2u.com. Please scan the QR code in the upper right corner of the
                                    delivery order that accompanies the delivery. Your ratings and comments are very valuable to us.
                                </p>
                            </div>
                        </div>
                        <div class="invoice-footer">
                            <div class="footer-dp2" style="font-size: 13px;">
                                <p>This is computer generated delivery order, no signature is required.</p>
                            </div>
                        </div>
                </td>
            </tr>
        </table>
        @if ($order->delivery_info != NULL && $order->delivery_method != 'Self-pickup')
        <table>
            <tr style="height:230mm;width:100%;vertical-align: top;">
                <td>
                    <p style="font-weight:bold;">DELIVERY ORDER NO. : <span style="font-weight:normal;">{{$order->delivery_order}}</span></p>
                </td>
            </tr>
        </table>
        @endif
    </div>
</div>
</body>
</html>
