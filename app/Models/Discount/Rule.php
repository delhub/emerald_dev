<?php

namespace App\Models\Discount;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    // Table Name
    protected $table = 'discount_rule';
    protected $fillable = [
        'id'
    ];
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestemps = true;
}