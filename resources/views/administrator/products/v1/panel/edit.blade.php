@inject('controller', 'App\Http\Controllers\Administrator\v1\Product\ProductByPanelController')

@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <a href="/administrator/products/panels" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back</a>
    </div>
</div>
<div class="card shadow-sm mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <h4>
                    Product Information
                </h4>

            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-md-2">
                        <img src="@if ($parentProduct->defaultImage) {{ asset('storage/' . $parentProduct->defaultImage->path . $parentProduct->defaultImage->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif"
                            class="mw-100" alt="">
                    </div>
                    <div class="col-12 col-md-10">
                        <div class="row">
                            <div class="col-12 col-md-4 form-group">
                                <label for="productName">Product Name</label>
                                <input type="text" id="productName" class="form-control" disabled
                                    value="{{ $parentProduct->name }}" style="background-color: #ffffff;">
                            </div>
                            <div class="col-12 col-md-4 form-group">
                                <label for="productQuality">Product Quality</label>
                                <input type="text" id="productQuality" class="form-control" disabled
                                    value="{{ $parentProduct->quality->name }}" style="background-color: #ffffff;">
                            </div>
                            <div class="col-12 col-md-4 form-group">
                                <label for="productCode">Product Code</label>
                                <input type="text" id="productCode" class="form-control" disabled
                                    value="{{ $parentProduct->product_code }}" style="background-color: #ffffff;">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <!-- Start Form -->
                    <div class="col-12 col-md-10 offset-md-1">
                        <form name="attform" action="{{ route('administrator.v1.products.panels.update', ['id' => $product->id]) }}"
                            method="POST" id="edit-panel-product-form">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="mb-3">Product By Panel ..</h5>
                                </div>

                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Panel Account ID <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <select name="panel_id" id="panel_id" class="select2 form-control">
                                        <option value="default">Please choose a panel.</option>
                                        @foreach($panels as $panel)
                                        <option value="{{ $panel->account_id }}"
                                            {{ ($product->panel_account_id == $panel->account_id) ? 'selected' : '' }}>
                                            {{ $panel->account_id }} - {{ $panel-> company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Panel name for display <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <select name="display_panel_name" id="display_panel_name"
                                        class="select2 form-control">
                                        <option value="default">Please choose a company name for display.</option>

                                        @foreach($panels as $panel)
                                        <option value="{{ $panel->company_name}}"
                                            {{ ($product->display_panel_name == $panel->company_name) ? 'selected' : '' }}>
                                            {{ $panel->account_id }} -
                                            {{ $panel->company_name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            @if ($user->hasRole('administrator') || $user->hasRole('management'))

                                <div class="row">
                                    <div class="col-12 col-md-4 text-md-right my-auto">
                                        <p>
                                            Edit Price<small class="text-danger">*</small>
                                        </p>
                                    </div>
                                    <div class="col-12 col-md-8 form-group">
                                        @php
                                            foreach($product->getPrice as $productPrice){

                                                echo '
                                                    <button type="button" class="btn btn-dark edit-price" data-toggle="modal" data-target="#newProductModaledit" data-price-id="'.$productPrice->id.'">'.$productPrice->country_id.'</button>
                                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                                ';
                                            }
                                        @endphp
                                    </button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-4 text-md-right my-auto">
                                        <p>
                                            Mark Up Category<small class="text-danger">*</small>
                                        </p>
                                    </div>
                                    <div class="col-12 col-md-8 form-group">
                                        <select name="markup_category[]" id="markup_category"
                                            class="select2 form-control">
                                            <option value="0">Choose one markup category</option>
                                            @foreach($markupCategories as $category)
                                            <option value="{{ $category->id }}" {{ $product->markupCategories && $product->markupCategories->contains($category->id) ? 'selected' : '' }}
                                            >Name : {{ $category->cat_name }}, Description : {{ $category->cat_desc}}, Markup Rate {{ $category->markup_rate}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                            @endif

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Estimate Ship Out Date (days)<small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <input type="number" name="estimate_ship_out_date" id="estimate_ship_out_date"
                                        class="form-control" value="{{ $product->estimate_ship_out_date }}">
                                </div>
                            </div>
                            {{-- @foreach ($controller::$category_type as $key => $category_name)
                                <div class="row">
                                    <div class="col-12 col-md-4 text-md-right my-auto">
                                        <p>
                                            {{$category_name}}<small class="text-danger">*</small>
                                        </p>
                                    </div>

                                    <div class="col-12 col-md-8 form-group">
                                        <select class="select2 form-control productCategory" id="{{$key}}Category" name="{{$key}}categories">
                                            <option value="default">Please choose {{$key}} category. </option>
                                            @foreach($shippingCategory->where('cat_type', $key); as $shippingcategory)
                                            @php
                                            $categoryName = $key.'_category';
                                            @endphp
                                            <option value="{{ $shippingcategory->id }}" {{$shippingcategory->id == $product->$categoryName ? 'selected' : ''}} >Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endforeach --}}

                              <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Shipping Categories<small class="text-danger">*</small>
                                    </p>
                                </div>
                                @php
                                $key = 'shipping';
                                $categoryName = $key.'_category';
                                @endphp
                                <div class="col-12 col-md-8 form-group">
                                    <select class="select2 form-control productCategory" id="{{$key}}Category" name="{{$key}}categories{{ $key == 'installation' ? '[]' : '' }}" {{ $key == 'installation' ? 'multiple="multiple"' : '' }}>
                                        <option value="0">Please choose {{$key}} category. </option>
                                        @foreach($shippingCategory->where('cat_type', $key); as $shippingcategory)
                                        <option value="{{ $shippingcategory->id }}"  {{$shippingcategory->id == $product->$categoryName ? 'selected' : ''}} >Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Installation Categories<small class="text-danger">*</small>
                                    </p>
                                </div>
                                @php
                                $key = 'installation';
                                $categoryName = $key.'_category';
                                @endphp
                                <div class="col-12 col-md-8 form-group">
                                    <select class="select2 form-control productCategory" id="installationCategory" name="installationCategories[]"multiple="multiple">
                                        <option value="0">Please choose installation category. </option>
                                        @foreach($shippingCategory->where('cat_type', 'installation'); as $shippingcategory)
                                        <option value="{{ $shippingcategory->id }}"  {{ $product->installationCategories() && $product->installationCategories()->contains($shippingcategory->id) ? 'selected' : '' }}
                                            {{-- $data!=0 && in_array($shippingcategory->id , $data) ? 'selected' : '' --}} >Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Ships From <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <select name="ships_from" id="ships_from" class="select2 form-control">
                                        <option value="default">Please choose origin state..</option>
                                        @foreach($states as $state)
                                        <option value="{{ $state->id }}"
                                            {{ ($product->origin_state_id == $state->id) ? 'selected' : '' }}>
                                            {{ $state->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <!-- Available In -->
                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Available In <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8" id="availableInContainer">
                                    @if($product->deliveries->count() > 0)
                                    @foreach($product->deliveries as $delivery)
                                    <div class="row no-gutters @if($loop->last) default-item @endif">
                                        <div class="col-12 col-md-6 form-group p-1">
                                            <select name="available_in[]" class="select2 form-control available-in">
                                                <option value="default">Please choose a state..</option>
                                                @foreach($states as $key => $state)
                                                <option value="{{ $state->id }}"
                                                    {{ (($delivery->state_id == $state->id) || (old('available_in') == $key)) ? 'selected' : '' }}>
                                                    {{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-10 col-md-6 form-group p-1">
                                            <input type="text" name="available_in_price[]"
                                                class="form-control input-mask-price d-inline-block" style="width: 75%;"
                                                value="{{  $delivery->delivery_fee }}">

                                            @if(!$loop->last)
                                            <button type="button" class="btn btn-danger" style="width: 20%;"
                                                onclick="removeAvailableInElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            @else
                                            <button type="button" class="btn btn-danger d-none" style="width: 20%;"
                                                onclick="removeAvailableInElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style="width: 20%;"
                                                onclick="addMoreAvailableInElement();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="row no-gutters default-item">
                                        <div class="col-12 col-md-6 form-group p-1">
                                            <select name="available_in[]" class="select2 form-control available-in">
                                                <option value="default">Please choose a state..</option>
                                                @foreach($states as $state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-10 col-md-6 form-group p-1">
                                            <input type="text" name="available_in_price[]"
                                                class="form-control input-mask-price d-inline-block"
                                                style="width: 75%;">

                                            <button type="button" class="btn btn-danger d-none" style="width: 20%;"
                                                onclick="removeAvailableInElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style="width: 20%;"
                                                onclick="addMoreAvailableInElement();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Know More <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_description" id="product_description" cols="30" rows="10"
                                        class="form-control summernote">
                                    {!! $product->product_description !!}
                                    </textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Materials <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_material" id="product_material" cols="30" rows="10"
                                        class="form-control summernote">
                                        {!! $product->product_material !!}
                                    </textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Consistency <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_consistency" id="product_consistency" cols="30" rows="10"
                                        class="form-control summernote">
                                        {!! $product->product_consistency !!}
                                    </textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Product Package <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="product_package" id="product_package" cols="30" rows="10"
                                        class="form-control summernote">
                                        {!! $product->product_package !!}
                                    </textarea>
                                </div>
                            </div>

                            <hr>
                            <!-- Delivery Coondition -->

                            <div class="row my-2">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <h5 class="mb-3">Product Delivery ..</h5>
                                </div>
                                <div class="col-6 col-md-8">
                                    <div class="form-check form-check-inline ">
                                        <label class="form-check-label check" for="individualDeliveryRange">
                                            <input class="inputGroup form-check-input" type="checkbox"
                                                id="individualDeliveryRange"
                                                {{ $deliveries->pluck('group_or_individual')->first() == 'i' ? 'checked' : '' }}>
                                            Individual <i class="fas fa-question-circle"
                                                title="Delivery fee is based on each product."></i>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline ">
                                        <label class="form-check-label check" for="groupDeliveryRange">
                                            <input class="form-check-input" type="checkbox" id="groupDeliveryRange"
                                                {{ $deliveries->pluck('group_or_individual')->first() == 'g' ? 'checked' : '' }}>
                                            Group <i class="fas fa-question-circle"
                                                title="Delivery fee is group by product category."></i>
                                        </label>
                                    </div>


                                </div>
                            </div>
                            <div class="row my-2">
                                <div class="col-12 offset-md-2 col-md-10 border" id="deliveryRangeContainer">
                                    @if($deliveries->count() > 0)
                                    @foreach ($deliveries as $deliveryRange)
                                    <div class="row px-3 p-3 @if($loop->last) default-item @endif">
                                        <div class="col-6 d-none">
                                            <input type="text" class="newID" name="firstCategory[]"
                                                value="{{ $deliveryRange->id }}">
                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="individualDeliveryRange"><input
                                                        class="inputGroup form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="individualDeliveryRange" value="i"
                                                        {{ $deliveryRange->group_or_individual=='i'?'checked':'' }}>Individual</label>
                                            </div>
                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="groupDeliveryRange"><input
                                                        class="form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="groupDeliveryRange" value="g"
                                                        {{ $deliveryRange->group_or_individual=='g'?'checked':'' }}>Group</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="min_delivery" class="col-5 pr-0 col-form-label my-auto">Min
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="min_delivery[]"
                                                        class="input-mask-price form-control" id="min_delivery"
                                                        value="{{ $deliveryRange->min_price }}">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="max_delivery" class="col-5 pr-0 col-form-label my-auto">Max
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="max_delivery[]"
                                                        class="input-mask-price form-control" id="max_delivery"
                                                        value="{{ $deliveryRange->max_price }}">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="inputPassword"
                                                    class="col-5 pr-0 col-form-label my-auto">Delivery
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="fixed_delivery[]"
                                                        class="input-mask-price form-control" id="fixed_delivery"
                                                        value="{{ $deliveryRange->delivery_fee }}">
                                                </div>
                                            </div>
                                            <div class="col-2 row mx-auto px-0">
                                                <div class="form-check my-auto">
                                                    <select name="no_purchase[]"
                                                        class="select2 form-control no-purchase">
                                                        <option value="0"
                                                            {{ $deliveryRange->no_purchase == '0' ? 'selected' : '' }}>
                                                            Can Purchase</option>
                                                        <option value="1"
                                                            {{ $deliveryRange->no_purchase == '1' ? 'selected' : '' }}>
                                                            No Purchase</option>
                                                    </select>
                                                </div>
                                            </div>
                                            @if(!$loop->last)
                                            <button type="button" class="btn btn-danger" style=""
                                                onclick="removeDeliveryRangeElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            @else
                                            <button type="button" class="btn btn-danger d-none" style=""
                                                onclick="removeDeliveryRangeElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style=""
                                                onclick="addDeliveryRangeElement();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="row px-3 p-3 default-item">
                                        <div class="col-6 d-none ">

                                            <input type="text" class="newID" name="firstCategory[]" value="new">

                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="individualDeliveryRange"><input
                                                        class="inputGroup form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="individualDeliveryRange"
                                                        value="i">Individual</label>
                                            </div>
                                            <div class="form-check form-check-inline">

                                                <label class="form-check-label " for="groupDeliveryRange"><input
                                                        class="form-check-input" type="checkbox"
                                                        name="selectDeliveries[]" id="groupDeliveryRange"
                                                        value="g">Group</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="min_delivery" class="col-5 pr-0 col-form-label my-auto">Min
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="min_delivery[]"
                                                        class="input-mask-price form-control" id="min_delivery">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="max_delivery" class="col-5 pr-0 col-form-label my-auto">Max
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="max_delivery[]"
                                                        class="input-mask-price form-control" id="max_delivery">
                                                </div>
                                            </div>
                                            <div class="col-3 row mx-auto px-0">
                                                <label for="inputPassword"
                                                    class="col-5 pr-0 col-form-label my-auto">Delivery
                                                    Price</label>
                                                <div class="col-7 pl-0 my-auto">
                                                    <input type="text" name="fixed_delivery[]"
                                                        class="input-mask-price form-control" id="fixed_delivery">
                                                </div>
                                            </div>
                                            <div class="col-2 row mx-auto px-0">
                                                <div class="form-check my-auto">
                                                    <select name="no_purchase[]" class="select2 form-control ">
                                                        <option value="0">
                                                            Can Purchase</option>
                                                        <option value="1">
                                                            No Purchase</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-danger d-none" style=""
                                                onclick="removeDeliveryRangeElement(this);">
                                                <i class="fa fa-minus"></i>
                                            </button>

                                            <button type="button" class="btn btn-success" style=""
                                                onclick="addDeliveryRangeElement();">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row my-3">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    Delivery Information
                                </div>
                                <div class="col-6 col-md-10">
                                    <textarea name="delivery_text" id="delivery_text" rows="1"
                                        class="form-control summernote">
                                        {!! isset($deliveries->first()->delivery_text) ? $deliveries->first()->delivery_text : '' !!}
                                    </textarea>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    Free delivery state
                                </div>
                                <div class="col-6 col-md-10">
                                    <div class="row">
                                        @foreach($states as $state)
                                        <div class="col-md-2">
                                            <span class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input item-checkbox"
                                                    name="freeStates[]" id="freeStates-{{ $state->id }}"
                                                    value="{{ $state->id }}" @if ($deliveries->count() > 0)

                                                @if(($deliveries->first()->free_state != null))
                                                {{ in_array($state->id , json_decode($deliveries->first()->free_state)) ? 'checked' : '' }}
                                                @endif

                                                @endif>
                                                <label class="custom-control-label" for="freeStates-{{ $state->id }}">
                                                    {{ $state->name }}</label>
                                            </span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <h5 class="mb-3">Product Variations ..</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-2 text-md-right my-auto">
                                    <p>
                                        Variations <small class="text-danger">*</small>
                                    </p>
                                </div>

                                <div class="col-12 col-md-10" id="variationContainer">
                                    @if($product->attributes->count() > 0)
                                    @foreach($product->attributes as $attribute)
                                        @include('administrator.products.v1.panel.partial.product-variations')
                                    @endforeach
                                    @else
                                    @php
                                        $attribute = null;
                                    @endphp
                                        @include('administrator.products.v1.panel.partial.product-variations')
                                    @endif
                                </div>
                            </div>

                            @if ($parentProduct->product_type =='bundle')
                            <hr>

                                <div class="row">
                                    <div class="col-12 col-md-2 text-md-right my-auto">
                                        <h5 class="mb-3">Product Bundle ..</h5>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-2 text-md-right my-auto">
                                        <p>Set <small class="text-danger">*</small></p>
                                    </div>

                                    <div class="col-12 col-md-10" id="bundledesignContainer">
                                        @if($product->bundles->count() > 0)
                                            @foreach($product->bundles as $bundle)

                                                <div class="col-12 bundle-box @if($loop->last) default-item @endif bundlebox" data-group="{{$bundle->bundle_id}}">
                                                    <div class="row no-gutters p-2 mb-1" style="border: 1px solid #b3b3b3; border-radius: 5px;">
                                                        <input type="hidden" class="product_bundle_id" name="product_bundle_id[]" id="newProductbundle" value="{{ $bundle->id }}">

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <select name="bundle_id[]" class="select2 form-control bundle_id-in">
                                                                <option value="default">Select a bundle ID..</option>
                                                                <option value="1" {{ ($bundle->bundle_id == '1') ? 'selected' : '' }}>Type 1</option>
                                                                <option value="2" {{ ($bundle->bundle_id == '2') ? 'selected' : '' }}>Type 2</option>
                                                                <option value="3" {{ ($bundle->bundle_id == '3') ? 'selected' : '' }}>Type 3</option>
                                                                <option value="4" {{ ($bundle->bundle_id == '4') ? 'selected' : '' }}>Type 4</option>
                                                                <option value="5" {{ ($bundle->bundle_id == '5') ? 'selected' : '' }}>Type 5</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <input type="number" name="bundle_qty[]" id="bundle_qty" class="form-control input-mask-price d-inline bundle_qty-in" placeholder="Price" style="width: 100%;" value="{{ $bundle->bundle_qty }}">
                                                        </div>

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <select name="bundle_type[]" class="select2 form-control bundle_type-in">asd
                                                                <option value="default">Select a bundle type..</option>
                                                                <option value="0" {{ ($bundle->bundle_type == '0') ? 'selected' : '' }}>Optional</option>
                                                                <option value="1" {{ ($bundle->bundle_type == '1') ? 'selected' : '' }}>Non - Optional</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <select name="product_attribute_id[]" class="select2 form-control bundle_product_attribute_id-in">
                                                                <option value="default">Please choose a product code.</option>
                                                                @foreach ($products as $product)
                                                                <option value="{{ $product->id }}" {{ ($bundle->product_attribute_id == $product->id) ? 'selected' : '' }}>
                                                                    {{$product->product_code}} - {{$product->name}} ({{$product->attribute_name}})
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <input type="text" name="bundle_title[]" class="form-control bundle_title-in" placeholder="Title Name" value="{{ $bundle->bundle_title }}">
                                                        </div>

                                                        {{-- decode json, bundle variation --}}
                                                        <?php $bundlejson = json_decode($bundle->bundle_variation,FALSE); ?>

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <select name="bundle_variation[]" class="select2 form-control">
                                                                <option value="default">Select a variation type..  </option>
                                                                <option value="color" {{ ($bundlejson->selection == 'color') ? 'selected' : '' }}>Color</option>
                                                                <option value="size" {{ ($bundlejson->selection == 'size') ? 'selected' : '' }}>Size</option>
                                                                <option value="light-temperature" {{ ($bundlejson->selection == 'light-temperature') ? 'selected' : '' }}>Light Temperature</option>
                                                                <option value="curtain-size" {{ ($bundlejson->selection == 'curtain-size') ? 'selected' : '' }}>Curtain Size</option>
                                                                <option value="miscellanous" {{ ($bundlejson->selection == 'miscellanous') ? 'selected' : '' }}>Miscellanous</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-12 col-md-3 form-group p-1">
                                                            <input type="text" name="bundle_variation_selection[]"
                                                            class="form-control" placeholder="Name" value="{{ $bundlejson->name }}">
                                                        </div>

                                                        <div class="ml-auto col-2 p-1">
                                                            @if(!$loop->last)
                                                            <button type="button" class="btn btn-danger" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                            @else
                                                            <button type="button" class="btn btn-danger d-none" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                                                <i class="fa fa-minus"></i>
                                                            </button>

                                                            <button type="button" class="btn btn-success" style="width: 100%;" onclick="addMoreBundleDesignSetElement();">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                            @endif
                                                        </div>
                                                        <div class='error-message text-danger'></div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        @else

                                            <div class="col-12 bundle-box default-item" data-group="1">
                                                <div class="row no-gutters p-2 mb-1" style="border: 1px solid #b3b3b3; border-radius: 5px;">

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <select name="bundle_id[]" class="select2 form-control bundle_id-in">
                                                            <option value="default">Select a bundle ID..</option>
                                                            <option value="1">Type 1</option>
                                                            <option value="2">Type 2</option>
                                                            <option value="3">Type 3</option>
                                                            <option value="4">Type 4</option>
                                                            <option value="5">Type 5</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <input type="number" name="bundle_qty[]" class="form-control bundle_qty-in" placeholder="bundle_qty">
                                                    </div>

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <select name="bundle_type[]" class="select2 form-control bundle_type-in">
                                                            <option value="default">Select a bundle type..</option>
                                                            <option value="0">Optional</option>
                                                            <option value="1">Non - Optional</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <select name="product_attribute_id[]" class="select2 form-control bundle_product_attribute_id-in">
                                                            <option value="default">Please choose a product code.</option>
                                                            @foreach ($products as $product)
                                                            <option value="{{ $product->id }}">{{$product->product_code}} - {{$product->name}} ({{$product->attribute_name}})</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <input type="text" name="bundle_title[]" class="form-control bundle_title-in" placeholder="Title Name">
                                                    </div>

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <select name="bundle_variation[]" class="select2 form-control">
                                                            <option value="default">Select a variation type..</option>
                                                            <option value="color">Color</option>
                                                            <option value="size">Size</option>
                                                            <option value="light-temperature">Light Temperature</option>
                                                            <option value="curtain-size">Curtain Size</option>
                                                            <option value="miscellanous">Miscellanous</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-12 col-md-3 form-group p-1">
                                                        <input type="text" name="bundle_variation_selection[]" class="form-control" placeholder="Name">
                                                    </div>

                                                    <div class="ml-auto col-2 p-1">
                                                        <button type="button" class="btn btn-danger d-none" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                                            <i class="fa fa-minus"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-success" style="width: 100%;" onclick="addMoreBundleDesignSetElement(this);">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>

                                                    <div class='error-message text-danger'></div>

                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="row mt-2">
                                <div class="col-12 text-right">
                                    <button type="submit" id="saveBtn" class="btn btn-primary submit">Save</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- End Form -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Empty Modal -->
<div class="modal fade" id="ModalId" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Ajax response loaded here -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('style')
<style>
    .select2.select2-container {
        width: 100% !important;
    }
</style>
    
@endpush

@push('script')
<script>

    // $.each($("input[name='cartItemId[]']:checked"), function() {
        // input[type="radio"]
    $("#normalDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', false);
    });
    $("#individualDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', false);
    });
    $("#groupDeliveryRange").click(function(){
        $("input:checkbox[id='normalDeliveryRange']").prop('checked', false);
        $("input:checkbox[id='groupDeliveryRange']").prop('checked', this.checked);
        $("input:checkbox[id='individualDeliveryRange']").prop('checked', false);
    });
    $('.check input:checkbox').click(function() {
        $('.check input:checkbox').not(this).prop('checked', false);
    });
    //
    function addMoreAvailableInElement() {
        let availableInContainer = $('#availableInContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('id')
        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.find('select.select2').removeClass('is-invalid')
        cloneItem.find('select.select2').removeClass('is-valid')
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeAvailableInElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function addMoreProductVariationElement() {
        let availableInContainer = $('#variationContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');


        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.find('.product_variation_id').val('new');
        cloneItem.find('.noClone').remove();
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeProductVariationElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function addMoreBundleDesignSetElement() {
        let availableInContainer = $('#bundledesignContainer')

        let currentItem = availableInContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');


        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.find('.product_bundle_id').val('new');
        cloneItem.appendTo(availableInContainer);
        cloneItem.slideDown();
        bundleDefaultAction(cloneItem);

        availableInContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeBundleDesignSetElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function addDeliveryRangeElement() {
        let deliveryRangeContainer = $('#deliveryRangeContainer')

        let currentItem = deliveryRangeContainer.find('.default-item');

        currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.newID').val('new');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(deliveryRangeContainer);
        cloneItem.slideDown();

        deliveryRangeContainer.find('select.select2').select2({
            theme: 'bootstrap4',
        });

        InputMaskPrice();
    }

    function removeDeliveryRangeElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

        $('.bundle-box').each(function(index, _selectGroup){
            bundleDefaultAction(_selectGroup);
        });

        function bundleDefaultAction(_selectGroup){

            $(_selectGroup).find('[name="bundle_id[]"]').change(function(){
                selectvalue = $(_selectGroup).find('[name="bundle_id[]"] option:selected').val();

                $(_selectGroup).data('group', selectvalue);
                $(_selectGroup).attr('data-group',selectvalue);

                saveBtn = document.querySelector('form #saveBtn');
                bundleAutoChange($(_selectGroup));
            });

            $(_selectGroup).find('[name="bundle_title[]"]').change(function(){
                bundleAutoChange($(_selectGroup));
            });

            $(_selectGroup).find('[name="bundle_qty[]"]').change(function(){
                bundleAutoChange($(_selectGroup));
            });

            $(_selectGroup).find('[name="bundle_type[]"]').change(function(){
                bundleAutoChange($(_selectGroup));
            });

        }

        function bundleAutoChange(_selectGroup){

            selectvalue = $(_selectGroup).data('group');
            bundle_title = $(_selectGroup).find('[name="bundle_title[]"]').val();
            bundle_qty = $(_selectGroup).find('[name="bundle_qty[]"]').val();
            bundle_type = $(_selectGroup).find('[name="bundle_type[]"]').val();

            bundle_title_error = true;
            bundle_qty_error = true;
            bundle_type_error = true;

            $('[data-group="'+selectvalue+'"]').each(function(index,_typeGroup){
               if ( $(_typeGroup).find('[name="bundle_title[]"]').val() != bundle_title) bundle_title_error = false;
               if ( $(_typeGroup).find('[name="bundle_qty[]"]').val() != bundle_qty) bundle_qty_error = false;
               if ( $(_typeGroup).find('[name="bundle_type[]"] option:selected').val() != bundle_type) bundle_type_error = false;
            });

            if(!bundle_title_error || !bundle_qty_error || !bundle_type_error){
                $(_selectGroup).find('.error-message').html('Type' +selectvalue+' : qty or choice or title are different');
                $(saveBtn).attr("disabled", true);
            }else{
                $(_selectGroup).find('.error-message').html('');
                $(saveBtn).attr("disabled", false);
            }

        }

        function InputMaskPrice(){
            let priceSelectors = document.getElementsByClassName('input-mask-price');

            let priceInputMask = new Inputmask({
                'mask': '99999.99',
                'numericInput': true,
                'digits': 2,
                'digitsOptional': false,
                'placeholder': '0'
            });

            for (var i = 0; i < priceSelectors.length; i++) {
                priceInputMask.mask(priceSelectors.item(i));
            }
        }

    $(document).ready(function() {
        InputMaskPrice();

        $('select.select2').select2({
            theme: 'bootstrap4',
        });

        $('.summernote').summernote({
            height: 200, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
        });

        // Variables Initialization
        // Input Into Variables.
        const panelAccountId = $('#panel_id');
        const price = $('#price');
        const memberPrice = $('#member_price');
        const bpRequired = $('#bp_required');
        const productSv = $('#product_sv');
        const productVariationSv = $('#product_variation_sv');
        const productCode = $('#product_variation_code');
        const bundleqty = $('#bundle_qty');
        const shipFrom = $('#ships_from');
        // const attributeType = $('#attriButeType');
        const attrcol = document.querySelectorAll('#colattr');
        // const imgcheck = document.querySelectorAll('#colattr #imgclr');

        // const attributeType = document.querySelectorAll('select#attriButeType');
        const myNotice = document.getElementById('mynote');
        const bundleGroup = document.querySelectorAll('.bundlebox');
        // const attValue = attributeType.options[attributeType.selectedIndex].value;
        const delivery = $('.available-in');
        // let bundleType = $('.bundle_type-in');
        // let bundleID = $('.bundle_id-in');
        // let bundleProduct = $('.bundle_product_attribute_id-in');
        // let bundleTitle = $('.bundle_title-in');

        panelAccountId.on('change', function() {
            if ($(this).val() == 'default') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        price.on('keyup', function() {
            if ($(this).val() == 0) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        memberPrice.on('keyup', function() {
            if ($(this).val() == 0) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        //curtain images check
        attrcol.forEach(attitem => {
            const a1 = attitem.querySelector('.type-attr');
            const b1 = attitem.querySelector('.curtain_size_icon');
            // console.log(typeof a1 + " " + 'type a1');

            a1.onchange = function() {
                console.log(a1.value);
                if(a1.value == 'default' || a1.value == 'curtain-size' && b1.value == '0'){
                    a1.classList.add('is-invalid');
                    b1.classList.add('is-invalid');
                } else {
                    a1.classList.remove('is-invalid');
                    b1.classList.remove('is-invalid');
                }
            };

            b1.onchange = function() {
                console.log(b1.value);
                if(b1.value == '0' && a1.value == 'curtain-size' ){
                    a1.classList.add('is-invalid');
                    b1.classList.add('is-invalid');
                } else {
                    a1.classList.remove('is-invalid');
                    b1.classList.remove('is-invalid');
                }
            }

        });

        //bundle check
        bundleGroup.forEach(item =>{
            const a = item.querySelector('.bundle_id-in');
            const b = item.querySelector('.bundle_product_attribute_id-in');
            const c = item.querySelector('.bundle-variation');
            const h = item.querySelector('.bundle_type-in');
            const j = item.querySelectorAll('input');

            a.onchange = function(){
                if(a.value == 'default'){
                    a.classList.add('is-invalid');
                }else {
                    a.classList.remove('is-invalid');
                }
            };

            b.onchange = function(){
                if(b.value == 'default'){
                    b.classList.add('is-invalid');
                }else {
                    b.classList.remove('is-invalid');
                }
            };

            c.onchange = function(){
                if(c.value == 'default'){
                    c.classList.add('is-invalid');
                }else {
                    c.classList.remove('is-invalid');
                }
            };

            h.onchange = function(){
                if(h.value == 'default'){
                    h.classList.add('is-invalid');
                }else {
                    h.classList.remove('is-invalid');
                }
            };

            j.forEach(inputItem => {
                inputItem.onchange = function(){
                    if(inputItem.value == '' || inputItem.value < 1){
                        inputItem.classList.add('is-invalid');
                    }else {
                        inputItem.classList.remove('is-invalid');
                    }
                };
            });
        });
        /*
        bpRequired.on('keyup', function() {
            if ($(this).val == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        */
        productSv.on('keyup', function() {
            if ($(this).val == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        bundleqty.on('keyup', function() {
            if ($(this).val == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        shipFrom.on('change', function() {
            if ($(this).val() == 'default') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

        // console.log(attributeType);
        // attributeType.forEach(function(testA){

        //     testA.addEventListener( 'change', function() {
        //         console.log(testA.classList);
        //         if(testA.value === 'default') {
        //             testA.classList.add('is-invalid');
        //             testA.classList.remove('is-valid');
        //         } else {
        //             testA.classList.remove('is-invalid');
        //             testA.classList.add('is-valid');
        //         }
        //     });
        // });

        $('#edit-panel-product-form').on('submit', function(event) {
            let errors = 0;
            let bundleCount = 0;

            function countBundle(){
                bundleCount = bundleCount + 1;
            }

            function countAttr(){
                errors = errors + 1;
            }

            // Validation
            if (panelAccountId.val() == 'default') {
                errors = errors + 1;
                panelAccountId.removeClass('is-valid');
                panelAccountId.addClass('is-invalid');
            }
            if (price.val() == 0) {
                errors = errors + 1;
                price.removeClass('is-valid');
                price.addClass('is-invalid');
            }
            if (memberPrice.val() == 0) {
                errors = errors + 1;
                memberPrice.removeClass('is-valid');
                memberPrice.addClass('is-invalid');
            }
            if (shipFrom.val() == 'default') {
                errors = errors + 1;
                shipFrom.removeClass('is-valid');
                shipFrom.addClass('is-invalid');
            };

            //special for curtain attribute type
            attrcol.forEach(attitem => {
                const cc = attitem.querySelector('.type-attr');
                const dd = attitem.querySelector('.curtain_size_icon');
                // console.log(bb.value);

                if(cc.value == 'curtain-size' && dd.value == '0' || cc.value == 'default'){
                    countAttr();
                }
            });

            //bundel all type validation
            bundleGroup.forEach(d =>{
                const a = d.querySelector('.bundle_id-in');
                const b = d.querySelector('.bundle_product_attribute_id-in');
                const c = d.querySelector('.bundle-variation');
                const f = d.querySelector('.bundle_type-in');
                const g = d.querySelector('.input-mask-price');
                const h = d.querySelectorAll('input');

                if(a.value == 'default' || b.value == 'default' || c.value == 'default' || f.value == 'default' || g.value == ''){
                    countBundle();
                }

                h.forEach(inputItemValid => {
                    if(inputItemValid.value == '' || inputItemValid.value < 1) {
                        countBundle();
                    }
                });

            });

            // const attributeType = document.querySelectorAll('#attriButeType');

            // attributeType.forEach(function(checkpass){
            //     if(checkpass.value === 'default') {
            //         errors = errors + 1;
            //         $(checkpass).removeClass('is-valid');
            //         $(checkpass).addClass('is-invalid');
            //     } else {
            //         $(checkpass).addClass('is-valid');
            //         $(checkpass).removeClass('is-invalid');
            //     }
            // });

            /*
            if (bpRequired.val() == '') {
                errors = errors + 1;
                bpRequired.removeClass('is-valid');
                bpRequired.addClass('is-invalid');
            }
            */

            // attrcol.forEach(function(checkimg) {
            //     if(checkedOne.checked == false) {
            //         errors = errors + 1;
            //         console.log('function');
            //         myNotice.setAttribute("style", "display:block;color:#f00;");
            //     }
            // });

                // if(checkedOne == false) {
                //     errors = errors + 1;
                //     console.log(checkedOne);
                //     // $(this).removeClass('is-valid');
                //     // $(myNotice).addClass('is-invalid');
                //     myNotice.setAttribute("style", "display:block;color:#f00;");
                // }


            if (productSv.val() == '') {
                errors = errors + 1;
                productSv.removeClass('is-valid');
                productSv.addClass('is-invalid');
            }

            // if ($('.productCategory :selected').val() == '0') {
            //     // if (productSv.val() == '') {
            //     errors = errors + 1;
            //     $('.productCategory').removeClass('is-valid');
            //     $('.productCategory').addClass('is-invalid');
            // }
            // if (bundleqty.val() == 'aaa') {
            //     errors = errors + 1;
            //     bundleqty.removeClass('is-valid');
            //     bundleqty.addClass('is-invalid');
            // }

            // if (bundleTitle.val() == '') {
            //     errors = errors + 1;
            //     bundleTitle.removeClass('is-valid');
            //     bundleTitle.addClass('is-invalid');
            // }

            // delivery = $('.available-in');

            // delivery.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            // bundleType = $('.bundle_type-in');

            // bundleType.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            // bundleID = $('.bundle_id-in');

            // bundleID.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            // bundleProduct = $('.bundle_product_attribute_id-in');

            // bundleProduct.each(function() {
            //     if ($(this).val() == 'default') {
            //         errors = errors + 1;
            //         $(this).removeClass('is-valid');
            //         $(this).addClass('is-invalid');
            //     }
            // });
            if (errors > 0 || bundleCount > 0) {
                toastr.error('Please make sure the input highlighted in RED is correctly filled in.');
                return false;
            } else {
                return true;
            }
        });

        let editURL;
        let updateURL;

        $('#app').on('click', '.edit-price', function() {
            // Price Edit URL - GET
            editURL = "{{ route('administrator.v1.products.panels.price.edit', ['id' => 'id']) }}";

            let id = $(this).data('price-id');
            editURL = editURL.replace('id', id);

            $.ajax({
                async: true,
                beforeSend: function() {
                },
                complete: function() {
                    InputMaskPrice();
                },
                url: editURL,
                type: 'GET',
                success: function(response) {
                    $('#ModalId .modal-body').empty();

                    // Change modal title
                    $('#ModalId .modal-title').text('Edit Price')
                    // Add response in Modal body
                    $('#ModalId .modal-body').html(response);

                    // Display Modal
                    $('#ModalId').modal('show');

                    $('.select2-edit').select2({
                        dropdownParent: $('#ModalId')
                    });
                },

                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON.message);
                }

            });

        });



    });

</script>
@endpush

@push('style')
<style>
    /* HIDE RADIO */
    [type=radio] {
    position: absolute;
    opacity: 0;
    width: 0;
    height: 0;
    }

    /* IMAGE STYLES */
    [type=radio] + img {
    cursor: pointer;
    }

    /* CHECKED STYLES */
    [type=radio]:checked + img {
    outline: 2px solid #f00;
    }
    .custom-control-input:checked~.custom-control-label::before {
        color: #fff;
        border-color: #ffcc00;
        background-color: #ffcc00;
    }
</style>
@endpush
