@extends('layouts.guest.main')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo" src="{{ asset('images/logo/formula-logo.png') }}" alt="">
        </div>
    </div>

    <div class="my-register-box my-guests-card">
        <div class="my-r-title-box">
            <h5><b>REGISTRATION</b></h5>
        </div>

        <div class="card-body">
            
            <form method="POST" action="{{ route('guest.register.walk-in.post') }}" id="register-walk-in">
                @csrf
                <div class="tab-content r-inner-box" id="myTabContent">
                    
                    <div class="r-subt-box tab-pane fade show active pl-3 pr-3 pt-3 pb-3" id="registration" role="tabpanel"
                        aria-labelledby="registration-tab">
                        <div class="form-row register-info">
                            <div class="form-group col-md-6">
                                <label for="referrer_name">Agent Name</label>
                                <input type="text" name="referrer_name"
                                    class="r-form-color form-control @error('referrer_name') is-invalid @enderror" readonly
                                    id="referrer_name" placeholder="Agent Name" value="{{$name}}">
                                @error('referrer_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="referrer_id">Agent ID</label>
                                <input type="text" name="referrer_id"
                                    class="r-form-color form-control @error('referrer_id') is-invalid @enderror" readonly
                                    id="referrer_id" placeholder="Agent ID" value="{{$id}}">
                                @error('referrer_id')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="full_name">Name</label>
                                <input type="text" name="full_name"
                                    class="r-form-color form-control @error('full_name') is-invalid @enderror" required
                                    id="full_name" placeholder="Name" value="{{ old('full_name') }}">
                                @error('full_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nric">IC Number</label>
                                <input type="text" name="nric"
                                    class="r-form-color form-control @error('nric') is-invalid @enderror" required
                                    id="nric" placeholder="IC Number" value="{{ old('nric') }}">
                                @error('nric')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number_mobile">Phone Number</label>
                                <input type="text" name="contact_number_mobile"
                                    class="r-form-color form-control @error('contact_number_mobile') is-invalid @enderror" required
                                    id="contact_number_mobile" placeholder="Phone Number" value="{{ old('contact_number_mobile') }}">
                                @error('contact_number_mobile')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="text" name="email"
                                    class="r-form-color form-control @error('email') is-invalid @enderror" required
                                    id="email" placeholder="Email" value="{{ old('email') }}">
                                @error('email')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control " required id="password" placeholder="Your password">
                                @error('password')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="r-next-btn ml-auto">
                                <a class="btn r-color-btn" id="submit-btn"><b>Submit</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#submit-btn').on('click', function () {
            $('#register-walk-in').submit();
        });
    </script>
@endpush
@push('style')
    <style>
        @media (max-width: 768px) {
            .r-subt-box {
                padding: 1rem;
            }
            .r-next-btn {
                padding-right: 1rem;
            }
        }
    </style>
@endpush