@extends('layouts.management.main-customer')

@section('content')

@section('page_title')
    {{ 'Promotion and Special Offer' }}
@endsection

<div class="fml-container">
    <div class="row">
        <div>
            <h4>Prepaid Method - Total {{count($ForCount)}}</h4>
        </div>
    </div>
    <div class="rbs-system">
        <table class="datatables-demo table blue table-striped">
            <thead class="pt-2">
                <tr class="tit-style" style="border-bottom:1px black solid;">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-sort="ascending" aria-label=" Rendering engine: activate to sort column descending"
                        style="width: 50px;">No.
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="Browser: activate to sort column ascending" style="width: 150px;">
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">Product Name
                    </th>

                    <th class="sorting col-12 col-md-8 col-lg-4" tabindex="0" aria-controls="DataTables_Table_1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 230px;">
                        Next Order
                    </th>
                    <th class="sorting col-12 col-md-8 col-lg-4" tabindex="0" aria-controls="DataTables_Table_1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 230px;">
                        Tracking
                    </th>
                    <th class="sorting col-12 col-md-8 col-lg-4" tabindex="0" aria-controls="DataTables_Table_1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 230px;">
                        Received
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 80px;">Renew
                    </th>

                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1"
                        aria-label="CSS grade: activate to sort column ascending" style="width: 180px;">Remark
                    </th>
                </tr>
            </thead>
            <tbody class="p-list">
                @foreach ($purchases as $purchase)
                @php
                    $items = collect($purchase->sortItems());

                    $pendingOrder = $purchase->orders->where('collected_date','Pending');
                    $allOrders = $purchase->orders;
                    $completedOrders = $purchase->orders->where('collected_date','!=','Pending');
                    $nextMonth = date('m',strtotime('first day of +1 month'));
                    $showOrder = 2;
                    // dd($pendingOrder,$allOrder,$completedOrder);
                @endphp
                <tr @if ($completedOrders->count() == 0) style="border-bottom:1px black solid;" @endif >
                    <td rowspan="{{ $completedOrders->count() + 1 }}">{{ ($purchases->currentPage() - 1) * $purchases->perPage() + $loop->iteration }}</td>
                    <td rowspan="{{ $completedOrders->count() + 1 }}">
                        <div class="a-product">
                            @if ($purchase->items->first()->product->parentProduct->defaultImage)
                                <img class="vr-p-img"
                                src="{{ asset('storage/' . $purchase->items->first()->product->parentProduct->images[0]->path . $purchase->items->first()->product->parentProduct->images[0]->filename) }}"/>
                            @else
                                <img class="vr-p-img imgproduct" src="{{asset('/images/product_item/default-p.png')}}" alt="">
                            @endif
                        </div>
                    </td>
                    <td rowspan="{{ $completedOrders->count() + 1 }}">
                        <div class="title pr-md-2">
                            @foreach($purchase->sortItems() as $key => $item)
                            @if ($loop->first)
                            <h3>{{ $item->product->parentProduct->name }}</h3>
                            @if(array_key_exists('product_size', $item->product_information))
                                <p>({{ $item->product_information['product_size'] }})</p>
                            @endif
                            @if(array_key_exists('product_rbs_times', $item->product_information))
                                <p  class="pt-2"><span class="text-danger">*</span>  {{ 'Deliver ' . $item->quantity . ' item(s) every ' . $item->product_information['product_rbs_frequency'] . ' month(s) for ' . $item->product_information['product_rbs_times'] . ' times ' }}</p>
                            @endif
                            <p class="pt-2"><a href="{{asset('/storage/documents/invoice/'.$purchase->getFormattedNumber().(($purchase->invoice_version != 0) ? '/v'.$purchase->invoice_version.'/' : '/').$purchase->getFormattedNumber().'.pdf')}}">{{$purchase->purchase_number}}</a></p>
                            @endif
                            @endforeach
                        </div>
                    </td>
                    <td style="border-bottom:1px black solid;">{{ $allOrders->first()->order_date }}</td>
                    <td style="border-bottom:1px black solid;">
                        @php
                            if ($purchase->orders->first()->shipping_date != 'Pending') {
                                $shipDate = date("d-m-Y", strtotime($purchase->orders->first()->shipping_date));
                            }else{
                                $shipDate = $purchase->orders->first()->shipping_date;
                            }

                            if ((isset($purchase->orders->first()->received_date) && $purchase->orders->first()->received_date != '')) {
                                $collectedDate = $items->first()->order->received_date;
                            }elseif ($purchase->orders->first()->collected_date != 'Pending') {
                                $collectedDate = date("d-m-Y", strtotime($purchase->orders->first()->collected_date));
                            }else{
                                $collectedDate = $purchase->orders->first()->collected_date;
                            }

                            switch ($purchase->orders->first()->order_status ) {
                                case '1001': // Order Shipped
                                case '1008': // ACD Delivered
                                case '1010': // ACD Shipout
                                case '1011': // ACD Self Delivered
                                $icon = 'lorry';
                                break;

                                case '1009': // ACD Collected
                                case '1003': // Order Delivered
                                $icon = 'collected';
                                break;

                                default:
                                $icon = 'none';
                                    break;
                            }
                        @endphp
                        <div class="vr-o-step">
                            <div class="vr-process1 step1">
                                <img class="img-process active"{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending')  ? 'active' : '' }}
                                title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? 'Estimate ship out date : ' . $shipDate : '' }}"
                                src="{{ asset('images/icons/vr-step1.png') }}"
                                data-toggle="tooltip">
                            </div>

                            <div class="midline"></div>

                            <div class="vr-process1 step2">
                                <img class="img-process {{  ( $icon == 'collected' || $purchase->orders->first()->order_status == '1002') ? 'active' : '' }}"
                                title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? ($purchase->orders->first()->delivery_method == 'Self-pickup') ?'Ready For Collection Date : ' . $shipDate : 'Ship out date : ' . $shipDate : '' }}"
                                {{-- title="Order Placed" --}}
                                src="{{ asset('images/icons/vr-step2.png') }}"
                                data-toggle="tooltip">
                            </div>

                            <div class="midline"></div>

                            <div class="vr-process1 step3">
                                <img class="img-process {{( $icon == 'collected') ? 'active' : ''}}"
                                    title="{{ ($purchase->orders->first()->order_status == '1003') || ($purchase->orders->first()->order_status == '1003') || ($purchase->orders->first()->order_status == '1009')
                                    || ($purchase->orders->first()->order_status == '1009') ? 'Order Complete : ' . $collectedDate : '' }}"
                                    src="{{ asset('images/icons/vr-step3.png') }}"
                                    data-toggle="tooltip">
                            </div>

                            <div class="midline"></div>

                            <div class="vr-process1">
                                <img class="img-process" src="{{ asset('images/icons/vr-step4.png')}}" data-toggle="tooltip" title="">
                            </div>
                        </div>
                        <br>
                        @if(isset($purchase->orders->first()->courier_name) && isset($purchase->orders->first()->tracking_number))
                            <div class="btn vr-filt-btn">
                                @php $tracking_link = \App\Models\Courier\Courier::trackingURL($purchase->orders->first()->courier_name, $purchase->orders->first()->tracking_number); @endphp
                                <a  @if($tracking_link != "#") target="_blank" href="{{$tracking_link}}"@endif >{{$purchase->orders->first()->courier_name}} Tracking: {{$purchase->orders->first()->tracking_number}}</a>
                            </div>
                        @endif
                    </td>
                    <td style="border-bottom:1px black solid;">{{ $allOrders->first()->collected_date }}</td>
                    <td rowspan="{{ $completedOrders->count() + 1 }}">
                        @if ($purchase->orders->last()->received_date != '')
                        <div class="icon-checkout-rbs">
                            <a href="#" id="DeleteCartItem" class="link-product">
                                <i class="fa fa-refresh" aria-hidden="true"></i>
                            </a>
                        </div>
                        @endif
                    </td>
                        <td rowspan="{{ $completedOrders->count() + 1 }}">
                            <div class="date-num">
                            </div>
                        </td>
                </tr>
                @if ($completedOrders->count() >= 1)
                    @foreach ($allOrders as $allOrder)
                        @if (!$loop->first && $loop->index < $completedOrders->count() + 1)
                        <tr style="border-bottom:1px black solid;">
                            <td>{{ $allOrder->order_date }}</td>
                            <td>
                                @php
                                    if ($allOrder->shipping_date != 'Pending') {
                                        $shipDate = date("d-m-Y", strtotime($allOrder->shipping_date));
                                    }else{
                                        $shipDate = $allOrder->shipping_date;
                                    }

                                    if ((isset($allOrder->received_date) && $allOrder->received_date != '')) {
                                        $collectedDate = $allOrder->received_date;
                                    }elseif ($allOrder->collected_date != 'Pending') {
                                        $collectedDate = date("d-m-Y", strtotime($allOrder->collected_date));
                                    }else{
                                        $collectedDate = $allOrder->collected_date;
                                    }

                                    switch ($allOrder->order_status ) {
                                        case '1001': // Order Shipped
                                        case '1008': // ACD Delivered
                                        case '1010': // ACD Shipout
                                        case '1011': // ACD Self Delivered
                                        $icon = 'lorry';
                                        break;

                                        case '1009': // ACD Collected
                                        case '1003': // Order Delivered
                                        $icon = 'collected';
                                        break;

                                        default:
                                        $icon = 'none';
                                            break;
                                    }
                                @endphp
                                <div class="vr-o-step">
                                    <div class="vr-process1 step1">
                                        <img class="img-process active"{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending')  ? 'active' : '' }}
                                        title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? 'Estimate ship out date : ' . $shipDate : '' }}"
                                        src="{{ asset('images/icons/vr-step1.png') }}"
                                        data-toggle="tooltip">
                                    </div>

                                    <div class="midline"></div>

                                    <div class="vr-process1 step2">
                                        <img class="img-process {{  ( $icon == 'collected' || $allOrder->order_status == '1002') ? 'active' : '' }}"
                                        title="{{ ($icon == 'lorry' || $icon == 'collected' || $shipDate != 'Pending') ? ($allOrder->delivery_method == 'Self-pickup') ?'Ready For Collection Date : ' . $shipDate : 'Ship out date : ' . $shipDate : '' }}"
                                        {{-- title="Order Placed" --}}
                                        src="{{ asset('images/icons/vr-step2.png') }}"
                                        data-toggle="tooltip">
                                    </div>

                                    <div class="midline"></div>

                                    <div class="vr-process1 step3">
                                        <img class="img-process {{( $icon == 'collected') ? 'active' : ''}}"
                                            title="{{ ($allOrder->order_status == '1003') || ($allOrder->order_status == '1003') || ($allOrder->order_status == '1009')
                                            || ($allOrder->order_status == '1009') ? 'Order Complete : ' . $collectedDate : '' }}"
                                            src="{{ asset('images/icons/vr-step3.png') }}"
                                            data-toggle="tooltip">
                                    </div>

                                    <div class="midline"></div>

                                    <div class="vr-process1">
                                        <img class="img-process" src="{{ asset('images/icons/vr-step4.png')}}" data-toggle="tooltip" title="">
                                    </div>
                                </div>
                                <br>
                                @if(isset($allOrder->courier_name) && isset($allOrder->tracking_number))
                                    <div class="btn vr-filt-btn">
                                        @php $tracking_link = \App\Models\Courier\Courier::trackingURL($allOrder->courier_name, $allOrder->tracking_number); @endphp

                                        <a  @if($tracking_link != "#") target="_blank" href="{{$tracking_link}}"@endif >{{$allOrder->courier_name}} Tracking: {{$allOrder->tracking_number}}</a>
                                    </div>
                                @endif
                            </td>
                            <td>{{ $allOrder->collected_date }}</td>
                        </tr>
                        @endif
                    @endforeach
                @endif
                @endforeach


                {!! $purchases->render() !!}
            </tbody>
        </table>
    </div>

    <div class="col-md-12 align-mid d-none">
        <ul class="nextpage" role="navigation">

            <li class="page-item">
                <a class="my-page-link" href="#" rel="prev" aria-label="« Previous">‹</a>
            </li>
            <li class="page-item"><a class="my-page-link" href="#">1</a></li>
            <li class="page-item active" aria-current="page"><span class="my-page-link">2</span></li>
            <li class="page-item"><a class="my-page-link" href="#">3</a></li>
            <li class="page-item"><a class="my-page-link" href="#">4</a></li>
            <li class="page-item"><a class="my-page-link" href="#">5</a></li>
            <li class="page-item"><a class="my-page-link" href="#">6</a></li>
            <li class="page-item"><a class="my-page-link" href="#">7</a></li>
            <li class="page-item"><a class="my-page-link" href="#">8</a></li>
            <li class="page-item"><a class="my-page-link" href="#">9</a></li>
            <li class="page-item"><a class="my-page-link" href="#">10</a></li>

            <li class="page-item disabled" aria-disabled="true"><span class="my-page-link">...</span></li>

            <li class="page-item"><a class="my-page-link" href="#">14</a></li>
            <li class="page-item"><a class="my-page-link" href="#">15</a></li>

            <li class="page-item">
                <a class="my-page-link" href="#" rel="next" aria-label="Next »">›</a>
            </li>
        </ul>
    </div>
</div>


@endsection
