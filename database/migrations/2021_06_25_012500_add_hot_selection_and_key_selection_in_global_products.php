<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHotSelectionAndKeySelectionInGlobalProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->integer('hot_selection')->default(0);
            $table->integer('key_selection')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->removeColumn('hot_selection');
            $table->removeColumn('key_selection');
        });
    }
}
