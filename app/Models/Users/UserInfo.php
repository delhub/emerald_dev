<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse\User\WarehouseUserLocation;
// Model to handle User Info

class UserInfo extends Model
{
    // Set table
    protected $table = 'user_infos';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'user_id';

    // Set mass assignable columns
    protected $fillable = [
        'account_id',
        'full_name',
        'nric',
        'referrer_id',
        'account_status'
    ];

    /**
     * Get the user info associated with the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id');
    }

    /**
     * Get the user contact associated with the user.
     */
    public function contacts()
    {
        return $this->hasMany('App\Models\Users\UserContact', 'account_id', 'account_id');
    }

    /**
     * Get all of the user's addresses.
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\Users\UserAddress', 'account_id', 'account_id');
    }

    /**
     * Get the customer's payment info.
     */
    public function paymentInfo()
    {
        return $this->hasOne('App\Models\Users\Customers\PaymentInfo', 'account_id', 'account_id');
    }

    /**
     * Get largest customer account id.
     */
    public function scopeLargestCustomerId($query)
    {
        return $query->max('account_id');
    }

    public function cartShippingAddress()
    {
        return $this->hasOne('App\Models\Users\UserAddress', 'account_id', 'account_id')
            ->where('is_shipping_address', 1)
            ->where('country_id', country()->country_id);
    }

    public function shippingAddress()
    {
        return $this->hasOne('App\Models\Users\UserAddress', 'account_id', 'account_id')
            ->where('is_shipping_address', 1);
    }

    public function mailingAddress()
    {
        return $this->hasOne('App\Models\Users\UserAddress', 'account_id', 'account_id')
            ->where('is_mailing_address', 1);
    }

    public function residentialAddress()
    {
        return $this->hasOne('App\Models\Users\UserAddress', 'account_id', 'account_id')
            ->where('is_residential_address', 1);
    }


    public function mobileContact()
    {
        return $this->hasOne('App\Models\Users\UserContact', 'account_id', 'account_id')
            ->where('is_mobile', 1);
    }

    public function homeContact()
    {
        return $this->hasOne('App\Models\Users\UserContact', 'account_id', 'account_id')
            ->where('is_home', 1);
    }

    public function officeContact()
    {
        return $this->hasOne('App\Models\Users\UserContact', 'account_id', 'account_id')
            ->where('is_office', 1);
    }

    /**
     * Get a user's agent information.
     */
    public function agentInformation()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerInfo', 'account_id', 'referrer_id');
    }

    public function agentGroup()
    {
        return $this->hasOne('App\Models\Dealers\DealerGroup', 'id', 'group_id');
    }

    public function showUserLevel()
    {

        if ($this->user_level == 6000) {
            return 'Standard Member';
        } elseif ($this->user_level == 6001) {
            return 'Advance Member';
        } elseif ($this->user_level == 6002) {
            return 'Premium Member';
        }
    }

    public function userCountries()
    {
        return $this->hasMany('App\Models\Users\UserCountry', 'account_id', 'account_id');
    }

    public function userCountry()
    {
        $country_id = country()->country_id;

        return $this->hasOne('App\Models\Users\UserCountry', 'account_id', 'account_id')->where('country_id', $country_id);

    }

    public function warehouseLocation()
    {
        return $this->hasMany(WarehouseUserLocation::class, 'account_id', 'account_id');
    }
}
