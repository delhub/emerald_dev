
@inject('controller', 'App\Http\Controllers\Warehouse\Location\LocationController')

@extends('layouts.administrator.main')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{route($controller::$home)}}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
    </div>
</div>
<div class="card shadow-sm mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <h4>
                    Edit {{$controller::$header}}
                </h4>
                @include('inc.messages')
            </div>
            <div class="col-12">
                <form action="{{ route($controller::$update,['id' => $data->id]) }}" id="edit-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                    {{-- <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Outlet Image <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <img id="featuredPreview"
                            src="@if ($data->defaultImage) {{ asset('storage/' . $data->defaultImage->path . $data->defaultImage->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif"
                            alt="Preview Image" style="width: 15rem; height: auto; margin-bottom: 1rem;">
                            <input type="file" name="outletImage" id="outletImage" class="form-control-file"
                                onchange="document.getElementById('featuredPreview').src = window.URL.createObjectURL(this.files[0])">
                            <small>Please make sure image is in 1:1 display ratio (square).</small>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Warehouse Key
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            {{-- <input type="text" id="warehouse_key" name="warehouse_key" class="form-control" value=""> --}}
                            {{ $data->location_key }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Location Name <small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="location_name" name="location_name" class="form-control" value="{{ $data->location_name }}">
                        </div>
                    </div>

                    <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Type<small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                {{ $data->type == 'WH' ? 'Warehouse' : 'Shop Outlet' }}
                                    {{-- <select name="type" class="form-control">
                                        <option value="WH" >Warehouse</option>
                                        <option value="SH" {{ $data->type == 'SH' ? 'selected' : '' }}>Shop Outlet</option>
                                    </select> --}}
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Country
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            {{$data->country_id}}
                            {{-- <select name="country_id" class="form-control">
                                @foreach($countries as $country)
                                    <option value="{{ $country->country_id }}" {{ $country->country_id == $data->country_id ? 'selected' : '' }}>{{ $country->country_name}}</option>
                                @endforeach
                            </select> --}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Printer Email
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="printer_email" name="printer_email" class="form-control" value="{{ $data->printer_email }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Contact Number
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="text" id="contact_number" name="contact_number" class="form-control" value="{{ $data->contact_number }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Default Address
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <textarea type="text" id="default_address" name="default_address" class="form-control" value="{{ $data->default_address }}">{{ $data->default_address }}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Postcode
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="number" id="postcode" name="postcode" class="form-control" value="{{ $data->postcode }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                State<small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            {{$data->state->name}}
                            {{-- <select  class="select2 select2-edit form-control" style="width: 100%;"
                                id="state_id" name="state_id">
                                @foreach($states as $state)
                                    <option value="{{ $state->id }}" {{ $state->id == $data->state_id ? 'selected' : '' }}>{{ $state->name}}</option>
                                @endforeach
                            </select> --}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                City<small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <select  class="select2 select2-edit form-control" style="width: 100%;"
                                id="city_key" name="city_key">
                                @foreach($cities as $city)
                                    <option value="{{ $city->city_key }}" {{ $city->city_key == $data->city_key ? 'selected' : '' }}>{{ $city->city_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Operation Time
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <textarea type="text" id="operation_time" name="operation_time" class="form-control" value="{{ $data->operation_time }}">{{ $data->operation_time }}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
