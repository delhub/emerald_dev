<hr class="mt-1">

<div style="height: 5rem;">
    {!! $product->details !!}
</div>
<!-- Miscellaneous -->
<!-- Add to cart / buy now -->
<div class="row no-gutters">
    <div class="col-12 p-1 m-0">
        <form id="renovation-add-to-cart-form" style="display: inline;" method="POST"
            action="/shop/product/interior-design/store">
            @method('POST')
            @csrf
            @if($panelProduct->miscellaneousAttributes->count() > 0)
            <div class="row mb-3">
                <div class="col-12">
                    {{-- <p class="mb-1">Sizes</p> --}}
                    <div class="boxed">
                        @foreach($panelProduct->miscellaneousAttributes as $key =>
                        $miscellaneousAttribute)
                        <input type="radio" id="miscellaneous-{{ $miscellaneousAttribute->id }}"
                            class="panel-product-attributes" name="miscellaneous"
                            value="{{ $miscellaneousAttribute->id }}" {{ ($key == 0) ? 'checked' : '' }}>
                        <label
                            for="miscellaneous-{{ $miscellaneousAttribute->id }}">{{ $miscellaneousAttribute->attribute_name }}</label>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            <div class="form-row form-group">
                <label for="price" class="col-lg-4 col-md-12 col-form-label">
                    <strong>Invoice Number</strong>
                </label>
                <div class="col-lg-6 col-md-8 col-12 ">
                    <input id="invoice_num" style="text-transform:uppercase" class="form-control"
                        type="text" name="invoice_num" id="invoice_num" placeholder="BJN0000000000">
                </div>
            </div>
            @if ($product->can_remark== 1)
            <div class="row mb-1">
                <div class="col-12">
                    <div class="form-group form-row order_remark">
                        <label for="miscellaneous_order_remark"
                            class="col-lg-4 col-md-12 col-form-label">
                            <strong>
                                Remarks
                            </strong>
                        </label>
                        <div class="col-lg-6 col-md-8 col-12">
                            <input type="text" name="miscellaneous_order_remark"
                                id="miscellaneous_order_remark" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if ($product->can_selfcollect== 1)
            <div class="row mb-1">
                <div class="col-12">
                    <div class="form-group form-row order_selfcollect">
                        <label for="miscellaneous_order_selfcollect"
                            class="col-lg-4 col-md-12 col-form-label">
                            <strong>
                                Self Collection?
                            </strong>
                        </label>
                        <div class="col-lg-6 col-md-8 col-12">
                            <input type="text" name="miscellaneous_order_selfcollect"
                                id="miscellaneous_order_selfcollect" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="form-row form-group">
                <label for="price" class="col-lg-4 col-md-12 col-form-label">
                    <strong>Price</strong>
                </label>
                <div class="col-lg-6 col-md-8 col-12 ">
                    <input id="price-input-mask"
                        class="input-mask-price form-control @error('price') is-invalid @enderror"
                        type="text" name="price" placeholder="eg: 120.00">
                    @error('price')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <input type="hidden" name="product_id" value="{{ $panelProduct->id }}">
            <input type="hidden" id="product_attribute_miscellaneous_buyNow"
                name="product_attribute_miscellaneous_buyNow" value="">
            <button type="button" id="miscellaneous_buyNow" onclick="miscellaneousSubmit()" class="btn btn-lg bjsh-btn-gradient font-weight-bold w-100"
                style="color: #1a1a1a;">Buy Now</button>
        </form>
    </div>
</div>
@push('miscellaneousScript')
<script>
    $(document).ready(function() {

        let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '999999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }

        const invoiceNumber = $('#invoice_num');
        const miscellaneousRemark = $('#miscellaneous_order_remark');
        const miscellaneousPrice = $('#price-input-mask');

        invoiceNumber.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        miscellaneousRemark.on('change', function() {
            if ($(this).val() == '') {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });
        miscellaneousPrice.on('change', function() {
            if ($(this).val() == '' || $(this).val() < 0.01) {
                $(this).removeClass('is-valid');
                $(this).addClass('is-invalid');
            } else {
                $(this).removeClass('is-invalid');
                $(this).addClass('is-valid');
            }
        });

    });
    function miscellaneousSubmit(){

        const invoiceNumber = $('#invoice_num');
        const miscellaneousRemark = $('#miscellaneous_order_remark');
        const miscellaneousPrice = $('#price-input-mask');

        let errors = 0;

        // Validation
        if (invoiceNumber.val() == '') {
            errors = errors + 1;
            invoiceNumber.removeClass('is-valid');
            invoiceNumber.addClass('is-invalid');
        }
        if (miscellaneousRemark.val() == '') {
            errors = errors + 1;
            miscellaneousRemark.removeClass('is-valid');
            miscellaneousRemark.addClass('is-invalid');
        }
        if (miscellaneousPrice.val() == '' || miscellaneousPrice.val() < 0.01) {
            errors = errors + 1;
            miscellaneousPrice.removeClass('is-valid');
            miscellaneousPrice.addClass('is-invalid');
        }

        if (errors > 0) {
            return false;
        } else {
            $('#renovation-add-to-cart-form').submit();
            return true;
        }


    }
</script>
@endpush
