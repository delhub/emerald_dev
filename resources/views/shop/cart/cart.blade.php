@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection

<div class="product-home" id="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist">
            <ul>
                <li><a href="/">Home</a></li>
                <li><span>/</span></li>
                <li><a href="#">Cart Page</a></li>
            </ul>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <section>
            <div class="fml-container">
                <div class="card-home">
                    {{-- Edit Address Modal --}}
                    <div class="modal fade" id="address_edit_form" aria-hidden="true"
                        aria-labelledby="address_edit_formLabel" tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box">
                                <form action="{{ route('shop.cart.update-address') }}" method="POST" id="cart-form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="modal-header">
                                        <h1 class="modal-title edit-tittle" id="address_edit_formLabel">Edit Your
                                            <span>Details</span>
                                        </h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleInputEmail0" class="form-label">Name<small
                                                    class="text-danger">*</small></label>
                                            <input type="text"
                                                class="checkout-box2 form-control @error('exampleInputEmail0') is-invalid @enderror"
                                                id="exampleInputEmail0" aria-describedby="emailHelp"
                                                placeholder="Your Name" name="name" value="{{ $cookie->name }}">
                                            @error('exampleInputEmail0')
                                                <small class="error form-text text-danger">{{ $message }}</small>
                                            @enderror
                                            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleInputEmail1" class="form-label">Contact (Mobile)<small
                                                    class="text-danger">*</small></label>
                                            <input type="text"
                                                class="checkout-box2 form-control @error('exampleInputEmail1') is-invalid @enderror"
                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                placeholder="Contact Number" name="phone"
                                                value="{{ $cookie->mobile }}">
                                            @error('exampleInputEmail1')
                                                <small class="form-text text-danger">{{ $message }}</small>
                                            @enderror
                                            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput0" class="form-label">Address 1<small
                                                    class="text-danger">*</small></label>
                                            <input type="text"
                                                class="checkout-box2 form-control @error('exampleFormControlInput0') is-invalid @enderror"
                                                id="exampleFormControlInput0" placeholder="Address 1" name="address1"
                                                value="{{ $cookie->address_1 }}">
                                            @error('exampleFormControlInput0')
                                                <small class="form-text text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput1" class="form-label">Address 2</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleFormControlInput1" placeholder="Address 2" name="address2"
                                                value="{{ $cookie ? $cookie->address_2 : '' }}">
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput1" class="form-label">Address 3</label>
                                            <input type="text" class="checkout-box2 form-control"
                                                id="exampleFormControlInput1" placeholder="Address 3" name="address3"
                                                value="{{ $cookie ? $cookie->address_3 : '' }}">
                                        </div>
                                        <div class="mb-3 cart-form1">
                                            <label for="exampleFormControlInput2" class="form-label">Postcode<small
                                                    class="text-danger">* </small> <span
                                                    class="form-text text-danger ajaxMessage d-inline"
                                                    style="display: none"></span></label>
                                            <input type="text"
                                                class="checkout-box2 form-control @error('exampleFormControlInput2') is-invalid @enderror"
                                                id="exampleFormControlInput2" placeholder="Postcode" name="postcode"
                                                value="{{ $cookie->postcode }}">


                                            @error('exampleFormControlInput2')
                                                <small class="form-text text-danger ">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="cart-edit-location">
                                            <div class="mb-3 cart-form3">
                                                <label for="stateChoices">State<small
                                                        class="text-danger">*</small></label>
                                                <select
                                                    class="form-select checkout-box3 @error('stateChoices') is-invalid @enderror"
                                                    aria-label="Default select example" id="stateChoices"
                                                    onchange="editAddressForm(this)" name="state">
                                                    <option selected disabled>Select...</option>
                                                    @foreach ($states as $state)
                                                        <option value="{{ $state->id }}"
                                                            {{ $cookie && $cookie->state_id == $state->id && !in_array($cookie->state_id,[10,11,15]) ? 'selected' : '' }}
                                                            {!! in_array($state->id,[10,11,15]) ? 'style="color:lightgrey;" disabled' : '' !!}>
                                                            {{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('stateChoices')
                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3 cart-form2">
                                                <label for="cityChoices">City<small
                                                        class="text-danger">*</small></label>
                                                <select
                                                    class="form-select checkout-box3 @error('cityChoices') is-invalid @enderror"
                                                    aria-label="Default select example" id="cityChoices" name="city">
                                                    <option selected disabled>Select...</option>
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->city_key }}"
                                                            {{ $cookie && $cookie->city_key == $city->city_key ? 'selected' : '' }}>
                                                            {{ $city->city_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('cityChoices')
                                                    <small class="form-text text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>
                                    <div class="edit-opt-btn modal-footer">
                                        <button type="button" class="cancel-btn btn btn-primary" data-bs-dismiss="modal"
                                            aria-label="Close">Back</button>
                                        <button type="button" class="savechg-btn btn-primary" id="cartAddress">Save
                                            Changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- Find Store Modal --}}
                    <div class="modal fade" id="findastore" aria-hidden="true" aria-labelledby="findastoreLabel"
                        tabindex="-1">
                        <div class="modal-dialog modal-xl edit-popup-bg">
                            <div class="modal-content edit-info-modal-box find-store-tittle">
                                <div class="modal-header top-findshop-tittle">
                                    <div class="s-title-box">
                                        <h1 class="modal-title edit-tittle2" id="findastoreLabel">Find A
                                            <span>Store</span>
                                        </h1>

                                        <div class="search-store">
                                            <input type="text" class="bg-form2 form-control" id="formGroupExampleInput"
                                                placeholder="Location">
                                            <a href="#" id="DeleteCartItem" class="link-product">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <!-- Store list -->
                                @include('shop.cart.findstorelist')

                            </div>
                        </div>
                    </div>
                    {{-- Confirm Address Modal --}}
                    <div class="modal fade" id="checkoutcart" aria-hidden="true" aria-labelledby="checkoutcartLabel"
                        tabindex="-1">
                        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box confirm-info">
                                {{-- <div class="modal-header">
                                    <h1 class="modal-title edit-tittle" id="checkoutcartLabel">Please Confirm The
                                        <span>Address Is Correct</span></h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div> --}}
                                <div class="modal-body">
                                    <!-- Ajax response loaded here -->
                                </div>
                                <div class="edit-opt-btn modal-footer">
                                    <button class="cancel-btn  btn btn-primary" data-bs-dismiss="modal"
                                        aria-label="Close">Back</button>
                                    <button class="savechg-btn btn-primary"
                                        onclick="window.location='{{ url('/payment/cashier') }}'">Proceed</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="checkError" aria-hidden="true" aria-labelledby="checkoutcartError"
                        tabindex="-1">
                        <div class="modal-dialog modal-md modal-dialog-centered edit-popup-bg">
                            <div class="modal-content edit-info-modal-box confirm-info">
                                <div class="modal-header">
                                    <h3 class="modal-title edit-tittle" id="checkoutcartError">Voucher Info</h3>

                                </div>
                                <div class="modal-body pt-0">
                                    <div class="row">
                                        <div class="col-12 pb-0 text-danger">
                                            <p class="m-0" id="vouchermsg" class="fs-6 ">text</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="edit-opt-btn modal-footer p-0">
                                    <button type="button" class="savechg-btn btn-primary" id="modalvoucher"
                                        data-bs-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Cart form --}}
                    <div class="checkout-page content">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="checkout-box">

                                    <div class="shopping-tittle">
                                        <p>Shopping Cart</p>
                                    </div>

                                    <div class="checkout-subtittle">
                                        <div class="checkout-tick">
                                            <input type="checkbox" name="selectAll" id="selectAll" /><label
                                                for="selectAll"></label>
                                        </div>
                                        <p>Select All</p>
                                        {{-- <span id="coupon_error" class="form-text text-danger small mx-auto" style="display: none">Error</span> --}}
                                        {{-- <p class="ml-5">Subtotal</p>
                                        <div class="loader-cart m-0" style="display:none;"></div>
                                        <p class="subLoader col-5">MYR <span
                                                class="cartSubtotalPrice">{{ number_format((array_sum(array_column($cartsItems,'subtotalPrice')) / 100) ,2) }}</span>
                                        {{ array_sum(array_column($cartsItems,'subtotalPrice')) != 0 && array_sum(array_column($cartsItems,'subtotalPoint')) != 0 ? '+' : '' }}
                                        <span
                                            class="cartSubtotalPoint">{{ array_sum(array_column($cartsItems,'subtotalPoint')) . ' pts' }}</span>

                                        </p> --}}

                                    </div>
                                    <div class="cart-scroll-spinner spinner-grow text-danger my-5 " role="status">
                                        <span class="sr-only"></span>
                                      </div>
                                    <div class="cart-scroll" style="display: none">

                                        @foreach ($cartsItems['items'] as $key => $cartsItem)
                                                <div class="checkout-cart active" id="itemBox{{ $key }}" style="display: {{$cartsItem['freegift'] == 1 ? 'none' : ''}}">
                                                    <div class="checkout-tick">
                                                        <input class="itemChecked disabled" type="checkbox"
                                                            name="rt[{{ $key }}]"
                                                            onclick="/*updateDatabase(this)*/"
                                                            id="rt[{{ $key }}]" data-process="status"
                                                            data-cart-id="{{ $key }}"
                                                            data-unit-price="{{ $cartsItem['price'] }}"
                                                            data-purchase-limit-eligible="{{ $cartsItem['purchase_limit']['eligible'] }}"
                                                            data-purchase-limit-amount="{{ $cartsItem['purchase_limit']['amount'] }}"
                                                            data-purchase-limit-purchase-left="{{ $cartsItem['purchase_limit']['purchase_left'] }}"
                                                            data-purchase-limit-period="{{ $cartsItem['purchase_limit']['period'] }}"
                                                            data-selfcollect-only="{{ $cartsItem['selfcollect-only'] }}"
                                                            {{ $cartsItem['status'] == '1' && in_array($cartsItem['disabled'], [0, 3, 9]) ? 'checked' : '' }}
                                                            {{ ($cartsItem['errorMessage'] == null && in_array($cartsItem['disabled'], [8])) || in_array($cartsItem['disabled'], [4]) || $cartsItem['direct_pay']['uuid'] ? 'disabled' : '' }} />
                                                        <label for="rt[{{ $key }}]"
                                                            style="{{ in_array($cartsItem['disabled'], [8, 4]) || $cartsItem['direct_pay']['uuid'] ? 'background: #d2d2d2' : '' }}"></label>
                                                    </div>
                                                    <div class="checkout-product">
                                                        <img class="checkout-p1"
                                                            src="{{ asset('/storage' . $cartsItem['images']) }}"
                                                            alt="">

                                                    </div>
                                                    <div class="checkout-text product-detail">
                                                        {{-- {{ $cartsItem['direct_pay']['uuid'] }}
                                                            {{ $cartsItem['direct_pay']['customer_details']['name'] }} --}}


                                                        <small
                                                            class="price-details-red2 errorMessage{{ $cartsItem['status'] }} {{ array_key_exists('product_rbs_frequency',$cartsItem['attributes']) ? 'rbsErrorMessage' : '' }}"
                                                            style="{{ $cartsItem['disabled'] == 9 ? 'display:none;' : '' }}">
                                                            {{-- @if ($globalMinPoint > $user->userInfo->user_points) Your points not reach minimum spent @else {{ $cartsItem['errorMessage'] }}
                                                    @endif --}}{{ $cartsItem['errorMessage'] }}
                                                        </small>

                                                        <p class="the-product-name"><a
                                                                href="{{ $cartsItem['link'] }}">{{ $cartsItem['name'] }} </a>
                                                        </p>
                                                        @if ($cartsItem['direct_pay']['uuid']) <small
                                                                class="m-0">Payment link created: <a
                                                                    href="{{ Request::root() }}/direct-pay/{{ $cartsItem['direct_pay']['uuid'] }}/?signature={{ csrf_token() }}"
                                                                    onclick="copyLink(event,{{ $key }})">{{ $cartsItem['direct_pay']['customer_details']['name'] }}</a>
                                                                <a id='copy-button' href='#'
                                                                    class='my-tool-tip-{{ $key }}'
                                                                    data-toggle="tooltip" data-placement="left"
                                                                    title="Link Copied!">
                                                                    <i class='glyphicon glyphicon-link'></i>
                                                                </a></small> @endif
                                                        {{-- <p class="m-0 p-0" style="line-height: 0px"></p> --}}
                                                        <p class="the-product-attribute">
                                                            {{ $cartsItem['attributes']['product_size']}}
                                                        </p>
                                                        <div class="">
                                                            <div class="loader-cart m-0 dd" style="display:none;"></div>
                                                            <span class=" itemSubtotalPrice{{ $key }}">{{ $cartsItem['pointCash'] != 7002 ? 'MYR ' . number_format(($cartsItem['price'] * $cartsItem['quantity']) / 100, 2) : '' }} </span>
                                                        </div>
                                                        @if (array_key_exists('product_rbs_frequency',$cartsItem['attributes']))
                                                           <span style="color:red;">*</span>
                                                           Deliver every {{ $cartsItem['attributes']['product_rbs_frequency'] }} month(s) for {{ $cartsItem['attributes']['product_rbs_times'] }} times.
                                                        @endif
                                                        @if ($cartsItem['selfcollect-only'] == 'true')
                                                        <span style="color:red;">*</span>
                                                        This product can self-pickup only.
                                                             @endif
                                                        </p>

                                                        <div class="product-quantity product-detail" id="product-quantity{{ $key }}">
                                                            <button class="product-minus-btn " type="button"
                                                                name="fml-minus-btn" onclick="updateDatabase(this)"
                                                                data-process="quantity"
                                                                data-cart-id="{{ $key }}"
                                                                {{ $cartsItem['direct_pay']['uuid'] ? 'disabled' : '' }}>
                                                                <i class="fa fa-minus" aria-hidden="true"></i>
                                                            </button>
                                                            <input class="quantityValue-{{ $key }}" type="text" name="quantity"
                                                                value="{{ $cartsItem['quantity'] }}" min="1"
                                                                onchange="updateDatabase(this)" data-process="quantity"
                                                                data-cart-id="{{ $key }}"
                                                                data-minimum="{{ array_key_exists('product_rbs_minimum',$cartsItem['attributes']) ? $cartsItem['attributes']['product_rbs_minimum'] : 0 }}"
                                                                id="inputChanges{{ $key }}"
                                                                {{ $cartsItem['direct_pay']['uuid'] ? 'readonly' : '' }}>
                                                            <button class="product-add-btn" type="button"
                                                                name="fml-add-btn" onclick="updateDatabase(this)"
                                                                data-process="quantity"
                                                                data-cart-id="{{ $key }}"
                                                                {{ $cartsItem['direct_pay']['uuid'] ? 'disabled' : '' }}>
                                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="icon-special icon-checkout">
                                                        <div class="icon-checkout-2">
                                                            <a id="DeleteCartItem[{{ $key }}]"
                                                                class="link-product dd" onclick="updateDatabase(this)"
                                                                data-process="delete"
                                                                data-cart-id="{{ $key }}">
                                                                <i class="fa fa-trash icon-chekout-1"
                                                                    aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                        {{-- @if ($cartsItem['direct_pay']['uuid'])
                                                        <div class="icon-edit-info mt-2">
                                                            <a id=""
                                                                class="link-product" onclick="updateDatabase(this)"
                                                                data-process="delete"
                                                                data-cart-id="{{ $key }}">
                                                                <i class="fa fa-copy icon-chekout-1"
                                                                    aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                        @endif --}}
                                                    </div>
                                                    {{-- <div class="check-bg">
                                                <img class="checkout-3dbg"
                                                    src="{{asset('/images/formula/cart-bg1.png')}}" alt="">
                                        </div> --}}
                                                </div>

                                        @endforeach

                                    </div>
                                </div>
                                <div class="checkout-bg"></div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="stick-window">
                                    <div class="checkout-box">
                                        <div class="order-box">
                                            <div class="shopping-tittle gg">
                                                <p>Order Summary</p>
                                            </div>
                                        </div>
                                        <div class="total-price">
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        {{-- {{ dd($cartsItems) }} --}}
                                                        <td class="price-details-1">Subtotal (Cash)</td>
                                                        <td class="price-details-2">MYR <span
                                                                class="cartSubtotalPrice" data-price="{{ array_sum(array_column($cartsItems['items'], 'subtotalPrice')) }}">{{ number_format(array_sum(array_column($cartsItems['items'], 'subtotalPrice')) / 100, 2) }}</span>
                                                        </td>
                                                    </tr>
                                                    {{-- <tr>
                                                        <td class="price-details-1">Subtotal (Points)</td>
                                                        <td class="price-details-2"> <span
                                                            class="cartSubtotalPoints">{{ number_format(array_sum(array_column($cartsItems,'subtotalPoint'))) }}
                                                pts</span>
                                                </td>
                                                </tr> --}}
                                                    <tr>
                                                        <td class="price-details-1">Shipping Fee</td>
                                                        <td class="price-details-2">MYR <span
                                                                class="cartShippingPrice">{{ number_format($cartsItems['shippingFee']['amount'] / 100, 2) }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="price-details-1">
                                                            Discount
                                                        </td>
                                                        <td class="price-details-2">
                                                            <span class="cartDiscountPrice
                                                            @if (($existingSession && array_key_exists('voucher',$existingSession) && count($existingSession['voucher']) >= 1)) d-none @endif" id="discount_tag" >
                                                            - MYR  {{ isset($cartsItems['discount']) ? number_format($cartsItems['discount']/ 100, 2) : '0.00' }}
                                                            </span>
                                                            {{-- <span class="cartDiscountPrice" id="discount_tag">
                                                                - MYR {{ number_format($cartsItems['discount'] / 100, 2) }}
                                                            </span> --}}
                                                        </td>
                                                    </tr>
                                                    <tr class="voucher-0 {{ $existingSession && array_key_exists('voucher',$existingSession)&& count($existingSession['voucher']) >= 1 ? '' : 'd-none' }}">
                                                        <td class="price-details-1 multipleVoucher px-0">
                                                            <span class="voucherCode-0">
                                                                {{ $existingSession && array_key_exists('voucher',$existingSession) && count($existingSession['voucher']) >= 1 ? substr_replace($existingSession['voucher'][0]['discount_code'],"-",4,0) : '' }}
                                                            </span>
                                                            <a onclick="clear_voucher(0)" class="multipleVoucher" style="cursor: pointer;text-decoration: underline;">
                                                                [Remove]
                                                            </a>
                                                        </td>
                                                        <td class="price-details-2 multipleVoucher">
                                                            - MYR
                                                            <span class="voucherAmount-0 ">
                                                                {{ $existingSession && $existingSession['voucher'][0]['discount_amount'] }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr class="voucher-1 {{ $existingSession && array_key_exists('voucher',$existingSession)&& count($existingSession['voucher']) == 2 ? '' : 'd-none' }}">
                                                        <td class="price-details-1 multipleVoucher px-0">
                                                            <span class="voucherCode-1">
                                                                {{ $existingSession && array_key_exists('voucher',$existingSession) && count($existingSession['voucher']) == 2 ? substr_replace($existingSession['voucher'][1]['discount_code'],"-",4,0) : '' }}
                                                            </span>
                                                            <a onclick="clear_voucher(1)" class="multipleVoucher" style="cursor: pointer;text-decoration: underline;">
                                                                [Remove]
                                                            </a>
                                                        </td>
                                                        <td class="price-details-2 multipleVoucher">
                                                            - MYR
                                                            <span class="voucherAmount-1">{{ $existingSession && array_key_exists('voucher',$existingSession) && count($existingSession['voucher']) == 2 ? $existingSession['voucher'][1]['discount_amount'] : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="price-details-1">Voucher</td>
                                                        <td class="price-details-2">
                                                            {{-- @dd($voucherLists) --}}


                                                            <form action="">
                                                                <div class="form-row coupon-box d-flex voucherSelect2">
                                                                    <div class="input-group col-md-11 ml-auto pr-md-2 d-flex align-items-right">
                                                                        <select class="form-control voucherField ff" id="coupon_code"
                                                                        @if ($existingSession && array_key_exists('voucher',$existingSession)
                                                                            && count($existingSession['voucher']) == 2 ) disabled="disabled"
                                                                            style="border:1px solid green"
                                                                        @endif>
                                                                            <option value="" selected>Voucher Code</option>
                                                                            {{-- @if (array_key_exists('code',$voucherLists)) --}}
                                                                            @foreach($voucherLists as $key => $voucherList)
                                                                                <option value="{{ $voucherList['discount_code'] }}">{{ $key . ' (RM'.$voucherList['discount_amount'].')' }}</option>
                                                                            @endforeach
                                                                            {{-- @endif --}}
                                                                        </select>
                                                                        {{-- <select name="coupon_code" placeholder="Voucher Code" id="coupon_code" class="select2 form-control voucherField ff">
                                                                            <option>Voucher Code</option>
                                                                            @if (array_key_exists('code',$voucherLists))
                                                                            @foreach($voucherLists['code'] as $voucherList)
                                                                            <option value="{{ $voucherList }}">
                                                                                {{ $voucherList }}</option>
                                                                            @endforeach
                                                                            @endif

                                                                        </select> --}}
                                                                        <input type="hidden" name="coupon_amount"
                                                                            id="coupon_amount" value="0">
                                                                        <input type="hidden" name="autoApply" id="autoApply" value="0">
                                                                        {{-- <input type="text" placeholder="Voucher Code"
                                                                            class="form-control voucherField ff"
                                                                            name="coupon_code" id="coupon_code"
                                                                            value="@if ($errors->any()) {{ $errors->first() }} @endif">
                                                                            <input type="hidden" name="coupon_amount"
                                                                            id="coupon_amount" value="0"> --}}

                                                                        <div class="input-group-append voucherButton">
                                                                            <button type="button" id="submitCoupon"
                                                                                name="voucher-point"
                                                                                class="btn btn-outline-secondary"
                                                                                @if ($existingSession && array_key_exists('voucher',$existingSession)&& count($existingSession['voucher']) == 2) style="background-color: red" @endif
                                                                                >
                                                                                <i aria-hidden="true"
                                                                                    class="fa fa-check pb-1"></i>
                                                                            </button>
                                                                        </div>
                                                                        {{-- <span class="cartDiscountPrice pl-3">MYR 0.00</span> --}}
                                                                    </div>

                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    {{-- <tr>
                                                        <td class="price-details-1">Voucher Code</td>
                                                        <td class="price-details-2">
                                                        </td>
                                                    </tr> --}}
                                                    {{-- <tr>
                                                        <td class="price-details-1">Available Points</td>
                                                        <td class="price-details-2"><span
                                                            class="cartDiscountPrice">{{ number_format($user->userInfo->user_points) }}</span>
                                                </td>
                                                </tr> --}}
                                                    <tr>
                                                        <td class="price-details-red1">Total </td>
                                                        <td class="price-details-red2">
                                                            <div class="spinner-border text-danger spinner-border-sm"
                                                                role="status" style="display:none;">
                                                            </div>
                                                            <span class="subLoader finalSubtotalPrice"
                                                                data-price="{{ $cartsItems['finalTotal'] }}"
                                                                data-shipping="{{ $cartsItems['shippingFee']['amount'] }}">{{ $cartsItems['finalTotal'] >0 ? 'MYR ' . number_format($cartsItems['finalTotal'] / 100, 2) : 'MYR 0.00' }}</span>
                                                            {{-- <span class="subLoader plusIcon">{{ ($cartsItems['finalTotal'] != 0 && $cartsItems['finalTotalPoint'] != 0) || ($cartsItems['finalTotal'] == 0 && $cartsItems['finalTotalPoint'] == 0) ? ' + ' : ''  }}</span>
                                                        <span
                                                            class="subLoader finalSubtotalPoint">{{ ($cartsItems['finalTotal'] == 0 && $cartsItems['finalTotalPoint'] != 0) || ($cartsItems['finalTotal'] == 0 && $cartsItems['finalTotalPoint'] == 0) ? $cartsItems['finalTotalPoint'] . ' pts' : '' }}</span> --}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="checkout-box checkout-box-space">
                                        <div class="ship-box">
                                            <div class="shopping-tittle textbox-width">
                                                <p>Ship To</p>
                                            </div>
                                            <div class="icon-btn">
                                                <div class="icon-edit-info i-edit-mt">
                                                    <a href="#address_edit_form" id="DeleteCartItem"
                                                        data-bs-toggle="modal" role="button" class="link-product">
                                                        <i class="fa fa-pencil-square-o icon-edit"
                                                            aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="total-price">
                                            <div class="info-checkout">
                                                <p>Name</p>
                                                <div class="bg-form">
                                                    <p class="">{{ $cookie ? $cookie->name : '' }}</p>
                                                </div>
                                            </div>
                                            <div class="info-checkout">
                                                <p>Contact (Mobile)</p>
                                                <div class="bg-form">
                                                    <p class="">{{ $cookie ? $cookie->mobile : '' }}</p>
                                                </div>

                                            </div>
                                            <div class="info-checkout">
                                                <p>Shipping Address</p>
                                                <div class="bg-form ">
                                                    <p class="">
                                                        {{ $cookie->address_1 ?? '' }}
                                                        {{ $cookie->address_2 ?? '' }}
                                                        {{ $cookie->address_3 ?? '' }}
                                                        {{ $cookie->postcode ?? '' }}
                                                        {{ $cookie->city_name->city_name ?? '' }}
                                                        {{ $cookie && $cookie->state_name ? $cookie->state_name->name : '' }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="checkout-box checkout-box-space delivery-tittle">
                                        <div class="delivery-box">
                                            <div class="shopping-tittle">
                                                <p>Delivery Method</p>
                                            </div>
                                        </div>
                                        <div class="delivery-choice">
                                            <ul class="nav nav-pills mb-3 delivery-option" id="pills-tab"
                                                role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link" id="delivery" data-toggle="pill"
                                                        href="#choose-delivery" role="tab"
                                                        aria-controls="choose-delivery" aria-selected="true">
                                                        <div class="red-dot " value="delivery"></div>
                                                        <img class="icon-size"
                                                            src="{{ asset('/images/icons/side03.png') }}" alt="">
                                                        <p>Delivery By Company</p>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link" id="Self-pickup" data-toggle="pill"
                                                        href="#choose-selfpick" role="tab"
                                                        aria-controls="choose-selfpick" aria-selected="false">
                                                        <div class="red-dot"></div>
                                                        <img class="icon-size "
                                                            src="{{ asset('/images/icons/side01.png') }}" alt="">
                                                        <p>Self-pickup</p>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="pills-tabContent">
                                                <div class="tab-pane fade" id="choose-delivery"
                                                    role="tabpanel" aria-labelledby="choose-delivery-tab">
                                                    <div class="find-store">
                                                        <a id="checkout-button disableClick"
                                                            data-bs-toggle="modal" role="button"
                                                            class="link-product checkout-btn action">Checkout
                                                            delivery</a>

                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="choose-selfpick" role="tabpanel"
                                                    aria-labelledby="Self-pickup">
                                                    <div class="find-store">
                                                        <label for="formGroupExampleInput">Find A Store</label>
                                                        <div class="voucher-box">
                                                            <input type="text" class="bg-form2 form-control storeName"
                                                                id="formGroupExampleInput" placeholder="Location"
                                                                value="">
                                                            <div class="sc-search">
                                                                <a href="#findastore" id="DeleteCartItem"
                                                                    class="link-product" data-bs-toggle="modal"
                                                                    role="button">
                                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="checkout-tick">
                                                            <div id="showSelectedAddress">
                                                            </div>
                                                            <div>
                                                                <a data-bs-toggle="modal"
                                                                    role="button"
                                                                    class="link-product checkout-btn action">Checkout
                                                                    Self-pickup</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="purchaseLimit" aria-hidden="true" aria-labelledby="purchaseLimit" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered edit-popup-bg">
        <div class="modal-content edit-info-modal-box">
            <div class="modal-header">
                <h1 class="modal-title edit-tittle" id="limitProduct"><span>Warning</span></h1>
                <button type="button" class="btn-close purchase-limit-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body limit-over purchase-limit">
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
<style>
    .multipleVoucher{
        color:#252525;
        font-size: 11px;
    }

    .link-product {
        cursor: pointer;
    }

    .paymentDisabled {
        filter: grayscale(100%);
        background-color: grey;
        opacity: .5;
        border: grey;
    }

    .textDisabled {
        filter: grayscale(100%);
        /* background-color: lightgrey; */
        opacity: .5;

        border: grey;
    }

    .btn.btn-outline-secondary:hover {
        color: red;
    }

    .disableClick {
        pointer-events: none;
    }

    .loader-cart {
        border: 5px solid #fff;
        border-radius: 50%;
        border-top: 5px solid #ef3842;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    .purchase-limit p {
        text-align: justify;
    }
    .purchase-limit p .purchase-limit-red {
        font-weight: bold;
        color: #ef3842;
    }
    .purchase-limit p .purchase-limit-red2 {
        font-weight: bold;
        color: #ef3842;
        display: inline-block
    }
    .purchase-limit p .purchase-limit-bold {
        font-weight: bold;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    .product-detail p a {
        color: inherit;
        text-decoration: inherit;
    }

    /* Small devices (landscape phones, 576px and up) */
    @media (max-width: 576px) {

        /*  */
        .price-details-1 {
            width: 110px;
        }

        .voucherField {
            border-radius: 10px 0px 0px 10px;
            margin-left: -40px;
            height: 10px;
            height: 23px;
            padding: 2px;
            width: 89%
            padding-top:0;
            padding-bottom: 0;
        }

        .voucherButton {
            /* background-color: rgb(239, 56, 66); */
            /* height: 73%; */
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }
    }

    /* Medium devices (tablets, 768px and up) */
    @media (min-width: 768px) {

        /*  */
        .price-details-1 {
            width: 110px;
        }

        .voucherField {
            border-radius: 10px 0px 0px 10px;
            margin-left: -30px;
            height: 10px;
            height: 23px;
            width: 89%

        }
        select.form-control.voucherField.ff{
            padding-top:0;
            padding-bottom: 0;
        }

        .voucherButton {
            /* background-color: rgb(239, 56, 66); */
            /* height: 73%; */
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }
    }

    /* Large devices (desktops, 992px and up) */
    @media (min-width: 992px) {
        /*  */
    }

    /* Extra large devices (large desktops, 1200px and up) */
    @media (min-width: 1200px) {
        /*  */
    }

</style>
@endpush

@push('script')
<script>
    //cart total show on top menu
    let getAllQlyTot = '';
    let cartTotValue = 0;
    let delNum = 0;
    let empty = 1;
    let numQ = 0;
    let $messages = 'RBS can only be purchased individually'

    function getVuluetoProductQly(gg) {
        getAllQlyTot = gg;
        ////console.log(getAllQlyTot);
    };

    // free gift disable default popup
    localStorage.setItem('fg_popup', 0);

    const getQltcartpage = document.querySelectorAll('.product-quantity input');
    const checkoutBtn = document.querySelectorAll('.link-product.checkout-btn.action');

    let popupData = []

    //check cart quantity check if over limit
     getQltcartpage.forEach(j =>{
        //console.log(j);
        numQ = Number(j.value);

        let item = $(j).parent().parent().parent().find('.itemChecked')
        let itemName = $(j).parent().parent().find('.the-product-name').text()
        let itemAttribute = $(j).parent().parent().find('.the-product-attribute').text().trim()
        let eligibleLimit = item.data('purchase-limit-eligible')
        let limitAmount = item.data('purchase-limit-amount')
        let limitPeriod = item.data('purchase-limit-period')
        let purchaseLeft = item.data('purchase-limit-purchase-left')
        if (purchaseLeft < 0) {
            purchaseLeft = 0
        }

        let inputQ = $(j).closest('div').find('input').val()
        console.log('inputQ', inputQ);
        console.log('purchaseLeft', purchaseLeft);

        if(numQ > 50){
            popFuncOverLimit();
            j.setAttribute('value',50);
            $(".product-add-btn").click();
            addTocartQly();
            //location.reload();
        }else if(eligibleLimit && numQ > purchaseLeft) {
            j.setAttribute('value', purchaseLeft)
            let itemData = {
                name: itemName,
                attribute: itemAttribute,
                amount: limitAmount,
                period: limitPeriod,
                purchaseLeft: purchaseLeft
            };
            popupData.push(itemData)
            $(".product-add-btn").click()
        }
    });
    console.log('popupData', popupData);
    if (popupData.length) {
        popFuncPurchaseLimit(popupData)
    }


    //cart total show on top menu end

    checkoutBtn.forEach(item => {
        item.addEventListener('click', function() {

            checkOutModalUrl = "{{ route('shop.cart.check-out-modal') }}";

            let id = $(this).data('price-id');

            checkOutModalUrl = checkOutModalUrl.replace('id', id);

            $.ajax({
                async: true,
                beforeSend: function() {},
                complete: function() {},
                url: checkOutModalUrl,
                type: 'GET',
                success: function(response) {
                    $('#checkoutcart .modal-body').empty();

                    // alert(response);
                    if (response == 0) {
                        $('#findastore').modal('show');
                    } else {
                        $('#checkoutcart .modal-body').html(response);

                        if ($(checkoutBtn).hasClass('rbsError')) {
                            $('#checkError').modal('show');
                            $("#vouchermsg").html($messages);
                        } else {
                        // Display Modal
                        $('#checkoutcart').modal('show');
                        }


                        $('.select2-edit').select2({
                            dropdownParent: $('#checkoutcart')
                        });
                    }
                    // Change modal title
                    // $('#checkoutcart .modal-title').text('Checkout')

                    // Add response in Modal body


                },
                error: function(response) {
                    toastr.error('Sorry! Something went wrong.', response.responseJSON
                        .message);
                }
            });

        })
    })

    $(document).ready(function() {

        if ($('#coupon_code').val() != '' && $('#coupon_code').val() != null) {
            var couponCodeValue = $('#coupon_code').val();
            var $voucherCodeAddress = couponCodeValue.substring(1, 5) + '-' + couponCodeValue.substring(5, 10)
            $('#coupon_code').val($voucherCodeAddress);
            $('#submitCoupon').trigger('click');
            get_coupon();
        }
        if ($('.itemChecked:checked').length == 0) {
            $('.link-product.checkout-btn').addClass('disableClick');
            $('.checkoutselfcollect').addClass('disableClick');
        }
        $('#findastore').modal({
            backdrop: 'static',
            keyboard: false
        });

        checkSelfCollectOnly();
        getAutoApplyStatus();
        onPageload();

        // Finish Loading
        // Hide spinner - show cart
        $('.cart-scroll-spinner').hide();
        $('.cart-scroll').show();

        $('.purchase-limit-close').click(function() {
            $('#purchaseLimit').modal('hide');
        });
    });

    $('#selectAll').click(async function() {
        const checked = this.checked;
        // // //console.log(this.checked);
        empty = 1;
        $('.itemChecked').each(async function() {
            if (!$(this).is(':checked')) {
                await updateDatabase(this);
            }
            this.checked = checked;
        });

        if (!$('#selectAll').is(':checked')) {
            // // //console.log($(this).is(':checked'));
            $('.itemChecked').each(async function() {
                // for (let i = 0; i < $('.itemChecked').length; i++) {
                //     await updateDatabase($('.itemChecked')[0]);
                //     await updateDatabase($('.itemChecked')[1]);
                empty = 0;
                await updateDatabase(this);

                // // //console.log('looping')
                // }
            });
        }
    });

    $('.itemChecked').click(function() {
        setCheckAll();
        empty = 1;
        updateDatabase(this);
    });

    // function changePayment(data){
    //     // //console.log(data.value );
    // }

    function editAddressForm(select) {
        var stateID = $(select).val();

        $.ajax({
            url: '/shop/cart/get-city/' + stateID,
            type: 'GET',
            data: {
                // "_token": "{{ csrf_token() }}",
                "id": stateID
            },
            success: function(html) {
                $('#cityChoices').html(html)
            },
            error: function() {
                alert("There's error. Please reload page");
            }

        });
    }

    function getAutoApplyStatus() {
        $.ajax({
            async: true,
            beforeSend: function() {},
            complete: function() {},
            url: "/web/cart?autoApply=1",
            type: "GET",
            success: function(d) {
                if(d.total > 0) {
                    $("#coupon_code").attr('disabled', 'disabled');
                }
                else {
                    $("#coupon_code").removeAttr('disabled');
                }
            },
            error: function(result) {

            }
        });
    }

    async function updateDatabase(items) {
        var cartID = $(items).data("cart-id");
        var process = $(items).data("process");
        var itemBox = $('#itemBox' + cartID);
        var loader = $('.spinner-border');
        var subLoader = $('.subLoader');
        var couponamount = $('#coupon_amount').val();
        var inputQuantity = null;
        var paymentType = null;
        var emptys = empty;

        let item = $(items).parent().parent().parent().find('.itemChecked')
        let itemName = $(items).parent().parent().parent().find('.the-product-name').text()
        let itemAttribute = $(items).parent().parent().parent().find('.the-product-attribute').text().trim()
        let eligibleLimit = item.data('purchase-limit-eligible')
        let limitAmount = item.data('purchase-limit-amount')
        let limitPeriod = item.data('purchase-limit-period')
        let purchaseLeft = item.data('purchase-limit-purchase-left')

        if (purchaseLeft < 0) {
            purchaseLeft = 0
        }

        if (couponamount != 0) {
            remove_coupon(0);
        }
        if (process == 'pointCash') {
            paymentType = items.value;
        }

        if (process == 'quantity') {
            setTimeout(function() {

                inputQuantity = $(items).closest('div').find('input').val();
                if(inputQuantity > 50){
                    // $(".product-add-btn").click();
                    $('.product-add-btn').trigger('click', [true])
                }
                //console.log(inputQuantity);
                if(eligibleLimit && inputQuantity > purchaseLeft) {
                    inputQuantity = purchaseLeft
                    $(items).closest('div').find('input').val(purchaseLeft)
                    let itemData = {
                        name: itemName,
                        attribute: itemAttribute,
                        amount: limitAmount,
                        period: limitPeriod,
                        purchaseLeft: purchaseLeft
                    };
                    let popupData = [itemData];
                    popFuncPurchaseLimit(popupData);
                }

            }, 500);
        }
        setTimeout(function() {
            // $('#autoApply').val(0)
            $.ajax({
                url: '/shop/cart/update-cart',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": cartID,
                    "process": process,
                    "quantity": inputQuantity,
                    "payment_type": paymentType,
                    "emptys": emptys
                },
                beforeSend: function() {
                    $('.link-product.checkout-btn').addClass('disableClick');
                    subLoader.hide();
                    loader.show();

                    if (process == 'delete') {
                        itemBox.attr('style', 'opacity:0.3');
                    }
                    if ($('#coupon_code').val() != '') {
                        remove_coupon(0);
                    }

                },
                success: function(success) {
                    cartTotValue = cartTotValue;
                    loader.hide();
                    subLoader.show();

                    console.log(success);

                    if (process == 'delete') {
                        // itemBox.hide(); change to remove
                        itemBox.remove();

                        // add cart total quantity on top menu
                        cartTotValue = 0;
                        getQltcartpage.forEach(v =>{
                            //delNum = 0;
                            const h = Number(v.value);
                            cartTotValue += h;

                            if($(v).data("cart-id") === cartID){
                                delNum = delNum + Number(v.value);

                                cartTotValue = cartTotValue - delNum;

                                //console.log(cartTotValue + 'this is delNum value ' + delNum);
                            }
                            //console.log('sucess delete leave total ' + cartTotValue);
                            $("#cart-quantity").html(cartTotValue);
                        })
                        cartTotValue = 0;
                        // add cart total quantity on top menu end

                        //When product delete reload the cart number
                    }

                    if (process == 'status') {

                        let smallBox = itemBox.find('.price-details-red2');

                            smallBox.toggleClass('errorMessage0 errorMessage1');


                        if (success.rbs !== undefined && success.rbs.length === 0) {
                            $(smallBox).hide();
                            $('#Self-pickup').attr('class','nav-link');
                        } else {
                            $('#Self-pickup').attr('class','nav-link paymentDisabled disabled');

                            if (success.rbs !== undefined && (success.rbs.length >= 1 && success.normal.length >= 1 || success.rbs.length > 1)) {

                                $.each(success.rbs, function(index, value) {
                                    $('#checkError').modal('show');
                                    $("#vouchermsg").html($messages);

                                    $('#itemBox' + value).find('.price-details-red2').html($messages)
                                    $('#itemBox' + value).find('.price-details-red2').show();

                                });

                            } else {

                                $.each(success.rbs, function(index, value) {

                                    $('#itemBox' + value).find('.price-details-red2').html('');
                                    $('#itemBox' + value).find('.price-details-red2').hide();
                                    $('.rbsErrorMessage').hide();

                                });

                            }
                        }
                    }

                    if (process == 'quantity') {
                        //
                        addTocartQly();
                        if (success.quantityError !== null) {
                            $('#checkError').modal('show');
                            // $("#coupon_error").html("Coupon was remove because there are changes on cart");
                            $("#vouchermsg").html('Minimum quantity is '+success.quantityError);
                            // itemBox.find('.price-details-red2').show().html('Minimum quantity is '+success.quantityError);
                            itemBox.find('.itemChecked').attr('checked',false);
                            // itemBox.find('.quantityValue').addAttr('disabled');
                            // let qtyDiv = itemBox.find('#product-quantity');
                            $('.quantityValue-'+cartID).val(success.quantityError);
                            // console.log('quantityValue',itemBox.find('.quantityValue').val());

                            // $( "#inputChanges"+cartID ).load(window.location.href + " #inputChanges"+cartID );

                            // $('#product-quantity'+cartID).load('#product-quantity'+cartID);
                            // $(itemBox.find('.product-quantity')).load(itemBox.find('.product-quantity'));
                        } else{
                            itemBox.find('.price-details-red2').hide();
                            // console.log('quantityValue',itemBox.find('.quantityValue').val());
                            // $('.quantityValue').val(success.quantityError);
                            // itemBox.find('.quantityValue').removeAttr('disabled');
                        }
                    }

                    $('.link-product.checkout-btn').removeClass('rbsError');

                    if (success.finalTotal === 0 ) {
                        $('.link-product.checkout-btn').addClass('disableClick');
                    } else {
                        if(success.rbs.length >= 1 && success.normal.length >= 1 || success.rbs.length > 1){
                            $('.link-product.checkout-btn').addClass('rbsError');
                        }
                        $('.link-product.checkout-btn').removeClass('disableClick');
                    }

                    $('.itemSubtotalPrice' + cartID).html('MYR ' + (success.itemSubtotalPrice / 100).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $('.finalSubtotalPrice').html('MYR ' + (success.finalTotal / 100).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $('.finalSubtotalPrice').attr('data-price', success.finalTotal);
                    $('.itemSubtotalPoint' + cartID).html(success.itemSubtotalPoint + ' pts');
                    $('.finalSubtotalPoint').html(success.finalTotalPoint + ' pts');
                    $('.cartSubtotalPrice').html((success.subtotalCash / 100).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                    $('.cartSubtotalPrice').attr('data-price', success.subtotalCash);
                    $('.cartShippingPrice').html((success.shippingFee / 100).toFixed(2));
                    $('.cartSubtotalPoints').html((success.subtotalPoint));
                    $('.cartDiscountPrice').html('- MYR ' + (success.discount / 100).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                    var autoApply = $('#autoApply').val();

                    if(success.autoApply == 1) {
                        $('#autoApply').val((autoApply / 100).toFixed(2));
                        $("#coupon_code").attr('disabled', 'disabled');
                    }
                    else {
                        $("#coupon_code").removeAttr('disabled');
                    }

                    $('#submitCoupon').trigger('click');

                    checkSelfCollectOnly();

                },
                error: function() {
                    alert("error in loading");
                    //location.reload();
                }

            });
        }, 500);
    };
    // Form Validation
    $("#cart-form").validate({
        rules: {
            name: {
                required: true
            },
            phone: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 15
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            address1: {
                required: true,
                minlength: 3
            },
            postcode: {
                required: true,
                digits: true,
                minlength: 5,
                maxlength: 6
                // postcode: true
            }
        },
        messages: {
            name: {
                required: "Please enter a full name.",
                email: "Wrong name."
            },
            phone: {
                required: "Please enter your mobile number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            },
            state: {
                required: "Please select your state"
            },
            city: {
                required: "Please select your city"
            },
            address1: {
                required: "Please enter your address"
            },
            postcode: {
                required: "Please enter your postcode"
            }
        }
    });

    // Validate form before submit.
    $('#cart-form').on('submit', function(e) {
        let error = 0;
        // // //console.log('submit');

        if (
            $("#cart-form").validate().element('#exampleInputEmail0')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#cart-form").validate().element('#exampleInputEmail1')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#cart-form").validate().element('#stateChoices')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (

            $("#cart-form").validate().element('#cityChoices')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (

            $("#cart-form").validate().element('#exampleFormControlInput0')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if ($("#cart-form").validate().element('#exampleFormControlInput2')) {

            error = error;
        } else {
            error = error + 1;
        }
    });

    $('#cartAddress').on('click', function(e) {
        var $postcode = $('#exampleFormControlInput2').val();
        // if ($postcode != '') {
        //         $.ajax({
        //         url: '/shop/cart/get-postcode',
        //         type: 'POST',
        //         data: {
        //             "_token": "{{ csrf_token() }}",
        //             "postcode": $postcode
        //         },
        //         beforeSend: function() {},
        //         success: function(success) {
        //             // //console.log(success.data);
        //             // //console.log(success.data.state_code);
        //             if (success.data == false) {
        //                 // $('#exampleFormControlInput2').attr('style','border:red;')
        //                 // alert('false');
        //                 $('.ajaxMessage').show().html('Invalid Postcode');
        //                     //console.log('return false');

        //                 return false;
        //             } else {
        //                 if (success.data.state_code == 'SRW' || success.data.state_code == 'SBH' || success.data.state_code == 'LBN') {
        //                     $('.ajaxMessage').show().html('Temporary no courier service to East Msia.');
        //                     return false;
        //                 } else {
        //                     // //console.log(success.data.state_code);
                            // $('.ajaxMessage').hide().html('');
                            $('#cart-form').submit();
        //                 }
        //             }

        //         },
        //         error: function() {
        //             alert("error in loading");
        //         }
        //     });
        // }

        //
    });

    // End Form Validation
    function onPageload() {
        setCheckAll();
        // //console.log('onload');
        // //console.log($('.itemChecked:checked').length == 0);

        const name = document.getElementById("exampleInputEmail0");
        const phone = document.getElementById("exampleInputEmail1");
        const state = document.getElementById("stateChoices");
        const city = document.getElementById("cityChoices");
        const address1 = document.getElementById("exampleFormControlInput0");
        const postcode = document.getElementById("exampleFormControlInput2");
        //const total = document.getElementById("finalSubtotalPrice");
        const total = document.querySelector(".finalSubtotalPrice");
        // var disablecheckout = $('.disableClick');
        // // //console.log('total.value');

        if (name.value == '' || phone.value == '' || state.value == '' || city.value == '' || address1.value == '' || postcode.value == '' || total.textContent == 'MYR 0.00') {
            $('.link-product.checkout-btn').addClass('disableClick');
        } else {
            // disablecheckout.hide();
            $('.link-product.checkout-btn').removeClass('disableClick');
            return true;
        }


    };

    function setCheckAll() {
        document.querySelector('#selectAll').checked = $('.itemChecked').length === $('.itemChecked:checked').length;
    }

    // check voucher
    $('#submitCoupon').click(function() {
        let text = $(this).children('i');
        let coupon = $("#coupon_code").val();
        // $("#coupon_error").hide();

      // console.log(text,coupon);

        if (coupon) {

            if ($(this).children('i').hasClass('fa-times')) {
                remove_coupon(1);
            $(this).children('i').toggleClass('fa-check fa-times');

                // $("#coupon_code").removeAttr('readonly');
                // onPageLoad();
            } else {
                get_coupon();

            }


            // $("#coupon_code").attr('readonly');

        }
    });

    function calculateTotalWithVoucher(operator,result){
        let voucherTotal = parseFloat($("#coupon_amount").val());

        if (operator === 'plus') {
            voucherTotal += parseFloat(result.coupon_amount)*100;
        } else {
            voucherTotal -= parseFloat(result.discount_amount)*100;
        }

        return voucherTotal.toFixed();
    }

    function calculateTotal(result){

        let cartTotal = $('.cartSubtotalPrice').attr('data-price');
        let shippingTotal = $('.finalSubtotalPrice').attr('data-shipping');

        newTotal = (parseInt(cartTotal) - voucherTotal);

        // console.log('newTotal',newTotal,newTotal <= 0);

        // less amount
        if (newTotal <= 0) {
            newTotal = parseInt(shippingTotal);
            $('#checkError').modal('show');
            $("#vouchermsg").html("Any unused balance will be burnt after checkout");
            $("#coupon_error").html("Any unused balance will be burnt after checkout");
            $("#coupon_code").addClass('is-invalid');
            $("#coupon_error").show();
            // $(".finalSubtotalPrice").html('MYR 0.00');
        } else {
            newTotal = newTotal + parseInt(shippingTotal);
        }

        $(".finalSubtotalPrice").html('MYR ' + (newTotal / 100).toFixed(2));

        if (result.coupon_amount <= 0) {
            $('.finalSubtotalPrice').attr('data-price', newTotal);
        }

    }

    function remove_coupon(type = 1) {
        $("#coupon_code").removeAttr('disabled');
        $("#submitCoupon").removeAttr('disabled');

        $("#coupon_code").removeClass('is-valid');
        $("#coupon_code").removeClass('is-invalid');

        $("#coupon_code").attr('style', 'border:solid 1px lightgrey;');

        $("#submitCoupon").attr('style', 'background-color: #263A8B');

        // $("#proceed-to-checkout-button").removeAttr('disabled');
        $("#coupon_code").val("");
        // $("#coupon_amount").val(0);
        // $("#discount_tag").html('- MYR 0.00');
        // $("#coupon_error").hide();

        $('.link-product.checkout-btn').removeClass('disableClick');

        // $('#submitCoupon').children('i').addClass('fa-check');


        if (type == 0) {
            clear_voucher();
            $('#checkError').modal('show');
            // $("#coupon_error").html("Coupon was remove because there are changes on cart");
            $("#vouchermsg").html("Coupon was removed because there were changes on cart");
            $("#modalvoucher").attr('onclick', 'location.reload();');

            // $("#coupon_error").show();
            $('#submitCoupon').children('i').toggleClass('fa-check fa-times');
            // setTimeout(function () {
            // location.reload();
            // }, 1000);
            // location.reload();


        }

    }

    function clear_voucher(key = null) {
        // $("#coupon_code").attr('style', 'border:solid 1px lightgrey;');
        // $("#submitCoupon").attr('style', 'background-color: #263A8B');

        $.ajax({
            async: true,
            beforeSend: function() {},
            complete: function() {},
            url: "/shop/cart/clear-voucher/"+key,
            type: "GET",
            success: function(result) {

                voucherTotal = calculateTotalWithVoucher('minus',result);

                //console.log('clear_voucher',result,voucherTotal);


                calculateTotal(result);

                $('.voucher-'+key).addClass('d-none');
                $("#coupon_code").removeAttr('disabled');
                $("#submitCoupon").removeAttr('disabled');

                $("#coupon_code").removeAttr('style');
                $("#submitCoupon").removeAttr('style');

                $("#coupon_amount").val(voucherTotal);

            },
            error: function(result) {

            }
        });

        // location.reload();
    }

    function get_coupon() {

        let originalCoupon = coupon = $("#coupon_code").val();
        // console.log('originalCoupon',originalCoupon);
        coupon = coupon.split('-').join('');

        if (coupon == '' || coupon == 0) {
            coupon = '0';
        }

        //console.log('coupon_code', coupon);


        // // //console.log(coupon);

        var loader = $('.spinner-border');
        var subLoader = $('.subLoader');
        var cartTotal = $('.finalSubtotalPrice').attr('data-price');
        var shippingTotal = $('.finalSubtotalPrice').attr('data-shipping');
        var newTotal = 0;


        if ($('.voucher-0').hasClass('d-none')) {
            var counter = 0;
        } else{
            var counter = 1;
        }
           // console.log(coupon,counter);

        $.ajax({
            async: true,
            beforeSend: function() {
                // Show loading spinner.
                // ItemContainer.hide();
                subLoader.hide();
                loader.show();
            },
            complete: function() {
                // Hide loading spinner.
                // ItemContainer.show();
                subLoader.show();
                loader.hide();
            },
            url: "/web/cart/getCoupon/" + coupon + "/" + counter,
            type: "GET",
            success: function(result) {
                if (result.status == 'error') {

                    $('#checkError').modal('show');
                    $("#vouchermsg").html(result.error);
                    $("#submitCoupon").attr('style', 'background-color: red');
                    $("#submitCoupon").children('i').toggleClass('fa-check fa-times');
                    $('.link-product.checkout-btn').addClass('disableClick');
                    $("#coupon_code").addClass('is-invalid');
                    $(".errorMessage1").show();

                } else {

                    voucherTotal = calculateTotalWithVoucher('plus',result);

                    calculateTotal(result);

                    let exceed_amount = result.coupon_amount * result.times_exceed;
                    let currency = 'RM';

                    if (result.coupon_amount != 0) {
                        $("#coupon_code").val('');
                    }

                    if ($('.voucher-0').hasClass('d-none')) {
                        $('.voucher-0').removeClass('d-none');
                        $("#discount_tag").addClass('d-none');
                        $('.voucherCode-0').html(originalCoupon);
                        $('.voucherAmount-0').html(result.coupon_amount);
                    } else{
                        $('.voucher-1').removeClass('d-none');
                        $('.voucherCode-1').html(originalCoupon);
                        $('.voucherAmount-1').html(result.coupon_amount);
                    }

                    if (!$('.voucher-0').hasClass('d-none') && !$('.voucher-1').hasClass('d-none')) {
                        $("#coupon_code").attr('disabled', 'disabled');
                        $("#submitCoupon").attr('disabled', 'disabled');
                        $("#coupon_code").attr('style', 'border:1px solid green');
                        $("#submitCoupon").attr('style', 'background-color: red');
                    }

                    $("#coupon_amount").val(voucherTotal);

                }
            },
            error: function(result) {
                // Log into console if there's an error.
                // // //console.log(result.status + ' ' + result.statusText);
            }
        });

    }

    function copyLink(obj, id) {
        obj.preventDefault();
        navigator.clipboard.writeText(obj.target.getAttribute('href')).then(() => {
            /* clipboard successfully set */
            $("a.my-tool-tip-"+id).attr('data-original-title', 'Link Copied');
            $("a.my-tool-tip-"+id).tooltip('show');
            setTimeout(function(){ $("a.my-tool-tip-"+id).tooltip('hide'); }, 1000);
        });
                /* clipboard write failed */
    };

    // add to cart show quantity functions
    function addTocartQly() {
        cartTotValue = 0;
        getQltcartpage.forEach(v =>{
            const h = Number(v.value);
            cartTotValue += h ;
            //console.log(cartTotValue + ' , this is i value ' + delNum);
        })
        if(delNum > 0){
            cartTotValue = cartTotValue - delNum;
            //console.log('yes ' + delNum , typeof delNum , typeof cartTotValue);
            } else {
                //console.log('no ' + delNum);
            }

        $("#cart-quantity").html(cartTotValue);
        ////console.log($("#cart-quantity").html(cartTotValue));
    };
    // add to cart show quantity functions end

    //check cart item limit over 50
    function popFuncOverLimit() {
        const limitOver = document.createElement('div');
        const mainBodyget = document.querySelector('#body-content-collapse-sidebar');
        limitOver.innerHTML = `
        <div class="modal fade" id="limititemModal" aria-hidden="true" aria-labelledby="limititemModal" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered edit-popup-bg">
                <div class="modal-content edit-info-modal-box">
                    <div class="modal-header">
                        <h1 class="modal-title edit-tittle" id="limititemModal"><span>Warning</span></h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="font-family: 'slick';font-size: 34px;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body limit-over">
                        <p>Each person limit to 50 units</p>
                    </div>
                </div>
            </div>
        </div>
        `;
        mainBodyget.appendChild(limitOver);
        ////console.log('item over');
        $('#limititemModal').modal('show')
    }
     //check cart item limit over 50 end

    //purchase limit popup
    function popFuncPurchaseLimit2(name, attribute, amount, period, purchaseLeft) {
        const purchaseOver = document.createElement('div');
        const mainBodyget = document.querySelector('#body-content-collapse-sidebar');
        purchaseOver.innerHTML = `
        <div class="modal fade" id="purchaseLimit" aria-hidden="true" aria-labelledby="purchaseLimit" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered edit-popup-bg">
                <div class="modal-content edit-info-modal-box">
                    <div class="modal-header">
                        <h1 class="modal-title edit-tittle" id="limitProduct"><span>Warning</span></h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body limit-over purchase-limit">
                        <ul>
                            <li>
                                <p>
                                    Purchase limit for <span class="purchase-limit-red"> ${name} </span> <span class="purchase-limit-red2">(${attribute}) </span>
                                    ${period} is <span class="purchase-limit-bold">${amount}</span> unit(s).
                                    You have a remaining balance of <span class="purchase-limit-bold">${purchaseLeft}</span> unit(s) for purchase.
                                    Contact your agent or cashier for additional purchases
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        `;
        mainBodyget.appendChild(purchaseOver);
        $('#purchaseLimit').modal('show')
    }

    function popFuncPurchaseLimit(items) {
        $('#purchaseLimit .modal-body').html(`<ul>${items.map(item => `
                                <li>
                                    <p>
                                        Purchase limit for <span class="purchase-limit-red">${item.name}</span>
                                        <span class="purchase-limit-red2">(${item.attribute}) </span>
                                        ${item.period} is <span class="purchase-limit-bold">${item.amount}</span> unit(s).
                                        You have a remaining balance of <span class="purchase-limit-bold">${item.purchaseLeft}</span> unit(s) for purchase.
                                        Contact your agent or cashier for additional purchases
                                    </p>
                                </li>
                            `).join('')} </ul>`);
            $('#purchaseLimit').modal('show');
    }

    // check self collect only for agent redemption product
     function checkSelfCollectOnly(){

        var $onlySelfCollect = [];
        var sessionMethod = "{{ Session::get('bjs_cart_delivery_method') }}"
        var sessionOutlet = "{{ Session::get('bjs_cart_delivery_outletId') }}"

        $('.itemChecked').each(function() {


            if ($(this).is(':checked') && $(this).attr('data-selfcollect-only') === 'true'){
                $onlySelfCollect.push(1);
            }
        });


        if ($onlySelfCollect.length > 0) {
            $('#delivery').addClass('disabled');
            $('#delivery').find('.icon-size').addClass('paymentDisabled');
            $('#delivery').find('.red-dot').addClass('paymentDisabled');
            $('#Self-pickup').click();
            sessionUpdate('Self-pickup');
        } else {
            $('#delivery').removeClass('disabled');
            $('#delivery').find('.icon-size').removeClass('paymentDisabled');
            $('#delivery').find('.red-dot').removeClass('paymentDisabled');

            //console.log('sessionMethod',sessionMethod);
        }

        if (sessionMethod) {
                var allDeliveryOptions = [];
                $('.delivery-option li a').each(function(){
                    allDeliveryOptions.push($(this));
                });

                $(allDeliveryOptions).each(function() {

                    if ($(this).attr('id') === sessionMethod){
                        $('#'+$(this).attr('id')).trigger('click');
                    }
                });
                if (sessionOutlet !== '') {
                    getOutletInfoURL = "{{ route('shop.cart.get-outletInfo', ['outletId' => 'outletId']) }}";
                    getOutletInfoURL = getOutletInfoURL.replace('outletId', sessionOutlet);

                    showAddress(getOutletInfoURL,sessionOutlet,sessionMethod);
                }
            } else {
                if ($onlySelfCollect.length == 0) sessionUpdate('delivery')

            }
    }

</script>
@endpush
