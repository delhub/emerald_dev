<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\User;

class CheckIfCustomerAgent implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $customer =  $this->user;

        if (!isset($customer)) return true;

        $agent = DealerInfo::where('user_id', $customer->id )->first();

        if (!isset($agent)) return true;
        
        return (isset($agent) && !$agent->is_dealership) ?  true : false ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Customer ID already Agent';
    }
}
