
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ActiveDpIdReminderInRbsPostpaid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rbs_postpaid', function (Blueprint $table) {
            $table->integer('active_dp_id')->nullable()->after('months_id');
            $table->date('active_reminder')->nullable()->after('start_reminder');
        });
        DB::table('rbs_postpaid')->where('start_reminder', '2022-09-01')->update(['start_reminder' => '2022-08-28']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rbs_postpaid', function (Blueprint $table) {
            $table->dropColumn('active_dp_id');
            $table->dropColumn('active_reminder');
        });
    }
}
