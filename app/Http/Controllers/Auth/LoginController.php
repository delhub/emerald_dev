<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

use Illuminate\Http\Request;

use App\Models\Categories\Category;
use App\Models\Globals\Countries;
use App\Models\Globals\State;
use App\Models\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use GeoIP;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
        // return $request->query();
        Session::put('url.intended', URL::previous());
        Session::get('url.intended');
        Cookie::queue(Cookie::forget('addressCookie'));
        $redirectFromRegister =  ($request->query('fromregister') == 'true') ? 'true' : 'false';
        /*$emailChanged = ($request->query('emailChanged') == 'true') ? 'true' : 'false';*/
        $sessionExpired =  ($request->query('pageExpired') == 'true') ? 'true' : 'false';
        $emailVerified =  ($request->query('emailVerified') == 'true') ? 'true' : 'false';
        $country = country()->country_id;
        return view('auth.login')
            ->with('redirectFromRegister', $redirectFromRegister)
            ->with('sessionExpired', $sessionExpired)
            /*->with('emailChanged', $emailChanged)*/
            ->with('emailVerified', $emailVerified)
            ->with('country', $country);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    // protected $redirectTo = '/shop';
    protected function redirectTo()
    {
        return '/shop';
    }

    public function logout(Request $request)
    {
        \Cookie::queue(\Cookie::forget('addressCookie'));
        \Cookie::queue(\Cookie::forget('bjs_cart'));

        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->loggedOut($request) ?: redirect('/');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => $request->getClientIp()
        ]);
    }
}
