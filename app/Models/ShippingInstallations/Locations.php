<?php

namespace App\Models\ShippingInstallations;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
         // Set table
         protected $table = 'shipping_locations';

         // Set primary key
         protected $primaryKey = 'id';

         // Set timestamps
         public $timestamps = true;

         // Set mass assignable columns
         protected $fillable = [
             'location_type',
             'location_id',
             'zone_id'
            ];

         public $incrementing = true;

        // Search
        public function scopesearchFilter($query,  $searchFilter, $selectSearch)
        {
            if (!is_null($searchFilter)) {

                if (($selectSearch == 'LT')) {
                    return $query
                        ->where('zone_locations.location_type', 'like', '%' . $searchFilter . '%');
                }

                elseif ($selectSearch == 'LI') {
                    return $query
                    ->where('zone_locations.location_id', 'like', '%' . $searchFilter . '%');
            }

                elseif ($selectSearch == 'ZI') {
                    return $query
                        ->Where('zone_locations.zone_id', 'like', '%' . $searchFilter . '%');

                }
            }

            return $query;
        }
}
