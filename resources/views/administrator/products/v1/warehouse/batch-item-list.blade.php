{{-- @inject('controller', 'App\Http\Controllers\Administrator\Countries\GlobalCountriesController') --}}

@extends('layouts.administrator.main')

@section('content')

<h1>Warehouse Batch Item List</h1>

<form action="{{ route('administrator.v1.products.warehouse.batch-item-list') }}" method="GET">
    <div class="row mt-2 ml-2">
        <div class="col-md-auto ml-auto">
            <div class="row">
                <div class="col-md-10 p-0">
                    <select name="location_id" class="form-control d-inline">
                        <option value="all">All Location</option>
                        @foreach ($locations as $location)
                        <option value="{{ $location->id }}" {{ $request->location_id == $location->id ? 'selected' : ''}}>
                            {{ $location->location_name }}</option>
                        @endforeach
                    </select>
                </div>

                <br><br>

                <div class="col-md-10 p-0">
                    <select class="custom-select" id="selectSearch" name="selectSearch">
                        <option value="ALL" selected>Choose...</option>
                        <option value="location_id" {{ $request->selectSearch == 'location_id' ? 'selected' : '' }}>Location Name</option>
                        <option value="item_batch_id" {{ $request->selectSearch == 'item_batch_id' ? 'selected' : '' }}>Batch Item ID</option>
                        <option value="product_code" {{ $request->selectSearch == 'product_code' ? 'selected' : '' }}>Product Code</option>
                        <option value="product_name" {{ $request->selectSearch == 'product_name' ? 'selected' : '' }}>Product Name</option>
                    </select>

                    <input type="text" class="form-control" id="" name="searchBox"
                        value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                </div>
                {{-- <div class="col-md-4 ">
                    <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                </div> --}}
            </div>
            <div class="row">
                <div class="col-md-10 p-0 text-right mt-2">
                    <button type="submit" class="btn btn-primary">Filter</button>
                    <button type="submit" class="btn btn-danger"><a href={{ route('administrator.v1.products.warehouse.batch-item-list') }} style="color:white;">Reset Filter</a></button>
                </div>
            </div>
        </div>
    </div>


    {{-- <div style="text-align:center;">
        <div class="col-md-auto px-1">
            <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
            <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.v1.products.warehouse.batch-item-list') }} style="color:black;">Reset Filter</a></button>
        </div>
    </div> --}}

    <br>

    {{-- <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Orders) : <b>{{ $countTotal}}</b></span></p> --}}
</form>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    {{-- <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a> --}}
                </div>
            </div>
        </div>
        {{$tables->links()}}

        <div class="table-responsive m-2">
            <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <th>Batch Item ID</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Product Attribute</th>
                        <th>Location Name</th>
                        <th>Used In</th>

                         {{--<td>Conversion Rate</td>
                        <td>Mark Up</td>
                        <td>Company Name</td>
                        <td>Company Address</td>
                        <td>Company Reg</td>
                        <td>Company Phone</td>
                        <td>Company Email</td> --}}
                        {{-- <td>Action</td> --}}
                    </tr>
                </thead>

                @if(count($tables) > 0)
                <tbody>

                    @foreach($tables as $row)
                    <tr>
                        <td>{{$row->items_id}}</td>
                        <td>{{$row->batchDetail->product_code}}</td>
                        <td>{{$row->batchDetail->productAttribute->product2->parentProduct->name}}</td>
                        <td>{{$row->batchDetail->productAttribute->attribute_name}}</td>
                        <td>{{$row->location->location_name}}</td>
                        <td>
                             @if ($row->type != NULL)
                             {{$row->type}} ({{getPickOrderId($row->type,$row->picking_order_id)}})
                             @endif
                        </td>

                        {{-- <td>{{$row->country_currency}}</td> --}}
                        {{--<td>{{$row->conversion_rate}}</td>
                        <td>{{$row->mark_up}}</td>
                        <td>{{$row->company_name}}</td>
                        <td>{{$row->company_address}}</td>
                        <td>{{$row->company_reg}}</td>
                        <td>{{$row->company_phone}}</td>
                        <td>{{$row->company_email}}</td> --}}
                        {{-- <td> --}}
                            {{-- <a style="color: white; font-style: normal; border-radius: 5px; float: left" class="btn btn-primary shadow-sm"
                            href="{{ route($controller::$edit,['id' => $row->country_id]) }}" >Edit</a> --}}

                            {{-- <form action="{{ route($controller::$destroy,['id' => $row->country_id]) }}" id="delete-form-id" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            @csrf
                                <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>
                            </form> --}}
                        {{-- </td> --}}
                    </tr>
                    @endforeach
                </tbody>
                @else
                <tbody>
                    <tr>
                        <td colspan="6" style="text-align:center">No Data ..</td>
                    </tr>
                </tbody>
                @endif
            </table>
            {{$tables->links()}}
            {{-- @else --}}
            {{-- <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p> --}}
            {{-- @endif --}}
        </div>
    </div>
</div>
@endsection
{{--
@push('script')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    });
</script>
@endpush --}}
