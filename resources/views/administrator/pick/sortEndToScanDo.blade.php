@inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

@if ($pickBatchDetail)
<br>

<h1>Continue Scan DO</h1>
<hr>
<br>
<br>
<h2>
    Batch ({{$pickBatchDetail->batch->batch_number}})
<br>
<br>

    Basket Number ({{$pickBatchDetail->batch->bin->bin_number}})
</h2>

@else

<h2>
    Scan DO Process (start)
</h2>

@endif

<div class="my-input-warehouse input-group input-group-lg mb-3">
    <input type="text" ref="scancode" v-model="scancode" name="scancode" class="bg-form2 form-control" id="my-input-warehouse"
        placeholder="Use Scanner Here" autofocus autocomplete="off" @@change="qrDo(scancode)">
    <div class="pop-btn">
        <button type="button" class="qrbtn btn-primary">Submit</button>
    </div>
</div>

@endsection

