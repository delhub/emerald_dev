@php
$countryObj = country();
$country = $countryObj->country_id;
@endphp


<header class="fml-head nav-height">
    <div class="fml-container">
        {{-- new menu --}}
        <div class="fml-menu">
            <nav class="navbar" id="fml">
                <a class="navbar-brand" href="/shop">
                    <img class="weblogo" src="{{ asset('images/logo/formula-logo.png') }}" alt="Formula" width="150">
                    <img class="mobilelogo" src="{{ asset('images/logo/mobile_logo.png') }}" alt="Formula"
                         width="150">
                </a>
                <ul>
                    <li class="nav-item dropdown"
                        style="{{ request()->route()->uri == 'payment/cashier' ? 'display:none;' : '' }}">
                        @php
                            use App\Models\Globals\Countries;
                            use Illuminate\Support\Facades\Cookie;
                            $cookies = $country;
                        @endphp
                    </li>
                    {{-- <li>
                            <div class="blink_me {{ Auth::user()->userInfo->account_status == '1' ? 'd-none' : '' }}" data-toggle="tooltip" title="Apply for DC customer"><a href="{{ route('customer.membership') }}">DC
                            </a></div>
                        </li>
                        <li>
                            <a href="/shop/dashboard/wishlist/index" class="nav-link fml-nav" data-toggle="tooltip" title="Perfect List">
                                <i class="fa fa-heart-o"></i>
                                <span id="wishlist-quantity" class="fml-cart-wish d-inline-block align-middle text-center">0</span>
                            </a>
                        </li> --}}
                    {{-- <li>

                        <form method="POST" action="{{ route('sg.launch.buyNow') }}" id="sg-launch-form"
                                enctype="multipart/form-data">
                                 @csrf
                                    <input type="hidden" name="panelProduct" value="467">
                                    <input type="hidden" name="parentProduct" value="474">
                                     <button  type="submit"  style="border:none; background:none;padding:4px 12px">
                          <img src="{{asset('/images/icons/menu-icon5.png')}}" alt="">
                            </button>
                        </form>
                        </li> --}}
                    <li>
                        <div class="search navbar-toggler" data-toggle="modal" data-target="#searchModal"
                             data-whatever="@mdo">
                            <img src="{{ asset('/images/icons/menu-icon3.png') }}" alt="">
                        </div>
                    </li>
                    @if (Auth::check())
                    <li>
                        <a href="/shop/cart">
                            <img src="{{ asset('/images/icons/menu-icon1.png') }}" class="open-menu navbar-toggler"
                                 alt="">
                            <span id="cart-quantity" class="fml-cart-wish d-inline-block align-middle text-center"
                                  onchange="getftval()">0</span>
                        </a>
                    </li>
                    @endif
                    <li class="mobile-none">
                        <button class="open-menu navbar-toggler menuMain" type="button">
                            <img src="{{ asset('/images/icons/menu-icon2.png') }}" alt="">
                        </button>
                    </li>
                    {{-- <li class="mobile-none">
                            <button class="open-menu navbar-toggler user" type="button" data-toggle="collapse" data-target="#userContent" aria-controls="userContent" aria-expanded="false" aria-label="Toggle navigation">
                                <img src="{{asset('/images/icons/menu-icon2.png')}}" alt="">
                            </button>
                        </li> --}}
                    {{-- <li >
                            <button class="open-menu navbar-toggler ctg" type="button" data-toggle="collapse" data-target="#menuContent" aria-controls="menuContent" aria-expanded="false" aria-label="Toggle navigation">
                                <img src="{{asset('/images/icons/menu-icon4.png')}}" alt="">
                              </button>
                        </li> --}}
                </ul>
            </nav>
        </div>

        <div class="mn collapse" id="menuContent">
            <div class="fml-menu-group">
                <div class="p-4">
                    <div class="row my-mt-5">
                        <div class="col-md-3 hr-right">
                            <ul>
                                <li>
                                    <a href="">Home</a>
                                </li>
                                <li>
                                    <a href="">About Us</a>
                                </li>
                                <li>
                                    <a href="">Brands</a>
                                </li>
                                <li>
                                    <div class="submenu-categories">
                                        <h6 href="">Product</h6>
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                             aria-orientation="vertical">

                                            <a class="nav-link active" id="men-care-tab" data-toggle="pill"
                                               href="#men-care" role="tab" aria-controls="men-care"
                                               aria-selected="true">Men’s Care</a>

                                            <a class="nav-link" id="women-care-tab" data-toggle="pill"
                                               href="#women-care" role="tab" aria-controls="women-care"
                                               aria-selected="false">Women’s Care</a>

                                            <a class="nav-link" id="baby-care-tab" data-toggle="pill" href="#baby-care"
                                               role="tab" aria-controls="baby-care" aria-selected="false">Baby’s
                                                Care</a>

                                            <a class="nav-link" id="kids-care-tab" data-toggle="pill" href="#kids-care"
                                               role="tab" aria-controls="kids-care" aria-selected="false">Kids’s
                                                Care</a>

                                            <a class="nav-link" id="aging-care-tab" data-toggle="pill"
                                               href="#aging-care" role="tab" aria-controls="aging-care"
                                               aria-selected="false">Aging Care</a>

                                            <a class="nav-link" id="health-care-tab" data-toggle="pill"
                                               href="#health-care" role="tab" aria-controls="health-care"
                                               aria-selected="false">Health Care</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="">Discover Yourself</a>
                                </li>
                                <li>
                                    <a href="">Medicine Reminder</a>
                                </li>
                                <li>
                                    <a href="">Health Articles</a>
                                </li>
                                <li>
                                    <a href="">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="submenu-categories02">
                                <h6 href="">Product</h6>
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">

                                    <a class="nav-link active" id="men-care-tab" data-toggle="pill" href="#men-care"
                                       role="tab" aria-controls="men-care" aria-selected="true">Men’s Care</a>

                                    <a class="nav-link" id="women-care-tab" data-toggle="pill" href="#women-care"
                                       role="tab" aria-controls="women-care" aria-selected="false">Women’s
                                        Care</a>

                                    <a class="nav-link" id="baby-care-tab" data-toggle="pill" href="#baby-care"
                                       role="tab" aria-controls="baby-care" aria-selected="false">Baby’s Care</a>

                                    <a class="nav-link" id="kids-care-tab" data-toggle="pill" href="#kids-care"
                                       role="tab" aria-controls="kids-care" aria-selected="false">Kids’s Care</a>

                                    <a class="nav-link" id="aging-care-tab" data-toggle="pill" href="#aging-care"
                                       role="tab" aria-controls="aging-care" aria-selected="false">Aging Care</a>

                                    <a class="nav-link" id="health-care-tab" data-toggle="pill" href="#health-care"
                                       role="tab" aria-controls="health-care" aria-selected="false">Health
                                        Care</a>
                                </div>
                            </div>
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="men-care" role="tabpanel"
                                     aria-labelledby="men-care-tab">
                                    <div class="product-item">
                                        <h4>Product <span>Men’s Care</span></h4>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Bone and Joint Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="women-care" role="tabpanel"
                                     aria-labelledby="women-care-tab">
                                    <div class="product-item">
                                        <h4>Product <span>Women Care</span></h4>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Bone and Joint Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="baby-care" role="tabpanel"
                                     aria-labelledby="baby-care-tab">
                                    <div class="product-item">
                                        <h4>Product <span>Baby Care</span></h4>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Bone and Joint Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="kids-care" role="tabpanel"
                                     aria-labelledby="kids-care-tab">
                                    <div class="product-item">
                                        <h4>Product <span>Kids Care</span></h4>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Bone and Joint Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aging-care" role="tabpanel"
                                     aria-labelledby="aging-care-tab">
                                    <div class="product-item">
                                        <h4>Product <span>Aging Care</span></h4>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Bone and Joint Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="health-care" role="tabpanel"
                                     aria-labelledby="health-care-tab">
                                    <div class="product-item">
                                        <h4>Product <span>Health Care</span></h4>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}" alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Stomach/ Gut Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Bone and Joint Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                        <div class="item-icon">
                                            <a href="{{ route('shop.product_multineed') }}">
                                                <img class="icon-image"
                                                     src="{{ asset('/images/icons/hair-icon.png') }}"
                                                     alt="Icon">
                                                Lung Care
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mn" id="userContent">
            <div class="fml-menu-group">

                @include('layouts.shop.footer-menu.user-ft-menu')
            </div>
        </div>
    </div>
</header>
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <form autocomplete="off">
                    <div class="form-group search">
                        <label type="text" for="searchFunc" class="col-form-label">
                            <img src="{{ asset('/images/icons/menu-icon3.png') }}" alt="">
                        </label>
                        <input type="text" class="form-control" id="searchFunc" placeholder="Search">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

    <script type="text/javascript">
        function onPageLoadGetQuantity() {
            @if (Auth::check())
                let getQuantityURL = "{{ route('web.shop.cart.quantity', ['id' => Auth::user()->id]) }}";
            @else
                let getQuantityURL = null;
            @endif
            const cartQuantityElement = $('#cart-quantity');
            const cartQuantityFt = document.querySelectorAll('#cart-quantity-ft');
            //console.log(getQuantityURL);

            function getQtyCart(i) {
                cartQuantityFt.forEach(item => {
                    item.innerHTML = i;
                })
            }

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    cartQuantityElement.text(response.data);
                    getQtyCart(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        // function to get perfect list quantity
        function onPageLoadGetPerfectListQuantity() {
            @if (Auth::check())
                let getQuantityURL = "{{ route('shop.perfect-list.quantity', ['id' => Auth::user()->id]) }}";
            @else
                let getQuantityURL = null;
            @endif
            const perfectListQuantityElement = $('#wishlist-quantity');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    perfectListQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        $(document).ready(function() {
            onPageLoadGetQuantity();
            onPageLoadGetPerfectListQuantity();
            $("a").tooltip();
        });

        //bootstrap3-typeahead.min.js, search bar
        $('#searchFunc').typeahead({

            items: 'all',
            autoSelect: true,
            showCategoryHeader: true,
            minLength: 0,

            source: function(queryText, response) {

                $.ajax({
                    url: "{{ route('shop.search') }}",
                    method: "GET",
                    data: {
                        query: queryText
                    },
                    dataType: "JSON",

                    success: function(data) {
                        response($.map(data, function(item) {

                            return {
                                url: item.url,
                                value: item.name,
                                image: item.image,
                                _category: item._category
                            }

                        }));
                    }
                });
            },

            displayText: function(item) {
                return item.value;
            },

            afterSelect: function(event) {
                window.location.href = event.url;
            },

            highlighter: function(item, data) {
                return "<div class='menu-search-box'><div class='box-size'><div class='msbs-1'><img src='" +
                    data.image +
                    "'></div><div class='msbs-2'><span>" + item + "</span></div></div></div>";
            },

        });
    </script>
@endpush
