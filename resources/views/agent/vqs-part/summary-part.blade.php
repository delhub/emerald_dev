<div class="card">
    <div class="card-body">
        <div class="row my-lg-1 my-2">
            <div class="col-lg-2" style="max-width: 130px;"> <strong>Participant ID</strong></div>
            <div class="col-lg-3 py-lg-1 mx-lg-0 mx-3 py-2 rounded summary-details ">
                {{ $summaryArchimedes['date_and_id']->dealer_id }}</div>
        </div>
        <div class="row my-lg-1 my-2">
            <div class="col-lg-2" style="max-width: 130px;"><strong>Date of joined</strong></div>
            <div class="col-lg-3 py-lg-1 mx-lg-0 mx-3 py-2 rounded summary-details">
                {{ date_format($summaryArchimedes['date_and_id']->created_at, 'd/m/Y') }}</div>
        </div>
        <div class="row my-lg-1 my-2">
            <div class="col-lg-2" style="max-width: 130px;"><strong>Deposit</strong></div>
            <div class="col-lg-3 py-lg-1 mx-lg-0 mx-3 py-2 rounded summary-details">
                {{-- RM {{ number_format(($summaryArchimedes['date_and_id']->value/100),2) }} --}}
                -
            </div>
        </div>
    </div>
</div>

@if (empty($summaryArchimedes['archimedes_details']))

    <div class="card my-2 px-lg-5  px-2">
        <div class="card-header archimedes-header  my-md-3">

        </div>
        <div class="card-body row">
            <div class="col-auto mx-auto">
                <strong>
                    No Order Found.
                </strong>
            </div>
        </div>
    </div>
@else
    @foreach ($summaryArchimedes['archimedes_details'] as $products)
        <div class="card px-lg-5 px-2">
            <div class="card-body px-0">
                <div class="row">
                    <div class="col-xl-1 col-lg-2 pr-lg-0 col-md-2 col-auto mx-auto mx-lg-0 mx-md-0">
                        @if ($itemTable->where('id', $products->first()->item_id)->first()->product->parentProduct->defaultImage)
                            <img class="vr-p-img"
                                src="{{ asset('storage/' .$itemTable->where('id', $products->first()->item_id)->first()->product->parentProduct->images[0]->path .$itemTable->where('id', $products->first()->item_id)->first()->product->parentProduct->images[0]->filename) }}" />
                        @else
                            <img class="responsive-img p-1" src="" alt="Product Image" />
                        @endif
                    </div>
                    <div class="col-lg-10 col-md-10 col-12 summaryProductTitle">
                        <strong style="font-size:17px;" class="font-weight-bold">
                            {{ $itemTable->where('id', $products->first()->item_id)->first()->product->parentProduct->name }}
                        </strong>
                        <br>
                        <p class="col-12 m-0 col-md-auto ">
                            {{ $products->sum('quantity') ?: '0' }}/{{ $products->where('type', 5002)->sum('quantity') ?: '0' }} (Stock on hand/VQS Subscribed)
                        </p>

                    </div>
                </div>
                <div class="row my-lg-2">
                    <!-- Table show in desktop -->
                    <div class="col-lg-12 col-md-12 col-12 py-md-2 d-none d-sm-block">
                        <table class="table table-striped table-bordered table-summary">
                            <thead style="background: #ffea94;">
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Invoice Number</th>
                                    <th scope="col">Tracking No.</th>
                                    {{-- <th scope="col">Recepient</th> --}}
                                    <th scope="col">Status</th>
                                    <th scope="col">Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products->forget('archimedes_value') as $product)
                                    <tr>

                                        <th scope="row">{{ date_format($product->created_at, 'd/m/Y @ H:i') }}</th>
                                        <td>{{ $product->ref_id && $product->type != 5002 ? $product->ref_id : '-' }}
                                        </td>
                                        <td>{{ $product->item ? ($product->item->tracking_number ? $product->item->tracking_number : '-') : '-' }}
                                        </td>
                                        {{-- <td>{{ $product->item ? $product->item->order->purchase->ship_full_name : '-' }}
                                        </td> --}}
                                        <td>{{ $statusLabel->find(isset($product->item->item_order_status) && $product->type != 5002? $product->item->item_order_status: $product->type)->name }}
                                        </td>
                                        <td>@php if ($product->quantity > 0) echo "+"; echo $product->quantity; @endphp</td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- Table show in mobile -->
                    <div class="col-lg-12 col-md-6 col-12 d-block d-sm-none">
                        @foreach ($products->forget('archimedes_value') as $product)
                            <div class="row my-lg-2 mx-1">
                                <div class="col-12 p-0">
                                    <hr style="border-top: 0.01rem solid #ffcc00;">
                                </div>
                                <div class="col-lg-12 col-md-6 col-9 pr-0">
                                    <div class="row">
                                        <div class="col-5 pr-0 pl-2 summary-title">Date</div>
                                        <div class="col-7 pl-0 summary-desc">:
                                            {{ date_format($product->created_at, 'd/m/Y @ H:i') }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5 pr-0 pl-2 summary-title">Invoice</div>
                                        <div class="col-7 pl-0 summary-desc">:
                                            {{ $product->ref_id ? $product->ref_id : '-' }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5 pr-0 pl-2 summary-title">Tracking No.</div>
                                        <div class="col-7 pl-0 summary-desc">:
                                            {{ $product->item ? ($product->item->tracking_number ? $product->item->tracking_number : '-') : '-' }}
                                        </div>
                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-5 pr-0 pl-2 summary-title">Recepient</div>
                                        <div class="col-7 pl-0 summary-desc">:
                                            {{ $product->item ? $product->item->order->purchase->ship_full_name : '-' }}
                                        </div>
                                    </div> --}}
                                    <div class="row d-none">
                                        <div class="col-5 pr-0 pl-2 summary-title">Status</div>
                                        <div class="col-7 summary-desc">: 02/01/2021</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5 pr-0 pl-2 summary-title">Qty</div>
                                        <div class="col-7 pl-0 summary-desc">: {{ abs($product->quantity) }}</div>
                                    </div>
                                </div>
                                <div class="col-3 pl-0 pr-4">
                                    <div class="row">
                                        <div class="col-auto ml-auto p-0">
                                            <button
                                                class="btn my-auto btn-small-screen font-weight-bold bjsh-btn-gradient ml-3"
                                                style="padding: 3px 5px; font-size:12px;">
                                                {{ $statusLabel->find($product->type)->name }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
