<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class AddGlobalCityAndDummyLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_cities')->insert(
            array(
                array('country_id' => 'MY', 'state_id' => '15', 'city_key' => 'LBN_LBN', 'city_name' => 'Labuan', 'city_data' => 'Labuan', 'active' => 'Y',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        DB::table('wh_location')->insert(
            array(
                array(
                    'id' => '2', 'location_key' => 'MYSGRPCG002', 'location_name' => 'Puchong Retail Shop', 'type' => 'SH', 'country_id' => 'MY', 'city_key' => 'SGR_PCG', 'state_id' => '12',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),
                ),
                array(
                    'id' => '999', 'location_key' => 'DUMMY', 'location_name' => 'DUMMY', 'type' => 'WH', 'country_id' => 'MY', 'city_key' => 'LBN_LBN', 'state_id' => '15',  'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),

                )
            )

        );

        DB::update('update wh_inventories set outlet_id = 999');
        DB::update('update wh_batch_item set location_id = 999');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
