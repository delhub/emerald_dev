<?php

namespace App\Jobs\Emails;

use App\Mail\MassMailing;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMassMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $email;
    public $title;
    public $content;
    public $recipient_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct( $email, $title, $content , $recipient_name)
    {
        $this->email = $email;
        $this->title = $title;
        $this->content = $content;
        $this->recipient_name  = $recipient_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emailInstance = new MassMailing($this->title, $this->content, $this->recipient_name);
        Mail::to($this->email)->send($emailInstance);
    }
}
