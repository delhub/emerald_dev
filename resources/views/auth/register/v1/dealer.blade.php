@extends('layouts.guest.v1.main')

@section('content')

@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp
<div class="bg-md bg-sm ">
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 mb-0 pt-2 pb-3">
            <img class="mw-100" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
        </div>
    </div>
    <div>
        <div class="card border-rounded-0 bg-bujishu-gold guests-card " style="border-radius: 10px;">
            <h5 class="text-center bujishu-gold form-card-title " style="border-radius: 10px;">Agent Registration</h5>
            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link register-tab-active active " id="home-tab" data-toggle="tab" href="#registration" role="tab" aria-controls="registration" aria-selected="true">Registration</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link register-tab-active " id="profile-tab" data-toggle="tab" href="#information" role="tab" aria-controls="profile" aria-selected="false">Introducer Information</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link register-tab-active " id="contact-tab" data-toggle="tab" href="#introducer" role="tab" aria-controls="contact" aria-selected="false">Payment Information</a>
                </li>
            </ul>
            <div class="card-body ">
                <!-- Dealer Registration Form -->
                <form method="POST" action="{{ route('guest.register.dealer.post') }}" id="register-form" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <!-- Registration  Tab-->
                        <div class="tab-pane fade show active" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                            <h5 class="text-center font-weight-bold" style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">Account Particulars</h5>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="email">Email <small class="text-danger">*</small></label>
                                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Email" value="{{ old('email') }}">
                                    @error('email')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="password">Password <small class="text-danger">*</small></label>
                                    <input type="password" name="password" class="form-control" id="password">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="password-confirm">Confirm Password <small class="text-danger">*</small></label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                                </div>
                            </div>

                            <!-- Next Button -->
                            <div class="text-right">
                                <a class="btn btn-secondary next-button bjsh-btn-gradient " id="next-btn"><b>Next</b></a>
                            </div>

                            <div id="error-div-1" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Information Tab -->
                        <div class="tab-pane fade" id="information" role="tabpanel" aria-labelledby="information-tab">
                            <!-- Personal Particulars -->
                            <h5 class="text-center" style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">Introducer Information</h5>
                            <div class="form-row">
                                <div class="col-12 col-md-6 form-group">
                                    <label for="introducer_name">Introducer Name <small class="text-danger">*</small></label>
                                    <input type="text" name="introducer_name" id="introducer_name" class="form-control">
                                    <div class="valid-feedback feedback-icon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <div class="invalid-feedback feedback-icon">
                                        <i class="fa fa-times"></i>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 form-group">
                                    <label for="introducer_id">Introducer ID</label>
                                    <input type="text" name="introducer_id" id="introducer_id" class="form-control">
                                    <div class="valid-feedback feedback-icon">
                                        <i class="fa fa-check"></i>
                                    </div>
                                    <div class="invalid-feedback feedback-icon">
                                        <i class="fa fa-times"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-12 form-group">
                                    <label for="agent_group">Agent Group <small class="text-danger">*</small></label>
                                    <select name="agent_group" id="agent_group" class="form-control">
                                        <option value="default">Select a group..</option>
                                        <optgroup label="Kuala Lumpur">
                                            <option value="1">Chiew Wei Ping @ Alex</option>
                                            <option value="2">Chow Siok Koon @ Tiffany</option>
                                            <option value="3">Kong Kim Leng @ June</option>
                                            <option value="4">Liew Wei Chyan @ Dave</option>
                                            <option value="5">Yeap Lay Yan</option>
                                        </optgroup>
                                        <optgroup label="Penang">
                                            <option value="6">Lee Kean Guan @ Sampson</option>
                                            <option value="7">Ch'ng Saw Boay @ May</option>
                                            <option value="8">Khong Bee Kim @ Kim</option>
                                            <option value="9">Tan Teng Siew @ Darren</option>
                                            <option value="10">Tang Hui Chin</option>
                                            <option value="11">NDV</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <!--
                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package1" name="package" value="1" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="package1">
                                        <p class="font-weight-bold">配套1 (DC客戶申請代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-1.jpg') }}" alt="Package 2">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 6,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            1,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem; color: #ffffff;">
                                                            RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package2" name="package" value="2" class="custom-control-input">
                                    <label class="custom-control-label" for="package2">
                                        <p class="font-weight-bold">配套2 (PJA申請代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-2.jpg') }}" alt="Package 2">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 6,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            3,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM29,000 指定生意
                                                            <br>
                                                            額外獲得 RM1,000 現金花紅
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package3" name="package" value="3" class="custom-control-input">
                                    <label class="custom-control-label" for="package3">
                                        <p class="font-weight-bold">配套3 A級創利方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-3.jpg') }}" alt="Package 3">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 7,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            7,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：60天內完成 RM19,000指定生意
                                                            <br>
                                                            額外獲得 RM1,000 現金花紅
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package4" name="package" value="4" class="custom-control-input">
                                    <label class="custom-control-label" for="package4">
                                        <p class="font-weight-bold">配套3 A+級擢昇方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-4.jpg') }}" alt="Package 4">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 17,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            17,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM39,000 指定生意
                                                            <br>
                                                            額外獲得 RM2,500 現金花紅
                                                            <br>
                                                            <span class="text-danger">僅限99個位子</span>
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package5" name="package" value="5" class="custom-control-input">
                                    <label class="custom-control-label" for="package5">
                                        <p class="font-weight-bold">配套3 A++領導方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-5.jpg') }}" alt="Package 5">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 38,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            38,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM59,000 指定生意
                                                            <br>
                                                            額外獲得 RM4,000 現金花紅
                                                            <br>
                                                            <span class="text-danger">僅限50個位子</span>
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
			    -->

                            <!-- Next Button -->
                            <div class="text-right">
                                <!-- <a class="btn btn-secondary" id="profile-tab" data-toggle="tab" href="#introducer" role="tab" aria-controls="profile" aria-selected="false">Next</a> -->
                                <a class="btn btn-secondary next-button bjsh-btn-gradient " id="next-btn2"><b>Next</b></a>
                            </div>

                            <div id="error-div-2" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Introducer Tab -->
                        <div class="tab-pane fade" id="introducer" role="tabpanel" aria-labelledby="introducer-tab">
                            <!-- Introducer Particular -->
                            <h5 class="text-center" style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">Payment Information</h5>
                            <!-- Card div -->
                            <!--
                            <div id="paymentCardDiv">
                                <div class="form-row">
                                    <div class="col-12 mb-1 form-group">
                                        <label for="card_number">Card Number <small class="text-danger">*</small></label>
                                        <input type="text" class="form-control" name="card_number" id="card_number" placeholder="Card Number">
                                        <input type="hidden" name="card_type" id="card_type" value="">
                                        <div class="valid-feedback feedback-icon with-cc-icon">
                                            <i class="fa"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon with-helper">
                                            <i class="fa fa-times"></i>
                                        </div>
                                        <div class="invalid-feedback invalid-helper">
                                            <small class="text-danger">Card number must be 16.</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12 col-md-6 mb-1 form-group">
                                        <label for="expiry_date">Expiration Date <small class="text-danger">*</small></label>
                                        <input type="text" class="form-control" name="expiry_date" id="expiry_date" placeholder="MMYY">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 mb-1 form-group">
                                        <label for="cvv">CVV <small class="text-danger">*</small></label>
                                        <input type="text" class="form-control" name="cvv" id="cvv" placeholder="CVV">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>
                                </div>
			    </div>
			   -->

                            <!-- Offline payment div -->
                            <div id="paymentOfflineDiv">
                                <div class="form-row">
                                    <div class="col-12 col-md-6 form-group">
                                        <label for="offline_reference">Reference Number / APP Code <small class="text-danger">*</small></label>
                                        <input type="text" name="offline_reference" id="offline_reference" class="form-control">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 form-group">
                                        <label for="offline_amount">Payment Amount <small class="text-danger">*</small></label>
                                        <input type="text" name="offline_amount" id="offline_amount" class="form-control input-mask-price">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-12">
                                        <label for="exampleFormControlFile1">Payment Proof <small>(Please upload payment receipt.) <small class="text-danger">*</small></small></label>
                                        <input type="file" name="payment_proof" id="payment_proof" class="form-control-file @error('payment_proof') is-invalid @enderror">
                                        @error('payment_proof')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                        <small style="display: none;" id="no-file-error" class="form-text text-danger">Please upload payment proof.</small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mt-2">
                                <div class="col-12" style="padding-left: .7rem;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="credit-debit-agree">
                                        <label class="custom-control-label" for="credit-debit-agree">
                                            I have read, understand and accepted
                                            the <a href="" data-toggle="modal" data-target="#purchaseAgreementModal">terms and conditions</a>.
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <!--	<div class="col-6">
                                    <button type="button" id="offline-payment-button" class="btn btn-secondary text-right">Offline Payment</button>
                                    <button type="button" id="card-payment-button" class="btn btn-secondary text-right" style="display: none;">Debit/Credit Payment</button>
				</div> -->
                                <div class="col-12 text-right">
                                    <!-- Submit button -->
                                    <input type="hidden" name="package" value="10" id="package">
                                    <input type="hidden" name="paymentType" value="offline" id="paymentType">
                                    <button type="submit" id="pay-now-button" class="btn next-button bjsh-btn-gradient text-right font-weight-bold" disabled>Sign Up</button>
                                </div>
                            </div>

                            <div id="error-div-3" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="purchaseAgreementModal" tabindex="-1" role="dialog" aria-labelledby="purchaseAgreementModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-12 mb-0">
                        <div class="overflow-auto" style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                             @if (country()->country_id == 'MY')
                                @include('shop.payment.service-terms-conditions-my')
                            @else
                                @include('shop.payment.service-terms-conditions-sg')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="emailErrorModal" tabindex="-1" role="dialog" aria-labelledby="emailErrorModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="emailErrorModalLabel">Email Is Currently Used</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.1rem;">
                    The email you entered has been used.
                    <br>
                    <br>
                    Please send an email to
                    <br>
                    <a style="font-size: 1.15rem; font-weight: 600;" href="mailto:formula2u-cs@delhubdigital.com">formula2u-cs@delhubdigital.com</a> for further assistance.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    @error('email')
    $('#emailErrorModal').modal('show');
    @enderror

    let paymentType = $('#paymentType');

    const paymentCardDiv = $('#paymentCardDiv');
    const paymentOfflineDiv = $('#paymentOfflineDiv');

    $('#offline-payment-button').on('click', function() {
        paymentCardDiv.hide();
        paymentOfflineDiv.show();
        $('#offline-payment-button').hide();
        $('#card-payment-button').show();
        paymentType.val('offline');
    });

    $('#card-payment-button').on('click', function() {
        paymentOfflineDiv.hide();
        paymentCardDiv.show();
        $('#card-payment-button').hide();
        $('#offline-payment-button').show();
        paymentType.val('card');
    });

    let priceSelectors = document.getElementsByClassName('input-mask-price');

    let priceInputMask = new Inputmask({
        'mask': '99999.99',
        'numericInput': true,
        'digits': 2,
        'digitsOptional': false,
        'placeholder': '0'
    });

    for (var i = 0; i < priceSelectors.length; i++) {
        priceInputMask.mask(priceSelectors.item(i));
    }

    // Credit/Debit Card JS
    // Credit card pattern recognition algorithm.
    function detectCardType(number) {
        var re = {
            // electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            // maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            // dankort: /^(5019)\d+$/,
            // interpayment: /^(636)\d+$/,
            // unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/,
            // amex: /^3[47][0-9]{13}$/,
            // diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            // discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            // jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        }

        for (var key in re) {
            if (re[key].test(number)) {
                return key
            }
        }
    }

    $('#introducer_name').on('keyup', function() {
        if ($(this).val().length == 0 || $(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#introducer_id').on('keyup', function() {
        if ($(this).val().length < 10 || $(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#agent_group').on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    // Form Validation - Credit/Debit Card.
    let cardNumber = null;
    let ccIcon = $('.valid-feedback.feedback-icon.with-cc-icon i');

    $('#card_number').on('keyup', function() {
        cardNumber = $(this).val();
        cardType = detectCardType(cardNumber);

        if ($(this).val().length < 7 || cardType == undefined) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
            ccIcon.removeClass();
            ccIcon.addClass('fa fa-cc-' + cardType).css('font-size', '30px');
            $('#card_type').val(cardType);
        }
    });

    $('#expiry_date').on('keyup', function() {
        if ($(this).val().length != 4 || !$.isNumeric($(this).val())) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#cvv').on('keyup', function() {
        if ($(this).val().length < 3 || !$.isNumeric($(this).val())) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    // End Form Validation - Credit/Debit Card
    // End Credit/Debit Card JS

    // Form Validation - Offline Payment
    $('#offline_reference').on('keyup', function() {
        if ($(this).val().length == 0 || $(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#offline_amount').on('keyup', function() {
        if ($(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });


    // End Form Validation - Offline Payment

    let currentTab = $('.nav-tabs > .active');
    let nextTab = currentTab.next('li');

    // Handles tabs click.
    $('.nav-link').click(function() {
        currentTab = $(this).parent();
        $('.nav-tabs > .active').removeClass('active');
        currentTab.addClass('active');
        nextTab = currentTab.next('li');
    });

    // Form Validation
    $("#register-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8,
            },
            password_confirmation: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            },
        },
        messages: {
            email: {
                required: "Please enter an email.",
                email: "The email is not valid."
            },
            password: {
                required: "Please enter a password.",
                minlength: "Password must be minimum of 8 characters."
            },
            password_confirmation: {
                required: "Please confirm your password",
                minlength: "Password must must be minimum of 8 characters",
                equalTo: "Password must be same as above"
            },
        }
    });

    // Validate fields in 1st tab.
    $('#next-btn').click(function() {
        if (
            $("#register-form").validate().element('#email') &&
            $("#register-form").validate().element('#password') &&
            $("#register-form").validate().element('#password-confirm')
        ) {
            $('#error-div-1').hide();
            nextTab.find('a').trigger('click');
        } else {
            $('#error-div-1').show();
        }
    });

    // Validate fields in 2nd tab.
    $('#next-btn2').click(function() {
        if (
            $('#introducer_name').val() != '' &&
            $('#agent_group').val() != 'default'
        ) {
            $('#error-div-2').hide();
            nextTab.find('a').trigger('click');
        } else {
            $('#error-div-2').show();
            $('#introducer_name').addClass('is-invalid');
            $('#agent_group').addClass('is-invalid');
        }
    });

    // Validate form before submit.
    $('#register-form').on('submit', function(e) {
        let error = 0;

        if (
            $("#register-form").validate().element('#email')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#register-form").validate().element('#password')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (

            $("#register-form").validate().element('#password-confirm')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $('#introducer_name').val().length == 0 || $('#introducer_name').val() == ''
        ) {
            error = error + 1;
            $('#introducer_name').addClass('is-invalid');
        }

        if ($('#agent_group').val() == 'default') {
            error = error + 1;
            $('#agent_group').addClass('is-invalid');
        }

        if (paymentType.val() == 'card') {

            if (
                $('#card_number').val().length < 7 ||
                cardType == undefined
            ) {
                error = error + 1;
                $('#card_number').addClass('is-invalid');
                $('#card_number').focus();
            }

            if ($('#expiry_date').val().length != 4 || !$.isNumeric($('#expiry_date').val())) {
                error = error + 1;
                $('#expiry_date').addClass('is-invalid');
                $('#expiry_date').focus();
            }

            if ($('#cvv').val().length < 3 || !$.isNumeric($('#cvv').val())) {
                error = error + 1;
                $('#cvv').addClass('is-invalid');
                $('#cvv').focus();
            }
        }

        if (paymentType.val() == 'offline') {
            if ($('#offline_reference').val() == '' || $('#offline_reference').val().length == 0) {
                error = error + 1
                $('#offline_reference').addClass('is-invalid');
            }
            if ($('#payment_proof').get(0).files.length == 0) {
                error = error + 1;
                $('#no-file-error').show();
            }
        }

        if (error == 0) {
            $('#error-div-3').hide();
            return true;
        } else {
            $('#error-div-3').show();
            return false;
        }
    });
    // End Form Validation

    $('#credit-debit-agree').on('change', function() {
        if ($('#credit-debit-agree').prop('checked', true)) {
            $('#pay-now-button').prop('disabled', false);
        } else {
            $('#pay-now-button').prop('disabled', true);
        }
    });
</script>
@endpush

@push('style')
<style>
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .error {
        color: red;
    }
</style>
@endpush
