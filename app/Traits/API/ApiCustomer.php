<?php

namespace App\Traits\API;

use App\Models\Customer;

trait ApiCustomer
{
    public function allCustomer($request)
    {
        $slug = isset($request->query) ? $request->input('query') : '';

        /* $customer = Customer::where('name','LIKE', '' .$slug. '%')
            ->whereNotNull('name')->get('name'); */

        $customer = Customer::where(function($q) use ($slug) {
            $q->where('name','LIKE',''.$slug.'%')
            ->orWhere('customer_id','LIKE','%'.$slug.'%')
            ->orWhere('email','LIKE','%'.$slug.'%')
            ->orWhere('nric','LIKE','%'.$slug.'%')
            ->orWhere('phone','LIKE','%'.$slug.'%');
            })->whereNotNull('name')->get('name');

        $customer = collect($customer)->unique();

        if(count($customer)) {
            foreach ($customer as $k => $v) {
                $data[] = \Str::title($v->name);
            }

            return $data;
        }
    }

    public function searchCustomer($request)
    {
        $slug = isset($request->query) ? $request->input('query') : '';

        $customer = Customer::where('name','LIKE', '' .$slug. '%')->first();

        if($customer) {
            switch ($customer->member_status) {
                case '6001':
                    $customer->member_status_name = 'Full Advance';
                    break;
                case '6002':
                    $customer->member_status_name = 'Premium';
                    break;
                default:
                    $customer->member_status_name = 'Normal';
            }

            return $customer;
        }
    }


}
