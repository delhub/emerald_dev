<?php

namespace App\Models\Warehouse\StockTransfer;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;


class StockTransferItem extends Model
{
    use TableLogs;

    protected $table = 'wh_transfer_item';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'location_id',
        'type',
        'inv_type',
        'date',
        'invoice_number',
        'delivery_order',
        'order_data',
        'inv_reference',
    ];

    public function batchItem()
    {
        return $this->belongsTo('App\Models\Products\WarehouseBatchItem', 'batch_item_id', 'id');
    }
}
