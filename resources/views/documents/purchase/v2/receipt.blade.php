<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bujishu - Receipt {{ $purchase->purchase_number }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
     @include('layouts.favicon')
    <style>
        * {
            padding: 0;
            margin: 0;
            font-family: 'Lato', sans-serif;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: white;
            text-align: center;
            line-height: 35px;
        }

        .text-grey {
            color: #58595b;
        }

        /* table,
        td,
        tr {
            border: solid black 1px;
        } */
        .topBottomBorder {
            padding: 13px 0px;
            border-top: 1.5px solid rgba(255, 204, 0, 0.6);
            border-bottom: 1.5px solid #ffcc00
        }
        .add-padding{
            padding: 13px 0px;
        }
        .topGold {
            border-top: 1px solid #ffcc00;
        }
        .bottomGold {
            border-bottom: 1px solid #ffcc00;
        }

        .totalAmount {
            border-top: 1px solid #ffcc00;
            width: 134px;
            text-align: center;
            padding: 12px 10px;
            background-color: #fef5d6;
            font-weight: 600;
            font-size: 16px;

        }

        .totalTitle {
            border-top: 1px solid #ffcc00;
            width: 151px;
            padding: 12px 10px;
            text-align: right;
            font-weight: 600;
            font-size: 16px;
        }

        .paymentReceived {
            border-top: 1px solid #ffcc00;
            width: 605px;
            padding: 12px 10px;
        }
        td img.my-img1{
            object-fit: contain!important;
            width: 100px!important;
            /* height: 80px!important; */
        }
        td img.my-img{
            object-fit: contain!important;
            width: 70px!important;
            float:right;
            margin-right:10px;
            /* height:50px!important; */
        }
    </style>
</head>

<body style="margin: 0; padding: 0; height: 100vh;">
<?php

    // Declare batch
    $batchs = array();
    $batch_count = 1;
    $item_count = 0;
    $batchs[$batch_count] = collect();

    // Calculate Subtotal
    $subtotalPrice = 0;
    $deliveryFee = 0;
    $installationFee = 0;
    $rebateFee = 0;
    $balanceDue = 0;
    $gst = 0;
    $grandTotal = 0;

    $key = 0;

    $deliveryFee = ($purchase->getFeeAmount('shipping_fee') != NULL) ? $purchase->getFeeAmount('shipping_fee') : NULL;
    $installationFee = ($purchase->getFeeAmount('installation_fee') != NULL) ? $purchase->getFeeAmount('installation_fee') : NULL;
    $rebateFee = ($purchase->getFeeAmount('rebate_fee') != NULL ) ? $purchase->getFeeAmount('rebate_fee') : NULL;

    foreach ($purchase->sortItems() as $item){

        $subtotalPrice = $subtotalPrice + $item->subtotal_price;
        if($purchase->getFeeAmount('shipping_fee') == NULL) $deliveryFee += $item->delivery_fee;
        if($purchase->getFeeAmount('installation_fee') == NULL) $installationFee += $item->installation_fee;
        if($purchase->getFeeAmount('rebate_fee') == NULL) $rebateFee += $item->rebate;

        $batchs[$batch_count]->add($item);
        $item_count++;

        if ($item_count >= 9 * $batch_count ){
            $batch_count++;
            $batchs[$batch_count] = collect();
        };
    }

    $gst = ($subtotalPrice + $deliveryFee + $installationFee - $rebateFee) * 0.07 ;

    if($purchase->country_id != 'SG'){
        $grandTotal = ($subtotalPrice   - $rebateFee) + ($deliveryFee + $installationFee);
    } else {
        $grandTotal = ($subtotalPrice + $deliveryFee + $installationFee  - $rebateFee) + $gst ;
    }

    // $successPayment = isset($purchase->successfulPayment) ? $purchase->successfulPayment->amount : 0;

    // $balanceDue = $grandTotal - $successPayment;

?>

    @foreach( $batchs as $batch_id => $batch)
    <!-- First Page -->
    <div style="page-break-inside: avoid; width: 100%; height: 1385px; position: relative; padding-top: 1%; padding-bottom: 1%;">
        <!-- Header -->
        @include('documents.purchase.v2.header')

        <!-- Second Header -->
        <div style="margin: 10px 40px;">
            <table style="width: 907px;">
                <tr>
                    <td style="width:453.5px;">
                        <table>
                            <tr>
                                <td class="text-grey"
                                    style="padding:0 15px;border-bottom:2px solid #ffcc00;width:450px; font-size:18px;">
                                    PURCHASER :
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0 15px;">
                                    <br>
                                    <p style="font-size: 1rem; line-height: 1.2;">
                                        {{ $purchase->user->userInfo->shippingAddress->address_1 }}
                                    </p>
                                    <p style="font-size: 1rem; line-height: 1.2;">
                                        {{ $purchase->user->userInfo->shippingAddress->address_2 }}
                                    </p>
                                    @if (isset($purchase->user->userInfo->shippingAddress->address_3))
                                    <p style="font-size: 1rem; line-height: 1.2;">
                                        {{ $purchase->user->userInfo->shippingAddress->address_3 }}
                                    </p>
                                    @endif
                                    <p style="font-size: 1rem; line-height: 1.2;">
                                        {{ $purchase->user->userInfo->shippingAddress->postcode }}
                                    </p>
                                    <p style="font-size: 1rem; line-height: 1.2;">
                                        @php
                                            if (!empty($purchase->user->userInfo->shippingAddress->cityKey->id)) {
                                                echo $purchase->user->userInfo->shippingAddress->cityKey->city_name;
                                            }else{
                                                echo $purchase->user->userInfo->shippingAddress->city;
                                            }
                                        @endphp
                                    </p>
                                    <p style="font-size: 1rem; line-height: 1.2;">
                                        {{ $purchase->user->userInfo->shippingAddress->state->name }}
                                        Malaysia.
                                    </p>
                                    <br>
                                    <p style="font-size: 1rem; margin-top: 2px;">
                                        Attn :
                                        <span style="font-weight:800;">
                                            {{ $purchase->user->userInfo->full_name }}
                                        </span>
                                    </p>
                                    <p style="font-size: 1rem; margin-top: 2px;">
                                        Tel :
                                        <span style="font-weight:800;">
                                            {{ $purchase->user->userInfo->contacts->first()->contact_num }}
                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding:0 15px;">
                        <table>
                            <tr>
                                <td style="font-size: 1.8rem; color: #ffcc00; font-weight: 700;">
                                    OFFICIAL RECEIPT
                                </td>
                            </tr>
                            <tr>
                                <td style="width:453.5px;">
                                    <table style="padding:10px; background-color:#fef5d6;border-radius:10px;">
                                        <tr>
                                            <td style="width:221px;">OFFICIAL RECEIPT NO.</td>
                                            <td style="width:21px;">:</td>
                                            <td style="width:211px;">{{$purchase->receipt_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>INVOICE NO.</td>
                                            <td>:</td>
                                            <td>{{$purchase->getFormattedNumber()}}</td>
                                        </tr>
                                        <tr>
                                            <td>Date</td>
                                            <td>:</td>
                                            <td> {{$purchase->purchase_date}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Payment Method</td>
                                            <td>:</td>
                                            <td>
                                                <span style="text-transform: capitalize;">
                                                    {{$purchase->purchase_type}}
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reference No</td>
                                            <td>:</td>
                                            <td>
                                                <span style="text-transform: capitalize;">
                                                    {{$purchase->offline_reference}}
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Received in</td>
                                            <td>:</td>
                                            <td>
                                                <span style="text-transform: capitalize;">
                                                    PBB
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>
                                                {{$purchase->user->userInfo->account_status == '1' ? 'DC Customer' : 'Bujishu Customer'}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Page</td>
                                            <td>:</td>
                                            <td>
                                                {{ $batch_id }}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <!-- End Second Header -->

        <!-- Content -->
        <div style="margin: 10px 40px;">

            <table cellspacing="0" cellpadding="0">
                <tr style="text-align: center;font-size:16px;background-color: #fef5d6;font-weight:600;">
                    <td class="topGold add-padding" style="width: 76.5px;">No.</td>
                    <td class="topGold add-padding" style="width: 236.5px;">Product ID</td>
                    <td class="topGold add-padding" style="width: 446.5px;">Description</td>
                    <td class="topGold add-padding" style="width: 146.5px;">Amount ({{country()->country_currency}})</td>
                </tr>
                @foreach ($batch as $item)
                    <tr class=" @if($item->bundle_id == 0) op @else non-op @endif">
                        <td @if($item->bundle_id == 0) class="topGold" @endif style="text-align: center;padding:10px;">
                            @php
                                if (($item->bundle_id == 0)) {
                                    $batchs[$key] = $item;
                                    $key++;
                                    echo $key;
                                }
                            @endphp
                        </td>

                        <td @if($item->bundle_id == 0) class="topGold" @endif style="padding:10px;text-align: center;word-wrap: break-word;">
                            @if ($item->bundle_id == 0)
                                {{ $item->product_code }}
                            @endif
                        </td>

                        <td @if($item->bundle_id == 0) class="topGold" @endif style="padding:10px;">
                            <table cellspacing=" 5" style="max-height:100px;">
                                <tr>
                                    <td style="width: 110px">
                                        @if (isset($item->product_information['product_color_img']) && $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first() != NULL)

                                            @php
                                                $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_img'])->first()
                                            @endphp

                                            <img class="mw-100 rounded d-inline @if($item->bundle_id == 0) my-img1 @else my-img @endif" style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};display: inline-block; margin-right: 12px;" src="{{ asset('storage/' . $value->path. '' . $value->filename) }}">

                                        @elseif(isset($item->product_information['product_color_code']) &&              $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first() !=NULL)

                                            @php
                                                $value = $item->product->parentProduct->images->where('id',$item->product_information['product_color_code'])->first()
                                            @endphp

                                            <img class="mw-100 rounded d-inline my-img1" style="background:{{isset($item->product_information['product_color_code']) ? $item->product_information['product_color_code'] : ''}};display: inline-block; margin-right: 12px;">

                                        @elseif($item->product->parentProduct->images->isnotEmpty())

                                            <img class="mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif "
                                            src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}" style="display: inline-block;float:right;background-size:contain;object-fit: contain;"
                                            alt="">

                                        @else

                                            <img class="mw-100 rounded d-inline @if ($item->bundle_id == 0) my-img1 @else my-img @endif "
                                            src="{{ asset('assets/images/errors/image-not-found.png') }}" style="display: inline-block;float:right;background-size:contain;object-fit: contain;"
                                            alt="">

                                        @endif
                                    </td>

                                    <td style="width: 241px">
                                        <p style=" vertical-align: top; text-align: left;">
                                            <!-- Product Name -->
                                            <span style="font-weight: 600;">{{ $item->product->parentProduct->name }}</span>
                                            @if($item->product_id != 175 || $item->product_id != 176)
                                                @if(array_key_exists('product_color_name', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Color: {{ $item->product_information['product_color_name'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_size', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Size: {{ $item->product_information['product_size'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_curtain_size', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Curtain Model: {{ $item->product_information['product_curtain_size'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_temperature', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Color Temperature:
                                                    <br />{{ $item->product_information['product_temperature'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_miscellaneous', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    <br />{{ $item->product_information['product_miscellaneous'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('invoice_number', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Invoice Number:
                                                    <br />{{ $item->product_information['invoice_number'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_preorder_date', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Pre Order
                                                    Delivery:<br />{{ $item->product_information['product_preorder_date'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_order_remark', $item->product_information))
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Remarks:<br />{{ $item->product_information['product_order_remark'] }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_order_selfcollect', $item->product_information) &&
                                                $item->product_information['product_order_selfcollect'])
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Self Collection: Yes
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_order_tradein', $item->product_information) &&
                                                $item->product_information['product_order_tradein'])
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    Rebate: - RM
                                                    {{ number_format($item->product_information['product_order_tradein'] / 100),2 }}
                                                </span>
                                                @endif
                                                @if(array_key_exists('product_installation', $item->product_information) &&
                                                $item->product_information['product_installation'])
                                                <br>
                                                <span style="font-size: .8rem;">
                                                    {{($item->installationCategories()!=NULL) ? $item->installationCategories()->cat_name : ''}}
                                                </span>
                                                @endif
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td @if($item->bundle_id == 0) class="topGold" @endif style=" text-align: center;padding:10px;background-color: #fef5d6;display:none;">
                            @if ($item->bundle_id == 0) {{ number_format(($item->subtotal_price / 100), 2) }} @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <!-- End Content -->

        <!-- Total Footer -->
        <div style="margin: 10px 40px; position: fixed; bottom: 40; width: 904px; z-index:-1;">
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 605px;">&nbsp;</td>
                    <td style="width: 151px;">&nbsp;</td>
                    <td style="background-color: #fef5d6;height:780px;">&nbsp;</td>
                </tr>
                <tr>
                    <td class="paymentReceived text-grey" rowspan="6" style="padding: 20px;">
                        <p style="font-size: 1.15rem; font-weight: 600; margin-bottom: 6px;">
                            REMARK
                            <p>
                                <div style="background-color: #fef5d6; border-radius: 10px; width: 45%;">

                                </div>
                                <p style="font-size: .75rem; color: #000000; margin-top: 6px;">
                                    The issuance of this Official Receipt shall be demeed as your
                                    acceptance of the terms and conditions of this Disclaimer. The Bujishu Agent Agreement shall only become effective after full payment is received by the Company.
                                </p>
                    </td>
                </tr>
                <tr>
                    <td class="totalTitle" style=" border-bottom: 1px solid #ffcc00;color: #ffcc00;">
                        Total
                    </td>
                    <td class=" totalAmount" style="background-color: #ffcc00;">
                        {{-- {{ number_format(($subtotalPrice / 100), 2)}} --}}
                        @if ($grandTotal <= '0')
                            {{ number_format(($grandTotal / 100), 2)}}
                        @else
                            0.00
                        @endif

                    </td>
                </tr>

            </table>
        </div>
        <!-- End Total Footer -->

        <!-- Footer -->
        <p class=" text-grey"
            style="position: absolute; bottom: 48; left: 0; right: 0; width: 100%; text-align: center; margin: 0; font-size: .85rem;">
            This is a computer generated invoice, no signature is required.</p>
        <div style="position: absolute; bottom: 0; left: 0; right: 0; width: 100%; border-top: 32px solid #ffcc00;">
        </div>
        <!-- End Footer -->
    </div>
    <!-- End First Page -->
    @endforeach

</body>

</html>
