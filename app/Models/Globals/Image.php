<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // Set table
    protected $table = 'images';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'path',
        'filename',
        'default',
        'brand',
        'imageable_id',
        'imageable_type'
    ];

    /**
     * Get the owning imageable model.
     */
    public function imageable()
    {
        return $this->morphTo;
    }
    public function getUrlAttribute(){
        return asset('storage/' . $this->path .  $this->filename) ;

    }
     /**
     * Get all curtain icon.
     */
    public function iconforCurtain()
    {
        return $this->belongsTo('App\Models\Globals\ProductAttribute', 'id');
    }
}
