@php
$panels = \App\Models\Users\Panels\PanelInfo::all();
@endphp

<div class="sidebar" id="nav">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/administrator">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
        @role('administrator')

            <li class="nav-title">Shop</li>
            <li class="nav-item nav-dropdown" :class="{open: ProductisToggled}" @@click="ProductisToggled = !ProductisToggled">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-drop"></i> Products</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{route('administrator.product')}}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Products
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown" :class="{open: PanelisToggled}" @@click="PanelisToggled = !PanelisToggled">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-drop"></i> Products By Panel</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/products/panels" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Products
                        </a>
                    </li>
                </ul>
                {{-- <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{route('administrator.v1.products.panels.price')}}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Products Prices
                        </a>
                    </li>
                </ul> --}}
            </li>

            <li class="nav-item nav-dropdown" :class="{open: CatisToggled}" @@click="CatisToggled = !CatisToggled">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-drop"></i> Categories</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/categories" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Categories
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="{{ route('administrator.courier.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>Courier
                </a>
            </li>

            <li class="nav-item nav-dropdown" :class="{open: UserisToggled}" @@click="UserisToggled = !UserisToggled">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Users
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/users" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Users
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown" :class="{open: PaymentisToggled}" @@click="PaymentisToggled = !PaymentisToggled">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Payments
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/payments" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> All Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=pending" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Pending Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=failed" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Failed Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=on-hold" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> On Hold Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=paid" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Paid Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=comm-cpcb" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> CP/CB Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=cancel" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Cancelled Payments
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/administrator/payments?paymentStatus=refund" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Refunded Payments
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('administrator.sidemenus.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>Side Menu
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('administrator.testbanners.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>Banner
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('administrator.faq.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>FAQ
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('administrator.vqs.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>
                    Archimedes
                </a>
            </li>

            <li class="nav-item nav-dropdown" :class="{open: ShipisToggled}" @@click="ShipisToggled = !ShipisToggled">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i>  Shipping / Installation
                </a>
                {{-- summary (panel) --}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                    <a href="{{ route('administrator.zones.index') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Zones
                        </a>
                    </li>
                </ul>
                {{-- summary (customer) --}}
                {{-- <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.zone_location.index') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Zones Location
                        </a>
                    </li>
                </ul> --}}

                <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a href="{{ route('administrator.cities.index') }}" class="nav-link">
                        <i class="nav-icon icon-drop"></i>
                        Cities
                    </a>
                </li>
                </ul>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.shipping-installations.ship_category.index') }}" class="nav-link">
                            <i class="nav-icon icon-drop"></i>
                            Ship Categories
                        </a>
                    </li>
                </ul>
            </li>


            <li class="nav-item nav-dropdown" :class="{open: OderisToggled}" @@click="OderisToggled = !OderisToggled">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Order Tracking
                </a>
                {{-- summary (panel) --}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                    <a href="{{ route('administrator.ordertracking.summary') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Order Tracking Summary (Panel)
                        </a>
                    </li>
                </ul>
                {{-- summary (customer) --}}
                 <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.ordertracking.summary.customer') }}" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Order Tracking Summary (Customer)
                        </a>
                    </li>
                </ul>
                {{-- panel --}}
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.ordertracking.detail') }}" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i>Order Tracking Detail
                        </a>
                    </li>
                </ul>

            </li>

            {{-- <li class="nav-item">
                <a href="{{ route('administrator.tax.index') }}" class="nav-link">
                    <i class="nav-icon icon-drop"></i>Tax
                </a>
            </li> --}}

            <li class="nav-item nav-dropdown {{ Route::currentRouteName() == 'administrator.discount.index' ? 'open' : ''  }} {{ Route::currentRouteName() == 'administrator.discount.rule' ? 'open' : ''  }}" :class="{open: DiscountisToggled}" @@click="DiscountisToggled = !DiscountisToggled" >
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Discount
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.discount.selector') }}" class="nav-link {{ request()->route()->uri == 'administrator/discount/product-selector' ? 'active' : ''  }}">
                        <i class="nav-icon icon-puzzle"></i> Product Selector
                        </a>
                    </li>
                </ul>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                    <a href="{{ route('administrator.discount.index') }}" class="nav-link {{ request()->route()->uri == 'administrator/discount' ? 'active' : ''  }}">
                        <i class="nav-icon icon-puzzle"></i> Code-list
                        </a>
                    </li>
                </ul>
                 <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.discount.rule') }}" class="nav-link {{ request()->route()->uri == 'administrator/discount/rule' ? 'active' : ''  }}">
                        <i class="nav-icon icon-puzzle"></i> Rule-list
                        </a>
                    </li>
                </ul>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route('administrator.discount.usage') }}" class="nav-link {{ request()->route()->uri == 'administrator/discount/usage' ? 'active' : ''  }}">
                        <i class="nav-icon icon-puzzle"></i> Usage-list
                        </a>
                    </li>
                </ul>
            </li>

        @endrole

        @role('redemption')
        <li class="nav-item">
            <a href="{{ route('administrator.redemption') }}" class="nav-link">
                <i class="nav-icon icon-drop"></i>
                Redemption
            </a>
        </li>
        @endrole



            {{-- All dashboard --}}
            {{-- <li class="nav-item nav-dropdown">
                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Dashboard
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/administrator/agent" class="nav-link">
                        <i class="nav-icon icon-puzzle"></i> Agent Categories Sales
                        </a>
                    </li>
                </ul>
            </li> --}}

            {{-- currently not using
            <li class="nav-item nav-dropdown">

                <a href="#" class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-drop"></i> Report
                </a>

                <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/administrator/report" class="nav-link">
                            <i class="nav-icon icon-puzzle"></i> Panel Report Summary
                            </a>
                        </li>
                </ul>

            </li> --}}



            <li class="divider"></li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
@push('script')
<script>
    //isToggled :class="{open: isToggled}" @@click="isToggled = !isToggled"
    var nav = new Vue({
    el: '#nav',
    data: {
        ProductisToggled: false,
        PanelisToggled: false,
        CatisToggled: false,
        ShipisToggled: false,
        UserisToggled: false,
        PaymentisToggled: false,
        OderisToggled: false,
        DiscountisToggled: false
    },
    methods: {
        //
    }
    });
</script>
@endpush
