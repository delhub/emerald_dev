@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection



{{-- {{ dd('asdasd') }} --}}
<!-- desktop -->
<div class="top-banner">
    <!-- sample for test dynamic banner Web  start -->
    @if (count($testbanners) > 0)
        <section id="slideshow" class="hidden-sm">
            <div class="slick">
                @foreach ($testbanners as $testbanner)
                    @if ($testbanner->banner_show == 1 && $testbanner->banner_type == 'home' && $testbanner->country_id == $banner_county)
                        <div>
                            <a href="{{ $testbanner->banner_link }}" alt="{{ $banner_county }}">
                                <img class="fml-banner"
                                    src="{{ asset('/storage/uploads/banner/' . $testbanner->banner_web) }}" alt="">
                            </a>
                        </div>
                    @else

                    @endif
                @endforeach
            </div>
        </section>
        <!-- sample for test dynamic banner Web  end -->
        <!-- sample for test dynamic banner Mobile  start -->
        <section id="slideshow" class="hidden-md">
            <div class="slick">
                @foreach ($testbanners as $testbanner)
                    @if ($testbanner->banner_show == 1 && $testbanner->banner_type == 'home' && $testbanner->country_id == $banner_county)
                        <div>
                            <a href="{{ $testbanner->banner_link }}">
                                <img class="image-slideshow"
                                    src="{{ asset('/storage/uploads/banner/' . $testbanner->banner_mobile) }}" alt="">
                            </a>
                        </div>
                    @else

                    @endif
                @endforeach
            </div>
        </section>
    @endif
</div>
<div class="bottom-content">
    {{-- <div class="row">
        <div class="col-md-12 text-center">
            <h1>Shop By<span>Categories</span></h1>
            <div class="fml-iconlist text-alignment my-pw mx-md-1 mx-2">
                <a href="/shop/category/adult">
                    <img class="icon-image" src="/images/icons/icon-adult.png" alt="Icon">
                    <h5 class="text-center" style="font-size:15px;"><b>Adult</b></h5>
                </a>
                <a href="/shop/category/elderly">
                    <img class="icon-image" src="/images/icons/icon-eldest.png" alt="Icon">
                    <h5 class="text-center" style="font-size:15px;"><b>Elderly</b></h5>
                </a>
                <a href="/shop/category/kids">
                    <img class="icon-image" src="/images/icons/icon-kids.png" alt="Icon">
                    <h5 class="text-center" style="font-size:15px;"><b>Kids</b></h5>
                </a>
            </div>
        </div>
    </div> --}}
    <section class="fml-special-pro my-mb-3 my-pt-3">
        <div>
            <div class="row special-pro">
                <div class="col-md-12 text-center my-mb-2 my-mt-2">
                    <h1>
                        Key Selection <span>Products</span>
                        <div class="info-hrline-red"></div>
                    </h1>
                </div>
                <div class="center my responsive slider">
                    @foreach ($products as $product)
                        <div class="special-box">
                            <div class="special-item-box">
                                <div class="special-left">

                                    @if ($product->defaultImage !== null)
                                        <img class="imgproduct"
                                            src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                            alt="">
                                    @endif

                                </div>
                                <div class="special-right">
                                    @if ($product->panelProduct->attributes->first()->getPriceAttributes('discount_percentage'))
                                        <div class="orfer-special">
                                            <div class="sale-special">
                                                <p>{{ $product->panelProduct->attributes->first()->getPriceAttributes('discount_percentage') }}%
                                                    <span>OFF</span>
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="name-special">
                                        <p class="special-tittle">{{ $product->name }}</p>
                                        <p class="special-price">{{ country()->country_currency }}
                                            {{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                        </p>
                                    </div>
                                    @php
                                        $slug = $product->categories->where('type', 'shop')->first();
                                        $url = '#';
                                        if ($slug) {
                                            $url = '/shop/product/' . $slug->slug . '/' . $product->name_slug;
                                        }

                                    @endphp
                                    <a href="{{ $url }}" class="fml-btn buy-btn link-product"
                                        data-bs-original-title="" title=""> Buy Now</a>
                                    {{-- <div class="icon-special">
                                    <div class="icon-viewmore">
                                        <a href="/shop/product/{{ $slug }}/{{ $product->parentProduct->name_slug}}?panel={{$product->panel_account_id}}" class="link-product">
                                            <i class="fa fa-arrow-circle-o-right icon-special-2" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div> --}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    {{-- web categories --}}
    <section class="fml-special-lead my-mb-5 my-pt-3">
        <div class="fml-container">

            <div class="lead-tit">
                <div class="lead-center">
                    <h5>
                        Lead by<span>Categories</span>
                    </h5>
                </div>
                <div class="bot-hole"></div>
            </div>
            <div class="lead-contant">
                <div class="top-bg">
                    <div class="inner-bg">
                        <img src="{{ asset('images/formula/left-sb.png') }}" class="img-sb-l" alt="">
                        <img src="{{ asset('images/formula/right-sb.png') }}" class="img-sb-r" alt="">
                    </div>
                </div>
                <div class="bot-bg lead-bg">
                    <div class="inner-bg">
                        <div id="owl-leadbyctg" class="owl-carousel home-leadbyctg owl-theme">
                            @foreach ($categories->where('featured', 1) as $category)
                                <div class="item-ctg">
                                    <a href="/shop/category/{{ $category->slug }}" data-bs-original-title=""
                                        title="">
                                        @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                            <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" alt="Icon"
                                                class="icon-image addhot">
                                        @else
                                            <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                        @endif
                                        <p>{{ $category->name }}</p>

                                    </a>
                                </div>
                            @endforeach
                            {{-- sample to show slider start --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fml-container">
            <img class="home-red-ball" src="{{ asset('/images/formula/redball.png') }}" alt="">
            <img class="home-yellow-ball" src="{{ asset('/images/formula/yellowball.png') }}" alt="">
            <img class="home-red-ball2" src="{{ asset('/images/formula/redball.png') }}" alt="">
            <img class="home-blue-ball" src="{{ asset('/images/formula/bluebox.png') }}" alt="">
            <div class="shopby-tit">
                <div class="shopby-center">
                    <h5>
                        Shop By Categories
                    </h5>
                </div>
            </div>
            <div class="shopby-content">
                <div class="shopby-body lead-bg">
                    {{-- change to slider owl --}}
                    <div class="inner-top">
                        <div class="shopbyTwo owl-carousel owl-theme">
                            @foreach ($categories->where('featured', 2) as $category)
                                <div class="item">
                                    {{-- <div class="item-ctg"> --}}
                                    <a href="/shop/category/{{ $category->slug }}" data-bs-original-title=""
                                        title="">
                                        @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                            <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" alt="Icon"
                                                class="icon-image">
                                        @else
                                            <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                        @endif
                                        <p>{{ $category->name }}</p>
                                    </a>
                                    {{-- </div> --}}
                                </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="inner-bot">
                        <div id="owl-shopbyone" class="owl-carousel home-pg owl-theme">

                            @foreach ($categories->where('featured', 2) as $category)
                                @foreach ($category->products->where('hot_selection', 1)->where('product_status', 1) as $product)
                                    <div class="item"
                                        data-imageLink="{{ '/storage/uploads/images/products/' . $product->product_code . '/' . $product->defaultImage->filename }}">
                                        <a href="/shop/product/{{ $category->slug }}/{{ $product->name_slug }}">
                                            @if ($product->defaultImage)
                                                <img src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                                    alt="">
                                            @else
                                                <img src="{{ asset('/images/formula/no-img.png') }}" alt="Icon"
                                                    class="icon-image" alt="">
                                            @endif
                                            <div class="item-shortprice">
                                                <p><span>{{ country()->country_currency }}</span>{{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                                </p>
                                            </div>
                                            <p class="ctg-name">{{ $product->name }}</p>
                                            <div class="item-shortprice-buy">
                                                <p>
                                                    Buy Now
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            @endforeach
                            {{-- sample if no images start --}}
                            {{-- <div class="item">
                                <a href="#">
                                    <img src="{{ asset('/images/formula/no-img.png') }}" alt="Icon"
                                        class="icon-image">
                                    <div class="item-shortprice">
                                        <p><span>MYR</span>999.99</p>
                                    </div>
                                    <p>Test images sample</p>
                                </a>
                            </div> --}}
                            {{-- sample if no images end --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fml-container">
            <img class="home-red-ball3" src="{{ asset('/images/formula/redball.png') }}" alt="">
            <img class="home-yellow-ball2" src="{{ asset('/images/formula/yellowball.png') }}" alt="">
            <div class="shopby-tit">
                <div class="shopby-center">
                    <h5>
                        Shop By Categories
                    </h5>
                </div>
            </div>
            <div class="shopby-content">
                <div class="shopby-body lead-bg">
                    <div class="inner-top">
                        {{-- change to slider owl --}}
                        <div class="shopbyTwo owl-carousel owl-theme">
                            @foreach ($categories->where('featured', 3) as $category)
                                <div class="item">
                                    {{-- <div class="item-ctg"> --}}
                                    <a href="/shop/category/{{ $category->slug }}" data-bs-original-title=""
                                        title="">
                                        @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                            <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" alt="Icon"
                                                class="icon-image">
                                        @else
                                            <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                        @endif
                                        <p>{{ $category->name }}</p>
                                    </a>
                                    {{-- </div> --}}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="inner-bot">
                        <div id="owl-shopbytwo" class="owl-carousel home-pg owl-theme">
                            @foreach ($categories->where('featured', 3) as $category)
                                @foreach ($category->products->where('hot_selection', 1)->where('product_status', 1) as $product)
                                    <div class="item">
                                        <a href="/shop/product/{{ $category->slug }}/{{ $product->name_slug }}">
                                            @if ($product->defaultImage)
                                                <img src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                                    alt="">
                                            @else
                                                <img src="{{ asset('/images/product_item/no-img.png') }}" alt="Icon"
                                                    class="icon-image" alt="">
                                            @endif
                                            {{-- @if (file_exists(public_path('/storage/uploads/images/products/' . $product->product_code . '/' . $product->defaultImage->filename)))
                                                <img src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}"
                                                    alt="">
                                            @else
                                                <img src="{{ asset('/images/formula/no-img.png') }}" alt="Icon"
                                                    class="icon-image">
                                            @endif --}}
                                            <div class="item-shortprice">
                                                <p><span>{{ country()->country_currency }}</span>{{ number_format($product->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100, 2) }}
                                                </p>
                                            </div>
                                            <p class="ctg-name">{{ $product->name }}</p>
                                            <div class="item-shortprice-buy">
                                                <p>
                                                    Buy Now
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- mobile categories --}}
    <section class="fml-homelist-icon my-mb-3 my-pt-3">
        <div class="row">
            <div class="col-md-11 col-sm-12 pro-bg fml-muiltiCtg">

                <div class="categeries-box text-center">
                    {{-- <h1 class="my-mb-1">
                        Shop By <span>Categories</span>
                        <div class="info-hrline-red"></div>
                    </h1> --}}
                    {{-- <div class="iconlist-box e-desktop">
                        @foreach ($desktop_seq as $seq)
                            @php $popularCategory = isset($popularCategories[$seq]) ? $popularCategories[$seq] : '';
                                if (!$popularCategory) {
                                    echo $seq;
                                    continue;
                                }
                            @endphp
                            <div class="ttlp">
                                <a href="/shop/category/{{ $popularCategory->slug }}">
                                    <img class="icon-image"
                                        src="/images/icons/category/{{ $popularCategory->slug }}.png" alt="Icon">
                                    <p>{{ $popularCategory->name }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div> --}}
                    <div class="group-ctg">
                        <div class="categories-slider owl-carousel">
                            <div class="item">
                                <div class="group-tit">
                                    <h1 class="my-mb-1">
                                        Lead By <span>Categories</span>
                                        <div class="info-hrline-red"></div>
                                    </h1>
                                </div>
                                <div class="group-list">
                                    @foreach ($categories->where('featured_mobile', 1) as $category)
                                        <div class="group-item">
                                            <a href="/shop/category/{{ $category->slug }}">
                                            @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                                <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" 
                                                    alt="Icon">
                                            @else
                                                <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                            @endif
                                                <p>{{ $category->name }}</p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="item">
                                <div class="group-tit">
                                    <h1 class="my-mb-1">
                                        Shop By Categories
                                        <div class="info-hrline-red"></div>
                                    </h1>
                                </div>
                                <div class="group-list">
                                    @foreach ($categories->where('featured_mobile', 2)->take(9) as $category)
                                        <div class="group-item">
                                            <a href="/shop/category/{{ $category->slug }}">
                                                @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                                    <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" alt="Icon"
                                                        class="icon-image">
                                                @else
                                                    <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                                @endif
                                                <p>{{ $category->name }}</p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="item">
                                <div class="group-tit">
                                    <h1 class="my-mb-1">
                                        Shop By Categories
                                        <div class="info-hrline-red"></div>
                                    </h1>
                                </div>
                                <div class="group-list">
                                    @foreach ($categories->where('featured_mobile', 3)->take(9) as $category)
                                        <div class="group-item">
                                            <a href="/shop/category/{{ $category->slug }}">
                                                @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                                <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" alt="Icon"
                                                    class="icon-image">
                                                @else
                                                    <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                                @endif
                                                <p>{{ $category->name }}</p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="item">
                                <div class="group-tit">
                                    <h1 class="my-mb-1">
                                        Shop By Categories
                                        <div class="info-hrline-red"></div>
                                    </h1>
                                </div>
                                <div class="group-list">
                                    @foreach ($categories->where('featured_mobile', 4)->take(9) as $category)
                                        <div class="group-item">
                                            <a href="/shop/category/{{ $category->slug }}">
                                                @if($category->image && file_exists(public_path('/images/icons/category/' . $category->image->filename)))
                                                <img src="{{ asset('/' . $category->image->path . $category->image->filename) }}" alt="Icon"
                                                    class="icon-image">
                                                @else
                                                    <img src="{{ asset('assets/images/errors/image-not-found.png') }}" class="mw-100" style="border-radius: 10px;" >
                                                @endif
                                                <p>{{ $category->name }}</p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="iconlist-box e-mobile">
                        @foreach ($categories->where('featured_mobile', 1)->take(9) as $category)
                        <div class="ttlp">page1
                            <a href="/shop/category/{{ $category->slug }}">
                                <img src="/images/icons/category/{{ $category->slug }}.png" alt="Icon" class="icon-image">
                                <p>{{ $category->name }}</p>
                            </a>
                        </div>
                        @endforeach
                        @foreach ($categories->where('featured_mobile', 2)->take(9) as $category)
                        <div class="ttlp">page2
                            <a href="/shop/category/{{ $category->slug }}">
                                <img src="/images/icons/category/{{ $category->slug }}.png" alt="Icon" class="icon-image">
                                <p>{{ $category->name }}</p>
                            </a>
                        </div>
                        @endforeach
                        @foreach ($categories->where('featured_mobile', 3)->take(9) as $category)
                        <div class="ttlp">page3
                            <a href="/shop/category/{{ $category->slug }}">
                                <img src="/images/icons/category/{{ $category->slug }}.png" alt="Icon" class="icon-image">
                                <p>{{ $category->name }}</p>
                            </a>
                        </div>
                        @endforeach
                        @foreach ($categories->where('featured_mobile', 4)->take(9) as $category)
                        <div class="ttlp">page4
                            <a href="/shop/category/{{ $category->slug }}">
                                <img src="/images/icons/category/{{ $category->slug }}.png" alt="Icon" class="icon-image">
                                <p>{{ $category->name }}</p>
                            </a>
                        </div>
                        @endforeach

                        <!-- bucgen -->
                        @foreach ($mobile_seq as $seq)
                            @php $popularCategory = isset($popularCategories[$seq]) ? $popularCategories[$seq] : '';
                                if (!$popularCategory) {
                                    echo $seq;
                                    continue;
                                }
                            @endphp
                            <div class="ttlp">
                                <a href="/shop/category/{{ $popularCategory->slug }}">
                                    <img class="icon-image"
                                        src="/images/icons/category/{{ $popularCategory->slug }}.png" alt="Icon">
                                    <p>{{ $popularCategory->name }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div> --}}
                </div>
                <div class="col-md-2"></div>
            </div>

    </section>
</div>

<div class="bee about-modal fml-special-pro">
    <div class="bee-bg">
        <div class="bee-bg2"></div>
        <img class="bee-blue-ball" src="{{ asset('/images/formula/bluebox.png') }}" alt="">
        <img class="bee-four-ball" src="{{ asset('/images/formula/frame9.png') }}" alt="">
        <img class="home-bee-ball2" src="{{ asset('/images/formula/yellowball.png') }}" alt="">
    </div>
    <section>
        <div class=" badge2">
            <h4 class="qualityText">Click Me</h4>
            <div class="text2">
                <div class="ani-body">
                    <div class="bee-img-r">
                        <img src="{{ asset('/images/formula/bee-back1.png') }}">
                    </div>
                    <div class="bee-img-l">
                        <img src="{{ asset('/images/formula/bee-back2.png') }}">
                    </div>
                    <div class="ani-inner">
                        <h3 class="my-mb-1">
                            Quality &
                            <span>
                                Trust
                                <div class="info-hrline-red"></div>
                            </span>
                        </h3>
                        <p>
                            We understand that looking good and feeling great are priorities, which makes us passionate
                            about
                            helping you vitalise and transform yourself in a way that works for you. Our nutritional
                            products
                            are
                            specially formulated to cater to your individual needs, from inner wellness to outer
                            radiance.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

{{-- start old banner layout --}}

{{-- <section class="fml-quality quality-bg my-mb-3 my-pt-3">
    <div class="h-btm-bg">
        <div class="fml-container">
            <div class="row">
                <div class="col-md-5"></div>
                <div class="multineed-header col-md-7 text-left mt-5">
                    <h1 class="my-mb-1 h-img-mt">
                        Quality &
                        <span>
                            Trust
                            <div class="info-hrline-red"></div>
                        </span>

                    </h1>
                    <p>
                        We understand that looking good and feeling great are priorities, which makes us passionate
                        about helping you vitalise and transform yourself in a way that works for you. Our
                        nutritional
                        products are specially formulated to cater to your individual needs, from inner wellness to
                        outer radiance.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section> --}}

{{-- end old banner layout --}}

@endsection

@push('script')
<script>
    const sliderimg = document.querySelector('#slideshow .slick');

    sliderimg.style.display = "none";

    $(document).ready(() => {

        sliderimg.style.display = "block";

        $('#slideshow .slick').slick({
            dots: true,
            infinite: false,
            autoplay: true,
            autoplaySpeed: 1750,
            slidesToShow: 1,
            arrows: false,
            slidesToScroll: 1
        });

        //categories mobile slider
        $(".categories-slider").owlCarousel({
            autoplay: true,
            // rewind: true,
            /* use rewind if you don't want loop */
            margin: 10,
            /*
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            */
            loop: true,
            responsiveClass: true,
            autoHeight: true,
            dots: false,
            autoplayTimeout: 16000,
            smartSpeed: 800,
            nav: true,
            responsive: {
                0: {
                    items: 1,
                    loop: true
                },

                600: {
                    items: 1,
                    loop: true
                }
            }
        });

        //categories web - shop by both-slider
        $('.shopbyTwo.owl-carousel').owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            dots: false,
            items: 10,
            responsive: {
                600: {
                    items: 5
                },
                1024: {
                    items: 10
                },
                1000: {
                    items: 10
                }
            }
        })

    });

    document.querySelector("#app").classList.remove('app-body');

    // homepage banner bottom//
    const beeBannerbg = document.querySelector('.bee-bg2');
    const btnBee = document.querySelector('.badge2');
    const qualityText = document.querySelector('.qualityText');

    btnBee.addEventListener('click', () => {
        if (beeBannerbg.classList.contains('active')) {
            beeBannerbg.classList.remove('active');
            btnBee.classList.remove('active');
            qualityText.classList.remove('active');
            qualityText.innerHTML = "Click Me";
        } else {
            beeBannerbg.classList.add('active');
            btnBee.classList.add('active');
            qualityText.classList.add('active');
            qualityText.innerHTML = "Back";
        }

    });
</script>
@endpush
