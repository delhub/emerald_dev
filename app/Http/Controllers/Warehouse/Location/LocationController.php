<?php

namespace App\Http\Controllers\Warehouse\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\City;
use App\Models\Globals\Countries;
use App\Models\Globals\State;
use App\Models\Warehouse\Location\Wh_location;

class LocationController extends Controller
{
    public static $home;
    public static $header;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$home = 'warehouse.location.index';
        self::$header = 'Location';

        self::$create = 'warehouse.location.create';
        self::$store = 'warehouse.location.store';

        self::$edit = 'warehouse.location.edit';
        self::$update = 'warehouse.location.update';

        self::$destroy = 'warehouse.location.destroy';

        // link to view page
        // view('warehouse.location.index');
    }


    public function index()
    {
        $tables = Wh_location::orderBy('state_id', 'ASC')->paginate(50);

        return view(self::$home, compact('tables'));
    }

    public function create()
    {
        $countries = Countries::orderBy('country_name', 'ASC')->get();
        $states = State::orderBy('name', 'ASC')->get();

        return view(self::$create, compact('countries', 'states'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            // 'outlet_key' => 'required',
            'location_name' => 'required',
            'type' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_key' => 'required',
            // 'postcode' => 'required',
            // 'city_key' => 'required',
            // 'state_id' => 'required',
            // 'operation_time' => 'required',
        ]);
        // $whKey = $request->input('country_id') . $stateAbbreviation->abbreviation . $request->input('type') . '001';
        $whKey = Wh_location::WarehouseKeyFormat($request->input('country_id'), $request->input('city_key'));

        $existingdata  = Wh_location::where('location_key', $whKey)->first();

        if (!$existingdata) {
            $total = Wh_location::get();
            $ccc = count($total);

            $data = new Wh_location;
            $data->id = $ccc;
            $data->location_key = $whKey;
            $data->location_name = $request->input('location_name');
            $data->type = $request->input('type');
            $data->country_id = $request->input('country_id');
            $data->printer_email = $request->input('printer_email');
            $data->contact_number = $request->input('contact_number');
            $data->default_address = $request->input('default_address');
            $data->postcode = $request->input('postcode');
            $data->city_key = $request->input('city_key');
            $data->state_id = $request->input('state_id');
            $data->operation_time = $request->input('operation_time');

            $data->save();

            // $outlet = Wh_location::find($data->id);

            // if ($request->file('outletImage')) {
            //     // Get existing default image.
            //     $defaultImages = $data->images->where('default', 1);

            //     // Delete if default image exists.
            //     if ($defaultImages->count() != 0) {
            //         foreach ($defaultImages as $defaultImage) {
            //             $defaultImage->delete();
            //         }
            //     }

            //     // Store new default image.
            //     $image = $request->file('outletImage');
            //     $imageDestination = public_path('/storage/uploads/images/outlet/' . $data->id . '/');
            //     $imageName = $data->id . '-' . 'default' . '.' . $image->getClientOriginalExtension();
            //     $image->move($imageDestination, $imageName);

            //     // Create record in DB.
            //     $data->images()->create([
            //         'path' => 'uploads/images/outlet/' . $data->id . '/',
            //         'filename' => $imageName,
            //         'default' => 1
            //     ]);
            // }


            return redirect()->route(self::$home)->with('success', 'submit done.');
        } else {
            return back()->with(['error' => self::$header . ' already exist  !!! ']);
        }
    }

    public function edit($id)
    {
        $data = Wh_location::find($id);

        $countries = Countries::orderBy('country_name', 'ASC')->get();
        // $states = State::orderBy('name', 'ASC')->get();
        $cities  = City::orderBy('city_name', 'ASC')->where('state_id', $data->state_id)->get();

        return view(self::$edit, compact('data', 'countries', 'cities'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'location_name' => 'required',
            // 'type' => 'required',
            // 'country_id' => 'required',
            // 'state_id' => 'required',
            'city_key' => 'required',
        ]);
        $data = Wh_location::find($id);

        $data->location_name = $request->input('location_name');
        // $data->type = $request->input('type');
        // $data->country_id = $request->input('country_id');
        $data->printer_email = $request->input('printer_email');
        $data->contact_number = $request->input('contact_number');
        $data->default_address = $request->input('default_address');
        $data->postcode = $request->input('postcode');
        $data->city_key = $request->input('city_key');
        // $data->state_id = $request->input('state_id');
        $data->operation_time = $request->input('operation_time');

        $data->save();

        // if ($request->file('outletImage')) {
        //     // Get existing default image.
        //     $defaultImages = $data->images->where('default', 1);

        //     // Delete if default image exists.
        //     if ($defaultImages->count() != 0) {
        //         foreach ($defaultImages as $defaultImage) {
        //             $defaultImage->delete();
        //         }
        //     }

        //     // Store new default image.
        //     $image = $request->file('outletImage');
        //     $imageDestination = public_path('/storage/uploads/images/outlet/' . $data->id . '/');
        //     $imageName = $data->id . '-' . 'default' . '.' . $image->getClientOriginalExtension();
        //     $image->move($imageDestination, $imageName);

        //     // Create record in DB.
        //     $data->images()->create([
        //         'path' => 'uploads/images/outlet/' . $data->id . '/',
        //         'filename' => $imageName,
        //         'default' => 1
        //     ]);
        // }

        return redirect()->route(self::$home)->with('success', self::$header . ' Updates');
    }

    // public function destroy($id)
    // {
    //     $data = Wh_location::find($id);

    //     $data->delete();

    //     return redirect()->route(self::$home)->with('success', self::$header . ' Remove');
    // }
}
