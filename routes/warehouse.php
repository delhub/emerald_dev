<?php

//API
Route::group(['prefix' => 'api', 'middleware' => []], function () {
    Route::get('stock-check/{location_id}', 'Warehouse\WarehouseAPIController\WarehouseAPIController@stockCheck')
        ->name('warehouse.WarehouseAPIController.index');

    Route::post('batch-order/', 'Warehouse\WarehouseAPIController\WarehouseAPIController@batchOrder')
        ->name('warehouse.batchOrder.index');
});

Route::group(['middleware' => ['web']], function () {

    Route::group(['prefix' => 'location', 'middleware' => ['role:administrator|formula_pic|wh_warehouse']], function () { //role:wh_warehouse|administrator', '
        Route::get('/', 'Warehouse\Location\LocationController@index')
            ->name('warehouse.location.index');

        // Create.
        Route::get('/create', 'Warehouse\Location\LocationController@create')
            ->name('warehouse.location.create');

        // Store.
        Route::post('/store', 'Warehouse\Location\LocationController@store')
            ->name('warehouse.location.store');

        // edit.
        Route::get('/edit/{id}', 'Warehouse\Location\LocationController@edit')
            ->name('warehouse.location.edit');

        // update.
        Route::post('/update/{id}', 'Warehouse\Location\LocationController@update')
            ->name('warehouse.location.update');

        // destroy.
        // Route::post('/destroy/{id}', 'Warehouse\Location\LocationController@destroy')
        //     ->name('warehouse.location.destroy');
    });

    Route::group(['prefix' => 'stockAdjustment', 'middleware' => ['role:administrator|formula_pic|wh_warehouse']], function () { //role:wh_warehouse|administrator', '
        Route::get('/', 'Warehouse\StockAdjustment\StockAdjustmentController@index')
            ->name('warehouse.stockAdjustment.index');

        // Create.
        Route::get('/adjust', 'Warehouse\StockAdjustment\StockAdjustmentController@adjust')
            ->name('warehouse.stockAdjustment.adjust');

        // Store.
        Route::post('/adjustStart', 'Warehouse\StockAdjustment\StockAdjustmentController@adjustStart')
            ->name('warehouse.stockAdjustment.adjustStart');

        // edit.
        Route::get('/edit/{id}', 'Warehouse\StockAdjustment\StockAdjustmentController@edit')
            ->name('warehouse.stockAdjustment.edit');

        // update.
        Route::post('/update/{id}', 'Warehouse\StockAdjustment\StockAdjustmentController@update')
            ->name('warehouse.stockAdjustment.update');

        // destroy.
        // Route::post('/destroy/{id}', 'Warehouse\Location\LocationController@destroy')
        //     ->name('warehouse.location.destroy');
    });

    Route::group(['prefix' => 'stock-transfer', 'middleware' => ['role:wh_warehouse|administrator|formula_pic|wh_transferrer|wh_receiver']], function () {

        //Create Box
        Route::get('/create-box', 'Warehouse\StockTransfer\StockTransferController@createBox')
            ->name('warehouse.StockTransfer.create-box');

        //Print Box
        Route::get('/print-label', 'Warehouse\StockTransfer\StockTransferController@printLabel')
            ->name('warehouse.StockTransfer.print-label');

        // Transfer Box List
        Route::get('/box-list', 'Warehouse\StockTransfer\StockTransferController@boxList')
            ->name('warehouse.StockTransfer.box-list');

        //Edit Box Notes
        Route::post('/edit-box-notes/{batchID}', 'Warehouse\StockTransfer\StockTransferController@editBoxNotes')
            ->name('warehouse.StockTransfer.stockTransferList.edit-box-notes');

        // Start.
        Route::post('/transfer-start', 'Warehouse\StockTransfer\StockTransferController@transferStart')
            ->name('warehouse.StockTransfer.transferStart');

        // Scan Product
        Route::get('/scan-product', 'Warehouse\StockTransfer\StockTransferController@scanProduct')
            ->name('warehouse.StockTransfer.scanProduct');

        // Close
        Route::post('/transfer-end/{stockTransferId}', 'Warehouse\StockTransfer\StockTransferController@transferEnd')
            ->name('warehouse.StockTransfer.transferEnd');

        // Cancel Transfer
        Route::post('/transfer-cancel/{stockTransferId}', 'Warehouse\StockTransfer\StockTransferController@transferCancel')
            ->name('warehouse.StockTransfer.transferCancel');

        //Stock Transfer List
        Route::get('/stock-transfer-list', 'Warehouse\StockTransfer\StockTransferController@stockTransferList')
            ->name('warehouse.StockTransfer.stockTransferList');

        //Edit Notes
        Route::post('/edit-notes/{batchID}', 'Warehouse\StockTransfer\StockTransferController@editNotes')
            ->name('warehouse.StockTransfer.stockTransferList.edit-notes');

        //Receive
        Route::get('/receive', 'Warehouse\StockTransfer\StockTransferController@receive')
            ->name('warehouse.StockTransfer.receive');

        // Receive Start.
        Route::get('/receive-start/{stockTransferId?}', 'Warehouse\StockTransfer\StockTransferController@receiveStart')
            ->name('warehouse.StockTransfer.receiveStart');

        // Receive Scan Product
        Route::get('/receive-scan-product', 'Warehouse\StockTransfer\StockTransferController@receiveScanProduct')
            ->name('warehouse.StockTransfer.receiveScanProduct');

        // Receive Batch
        Route::get('/receive-batch', 'Warehouse\StockTransfer\StockTransferController@receiveBatch')
            ->name('warehouse.StockTransfer.receiveBatch');

        // Receive Batch Checking
        Route::post('/receive-batch-checking/{stockTransferId}', 'Warehouse\StockTransfer\StockTransferController@receiveBatchChecking')
            ->name('warehouse.StockTransfer.receiveBatchChecking');

        //Index
        Route::get('/{boxId?}', 'Warehouse\StockTransfer\StockTransferController@index')
            ->name('warehouse.StockTransfer.index');
    });

    Route::group(['prefix' => 'order', 'middleware' => ['role:wh_warehouse|administrator|formula_pic']], function () {
        Route::get('/', 'Warehouse\Order\OrderController@index')
            ->name('warehouse.order.index');

        // Route::get('/', 'Warehouse\Order\OrderController@index')
        //     ->name('warehouse.order.index');

        // // Create.
        // Route::get('/create', 'Warehouse\Location\LocationController@create')
        //     ->name('warehouse.location.create');

        // // Store.
        // Route::post('/store', 'Warehouse\Location\LocationController@store')
        //     ->name('warehouse.location.store');

        // // edit.
        // Route::get('/edit/{id}', 'Warehouse\Location\LocationController@edit')
        //     ->name('warehouse.location.edit');

        // // update.
        // Route::post('/update/{id}', 'Warehouse\Location\LocationController@update')
        //     ->name('warehouse.location.update');

        // destroy.
        // Route::post('/destroy/{id}', 'Warehouse\Location\LocationController@destroy')
        //     ->name('warehouse.location.destroy');
    });

    Route::group(['prefix' => 'b_unbundle', 'middleware' => ['role:wh_warehouse|administrator|formula_pic|wh_b_unbundle']], function () {
        Route::get('/', 'Warehouse\Bunbundle\BunbundleController@index')
            ->name('warehouse.b_unbundle.index');

        // Start.
        Route::post('/start', 'Warehouse\Bunbundle\BunbundleController@start')
            ->name('warehouse.b_unbundle.start');

        // scan product
        Route::get('/scan-product', 'Warehouse\Bunbundle\BunbundleController@scanProduct')
            ->name('warehouse.b_unbundle.scanProduct');

        // Close
        Route::get('/end/{stockTransferId}', 'Warehouse\Bunbundle\BunbundleController@end')
            ->name('warehouse.b_unbundle.end');

        Route::get('/list', 'Warehouse\Bunbundle\BunbundleController@list')
            ->name('warehouse.b_unbundle.list');
    });

    Route::group(['prefix' => 'user', 'middleware' => ['role:wh_warehouse|administrator|formula_pic']], function () {
        Route::get('/', 'Warehouse\User\UserController@index')
            ->name('warehouse.user.index');

        // // Create.
        Route::get('/create', 'Warehouse\User\UserController@create')
            ->name('warehouse.user.create');

        //Store
        Route::post('/store', 'Warehouse\User\UserController@store')
            ->name('warehouse.user.store');

        // // scan product
        Route::get('/edit/{id}', 'Warehouse\User\UserController@edit')
            ->name('warehouse.user.edit');

        // // Close
        Route::post('/update/{id}', 'Warehouse\User\UserController@update')
            ->name('warehouse.user.update');

        Route::get('/show/{id}', 'Warehouse\User\UserController@show')
            ->name('warehouse.user.show');

        // destroy.
        // Route::post('/destroy/{id}', 'Warehouse\User\UserController@destroy')
        //     ->name('warehouse.user.destroy');
    });

    Route::group(['prefix' => 'redemption', 'middleware' => ['role:wh_supervisor|wh_cashier']], function () {
        Route::get('/', 'Warehouse\Redemption\WarehouseRedemptionController@index')
            ->name('warehouse.redemption.index');

        // exportCsv
        Route::get('/exportCsv', 'Warehouse\Redemption\WarehouseRedemptionController@exportCsv')
            ->name('warehouse.redemption.exportCsv');
    });
});
