@extends('layouts.guest.main')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo"
                src="{{ asset('images/logo/formula-logo.png') }}"
                alt="">
        </div>
    </div>

    <div class="my-register-box my-guests-card">
        <div class="my-r-title-box">
            <h5><b>REGISTRATION</b></h5>
        </div>
        <div id="rtab-opt"
            class=" my-r-nav">
            <ul class="nav nav-tabs nav-fill"
                role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active "
                        id="home-tab"
                        data-toggle="tab"
                        href="#registration"
                        role="tab"
                        aria-controls="registration"
                        aria-selected="true">Registration</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link register-tab-active "
                        id="profile-tab"
                        data-toggle="tab"
                        href="#information"
                        role="tab"
                        aria-controls="profile"
                        aria-selected="false">Information</a>
                </li>
                {{-- <li class="nav-item">
                <a class="nav-link register-tab-active " id="agreement-tab" data-toggle="tab" href="#agreement"
                    role="tab" aria-controls="agreement" aria-selected="false">Agreement</a>
            </li> --}}
            </ul>
        </div>

        <div class="card-body">

            <!-- Dealer Registration Form -->
            <form method="POST"
                action="{{ route('guest.register.customer.post') }}"
                id="register-form">
                @csrf
                <div class="tab-content r-inner-box"
                    id="myTabContent">
                    <!-- Registration  Tab-->
                    <div class="r-subt-box tab-pane fade show active "
                        id="registration"
                        role="tabpanel"
                        aria-labelledby="registration-tab">
                        <h5>Account Particulars</h5>
                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="email">Email</label>
                                <input type="email"
                                    name="email"
                                    class="r-form-color form-control @error('email') is-invalid @enderror"
                                    required
                                    id="email"
                                    placeholder="Email"
                                    value="{{ old('email') }}">
                                @error('email')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Password</label>
                                <input type="password"
                                    name="password"
                                    class="form-control "
                                    required
                                    id="password">
                                @error('password')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <div class="form-group col-md-12">
                                <label for="password-confirm">Confirm Password</label>
                                <input id="password-confirm"
                                    type="password"
                                    class="form-control "
                                    name="password_confirmation"
                                    required
                                    autocomplete="new-password">
                                @error('password_confirmation')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            {{-- <div class="form-group col-md-12">
                            <label for="validate-customer">Existing DC customer?</label>
                            <div class="validate-customer">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" onclick="javascript:DCorBJScheck();" type="radio"
                                        name="validationOptions" id="DCBJSRadio" value="1">
                                    <label class="form-check-label" for="DCRadio">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" onclick="javascript:DCorBJScheck();" type="radio"
                                        name="validationOptions" id="DCBJSRadio" value="0" checked>
                                    <label class="form-check-label" for="BJSRadio">No</label>
                                </div>
                            </div>
                        </div> --}}
                            <div class="form-group col-md-12"
                                id="ifDC"
                                style="display: none;">
                                <label for="yearStartDC">Year becomes DC customer</label>
                                <input type="text"
                                    name="yearStartDC"
                                    class="form-control "
                                    id="yearStartDC">

                                <label for="whatProduct">What product?</label>
                                <select class="form-control"
                                    id="whatProduct"
                                    name="whatProduct">
                                    <option value=""
                                        selected>Choose your product...</option>
                                    <option value="PR">PR</option>
                                    <option value="FS">FS</option>
                                    <option value="SF">SF</option>
                                    <option value="RW">RW</option>
                                    <option value="AP">AP</option>
                                    <option value="FC">FC</option>
                                    <option value="CL">CL</option>
                                </select>
                            </div>
                        </div>

                        <!-- Next Button -->
                        <div class="r-next-btn">
                            <a class="btn r-color-btn"
                                id="next-btn"><b>Next</b></a>
                        </div>
                    </div>

                    <!-- Information Tab -->
                    <div class="tab-pane fade r-subt-box"
                        id="information"
                        role="tabpanel"
                        aria-labelledby="information-tab">
                        <!-- Personal Particulars -->
                        <h5>Personal Particulars</h5>
                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="full_name">Full Name (as per NRIC)</label>
                                <input type="text"
                                    name="full_name"
                                    class="form-control @error('full_name') is-invalid @enderror"
                                    id="full_name"
                                    required
                                    placeholder=""
                                    value="{{ request()->agent_id ? json_decode(request()->details)->name : old('full_name') }}">
                                @error('full_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="validate-customer">Please choose : </label>
                                <div class="validate-customer">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input"
                                            type="radio"
                                            name="frm_customer"
                                            id="frm_customer_1"
                                            value="1"
                                            checked
                                            {{-- @if (old('frm_customer') == '1') checked @endif --}}>
                                        <label class="form-check-label"
                                            for="frm_customer">NRIC Number</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input"
                                            type="radio"
                                            name="frm_customer"
                                            id="frm_customer_0"
                                            value="0"
                                            {{-- @if (old('frm_customer') == null || old('frm_customer') == '0')
                                        checked @endif --}}>
                                        <label class="form-check-label"
                                            for="frm_customer">Passport Number</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12 if-frm"
                                style="display:none">
                                <label for="nric">NRIC Number <small>(Numbers Only)</small></label>
                                <input type="text"
                                    name="nric"
                                    class="form-control @error('nric') is-invalid @enderror"
                                    id="nric"
                                    placeholder=""
                                    value="{{ old('nric') }}">
                                @error('nric')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row register-info">
                            <div class="form-group col-md-12 if-notfrm">
                                <label for="date_of_birth">Date Of Birth</label>
                                <input type="date"
                                    name="date_of_birth"
                                    class="form-control datepicker @error('date_of_birth') is-invalid @enderror"
                                    id="date_of_birth"
                                    required>
                                @error('date_of_birth')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 if-notfrm">
                                <label for="passport_no">Passport Number</label>
                                <input type="text"
                                    name="passport_no"
                                    class="form-control @error('passport_no') is-invalid @enderror"
                                    id="passport_no"
                                    placeholder="Passport Number">
                                @error('passport_no')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 if-notfrm">
                                <label for="country">Country</label>
                                <select name="country"
                                    id="country"
                                    class="form-control @error('country') is-invalid @enderror">
                                    <option disabled
                                        selected>Choose your country..</option>
                                    @foreach ($countries as $country)
                                        <option class="text-capitalize"
                                            value="{{ $country->country_name }}">
                                            {{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{-- <div class="form-row register-info">
                        <div class="form-group col-md-12">
                            <label for="date_of_birth">Date Of Birth </label>
                            <input type="date"
                                class="form-control datepicker @error('date_of_birth') is-invalid @enderror"
                                id="date_of_birth" name="date_of_birth" {{--
                                value="{{ old('date_of_birth', $user->dealerInfo->date_of_birth) }}"> --}}

                        {{-- value="{{ old('nric') }}">
                            @error('date_of_birth')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div> --}}

                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="address_1">Address Line 1</label>
                                <input type="text"
                                    name="address_1"
                                    id="address_1"
                                    class="form-control @error('address_1') is-invalid @enderror"
                                    placeholder="Residential Address Line 1"
                                    value="{{ request()->agent_id ? json_decode(request()->details)->address_1 : old('address_1') }}">
                                @error('address_1')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="address_1">Address Line 2</label>
                                <input type="text"
                                    name="address_2"
                                    id="address_2"
                                    class="form-control @error('address_2') is-invalid @enderror"
                                    placeholder="Residential Address Line 2"
                                    value="{{ request()->agent_id ? json_decode(request()->details)->address_2 : old('address_2') }}">
                                @error('address_2')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="address_1">Address Line 3</label>
                                <input type="text"
                                    name="address_3"
                                    id="address_3"
                                    class="form-control @error('address_3') is-invalid @enderror"
                                    placeholder="Residential Address Line 3"
                                    value="{{ request()->agent_id ? json_decode(request()->details)->address_3 : old('address_3') }}">
                                @error('address_3')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="state">State</label>
                                <select name="state"
                                    id="state"
                                    class="form-control @error('state') is-invalid @enderror">
                                    <option disabled
                                        selected>Choose your state..</option>
                                    @foreach ($states as $state)
                                        <option class="text-capitalize"
                                            value="{{ $state->id }}"
                                            @if ((request()->agent_id ? json_decode(request()->details)->state_id : old('state')) == $state->id) selected @endif>{{ $state->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('state')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12"
                                id="class_city_key">
                                <label for="city_key">City</label>
                                <select name="city_key"
                                    id="city_key"
                                    class="form-control @error('city_key') is-invalid @enderror">
                                    <option disabled
                                        selected>Choose your city..</option>
                                    @foreach ($cities as $city)
                                        <option class="text-capitalize"
                                            value="{{ $city->city_key }}"
                                            @if ((request()->agent_id ? json_decode(request()->details)->city_key : old('city_key')) == $city->city_key) selected @endif>{{ $city->city_name }}
                                        </option>
                                    @endforeach
                                    <option value="0">Others</option>
                                </select>
                            </div>
                            @error('city_key')
                                <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                            <div class="form-group col-md-6"
                                id="city">
                                <label for="city">Others City</label>
                                <input type="text"
                                    name="city"
                                    class="form-control"
                                    value=""
                                    placeholder="Entry Your City Here">
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-12">
                                <label for="postcode">Postcode <span class="form-text text-danger ajaxMessage d-inline"
                                        style="display: none"></span></label>
                                <input type="text"
                                    name="postcode"
                                    id="postcode"
                                    class="form-control @error('postcode') is-invalid @enderror"
                                    placeholder=""
                                    value="{{ request()->agent_id ? json_decode(request()->details)->postcode : old('postcode') }}">
                                @error('postcode')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-6">
                                <label for="contact_number_home">Contact Number (Home)</label>
                                <input type="text"
                                    name="contact_number_home"
                                    id="contact_number_home"
                                    class="form-control @error('contact_number_home') is-invalid @enderror"
                                    placeholder=""
                                    value="{{ old('contact_number_home') }}">
                                @error('contact_number_home')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number_mobile">Contact Number (Mobile)</label>
                                <input type="text"
                                    name="contact_number_mobile"
                                    id="contact_number_mobile"
                                    class="form-control @error('contact_number_mobile') is-invalid @enderror"
                                    placeholder=""
                                    value="{{ request()->agent_id ? json_decode(request()->details)->mobile : old('contact_number_mobile') }}">
                                @error('contact_number_mobile')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info text-center py-2">
                            <div class="form-group col-md-auto radiobtn my-0 col-6">
                                <input type="radio"
                                    id="yes_referrer"
                                    name="referrer_choices"
                                    value="yes_referrer"
                                    checked="checked" />
                                <label for="yes_referrer"
                                    data-referrer="yes"
                                    class="button_ref py-1 px-md-3">Referred
                                    By</label>
                            </div>
                            <div class="form-group col-md-auto radiobtn my-0 col-6 ">
                                <input type="radio"
                                    id="no_referrer"
                                    name="referrer_choices"
                                    value="no_referrer" />
                                <label for="no_referrer"
                                    data-referrer="no"
                                    class="button_ref py-1 px-md-3">No
                                    Referrer</label>
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-md-6">
                                <label for="referrer_id">Agent Formula ID</label>
                                <input type="text"
                                    name="referrer_id"
                                    id="referrer_id"
                                    class="form-control @error('referrer_id') is-invalid @enderror"
                                    placeholder="3911XXXXXX"
                                    value="{{ request()->agent_id ? request()->agent_id : old('referrer_id') }}">
                                @error('referrer_id')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="referrer_name">Agent Name</label>
                                <input type="text"
                                    name="referrer_name"
                                    id="referrer_name"
                                    class="form-control @error('referrer_name') is-invalid @enderror"
                                    agent-name
                                    placeholder=""
                                    value="{{ request()->agent_name ? request()->agent_name : old('referrer_name') }}">
                                @error('referrer_name')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row register-info">
                            <div class="form-group col-12">
                                <label for="invoice_number">Invoice Number <small class="text-danger">*</small></label>
                                <input type="text"
                                    name="invoice_number"
                                    id="invoice_number"
                                    class="form-control @error('invoice_number') is-invalid @enderror input-mask-invoice"
                                    placeholder="{{ $inv_placehold }}"
                                    value="{{ request()->invoice ? request()->invoice : old('invoice_number') }}">
                                <small class="form-text text-danger invoice_error"
                                    style="display: none;"></small>
                            </div>
                        </div>

                        <!-- Next Button -->
                        <div class="ri-btn">
                            <!-- <a class="btn btn-secondary next-button" id="agreement-tab" data-toggle="tab" href="#agreement" role="tab" aria-controls="profile" aria-selected="false">Next</a> -->
                            {{-- <a class="btn btn-secondary next-button bjsh-btn-gradient " id="next-btn2"><b>Next</b></a> --}}
                            <!-- Submit Button -->
                            <div class="form-check checkbox-style form-submit-list padding-zero my-mt-1">
                                <input class="form-check-input"
                                    type="radio"
                                    name="tncRadio"
                                    id="gridCheck">
                                <label class="form-check-label"
                                    for="gridCheck"></label>
                                </input>
                                <span> I have read, understand and accepted the <a href="#termsNconditions"
                                        id="DeleteCartItem"
                                        data-bs-toggle="modal"
                                        role="button"
                                        data-toggle="modal"
                                        data-target="#termsNconditions">membership terms and conditions</a>. </span>
                            </div>

                            <input type="hidden"
                                name="registrationFor"
                                value="customer">
                            <button type="submit"
                                id="submit"
                                class="r-color-btn my-3 submitDisabled"
                                disabled
                                style="width: 100px; height: 40px; color: grey; background: lightgrey;"><b>Sign
                                    Up</b></button>
                        </div>
                    </div>

                    <div class="tab-pane fade "
                        id="agreement"
                        role="tabpanel"
                        aria-labelledby="agreement-tab">
                        <h5 class="text-center"
                            style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">
                            Agreement</h5>

                        <!-- Registration Agreement -->
                        <div class="row ">
                            <div class="col-12 mb-0">
                                <div class="overflow-auto"
                                    style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                                    @if (country()->country_id == 'MY')
                                        @include('shop.payment.service-terms-conditions-my')
                                    @else
                                        @include('shop.payment.service-terms-conditions-sg')
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-8 offset-md-2 pl-3 pr-3 pt-2 mb-0">
                                <canvas class="display-block signature-pad"
                                    style="touch-action: none;"></canvas>
                                <p id="signatureError"
                                    name="signatureError"
                                    style="color: red; display: none;">
                                    Please provide your signature.</p>
                                <div class="p-1 text-right">
                                    <button id="resetSignature"
                                        type="button"
                                        class="btn btn-sm btn-info"
                                        style="background-color: lightblue;">Reset</button>
                                    <!-- <button id="saveSignature" type="button" class="btn btn-sm" style="background-color: #fbcc34;">Save</button> -->
                                </div>
                                <input type="hidden"
                                    name="signature"
                                    id="signatureInput">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 mb-0 pt-2">
                                <!-- Submit Button -->
                                {{-- <div class="text-right">
                                <input type="hidden" name="registrationFor" value="customer">
                                <button type="submit" id="submit"
                                    class=" btn next-button bjsh-btn-gradient text-right "><b>Sign Up</b></button>
                            </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade"
        id="termsNconditions"
        aria-hidden="true"
        aria-labelledby="termsNconditionsLabel"
        tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
            <div class="modal-content edit-info-modal-box">
                <div class="modal-header">
                    <h1 class="modal-title edit-tittle"
                        id="termsNconditionsLabel">Membership Terms and Conditions</h1>
                    <button type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p style="text-align: justify;">
                    <ol type="A">
                        <li style="text-align:justify;">MEMBERSHIP</li>
                        Membership Program is an exclusive members’ service provided by Formula Healthcare Sdn Bhd and its
                        affiliate companies (hereinafter referred to as “FHSB”). By applying for membership, you hereby
                        AGREE to be bound by the following terms and conditions: -
                        <ol type="1">
                            <li style="text-align:justify;">
                                Customers are required to register as member before they can make purchase via FHSB’s
                                website. All members’ ID will be recorded in membership account.
                            </li>
                            <li style="text-align:justify;">
                                Application is open to customers who are aged 18 years and above.
                            </li>
                            <li style="text-align:justify;">
                                Membership are divided to Standard Member, Advance Member and Premier Member, each category
                                of membership will enjoy different types of benefits and offers from FHSB.
                            </li>
                            <li style="text-align:justify;">
                                Membership for both Advance and Premier categories will be renewed automatically after
                                spending minimum amount annually. Member who has failed to meet the minimum transaction
                                amount will be converted to become a Standard Member. There is no special transaction
                                requirement for Standard Member.
                            </li>
                            <li style="text-align:justify;">
                                A member may only hold one membership account at one time.
                            </li>
                            <li style="text-align:justify;">
                                Membership will be suspended after one year of zero transaction.
                            </li>
                        </ol>
                        <li style="text-align:justify;">POINTS</li>
                        <ol type="1">
                            <li style="text-align:justify;">
                                ONE (1) membership point will be awarded for every RM1 spend in a single receipt. Every 2000
                                points can be exchangeable for RM20 cash voucher to purchase selected merchandise.
                            </li>
                            <li style="text-align:justify;">
                                Any unused cash voucher cannot be exchanged for cash.
                            </li>
                            <li style="text-align:justify;">
                                Earning of points will be based on the total amount purchase in a single receipt.
                                Accumulation/Combination of receipts is prohibited.
                            </li>
                            <li style="text-align:justify;">
                                All points earned can be used to offset any purchases at FHSB’s official website and its
                                physical store.
                            </li>
                            <li style="text-align:justify;">
                                FHSB has the right to cancel the points awarded if products are returned and refunded in
                                full or partially. This will also apply to exchange of products, unless it is for exchange
                                of products with equivalent value.
                            </li>
                            <li style="text-align:justify;">
                                Points earned cannot be exchanged for cash and can only be used to offset purchases
                                according to terms and conditions set by FHSB.
                            </li>
                            <li style="text-align:justify;">
                                Points earned are strictly non-transferable and non-assignable.
                            </li>
                            <li style="text-align:justify;">
                                Points earned can only be valid for 36 months from the date of points were earned.
                            </li>
                            <li style="text-align:justify;">
                                FHSB shall have the absolute discretion to refuse to process any member’s redemption request
                                if fraudulent transactions/ activities are suspected/ involved.
                            </li>
                            <li style="text-align:justify;">
                                FHSB reserves the right to determine number of points can be used to offset the purchase of
                                any merchandise. Any member of FHSB who commits a misconduct, engages in fraud, abuses of
                                FHSB benefits and rewards, and fails to adhere to these Terms and Conditions, will have
                                his/her membership terminated. All of his/her benefits and privileges shall cease forthwith
                                and their points will become void immediately.
                            </li>
                            <li style="text-align:justify;">
                                Any transaction using the accumulated points will not be awarded with points.
                            </li>
                        </ol>
                        <li style="text-align:justify;">MEMBERSHIP TERMINATION</li>
                        <ol>
                            <li style="text-align:justify;">
                                Members who wish to discontinue their membership must submit written notice or notification
                                via email to formula2u-cs@delhubdigital.com. Membership account will be terminated within 30
                                days upon receipt of such notice and all benefits and privileges of the member shall cease
                                and all outstanding points of such member will become void.
                            </li>
                            <li style="text-align:justify;">
                                FHSB reserves the right to terminate any membership at its sole and absolute discretion, at
                                which point all unused points, benefits and privileges will be cancelled. Such termination
                                shall be without prejudice to the accrued rights and remedies of FHSB, its participating
                                partners and affiliates.
                            </li>
                            <li style="text-align:justify;">
                                If a Member has obtained redemption awards or other products or services through fraud,
                                dishonesty or deceit then the member shall without limitation be liable to FHSB and its
                                participating partners or affiliates for the full price of the redemption awards or other
                                products or services obtained together with all costs and damages incurred or suffered by
                                FHSB and its participating partners, affiliates as a result thereof.
                            </li>
                        </ol>
                        <li style="text-align:justify;">GENERAL</li>
                        <ol>
                            <li style="text-align:justify;">The membership shall remain as property of FHSB at all times.
                            </li>
                            <li style="text-align:justify;">By applying for and using FHSB membership, members must consent
                                to the collection, storage, use and disclosure of the personal information in accordance
                                with FHSB’s Privacy Policy and Privacy notice. Members can find the Privacy Policy online at
                                https://formula2u.com/.</li>
                            <li style="text-align:justify;">
                                In addition to the matters set out in the Privacy Policy, member’s personal information is
                                collected so that the Company can continuously update its members of new range of products
                                and services and other promotional materials or notices from time to time.
                            </li>
                            <li style="text-align:justify;">
                                Members can at any one-time email to formula2u-cs@delhubdigital.com of their wish to
                                terminate the membership and thereafter they will not receive any promotional material or
                                notices from FHSB or its participating partners and/or affiliates.
                            </li>
                            <li style="text-align:justify;">
                                FHSB shall hold no liability if a membership application is rejected for any reason
                                whatsoever.
                            </li>
                            <li style="text-align:justify;">
                                Any dispute related to points and services must be directed to
                                formula2u-cs@delhubdigital.com within seven (7) days of the said dispute arises.
                            </li>
                            <li style="text-align:justify;">
                                The Terms and conditions are governed by laws of Malaysia.
                            </li>
                            <li style="text-align:justify;">
                                FHSB has the final authority as to the interpretation of the Terms and Conditions and as to
                                any other disputes regarding the membership.
                            </li>
                            <li style="text-align:justify;">
                                FHSB reserves the right to amend any of these Terms and Conditions from time to time as and
                                when it deems necessary. Reasonable notice will be posted on FHSB’s official website as
                                notification of the said amendment(s). Your continuous usage of the membership after any
                                amendment(s) to the Terms and Conditions shall constitute your acceptance to such
                                amendment(s).
                            </li>
                        </ol>
                        </p>
                </div>
                <div class="edit-opt-btn modal-footer">
                    <button class="cancel-btn  btn btn-primary"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                        id="closeMemberModal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            frmCustomerSelect();
            let referrerChoices = $('input[name="referrer_choices"]:checked').val();
        });

        function frmCustomerSelect() {

            frm_customer = $('input[name="frm_customer"]:checked').val();

            if (frm_customer == '1') {

                $('.if-frm').show();
                $('.if-notfrm').hide();

            } else {

                $('.if-frm').hide();
                $('.if-notfrm').show();
            }

        }

        $('input[name="frm_customer"]').click(frmCustomerSelect);

        $('#closeMemberModal').click(function() {
            $('#termsNconditions').modal("hide");
        });

        $('input:radio[name="tncRadio"]').change(function() {
                submitButtonDisabled()
            });

            function submitButtonDisabled(){
                let referrerChoices = $('input[name="referrer_choices"]:checked').val();
                let tncRadio = $('input:radio[name="tncRadio"]')
                if (($(tncRadio).is(':checked') && referrerChoices == 'yes_referrer' && $('#invoice_number').hasClass(
                        'is-valid')) || ($(tncRadio).is(':checked') && referrerChoices == 'no_referrer')) {
                    $('#submit').removeAttr('disabled');
                    $('#submit').css("background", "");
                    $('#submit').css("color", "");
                }
            }

        function DCorBJScheck() {
            if (document.getElementById('DCBJSRadio').checked) {
                document.getElementById('ifDC').style.display = 'block';
            } else document.getElementById('ifDC').style.display = 'none';

        }
        let invoiceSelectors = document.getElementsByClassName('input-mask-invoice');

        let invoiceInputMask = new Inputmask({
            // 'mask': 'BJN99999999999',
            'placeholder': '0'
        });

        for (var i = 0; i < invoiceSelectors.length; i++) {
            invoiceInputMask.mask(invoiceSelectors.item(i));
        }

        // Variablie initialization.
        // var canvas = document.querySelector("canvas");
        // // let canvas = document.querySelector('.signature-pad');
        // // const signatureSaveButton = document.getElementById("saveSignature");
        // const signatureResetButton = document.getElementById("resetSignature");
        // const signatureError = document.getElementById("signatureError");
        // const signatureInput = document.getElementById("signatureInput");

        let currentTab = $('.nav-tabs > .active');
        let nextTab = currentTab.next('li');

        //Handles tabs click.
        $('.nav-link').click(function() {
            currentTab = $(this).parent();
            $('.nav-tabs > .active').removeClass('active');
            currentTab.addClass('active');
            nextTab = currentTab.next('li');
        });

        // TODO: Not needed as each button click will need to validate the form fields first.
        // Handles next button click.
        // $('.next-button').click(function() {
        //     currentTab.removeClass('active');
        //     nextTab.find('a').trigger('click');
        //     nextTab.addClass('active');
        //     currentTab = $('.nav-tabs > .active');
        //     nextTab = currentTab.next('li');
        // });

        // Initialize a new signaturePad instance.
        // let signaturePad = new SignaturePad(canvas);

        // // Clear signature pad.
        // signatureResetButton.addEventListener("click", function(event) {
        //     signaturePad.clear();
        // });

        // TODO: Not needed as submiting the form will automatically inject the signature into an input field.
        // Save signature pad as data url.
        // signatureSaveButton.addEventListener("click", function(event) {
        //     if (signaturePad.isEmpty()) {
        //         signatureError.style.display = "block";
        //     } else {
        //         signatureUrl = signaturePad.toDataURL();
        //         signatureInput.value = signatureUrl;
        //     }
        // });

        // Custom validator for postcode.
        jQuery.validator.addMethod("postcode", function(value, element) {
            return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
        }, "Please provide a valid postcode.");

        let referrerChoices = $('input[name="referrer_choices"]:checked').val();
        let cityKey = $('input[name="city_key"]:selected').val();

        console.log('referrerChoices',referrerChoices)
        // Validate registration tab before moving to the next tab
        $("#register-form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 8,
                },
                password_confirmation: {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                },
                full_name: {
                    required: true,
                    minlength: 3
                },
                nric: {
                    required: true,
                    digits: true,
                    minlength: 12,
                    maxlength: 12
                },
                date_of_birth: {
                    required: true
                },
                passport_no: {
                    required: true
                },
                country: {
                    required: true
                },
                address_1: {
                    required: true,
                    minlength: 3
                },

                postcode: {
                    required: true,
                    minlength: 5,
                    maxlength: 6
                    // postcode: true
                },
                city_key: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                contact_number_mobile: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 15
                },
                referrer_id: {
                    required: function(element) {
                        return referrerChoices == 'yes_referrer';
                    },
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                },
                referrer_name: {
                    required: function(element) {
                        return referrerChoices == 'yes_referrer';
                    },
                    minlength: 3
                },
                invoice_number: {
                    required: function(element) {
                        return referrerChoices == 'yes_referrer';
                    },
                    minlength: 14,
                    maxlength: 14,
                }
            },
            messages: {
                email: {
                    required: "Please enter an email.",
                    email: "The email is not valid."
                },
                password: {
                    required: "Please enter a password.",
                    minlength: "Password must be minimum of 8 characters."
                },
                password_confirmation: {
                    required: "Please confirm your password",
                    minlength: "Password must must be minimum of 8 characters",
                    equalTo: "Password must be same as above"
                },
                full_name: {
                    required: "Please enter your full name.",
                    minlength: "Your name must be more than 3 characters."
                },
                nric: {
                    required: "Please enter your NRIC number.",
                    minlength: "Please enter a valid NRIC number.",
                    maxlength: "Please enter a valid NRIC number."
                },
                address_1: {
                    required: "Please enter your address"
                },

                postcode: {
                    required: "Please enter your postcode"
                },
                city_key: {
                    required: "Please select your city"
                },
                city: {
                    required: "Please enter your city"
                },
                state: {
                    required: "Please select your state"
                },
                contact_number_mobile: {
                    required: "Please enter your mobile number.",
                    digits: "Please enter number only.",
                    minlength: "Contact number must at least be 10 digits.",
                    maxlength: "Please enter a valid contact number."
                },
                referrer_id: {
                    required: "Please enter Agent ID.",
                    digits: "Please enter number only.",
                    minlength: "Agent ID must be at least 10 digits.",
                    maxlength: "Please enter a valid Agent ID"
                },
                referrer_name: {
                    required: "Please enter an Agent name.",
                    minlength: "Please enter at least 3 characters."
                },
                invoice_number: {
                    required: "Please provide an invoice number.",
                    minlength: "Invoice number must have 14 characters.",
                    maxlength: "Please enter a valid invoice."
                }
            }
        });


        // validate fields in 1st tab
        $('#next-btn').click(function() {
            if (
                $("#register-form").validate().element('#email') &&
                $("#register-form").validate().element('#password') &&
                $("#register-form").validate().element('#password-confirm')
            ) {
                nextTab.find('a').trigger('click');
            }
        });

        // validate fields in 2nd tab
        // $('#next-btn2').click(function() {
        //     if (
        //         $("#register-form").validate().element('#full_name') &&
        //         $("#register-form").validate().element('#nric') &&
        //         $("#register-form").validate().element('#address_1') &&
        //         $("#register-form").validate().element('#postcode') &&
        //         $("#register-form").validate().element('#city') &&
        //         $("#register-form").validate().element('#state') &&
        //         $("#register-form").validate().element('#contact_number_home') &&
        //         $("#register-form").validate().element('#contact_number_mobile')
        //     ) {
        //         nextTab.find('a').trigger('click');
        //     }
        // });

        // Validate the signature pad before submitting form.
        $('#submit').click(function(e) {
            frmCustomer = $('input[name="frm_customer"]:checked').val();
            let referrerChoices = $('input[name="referrer_choices"]:checked').val();
            var compulsoryValidate = 0;
            var referrerValidate = 0;
            var foreignValidate = 0;
            var cityValidate = 0;

        // if city key and city not empty
        if ($("#register-form").validate().element('#city_key') &&
            (($('#city_key').val() == 0 && $("#register-form").validate().element('#city')) ||
            ($('#city_key').val() != 0))
            ) {
                cityValidate = 1;
        }

        // other compulsory field
        if ($("#register-form").validate().element('#full_name') &&
            $("#register-form").validate().element('#address_1') &&
            $("#register-form").validate().element('#postcode') &&
            $("#register-form").validate().element('#state') &&
            $("#register-form").validate().element('#contact_number_mobile') &&
            cityValidate == 1
            ) {
                compulsoryValidate = 1
            }

        // if choose referred by, referrer field must fill
        if ((referrerChoices == 'yes_referrer' && $('#register-form').validate().element('#referrer_id') &&
            $('#register-form').validate().element('#referrer_name')) || referrerChoices == 'no_referrer') {
            referrerValidate = 1
        }

        // frmCustomer = 1 is malaysian, frmCustomer = 0 is foreigner
        if (
            (frmCustomer == '1' && $("#register-form").validate().element('#nric')) ||
            (frmCustomer == '0' &&
                $("#register-form").validate().element('#date_of_birth') &&
                $("#register-form").validate().element('#passport_no') &&
                $("#register-form").validate().element('#country'))
            ) {
            foreignValidate = 1
        }

            // console.log('compulsoryValidate',compulsoryValidate,'referrerValidate',referrerValidate,'foreignValidate',foreignValidate)
            // return false;

        if (compulsoryValidate == 1 && referrerValidate == 1 && foreignValidate == 1) {
            return true;
        } else {
            return false;
        }

        // if (foreignValidate == 1 && compulsoryValidate == 1(referrerChoices == 'yes_referrer' && referrerValidate ==
        //         1) ||
        //     (referrerChoices == 'no_referrer')) {
        //     return true;
        // } else if (
        //     (referrerChoices == 'yes_referrer' &&
        //         $("#register-form").validate().element('#full_name') &&
        //         $("#register-form").validate().element('#address_1') &&
        //         $("#register-form").validate().element('#postcode') &&
        //         $("#register-form").validate().element('#city_key') &&
        //         $("#register-form").validate().element('#city') &&
        //         $("#register-form").validate().element('#state') &&
        //         $("#register-form").validate().element('#contact_number_mobile') &&
        //         $('#register-form').validate().elemenet('#referrer_id') &&
        //         $('#register-form').validate().element('#referrer_name')) ||
        //     (referrerChoices == 'no_referrer' && )
        // ) {
        //     return true;
        // } else {
        //     return false;
        // }

        // if (signaturePad.isEmpty()) {
        //     signatureError.style.display = "block";
        //     return false;
        // } else {
        //     signatureUrl = signaturePad.toDataURL();
        //     signatureInput.value = signatureUrl;
        //     return true;
        // }
        });

        $(document).ready(function() {

            /* City */
            var cityKey = document.getElementById("city_key");
            var city = document.getElementById("city");
            var classCityKey = document.getElementById("class_city_key");

            displayCity();

            $('#city_key').on('change', function() {
                displayCity();
            });

            $("#state").on("change", function() {
                var variableID = $(this).val();

                if (variableID) {
                    $.ajax({
                        type: "POST",
                        url: '{{ route('guest.register.customer.state-filter-city') }}',
                        data: $("#register-form").serialize(),
                        success: function(toajax) {
                            $("#city_key").html(toajax);
                            displayCity();
                        }
                    });
                }

            });

            function displayCity() {
                if (cityKey.value == '0') {
                    city.style.display = "block";
                    classCityKey.className = "col-12 col-md-6 form-group";
                } else {
                    city.style.display = "none";
                    classCityKey.className = "col-12 col-md-12 form-group";
                }
            }
            /* End City */

            invoiceCheck();

        });

        $("#postcode").keyup(function() {
            var $postcode = $('#postcode').val();
            if ($postcode.length == 5) {
                $.ajax({
                    url: '/get-postcode',
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "postcode": $postcode
                    },
                    beforeSend: function() {},
                    success: function(success) {
                        // console.log(success.data);
                        if (success.data == false) {
                            // $('#exampleFormControlInput2').attr('style','border:red;')
                            // alert('false');
                            $('.ajaxMessage').show().html('Invalid Postcode');
                            return false;
                        } else {
                            $('.ajaxMessage').hide().html('');
                            $('#cart-form').submit();

                        }

                    },
                    error: function() {
                        alert("error in loading");
                    }

                });
            }
        });

        $("#invoice_number").on("keyup change load", function() {
            invoiceCheck();
        });

        $(".button_ref").on("click change load", function() {
            referrerField($(this).attr('data-referrer'))
            submitButtonDisabled()
        });

        // $('.button_ref').click(function(){
        //     //
        //     // console.log($(this).attr('data-referrer'));
        //     referrerField($(this).attr('data-referrer'))
        // })

        function invoiceCheck() {
            var $invoice_number = $('#invoice_number').val();
            $('.invoice_error').hide();
            if ($invoice_number.length == 14) {
                var route = "{{ route('customer.check.invoice', ':invoiceNo') }}";
                route = route.replace(':invoiceNo', $invoice_number);
                // console.log($invoice_number);


                $.ajax({
                    url: route,
                    type: 'GET',
                    beforeSend: function() {},
                    success: function(success) {
                        // console.log(success);
                        if (success === 'valid') {
                            $('#invoice_number').addClass('is-valid');
                            $('#invoice_number').removeClass('is-invalid');
                            $('.invoice_error').hide();
                            if ($('input:radio[name="tncRadio"]').is(':checked')) {
                                $('#submit').removeAttr('disabled');
                                $('#submit').css("background", "");
                                $('#submit').css("color", "");
                            }
                        } else {
                            $('#invoice_number').removeClass('is-valid');
                            $('#invoice_number').addClass('is-invalid');
                            $('.invoice_error').show().html(success);
                        }
                    },
                    error: function() {
                        alert("error in loading");
                    }

                });
            }
        }

        function referrerField(data) {
            referrerChoices = data + '_referrer'

            if (data == 'yes') {
                $('input[name="referrer_id"]').attr('readonly', false)
                $('input[name="referrer_name"]').attr('readonly', false)
                $('input[name="invoice_number"]').attr('readonly', false)
            } else {

                $('input[name="referrer_id"]').attr('readonly', true)
                $('input[name="referrer_name"]').attr('readonly', true)
                $('input[name="invoice_number"]').attr('readonly', true)
            }
            // console.log(data)

        }
    </script>
@endpush
