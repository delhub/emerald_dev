@extends('layouts.administrator.main')

@section('pageTitle', 'Users List')

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
        Warehouse Users Management
    </h4>


<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-10 margin-tb">
                        <form action="{{ route('warehouse.user.index') }}" method="GET">
                            <div class="row mt-2 ml-2">
                                <!-- States -->
                                <div class="col-md-auto px-1">
                                    <select name="location_id" class="col-12 text-left custom-select">
                                        <option value="">All Location</option>
                                        @foreach ($locations as $location)
                                        <option value="{{ $location->id }}" {{ $request->location_id == $location->id ? 'selected' : ''}}>
                                            {{ $location->location_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-auto ml-auto">
                                    <div class="row">
                                        <div class="col-md-8 p-0">
                                            <select class="custom-select" id="selectSearch" name="selectSearch">
                                                <option value="ALL" selected>Choose...</option>
                                                <option value="name" {{ $request->selectSearch == 'name' ? 'selected' : '' }}>Name</option>
                                                <option value="email" {{ $request->selectSearch == 'email' ? 'selected' : '' }}>Email</option>
                                                <option value="account_type" {{ $request->selectSearch == 'account_type' ? 'selected' : '' }}>Roles</option>
                                            </select>

                                            <input type="text" class="form-control" id="" name="searchBox"
                                                value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                                        </div>
                                        <div class="col-md-4 ">
                                            <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="text-align:center;">
                                <div class="col-md-auto px-1">
                                    <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
                                    <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('warehouse.user.index') }} style="color:black;">Reset Filter</a></button>
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col-auto ml-auto">
                                    {!! $users->render() !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-right mb-3">
                            <a class="btn btn-success" href="{{ route('warehouse.user.create') }}"> Create New User</a>
                        </div>
                    </div>
                </div>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <span>{{ $message }}</span>
                    </div>
                @endif

                <table class="table table-bordered table-striped">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Location</th>
                        <th>Roles</th>
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($users as $key => $user)

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $user->userInfo->full_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @php
                                    if (isset($user->user_warehouse_location) && $user->user_warehouse_location) {
                                        foreach ($user->user_warehouse_location as $key => $whLocation){
                                            echo warehouseLocation($whLocation)->location_name.'&nbsp;&nbsp;';
                                        }
                                    }
                                @endphp
                            </td>
                            <td>
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                        <label class="badge badge-success" style="font-size:15px">{{ $v }}</label>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                {{-- <a class="btn btn-info" href="{{ route('warehouse.user.show',$user->id) }}">Show</a> --}}
                                <a class="btn btn-primary" href="{{ route('warehouse.user.edit',$user->id) }}">Edit</a>
                                {{-- {!! Form::open(['method' => 'DELETE','route' => ['warehouse.user.destroy', $user->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!} --}}
                            </td>
                        </tr>
                    @endforeach
                </table>

                {!! $users->render() !!}

            </div>
        </div>
    </div>
</div>
@endsection
