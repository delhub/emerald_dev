<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPointColumnAtPurchaseRelatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('point_amount')->after('order_amount')->default(0);
        });

        Schema::table('items', function (Blueprint $table) {
            $table->integer('subtotal_point')->after('subtotal_price')->default(0);
            $table->integer('unit_point')->after('subtotal_price')->default(0);
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->integer('point_amount')->after('local_amount')->default(0);
        });

        Schema::table('purchases', function (Blueprint $table) {
            $table->integer('point_amount')->after('purchase_amount')->default(0);
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->integer('subtotal_point')->after('subtotal_price')->default(0);
            $table->integer('unit_point')->after('subtotal_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['point_amount']);
        });

        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn(['subtotal_point','unit_point']);
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn(['user_points_id','point_amount']);

        });

        Schema::table('purchases', function (Blueprint $table) {
            $table->dropColumn(['point_amount']);
        });
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn(['subtotal_point','unit_point']);
        });
    }
}
