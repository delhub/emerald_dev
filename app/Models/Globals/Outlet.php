<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;

class Outlet extends Model
{
    protected $table = 'outlet_infos';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];


    public function city()
    {
        return $this->hasOne('App\Models\Globals\City', 'city_key', 'city_key');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\Globals\State', 'state_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Globals\Countries', 'country_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Globals\Image', 'imageable');
    }

    /**
     * Get default image of a product.
     */
    public function defaultImage()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable')
            ->where('default', 1);
    }

    public function warehouseLocation()
    {
        return $this->hasOne('App\Models\Warehouse\Location\Wh_location', 'location_key', 'outlet_key');
    }
}
