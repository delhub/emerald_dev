<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\Image;
use App\Models\Products\ProductAttribute;

class ProductBundle extends Model
{
    // Set table
    protected $table = 'panel_product_bundle';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'panel_product_id',
        'bundle_type',
        'bundle_id',
        'bundle_qty',
        'bundle_product_id'
    ];

    /**
     * Get an attribute's product.
     */
    public function product()
    {
        return $this->hasOne('App\Models\Products\Product', 'id', 'panel_product_id');
    }

    public function attribute()
    {
        return $this->hasOne('App\Models\Products\ProductAttribute', 'id', 'product_attribute_id');
    }
    public function getBundlePhotoAttribute()
    {
        if (isset($this->attribute->color_images)){

            $color_image_id = $this->attribute->color_images;
 
            //get the image            
            $image_url = Image::find($color_image_id )->url;
            
            return array('type' =>'image', 'attribute' => $image_url );
        
        }

        if (isset($this->attribute->color_hex)){

            $color_code_id = $this->attribute->color_hex;

            //get the color
            return array('type' =>'color', 'attribute' => $color_code_id );
        } 

        return array('type' =>'none', 'attribute' => $this->product->parentProduct->defaultImage->url);
     
    }
}
