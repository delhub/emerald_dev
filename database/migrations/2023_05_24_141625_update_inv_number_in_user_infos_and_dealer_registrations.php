<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Users\UserInfo;
use App\Models\Registrations\Dealer\Registration as DealerRegistration;

class UpdateInvNumberInUserInfosAndDealerRegistrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserInfo::selectRaw('user_infos.invoice_number, p.inv_number')
            ->join('purchases as p', 'p.purchase_number', '=', 'user_infos.invoice_number')
            ->whereNotNull('p.inv_number')
            ->each(function ($uiData) {
                UserInfo::where('invoice_number', $uiData->invoice_number)->update(['invoice_number' => $uiData->inv_number]);
            });

        DealerRegistration::selectRaw('dealer_registrations.invoice_number, p.inv_number')
            ->join('purchases as p', 'p.purchase_number', '=', 'dealer_registrations.invoice_number')
            ->whereNotNull('p.inv_number')
            ->each(function ($dData) {
                DealerRegistration::where('invoice_number', $dData->invoice_number)->update(['invoice_number' => $dData->inv_number]);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
