<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Products\ProductAttribute;
use App\Models\Globals\Products\Product;
// use App\Models\Products\Product as panelProduct;
use App\Models\Categories\Category;
use App\Models\Categories\PivCategory;

class ProductManufactureImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows[0] as $c => $d) {
            $rows[0][$c] = str_slug($d);
        }

        foreach ($rows as $k => $v) {
            if($k > 0) {
                foreach ($rows[0] as $k1 => $v1) {
                    $rb[$v1] = $v[$k1];
                }

                $data[] = $rb;
            }
        }

        if(count($data) > 0) {
            foreach ($data as $a => $b) {
                if(isset($b['country-of-origin']) && !empty($b['country-of-origin'])) {
                    $attribute = $this->getProduct($b['product-code']);
                    $origin = $this->getOriginCountry($b['country-of-origin']);

                    $raw = [
                        'key' => $a,
                        'pc' => $b['product-code'],
                        'gid' => isset($attribute->product2) ? $attribute->product2->global_product_id : 0,
                        'manufacture_id' => $this->getManufactureID($b['quality']),
                        'origin' => $origin->id
                    ];

                    if($raw['gid'] > 0) {
                        $this->updateProduct($raw);
                    }
                }

            }

            return 'data has been imported successfully...';
        }

    }

    public function updateCountryOriginQuality($data) {

    }

    public function getManufactureID($data) {
        switch (strtolower($data)) {
            case 'import':
                    return 2;
                break;
            case 'exclusive':
                    return 3;
                break;
            default:
                return 1;
        }
    }

    public function getOriginCountry($data) {
        $origin = Category::where([['name', $data], ['type', 'origin_country']])->first();

        if(!$origin) {
            $origin = new Category;

            $origin->name = ucwords(strtolower($data));
            $origin->slug = str_slug($data);
            $origin->parent_category_id = 0;
            $origin->type = 'origin_country';

            $origin->save();

            return $origin;
        }
        else {
            $origin->name = ucwords(strtolower($data));

            $origin->save();

            return $origin;
        }
    }

    public function getProduct($data) {
        $attribute = ProductAttribute::where('product_code',$data)->first();

        if($attribute) {
            return $attribute;
        }
    }

    public function updateProduct($data) {

        $product = Product::find($data['gid']);

        if($product) {
            $product->quality_id = $data['manufacture_id'];

            $product->save();

        }

        $this->deleteExistPivOriginCountry($data);

        $this->createNewPivOrigin($data);
    }

    function deleteExistPivOriginCountry($data) {

        $pivOrigin = PivCategory::select('*')
            ->join('categories', 'piv_category_product.category_id', '=', 'categories.id')
            ->where('piv_category_product.product_id', $data['gid'])
            ->get();

        if(count($pivOrigin)) {
            foreach ($pivOrigin as $key => $v) {
                if($v->type == 'origin_country') {
                    $delete = PivCategory::where([['product_id',$v->product_id],['category_id',$v->category_id]])->delete();
                }
            }
        }

            $pivOriginFilter = PivCategory::where([['product_id',$data['gid']]])->get();
            $delete = PivCategory::where([['product_id',$v->product_id]])->delete();

            $collection = collect($pivOriginFilter);
            $unique = $collection->unique('category_id');

            foreach ($unique as $k => $v) {
                $pivOrigins = new PivCategory;

                $pivOrigins->product_id = $v->product_id;
                $pivOrigins->category_id = $v->category_id;

                $pivOrigins->save();
            }
    }

    function createNewPivOrigin($data) {

        $pivOrigin = PivCategory::where([['product_id',$data['gid']],['category_id',$data['origin']]])->first();

        if(!$pivOrigin) {
            $pivOrigin = new PivCategory;
        }

        $pivOrigin->product_id = $data['gid'];
        $pivOrigin->category_id = $data['origin'];

        $pivOrigin->save();
    }


}
