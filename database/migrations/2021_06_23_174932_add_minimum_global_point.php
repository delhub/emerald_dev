<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddMinimumGlobalPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_statuses', function (Blueprint $table) {

        });

        DB::table('global_statuses')->insert(
            array(
                'id'=> '1',
                'name'=> 'Minimum Point For Purchase',
                'description'=> 2000,
                'status_type'=> 'minimum_point',
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            )
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_statuses', function (Blueprint $table) {
            //
        });
    }
}
