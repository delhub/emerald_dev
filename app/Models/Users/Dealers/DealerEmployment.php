<?php

namespace App\Models\Users\Dealers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DealerEmployment
 *
 * @property int $id
 * @property int $account_id
 * @property int|null $employment_type
 * @property string|null $company_name
 * @property string|null $company_address_1
 * @property string|null $company_address_2
 * @property string|null $company_address_3
 * @property string|null $company_postcode
 * @property string|null $company_city
 * @property string $company_state_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models\Users\Dealers
 */
class DealerEmployment extends Model
{
    // Set table
    protected $table = 'dealer_employments';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Casts
    protected $casts = [
        'account_id' => 'int',
        'employment_type' => 'int'
    ];

    // Set mass assignable columns
    protected $fillable = [
        'account_id',
        'employment_type',
        'company_name',
        'company_address_1',
        'company_address_2',
        'company_address_3',
        'company_postcode',
        'company_city',
        'company_city_key',
        'company_state_id'
    ];

    /**
     * Get state.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\Globals\State', 'company_state_id');
    }
    public function cityKey()
    {
        return $this->belongsTo('App\Models\Globals\City', 'company_city_key', 'city_key');
    }
}
