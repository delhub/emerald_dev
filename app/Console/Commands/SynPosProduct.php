<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\PosProductAPI;

class SynPosProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syn:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize pos product api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(PosProductAPI::synAll()) {
            echo 'Synchronize Product --- OK';
        }
        else {
            echo 'Error -- Synchronize Failed!...';
        }

    }
}
