<?php

use App\Models\Globals\Countries;
use App\Models\Globals\Settings;
use App\Models\Globals\Status;
use App\Models\Warehouse\Location\Wh_location;
use Illuminate\Support\Facades\Auth;
use App\Models\Pick\PickBatchDetail;
use App\Models\Warehouse\Order\WarehouseOrder;

if (!function_exists('country')) {

    function country()
    {
        return Countries::currentCountry();
    }
}

if (!function_exists('warehouseLocation')) {

    function warehouseLocation($id)
    {
        return Wh_location::find($id);
    }
}

if (!function_exists('showPickPackStatus')) {

    function showPickPackStatus($pick_pack_status)
    {
        switch ($pick_pack_status) {
            case 1:
                $result = 'Pick Started';
                break;
            case 2:
                $result = 'Pick Ended';
                break;
            case 3:
                $result = 'Sort Start';
                break;
            case 4:
                $result = 'Sort End';
                break;
            case 5:
                $result = 'Pack Start';
                break;
            case 6:
                $result = 'Pack End';
                break;
            case 9:
                $result = 'Ignored';
                break;

            default:
                $result = 'Ready to Pick';
        }

        return $result;
    }
}

if (!function_exists('showPickPackBatch')) {

    function showPickPackBatch($delivery_order)
    {
        $result = PickBatchDetail::where('delivery_order', $delivery_order)->first();
        if (isset($result)) {
            return '(' . $result->batch->batch_number . ')';
        } else {
            return;
        }
    }
}


if (!function_exists('showLocationName')) {

    function showLocationName($location_id)
    {
        $location =  Wh_location::find($location_id);
        return $location->location_name;
    }
}

if (!function_exists('showCompletedStatus')) {

    function showCompletedStatus($completed)
    {
        if ($completed == 1) {
            return 'Completed';
        } elseif ($completed == -1) {
            return 'Cancelled';
        } {
            return 'Pending';
        }
    }
}

if (!function_exists('showGlobalProductQuality')) {

    function showGlobalProductQuality($quality)
    {
        switch ($quality) {
            case 1:
                $result = 'Domestic';
                break;
            case 2:
                $result = 'Import';
                break;
            case 3:
                $result = 'Exclusive';
                break;

            default:
                $result = '-';
        }

        return $result;
    }
}

if (!function_exists('showWarehouseTransferType')) {

    function showWarehouseTransferType($type)
    {
        switch ($type) {
            case 0:
                $result = 'Auto';
                break;
            case 1:
                $result = 'Direct Transfer';
                break;
            case 2:
                $result = 'Batch Transfer';
                break;

            default:
                $result = '-';
        }

        return $result;
    }
}

if (!function_exists('showWarehouseTransferstatus')) {

    function showWarehouseTransferstatus($status)
    {
        switch ($status) {
            case -1:
                $result = 'Canceled';
                break;
            case 1:
                $result = 'Transfering start';
                break;
            case 3:
                $result = 'Transfer Done';
                break;
            case 4:
                $result = 'Receiving Start';
                break;
            case 5:
                $result = 'Completed';
                break;

            default:
                $result = '-';
        }

        return $result;
    }
}

if (!function_exists('showGloabalStatus')) {

    function showGloabalStatus($id)
    {
        $gloablStatusName = Status::where('id', $id)->first()->name;
        return $gloablStatusName;
    }
}

if (!function_exists('getUser')) {

    function getUser($info)
    {
        $user = Auth::user();

        switch ($info) {
            case 'full_name':
                $result = $user->userInfo->full_name;
                break;

            case 'warehouseLocationName';

                $userLocationId = $user->user_warehouse_location;
                $warehouseLocation = Wh_location::whereIn('id', $userLocationId)->get('location_name');

                if ($warehouseLocation->isNotEmpty()) {

                    foreach ($warehouseLocation as $key => $location) {
                        $userLocationName[] = $location->location_name;
                    }

                    $result = $userLocationName;
                    break;
                }

            default:
                $result = '-';
        }

        return $result;
    }
}

if (!function_exists('setting')) {

    function setting($settingType)
    {
        $setting = Settings::where('name',$settingType)->first();
        return $setting->value;
        }
    }
if (!function_exists('getPickOrderId')) {

    function getPickOrderId($type, $picking_order_id)
    {
        $result = '-';
        switch ($type) {
            case 'pick':

                $result = PickBatchDetail::find($picking_order_id);
                if (isset($result)) $result = $result->order->purchase->purchase_number;
                break;

            case 'pos':

                $result = WarehouseOrder::find($picking_order_id);
                if (isset($result)) $result = $result->invoice_number;
                break;

            default:
                $result = '-';
                break;
        }

        return $result;
    }
}
