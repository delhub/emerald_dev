<?php

namespace App\Models\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Warehouse\Location\Wh_location;
use App\Models\Globals\Outlet;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, HasRoles;


    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'last_login_at',
        'last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /* Attribute to set primary user */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    /**
     * Get user's image/
     */
    public function image()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable');
    }

    /**
     * Get the user info associated with the user.
     * each user has one user info
     */
    public function userInfo()
    {
        return $this->hasOne('App\Models\Users\UserInfo', 'user_id');
    }

    /**Get panel account id of panel**/
    public function panelInfo()
    {
        return $this->hasOne('App\Models\Users\Panels\PanelInfo', 'user_id');
    }

    /***Get dealer account id of dealer**/

    public function dealerInfo()
    {
        return $this->hasOne('App\Models\Users\Dealers\DealerInfo', 'user_id');
    }

    /**
     * Get the user address associated with the user.
     */

    public function userAddresses()
    {
        return $this->hasMany('App\Models\Users\UserAddress', 'user_id');
    }

    /**
     * Get the user's billing address.
     */
    public function billingAddress()
    {
        return $this->hasOne('App\Models\Users\UserAddress', 'account_id')->where('is_mailing_address', 1);
    }

    /**
     * Get the user's shipping address.
     */
    public function shippingAddress()
    {
        return $this->hasOne('App\Models\Users\UserAddress', 'user_id')->where('is_shipping_address', 1);
    }

    /**
     * Get the user contact associated with the user.
     */
    public function userContacts()
    {
        return $this->hasMany('App\Models\Users\UserContact', 'id');
    }
    /**
     * Get the user's mobile contact.
     */
    public function mobileContact()
    {
        return $this->hasOne('App\Models\Users\UserContact', 'account_id')->where('is_mobile', 1);
    }

    /**
     * Get the user's emergency contact.
     */
    public function emergencyContact()
    {
        return $this->hasOne('App\Models\Users\Usercontact', 'user_id')->where('is_emergency', 1);
    }

    /**
     * Get user's purchase.
     */
    public function purchases()
    {
        return $this->hasMany('App\Models\Purchases\Purchase', 'user_id', 'id');
    }

    /**
     * Get all items in customer's cart.
     */
    public function carts()
    {
        return $this->hasMany('App\Models\Users\Customers\Cart', 'user_id', 'id');
    }

    /**
     * Get all items in customer's favorite.
     */
    public function favorites()
    {
        return $this->hasMany('App\Models\Users\Customers\Favorite', 'user_id', 'id');
    }

    public function userMembership()
    {
        return $this->hasOne(UserMembershipLogs::class, 'user_id', 'id');
    }

    // Eloquent Scopes

    /**
     * Get largest customer account id.
     */
    public function scopeLargestCustomerId($query)
    {
        return $query->where('account_id', 'LIKE', '1913%')->max('account_id');
    }

    /**
     * Get largest dealer account id.
     */
    public function scopeLargestDealerId($query)
    {
        // return $this->where('account_id', 'LIKE', '1911%')->max('account_id');
        return $query->where('account_id', 'LIKE', '1911%')->max('account_id');
    }

    /**
     * Get largest panel account id.
     */
    public function scopeLargestPanelId($query)
    {
        return $query->where('account_id', 'LIKE', '1918%')->max('account_id');
    }

    /**
     * Get largest administrator account id.
     */
    public function scopeLargestAdministratorId($query)
    {
        $query->where('account_id', 'LIKE', '1919%')->max('account_id');
    }

    public function getFormattedtDate($date)
    {
        return (new Carbon($date))->format('d/m/Y');
    }

    public function paymentToken()
    {
        return $this->hasMany('App\Models\Users\UserPaymentToken', 'user_id', 'id');
    }

    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {
            $query
            ->join('user_infos', 'user_infos.user_id', '=', 'users.id');

            if (in_array($selectSearch,['name','NM'])) {
                return $query
                    ->where('user_infos.full_name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'email') {

                return $query
                    ->where('users.email', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'account_type') {

                return
                    $query->whereHas('roles', function ($query) use ($searchFilter) {
                        $query->where('roles.name', 'like', '%' . $searchFilter . '%');
                    });
            } elseif ($selectSearch == 'ID') {
                return
                    $query->where('user_infos.account_id', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }

    public function scopewarehouseLocationFilter($query,  $location_id)
    {
        if (!is_null($location_id)) {
            return $query->whereHas('userInfo', function (Builder $query) use ($location_id) {
                $query->whereHas('warehouseLocation', function (Builder $query) use ($location_id) {
                    $query->where('location_id', 'like', $location_id);
                });
            });
        }

        return $query;
    }

    public function getUserWarehouseLocationAttribute()
    {
        $userLocationId = array();
        $userwarehouseLocation = $this->userInfo->warehouseLocation;
        if ($userwarehouseLocation != NULL) {
            foreach ($userwarehouseLocation as $location) {
                $userLocationId[] = $location->location_id;
            }
        }

        return $userLocationId;
    }
    public function getUserWarehouseOutletAttribute()
    {
        $warehouseLocation = $this->getUserWarehouseLocationAttribute();

        $whLocation = Wh_location::whereIn('id', $warehouseLocation)->pluck('location_key');
        $outlet_id = Outlet::whereIn('outlet_key', $whLocation)->pluck('id');

        return $outlet_id;
    }

    public function scoperankFilter($query, $rank,$searchBox = null)
    {
        if (!is_null($rank)) {
            if ($rank != 'ALL') {
                if (is_null($searchBox)) {
                    $query->join('user_infos', 'user_infos.user_id', '=', 'users.id');
                }
                $query = $query->where('user_infos.user_level', $rank);
            }

        }

        return $query;
    }
    public function scopePurchaseFilter($query)
    {
        $query = $query->join('purchases','purchases.user_id','users.id');

        return $query;
    }
}


