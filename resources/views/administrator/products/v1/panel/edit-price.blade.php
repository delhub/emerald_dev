<form name="form" action="{{ route('administrator.v1.products.panels.price.update', ['id' => $productPrice->id]) }}"
    method="POST" id="formID">
    @csrf
    @method('POST')

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Model Type</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>{{ $productPrice->model_type }}</p>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Panel Product Name / Attribute Name</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>
                {{isset($productPrice->model->parentProduct->name) ? $productPrice->model->parentProduct->name : '' }}
                {{isset($productPrice->model->attribute_name) ? $productPrice->model->attribute_name : '' }}

            </p>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Prouct Code</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>
                {{isset($productPrice->model->parentProduct->product_code) ? $productPrice->model->parentProduct->product_code : '' }}
                {{isset($productPrice->model->product_code) ? $productPrice->model->product_code : '' }}
            </p>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Product Quanlity</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>
                {{isset($productPrice->model->parentProduct->quality->name) ? $productPrice->model->parentProduct->quality->name : '' }}
                {{isset($productPrice->model->product->parentProduct->quality->name) ? $productPrice->model->qproduct->parentProductuality->name : '' }}
            </p>
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Country</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <p class="form-control" readonly>{{ $productPrice->country_id }}</p>
        </div>
    </div>

    <hr>

    @if(isset($productPrice->model->fixed_showitem))
    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
        </div>
            <div class="col-12 col-md-6 form-check" style="padding-left: 1rem;">
                <input class="form-check-input" type="checkbox" name="fixed_showitem"
                    id="fixed_showitem" value="1" style="margin-left: 0rem;margin-top: 10px;"
                    {{ ($productPrice->model->fixed_showitem == "1") ? "checked" : "" }}>

                <label class="form-check-label" for="fixed_showitem"
                        style="margin-left: 20px;margin-top: 5px;"><small
                        class="text-danger">*</small>if tick will show fixed/selling price
                </label>
            </div>
        </div>
    </div>
    @endif
    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Fixed Price</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <input type="text" name="fixed_price" id="fixed_price" class="form-control input-mask-price" value="{{ $productPrice->fixed_price }}">
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>BJS Price</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <input type="text" name="price" id="price" class="form-control input-mask-price" value="{{ $productPrice->price }}">
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>DC Customer's Price</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <input type="text" name="member_price" id="member_price" class="form-control input-mask-price" value="{{ $productPrice->member_price }}">
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Offer Price</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <input type="text" name="offer_price" id="offer_price" class="form-control input-mask-price" value="{{ $productPrice->offer_price }}">
        </div>
    </div>

    <div class="form-row">
        <div class="col-12 col-md-4 my-auto text-right">
            <p>Product SV</p>
        </div>
        <div class="col-12 col-md-6 form-group">
            <input type="text" name="product_sv" id="product_sv" class="form-control" value="{{ $productPrice->product_sv }}">
        </div>
    </div>

    <div class="form-row mt-2">
        <div class="col-12 col-md-6 offset-md-4">
            <button id="modalSubmitBtn" type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i>
                <span class="spinner-border spinner-border-sm" style="display: none;" role="status" aria-hidden="true"></span>
                Submit
            </button>
        </div>
    </div>

</form>
