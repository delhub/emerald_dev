<?php

namespace App\Http\Controllers\Administrator\ShippingInstallations;

use App\Http\Controllers\Controller;

class ShippingInstallationsController extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function rateTable()
    {
       return view('administrator.shipping-installations.rate-table');
    }
}