<?php

namespace App\Http\Controllers\Administrator\v1\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use File;

use App\Models\Globals\Products\Product as GlobalProduct;
use App\Models\Categories\Category;
use App\Models\Globals\Quality;
use App\Models\Products\ProductBrand;
use App\Models\Products\ProductBrandLogs;
use App\Traits\Voucher;
use Carbon\Carbon;

class ProductController extends Controller
{
    use Voucher;
    public static $category_type;

    public function __construct()
    {

        self::$category_type = array(
            'shop' => 'Product Categories',
            'agent' => 'Agent Report Categories',
            'accounting' => 'Accounting Categories'
        );
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        dd(1);
        // Handles AJAX request if it exists.
        if ($request->ajax()) {
            $products = new ProductCollectionResource(GlobalProduct::all());

            return response()->json($products, 200);
        }

        // Return view.
        return view('administrator.products.v1.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.products.v1.global.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $qualities = Quality::all();
        $categories = Category::all();
        $product = GlobalProduct::findOrFail($id);
        $tradein = $this->getTradeInRule();

        return view('administrator.products.v1.global.edit')
            ->with('qualities', $qualities)
            ->with('categories', $categories)
            ->with('tradein', $tradein)
            ->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories = array();
        foreach (self::$category_type as $key => $type) {

            $categories =  array_merge($categories, $request->input($key . 'categories'));
        }

        $product = GlobalProduct::findOrFail($id);
        $product->product_type = $request->input('product_type');
        $product->name = $request->input('productName');
        $product->name_slug = Str::slug($request->input('productName'), '-');
        $product->product_code = $request->input('productCode');
        // $product->details = $request->input('productDetails');
        // $product->description = $request->input('productDescriptions');
        $product->quality_id = $request->input('productQuality');
        $product->tradein_rule = $request->input('tradein_rule');
        if ($request->input('productPublished')) {
            $product->product_status = 1;
        } else {
            $product->product_status = 2;
        }
        if ($request->input('bluepointPurchaseable')) {
            $product->bluepoint_purchaseable = 1;
        } else {
            $product->bluepoint_purchaseable = 0;
        }
        if ($request->input('canPreorder')) {
            $product->can_preorder = 1;
        } else {
            $product->can_preorder = 0;
        }

        if ($request->input('canRemark')) {
            $product->can_remark = 1;
        } else {
            $product->can_remark = 0;
        }
        if ($request->input('canSelfcollect')) {
            $product->can_selfcollect = 1;
        } else {
            $product->can_selfcollect = 0;
        }

        if (isset($request->canInstallment['check']) && $request->canInstallment['check'] == '1') {

            $installmentDesc = $request->canInstallment;

            $installmentDesc['productDescription'] = $request->productName;
            unset($installmentDesc['check']);

            $request->canInstallment = $installmentDesc;
            $product->can_installment = json_encode($request->canInstallment);
        } else {
            $product->can_installment = 0;
        }

        if ($request->input('canTradeIn')) {
            $product->can_tradein = 1;
        } else {
            $product->can_tradein = 0;
        }

        if ($request->input('eligibleDiscount')) {
            $product->eligible_discount = 0;
        } else {
            $product->eligible_discount = 1;
        }

        if ($request->input('generateDiscount')) {
            $product->generate_discount = 0;
        } else {
            $product->generate_discount = 1;
        }

        $product->save();

        if ($request->file('productImage')) {
            // Get existing default image.
            $defaultImages = $product->images->where('default', 1);

            // Delete if default image exists.
            if ($defaultImages->count() != 0) {
                foreach ($defaultImages as $defaultImage) {
                    $defaultImage->delete();
                }
            }

            // Store new default image.
            $image = $request->file('productImage');
            $imageDestination = public_path('/storage/uploads/images/products/' . $product->id . '/');
            $imageName = $product->id . '-' . 'default' . '.' . $image->getClientOriginalExtension();
            $image->move($imageDestination, $imageName);

            // Create record in DB.
            $product->images()->create([
                'path' => 'uploads/images/products/' . $product->id . '/',
                'filename' => $imageName,
                'default' => 1
            ]);
        }

        if ($request->file('brandLogo')) {
            // Get existing default image.
            $brandImages = $product->images->where('brand', 1);

            // Delete if default image exists.
            if ($brandImages->count() != 0) {
                foreach ($brandImages as $brandImage) {
                    $brandImage->delete();
                }
            }

            // Store new default image.
            $image = $request->file('brandLogo');
            $imageDestination = public_path('/storage/uploads/images/products/' . $product->id . '/');
            $imageName = $product->id . '-' . 'brand' . '.' . $image->getClientOriginalExtension();
            $image->move($imageDestination, $imageName);

            // Create record in DB.
            $product->images()->create([
                'path' => 'uploads/images/products/' . $product->id . '/',
                'filename' => $imageName,
                'brand' => 1
            ]);
        }

        $product->categories()->sync($categories);

        return redirect(route('administrator.products'))
            ->with('success', $product->name . ' saved successfully!');
    }

    /**
     * Return image if exists to DropzoneJS
     */
    public function getImage(Request $request, $id)
    {
        $product = GlobalProduct::find($id);
        $result  = array();
        $storeFolder = public_path('/storage/uploads/images/products/' . $product->product_code . '/');
        // return $product->product_code;
        if ($product->product_code != null) {

            $files = scandir($storeFolder);

            if (false !== $files) {
                foreach ($files as $file) {
                    if ('.' != $file && '..' != $file) {       //2
                        $obj['name'] = $file;
                        $obj['size'] = filesize($storeFolder . '/' . $file);
                        $result[] = $obj;
                    }
                }
            }

            return response()->json($result);
        } else {
            return response();
        }
    }

    /**
     * Store image through DropzoneJS
     */
    public function storeImage(Request $request, $id)
    {
        $product = GlobalProduct::findOrFail($id);

        $image = $request->file('file');
        $imageDestination = public_path('/storage/uploads/images/products/' . $product->id . '/');
        $imageName = $product->id . '-' . date('Y-m-d-H:i:s');
        $image->move($imageDestination, $imageName);

        $product->images()->create([
            'path' => 'uploads/images/products/' . $product->id . '/',
            'filename' => $imageName,
            'default' => 0
        ]);

        return response()->json(['success' => $imageName]);
    }

    /**
     * Delete image through DropzoneJS.
     */
    public function deleteImage(Request $request, $id)
    {
        $product = GlobalProduct::findOrFail($id);
        $path = public_path('/storage/uploads/images/products/' . $product->product_code . '/');
        $filename = $request->input('filename');
        if (File::exists($path . $filename)) {
            File::delete($path . $filename);
            $message = 'true';
        } else {
            $message = 'false';
        }
        $product->images()->where('filename', $filename)->delete();
        return $filename;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reports (){
        //
        $monthlyReports = array();
        $qualityReports = array(
             1 => array(
                 'brand' => array(),
                 'product' => array(
                     0 => array(
                         0 =>array(),
                         1 =>array(),
                         2 =>array(),
                     ),
                     1 => array(
                        0 =>array(),
                        1 =>array(),
                        2 =>array(),
                     ),
                 ),
             ),
             2 => array(
                'brand' => array(),
                'product' => array(
                    0 => array(
                        0 =>array(),
                        1 =>array(),
                        2 =>array(),
                    ),
                    1 => array(
                        0 =>array(),
                        1 =>array(),
                        2 =>array(),
                    ),
                ),
             ),
             3 => array(
                'brand' => array(),
                'product' => array(
                    0 => array(
                        0 =>array(),
                        1 =>array(),
                        2 =>array(),
                    ),
                    1 => array(
                        0 =>array(),
                        1 =>array(),
                        2 =>array(),
                    ),
                ),
             ),
         );


        $brands = ProductBrand::all();
        $globalProducts = GlobalProduct::get(['id','quality_id','brand_id','product_status','display_only']);
        $quality = Quality::all();

         foreach ($brands as $brand) {
            $products = $globalProducts->where('brand_id',$brand->id);
            foreach ($products as $product) {
                $qualityReports[$product->quality_id]['brand'][$brand->id][] = $product;
            }
        }


        foreach ($globalProducts as $globalProduct) {
            $qualityReports[$globalProduct->quality_id]['product'][$globalProduct->display_only][$globalProduct->product_status][] = $globalProduct->id;
        }

        // dd($qualityReports[1]['product'][0][2]);

        $productBrandLogs = ProductBrandLogs::all();

        foreach ($productBrandLogs as $key => $productBrandLog) {
            $month = Carbon::parse($productBrandLog->log_month)->format('M');
            $array = $productBrandLog->toArray();
            // dd($array);
            $monthlyReports[$month]['tr'] = $month;
            $monthlyReports[$month]['td']['brand_count'] = $array['brand_count'];
            $monthlyReports[$month]['td']['product_count'] = $array['product_count'];
            $monthlyReports[$month]['td']['new_brand_count'] = $array['new_brand_count'];
            $monthlyReports[$month]['td']['new_product_count']= $array['new_product_count'];
            $monthlyReports[$month]['td']['product_suspended'] = $array['product_suspended'];
            $monthlyReports[$month]['td']['product_terminate'] = $array['product_terminate'];
        }
        // dd($monthlyReports);

        return view('administrator.panels.reports')
        ->with('qualityReports',$qualityReports)
        ->with('monthlyReports',$monthlyReports)
        ->with('quality',$quality)
        ;
    }
}
