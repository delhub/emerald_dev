<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateIconsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('categories')->insert(
            array(
                array(
                    'name' => 'Oral Care',
                    'slug' => 'oral-care',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Mouth/Lungs',
                    'slug' => 'mouth-lungs',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Immunity Care',
                    'slug' => 'immunity-care',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Muscle Care',
                    'slug' => 'muscle-care',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Heart Care',
                    'slug' => 'heart-care',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Liver Care',
                    'slug' => 'livercare',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Blood Pressure',
                    'slug' => 'blood-pressure',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Bone& Joint Care',
                    'slug' => 'bone-care',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Kidney Care',
                    'slug' => 'kidney',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Cosmetic/ Apperance Care',
                    'slug' => 'cosmetic-apperance-care',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Skin Care',
                    'slug' => 'skincare',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Body Wash',
                    'slug' => 'bodywash',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'High Cholesterol',
                    'slug' => 'high-cholesterol',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Hair Care (Men)',
                    'slug' => 'haircare(men)',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Hair Care (Women)',
                    'slug' => 'haircare(women)',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Weight Management',
                    'slug' => 'weight-management',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'name' => 'Skin Allergic',
                    'slug' => 'skin-allergic',
                    'type' => 'shop',
                    'parent_category_id' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),

            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('icons_categories');
    }
}
