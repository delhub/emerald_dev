@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-3 d-flex justify-content-between align-items-center">
            <h1 style="display:inline;">Product Restriction</h1>
            <a style="color: white; font-style: normal; border-radius: 5px;" 
                id="limit-create"
                class="btn btn-dark" 
                data-toggle="modal" 
                data-target="#limitModal" 
                href="#limitModal">
                New Restriction
            </a>
        </div>
    </div>
    <div class="col-12">
        <form method="GET" action="{{ route('administrator.product.product-restriction') }}" class="row">
            @csrf
            <div class="form-group col-12 col-md-4">
                <label for="code_filter"><b>Product Code</b></label>
                <input class="form-control" id="code_filter" type="text" name="code_filter" placeholder="Product Code Filter">
            </div>
            <div class="form-group col-12 col-md-4">
                <label for="type_filter"><b>Restriction Type</b></label>
                <select name="type_filter" id="type_filter" class="select2 form-control">
                    <option value="" selected disabled >Limit Type Filter</option>
                    <option value="roles">Roles Restriction</option>
                    <option value="agent">Agent Restriction</option>
                </select>
            </div>
            <div id="user_filter_container" class="form-group col-12 col-md-4" style="display: none">
                <label for="user_filter"><b>Users</b></label>
                <select id="user_filter" name="user_filter" class="select2 form-control">
                </select>
            </div>
            <div id="roles_filter_container" class="form-group col-12 col-md-4" style="display: none">
                <label for="roles_filter"><b>Roles</b></label>
                <select id="roles_filter" name="roles_filter" class="form-control">
                </select>
            </div>
            <div class="col-12 text-right p-0">
                <button type="submit" class="btn btn-warning">
                    <span>Search</span>
                </button>
                <a class="btn btn-warning" href="{{ route('administrator.product.product-restriction') }}">
                    <span>Reset Filter</span>
                </a>
            </div>
        </form>
    </div>
    <div class="table-responsive m-2">
        <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
            <thead>
                <tr>
                    <td>No.</td>
                    <td>Product Code</td>
                    <td>Restriction Type</td>
                    <td>Restriction Details</td>
                    <td>Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($limits as $code => $limit)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $code . ' - ' . $limit['product_name'] }}</td>
                    <td>{{ ucwords($limit['limit_type']) }}</td>
                    <td>
                        @switch($limit['limit_type'])
                            @case('agent')
                                @foreach($limit['details'] as $details)
                                    <p> {{ $details['account_id'] . $details['name'] . ': ' . $details['amount'] . 'qty ( ' . ucwords($details['duration']) . ' )' }} </p>
                                @endforeach
                                @break

                            @case('roles')
                                <p>{{ ucwords($limit['details'][0]['duration']) . ' - ' . $limit['details'][0]['amount'] .'qty' }}</p>
                                <p>Roles: {{ $limit['details'][0]['roles'] }}</p>
                                @break
                        @endswitch
                    </td>
                    <td>
                        <div class="d-flex">
                            <button class="btn btn-primary shadow-sm limit-edit mr-1"
                                style="color: white; font-style: normal; border-radius: 5px;"
                                data-id="{{ $limit['attribute_id'] }}"
                                data-displayName="{{ $code . ' - ' . $limit['product_name'] }}"
                                data-type="{{ $limit['limit_type'] }}"
                                data-toggle="modal" data-target="#limitModal" href="#limitModal">
                                Edit
                            </button>
                            <button class="btn btn-danger shadow-sm limit-remove" style="color: white; font-style: normal; border-radius: 5px;"
                                data-toggle="modal" data-target="#limitRemoveModal" href="#limitRemoveModal"
                                data-id="{{ $limit['attribute_id'] }}"
                                data-code="{{ $code }}">
                                Remove
                            </button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="limitModal" tabindex="-1" role="dialog" aria-labelledby="limitBtn" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-capitalize">Product Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('administrator.product.restriction-submit') }}" method="POST">
                @csrf
                <div class="modal-body row"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="limitRemoveModal" tabindex="-1" role="dialog" aria-labelledby="limitRemoveBtn" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-capitalize">Limit Remove Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('administrator.product.restriction-remove') }}" method="POST">
                @csrf
                <div class="modal-body row">
                    <input type="hidden" name="attr_id">
                    <div class="col-12">
                        <p>Are you sure you want to remove <span id="limit-code-remove"></span></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="agent-options" class="d-none">
    <option value="" selected disabled >Please select Agent</option>
    @foreach($dealers as $dealer)
        <option value="{{ $dealer->user_id }}">{{ $dealer->account_id . ' - ' . $dealer->full_name }}</option>;
    @endforeach
    <option value="-1">Others</option>
</div>
<div id="roles-options" class="d-none">
    @foreach($roles as $role)
        <option value="{{ $role->name }}">{{ $role->name }}</option>
    @endforeach
</div>
<div id="attr-options" class="d-none">
    <option value="" selected disabled >Please select product code.</option>
    @foreach($prodAttr as $attr)
        <option value="{{ $attr->id }}">{{ $attr->product_code . ' - ' . $attr->product2->parentProduct->name . ' (' . $attr->attribute_name . ')' }}</option>
    @endforeach
</div>
@endsection

@push('script')
<script>
    let limitModal = $('#limitModal')
    let limitModalBody = limitModal.find('.modal-body')

    $(document).ready(function(){
        $('select.select2').select2({
            theme: 'bootstrap4',
        })
        $('#code_filter').val("{{ $request->code_filter }}")
        $('#type_filter').val("{{ $request->type_filter }}").trigger('change')
        $('#roles_filter').val("{{ $request->roles_filter }}").trigger('change')
        $('#user_filter').val("{{ $request->user_filter }}").trigger('change')
    })
    
    $(document).on('click', '#limit-create', function(){
        limitModal.find('.modal-title').text('New Limit')
        var modalsubmitbtn = limitModal.find('button[type="submit"]')
        modalsubmitbtn.attr('disabled', true)

        limitModalBody.html(getHtml('data-select'))
        limitModalBody.find('select.ds-select2').select2({
            theme: 'bootstrap4',
        })
        limitModal.show()
    })

    $(document).on('change', 'select[name="limit_type"]', function(){
        
        modalsubmitbtn = limitModal.find('button[type="submit"]')
        modalsubmitbtn.attr('disabled', false)
        
        var triggeringElement = $('#new-limit-select-div')
        limitModalBody.empty()
        limitModalBody.append(triggeringElement)

        var limit_type = $(this).val()
        switch (limit_type) {
            case 'roles':
            limitModalBody.append(getHtml(limit_type))
                break;

            case 'agent':
            limitModalBody.append(getHtml(limit_type))
            limitModalBody.append(getHtml('agent-btn'))
                break;

            default:
                modalsubmitbtn.attr('disabled', true)
                break;
        }
        var selectElement = limitModalBody.find('select.s-select2');
        

        selectElement.select2({
            theme: 'bootstrap4',
        })
    })

    $(document).on('click', '.limit-edit', function(){
        var attr_id = $(this).attr('data-id')        
        var display_name = $(this).attr('data-displayName')
        var limit_type = $(this).attr('data-type')
        limitModal.find('.modal-title').text(limit_type + ` Restriction: ` + display_name)
    
        getLimitAjax(attr_id, limit_type, function(datas){
            limitModalBody.append(getHtml('data-hidden'))
            limitModalBody.find('input[name="attr_id"]').val(attr_id)
            limitModalBody.find('input[name="limit_type"]').val(limit_type)
            switch (limit_type) {
                case 'roles':
                    limitModalBody.append(getHtml(limit_type))
                    limitModalBody.find('select.s-select2').select2({
                        theme: 'bootstrap4',
                    })
                    data = datas[0]
                    
                    limitModalBody.find('#edit_attr_id').val(data.attribute_id)
                    limitModalBody.find('#edit_limit_type').val(limit_type)
                    limitModalBody.find('input[name="product_limit_id"]').val(data.id).trigger('change')
                    limitModalBody.find('select[name="product_limit_duration"]').val(data.duration).trigger('change')
                    limitModalBody.find('select[name="product_limit_roles[]"]').val(data.roles).trigger('change')
                    limitModalBody.find('input[name="purchase_limit_amount"]').val(data.amount)
                    
                    break;
                case 'agent':
                    datas.forEach(function(data, i){       
                        var html_type = limit_type                  

                        limitModalBody.append(getHtml(limit_type))
                        var agentData = limitModalBody.find('.agent-data').last()
                        agentData.find('select.s-select2').select2({
                            theme: 'bootstrap4',
                        })
                        agentData.find('input[name="limit_id[]"]').val(data.id)
                        agentData.find('select[name="agent_limit_duration[]"]').val(data.duration).trigger('change')
                        agentData.find('select[name="agent_limit_id[]"]').val(data.user_id).trigger('change')
                        agentData.find('input[name="agent_limit_amt[]"]').val(data.amount)

                    })
                    limitModalBody.append(getHtml('agent-btn'))

                    break;
            }
        })
    })

    $(document).on('click', '.limit-remove', function(){
        var code = $(this).attr('data-code')
        var attr_id = $(this).attr('data-id')
        var modal = $('#limitRemoveModal')
        modal.find('#limit-code-remove').text(code)
        modal.find('input[name="attr_id"]').val(attr_id)
    })

    $(document).on('hidden.bs.modal', '.limit-remove', function(){
        modal.find('#limit-code-remove').text('')
    })

    $(document).on('click', '.agent_add', function(){
        limitModalBody.find('.agent-data').last().after(getHtml('agent'))
        var newLast = limitModalBody.find('.agent-data').last()
        $(newLast).find('select.s-select2').select2({
            theme: 'bootstrap4',
        })        
    })

    $(document).on('click', '.agent_remove', function(){
        $(this).closest('.row.agent-data').remove()
    })

    $(document).on('hidden.bs.modal', '#limitModal', function(){
        limitModalBody.empty()
    })

    $(document).on('change', '#type_filter', function(){
        var limitType = $(this).val()
        // Agent Filter
        var userFilterContainer = $('#user_filter_container')
        var userFilterSelect = userFilterContainer.find('#user_filter')
        
        // Roles Filter
        var rolesFilterContainer = $('#roles_filter_container')
        var rolesFilterSelect = rolesFilterContainer.find('#roles_filter')

        switch ($(this).val()) {
            case 'agent':
                var agentOption = $('#agent-options').html()
                
                rolesFilterSelect.empty()
                rolesFilterContainer.hide()
                userFilterSelect.html(agentOption)
                userFilterContainer.show()
                break;

            case 'roles':
                var rolesOption = `<option value="" selected disabled >Please select Roles</option>` + $('#roles-options').html()
                
                userFilterSelect.empty()
                userFilterContainer.hide()
                rolesFilterSelect.html(rolesOption)
                rolesFilterContainer.show()
                break;
        
            default:
                userFilterSelect.empty()
                userFilterContainer.hide()
                rolesFilterSelect.empty()
                rolesFilterContainer.hide()
                break;
        }
    })

    function getLimitAjax(attr_id, limit_type, callback){        
        $.ajax({
            url: "{{ route('administrator.get-agent-list') }}",
            method: 'GET',
            data: {
                attr_id: attr_id,
                limit_type: limit_type
            },
            success: function(result) {      
                callback(result.datas)
            },
            error: function(result) {
                console.log(result.status + ' ' + result.statusText);
            }
        })
    }

    function getHtml(select){
        var html = ``

        switch (select) {
            case 'data-hidden':
                html += `<input type="hidden" name="attr_id" id="edit_attr_id">
                        <input type="hidden" name="limit_type" id="edit_limit_type">`
                break;

            case 'data-select':
                html += `<div id="new-limit-select-div" class="col-12">
                            <label for="attr_id" class="col-form-label font-weight-bold">Product Code</label>
                            <select name="attr_id" class="ds-select2 form-control pc" required>`
                html +=         $('#attr-options').html()          
                html +=     `</select>
                            <label for="limit_type" class="col-form-label font-weight-bold">Limit Type</label>
                            <select name="limit_type" class="select2 form-control pc" required>
                                <option value="" selected disabled >Please select limit type.</option>
                                <option value="roles">Roles Restriction</option>
                                <option value="agent">Agent Restriction</option>
                            </select>
                        </div>`
                break;

            case 'agent-btn':
                html += `<div class="col-12">
                            <button class="my-2 mb-0 agent_add btn btn-success mt-1" type="button">New</button>
                        </div>`
                break;
                
            case 'agent':
                html += `<div class="row col-12 agent-data">
                            <input type="hidden" name="limit_id[]">
                            <div class="col-12 col-sm-12 col-md-4">
                                <label for="agent_limit_duration[]" class="col-form-label font-weight-bold">Duration</label>
                                <select name="agent_limit_duration[]" class="form-control s-select2 pc" required>
                                    <option value="" selected disabled >Please choose duration.</option>
                                    <option value="yearly">Yearly</option>
                                    <option value="monthly">Monthly</option>
                                    <option value="weekly">Weekly</option>
                                    <option value="daily">Daily</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4">
                                <label for="agent_limit_id[]" class="col-form-label font-weight-bold">Dealer</label>
                                <select class="s-select2 form-control" name="agent_limit_id[]" required>`
                html +=             $('#agent-options').html()      
                html +=         `</select>
                            </div>
                            <div class="col-4 col-sm-12 col-md-4">
                                <label for="agent_limit_amt[]" class="col-form-label font-weight-bold">Quantity</label>
                                <input type="number" name="agent_limit_amt[]" class="form-control" value="" placeholder="Insert Limit Quantity" required>
                            </div>
                            <div class="col-4 col-sm-12 col-md-4 d-flex align-items-center">
                                <button class="agent_remove btn btn-danger mt-1">Remove</button>
                            </div>
                        </div>`
                break;

            case 'roles':
                html += `<div class="col-12 col-sm-12 col-md-4">
                            <input type="hidden" class="product_limit_id col-12" name="product_limit_id" id="productLimitID">
                            <label for="product_limit_duration" class="col-form-label font-weight-bold">Duration</label>
                            <select name="product_limit_duration" class="form-control s-select2 pc" required>
                                <option value="" selected disabled >Please choose duration.</option>
                                <option value="yearly">Yearly</option>
                                <option value="monthly">Monthly</option>
                                <option value="weekly">Weekly</option>
                                <option value="daily">Daily</option>
                            </select>
                        </div>

                        <div class="col-12 col-sm-12 col-md-4">
                            <label for="product_limit_roles[]" class="col-form-label font-weight-bold">Apply To</label>
                            <select class="s-select2 form-control role" multiple="multiple" name="product_limit_roles[]" required>`
                html +=         $('#roles-options').html() 
                html +=     `</select>
                        </div>

                        <div class="col-4 col-sm-12 col-md-4">
                            <label for="purchase_limit_amount" class="col-form-label font-weight-bold">Purchase Limit Quantity</label>
                            <input type="number" name="purchase_limit_amount" class="form-control">
                        </div>`
            default:
                break;
        }
        return html
    }

    $(document).on('submit', 'form', function(evt){
        evt.preventDefault()
        agent_selection = $(this).find('select[name="agent_limit_id[]"]')
        var selectedValues = agent_selection.map(function() {
          return $(this).val();
        }).get();

        var uniqueValues = new Set(selectedValues);
        if (uniqueValues.size === selectedValues.length) {
            this.submit()
        } else {
            toastr.error('Dealer Duplicate.')
        }
        
    })
</script>
<script>
    $(document).ready(function(){
        <?php
        if (Session::has('successnotice')) {
            echo 'toastr.success("' . Session::pull('successnotice') . '");';
        }
        if (Session::has('errornotice')) {
            echo 'toastr.error("' . Session::pull('errornotice') . '");';
        }
        ?>
    })
</script>
@endpush
