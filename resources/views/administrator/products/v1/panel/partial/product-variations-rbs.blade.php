<div class="col-12">
    <div class="row p-0 col-md-6">
        <div class="col-12 col-md-auto m-0 py-2 form-group">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" id="allowRBS[{{ $attribute->id }}]" class="custom-control-input" name="rbs[allowRBS][{{ $attribute->id }}]"
                    {{ ($attribute && $attribute->allow_rbs == 1) ? 'checked' : '' }} value="1">
                <label class="custom-control-label" id="ee" for="allowRBS[{{ $attribute->id }}]"><b>Allow RBS <small class="text-danger">*</small></b></label>
            </div>
        </div>

        <div class="col-12 col-md-auto m-0 py-2 form-group">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" id="allowReminder[{{ $attribute->id }}]" class="custom-control-input"  name="rbs[allowReminder][{{ $attribute->id }}]"
                    {{ ($attribute && $attribute->allow_reminder == 1) ? 'checked' : '' }} value="1">
                <label class="custom-control-label" for="allowReminder[{{ $attribute->id }}]"><b>Allow Reminder <small class="text-danger">*</small></b></label>
            </div>
        </div>
    </div>


  <div class="row allowRBSChecked">
    @foreach ($controller::$rbs_options as $key => $rbs_option)

      <div class="col-12 col-md-2 text-md-left my-auto">
          <p>
              {{$rbs_option}}
          </p>
      </div>
      <div class="col-12 col-md-10 form-group">
          <select class="select2 form-control rbsOptions" id="{{$key}}Options{{ $attribute->id }}" data-attributes-id="{{ $attribute->id }}" name="rbs[{{$key}}options][{{ $attribute->id }}][]" multiple>
              @foreach($rbsChoices->where('option_type', $key); as $rbsChoice)
                  <option value="{{ $rbsChoice->rbs_data }}" {{ (($attribute->rbs_frequency) && in_array($rbsChoice->id,$attribute->rbs_frequency)) || ($attribute->rbs_times_send  && array_key_exists($rbsChoice->id, $attribute->rbs_times_send)) ? 'selected' : '' }}>{{ $rbsChoice->rbs_label }}</option>
              @endforeach
          </select>
      </div>
      @endforeach
      <div class="col-12 col-md-2 text-md-left my-auto">
        <p>
            Shipping Calculation
        </p>
    </div>

    <div class="col-12 col-md-10 form-group">
        <select name="" id="" class="select2 form-control">
          <option value="0" selected>All months</option>
          <option value="1" >1 month</option>
        </select>
    </div>
      <div class="col-12 col-md-12 my-auto">
        <p>
            Details <small class="text-danger">*</small>
        </p>
      </div>
      @foreach($rbsChoices->where('option_type', 'times'); as $key => $rbsChoice)
      <div class="col-12 col-md-12 my-2 row rbsDetails{{ $rbsChoice->rbs_data }}-{{ $attribute->id }}" >
        <div class="col-md-1 my-auto">{{ $rbsChoice->rbs_label }}</div>
          <div class="col-md-3">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="">Price per bottle (RM)</span>
                </div>
                <input type="text" class="form-control input-mask-price" name="rbs[rbsDetails][{{ $attribute->id }}][{{ $rbsChoice->id }}][price]"
                @if ($attribute->rbs_times_send  && array_key_exists($rbsChoice->id, $attribute->rbs_times_send)) value="{{ $attribute->rbs_times_send[$rbsChoice->id]['price'] }}" @endif>
              </div>
          </div>
        <div class="col-md-3 px-0">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text " id="">Minimum quantity</span>
                </div>
                <input type="number" class="form-control" name="rbs[rbsDetails][{{ $attribute->id }}][{{ $rbsChoice->id }}][qty]" @if ($attribute->rbs_times_send  && array_key_exists($rbsChoice->id, $attribute->rbs_times_send)) value="{{ $attribute->rbs_times_send[$rbsChoice->id]['qty'] }}" @endif>
            </div>
        </div>
        <div class="col-md-5">
          <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text " id="">Shipping Categories</span>
              </div>
              <select {{-- {{ $inactive ? 'disabled' : '' }} --}} class="select2 form-control productCategory"
                    id="{{$key}}shipping_category{{ $attribute->id }}" name="rbs[rbsDetails][{{ $attribute->id }}][{{ $rbsChoice->id }}][shipping_category]">
                    <option value="0">-</option>
                    @foreach($shippingCategory->where('cat_type', 'shipping'); as $shippingcategory)
                    <option value="{{ $shippingcategory->id }}"
                      @if ($attribute->rbs_times_send  && array_key_exists($rbsChoice->id, $attribute->rbs_times_send) && array_key_exists('shipping_category', $attribute->rbs_times_send[$rbsChoice->id] )) {{ $shippingcategory->id == $attribute->rbs_times_send[$rbsChoice->id]['shipping_category'] ? 'selected' : '' }} @endif
                        >Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}
                    </option>
                    @endforeach
                </select>
          </div>
      </div>
      </div>
      @endforeach
  </div>
</div>
