@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">Pick (Orders) -Stock Ready Only</h1>
        </div>
    </div>

    {{-- select bulk action  --}}
    <form id="bulkactionform" action={{route('bulkupdate.ordertracking.picker') }} method="POST">
        <div class="row mt-2 ml-2">
            <div class="col-md-auto px-1">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="action_ids" id="action_ids" value="">

        <div class="input-group mb-1">
            <select name="bulkaction" id="bulkaction" class="col-12 text-left custom-select">
                <option value="selection">Select action</option>
                <option value="generate_picking_list">Generate Picking List</option>
            </select>

            <div class="col-md-auto px-1">
                <input name='action_date' id="action_date" class="estimatedateonly form-control" type="text"
                placeholder="Select Date" autocomplete="off">
            </div>

            <div class="input-group-append">
                <input type="submit" class="btn btn-primary" value="Submit" id="btnid">
            </div>
        </div>
            </div>
        </div>
    </form>

    <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Orders) : <b>{{ $countTotal}}</b></span></p>

    <div class="row">
        <div class="col-auto ml-auto">
            {{ $customerOrders->links() }}
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-bordered text-center table-striped " id="sales-tracking" style="width:100%">
                    <thead style="background-color:	#E8E8E8;">
                        <tr>
                            <th scope="col"><input type="checkbox" class="largerCheckBox" id="checkAll"/>Action</th>
                            <th scope="col">DO No.</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Pending Days</th>
                            <th scope="col">Order Status</th>
                            <th scope="col">States</th>
                            <th scope="col">Pick Pack Status</th>
                            <th scope="col">Remark</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($customerOrders as $customerOrderA)
                        @foreach ($customerOrderA as $customerOrder)
                            @if ($customerOrder->est_shipping_date < now() && $customerOrder->shipping_date == 'Pending' && $customerOrder->collected_date == 'Pending' && $customerOrder->order_status != 1004)
                                <tr style="background-color:#ffb3b3">
                            @elseif (isset($customerOrder->tracking_number) && $customerOrder->order_status == 1001)
                                <tr style="background-color:#b3daff">
                            @elseif ($customerOrder->order_status == 1002 || $customerOrder->order_status == 1003)
                                <tr style="background-color:#DFEFCB">
                            @else
                                <tr>
                            @endif

                                <td>
                                    @if ($customerOrder->pick_pack_status == 0)
                                        <div class="checkout">
                                            <input type="checkbox" class="cb-element largerCheckBox"
                                            name="orderName[]"  id="{{ $customerOrder->id }}" value="{{ $customerOrder->delivery_order }}"/>
                                        </div>
                                    @endif


                                    @php $itemid = array(); foreach ($customerOrder->items  as $item) $itemid[] = $item->id; @endphp

                                    @if ($user->hasRole('administrator'))
                                        <form action="{{ route('administrator.ordertracking.detail')}}">
                                            <input type="hidden" name="manyID" value="{{ implode("," , $itemid )}}">
                                            <button class="btn btn-primary" type="submit" formtarget="_blank">Detail</button>
                                        </form>
                                    @endif
                                </td>

                                <td>
                                    {{$customerOrder->purchase->getFormattedNumber()}}
                                    <hr>
                                    <a target="_blank"
                                    href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->purchase->getFormattedNumber().(($customerOrder->purchase->invoice_version != 0) ? '/v'.$customerOrder->purchase->invoice_version.'/' : '/').'delivery-orders/'.$customerOrder->delivery_order.'.pdf')}}"
                                    >{{$customerOrder->delivery_order}}
                                    </a>
                                </td>

                                <td>
                                    {{$customerOrder->created_at->format('d/m/Y')}}
                                </td>

                                <td>
                                    {{$customerOrder->getPendingAttribute()}}
                                </td>

                                <td>
                                    @foreach ($statuses as $status)
                                        @if ($status->id == $customerOrder->order_status)
                                        {{ $status->name == 'Order Partial Refunded' ? 'Order Placed' : $status->name }}
                                        @endif
                                    @endforeach
                                    &nbsp;
                                    <br>
                                    @if ($customerOrder->order_status = 1010 && isset($customerOrder->outlet))
                                        <p class="text-capitalize text-secondary m-0">{{$customerOrder->outlet->outlet_name}}</p>
                                    @endif
                                </td>

                                <td>
                                    {{ $customerOrder->purchase->state->name }}
                                </td>

                                <td>
                                    {{showPickPackStatus($customerOrder->pick_pack_status)}}
                                </td>

                                <td>
                                    <input readonly name='admin_remarks' class="form-control" value="{{$customerOrder->admin_remarks}}" placeholder="Admin remark" >{{$customerOrder->admin_remarks}}</input>
                                </td>
                            </tr>
                            @endforeach
                        @endforeach

                    </tbody>
                </table>

                {{-- @if ($customerOrderNoStocks)
                    <br>
                    <hr>
                    <br>

                    <div class="row">
                        <div class="col-12 mb-3">
                            <h5 style="display:inline;">No stock Order - Total (<b>{{ $countTotalNoStock}}</b>)</h5>
                        </div>
                    </div>

                    <tbody>
                        @foreach ($customerOrderNoStocks as $customerOrderB)
                            @foreach ($customerOrderB as $customerOrderz)
                                        {{$loop->iteration}})
                                    <td>
                                        Invocice : {{$customerOrderz->purchase->getFormattedNumber()}}
                                        Delivery Order :
                                        <a target="_blank"
                                        href="{{ URL::asset('storage/documents/invoice/'.$customerOrderz->purchase->getFormattedNumber().(($customerOrderz->purchase->invoice_version != 0) ? '/v'.$customerOrderz->purchase->invoice_version.'/' : '/').'delivery-orders/'.$customerOrderz->delivery_order.'.pdf')}}"
                                        >{{$customerOrderz->delivery_order}}
                                        </a>
                                        ,
                                        Order Date : {{$customerOrderz->created_at->format('d/m/Y')}}
                                    </td>
                                    <br>

                                    {{$customerOrderz->admin_remarks}}
                                <hr>
                            @endforeach
                        @endforeach
                    </tbody>
                @endif --}}

            </div>
        </div>

        <div class="col-auto ml-auto py-3">
            {{ $customerOrders->links() }}
        </div>

    </div>

</div>

@push('style')
    <style>
        .text-bold {
            font-weight: bold;
        }

        .table .thead-light th {
            font-weight: bold;
            font-family: 'Nunito', sans-serif;
        }

        .largerCheckBox{
            width: 25px;
            height: 25px;
        }

        @media(max-width:767px) {
            .hidden-sm {
                display: none;
            }

        }

        @media(min-width:767px) {
            .hidden-md {
                display: none;
            }
        }


        .text-font-family {
            font-family: 'Nunito', sans-serif;
        }

        /* freeze header  */
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

            table  { border-collapse: collapse; width: 100%; }
            th     { background:#eee; }

    </style>
@endpush

@push('script')
    <script>
        $(function() {
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
                });

                $(".estimatedateonly").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                changeMonth: true,
                changeYear: true
                });

                $(".filterDate").datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                changeMonth: true,
                changeYear: true
            });
        });

        $(document).ready(function () {

            document.getElementById("action_date").style.display = "none";

            //check all checkbox
            $("#checkAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Get checked id
            $("#bulkactionform").submit(function(){
                var checked = new Array();
                var ids = new Array();

                checked = $('input[name="orderName[]"]:checked');
                for (i = 0; i < checked.length; i++) {
                    ids.push(checked[i].value);
                    }

                var stringids = ids.toString();
                $("#action_ids").val(stringids);
            });

            //show or hide when selected bulk action
            $("#bulkaction").click(function(){

                if(bulkaction.value == 'combine_do' || bulkaction.value == 'splitPOnDO' || bulkaction.value == 'selection'|| bulkaction.value == 'generate_picking_list'){

                document.getElementById("action_date").style.display = "none";
                }else if(bulkaction.value == 'mark_order_date'){

                    document.getElementById("action_date").className = "estimatedateonly form-control";

                    $(".estimatedateonly").datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else if(bulkaction.value == 'mark_actual_ship_date'){

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else{

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }
            });

        });
    </script>
@endpush

@endsection
