<?php

namespace App\Repository;

use DB;
use App\Models\Products\Product;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class CoreCache {

    CONST CACHE_KEY = 'CORE';

    public static function cacheTimeMin($min){
        return Carbon::now()->addMinutes($min);
    }

    public function getCacheKey($key) {
        $key = strtoupper($key);
        return self::CACHE_KEY . ".$key";
    }

    public function limit_category_product($limit_type,$limit_id) {

        $key = md5("cat".$limit_type.$limit_id);
        $cacheKey = $this->getCacheKey($key);

        return cache()->remember($cacheKey, self::cacheTimeMin(1), function() use($limit_type,$limit_id) {

            switch($limit_type) {
                case 'ex_category':
                    $result = DB::table('piv_category_product')
                            ->select('product_id as global_product_id','category_id')
                            ->whereNotIn('category_id',json_decode($limit_id))
                            ->get();
                    break;
                case 'in_category':
                    $result = DB::table('piv_category_product')
                            ->select('product_id as global_product_id','category_id')
                            ->whereIn('category_id',json_decode($limit_id))
                            ->get();
                    break;
                case 'ex_product':
                    $result = Product::select('id','global_product_id')
                            ->whereNotIn('global_product_id',json_decode($limit_id))
                            ->get();
                    break;
                case 'in_product':
                    $result = Product::select('id','global_product_id')
                            ->whereIn('global_product_id',json_decode($limit_id))
                            ->get();
                    break;
                default: //na
                    $result = DB::table('panel_products')
                            ->whereIn('id',json_decode($limit_id))
                            ->get(['id','global_product_id']);

            }

            if($result){
                return $result;
            }
        });
    }

    public function getProductId($panel_id) {

        $key = md5("global_product_id".$panel_id);
        $cacheKey = $this->getCacheKey($key);

        return cache()->remember($cacheKey, self::cacheTimeMin(1), function() use($panel_id) {

            $result = Product::where('id',$panel_id)
                    ->first(['id','global_product_id']);

            if($result) {
                return $result->global_product_id;
            }
        });
    }

    public function getProductCanGenerate($id) {

        $key = md5("global_product_can_generate".$id);
        $cacheKey = $this->getCacheKey($key);

        return cache()->remember($cacheKey, self::cacheTimeMin(1), function() use($id) {

            $result = DB::table('global_products as g')
                    ->join('panel_products as p','p.global_product_id','=','g.id')
                    ->whereIn('g.id',json_decode($id))
                    ->where('g.generate_discount',0)
                    ->get(['p.id','p.global_product_id','g.generate_discount']);

            if($result) {
                return $result;
            }
        });
    }

    public function getProductEligibleDiscount($id) {

        $key = md5("global_product_eligible_discount".$id);
        $cacheKey = $this->getCacheKey($key);

        return cache()->remember($cacheKey, self::cacheTimeMin(1), function() use($id) {

            $result = DB::table('global_products as g')
                    ->join('panel_products as p','p.global_product_id','=','g.id')
                    ->whereIn('g.id',json_decode($id))
                    ->where('g.eligible_discount',0)
                    ->get(['p.id','p.global_product_id','g.eligible_discount']);

            if($result) {
                return $result;
            }
        });
    }


}
