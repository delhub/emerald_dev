<?php

use App\Models\Users\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;

class UpdateAccountRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Role::firstOrCreate([
            'name' => 'account_manager',
            'guard_name' => 'web'
        ], [
            'created_at' => now(),
            'updated_at' => now()
        ]);

        User::whereIn('email', ['acc.fhcsb@gmail.com', 'formulahealthcare.acc@gmail.com', 'mags.jeannelim@gmail.com'])->get()->each(function ($user) {
            $user->assignRole('account_manager');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
