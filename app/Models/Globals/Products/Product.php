<?php

namespace App\Models\Globals\Products;

use App\Traits\ProductBrandLogs;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use ProductBrandLogs;
    // Set table
    protected $table = 'global_products';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'product_code',
        'name',
        'name_slug',
        'details',
        'description',
        'quality_id',
        'product_rating'
    ];

    // Casts
    protected $casts = [
        'product_rating' => 'double',
    ];

    /**
     * Get all images of a product.
     *
     * Example Create = $product->images()->create(['key' => 'value']);
     * Example Retrieve = $product->images;
     */


    public function images()
    {
        return $this->morphMany('App\Models\Globals\Image', 'imageable');
    }

    /**
     * Get default image of a product.
     */
    public function defaultImage()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable')
            ->where('default', 1);
    }


    /**
     * Get brand image of a product.
     */
    public function brandImage()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable')
            ->where('brand', 1);
    }

    /**
     *  Get product's categories.
     */
    public function categories()
    {
        return $this->belongsToMany(
            'App\Models\Categories\Category',
            'piv_category_product',
            'product_id',
            'category_id'
        );
    }

    public function accountingCat()
    {
        return $this->categories()->where('type','accounting');
    }
    /**
     * Get all attribute of a product.
     */
    public function attributes()
    {
        return $this->hasMany('App\Models\Globals\Products\ProductAttribute', 'product_id');
    }

    public function getSameBrand($brandId)
    {
        $sameBrand = self::where('brand_id', $brandId)->where('product_status',1)->get();

        return $sameBrand;
    }

    /**
     * Get all color attributes of a product.
     */
    public function colorAttributes()
    {
        return $this->hasMany('App\Models\Globals\Products\ProductAttribute', 'product_id')
            ->where('attribute_type', 'color');
    }

    /**
     * Get all size attributes of a product.
     */
    public function sizeAttributes()
    {
        return $this->hasMany('App\Models\Globals\Products\ProductAttribute', 'product_id')
            ->where('attribute_type', 'size');
    }

    /**
     * Get all miscellaneous attributes of a product.
     */
    public function miscellaneousAttributes()
    {
        return $this->hasMany('App\Models\Globals\Products\ProductAttribute', 'product_id')
            ->where('attribute_type', 'miscellaneous');
    }

    /**
     * Get all light temperature of a product.
     */
    public function lightTemperatureAttributes()
    {
        return $this->hasMany('App\Models\Globals\Products\ProductAttribute', 'product_id')
            ->where('attribute_type', 'light-temperature');
    }

    /**
     * Get all panel product of a product.
     */
    public function productSoldByPanels()
    {
        return $this->hasMany('App\Models\Products\Product', 'global_product_id');
    }

    /**
     * Get the quality of a product.
     */
    public function quality()
    {
        return $this->belongsTo('App\Models\Globals\Quality', 'quality_id');
    }

    /**
     * Get panel product.
     */
    public function panelProduct()
    {
        return $this->hasOne('App\Models\Products\Product', 'global_product_id');
    }

    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if ($selectSearch == 'name') {

                return $query
                    ->where('global_products.name', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'code') {

                return $query
                    ->where('global_products.product_code', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'category') {

                return $query
                    ->join('piv_category_product', 'piv_category_product.product_id', '=', 'global_products.id')
                    ->join('categories', 'categories.id', '=', 'piv_category_product.category_id')
                    ->where('categories.name', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }

    //Get global product URL attribute
    public function getUrlAttribute()
    {
        $categorySlug = $this->categories->where('parent_category_id', 0)->where('type', 'shop')->first();
        $slug = $categorySlug->slug ?? '';

        return "/shop/product/$slug/{$this->name_slug}?panel={$this->productSoldByPanels[0]->panel_account_id}";
    }

    public function scopeValidPrice($query)
    {
        return $query->whereDoesntHave('panelProduct.attributes.getPrice', function ($priceQuery) {
            $priceQuery->where('country_id', 'MY')
                ->where('panel_product_prices.active', 1)
                ->where('fixed_price', 0);
        });
    }
}
