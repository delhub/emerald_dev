<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!--Bootstrap style for table form -->

    <!-- Latest compiled and minified JavaScript -->

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}?v=2.4" rel="stylesheet">

    <style>
        .bg-black {
            background-color: #000000;
        }
        .navbar-toggler:hover,
        .navbar-toggler:focus {
            text-decoration: none;
            outline: none;
        }
        .apexcharts-legend.apexcharts-align-center.position-bottom{
            max-height: 220px!important;
        }
        .apexcharts-inner.apexcharts-graphical{
            background: rgb(255, 219, 12);
        }
        .apexcharts-legend-series{
            line-height: 20px!important;
        }
        .agent-db{
            min-height: 400px;
        }
        .navigation-icon {
            font-size: 1.8rem;
            margin-left: 18px;
            color: #fbcc34;
        }
        img {
            vertical-align: middle;
            border-style: none;
            margin-left: -7px;
        }
        element.style {
            max-height: 35px;
            margin-left: -6px;
        }
        @media only screen and (max-width: 414px){
            .app-header .navbar-brand {
                left: 60px;
                margin-left: 0px;
            }
        }
        @media only screen and (max-width: 320px){
            .navigation-icon {
                font-size: 1.8rem;
                margin-left: 20px;
                color: #fbcc34;
            }
            img {
                vertical-align: middle;
                border-style: none;
                margin-left: -4px;
                margin-right: 5px;
            }
            .d-inline-block.align-middle {
                margin-top: -2px;
                max-height: 35px;
                margin-left: -7px;
            }
            .app-header .nav-item .nav-link > .img-avatar, .app-header .nav-item .avatar.nav-link > img {
                height: 35px;
                margin: 0px -3px;
                border: 2px solid #ffcc00;
            }
            .fa.fa-caret-down{
                color: #ffcc00;
                margin-right: -6px;
                margin-left:1px;
            }
            .app-header .nav-item {
                position: relative;
                width: auto;
                margin: -5px;
                text-align: center;
                float: right;
            }
            .margin-logo-mobile {
                margin-left: -10px;
            }
 
        }
        @media only screen and (max-width: 280px){
            .navigation-icon {
                font-size: 1.8rem;
                margin-left: -4px;
                color: #fbcc34;
            }
            img {
                vertical-align: middle;
                border-style: none;
                margin-left: -7px;
                margin-right: -3px;
            }
            .d-inline-block.align-middle {
                margin-top: -2px;
                max-height: 35px;
                margin-left: -7px;
            }
            .app-header .nav-item .nav-link > .img-avatar, .app-header .nav-item .avatar.nav-link > img {
                height: 35px;
                margin: 0px -3px;
                border: 2px solid #ffcc00;
            }
            .fa.fa-caret-down{
                color: #ffcc00;
                margin-right: -6px;
                margin-left:1px;
            }
            .app-header .nav-item {
                position: relative;
                width: auto;
                margin: -5px;
                text-align: center;
                float: right;
            }
            .margin-logo-mobile {
                margin-left: -34px;
            }
        }
        @media only screen and (max-width: 240px){
            .navigation-icon {
                font-size: 1.8rem;
                margin-left: -4px;
                color: #fbcc34;
            }
            img {
                vertical-align: middle;
                border-style: none;
                margin-left: -8px;
                margin-right: -29px;
            }
            .d-inline-block.align-middle {
                margin-top: -2px;
                max-height: 35px;
                margin-left: -7px;
            }
            .app-header .nav-item .nav-link > .img-avatar, .app-header .nav-item .avatar.nav-link > img {
                height: 35px;
                margin: 0px -3px;
                border: 2px solid #ffcc00;
            }
            .fa.fa-caret-down{
                color: #ffcc00;
                margin-right: -6px;
                margin-left:1px;
            }
            .app-header .nav-item {
                position: relative;
                width: auto;
                margin: -16px;
                text-align: center;
                float: right;
            }
            .margin-logo-mobile {
                margin-left: -34px;
            }
            .nav-link{
                margin-left: 24px
            }
        }
        

    </style>

    @stack('style')

</head>

<body class="app header-fixed aside-menu-fixed sidebar-lg-show">
    @include('layouts.agent.navigations.topbar')
    <div class="">
        @include('layouts.agent.navigations.sidebar')
        <main class="main">
            @yield('breadcrumbs')
            <div class="animated fadeIn" style="padding-top: 20px;">
                <!-- Content Here -->
                @yield('content')
            </div>
        </main>
    </div>
    @include('layouts.shop.footer.footer')
    <!-- Main scripts for this application -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script type="text/javascript">
        function onPageLoadGetQuantity() {
            let getQuantityURL = "{{ route('web.shop.cart.quantity', ['id' => Auth::user()->id]) }}";
            const cartQuantityElement = $('#cart-quantity');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    cartQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        // function to get perfect list quantity
        function onPageLoadGetPerfectListQuantity() {
            let getQuantityURL = "{{ route('shop.perfect-list.quantity', ['id' => Auth::user()->id]) }}";
            const perfectListQuantityElement = $('#wishlist-quantity');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    perfectListQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        $(document).ready(function() {
            onPageLoadGetQuantity();
            onPageLoadGetPerfectListQuantity();
        });

        $(document).ready(function() {
            $("a").tooltip();
        });
    </script>
    @stack('script')
</body>

</html>