@extends('layouts.management.main-customer')

@section('content')

@section('page_title')
{{ "Welcome to My Dashboard" }}
@endsection

{{-- new layout --}}
<div class="fml-container">
            <div class="row">
              <div class="col-lg-4 dashboard-pt-b">
                <div class="checkout-box-s2 checkout-box-height">
                    <div class="dashboard-box">
                        <div class="dashboard-title">
                            <p>My Account QR Code</p>
                        </div>
                    </div>
                        <div class="body-inner" style="display: flex; align-items: center; justify-content: center;">
                            {!! QrCode::size(200)->generate($userInfo); !!}
                        </div>
                </div>
              </div>

              <div class="col-lg-4 dashboard-pt-b">
                  <div class="checkout-box-s2 checkout-box-height">
                      <div class="dashboard-box">
                          <div class="dashboard-title">
                              <p>Voucher Code</p>
                          </div>
                      </div>
                      <div class="my-voucher-code-body">
                          <div class="body-inner">
                              <div class="col-lg-4 col-4 voucher-icon">
                                  <img class="db-icon1" src="{{asset('/images/icons/dashboard-icon1.png')}}">
                              </div>
                              <div class="col-lg-8 col-8 voucher-text">
                                  <h1>{{$voucher}}</h1>
                                  <h6>Valid Voucher(s)</h6>
                              </div>
                          </div>

                      </div>
                      <div class="dashboard-card-bottom">
                          <a href="{{route('shop.voucher.list')}}" id="voucher-btn" class="btn voucher-btn">Redeem Vouchers</a>
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 dashboard-pt-b">
                  <div class="checkout-box-s3 checkout-box-height">
                      <div class="dashboard-box">
                          <div class="dashboard-title">
                              <div class="title-width">
                                  <p>Value Records</p>
                              </div>
                              <div class="icon-special2">
                                  <div class="icon-viewmore2">
                                      <a href="{{route('shop.customer.orders')}}" data-original-title="" title="" tabindex="0">
                                          <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                      </a>
                                  </div>
                              </div>

                          </div>
                      </div>
                    @if(!$purchases->isEmpty())
                    {{-- @foreach ($purchases as $purchase)
                    @foreach ($purchase->orders as $order)
                    @foreach ($order->items as $item)
                      <div class="my-value-record-body">
                          <div class="myv-img">
                              <img class="db-img1"
                              src="{{asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename)}}"
                              >
                          </div>
                          <h4>
                            {{ $item->product->parentProduct->name}}
                          </h4>



                      </div>
                      <div class="dashboard-card-bottom">
                          <h5>
                            @if ($order->order_status === 1000)
                                Record Created
                            @elseif ($order->order_status === 1001)
                                Order Placed
                            @elseif($order->order_status === 1002)
                                Order Shipped
                            @elseif($order->order_status === 1003)
                                Order Delivered
                            @endif
                          </h5>
                      </div>
                      @endforeach
                      @endforeach
                      @endforeach --}}

                      @php
                        $order = $purchases[0]->orders[0];
                        $item = $purchases[0]->orders[0]->items[0];
                      @endphp

                      <div class="my-value-record-body">
                        @if (isset($item->product->parentProduct) && $item->product->parentProduct != NULL && isset($item->product->parentProduct->images[0]))
                            <div class="myv-img">
                                <img class="db-img1"
                                src="{{asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename)}}"
                                >
                            </div>
                            <h4>
                            {{ $item->product->parentProduct->name}}
                            </h4>
                        @endif

                    </div>
                    <div class="dashboard-card-bottom">
                        <h5>
                          @if ($order->order_status === 1000)
                              Record Created
                          @elseif ($order->order_status === 1001)
                              Order Placed
                          @elseif($order->order_status === 1002)
                              Order Shipped
                          @elseif($order->order_status === 1003)
                              Order Delivered
                          @endif
                        </h5>
                    </div>
                      @else
                      <div class="row">
                        <div class="col-12 text-center">
                          <strong class="" style="font-size: 11pt;">There are no orders found.</strong>
                        </div>
                        <div class="col-12 text-center db-mt-btn">
                          <a class="btn btn-vr-blue  padding-sm  button-size-laptop" href="/shop">Continue Shopping</a>
                        </div>
                      </div>
                      @endif
                  </div>
              </div>
              <div class="col-lg-4 dashboard-pt-b">
                  <div class="checkout-box-s3 checkout-box-height">
                      <div class="dashboard-box">
                          <div class="dashboard-title">
                              <div class="title-width">
                                  <p>Perfect List</p>
                              </div>
                              <div class="icon-special2">
                                  <div class="icon-viewmore2">
                                      <a href="{{route('shop.wishlist.home')}}"data-original-title="" title="" tabindex="0">
                                          <i aria-hidden="true" class="fa fa-arrow-circle-o-right icon-special-2"></i>
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                    @if(count($favourite))
                    @foreach ($favourite as $item)
                      <div class="my-perfect-list-body">
                        @if (isset($item->product->parentProduct) && $item->product->parentProduct != NULL)
                          <div class="myp-img">
                              <img class="db-img2" src="{{asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename)}}">
                          </div>
                          <h4>
                            {{ $item->product->parentProduct->name}}
                          </h4>
                          @endif
                      </div>
                      <div class="dashboard-card-bottom">
                          <a href="{{route('shop.wishlist.home')}}" id="cart-btn" class="btn voucher-btn">Add to Cart</a>
                      </div>
                    @endforeach
                      @else
                      <div class="row">
                        <div class="col-12 text-center">
                          <h5>No Perfect List</h5>
                        </div>
                      </div>
                      @endif
                  </div>
              </div>
    </div>
</div>



{{-- new layout end --}}

{{----------Desktop Layout original---------------------}}
{{-- <div class="container-fluid p-0">
  <div class="row">
    <div class="col-lg-5 col-md-12 col-12 order-12 order-md-12 order-lg-1">
      @if ($country == 'MY')
      <div class="card card-effect-bg">
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <h4 class="card-title  font-size" style="font-weight:700; ">Voucher Code</h4>
            </div>
            <div class="col-6">
              <a href="{{route('shop.voucher.list')}}">
                <p class="card-title " style="float: right; color:#ffcc00;">Show More</p>
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="overflow: auto;">
              <div class="mx-5 my-3">
                <button type="button" class="btn grad2 bjsh-btn-gradient btn-small-screen align-middle" onclick="window.location.href = '{{route('shop.voucher.list')}}';"><strong>{{$voucher}}</strong></button>
                <span class="ml-2">Valid Voucher(s)</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="card card-effect-bg">
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <h4 class="card-title  font-size" style="font-weight:700; ">Value Records</h4>
            </div>
            <div class="col-6">
              <a href="{{route('shop.customer.orders')}}">
                <p class="card-title " style="float: right; color:#ffcc00;">Show More</p>
              </a>
            </div>
          </div>
          @if(!$purchases->isEmpty())
          @foreach ($purchases as $purchase)
          @foreach ($purchase->orders as $order)
          @foreach ($order->items as $item)
          <div class="row my-2">
            <div class="col-md-3 col-4">
              <img class="my100img w-100"
                src="{{asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename)}}"
                alt="product-image">
            </div>
            <div class="col-md-4 col-5 my-auto">
              <p><b>{{ $item->product->parentProduct->name}}</b></p>
            </div>
            <div class="col-md-4 col-3 my-auto text-right">
              <p class="" style="float: right; font-weight:700;">
                @if ($order->order_status === 1000)
                Record Created
                @elseif ($order->order_status === 1001)
                Order Placed
                @elseif($order->order_status === 1002)
                Order Shipped
                @else
                Order Delivered
                @endif
              </p>
            </div>
          </div>
          @endforeach
          @endforeach
          @endforeach
          @else
          <div class="row">
            <div class="col-12 text-center">
              <strong class="" style="font-size: 11pt;">There are no orders found.</strong>
            </div>
            <div class="col-12 text-center">
              <a class="btn bjsh-btn-gradient  padding-sm  button-size-laptop" href="/shop">Continue Shopping</a>
            </div>
          </div>
          @endif
        </div>
      </div>
      <div class="card card-effect-bg">
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <h4 class="card-title  font-size" style="font-weight:700; ">Perfect List</h4>
            </div>
            <div class="col-6 ">
              <a href="{{route('shop.wishlist.home')}}">
                <p class="card-title " style="float: right; color:#ffcc00;">Show More</p>
              </a>
            </div>
          </div>
          @if(count($favourite))
          @foreach ($favourite as $item)
          <div class="row my-2">
            <div class="col-md-3 col-3">
              <img class="my100img w-100"
                src="{{asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename)}}"
                alt="product-image">
            </div>
            <div class="col-md-4 col-5 my-auto">
              <p><b>{{ $item->product->parentProduct->name}}</b></p>
            </div>
            <div class="col-md-4 col-4 my-auto text-right">
              <p class="" style="float: right; font-weight:700;">
                @if ($item->order_status === 1000)
                Record Created
                @elseif ($item->order_status === 1001)
                Order Placed
                @elseif($item->order_status === 1002)
                Order Shipped
                @else
                Order Delivered
                @endif
              </p>
            </div>
          </div>
          @endforeach
          @else
          <div class="row mt-2">
            <div class="col-12">
              <h5>No Perfect List</h5>
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="col-lg-7 col-md-12 col-12 order-1 order-md-1 mb-3 order-lg-12">
      <img class="web-image img-fluid my-1" src="{{ asset('assets/images/banners/customer-dashboard/my_Dash_Welcome_Banner.jpg') }}" alt="Welcome-Banner">
      <img class="mobile-image img-fluid my-1 " src="{{ asset('assets/images/banners/customer-dashboard/my_Dash_Welcome_Banner_M.jpg') }}" alt="Welcome-Banner">
      <img class="img-fluid my-1" src="{{ asset('assets/images/banners/customer-dashboard/my_Dash_Banner_M.jpg') }}" alt="Ramadan-Banner">
    </div>
  </div>

</div> --}}

{{-- <style>
  .card-effect-bg{
    border-radius: 10px;
    box-shadow: #d8d8d8 0 0 8px 0px;
  }
  .mobile-image {
    display: none;
  }

  .border-radius-card {
    border-radius: 15px;

  }

  .sub-border-color {
    border: 2pt solid #fbcc34;
  }

  @media(max-width:425px) {
    .mobile-image {
      display: block;
    }

    .web-image {
      display: none;
    }
  }

  @media(min-width:767px) {
    .hidden-md {
      display: none;
    }

    .font-size {
      font-size: 15pt;
    }
  }

  /*Laptop screen*/
  @media (max-width: 1800px) and (min-width: 1200px) {
    .font-size {
      font-size: 13pt;
    }

    .button-size-laptop {
      font-size: 7pt;
    }

  }
</style> --}}
{{----------Desktop Layout original end---------------------}}
@endsection
