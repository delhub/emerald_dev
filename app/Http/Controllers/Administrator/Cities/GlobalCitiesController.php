<?php

namespace App\Http\Controllers\Administrator\Cities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\City;
use App\Models\Globals\State;
use Illuminate\Support\Facades\Validator;
use DB;

class GlobalCitiesController extends Controller
{
    public function index(Request $request)
    {
        // $cities = City::all();

      $cities = City::with('state')
            ->orderBy('global_cities.id')
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->paginate(10);

        $cities->appends(request()->query());


        return view('administrator.cities.index')
            ->with('cities', $cities)
            ->with('request', $request);
    }

    public function create()
    {
        // $newCities = new Cities();
        // // $newCities->save();
        // return view('administrator.cities.create')->with('newCities', $newCities);

        $cities = City::all();

        // $states = State::all();

        $states = DB::table('global_states')
            ->where('id', '<=', 16)
            ->get();

        return view('administrator.cities.create', compact(['cities', 'states']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        // return $request;

        $this->validate($request, [
            // 'id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_key' => 'required|unique:global_cities|max:50',
            'city_name' => 'required|unique:global_cities|max:255',
            'city_data' => 'required|unique:global_cities|max:255'
        ]);

        $city = new City;

        // $city->id = $request->input('id');
        $city->country_id = $request->input('country_id');
        $city->state_id = $request->input('state_id');
        $city->city_key = $request->input('city_key');
        $city->city_name = $request->input('city_name');
        $city->city_data = $request->input('city_data');

        $city->save();

        return redirect()->route('administrator.cities.index')->with('success', 'Submit Done.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function edit(Request $request, $id)
    public function edit($id)
    {
        // $cities = City::findOrFail($id);
        // return view('administrator.cities.edit')->with('cities', $cities);
        // // return view('administrator.cities.edit' , compact('cities'));

         $city = City::find($id);

         $states = State::where('id','<','17')->get();
        //  return $states->where('id',$city->state_id)->first();

    //    return $states = DB::table('global_states')
    //         ->where('id', '<=', '16')
    //         ->get();

        return view('administrator.cities.edit')
            ->with('city', $city)
            ->with('states', $states);
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        // $cities = new Cities;
        // $cities->id = $request->input('id');
        // $cities->country_id = $request->input('country_id');
        // $cities->city_id = $request->input('city_id');
        // $cities->name = $request->input('name');
        // $cities->data = $request->input('data');

        // $cities->save();

        // return view('administrator.cities.index')->with('success', 'submit done');

        $this->validate($request, [
            // 'id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_key' => 'required',
            'city_name' => 'required',
            'city_data' => 'required'
        ]);

        $city = City::find($id);

        // $city->id = $request->input('id');
        $city->country_id = $request->input('country_id');
        $city->state_id = $request->input('state_id');
        $city->city_key = $request->input('city_key');
        $city->city_name = $request->input('city_name');
        $city->city_data = $request->input('city_data');

        $city->save();

        return redirect()->route('administrator.cities.index')->with('success', 'Cities Updates.');
    }
}
