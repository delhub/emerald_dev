<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href='https://fonts.googleapis.com/css?family=Poppins&display=swap' rel='stylesheet'>
</head>
<body>
    <style>
        .inner-body {
            width: 625px !important;
        }

        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
        .header {
            background: #263A8B;
            background-size: contain;
            border-radius: 10px 10px 0px 0px;
            padding: 15px 0 !important;
        }
        .body {
            background: white;
            border-top: 1px solid #ffdd9c;
            border-bottom: 1px solid #f8fafc;
        }
        .content {
            width: 50% !important;
        }
        .content-cell {
            padding: 25px 35px !important;
        }
        .content-cell p/* ,
        .content-cell h1 */,
        .content-cell span {
            font-family: 'Poppins';
            font-weight: 500;
            color: black;
        }

        .content-cell h1 {
            color: black;
        }
        .content-cell a {
            padding: 10px 20px;
            background: #E62A2C;
            background-size: contain;
            text-decoration: none;
            border: 1px solid #e91919;
            border-radius: 10px;
            color: white;
            font-weight: bold;
        }
        .content-cell table {
            border-top: none;
        }

        .revert {
            padding: revert !important;
            background: revert !important;
            font-weight: revert !important;
            color: revert !important;
            border: revert !important;
            text-decoration: revert !important;
        }

        .red {
            color: #EF3842 !important;
        }

        .content-cell img {
            width: 120px;
            height: 120px;
            object-fit: contain;
            background: #F1F1F1;
            border-radius: 5px;
        }

        .blue {
            color: #263A8B !important;
        }

        .row {
            --bs-gutter-x: 1.5rem;
            --bs-gutter-y: 0;
            display: flex;
            flex-wrap: wrap;
            margin-top: calc(-1 * var(--bs-gutter-y));
            margin-right: calc(-.5 * var(--bs-gutter-x));
            margin-left: calc(-.5 * var(--bs-gutter-x));
        }

        .px-4 {
            padding-right: 1.5rem!important;
            padding-left: 1.5rem!important;
        }

        .py-3 {
            padding-top: 1rem!important;
            padding-bottom: 1rem!important;
        }

        .col {
            flex: 1 0 0%;
        }

        .col-1 {
            flex: 0 0 auto;
            width: 8.33333333%;
        }
    </style>

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
            <td align="center">
                <table class="content" width="50%" cellpadding="0" cellspacing="0" role="presentation">
                    <tr>
                        <td style="background-color: #f8fafc" align="center">
                            <a href="{{ url('/') }}">
                                <img src="{{ url('/images/logo/formula-logo.png') }}" style="width: 10em;padding: 1em 0;">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="header">
                            <table align="center" width="570" style="padding: 0 35px">
                                <tr>
                                    <td>
                                        <p style="margin-bottom: 0;color: white;font-family:'Poppins'; font-style: normal;font-weight: 700;text-align: center;font-size: 25px;">
                                            {{-- @if(Str::contains($title, 'verify')) Verify Your Email @else Password Reset @endif --}}
                                            Welcome to Formula2u
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                    

                    <!-- Email Body -->
                    <tr>
                        <td class="body" width="100%" cellpadding="0" cellspacing="0" style="background-color: #white;">
                            <table class="inner-body" align="center" width="625" cellpadding="0" cellspacing="0" role="presentation" style="background-color: #white;">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                        <p>Dear {{ $directPay->user->userInfo->full_name }},</p>
                                        <p>
                                            You hereby received an email to pay for the product that you’ve book for the Recurring Booking System:
                                        </p>
                                        <hr>
                                        @foreach ($products as $product)
                                            <div class="row py-3">
                                                <img class="col-1" src="{{ ($product->defaultImage) ? asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) : asset('assets/images/errors/image-not-found.png') }}" alt="{{ $product->name }}">
                                                <div class="col px-4 py-3">
                                                    <span class="blue" style="font-weight: 700">{{ $product->name }}</span><br>
                                                    <span>{{ $product->productInformation }}</span><br>
                                                    @if ($product->deliveryFrequency)
                                                        <span style="color:red;">* </span><span>{{ $product->deliveryFrequency }}</span><br>
                                                    @endif
                                                    <span class="red">MYR {{ number_format($product->subTotalPrice / 100, 2) }}</span>
                                                </div>
                                            </div>
                                        @endforeach
                                        <p>Please click on the following link to start the payment process:</p>
                                        <table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                            <tr>
                                                <td align="center">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="margin: auto;">
                                                                    <tr>
                                                                        <td>
                                                                            <a href="{{ $webLink }}" class="button button-{{ $color ?? 'primary' }}" target="_blank">Pay Now</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr style="margin: 1.5em 0">
                                        <p>
                                            We are happy to serve you. If you have any question, please do not hesitate to contact our support team
                                            <br>
                                            <a class="revert" href="mailto:formula2u-cs@delhubdigital.com">formula2u-cs@delhubdigital.com</a>
                                        </p>
                                        <p style="margin-bottom: 0">Regards,<br><b>Formula Healthcare Management</b></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                <tr>
                                    <td class="content-cell" align="center">
                                        <p>© {{ now()->format('Y') .' '. config('app.name') }}. All rights reserved.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                    
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
