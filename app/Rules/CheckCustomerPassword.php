<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\UserInfo;
use Illuminate\Support\Facades\Hash;
class  CheckCustomerPassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $customer =   $this->user;

        if (!isset($customer)) return true;

        $hasher = app('hash');

        return Hash::check($value, $customer->password) ?  true : false ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please enter a correct password.';
    }
}
