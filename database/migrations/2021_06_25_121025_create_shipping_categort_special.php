<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Support\Facades\DB;

class CreateShippingCategortSpecial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //west malaysia
        $shipping_category = new Ship_category;
        $shipping_category->cat_name = 'West Malaysia <RM100 (RM5 shipping fee),RM101 - RM299(RM10 shipping fee), >RM100(Free shipping fee), Sabah AND Sarawak >RM499(Free shipping fee)';
        $shipping_category->cat_desc = 'West Malaysia <RM100 (RM5 shipping fee),RM101 - RM299(RM10 shipping fee), >RM100(Free shipping fee), Sabah AND Sarawak >RM499(Free shipping fee)';
        $shipping_category->cat_type = 'shipping_special';

        $shipping_category->active = '1';
        $shipping_category->for_user = '["6000"]';
        $shipping_category->start_date = '2021-06-26 00:00:00';
        $shipping_category->end_date = '2021-06-28 00:00:00';
        $shipping_category->save();

        DB::table('shipping_rates')->insert(
            array(
                array('zone_id' => '1', 'category_id' => $shipping_category->id, 'price' => '500', 'condition' => 'price', 'min_rates' => '0', 'max_rates' => '100', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '1', 'category_id' => $shipping_category->id, 'price' => '1000', 'condition' => 'price', 'min_rates' => '100.01', 'max_rates' => '299', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '1', 'category_id' => $shipping_category->id, 'price' => '0', 'condition' => 'price', 'min_rates' => '299.01', 'max_rates' => '299.01', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '2', 'category_id' => $shipping_category->id, 'price' => '2000', 'condition' => 'price', 'min_rates' => '0', 'max_rates' => '499.00', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),
                array('zone_id' => '2', 'category_id' => $shipping_category->id, 'price' => '0', 'condition' => 'price', 'min_rates' => '499.01', 'max_rates' => '499.01', 'calculation_type' => 'per-shipping-category', 'set_status' => '0', 'active' => 'Y'),

            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_categort_special');
    }
}
