@extends('layouts.management.main-customer')



@section('content')

@section('page_title')
{{ "Welcome to My Profile" }}
@endsection

{{------------------Desktop Layout---------------------}}

<div class="container-fluid " style="min-height: 100vh; ">
    <div class="row">
        <div><h4>Coming Soon</h4></div>
    </div>
    <div class="row align-items-end" style="display: none">
        <div class="col-6">
            <p class="headerText myfont_weight700">Perfect List</p>
        </div>
        <div class="col-12 mywebmenu">
            <a href="#" class="my_order_text active"><strong>My Favourite
                </strong></a>
            <a href="#" class="my_order_text"><strong>My Home List</strong></a>
            <a href="#" class="my_order_text"><strong>Perfect Home List</strong></a>
        </div>
        <div class="col-6 mymobilemenu">
            <a class="btn btn-secondary  bjsh-btn-gradient my-1" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                style="background-color:rgb(250, 172, 24); color:black;">
                My Favourite <i style="font-size: 10px;" class="fa fa-arrow-down"> </i>
        </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">My Favourite</a>
                <a class="dropdown-item" href="#">My Home List</a>
                <a class="dropdown-item" href="#">Perfect Home List</a>
            </div>
        </div>
    </div>




    {{--
        <!-- <div class="row ">
    <div class="col-2">
        <a href="#" class="my_order_text active">
        <strong >My Favourite</strong>
        </a>
    </div>

    <div class="col-2">
        <a href="#" class="my_order_text"><strong>My Home List</strong>
        </a>
    </div>
    <div class="col-3">
        <a href="#" class="my_order_text"><strong>Perfect Home List</strong>
        </a>

    </div>
</div>  -->
--}}





    {{--Template for wish list--}}



    <div class="card shadow-sm"  style="display: none">
        <div class="card-body">

            {{-- <h4 style="font-weight:bold; color:rgb(250, 172, 24);">Purchase #:
        </h4>
        --}}
            <table class="table">
                <!-- Starting Item Template -->
                @if(count($favourite) > 0)
                @foreach ($favourite as $item)
                <tr>
                    <td class="align-top" style="max-width: 400px; border-top:white;">
                        <div class="row">
                            <div class="col-md-2">
                                <a
                                    href="/shop/product/{{ $item->product->parentProduct->name_slug}}?panel={{$item->product->panel_account_id}}">
                                    <img class="responsive-img p-1"
                                        src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . $item->product->parentProduct->images[0]->filename)}}"
                                        alt="Product Image">
                                </a>

                            </div>
                            <div class="col-md-6 my-auto wishlistbox">


                                <a style="color:black; font-weight:bold;"
                                    href="/shop/product/{{ $item->product->parentProduct->name_slug}}?panel={{$item->product->panel_account_id}}">
                                    <h4>{{ $item->product->parentProduct->name}}</h4>
                                </a>

                                <p class="text-capitalize">Sold by: {{$item->product->panel->company_name}}
                                </p>
                                <p class="text-capitalize">Unit Price:
                                    <?php echo 'RM ' . number_format(($item->product->price / 100), 2); ?>
                                </p>
                                {{-- <button class="text-capitalize bjsh-btn-gradient"><a
                                    style="color:black; text-decoration:none;"
                                    href="/shop/product/">
                                    Buy It Again</a></button> --}}

                                <p class="mypbotmg30px">Added on: {{$item->created_at}}</p>
                            </div>

                            <div class="offset-2 mt-4">
                                {{-- Quantity: xx --}}
                            </div>

                            <div class="col-md-2 padding-button">
                                <button
                                    class="btn btn-md bjsh-btn-product-page font-weight-bold w-100 bjsh-button-mobile"><a
                                        style="color:black; text-decoration:none;"
                                        href="/shop/product/{{ $item->product->parentProduct->name_slug}}?panel={{$item->product->panel_account_id}}">
                                        Buy Now</a>
                                </button>


                                <form id="add-to-cart-form" style="display: inline;" method="POST"
                                    action="{{ route('shop.cart.add-item') }}">
                                    @method('POST')
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{ $item->product_id }}">
                                    <input type="hidden" id="product_attribute_color" name="product_attribute_color"
                                        value="">
                                    <input type="hidden" id="product_attribute_size" name="product_attribute_size"
                                        value="">
                                    <input type="hidden" id="product_attribute_temperature"
                                        name="product_attribute_temperature" value="">
                                    <input type="hidden" name="productQuantity" value="1">
                                    <button type="submit"
                                        class="btn btn-md bjsh-btn-product-page font-weight-bold w-100 bjsh-button-mobile mt-4"
                                        style="color:black; text-decoration:none;">
                                        Add to Cart
                                    </button>
                                </form>

                                <div class="col-12 mycentertext">
                                    <a href="{{route('shop.perfect-list.destroy',[$item->product->id])}}"
                                        onclick="event.preventDefault(); document.getElementById('submit-form-{{ $loop->index }}').submit();">
                                        <i class="fa fa-trash-o fa-2x text-muted mt-4"></i></a>
                                    <form id="submit-form-{{ $loop->index }}"
                                        action="{{ route('shop.perfect-list.destroy',[ $item->product->id]) }}"
                                        method="POST" class="hidden">
                                        @csrf
                                        @method('DELETE')

                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </td>
                </tr>

                @endforeach

                <!-- Ending Item Template -->
                @else
                <div class="row">
                    <div class="col-12">
                        <strong class="mr-2 " style="font-size:15pt;"> Your Perfect List is empty.</strong>
                        <a class="btn bjsh-btn-gradient" href="/shop">Continue Shopping</a>
                    </div>
                </div>
                @endif
            </table>

        </div>
    </div>

</div>

{{--Ending template--}}



<style>
    @media(min-width:1320px) and (max-width:1500px) {
        .padding-left-md {
            padding-left: 6rem !important;
        }

        .padding-left-icon {
            padding-left: 3rem !important;
        }
    }

    .text-style {
        color: rgb(250, 172, 24);
        margin-right: 45px;

    }


    /* .header-text-style {
        color: rgb(250, 172, 24);
    } */


    .btn-size {
        width: 100%;
    }


    .fa.fa-trash-o:before {
        color: #fbcc34;


    }

    @media(max-width:767px) {
        .hidden-sm {
            display: none;
        }

    }

    @media(min-width:767px) {
        .hidden-md {
            display: none;
        }

    }

    @media(min-width: 760px) and (max-width:1200px) {
        .padding-left-md {
            padding-left: 8rem !important;
        }

        .padding-button {
            padding: 0px;

        }

        .padding-left-icon {
            padding-left: 1rem;
        }
    }

    @media(min-width:1500px) {
        .padding-left-icon {
            padding-left: 4rem !important;
        }
    }
</style>

@push('script')
<script>

    // function onPageload() {


    //             inputColor = $('#product_attribute_color');
    //             inputSize = $('#product_attribute_size');
    //             inputTemperature = $('#product_attribute_temperature');

    //             priceTag = $('#price_tag');
    //             memberPriceTag = $('#member_price_tag');

    //             if ($('input[name="color"]:checked').val()) {
    //                 panelColor = $('input[name="color"]:checked').val();
    //             } else {
    //                 panelColor = null;
    //             }

    //             if ($('input[name="size"]:checked').val()) {
    //                 panelSize = $('input[name="size"]:checked').val();
    //             } else {
    //                 panelSize = null;
    //             }

    //             if ($('input[name="temperature"]:checked').val()) {
    //                 panelTemperature = $('input[name="temperature"]:checked').val();
    //             } else {
    //                 panelTemperature = null;
    //             }

    //             let priceByAttr;
    //             let mmbrPriceByAttr;

    //             if (
    //                 $('input[name="color"]:checked').data('price')) {
    //                 priceByAttr = $('input[name="color"]:checked').data('price');
    //             } else if ($('input[name="size"]:checked').data('price')) {
    //                 priceByAttr = $('input[name="size"]:checked').data('price');
    //             } else if ($('input[name="temperature"]:checked').data('price')) {
    //                 priceByAttr = $('input[name="temperature"]:checked').data('price')
    //             } else {
    //                 priceByAttr = 0;
    //             }

    //             if ($('input[name="color"]:checked').data('member-price')) {
    //                 mmbrPriceByAttr = $('input[name="color"]:checked').data('member-price');
    //             } else if ($('input[name="size"]:checked').data('price')) {
    //                 mmbrPriceByAttr = $('input[name="size"]:checked').data('member-price');
    //             } else if ($('input[name="temperature"]:checked').data('member-price')) {
    //                 mmbrPriceByAttr = $('input[name="temperature"]:checked').data('member-price')
    //             } else {
    //                 mmbrPriceByAttr = 0;
    //             }

    //             inputColor.val(panelColor);
    //             inputSize.val(panelSize);
    //             inputTemperature.val(panelTemperature);


    //             if (priceByAttr != 0) {
    //                 priceTag.text('RM ' + priceByAttr);
    //             }

    //             if (mmbrPriceByAttr != 0) {
    //                 memberPriceTag.text('RM ' + mmbrPriceByAttr);
    //             }
    //         }

    //         $('.panel-product-attributes').on('click', function(e) {
    //             inputColor = $('#product_attribute_color');
    //             inputSize = $('#product_attribute_size');
    //             inputTemperature = $('#product_attribute_temperature');


    //             if ($('input[name="color"]:checked').val()) {
    //                 panelColor = $('input[name="color"]:checked').val();
    //             } else {
    //                 panelColor = null;
    //             }

    //             if ($('input[name="size"]:checked').val()) {
    //                 panelSize = $('input[name="size"]:checked').val();
    //             } else {
    //                 panelSize = null;
    //             }

    //             if ($('input[name="temperature"]:checked').val()) {
    //                 panelTemperature = $('input[name="temperature"]:checked').val();
    //             } else {
    //                 panelTemperature = null;
    //             }

    //             if (
    //                 $('input[name="color"]:checked').data('price')) {
    //                 priceByAttr = $('input[name="color"]:checked').data('price');
    //             } else if ($('input[name="size"]:checked').data('price')) {
    //                 priceByAttr = $('input[name="size"]:checked').data('price');
    //             } else if ($('input[name="temperature"]:checked').data('price')) {
    //                 priceByAttr = $('input[name="temperature"]:checked').data('price')
    //             } else {
    //                 priceByAttr = 0;
    //             }

    //             if ($('input[name="color"]:checked').data('member-price')) {
    //                 mmbrPriceByAttr = $('input[name="color"]:checked').data('member-price');
    //             } else if ($('input[name="size"]:checked').data('price')) {
    //                 mmbrPriceByAttr = $('input[name="size"]:checked').data('member-price');
    //             } else if ($('input[name="temperature"]:checked').data('member-price')) {
    //                 mmbrPriceByAttr = $('input[name="temperature"]:checked').data('member-price')
    //             } else {
    //                 mmbrPriceByAttr = 0;
    //             }

    //             inputColor.val(panelColor);
    //             inputSize.val(panelSize);
    //             inputTemperature.val(panelTemperature);


    //             if (priceByAttr != 0) {
    //                 priceTag.text('RM ' + priceByAttr);
    //             }

    //             if (mmbrPriceByAttr != 0) {
    //                 memberPriceTag.text('RM ' + mmbrPriceByAttr);
    //             }
    //         });

    //         onPageload();
</script>
@endpush
@endsection