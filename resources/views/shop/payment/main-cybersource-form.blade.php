<div class="form-row my-pt-1 red-ball-bg">
  <div class="form-group col-md-6">
      <label for="cart-num">Card Number<span>*<span></label>
      <input type="text" class="form-control" id="card_number"
          placeholder="Card Number" name="card_number">
      {{-- @error('card_number')
              <small class="form-text text-danger">{{ $message }}</small>
          @enderror --}}
  </div>
  <div class="form-group col-md-6">
      <label for="cart-name">Card Holder
          Name<span>*<span></label>
      <input type="text" class="form-control" id="cart-name"
          placeholder="Card Holder Name" name="card_holder">
      {{-- @error('cart-name')
          <small class="form-text text-danger">{{ $message }}</small>
      @enderror --}}
  </div>
</div>
<div class="form-row my-pt-1">
  <div class="form-group col-md-6">
      <label for="card-type">Card Type<span>*<span></label>
      <select id="card-type" class="form-control"
          name="card_type">
          <option selected disabled value="0">Choose...
          </option>
          @foreach ($cartsItems['issuer'] as $issuer)
              <option value="{{ $issuer->issuer_name }}">
                  {{ ucfirst($issuer->issuer_name) }}
              </option>
          @endforeach
      </select>
  </div>
  <div class="form-group col-md-4">
      <label for="cart-date">Card Expiration
          Date<span>*<span></label>
      <input type="text" class="form-control" id="cart-date"
          placeholder="MMYY" maxlength="4" name="card_expiry">
  </div>
  <div class="form-group col-md-2">
      <label for="cart-cvv">CVV</label>
      <input type="text" class="form-control" id="cart-cvv"
          placeholder="000" maxlength="3" name="card_cvv">
  </div>

  {{-- <div class="form-check checkbox-style form-submit-list padding-zero my-mt-1">
      <input class="form-check-input radio" type="radio" name="ccRadio" id="cc-radio" >
      <label class="form-check-label"></label>
      <span>Save credit card to use later<span>
  </div> --}}
</div>