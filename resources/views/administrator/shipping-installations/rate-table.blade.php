
    <input type="hidden" name="rate_zone_id[]" id="location_id" class="form-control location_id"  value="{{ isset($zones->id) ? $zones->id : '' }}" readonly>
    <input type="hidden" name="rate_id[]" id="rate_id" class="form-control rate_id"  value="{{ isset($rate) ? $rate->id : 'new' }}" readonly>
<div class="col-md-4 ">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
                Category Id
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
            <select name="category_id[]" id="category_id" class="select2 form-control" required>
                {{-- <option value="default">Choose category...</option> --}}
                @php
                    $rateCategoryId =  isset($rate) ? $rate->category_id : '';
                @endphp
                @foreach ($shipCategorys as $shipCategory)
                    <option value="{{ $shipCategory->id }}" {{$shipCategory->id == $rateCategoryId ? 'selected' : ''}} >Type : {{ $shipCategory->cat_type }} - {{ $shipCategory->cat_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0 calculationType">
            <p >
                Calculation Type <i class="fas fa-question-circle" id="tooltipdemo"></i>
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
            <select name="groupOrIndividual[]" id="groupOrIndividual" class="select2 form-control">
                <option value="per-line-quantity" {{ isset($rate) && $rate->calculation_type == 'per-line-quantity' ? 'selected' : ''}}>Per Line Quantity</option>
                <option value="per-shipping-category" {{ isset($rate) && $rate->calculation_type == 'per-shipping-category' ? 'selected' : ''}}>Per Shipping Category</option>
                <option value="self-collect-only" {{ isset($rate) && $rate->calculation_type == 'self-collect-only' ? 'selected' : ''}}>Self Collect Only</option>
                {{-- <option value="per-category-quantity" {{ isset($rate) && $rate->calculation_type == 'per-category-quantity' ? 'selected' : ''}}>Per Category Quantity (LIGHTING)</option> --}}
            </select>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
                Conditions
                <br>
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
            <select name="conditions[]" id="conditions" class="select2 form-control">
                <option value="quantity" {{ isset($rate) && $rate->condition == 'quantity' ? 'selected' : ''}}>Quantity</option>
                <option value="price" {{ isset($rate) && $rate->condition == 'price' ? 'selected' : ''}}>Price</option>
                <option value="weight" {{ isset($rate) && $rate->condition == 'weight' ? 'selected' : ''}}>Weight</option>
            </select>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
                Min
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">

            <input type="number" step=".01" name="min_rates[]" id="min" class="form-control"  value="{{ isset($rate) ? $rate->min_rates : '' }}" required>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
                Max
            </p>
        </div>
        <div class="col-12 col-md-8 form-group ">
            <input type="number" step=".01" name="max_rates[]" id="max" class="form-control"  value="{{ isset($rate) ? $rate->max_rates : '' }}" required>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
                Price
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
                <input type="text" name="price[]"class="form-control input-mask-price" value="{{ isset($rate) ? $rate->price : ''}}" required>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
               Message
                <br>
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
            <select name="set_status[]" id="set_status" class="select2 form-control">
                <option value="0" {{ isset($rate) && $rate->set_status == '0' ? 'selected' : ''}}>Select message</option>
                <option value="3" {{ isset($rate) && $rate->set_status == '3' ? 'selected' : ''}}>Ask for top up delivery fee</option>
            </select>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="row">
        <div class="col-12 col-md-4 text-md-right my-auto p-md-0">
            <p>
                Status
            </p>
        </div>
        <div class="col-12 col-md-8 form-group">
            <select name="active[]" id="active" class="select2 form-control">
                <option value="Y" {{ isset($rate) && $rate->active == 'Y' ? 'selected' : ''}}>Active</option>
                <option value="N" {{ isset($rate) && $rate->active == 'N' ? 'selected' : ''}}>Inactive</option>
            </select>
        </div>
    </div>
</div>



@if (isset($loop) && !$loop->last)
    <button type="button" class="btn btn-danger ml-auto col-md-1" style="height:30px;" onclick="removeRateElement(this);">
        <i class="fa fa-minus"></i>
    </button>
@else
    <button type="button" class="btn btn-danger d-none ml-auto col-md-1" style="height:30px;" onclick="removeRateElement(this);">
    <i class="fa fa-minus"></i>
    </button>

    <button type="button" class="btn btn-success ml-auto col-md-1" style="height:30px;" onclick="addRateElement();">
        <i class="fa fa-plus"></i>
    </button>
    @endif


@push('rateStyle')

<style>
    i#tooltipdemo {
  position: relative ;
}
i#tooltipdemo:hover::after {
  content: "Per Line Quantity is for normal product.\A Per Shipping Category is for group product. \A " ;
  /* Per Category Quantity is for individual product */
  font-weight: 300;
  line-height: 20px;
  white-space: pre-wrap;
  position: absolute ;
  text-align:left;
  top: 1.1em ;
  left: 1em ;
  min-width: 320px ;
  border: 1px #808080 solid ;
  padding: 8px ;
  color: black ;
  background-color: #fff8db ;
  z-index: 1 ;
}
    </style>

@endpush
@push('rateJS')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        maskPrice();
    });

    function addRateElement() {
    let rateContainer = $('#rateContainer')

    let currentItem = rateContainer.find('.default-item');

    currentItem.find('select.select2').select2('destroy');
    currentItem.find('.btn.btn-danger').removeClass('d-none');
    currentItem.find('.btn.btn-success').addClass('d-none');
    currentItem.removeClass('default-item');

    let cloneItem = currentItem.clone(false).hide();

    cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
    cloneItem.find('option').removeAttr('data-select2-id');
    cloneItem.addClass('default-item');
    cloneItem.find('.btn.btn-danger').addClass('d-none');
    cloneItem.find('.btn.btn-success').removeClass('d-none');
    cloneItem.find('.rate_id').val('new');
    cloneItem.appendTo(rateContainer);
    cloneItem.slideDown();

    rateContainer.find('select.select2').select2({
        theme: 'bootstrap4',
    });

    maskPrice();
    }

    function removeRateElement(element) {
        $(element).parent().slideUp(500, function() {
            $(this).remove();
        });
    }

    function maskPrice() {
        let priceSelectors = document.getElementsByClassName('input-mask-price');

        let priceInputMask = new Inputmask({
            'mask': '99999.99',
            'numericInput': true,
            'digits': 2,
            'digitsOptional': false,
            'placeholder': '0'
        });

        for (var i = 0; i < priceSelectors.length; i++) {
            priceInputMask.mask(priceSelectors.item(i));
        }
    }
</script>
@endpush
