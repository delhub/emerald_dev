<?php

namespace App\Console\Commands;

use App\Models\Purchases\Purchase;
use App\Models\Users\UserInfo;
use App\Models\Users\UserMembershipLogs;
use Illuminate\Console\Command;
use App\Traits\Membership;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class MembershipCheck extends Command
{
    use Membership;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade standard mmebership to advance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::createFromDate(Carbon::now()->toDateString());

        // check user expired date
        $this->checkExpiredMembership($today);

        $userID = $this->getTotalPurchaseAmount();

        $info = array();
        $info['job'] = 'upgrade membership';
        $info['upgraded_member'] = $userID;
        Log::error($info);
    }

    public function upgradeMember($membershipLogs)
    {
        //
        $userInfo = UserInfo::where('user_id', $membershipLogs->user_id)->first();
        $userInfo->user_level = 6001;
        $userInfo->save();

        $membershipLogs->last_checked = Carbon::now();
        $membershipLogs->checking_status = 'upgraded';
        $membershipLogs->save();

        return $userInfo->account_id;
    }

    public function getTotalPurchaseAmount()
    {
        //
        $userID = array();
        $membershipExpired = UserMembershipLogs::where('checking_status', 'active')->get();

        foreach ($membershipExpired as $membership) {
            $dateStart =  Carbon::createFromDate($membership->date_start)->toDateString();
            $dateEnd = Carbon::createFromDate($membership->date_end);

            $purchase_sum = Purchase::where('user_id',$membership->user_id)->whereIn('purchase_status',[3001, 3002, 4002, 4007])->whereBetween('created_at', [$dateStart, $dateEnd])->sum('purchase_amount');
            if ($purchase_sum != $membership->total_expenses) {

                $error = array();
                $error['file'] = 'chron membership';
                $error['user_id'] = $membership->user_id;
                $error['amount_before'] = $membership->total_expenses;
                $error['amount_after_sum'] = $purchase_sum ;
                Log::error($error);

                $membership->last_checked = Carbon::now();
                $membership->total_expenses = $purchase_sum ;
                $membership->save();
            }

            if ($membership->total_expenses >= (setting('standard_member_upgrade') * 100)) {
                $userID[] = $this->upgradeMember($membership);
            }
        }
        return $userID;
    }

    public function checkExpiredMembership($today){

        // $today = Carbon::now()->toDateString();
        //
        $membershipExpired = UserMembershipLogs::where('checking_status', 'active')->get();
        foreach ($membershipExpired as $membership) {
            $date_end = Carbon::createFromDate($membership->date_end);
            if ($today->gt($date_end)) {
                $membership->checking_status = 'expired';
                $membership->last_checked = Carbon::now();
                $membership->save();
            }
        }
    }
}
