@extends('layouts.guest.main')
@section('content')

<div class="container-fluid bg-white">
    <div class="row">
        <div class="col-md-8 col-sm-8 bgIMG">
            <div class="row form-row">
                <div class="col-12 d-block d-md-block d-lg-none" style="min-height: 250px;"></div>
                <div class="offset-md-2 col-md-10 col-12 offset-lg-3 col-lg-7">

                    <h3>Mattress Warranty Registration</h3>
                    <hr style="width: 92%;border:1px solid #ffcc00;" align="left">

                </div>
                <div class="col-12 offset-md-1 col-md-10 offset-md-1 offset-lg-3 col-lg-7">
                    <form action="{{route('guest.warranty.submit')}}" method="GET">

                        {{-- TEST START --}}


                                <div class="row">
                                    <div class="col-md-6 col-12 form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="" aria-describedby="" required>
                                    </div>
                                    <div class="col-md-6 col-12 form-group">
                                        <label for="contact">Contact Number</label>
                                        <input type="text" class="form-control" id="contact" name="contact" aria-describedby="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 form-group ">

                                            <label for="invoice">Invoice No</label>
                                            <input type="text" class="form-control" id="invoice" name="invoice" aria-describedby="" required>
                                    </div>
                                    <div class="col-sm-12 col-md-6 form-group ">
                                            <label for="delivery">Delivery Order</label>
                                            <input type="text" class="form-control" id="delivery" name="delivery" aria-describedby="" required>

                                    </div>
                                    <div class="col-sm-12 col-md-6 form-group">

                                            <label for="code">Code</label>
                                            <input type="text" class="form-control" id="code" name="code" aria-describedby="" required>

                                    </div>
                                    <div class="col-sm-12 col-md-6 form-group">
                                            <label for="product">Product List</label>
                                                <div class="form-control">
                                                    <div class="productList">
                                                        <select name="product" id="product">
                                                            <option value="Royal Series 1 - King">Royal Series 1 - King</option>
                                                            <option value="Royal Series 1 - Queen">Royal Series 1 - Queen </option>
                                                            <option value="Royal Series 1 - Super Single">Royal Series 1 - Super Single</option>
                                                            <option value="Wealth Series 1 - King">Wealth Series 1 - King</option>
                                                            <option value="Wealth Series 1 - Queen">Wealth Series 1 - Queen</option>
                                                            <option value="Wealth Series 1 - Super Single">Wealth Series 1 - Super Single</option>
                                                        </select>
                                                    </div>
                                                </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-12 form-check" style="margin-left:15px;">
                                        <input type="checkbox" class="form-check-input" id="checkBOX"  required>
                                        <label class="form-check-label" for="checkBOX">I have read, understand and accepted the <a href="" data-toggle="modal" data-target="#warrantyAgreementModal" style="color:#ffcc00;cursor: pointer;">terms and conditions</a>.</label>
                                    </div>

                                    <button class="col-md-3 col-5 btn grad2 bjsh-btn-gradient btn-small-screen" style="margin-left:10px; height: fit-content;">Submit</button>
                                </div>


                        {{-- TEST END --}}

                        {{-- <div class="row">
                            <div class="col-md-6 col-12 form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="" aria-describedby="" required>
                            </div>
                            <div class="col-md-6 col-12 form-group">
                                <label for="contact">Contact Number</label>
                                <input type="text" class="form-control" id="contact" name="contact" aria-describedby="" required>
                            </div>
                            <button class="col-md-3 col-5 btn grad2 bjsh-btn-gradient btn-small-screen" style="margin-left:10px; height: fit-content;">Submit</button>
                        </div>


                            </div>
                            <button class="col-md-3 col-5 btn grad2 bjsh-btn-gradient btn-small-screen" style="margin-left:10px; height: fit-content;">Submit</button>
                        </div>
                        </div> --}}
                            {{-- <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                <label for="invoice">Invoice No</label>
                                <input type="text" class="form-control" id="invoice" name="invoice" aria-describedby="" required>
                                </div>
                                <div class="col-md-6 col-12 form-group">
                                <label for="code">Code</label>
                                <input type="text" class="form-control" id="code" name="code" aria-describedby="" required>
                                </div>
                            </div> --}}

                            {{-- <div class="row">
                                <div class="col-md-6 col-12 form-group">
                                    <label for="delivery">Delivery Order</label>
                                    <input type="text" class="form-control" id="delivery" name="delivery" aria-describedby="" required>
                                </div>

                                <div class="col-md-6 col-12 form-group">
                                    <label for="product">Product List</label>



                                    <div class="form-control">
                                        <div class="productList">
                                            <select name="product" id="product">
                                                <option value="Royal Series 1 - King">Royal Series 1 - King</option>
                                                <option value="Royal Series 1 - Queen">Royal Series 1 - Queen </option>
                                                <option value="Royal Series 1 - Super Single">Royal Series 1 - Super Single</option>
                                                <option value="Wealth Series 1 - King">Wealth Series 1 - King</option>
                                                <option value="Wealth Series 1 - Queen">Wealth Series 1 - Queen</option>
                                                <option value="Wealth Series 1 - Super Single">Wealth Series 1 - Super Single</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                            {{-- <div class="row">
                                <div class="col-md-8 col-12 form-check" style="margin-left:15px;">
                                    <input type="checkbox" class="form-check-input" id="checkBOX"  required>
                                    <label class="form-check-label" for="checkBOX">I have read, understand and accepted the <a href="" data-toggle="modal" data-target="#warrantyAgreementModal" style="color:#ffcc00;cursor: pointer;">terms and conditions</a>.</label>
                                </div>

                                <button class="col-md-3 col-5 btn grad2 bjsh-btn-gradient btn-small-screen" style="margin-left:10px; height: fit-content;">Submit</button>
                            </div> --}}

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 order-first order-md-last">
            <div class="row h-100 ">
                <div class="col-lg-8 col-md-12 col-12 bg-light terms-row">
                    <br>
                    <span style="color: brown; font-weight:800;">STEP 1</span>
                    <br><br>
                    Complete your Warranty Registration using the online form below. Please fill in your invoice number from purchased invoice and code number on the mattress warranty tag.
                    <br><br><br>
                    <span style="color: brown; font-weight:800;">STEP 2</span>
                    <br><br>
                    Remember to read though the terms and conditions of warranty before submit the warranty registration form.
                    <br><br><br>
                    <span style="color: brown; font-weight:800;">STEP 3</span>
                    <br><br>
                    Fill up the form and click on SUBMIT button.
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="warrantyAgreementModal" tabindex="-1" role="dialog" aria-labelledby="warrantyAgreementModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-12 mb-0">
                        <div class="overflow-auto" style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                            <h5 class="header">Guarantee / warranty terms & conditions</h5>

                            <ol class="px-3 py-2">
                                <li>
                                    <p class="paragraph">
                                        Bujishu mattress carries a 10-years Pro-Rated Limited Warranty against manufacturing defects on its spring unit.
                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        Warranty on other components against manufacturing defects is valid for one year which excludes fair wear and tear.
                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        Warranty does not cover mattress that is soiled, burnt or evidently abused due to improper usage and handling.
                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        Any manufacturing defects arose within the first year of delivery shall be repaired without charge, but excludes transportation charge. It is to be borne by the purchaser.

                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        Should manufacturing defects develop after the first year of delivery, repairs shall be charged Pro-Rated on 1/10th of the purchase retail price multiply by the number of year used plus transportation charges.
                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        Bujishu reserves the right to substitute fabrics of equal quality if identical fabrics are not available at the time of repair or replacement.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Normal body impression on the quilted surface of the mattress is a normal occurrence as the upholstery layers are conforming to your body contours. These body impressions are characteristics of the mattress and are not considered manufacturing defects.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Any depression of less than 3cm of the original mattress height is considered physiological due to body impression, high elasticity of materials used in the manufacturing and normal wearing, thus not sagging.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Any possible odours arising from the mattress is considered normal and will diminish over time with sufficient amount of ventilation in your bedroom.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        For any new mattress, a ‘variable period’ for your body to adapt to the mattress is necessary.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Warranty does not cover ‘comfort expectation’.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        Bujishu shall not be held responsible for any repairs carried out outside its factory.
                                    </p>
                                </li>
                                <li>
                                    <p class="paragraph">
                                        This product warranty is only valid within Malaysia.
                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        This warranty is deemed void if:-
                                        <ol class="sub-list">
                                            <li>
                                                <p class="paragraph">
                                                    The mattress is on a sagging base.
                                                </p>
                                            </li>

                                            <li>
                                                <p class="paragraph">
                                                    The mattress is torn or mutilated.
                                                </p>
                                            </li>

                                            <li>
                                                <p class="paragraph">
                                                    The label is removed altered or tampered with.
                                                </p>
                                            </li>

                                            <li>
                                                <p class="paragraph">
                                                    The mattress is exposed to direct sunlight or soiled in unsanitary conditions.
                                                </p>
                                            </li>
                                        </ol>
                                    </p>
                                </li>

                                <li>
                                    <p class="paragraph">
                                        This warranty endures only to the benefit of the original purchaser (“The Purchaser”) of the product, commences on the date of mattress purchase, and shall not be extended or restarted should the mattress be replaced or repaired.
                                    </p>
                                </li>
                            </ol>
                            <h5 style="color: red;">ATTENTION</h5>
                            <ul style="color: red;">
                                <li>Mattress need to be unpacked and let it regain the shape, length, and thickness completely after 3-4 days;
                                </li>
                                <li>Mattress need to be unpacked within 6 months time from the product manufacturing date;</li>
                                <li>Do not remove any legal, material description, or branding labels attached to the mattress. These labels provide product identity and information on your rights to the warranty;</li>
                                <li>Do not attempt to bend or fold the mattress;</li>
                                <li>To avoid possible damage, please refrain from using cleansers on the mattress without seeking professional advice;</li>
                                <li>This warranty is only apply to mattresses sold and used within Malaysia.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>











.warranty-form{
    position: absolute;
    bottom: 22px;
    width: 75%;
    margin: auto;
    right: 40px;
}

.input-form-control{
    width: 300px;
    border: none;
    box-shadow: 0 0 20px 0px lightgrey;
    border-radius: 6px;
    padding: 5px 0px 5px 10px;
}

/* .productList{
    border-width: 1px !important;
      border-style: solid !important;

} */

.input-form-control:focus{
    box-shadow: 0 0 8px 0px #ffcc00ab;
    outline: none !important;

}

.terms-checkbox:hover{
    box-shadow: 0 0 20px 0px #ffcc00;
    border: 0;
    outline: none !important;
}

/* Small devices (landscape phones, 576px and up) */
@media (max-width: 575.98px) {
    #product{
    border:0px;
   outline:0px;
   width:100%;
}

    .form-row{
        /* background-color: green; */
        margin:0;
    }
    .bgIMG {
        background-image: url(/assets/images/background-image/warranty-bg.jpg);
        background-repeat: no-repeat;
        background-position: center top;
        background-size: 500px;
    }
    .btn-middle{
        margin:0 auto;
    }
    /* #product{
        width:360px;
        height:40px;
    } */
}

/* Medium devices (tablets, 768px and up) */
@media (min-width: 576px) and (max-width: 767.98px) {
    #product{
    border:0px;
   outline:0px;
   width:100%;
}
    .form-row{
        /* background-color: yellow; */


    }
    .terms-row{
        padding: 20px 10px;
    }
    /* #product{
 width:360px;
 height:45px;
} */
}

/* Large devices (desktops, 992px and up) */
@media (min-width: 768px) and (max-width: 991.98px) {
    #product{
    border:0px;
   outline:0px;
   width:100%;
}
    .form-row{
        /* background-color: blue; */
        margin:0;

    }

    .terms-row{
        padding: 20px 10px;
    }

    .bgIMG {
        background-image: url(/assets/images/background-image/warranty-bg.jpg);
        background-repeat: no-repeat;
        background-position: center top;
        background-size: 500px;
    }

    /* #product{
 width:235px;
 height:38px;
} */
}

/* Extra large devices (large desktops, 1200px and up) */
@media (min-width: 992px) {
    #product{
    border:0px;
   outline:0px;
   width:100%;
}
    .form-row {
        /* background-color: red; */
        position:absolute;
        bottom:0px;
        width:100%;
    }

    /* .productList {

        position:absolute;
        bottom:0px;
        width:100%;

    } */
    /* #product{
 width:250px;
 height:40px;
} */

    .bgIMG {
        background-image: url(/assets/images/background-image/warranty-bg.jpg) ;
        background-repeat: no-repeat;
        background-position: right top;
        background-size: contain;
        min-height: 681px;
    }
}

/* .productList{
    border: none;
} */



.dropdown-toggle {

 }

 .dropdown-menu{
      outline:0px !important;
    -webkit-appearance:none;
    box-shadow: none !important;

 }

 .form-control {text-align:left;}



</style>
<script>

$(function(){

$(".dropdown-menu").on('click', 'a', function(){
  $(".form-control:first-child").text($(this).text());
  $(".form-control:first-child").val($(this).text());
});

});
</script>
@endsection
