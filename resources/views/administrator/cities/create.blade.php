@extends('layouts.administrator.main')

@section('content')

    <div class="row">
        <div class="col-12">
            {{-- href="/administrator/faq" --}}
            <a href="{{ route('administrator.cities.index') }}"  class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>

    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">

                    <h4>
                        Create Cities
                    </h4>
                </div>

                <div class="col-12">

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="{{ route('administrator.cities.store')}}" id="edit-cities-form" method="POST" enctype= "multipart/form-data">

                        @csrf

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Country
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                <input type="text" name="country_id" id="country_id" class="form-control" value="{{ old('country_id') }}" pattern="[A-Z]{2}" placeholder="MY" title="Two capital letter country code" maxlength="2" style="text-transform:uppercase">
                                <small class="text-danger">{{ $errors->first('country_id') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    State
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
                                @if(count($states) > 0)

                                <select name="state_id" id="state_id" class="select2 form-control form-group{{ $errors->has('state_id') ? ' has-error' : '' }}" style="width: 100%;" onchange="key_state()">

                                    <option value="">Select a state</option>

                                    @foreach($states as $state)

                                    <option data-key="{{ $state->abbreviation }}" value="{{ $state->id }}">{{ $state->name }}</option>

                                    @endforeach

                                </select>
                                <small class="text-danger">{{ $errors->first('state_id') }}</small>

                                @endif
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City Key
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('key') ? ' has-error' : '' }}">

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="cityKey_state">_</span>
                                    </div>
                                    <input type="text" name="cityKey_city" id="cityKey_city" class="form-control" value="{{ old('cityKey_city') }}" maxlength="3" style="text-transform:uppercase" onkeyup="combineKey()">
                                </div>
                                <input type="text" name="city_key" id="city_key" class="form-control" value="{{ old('city_key') }}" maxlength="3" style="text-transform:uppercase">
                                <small class="text-danger">{{ $errors->first('key') }}</small>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City Name
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" name="city_name" id="city_name" class="form-control" oninput="myFunction()" value="{{ old('city_name') }}">
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    City Data
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                                <input type="text" name="city_data" id="city_data" class="form-control" value="{{ old('city_data') }}">
                                <small class="text-danger">{{ $errors->first('data') }}</small>
                            </div>
                        </div>

                        <div class="row d-none">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Active
                                </p>
                            </div>

                            <div class="col-12 col-md-3 form-group">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="citySwitch" name="citySwitch" checked value="Y">
                                  </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-7 text-md-right my-auto">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')

<script>
// function myFunction() {
//   var x = document.getElementById("name").value;
//   document.getElementById("data").value =  x;
// }

function key_state() {

    var d = $('#state_id').data('key');
    var state_abbv = $("#state_id option:selected").attr('data-key');
    $('#cityKey_state').html(state_abbv + '_');
    combineKey();
}

function combineKey() {
  var state = document.getElementById("cityKey_state").innerHTML;
  var city = document.getElementById("cityKey_city").value;
  var key = state + city;
  document.getElementById("city_key").value = key;
}

</script>

@endpush