<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Products\WarehouseInventories;

class RemoveEmptyOrderInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:removeEmptyOrder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Inventory that empty warehouse order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $totalRemove = 0;
        $allInventory = WarehouseInventories::with('order')->where('model_type', 'App\Models\Warehouse\Order\WarehouseOrder')->get();

        foreach ($allInventory as $key => $inventory) {

            if (!isset($inventory->order)) {

                $inventory->delete();
                $totalRemove++;
                $this->info('removed inventory id = ' . $inventory->id);
            }
        }


        $this->info('Done. Total removed :' . $totalRemove);
        return;
    }
}
