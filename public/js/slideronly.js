$(document).ready(() => {

    //single product item slider
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        speed: 500,
        asNavFor: '.slider-for',
        dots: false,
        arrows: true,
        centerMode: true,
        focusOnSelect: true
    });



    //bottom product slider
    $('.center.my.responsive').slick({
        centerMode: true,
        centerPadding: '10px',
        autoplay: true,
        arrows: true,
        autoplaySpeed: 1750,
        mobileFirst: true,
        pauseOnFocus: false,
        //pauseOnHover: false,
        infinite: true,
        slidesToShow: 1,
        loop: true,
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    arrows: true,
                    autoplaySpeed: 1750,
                    autoplay: true,
                    centerMode: true,
                    centerPadding: '200px',
                    loop: true,
                    pauseOnFocus: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1400,
                settings: {
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 1750,
                    centerMode: true,
                    centerPadding: '200px',
                    loop: true,
                    pauseOnFocus: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 1750,
                    centerMode: true,
                    centerPadding: '50px',
                    loop: true,
                    pauseOnFocus: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 1750,
                    centerMode: true,
                    centerPadding: '50px',
                    loop: true,
                    pauseOnFocus: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 458,
                settings: {
                    arrows: true,
                    autoplay: true,
                    sautoplaySpeed: 1750,
                    centerMode: true,
                    centerPadding: '10px',
                    loop: true,
                    pauseOnFocus: false,
                    slidesToShow: 3
                }
            }
        ]
    });

    //item slider
    $('.centermy').slick({
        centerMode: true,
        centerPadding: '100px',
        slidesToShow: 1,
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '10px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '100px',
                    slidesToShow: 1
                }
            }
        ]
    });

    $(".home-pg").owlCarousel({
        dots: false,
        autoplay: true,
        autoplayTimeout: 2500,
        loop: true,
        margin: 10,
        nav: true,
        //navText: ['&#8249', '&#8250;'],
        responsive: {
            0: {
                items: 1
            },

            320: {

                items: 2
            },

            600: {
                items: 3
            },

            900: {
                items: 5
            },

            1024: {
                items: 6
            },

            1366: {
                items: 7
            }
        }
    });

    $(".home-leadbyctg").owlCarousel({
        dots: false,
        loop: false,
        margin: 10,
        nav: true,
        responsive: {
            0: {

                items: 3
            },

            320: {

                items: 5
            },

            600: {
                items: 5
            },

            1024: {
                items: 7
            },

            1366: {
                items: 10
            }
        }
    });


    // owl slider
    // $('.owl-carousel').owlCarousel({
    //     // stagePadding: 50,
    //     dots: false,
    //     loop: false,
    //     margin: 10,
    //     nav: true,

    //     responsive: {
    //         0: {
    //             items: 2
    //         },
    //         600: {
    //             items: 3
    //         },
    //         1000: {
    //             items: 6
    //         },
    //         1280: {
    //             items: 6
    //         },
    //         1920: {
    //             items: 7
    //         }
    //     }
    // });

    //slider menu
    var metrics = {};
    var scrollOffset = 0;

    var container = document.getElementById("outer");
    var bar = document.getElementById("pills-tab");

    if (container != null) {

        function setMetrics() {
            metrics = {
                bar: bar.scrollWidth || 0,
                container: container.clientWidth || 0,
                left: parseInt(bar.offsetLeft),
                getHidden() {
                    return (this.bar + this.left) - this.container
                }
            }

            updateArrows();
        }

        function doSlide(direction) {
            setMetrics();
            var pos = metrics.left;
            if (direction === "right") {
                amountToScroll = -(Math.abs(pos) + Math.min(metrics.getHidden(), metrics.container));
            }
            else {
                amountToScroll = Math.min(0, (metrics.container + pos));
            }
            bar.style.left = amountToScroll + "px";
            setTimeout(function () {
                setMetrics();
            }, 400)
        }

        function updateArrows() {
            if (metrics.getHidden() === 0) {
                document.getElementsByClassName("toggleRight")[0].classList.add("text-light");
            }
            else {
                document.getElementsByClassName("toggleRight")[0].classList.remove("text-light");
            }

            if (metrics.left === 0) {
                document.getElementsByClassName("toggleLeft")[0].classList.add("text-light");
            }
            else {
                document.getElementsByClassName("toggleLeft")[0].classList.remove("text-light");
            }
        }

        function adjust() {
            bar.style.left = 0;
            setMetrics();
        }

        document.getElementsByClassName("toggleRight")[0].addEventListener("click", function (e) {
            e.preventDefault()
            doSlide("right")
        });

        document.getElementsByClassName("toggleLeft")[0].addEventListener("click", function (e) {
            e.preventDefault()
            doSlide("left")
        });

        window.addEventListener("resize", function () {
            // reset to left pos 0 on window resize
            adjust();
        });

        setMetrics();

    } else { }

});
