<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\Registrations\WelcomeAndVerify;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\User;
use App\Models\Users\UserInfo;
use App\Models\Users\UserAddress;
use App\Models\Users\UserContact;
use App\Models\Globals\City;
use App\Http\Controllers\BujishuAPI;
use App\Http\Controllers\XilnexAPI;
use App\Models\Users\UserCountry;
use App\Models\Purchases\Purchase;
use App\Models\Globals\Countries;
use Illuminate\Support\Facades\Auth;
use App\Models\Globals\State;
use App\Models\ShippingInstallations\Fee;
use App\Models\Users\UserMembershipLogs;

class CustomerRegisterController extends Controller
{
    public function index()
    {
        // Get values from database to populate form fields.
        $genders = Gender::all();
        $races = Race::all();
        $states = State::all();
        $countries = Countries::all();
        $maritals = Marital::all();
        $employments = Employment::all();
        $inv_placehold = (country()->country_id == 'MY') ? 'BJN2021XXXXXXX' : 'SBJN2021XXXXXXX';

        return view('auth.register.customer')
            ->with('genders', $genders)
            ->with('races', $races)
            ->with('states', $states)
            ->with('countries', $countries)
            ->with('maritals', $maritals)
            ->with('inv_placehold', $inv_placehold)
            ->with('employments', $employments);
    }

    public function store(Request $request)
    {
        // return $request;
        $request->validate([
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users'
            ],
            'password' => [
                'required',
                'string',
                'min:8',
                'confirmed'
            ],
            'full_name' => [
                'required',
                'string'
            ],
            // 'nric' => [
            //     'required',
            //     'min:12',
            //     'max:12'
            // ],
            'address_1' => [
                'required'
            ],
            'postcode' => [
                'required'
            ],
            'city_key' => [
                'required'
            ],
            'state' => [
                'required'
            ],
            'contact_number_mobile' => [
                'required',
                'min:10'
            ]
        ]);
        if ($request->referrer_choices == 'yes_referrer') {
            $request->validate([
                'referrer_id' => [
                    'required',
                    'min:10',
                    'max:10',
                    'exists:dealer_infos,account_id'
                ],
                'referrer_name' => [
                    'required',
                ],
                'invoice_number' => [
                    'required'
                ]
            ]);
        }
        if ( $request->input('whatProduct') != NULL ) return back()->with(['error' => "Verification Error"]);

        $purchase = Purchase::where('purchase_number', 'LIKE', '%' . $request->input('invoice_number'))->first();

        // xilnex data create client
        $data = [
            'name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('contact_number_mobile'),
            'category' => 'Personal'
        ];

        //XilnexAPI POS create client
        /* $xilnex = XilnexAPI::createClient($data);
        $request->xilnex_client_id = $xilnex->data->client->id; */

        $user = self::createCustomer($request, false, $purchase->country_id ?? country()->country_id);
        $user->notes = null;
        $user->save();

        // disable server b API
        // $bjs_user_id = $user->userInfo->account_id;
        // $name = addslashes($request->input('full_name'));
        // $upline_agent_id = $request->input('referrer_id');
        // $ic = $request->input('nric');
        // $email = $request->input('email');
        // $referral_name = $user->referrer_name;
        // $group_id = $user->group_id;

        // disable server b API
        // BujishuAPI::createUser($bjs_user_id, $name, $upline_agent_id, $ic, $email, $referral_name, $group_id);

        if ($request->referrer_choices == 'no_referrer') {

            $url = URL::temporarySignedRoute('verification.verify', Carbon::now()->addDay(1), [
                'id' => $user->id,
                'hash' => $user->email
            ]);

            // return $url;

            // $emailTo = DealerInfo::where('account_id', $request->input('referrer_id'))
            //     ->first()->user->email;

            $emailTo = $user->email;



            Mail::to($emailTo)->send(new WelcomeAndVerify($url, $user));
        }

        return $this->verifyPage();
        // return redirect('/login?fromregister=true');
    }
    public function verifyPage(){
        return view('auth.verify');
    }

    public static function createCustomer($request, $verify = false, $country_id)
    {
        $city = NULL;

        // Register customer.
        if ($request->input('city') != NULL && $request->input('city_key') == 0) {
            $city = $request->input('city');
        }
        $password = Hash::make($request->input('password'));
        if (isset($request->hashPass)) {
            $password = $request->hashPass;
        }
        // Users table.
        $user = new User;
        $user->email = $request->input('email');
        $user->password = $password;
        if ($verify) $user->email_verified_at = Carbon::now();
        $user->notes = 'incomplete';
        $user->save();

        // Generating new customer account id.
        $largestCustomerId = 0;
        if (UserInfo::all()->count() == 0) {
            $largestCustomerId = 1913000101;
        } else {
            $largestCustomerId = UserInfo::largestCustomerId() + 1;
        }
        $referrer_id = ($request->input('referrer_id') != NULL) ? $request->input('referrer_id') : 1010;
        $upline_dealer = DealerInfo::where('account_id', $referrer_id)->first();
        $group_id = (isset($upline_dealer->group_id)) ? $upline_dealer->group_id : 14;

        // User_infos table.
        $userInfo = new UserInfo;
        $userInfo->user_id = $user->id;
        $userInfo->account_id = $largestCustomerId;

        if(isset($request->metapoint)) {
            if ($request->metapoint >= 58800) {
                $userInfo->user_level = 6001;
            } else {
                $userInfo->user_level = 6000;
            }
        }
        else {
            $userInfo->user_level = self::checkInvoiceAmount($request);
        }
        // $userInfo->xilnex_client_id = $request->xilnex_client_id;
        // $userInfo->account_status = ($request->input('validationOptions')) ? $request->input('validationOptions') : 0;

        if ($request->input('validationOptions') != null) {
            $userInfo->account_status  = $request->input('validationOptions');
        }

        $userInfo->full_name = $request->input('full_name');
        $userInfo->nric = $request->input('nric');
        $userInfo->date_of_birth = $request->input('date_of_birth');
        $userInfo->passport_no = $request->input('passport_no');
        $userInfo->country = $request->input('country');
        $userInfo->referrer_id = $referrer_id;
        $userInfo->group_id = $group_id;
        $userInfo->referrer_name = $request->input('referrer_name') ?? (isset($upline_dealer->full_name) ? $upline_dealer->full_name : null);
        $userInfo->invoice_number = $request->input('invoice_number');
        $userInfo->year_joined = $request->input('yearStartDC');
        $userInfo->product_used = $request->input('whatProduct');
        $userInfo->save();

        // Users table.
        $userCountry = new UserCountry;
        $userCountry->account_id = $largestCustomerId;
        $userCountry->country_id = $country_id;
        $userCountry->referrer_id =  $country_id . $referrer_id;
        $userCountry->group_id = $group_id;
        $userCountry->save();

        // User_addresses table(two records - billing address and shipping address)
        $userAddress_billing_address = new UserAddress;
        $userAddress_billing_address->account_id = $userInfo->account_id;
        $userAddress_billing_address->address_1 = $request->input('address_1');
        $userAddress_billing_address->address_2 = $request->input('address_2');
        $userAddress_billing_address->address_3 = $request->input('address_3');
        $userAddress_billing_address->postcode = $request->input('postcode');
        $userAddress_billing_address->city = $city;
        $userAddress_billing_address->city_key = $request->input('city_key');
        $userAddress_billing_address->state_id = ($state = $request->input('state')) ? $state : 14;
        $userAddress_billing_address->country_id = country()->country_id;
        $userAddress_billing_address->is_shipping_address = 0;
        $userAddress_billing_address->is_residential_address = 0;
        $userAddress_billing_address->is_mailing_address = 1;
        $userAddress_billing_address->save();

        $userAddress_shipping_Address = new UserAddress;
        $userAddress_shipping_Address->account_id = $userInfo->account_id;
        $userAddress_shipping_Address->address_1 = $request->input('address_1');
        $userAddress_shipping_Address->address_2 = $request->input('address_2');
        $userAddress_shipping_Address->address_3 = $request->input('address_3');
        $userAddress_shipping_Address->postcode = $request->input('postcode');
        $userAddress_shipping_Address->city = $city;
        $userAddress_shipping_Address->city_key = $request->input('city_key');
        $userAddress_shipping_Address->state_id = ($state = $request->input('state')) ? $state : 14;
        $userAddress_shipping_Address->country_id = country()->country_id;
        $userAddress_shipping_Address->is_shipping_address = 1;
        $userAddress_shipping_Address->is_residential_address = 0;
        $userAddress_shipping_Address->is_mailing_address = 0;
        $userAddress_shipping_Address->save();

        // User_contacts table (Home).
        $userContactHome = new UserContact;
        $userContactHome->account_id = $userInfo->account_id;
        $userContactHome->contact_num = $request->input('contact_number_home');
        $userContactHome->is_home = 1;
        $userContactHome->save();

        // User_contacts table (Mobile).
        $userContactMobile = new UserContact;
        $userContactMobile->account_id = $userInfo->account_id;
        $userContactMobile->contact_num = $request->input('contact_number_mobile');
        $userContactMobile->is_mobile = 1;
        $userContactMobile->save();

        $user->assignRole('customer');

        return $user;
    }

    public function stateFilterCity(Request $request)
    {
        $state = $request->input('state');
        $cities = City::where('state_id', $state)->orderBy('city_name')->get();

        foreach ($cities as $city) {
            $rCity[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $rCity[] = '<option value="0" "selected">Others</option>';

        return $rCity;
    }

    public function stateFilterCompanyCity(Request $request)
    {
        $state = $request->input('company_state');
        $cities = City::where('state_id', $state)->get();

        foreach ($cities as $city) {
            $rCity[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $rCity[] = '<option value="0" "selected">Others</option>';

        return $rCity;
    }

    public function customerInformationRegisterView()
    {
        $user = User::find(Auth::user()->id);
        $user_info = UserInfo::where('user_id', $user->id)->first();
        $user_contact = UserContact::where('account_id', $user_info->account_id)->where('is_mobile', 1)->first();
        $states = State::all();
        $cities = City::all();
        $countries = Countries::all();
        return view('auth.register.v1.customer-info')
            ->with('user', $user)
            ->with('user_info' ,$user_info)
            ->with('user_contact' ,$user_contact)
            ->with('states' ,$states)
            ->with('cities' ,$cities)
            ->with('countries' ,$countries);
    }

    /*
     * Customer information POST.
     */
    public function customerInformationRegister(Request $request)
    {
        // Validator::make($request->all(),
        // [
        //     'full_name' => ['required', 'min:3'],
        //     'nric' => ['required', 'digits:12'],
        //     'date_of_birth' => ['required'],
        //     'address_1' => ['required', 'min:3'],
        //     'postcode' => ['required', 'digits_between:5,6'],
        //     'city_key' => ['required'],
        //     'state' => ['required'],
        //     'contact_number_mobile' => ['required', 'digits_between:10,15'],
        // ],[
        //     'full_name.required' => 'Please enter your full name.',
        //     'full_name.min' => 'Your name must be more than 3 characters',
        //     'nric.required' => 'Please enter your NRIC number.',
        //     'address_1.required' => 'Please enter your address.',
        //     'postcode.required' => 'Please enter your postcode.',
        //     'city_key.required' => 'Please select your city.',
        //     'state.required' => 'Please select your state.',
        //     'contact_number_mobile.required' => 'Please enter your mobile number.',
        //     'contact_number_mobile.digits_between' => 'Please enter a valid mobile number.',
        // ])->validate();

        $user = User::find(Auth::user()->id);

        $userInfo = UserInfo::find($user->id);
        $userInfo->full_name = $request->input('full_name');
        $userInfo->nric = $request->input('nric');
        $userInfo->date_of_birth = $request->input('date_of_birth');
        $userInfo->passport_no = $request->input('passport_no');
        $userInfo->country = $request->input('country');
        $userInfo->save();

        $userAddressBilling = $userInfo->shippingAddress;
        $userAddressBilling->address_1 = $request->input('address_1');
        $userAddressBilling->address_2 = $request->input('address_2');
        $userAddressBilling->address_3 = $request->input('address_3');
        $userAddressBilling->postcode = $request->input('postcode');
        $userAddressBilling->city = $request->input('city');
        $userAddressBilling->city_key = $request->input('city_key');
        $userAddressBilling->state_id = $request->input('state');
        $userAddressBilling->save();

        $userAddressMailing = $userInfo->mailingAddress;
        $userAddressMailing->address_1 = $request->input('address_1');
        $userAddressMailing->address_2 = $request->input('address_2');
        $userAddressMailing->address_3 = $request->input('address_3');
        $userAddressMailing->postcode = $request->input('postcode');
        $userAddressMailing->city = $request->input('city');
        $userAddressMailing->city_key = $request->input('city_key');
        $userAddressMailing->state_id = $request->input('state');
        $userAddressMailing->save();

        if ($request->input('contact_number_home') != null) {
            $userContactHome = $userInfo->homeContact;
            $userContactHome->contact_num = $request->input('contact_number_home');
            $userContactHome->save();
        }

        $userContactMobile = $userInfo->mobileContact;
        $userContactMobile->contact_num = $request->input('contact_number_mobile');
        $userContactMobile->save();

        $bjs_user_id = $userInfo->account_id;
        $name = addslashes($request->input('full_name'));
        $upline_agent_id = $request->input('referrer_id');
        $ic = $userInfo->nric;
        $email = $user->email;
        $referral_name = $request->input('referrer_name');
        $group_id = $user->group_id;

        BujishuAPI::createUser($bjs_user_id, $name, $upline_agent_id, $ic, $email, $referral_name, $group_id);

        return redirect('/');
    }

    public static function createMembershipLogs($user){
        $userInfo = $user->userInfo;
        if ($userInfo && $userInfo->user_level == 6000) {
            $membershipLogs = new UserMembershipLogs;
            $membershipLogs->user_id = $userInfo->user_id;
            $membershipLogs->date_start = Carbon::now();
            $membershipLogs->date_end = Carbon::now()->addYear(1);
            $membershipLogs->last_checked = Carbon::now();
            $membershipLogs->total_expenses = 0;
            $membershipLogs->checking_status = 'active';
            $membershipLogs->created_at = Carbon::now();
            $membershipLogs->updated_at = Carbon::now();
            $membershipLogs->save();
        }
        // dd($user,$user->userInfo);
    }

    public static function checkInvoiceAmount($request){
        $userLevel = 6000;

        if ($request->referrer_choices == 'yes_referrer') {
            $purchase_table = Purchase::where('inv_number', $request->invoice_number)->first();

            if ($purchase_table) {
                $feeTable = Fee::where('purchase_id', $purchase_table->id)->where('type', 'shipping_fee')->first();

                if (isset($feeTable)) {
                    $purchase_amount = $purchase_table->purchase_amount - $feeTable->amount;
                } else {
                    $purchase_amount = $purchase_table->purchase_amount;
                }

                if ($purchase_amount >= setting('advance_member_invoice')*100) {
                    $userLevel = 6001;
                }
            }
        }

        return $userLevel;
    }
}
