<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRbsIdInDealerDirectPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            $table->string('rbs_id')->nullable()->after('outlet_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            $table->dropColumn('rbs_id');
        });
    }
}
