<?php

namespace App\Models\Products;

use App\Traits\ProductBrandLogs;
use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    use ProductBrandLogs;
    // Set table
    protected $table = 'product_brand';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'name',
        'slug',
        'type',
        'parent_category_id'
    ];

    /**
     * Get the category's image.
     */
    public function image()
    {
        return $this->morphOne('App\Models\Globals\Image', 'imageable');
    }

    public function productAssigned()
    {
        return $this->hasMany('App\Models\Globals\Products\Product', 'brand_id', 'id');
    }

    /**
     * Get top level category.
     */
    // public function scopeTopLevelCategory($query)
    // {
    //     return $query->where('parent_category_id', 0)->where('type', 'shop')->get();
    // }

    /**
     * Get the category's parent if any.
     */
    // public function parentCategory()
    // {
    //     return $this->belongsTo('App\Models\Categories\Category', 'parent_category_id');
    // }

    // public function getLabelAttribute()
    // {
    //     $currentCategory = $this;
    //     $label = $currentCategory->name;
    //     while (isset($currentCategory->parentCategory)){
    //         $currentCategory = $currentCategory->parentCategory;

    //         $label = $currentCategory->name . ' > '. $label;
    //    }
    //     return $label;
    // }

    /**
     * Get all of the category's child if any.
     */
    // public function childCategories()
    // {
    //     return $this->hasMany('App\Models\Categories\Category', 'parent_category_id');
    // }

    /**
     * Get all products belonging to a category.
     */
    // public function products()
    // {
    //     return $this->belongsToMany(
    //         'App\Models\Globals\Products\Product',
    //         'piv_category_product',
    //         'category_id',
    //         'product_id'
    //     );
    // }

    // public function categoryId()
    // {
    //     return $this->hasMany('App\Models\Categories\Category', 'active');
    // }

    // public function scopetopFilter($query, $topLevelSlug)
    // {
    //     if ($topLevelSlug != null) {
    //         $query = $query->where('slug', $topLevelSlug);
    //     }
    //     return $query;
    // }

    // public function scopesubFilter($query,$topLevelSlug)
    // {
    //     if ($subLevel != null) {
    //         // $subLevelID = $query->where('slug',$subLevel)->first();
    //         $query = $query->where('slug',$subLevel);
    //     } else {
    //         $query = $query->where('slug',$topLevelSlug);
    //     }
    //     return $query;
    // }

    // public function scopeQualityFilter($query, $quality)
    // {
    //     if ($quality != null) {
    //         switch ($quality) {
    //             case 'standard':
    //                 $qualityID = 1;
    //                 break;
    //             case 'moderate':
    //                 $qualityID = 2;
    //                 break;
    //             case 'premium':
    //                 $qualityID = 3;
    //                 break;
    //         }
    //         $query = $query
    //             ->join('piv_category_product', 'piv_category_product.category_id', '=', 'categories.id')
    //             ->join('global_products', 'global_products.id', '=', 'piv_category_product.product_id')
    //             ->select('global_products.*')
    //             ->where('global_products.quality_id',$qualityID);
    //     }

    //     return $query;
    // }

    // do if null return 1=1
}
