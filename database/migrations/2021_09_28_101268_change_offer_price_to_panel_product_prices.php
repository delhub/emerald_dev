<?php

use App\Models\Products\ProductPrices;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOfferPriceToPanelProductPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->tinyInteger('is_free')->default('0')->after('product_sv');
            $table->tinyInteger('is_offer')->default('0')->after('product_sv');

            $table->bigInteger('outlet_offer_price')->default('0')->after('premier_price');
            $table->bigInteger('web_offer_price')->default('0')->after('premier_price');
            $table->bigInteger('outlet_price')->default('0')->after('fixed_price');
            $table->bigInteger('standard_price')->default('0')->after('outlet_price');
            $table->bigInteger('advance_price')->default('0')->after('outlet_price');
        });

        $productPrices = ProductPrices::all();
        $pos_prices = self::prices();

        foreach ($productPrices as $productPrice) {
            $product_code = $productPrice->product_code;
            $productPrice->advance_price =  $productPrice->price;
            $productPrice->web_offer_price =  $productPrice->price;
            if ($productPrice->country_id == 'MY' && isset($pos_prices[$product_code]) && $productPrice->active == 1) {
                $productPrice->outlet_price = $pos_prices[$product_code]['fixed_price'];

            } else {
                if( $productPrice->country_id == 'MY' &&  $productPrice->active == 1)  echo  $product_code . ' NO POS PRICE. // ';

                $productPrice->outlet_price =  $productPrice->fixed_price;

            }
            $productPrice->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->dropColumn('is_free');
            $table->dropColumn('is_offer');
            $table->dropColumn('outlet_offer_price');
            $table->dropColumn('outlet_price');
            $table->dropColumn('standard_price');
            $table->dropColumn('advance_price');
            $table->dropColumn('outlet_offer_price');
            $table->dropColumn('web_offer_price');
        });
    }

    public static function prices()
    {
        $panel_product_prices = array(
            array(
                "product_code" => "AVNBWX0002",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "AVNBOX0003",
                "fixed_price" => 4990,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "AVNBOX0004",
                "fixed_price" => 5890,
                "advance_price" => 5590,
            ),
            array(
                "product_code" => "AVNBOX0005",
                "fixed_price" => 5390,
                "advance_price" => 5090,
            ),
            array(
                "product_code" => "AVNBWX0006",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "AVNBOX0007",
                "fixed_price" => 4990,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "AVNBOX0008",
                "fixed_price" => 1749,
                "advance_price" => 1590,
            ),
            array(
                "product_code" => "AVNBWX0009",
                "fixed_price" => 5590,
                "advance_price" => 5390,
            ),
            array(
                "product_code" => "AVNBOX0010",
                "fixed_price" => 4590,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "AVNBWX0011",
                "fixed_price" => 5490,
                "advance_price" => 5290,
            ),
            array(
                "product_code" => "AVNBOX0012",
                "fixed_price" => 4590,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "AVNBWX0013",
                "fixed_price" => 7290,
                "advance_price" => 6890,
            ),
            array(
                "product_code" => "AVNBOX0014",
                "fixed_price" => 6790,
                "advance_price" => 6390,
            ),
            array(
                "product_code" => "VITBJX0001",
                "fixed_price" => 16890,
                "advance_price" => 15590,
            ),
            array(
                "product_code" => "VITANX0002",
                "fixed_price" => 30790,
                "advance_price" => 28290,
            ),
            array(
                "product_code" => "VITENX0003",
                "fixed_price" => 15290,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "VITVIX0004",
                "fixed_price" => 16990,
                "advance_price" => 14990,
            ),
            array(
                "product_code" => "VITANX0005",
                "fixed_price" => 19990,
                "advance_price" => 18290,
            ),
            array(
                "product_code" => "VITBJX0006",
                "fixed_price" => 10990,
                "advance_price" => 9490,
            ),
            array(
                "product_code" => "VITENX0007",
                "fixed_price" => 13990,
                "advance_price" => 12690,
            ),
            array(
                "product_code" => "VITENX0008",
                "fixed_price" => 25090,
                "advance_price" => 22690,
            ),
            array(
                "product_code" => "VITENX0009",
                "fixed_price" => 10090,
                "advance_price" => 8990,
            ),
            array(
                "product_code" => "VITENX0010",
                "fixed_price" => 19290,
                "advance_price" => 17190,
            ),
            array(
                "product_code" => "VITENX0011",
                "fixed_price" => 28790,
                "advance_price" => 26190,
            ),
            array(
                "product_code" => "VITENX0012",
                "fixed_price" => 17390,
                "advance_price" => 16590,
            ),
            array(
                "product_code" => "VITCHX0013",
                "fixed_price" => 16590,
                "advance_price" => 14990,
            ),
            array(
                "product_code" => "VITIMX0014",
                "fixed_price" => 18290,
                "advance_price" => 16590,
            ),
            array(
                "product_code" => "VITVIX0015",
                "fixed_price" => 10590,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "VITVIX0016",
                "fixed_price" => 21290,
                "advance_price" => 19090,
            ),
            array(
                "product_code" => "VITMOX0017",
                "fixed_price" => 12990,
                "advance_price" => 12190,
            ),
            array(
                "product_code" => "VITREX0018",
                "fixed_price" => 18390,
                "advance_price" => 16690,
            ),
            array(
                "product_code" => "VITBSX0019",
                "fixed_price" => 19390,
                "advance_price" => 17690,
            ),
            array(
                "product_code" => "VITBSX0020",
                "fixed_price" => 22290,
                "advance_price" => 20190,
            ),
            array(
                "product_code" => "VITBSX0021",
                "fixed_price" => 15290,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "VITANX0022",
                "fixed_price" => 20390,
                "advance_price" => 18590,
            ),
            array(
                "product_code" => "VITWEX0023",
                "fixed_price" => 24890,
                "advance_price" => 22690,
            ),
            array(
                "product_code" => "VITCHX0024",
                "fixed_price" => 5090,
                "advance_price" => 4590,
            ),
            array(
                "product_code" => "VITCHX0025",
                "fixed_price" => 8490,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "VITCHX0026",
                "fixed_price" => 5490,
                "advance_price" => 4990,
            ),
            array(
                "product_code" => "VITANX0031",
                "fixed_price" => 23390,
                "advance_price" => 21390,
            ),
            array(
                "product_code" => "VITBJX0032",
                "fixed_price" => 16290,
                "advance_price" => 14990,
            ),
            array(
                "product_code" => "VITLIX0033",
                "fixed_price" => 10590,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "VITLIX0034",
                "fixed_price" => 10190,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "VITENX0035",
                "fixed_price" => 14690,
                "advance_price" => 13390,
            ),
            array(
                "product_code" => "VITENX0037",
                "fixed_price" => 23390,
                "advance_price" => 21290,
            ),
            array(
                "product_code" => "VITENX0038",
                "fixed_price" => 15090,
                "advance_price" => 13690,
            ),
            array(
                "product_code" => "VITANX0039",
                "fixed_price" => 18690,
                "advance_price" => 17090,
            ),
            array(
                "product_code" => "VITENX0040",
                "fixed_price" => 9390,
                "advance_price" => 8590,
            ),
            array(
                "product_code" => "VITENX0041",
                "fixed_price" => 6290,
                "advance_price" => 5890,
            ),
            array(
                "product_code" => "VITENX0042",
                "fixed_price" => 30290,
                "advance_price" => 27190,
            ),
            array(
                "product_code" => "VITCLX0043",
                "fixed_price" => 11990,
                "advance_price" => 11590,
            ),
            array(
                "product_code" => "VITSKX0044",
                "fixed_price" => 20300,
                "advance_price" => 18390,
            ),
            array(
                "product_code" => "VITENX0045",
                "fixed_price" => 21500,
                "advance_price" => 19490,
            ),
            array(
                "product_code" => "VITBJX0046",
                "fixed_price" => 8390,
                "advance_price" => 7790,
            ),
            array(
                "product_code" => "VITSKX0047",
                "fixed_price" => 17790,
                "advance_price" => 12690,
            ),
            array(
                "product_code" => "VITMEX0048",
                "fixed_price" => 14590,
                "advance_price" => 13290,
            ),
            array(
                "product_code" => "VITENX0049",
                "fixed_price" => 18290,
                "advance_price" => 16990,
            ),
            array(
                "product_code" => "VITMEX0050",
                "fixed_price" => 27890,
                "advance_price" => 25390,
            ),
            array(
                "product_code" => "VITENX0051",
                "fixed_price" => 8790,
                "advance_price" => 7890,
            ),
            array(
                "product_code" => "VITKYX0053",
                "fixed_price" => 18390,
                "advance_price" => 17690,
            ),
            array(
                "product_code" => "VITENX0054",
                "fixed_price" => 33990,
                "advance_price" => 31090,
            ),
            array(
                "product_code" => "VITVIX0055",
                "fixed_price" => 20390,
                "advance_price" => 18390,
            ),
            array(
                "product_code" => "VITBJX0056",
                "fixed_price" => 27490,
                "advance_price" => 24990,
            ),
            array(
                "product_code" => "VITLIX0057",
                "fixed_price" => 30190,
                "advance_price" => 27190,
            ),
            array(
                "product_code" => "VITANX0059",
                "fixed_price" => 15290,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "VITENX0060",
                "fixed_price" => 12090,
                "advance_price" => 10990,
            ),
            array(
                "product_code" => "VITENX0063",
                "fixed_price" => 10590,
                "advance_price" => 9790,
            ),
            array(
                "product_code" => "VITSTX0064",
                "fixed_price" => 24390,
                "advance_price" => 21990,
            ),
            array(
                "product_code" => "VITENX0066",
                "fixed_price" => 16190,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "VITHEX0067",
                "fixed_price" => 19290,
                "advance_price" => 17090,
            ),
            array(
                "product_code" => "LRPFWX0001",
                "fixed_price" => 10190,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "LRPFWX0002",
                "fixed_price" => 2990,
                "advance_price" => 2790,
            ),
            array(
                "product_code" => "LRPFWX0003",
                "fixed_price" => 8190,
                "advance_price" => 7390,
            ),
            array(
                "product_code" => "LRPFWX0005",
                "fixed_price" => 3490,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "LRPFWX0006",
                "fixed_price" => 7090,
                "advance_price" => 6890,
            ),
            array(
                "product_code" => "LRPBOX0007",
                "fixed_price" => 10190,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "LRPBOX0008",
                "fixed_price" => 10190,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "LRPFWX0009",
                "fixed_price" => 8090,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "LRPFWX0010",
                "fixed_price" => 17290,
                "advance_price" => 16290,
            ),
            array(
                "product_code" => "LRPSKX0011",
                "fixed_price" => 5990,
                "advance_price" => 5790,
            ),
            array(
                "product_code" => "DMVBOX0001",
                "fixed_price" => 6390,
                "advance_price" => 5790,
            ),
            array(
                "product_code" => "DMVBWX0002",
                "fixed_price" => 6090,
                "advance_price" => 5690,
            ),
            array(
                "product_code" => "DMVBWX0003",
                "fixed_price" => 6290,
                "advance_price" => 5790,
            ),
            array(
                "product_code" => "DMVBOX0004",
                "fixed_price" => 3690,
                "advance_price" => 3390,
            ),
            array(
                "product_code" => "DMVHWX0005",
                "fixed_price" => 6290,
                "advance_price" => 5790,
            ),
            array(
                "product_code" => "DMVHWX0006",
                "fixed_price" => 3790,
                "advance_price" => 3390,
            ),
            array(
                "product_code" => "SBXIMX0001",
                "fixed_price" => 5690,
                "advance_price" => 5090,
            ),
            array(
                "product_code" => "SBXENX0002",
                "fixed_price" => 8690,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "SBXENX0003",
                "fixed_price" => 13890,
                "advance_price" => 12290,
            ),
            array(
                "product_code" => "SBXHEX0004",
                "fixed_price" => 8690,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "SBXHEX0005",
                "fixed_price" => 14200,
                "advance_price" => 12690,
            ),
            array(
                "product_code" => "SBXHEX0006",
                "fixed_price" => 21990,
                "advance_price" => 19590,
            ),
            array(
                "product_code" => "SBXWOX0007",
                "fixed_price" => 2469,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "SBXSKX0008",
                "fixed_price" => 6190,
                "advance_price" => 5590,
            ),
            array(
                "product_code" => "SBXSKX0009",
                "fixed_price" => 9890,
                "advance_price" => 8890,
            ),
            array(
                "product_code" => "SBXSKX0010",
                "fixed_price" => 13990,
                "advance_price" => 12690,
            ),
            array(
                "product_code" => "SBXBJX0011",
                "fixed_price" => 4090,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "SBXBJX0012",
                "fixed_price" => 6790,
                "advance_price" => 6290,
            ),
            array(
                "product_code" => "SBXBJX0013",
                "fixed_price" => 11390,
                "advance_price" => 10190,
            ),
            array(
                "product_code" => "SBXWOX0014",
                "fixed_price" => 4490,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "SBXWOX0015",
                "fixed_price" => 7490,
                "advance_price" => 6690,
            ),
            array(
                "product_code" => "SBXWOX0016",
                "fixed_price" => 15090,
                "advance_price" => 13390,
            ),
            array(
                "product_code" => "SBXVIX0020",
                "fixed_price" => 16690,
                "advance_price" => 14890,
            ),
            array(
                "product_code" => "SBXSTX0021",
                "fixed_price" => 4990,
                "advance_price" => 4590,
            ),
            array(
                "product_code" => "SBXHEX0022",
                "fixed_price" => 6690,
                "advance_price" => 5990,
            ),
            array(
                "product_code" => "SBXHEX0023",
                "fixed_price" => 12100,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "BLMANX0001",
                "fixed_price" => 7590,
                "advance_price" => 6690,
            ),
            array(
                "product_code" => "BLMANX0002",
                "fixed_price" => 18890,
                "advance_price" => 16690,
            ),
            array(
                "product_code" => "BLMIMX0004",
                "fixed_price" => 9090,
                "advance_price" => 7890,
            ),
            array(
                "product_code" => "BLMIMX0005",
                "fixed_price" => 15890,
                "advance_price" => 13690,
            ),
            array(
                "product_code" => "BLMANX0006",
                "fixed_price" => 7490,
                "advance_price" => 6590,
            ),
            array(
                "product_code" => "BLMSKX0008",
                "fixed_price" => 6000,
                "advance_price" => 5290,
            ),
            array(
                "product_code" => "BLMHEX0015",
                "fixed_price" => 8100,
                "advance_price" => 6990,
            ),
            array(
                "product_code" => "BLMHEX0016",
                "fixed_price" => 14190,
                "advance_price" => 12190,
            ),
            array(
                "product_code" => "BLMWOX0020",
                "fixed_price" => 19390,
                "advance_price" => 17390,
            ),
            array(
                "product_code" => "BLMSTX0021",
                "fixed_price" => 10590,
                "advance_price" => 9390,
            ),
            array(
                "product_code" => "BLMMOX0022",
                "fixed_price" => 17200,
                "advance_price" => 14890,
            ),
            array(
                "product_code" => "BLMENX0025",
                "fixed_price" => 9490,
                "advance_price" => 8290,
            ),
            array(
                "product_code" => "BLMWOX0026",
                "fixed_price" => 10090,
                "advance_price" => 8890,
            ),
            array(
                "product_code" => "BLMWOX0027",
                "fixed_price" => 10890,
                "advance_price" => 9690,
            ),
            array(
                "product_code" => "BLMHEX0034",
                "fixed_price" => 26690,
                "advance_price" => 23390,
            ),
            array(
                "product_code" => "BLMMOX0035",
                "fixed_price" => 3090,
                "advance_price" => 2590,
            ),
            array(
                "product_code" => "BLMBRX0037",
                "fixed_price" => 5790,
                "advance_price" => 5090,
            ),
            array(
                "product_code" => "BLMBRX0038",
                "fixed_price" => 18090,
                "advance_price" => 15890,
            ),
            array(
                "product_code" => "BLMLUX0041",
                "fixed_price" => 5990,
                "advance_price" => 5190,
            ),
            array(
                "product_code" => "BLMENX0042",
                "fixed_price" => 5990,
                "advance_price" => 5190,
            ),
            array(
                "product_code" => "BLMVIX0043",
                "fixed_price" => 7390,
                "advance_price" => 6390,
            ),
            array(
                "product_code" => "BLMVIX0044",
                "fixed_price" => 12390,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "BLMMSX0045",
                "fixed_price" => 4690,
                "advance_price" => 4190,
            ),
            array(
                "product_code" => "BLMWOX0054",
                "fixed_price" => 9490,
                "advance_price" => 8190,
            ),
            array(
                "product_code" => "BLMMEX0055",
                "fixed_price" => 10100,
                "advance_price" => 8990,
            ),
            array(
                "product_code" => "BLMIMX0059",
                "fixed_price" => 4090,
                "advance_price" => 2990,
            ),
            array(
                "product_code" => "BLMIMX0060",
                "fixed_price" => 7090,
                "advance_price" => 6290,
            ),
            array(
                "product_code" => "BLMBJX0014",
                "fixed_price" => 9590,
                "advance_price" => 8290,
            ),
            array(
                "product_code" => "BLMREX0031",
                "fixed_price" => 14690,
                "advance_price" => 13290,
            ),
            array(
                "product_code" => "BLMENX0051",
                "fixed_price" => 16290,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "abc",
                "fixed_price" => 100,
                "advance_price" => 100,
            ),
            array(
                "product_code" => "NOVENX0001",
                "fixed_price" => 6790,
                "advance_price" => 6190,
            ),
            array(
                "product_code" => "NOVBRX0002",
                "fixed_price" => 12890,
                "advance_price" => 11690,
            ),
            array(
                "product_code" => "NOVVIX0003",
                "fixed_price" => 10690,
                "advance_price" => 9690,
            ),
            array(
                "product_code" => "NOVIMX0004",
                "fixed_price" => 9890,
                "advance_price" => 8990,
            ),
            array(
                "product_code" => "NOVBRX0005",
                "fixed_price" => 11690,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "NOVLUX0006",
                "fixed_price" => 5790,
                "advance_price" => 5290,
            ),
            array(
                "product_code" => "NOVHEX0007",
                "fixed_price" => 9190,
                "advance_price" => 8390,
            ),
            array(
                "product_code" => "NOVBJX0008",
                "fixed_price" => 5190,
                "advance_price" => 4590,
            ),
            array(
                "product_code" => "NOVBJX0009",
                "fixed_price" => 7890,
                "advance_price" => 7090,
            ),
            array(
                "product_code" => "NOVWOX0010",
                "fixed_price" => 9590,
                "advance_price" => 8690,
            ),
            array(
                "product_code" => "NOVLUX0011",
                "fixed_price" => 14890,
                "advance_price" => 13590,
            ),
            array(
                "product_code" => "NOVBSX0012",
                "fixed_price" => 9190,
                "advance_price" => 8390,
            ),
            array(
                "product_code" => "NOVWOX0013",
                "fixed_price" => 6990,
                "advance_price" => 6390,
            ),
            array(
                "product_code" => "NOVHEX0014",
                "fixed_price" => 25990,
                "advance_price" => 23390,
            ),
            array(
                "product_code" => "NOVHEX0015",
                "fixed_price" => 8090,
                "advance_price" => 7390,
            ),
            array(
                "product_code" => "NOVLIX0016",
                "fixed_price" => 14390,
                "advance_price" => 12990,
            ),
            array(
                "product_code" => "NOVENX0017",
                "fixed_price" => 4090,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "NOVHEX0018",
                "fixed_price" => 7490,
                "advance_price" => 6890,
            ),
            array(
                "product_code" => "NOVBRX0019",
                "fixed_price" => 9090,
                "advance_price" => 8190,
            ),
            array(
                "product_code" => "NOVBRX0020",
                "fixed_price" => 4490,
                "advance_price" => 4190,
            ),
            array(
                "product_code" => "NOVBJX0021",
                "fixed_price" => 7090,
                "advance_price" => 6390,
            ),
            array(
                "product_code" => "NOVBSX0023",
                "fixed_price" => 9490,
                "advance_price" => 8590,
            ),
            array(
                "product_code" => "NOVLIX0024",
                "fixed_price" => 7490,
                "advance_price" => 6990,
            ),
            array(
                "product_code" => "NOVHEX0025",
                "fixed_price" => 4590,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "NOVBSX0026",
                "fixed_price" => 19390,
                "advance_price" => 17390,
            ),
            array(
                "product_code" => "NOVIMX0027",
                "fixed_price" => 30290,
                "advance_price" => 27590,
            ),
            array(
                "product_code" => "NOVBRX0028",
                "fixed_price" => 19990,
                "advance_price" => 18190,
            ),
            array(
                "product_code" => "NOVENX0029",
                "fixed_price" => 8190,
                "advance_price" => 7390,
            ),
            array(
                "product_code" => "NOVENX0030",
                "fixed_price" => 21290,
                "advance_price" => 19290,
            ),
            array(
                "product_code" => "NOVBJX0031",
                "fixed_price" => 10790,
                "advance_price" => 9890,
            ),
            array(
                "product_code" => "NOVMEX0032",
                "fixed_price" => 7290,
                "advance_price" => 6690,
            ),
            array(
                "product_code" => "NOVSTX0033",
                "fixed_price" => 19290,
                "advance_price" => 17390,
            ),
            array(
                "product_code" => "NOVHEX0034",
                "fixed_price" => 13190,
                "advance_price" => 11990,
            ),
            array(
                "product_code" => "NOVVIX0035",
                "fixed_price" => 9490,
                "advance_price" => 8590,
            ),
            array(
                "product_code" => "NOVWOX0036",
                "fixed_price" => 4690,
                "advance_price" => 4190,
            ),
            array(
                "product_code" => "NOVWEX0037",
                "fixed_price" => 10390,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "NOVWOX0038",
                "fixed_price" => 10190,
                "advance_price" => 9390,
            ),
            array(
                "product_code" => "NOVENX0039",
                "fixed_price" => 7890,
                "advance_price" => 6990,
            ),
            array(
                "product_code" => "NOVVIX0040",
                "fixed_price" => 15990,
                "advance_price" => 14590,
            ),
            array(
                "product_code" => "NOVIMX0041",
                "fixed_price" => 4390,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "NOVBJX0042",
                "fixed_price" => 9090,
                "advance_price" => 8190,
            ),
            array(
                "product_code" => "NOVBRX0044",
                "fixed_price" => 6290,
                "advance_price" => 5890,
            ),
            array(
                "product_code" => "NOVCHX0045",
                "fixed_price" => 8590,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "NOVBRX0046",
                "fixed_price" => 5290,
                "advance_price" => 4890,
            ),
            array(
                "product_code" => "NOVIMX0047",
                "fixed_price" => 5690,
                "advance_price" => 5290,
            ),
            array(
                "product_code" => "NOVHEX0048",
                "fixed_price" => 6290,
                "advance_price" => 5890,
            ),
            array(
                "product_code" => "SK8BWX0001",
                "fixed_price" => 1439,
                "advance_price" => 1390,
            ),
            array(
                "product_code" => "SK8BWX0002",
                "fixed_price" => 4090,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "SK8BWX0003",
                "fixed_price" => 4390,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "SK8HWX0004",
                "fixed_price" => 3790,
                "advance_price" => 3490,
            ),
            array(
                "product_code" => "SK8SKX0005",
                "fixed_price" => 4090,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "SK8IWX0006",
                "fixed_price" => 2790,
                "advance_price" => 2590,
            ),
            array(
                "product_code" => "NTFENX0004",
                "fixed_price" => 12390,
                "advance_price" => 11290,
            ),
            array(
                "product_code" => "NTFENX0005",
                "fixed_price" => 12090,
                "advance_price" => 10990,
            ),
            array(
                "product_code" => "NTFENX0006",
                "fixed_price" => 15690,
                "advance_price" => 14290,
            ),
            array(
                "product_code" => "NTFENX0007",
                "fixed_price" => 23890,
                "advance_price" => 21690,
            ),
            array(
                "product_code" => "NTFENX0008",
                "fixed_price" => 15190,
                "advance_price" => 14590,
            ),
            array(
                "product_code" => "NTFENX0009",
                "fixed_price" => 11390,
                "advance_price" => 10290,
            ),
            array(
                "product_code" => "NTFENX0010",
                "fixed_price" => 13190,
                "advance_price" => 11890,
            ),
            array(
                "product_code" => "NTFENX0011",
                "fixed_price" => 17190,
                "advance_price" => 15490,
            ),
            array(
                "product_code" => "NTFENX0012",
                "fixed_price" => 11490,
                "advance_price" => 10390,
            ),
            array(
                "product_code" => "NTFENX0013",
                "fixed_price" => 17490,
                "advance_price" => 15890,
            ),
            array(
                "product_code" => "NTFENX0014",
                "fixed_price" => 9590,
                "advance_price" => 8590,
            ),
            array(
                "product_code" => "NTFENX0015",
                "fixed_price" => 6990,
                "advance_price" => 6290,
            ),
            array(
                "product_code" => "NTFENX0016",
                "fixed_price" => 10890,
                "advance_price" => 9690,
            ),
            array(
                "product_code" => "NTFENX0017",
                "fixed_price" => 18290,
                "advance_price" => 16390,
            ),
            array(
                "product_code" => "NTFENX0019",
                "fixed_price" => 13200,
                "advance_price" => 11890,
            ),
            array(
                "product_code" => "KDLCHX0066",
                "fixed_price" => 4090,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "KDLCHX0067",
                "fixed_price" => 3790,
                "advance_price" => 3390,
            ),
            array(
                "product_code" => "KDLCHX0068",
                "fixed_price" => 15190,
                "advance_price" => 13890,
            ),
            array(
                "product_code" => "SWSENX0001",
                "fixed_price" => 5290,
                "advance_price" => 4890,
            ),
            array(
                "product_code" => "SWSENX0002",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "SWSWEX0003",
                "fixed_price" => 12090,
                "advance_price" => 11290,
            ),
            array(
                "product_code" => "SWSENX0004",
                "fixed_price" => 12090,
                "advance_price" => 11290,
            ),
            array(
                "product_code" => "SWSENX0005",
                "fixed_price" => 8100,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "SWSENX0007",
                "fixed_price" => 10100,
                "advance_price" => 9590,
            ),
            array(
                "product_code" => "SWSENX0009",
                "fixed_price" => 4590,
                "advance_price" => 4390,
            ),
            array(
                "product_code" => "SWSENX0010",
                "fixed_price" => 6990,
                "advance_price" => 6590,
            ),
            array(
                "product_code" => "SWSENX0011",
                "fixed_price" => 819,
                "advance_price" => 759,
            ),
            array(
                "product_code" => "SWSENX0012",
                "fixed_price" => 6990,
                "advance_price" => 6590,
            ),
            array(
                "product_code" => "SWSENX0013",
                "fixed_price" => 819,
                "advance_price" => 759,
            ),
            array(
                "product_code" => "SWSENX0014",
                "fixed_price" => 6990,
                "advance_price" => 6590,
            ),
            array(
                "product_code" => "SWSENX0015",
                "fixed_price" => 819,
                "advance_price" => 759,
            ),
            array(
                "product_code" => "SWSSKX0016",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "SWSSKX0017",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "SWSSKX0018",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "SWSSKX0019",
                "fixed_price" => 4690,
                "advance_price" => 4490,
            ),
            array(
                "product_code" => "SWSSKX0020",
                "fixed_price" => 7390,
                "advance_price" => 6890,
            ),
            array(
                "product_code" => "CLLANX0002",
                "fixed_price" => 15290,
                "advance_price" => 13890,
            ),
            array(
                "product_code" => "CLLHEX0003",
                "fixed_price" => 30500,
                "advance_price" => 27900,
            ),
            array(
                "product_code" => "CLLIMX0004",
                "fixed_price" => 16190,
                "advance_price" => 15290,
            ),
            array(
                "product_code" => "CLLBRX0005",
                "fixed_price" => 20300,
                "advance_price" => 18190,
            ),
            array(
                "product_code" => "CLLIMX0006",
                "fixed_price" => 13090,
                "advance_price" => 11590,
            ),
            array(
                "product_code" => "CLLIMX0007",
                "fixed_price" => 21290,
                "advance_price" => 18990,
            ),
            array(
                "product_code" => "BIGINX0002",
                "fixed_price" => 17790,
                "advance_price" => 16990,
            ),
            array(
                "product_code" => "BIGSTX0003",
                "fixed_price" => 9290,
                "advance_price" => 8890,
            ),
            array(
                "product_code" => "NTWCHX0002",
                "fixed_price" => 9190,
                "advance_price" => 8590,
            ),
            array(
                "product_code" => "NTWCHX0003",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWCHX0004",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "NTWCHX0005",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWCHX0006",
                "fixed_price" => 4090,
                "advance_price" => 3790,
            ),
            array(
                "product_code" => "NTWCHX0007",
                "fixed_price" => 4090,
                "advance_price" => 3790,
            ),
            array(
                "product_code" => "NTWCHX0008",
                "fixed_price" => 4090,
                "advance_price" => 3790,
            ),
            array(
                "product_code" => "NTWCHX0009",
                "fixed_price" => 7790,
                "advance_price" => 7290,
            ),
            array(
                "product_code" => "NTWENX0010",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWENX0011",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWENX0012",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWENX0013",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWENX0014",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWENX0016",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWENX0019",
                "fixed_price" => 7490,
                "advance_price" => 7090,
            ),
            array(
                "product_code" => "NTWBRX0020",
                "fixed_price" => 5090,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "NTWIMX0022",
                "fixed_price" => 14890,
                "advance_price" => 13890,
            ),
            array(
                "product_code" => "NTWIMX0023",
                "fixed_price" => 25400,
                "advance_price" => 22690,
            ),
            array(
                "product_code" => "NTWIMX0024",
                "fixed_price" => 15200,
                "advance_price" => 14290,
            ),
            array(
                "product_code" => "NTWIMX0025",
                "fixed_price" => 25400,
                "advance_price" => 23690,
            ),
            array(
                "product_code" => "BINANX0001",
                "fixed_price" => 15290,
                "advance_price" => 14290,
            ),
            array(
                "product_code" => "QVABOX0001",
                "fixed_price" => 3990,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "QVABOX0002",
                "fixed_price" => 8290,
                "advance_price" => 7890,
            ),
            array(
                "product_code" => "QVABOX0003",
                "fixed_price" => 12090,
                "advance_price" => 11590,
            ),
            array(
                "product_code" => "QVABOX0004",
                "fixed_price" => 19390,
                "advance_price" => 18590,
            ),
            array(
                "product_code" => "QVABOX0005",
                "fixed_price" => 3790,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "QVABOX0006",
                "fixed_price" => 6390,
                "advance_price" => 6090,
            ),
            array(
                "product_code" => "QVABOX0007",
                "fixed_price" => 10190,
                "advance_price" => 9790,
            ),
            array(
                "product_code" => "QVABWX0008",
                "fixed_price" => 3190,
                "advance_price" => 2890,
            ),
            array(
                "product_code" => "QVABWX0009",
                "fixed_price" => 5490,
                "advance_price" => 5290,
            ),
            array(
                "product_code" => "QVABWX0010",
                "fixed_price" => 9190,
                "advance_price" => 8690,
            ),
            array(
                "product_code" => "QVABWX0011",
                "fixed_price" => 4590,
                "advance_price" => 4390,
            ),
            array(
                "product_code" => "QVABWX0012",
                "fixed_price" => 4090,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "QVABWX0013",
                "fixed_price" => 7190,
                "advance_price" => 6890,
            ),
            array(
                "product_code" => "QVAINX0014",
                "fixed_price" => 2490,
                "advance_price" => 2290,
            ),
            array(
                "product_code" => "QVAINX0015",
                "fixed_price" => 4590,
                "advance_price" => 4390,
            ),
            array(
                "product_code" => "QVAINX0016",
                "fixed_price" => 3490,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "QVAINX0017",
                "fixed_price" => 5790,
                "advance_price" => 5590,
            ),
            array(
                "product_code" => "QVAINX0018",
                "fixed_price" => 4290,
                "advance_price" => 4090,
            ),
            array(
                "product_code" => "QVAINX0019",
                "fixed_price" => 4290,
                "advance_price" => 4090,
            ),
            array(
                "product_code" => "QVAINX0020",
                "fixed_price" => 8790,
                "advance_price" => 8190,
            ),
            array(
                "product_code" => "QVAINX0021",
                "fixed_price" => 3990,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "QVAINX0022",
                "fixed_price" => 12090,
                "advance_price" => 11590,
            ),
            array(
                "product_code" => "QVAINX0023",
                "fixed_price" => 4190,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "QVABWX0024",
                "fixed_price" => 7190,
                "advance_price" => 6890,
            ),
            array(
                "product_code" => "QVABWX0025",
                "fixed_price" => 12100,
                "advance_price" => 11190,
            ),
            array(
                "product_code" => "QVABOX0026",
                "fixed_price" => 4990,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "QVABOX0027",
                "fixed_price" => 15190,
                "advance_price" => 14590,
            ),
            array(
                "product_code" => "QVABOX0028",
                "fixed_price" => 5490,
                "advance_price" => 5190,
            ),
            array(
                "product_code" => "QVABWX0029",
                "fixed_price" => 4990,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "QVABWX0030",
                "fixed_price" => 4990,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "QVABOX0031",
                "fixed_price" => 4490,
                "advance_price" => 4190,
            ),
            array(
                "product_code" => "QVABOX0032",
                "fixed_price" => 4490,
                "advance_price" => 4190,
            ),
            array(
                "product_code" => "QVABOX0033",
                "fixed_price" => 5790,
                "advance_price" => 5490,
            ),
            array(
                "product_code" => "QVAFWX0034",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "QVABOX0035",
                "fixed_price" => 6390,
                "advance_price" => 6090,
            ),
            array(
                "product_code" => "QVABOX0036",
                "fixed_price" => 3090,
                "advance_price" => 2890,
            ),
            array(
                "product_code" => "QVABOX0037",
                "fixed_price" => 1949,
                "advance_price" => 1790,
            ),
            array(
                "product_code" => "QVAHWX0038",
                "fixed_price" => 5790,
                "advance_price" => 5390,
            ),
            array(
                "product_code" => "QVAHWX0039",
                "fixed_price" => 5690,
                "advance_price" => 5490,
            ),
            array(
                "product_code" => "QVAHWX0040",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "KDLBJX0001",
                "fixed_price" => 5190,
                "advance_price" => 4790,
            ),
            array(
                "product_code" => "KDLBJX0002",
                "fixed_price" => 7190,
                "advance_price" => 6590,
            ),
            array(
                "product_code" => "KDLBJX0003",
                "fixed_price" => 8890,
                "advance_price" => 7990,
            ),
            array(
                "product_code" => "KDLBJX0004",
                "fixed_price" => 26990,
                "advance_price" => 24390,
            ),
            array(
                "product_code" => "KDLBJX0005",
                "fixed_price" => 29590,
                "advance_price" => 26690,
            ),
            array(
                "product_code" => "KDLBJX0006",
                "fixed_price" => 12090,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "KDLBJX0007",
                "fixed_price" => 41390,
                "advance_price" => 37390,
            ),
            array(
                "product_code" => "KDLBJX0008",
                "fixed_price" => 11490,
                "advance_price" => 10590,
            ),
            array(
                "product_code" => "KDLBJX0009",
                "fixed_price" => 8690,
                "advance_price" => 7890,
            ),
            array(
                "product_code" => "KDLBJX0010",
                "fixed_price" => 7790,
                "advance_price" => 7190,
            ),
            array(
                "product_code" => "KDLBJX0011",
                "fixed_price" => 12090,
                "advance_price" => 11190,
            ),
            array(
                "product_code" => "KDLBRX0012",
                "fixed_price" => 12090,
                "advance_price" => 10390,
            ),
            array(
                "product_code" => "KDLWEX0013",
                "fixed_price" => 16590,
                "advance_price" => 15090,
            ),
            array(
                "product_code" => "KDLWEX0014",
                "fixed_price" => 4990,
                "advance_price" => 4490,
            ),
            array(
                "product_code" => "KDLLIX0015",
                "fixed_price" => 14390,
                "advance_price" => 13290,
            ),
            array(
                "product_code" => "KDLREX0016",
                "fixed_price" => 20790,
                "advance_price" => 18890,
            ),
            array(
                "product_code" => "KDLREX0017",
                "fixed_price" => 5000,
                "advance_price" => 4590,
            ),
            array(
                "product_code" => "KDLVIX0018",
                "fixed_price" => 9690,
                "advance_price" => 8690,
            ),
            array(
                "product_code" => "KDLIMX0019",
                "fixed_price" => 6290,
                "advance_price" => 5690,
            ),
            array(
                "product_code" => "KDLIMX0020",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "KDLIMX0021",
                "fixed_price" => 18890,
                "advance_price" => 17190,
            ),
            array(
                "product_code" => "KDLIMX0022",
                "fixed_price" => 3390,
                "advance_price" => 3090,
            ),
            array(
                "product_code" => "KDLIMX0023",
                "fixed_price" => 6290,
                "advance_price" => 5790,
            ),
            array(
                "product_code" => "KDLIMX0025",
                "fixed_price" => 4290,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "KDLIMX0026",
                "fixed_price" => 4290,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "KDLENX0027",
                "fixed_price" => 10890,
                "advance_price" => 9990,
            ),
            array(
                "product_code" => "KDLENX0028",
                "fixed_price" => 14990,
                "advance_price" => 13690,
            ),
            array(
                "product_code" => "KDLENX0029",
                "fixed_price" => 8190,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "KDLENX0030",
                "fixed_price" => 11290,
                "advance_price" => 10290,
            ),
            array(
                "product_code" => "KDLENX0031",
                "fixed_price" => 11390,
                "advance_price" => 10390,
            ),
            array(
                "product_code" => "KDLENX0032",
                "fixed_price" => 21090,
                "advance_price" => 19390,
            ),
            array(
                "product_code" => "KDLENX0033",
                "fixed_price" => 5490,
                "advance_price" => 5090,
            ),
            array(
                "product_code" => "KDLENX0034",
                "fixed_price" => 21690,
                "advance_price" => 19690,
            ),
            array(
                "product_code" => "KDLHWX0035",
                "fixed_price" => 15890,
                "advance_price" => 14890,
            ),
            array(
                "product_code" => "KDLHWX0036",
                "fixed_price" => 4090,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "KDLHWX0037",
                "fixed_price" => 4790,
                "advance_price" => 4390,
            ),
            array(
                "product_code" => "KDLHWX0038",
                "fixed_price" => 6690,
                "advance_price" => 5990,
            ),
            array(
                "product_code" => "KDLHWX0039",
                "fixed_price" => 17290,
                "advance_price" => 15890,
            ),
            array(
                "product_code" => "KDLHWX0040",
                "fixed_price" => 11890,
                "advance_price" => 10990,
            ),
            array(
                "product_code" => "KDLHWX0041",
                "fixed_price" => 24490,
                "advance_price" => 22990,
            ),
            array(
                "product_code" => "KDLHWX0042",
                "fixed_price" => 3790,
                "advance_price" => 3390,
            ),
            array(
                "product_code" => "KDLHWX0043",
                "fixed_price" => 12390,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "KDLBSX0044",
                "fixed_price" => 10190,
                "advance_price" => 9390,
            ),
            array(
                "product_code" => "KDLBSX0045",
                "fixed_price" => 18090,
                "advance_price" => 16390,
            ),
            array(
                "product_code" => "KDLBSX0046",
                "fixed_price" => 16890,
                "advance_price" => 16390,
            ),
            array(
                "product_code" => "KDLBSX0047",
                "fixed_price" => 13990,
                "advance_price" => 12690,
            ),
            array(
                "product_code" => "KDLBSX0048",
                "fixed_price" => 9190,
                "advance_price" => 8390,
            ),
            array(
                "product_code" => "KDLANX0049",
                "fixed_price" => 11190,
                "advance_price" => 10390,
            ),
            array(
                "product_code" => "KDLANX0050",
                "fixed_price" => 11590,
                "advance_price" => 10590,
            ),
            array(
                "product_code" => "KDLANX0051",
                "fixed_price" => 12590,
                "advance_price" => 11290,
            ),
            array(
                "product_code" => "KDLANX0052",
                "fixed_price" => 21590,
                "advance_price" => 19690,
            ),
            array(
                "product_code" => "KDLWOX0053",
                "fixed_price" => 6790,
                "advance_price" => 6290,
            ),
            array(
                "product_code" => "KDLSKX0054",
                "fixed_price" => 19290,
                "advance_price" => 17890,
            ),
            array(
                "product_code" => "KDLHWX0055",
                "fixed_price" => 12190,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "KDLSKX0056",
                "fixed_price" => 8190,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "KDLSKX0057",
                "fixed_price" => 12190,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "KDLSKX0058",
                "fixed_price" => 6000,
                "advance_price" => 5690,
            ),
            array(
                "product_code" => "KDLSTX0059",
                "fixed_price" => 7090,
                "advance_price" => 6390,
            ),
            array(
                "product_code" => "KDLSTX0060",
                "fixed_price" => 10090,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "KDLINX0061",
                "fixed_price" => 15190,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "KDLCHX0062",
                "fixed_price" => 4090,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "KDLCHX0063",
                "fixed_price" => 3790,
                "advance_price" => 3390,
            ),
            array(
                "product_code" => "KDLCHX0064",
                "fixed_price" => 15190,
                "advance_price" => 13890,
            ),
            array(
                "product_code" => "NVTWOX0001",
                "fixed_price" => 18300,
                "advance_price" => 17190,
            ),
            array(
                "product_code" => "NVTWOX0002",
                "fixed_price" => 18300,
                "advance_price" => 17190,
            ),
            array(
                "product_code" => "NVTENX0004",
                "fixed_price" => 18290,
                "advance_price" => 16990,
            ),
            array(
                "product_code" => "NVTANX0006",
                "fixed_price" => 48290,
                "advance_price" => 45990,
            ),
            array(
                "product_code" => "NVTENX0007",
                "fixed_price" => 14590,
                "advance_price" => 13590,
            ),
            array(
                "product_code" => "NVTENX0011",
                "fixed_price" => 16290,
                "advance_price" => 15390,
            ),
            array(
                "product_code" => "NVTHEX0013",
                "fixed_price" => 12890,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "NVTHEX0015",
                "fixed_price" => 33600,
                "advance_price" => 31390,
            ),
            array(
                "product_code" => "BVSKYX0001",
                "fixed_price" => 10100,
                "advance_price" => 8990,
            ),
            array(
                "product_code" => "BVSIMX0002",
                "fixed_price" => 10100,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "BVSBJX0003",
                "fixed_price" => 10100,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "BVSLIX0004",
                "fixed_price" => 10100,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "BVSBSX0005",
                "fixed_price" => 10100,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "BVSHEX0006",
                "fixed_price" => 10100,
                "advance_price" => 9190,
            ),
            array(
                "product_code" => "BVSMEX0007",
                "fixed_price" => 10100,
                "advance_price" => 8990,
            ),
            array(
                "product_code" => "BVSWOX0008",
                "fixed_price" => 8190,
                "advance_price" => 7290,
            ),
            array(
                "product_code" => "BVSBJX0009",
                "fixed_price" => 12100,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "RSKBOX0006",
                "fixed_price" => 1749,
                "advance_price" => 1590,
            ),
            array(
                "product_code" => "RSKBOX0007",
                "fixed_price" => 3490,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "RSKBOX0008",
                "fixed_price" => 4490,
                "advance_price" => 4190,
            ),
            array(
                "product_code" => "RSKBWX0012",
                "fixed_price" => 3990,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "RSKSKX0013",
                "fixed_price" => 3090,
                "advance_price" => 2890,
            ),
            array(
                "product_code" => "RSKSKX0014",
                "fixed_price" => 2990,
                "advance_price" => 2790,
            ),
            array(
                "product_code" => "RSKBOX0015",
                "fixed_price" => 4690,
                "advance_price" => 4390,
            ),
            array(
                "product_code" => "RSKBOX0016",
                "fixed_price" => 3490,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "RSKBOX0017",
                "fixed_price" => 2690,
                "advance_price" => 2490,
            ),
            array(
                "product_code" => "RSKBOX0018",
                "fixed_price" => 7790,
                "advance_price" => 7290,
            ),
            array(
                "product_code" => "SLRBJX0001",
                "fixed_price" => 11790,
                "advance_price" => 10390,
            ),
            array(
                "product_code" => "SLRENX0002",
                "fixed_price" => 8890,
                "advance_price" => 7890,
            ),
            array(
                "product_code" => "SLRBSX0003",
                "fixed_price" => 13290,
                "advance_price" => 11890,
            ),
            array(
                "product_code" => "SLRCLX0004",
                "fixed_price" => 6000,
                "advance_price" => 5690,
            ),
            array(
                "product_code" => "SLRKYX0005",
                "fixed_price" => 10990,
                "advance_price" => 9890,
            ),
            array(
                "product_code" => "SLRENX0007",
                "fixed_price" => 11100,
                "advance_price" => 9890,
            ),
            array(
                "product_code" => "SLRENX0009",
                "fixed_price" => 13690,
                "advance_price" => 11990,
            ),
            array(
                "product_code" => "SLRSTX0010",
                "fixed_price" => 15200,
                "advance_price" => 13690,
            ),
            array(
                "product_code" => "SLRMEX0011",
                "fixed_price" => 5590,
                "advance_price" => 4990,
            ),
            array(
                "product_code" => "SLRANX0012",
                "fixed_price" => 9490,
                "advance_price" => 8590,
            ),
            array(
                "product_code" => "SLRLUX0013",
                "fixed_price" => 6990,
                "advance_price" => 5990,
            ),
            array(
                "product_code" => "SLRREX0014",
                "fixed_price" => 7890,
                "advance_price" => 6990,
            ),
            array(
                "product_code" => "SLRWOX0015",
                "fixed_price" => 8190,
                "advance_price" => 7390,
            ),
            array(
                "product_code" => "SLRCLX0016",
                "fixed_price" => 7090,
                "advance_price" => 6590,
            ),
            array(
                "product_code" => "SLRSTX0017",
                "fixed_price" => 8890,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "SLRSTX0018",
                "fixed_price" => 9490,
                "advance_price" => 8390,
            ),
            array(
                "product_code" => "SLRBJX0019",
                "fixed_price" => 7390,
                "advance_price" => 6690,
            ),
            array(
                "product_code" => "SLRVIX0020",
                "fixed_price" => 13590,
                "advance_price" => 12190,
            ),
            array(
                "product_code" => "SLRBPX0021",
                "fixed_price" => 8390,
                "advance_price" => 7390,
            ),
            array(
                "product_code" => "SLRLIX0022",
                "fixed_price" => 14590,
                "advance_price" => 12890,
            ),
            array(
                "product_code" => "THSBRX0002",
                "fixed_price" => 27490,
                "advance_price" => 25990,
            ),
            array(
                "product_code" => "THSBRX0003",
                "fixed_price" => 41190,
                "advance_price" => 38990,
            ),
            array(
                "product_code" => "THSBRX0004",
                "fixed_price" => 76400,
                "advance_price" => 72390,
            ),
            array(
                "product_code" => "THSLIX0005",
                "fixed_price" => 11390,
                "advance_price" => 10890,
            ),
            array(
                "product_code" => "THSLIX0006",
                "fixed_price" => 28500,
                "advance_price" => 26890,
            ),
            array(
                "product_code" => "THSLIX0007",
                "fixed_price" => 54000,
                "advance_price" => 50890,
            ),
            array(
                "product_code" => "THSBJX0008",
                "fixed_price" => 12100,
                "advance_price" => 11590,
            ),
            array(
                "product_code" => "THSBJX0009",
                "fixed_price" => 30500,
                "advance_price" => 28890,
            ),
            array(
                "product_code" => "THSBJX0010",
                "fixed_price" => 45800,
                "advance_price" => 43390,
            ),
            array(
                "product_code" => "THSBRX0011",
                "fixed_price" => 12990,
                "advance_price" => 12590,
            ),
            array(
                "product_code" => "TPMMEX0001",
                "fixed_price" => 24090,
                "advance_price" => 23390,
            ),
            array(
                "product_code" => "TPMMEX0002",
                "fixed_price" => 34190,
                "advance_price" => 32290,
            ),
            array(
                "product_code" => "TPMWOX0003",
                "fixed_price" => 11390,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "TPMWOX0004",
                "fixed_price" => 13490,
                "advance_price" => 12690,
            ),
            array(
                "product_code" => "TPMVIX0005",
                "fixed_price" => 10390,
                "advance_price" => 9890,
            ),
            array(
                "product_code" => "TPMBJX0006",
                "fixed_price" => 12890,
                "advance_price" => 12190,
            ),
            array(
                "product_code" => "TPMMEX0007",
                "fixed_price" => 15390,
                "advance_price" => 14590,
            ),
            array(
                "product_code" => "TPMSTX0008",
                "fixed_price" => 13690,
                "advance_price" => 12190,
            ),
            array(
                "product_code" => "CELBWX0001",
                "fixed_price" => 3490,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "CELBWX0002",
                "fixed_price" => 4090,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "CELBWX0003",
                "fixed_price" => 6590,
                "advance_price" => 6290,
            ),
            array(
                "product_code" => "CELBWX0004",
                "fixed_price" => 12090,
                "advance_price" => 11290,
            ),
            array(
                "product_code" => "CELBWX0005",
                "fixed_price" => 6690,
                "advance_price" => 6190,
            ),
            array(
                "product_code" => "CELBWX0006",
                "fixed_price" => 8490,
                "advance_price" => 7890,
            ),
            array(
                "product_code" => "CELBOX0007",
                "fixed_price" => 12890,
                "advance_price" => 12090,
            ),
            array(
                "product_code" => "CELBOX0008",
                "fixed_price" => 6490,
                "advance_price" => 5890,
            ),
            array(
                "product_code" => "CELBOX0009",
                "fixed_price" => 11290,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "CELBOX0010",
                "fixed_price" => 6390,
                "advance_price" => 5890,
            ),
            array(
                "product_code" => "CELBWX0011",
                "fixed_price" => 3990,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "CELBOX0012",
                "fixed_price" => 5990,
                "advance_price" => 5690,
            ),
            array(
                "product_code" => "CELBOX0013",
                "fixed_price" => 11590,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "CELBOX0014",
                "fixed_price" => 11590,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "CELBWX0015",
                "fixed_price" => 3090,
                "advance_price" => 2890,
            ),
            array(
                "product_code" => "CELBWX0016",
                "fixed_price" => 6390,
                "advance_price" => 5990,
            ),
            array(
                "product_code" => "CELBOX0017",
                "fixed_price" => 8090,
                "advance_price" => 7390,
            ),
            array(
                "product_code" => "CELBOX0018",
                "fixed_price" => 15290,
                "advance_price" => 14890,
            ),
            array(
                "product_code" => "CELBOX0019",
                "fixed_price" => 11190,
                "advance_price" => 10590,
            ),
            array(
                "product_code" => "CELBOX0020",
                "fixed_price" => 7390,
                "advance_price" => 6990,
            ),
            array(
                "product_code" => "CELBOX0021",
                "fixed_price" => 8190,
                "advance_price" => 7690,
            ),
            array(
                "product_code" => "CELINX0022",
                "fixed_price" => 8390,
                "advance_price" => 7790,
            ),
            array(
                "product_code" => "CELINX0023",
                "fixed_price" => 3990,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "CELBWX0024",
                "fixed_price" => 3990,
                "advance_price" => 3690,
            ),
            array(
                "product_code" => "CELINX0025",
                "fixed_price" => 4390,
                "advance_price" => 4090,
            ),
            array(
                "product_code" => "CELINX0026",
                "fixed_price" => 6390,
                "advance_price" => 5890,
            ),
            array(
                "product_code" => "CELBOX0027",
                "fixed_price" => 1959,
                "advance_price" => 1690,
            ),
            array(
                "product_code" => "CELBOX0028",
                "fixed_price" => 3390,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "CELBOX0029",
                "fixed_price" => 9490,
                "advance_price" => 8890,
            ),
            array(
                "product_code" => "CELBOX0030",
                "fixed_price" => 11390,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "SNFENX0001",
                "fixed_price" => 3690,
                "advance_price" => 3490,
            ),
            array(
                "product_code" => "SNFENX0002",
                "fixed_price" => 11890,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "SNFLIX0003",
                "fixed_price" => 8590,
                "advance_price" => 8290,
            ),
            array(
                "product_code" => "SNFSTX0005",
                "fixed_price" => 1149,
                "advance_price" => 990,
            ),
            array(
                "product_code" => "SNFSTX0007",
                "fixed_price" => 1149,
                "advance_price" => 1039,
            ),
            array(
                "product_code" => "SNFSTX0008",
                "fixed_price" => 2049,
                "advance_price" => 1790,
            ),
            array(
                "product_code" => "SNFIWX0010",
                "fixed_price" => 1090,
                "advance_price" => 990,
            ),
            array(
                "product_code" => "SNFIWX0011",
                "fixed_price" => 1090,
                "advance_price" => 990,
            ),
            array(
                "product_code" => "SNFIWX0012",
                "fixed_price" => 1190,
                "advance_price" => 1090,
            ),
            array(
                "product_code" => "SNFIWX0013",
                "fixed_price" => 1190,
                "advance_price" => 1090,
            ),
            array(
                "product_code" => "SNFIWX0014",
                "fixed_price" => 2890,
                "advance_price" => 2690,
            ),
            array(
                "product_code" => "SNFIWX0015",
                "fixed_price" => 2790,
                "advance_price" => 2590,
            ),
            array(
                "product_code" => "SNFIWX0016",
                "fixed_price" => 2790,
                "advance_price" => 2590,
            ),
            array(
                "product_code" => "SNFIWX0017",
                "fixed_price" => 3090,
                "advance_price" => 2890,
            ),
            array(
                "product_code" => "SNFIWX0018",
                "fixed_price" => 3090,
                "advance_price" => 2890,
            ),
            array(
                "product_code" => "FH2006-REG-VD1",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-VD2",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-AD",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-MD",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-SHOP",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "BLMHEX0020",
                "fixed_price" => 19790,
                "advance_price" => 17390,
            ),
            array(
                "product_code" => "FH2006-REG-VDO1",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-AD01",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-MDO1",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "miscellaneous",
                "fixed_price" => 100,
                "advance_price" => 100,
            ),
            array(
                "product_code" => "FH2006-REG-VD1.1",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "FH2006-REG-SHOP.1",
                "fixed_price" => 0,
                "advance_price" => 0,
            ),
            array(
                "product_code" => "KDLHEX0066",
                "fixed_price" => 16190,
                "advance_price" => 14890,
            ),
            array(
                "product_code" => "KDLHEX0067",
                "fixed_price" => 4090,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "KDLHEX0068",
                "fixed_price" => 4790,
                "advance_price" => 4390,
            ),
            array(
                "product_code" => "KDLHEX0069",
                "fixed_price" => 6690,
                "advance_price" => 5990,
            ),
            array(
                "product_code" => "KDLHEX0070",
                "fixed_price" => 17290,
                "advance_price" => 15890,
            ),
            array(
                "product_code" => "KDLHEX0071",
                "fixed_price" => 11890,
                "advance_price" => 10990,
            ),
            array(
                "product_code" => "KDLHEX0072",
                "fixed_price" => 24490,
                "advance_price" => 22990,
            ),
            array(
                "product_code" => "KDLHEX0073",
                "fixed_price" => 3790,
                "advance_price" => 3390,
            ),
            array(
                "product_code" => "KDLHEX0074",
                "fixed_price" => 12390,
                "advance_price" => 11390,
            ),
            array(
                "product_code" => "SWSBJX0021",
                "fixed_price" => 4590,
                "advance_price" => 4090,
            ),
            array(
                "product_code" => "BOXSMT001",
                "fixed_price" => 3990,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "BOXSLM002",
                "fixed_price" => 4090,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "BOXSMT002",
                "fixed_price" => 39900,
                "advance_price" => 36900,
            ),
            array(
                "product_code" => "BOXSLM003",
                "fixed_price" => 61050,
                "advance_price" => 58390,
            ),
            array(
                "product_code" => "BOXSLM004",
                "fixed_price" => 61090,
                "advance_price" => 58390,
            ),
            array(
                "product_code" => "BRTBOX0001",
                "fixed_price" => 3990,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "BRTBOX0002",
                "fixed_price" => 3990,
                "advance_price" => 3990,
            ),
            array(
                "product_code" => "EDSORX0001",
                "fixed_price" => 25400,
                "advance_price" => 23900,
            ),
            array(
                "product_code" => "TRTHEX0001",
                "fixed_price" => 9890,
                "advance_price" => 9290,
            ),
            array(
                "product_code" => "TRTBJX0002",
                "fixed_price" => 12100,
                "advance_price" => 11290,
            ),
            array(
                "product_code" => "TRTKYX0003",
                "fixed_price" => 2839,
                "advance_price" => 2690,
            ),
            array(
                "product_code" => "TRTLUX0004",
                "fixed_price" => 3790,
                "advance_price" => 3590,
            ),
            array(
                "product_code" => "TRTMOX0005",
                "fixed_price" => 1749,
                "advance_price" => 1639,
            ),
            array(
                "product_code" => "TRTMOX0006",
                "fixed_price" => 2319,
                "advance_price" => 2139,
            ),
            array(
                "product_code" => "TRTMOX0007",
                "fixed_price" => 2159,
                "advance_price" => 1990,
            ),
            array(
                "product_code" => "TRTSTX0008",
                "fixed_price" => 56390,
                "advance_price" => 52690,
            ),
            array(
                "product_code" => "TRTWOX0009",
                "fixed_price" => 2359,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "TRTMOX0010",
                "fixed_price" => 2790,
                "advance_price" => 2539,
            ),
            array(
                "product_code" => "TRTMOX0011",
                "fixed_price" => 3490,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "TRTMOX0012",
                "fixed_price" => 1229,
                "advance_price" => 1090,
            ),
            array(
                "product_code" => "TRTMOX0013",
                "fixed_price" => 1849,
                "advance_price" => 1690,
            ),
            array(
                "product_code" => "TRTLIX0014",
                "fixed_price" => 2690,
                "advance_price" => 2490,
            ),
            array(
                "product_code" => "TRTMOX0015",
                "fixed_price" => 3390,
                "advance_price" => 3190,
            ),
            array(
                "product_code" => "TRTMOX0016",
                "fixed_price" => 2690,
                "advance_price" => 2490,
            ),
            array(
                "product_code" => "BRTBOX0003",
                "fixed_price" => 39900,
                "advance_price" => 35900,
            ),
            array(
                "product_code" => "HOGWOX0001",
                "fixed_price" => 9490,
                "advance_price" => 8890,
            ),
            array(
                "product_code" => "HOGLUX0002",
                "fixed_price" => 9100,
                "advance_price" => 8390,
            ),
            array(
                "product_code" => "YBYANX0001",
                "fixed_price" => 4090,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "YBYBPX0002",
                "fixed_price" => 2690,
                "advance_price" => 2490,
            ),
            array(
                "product_code" => "YBYBJX0003",
                "fixed_price" => 2690,
                "advance_price" => 2490,
            ),
            array(
                "product_code" => "ECBIMX0009",
                "fixed_price" => 24000,
                "advance_price" => 22900,
            ),
            array(
                "product_code" => "ECBIMX0010",
                "fixed_price" => 13800,
                "advance_price" => 13290,
            ),
            array(
                "product_code" => "ECBSTX0013",
                "fixed_price" => 13600,
                "advance_price" => 12900,
            ),
            array(
                "product_code" => "ECBSTX0014",
                "fixed_price" => 7900,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "ECBHEX0022",
                "fixed_price" => 16800,
                "advance_price" => 16190,
            ),
            array(
                "product_code" => "ECBHEX0023",
                "fixed_price" => 45900,
                "advance_price" => 44390,
            ),
            array(
                "product_code" => "ECBENX0024",
                "fixed_price" => 5600,
                "advance_price" => 5390,
            ),
            array(
                "product_code" => "ECBENX0025",
                "fixed_price" => 13300,
                "advance_price" => 12900,
            ),
            array(
                "product_code" => "YBYMSX0003",
                "fixed_price" => 2690,
                "advance_price" => 2490,
            ),
            array(
                "product_code" => "BYRENX0004",
                "fixed_price" => 3390,
                "advance_price" => 3090,
            ),
            array(
                "product_code" => "BYRSKX0015",
                "fixed_price" => 1449,
                "advance_price" => 1290,
            ),
            array(
                "product_code" => "BYRSKX0016",
                "fixed_price" => 1439,
                "advance_price" => 1290,
            ),
            array(
                "product_code" => "BYRSKX0020",
                "fixed_price" => 3890,
                "advance_price" => 3590,
            ),
            array(
                "product_code" => "BYRSKX0021",
                "fixed_price" => 1379,
                "advance_price" => 1290,
            ),
            array(
                "product_code" => "OTXVIX0001",
                "fixed_price" => 999,
                "advance_price" => 919,
            ),
            array(
                "product_code" => "OTXVIX0002",
                "fixed_price" => 1290,
                "advance_price" => 1239,
            ),
            array(
                "product_code" => "OTXVIX0003",
                "fixed_price" => 2569,
                "advance_price" => 2439,
            ),
            array(
                "product_code" => "OTXVIX0004",
                "fixed_price" => 4090,
                "advance_price" => 3890,
            ),
            array(
                "product_code" => "DTLBOX0001",
                "fixed_price" => 690,
                "advance_price" => 649,
            ),
            array(
                "product_code" => "DTLBOX0002",
                "fixed_price" => 1099,
                "advance_price" => 1029,
            ),
            array(
                "product_code" => "DTLBOX0003",
                "fixed_price" => 1990,
                "advance_price" => 1890,
            ),
            array(
                "product_code" => "DTLBOX0004",
                "fixed_price" => 4290,
                "advance_price" => 4059,
            ),
            array(
                "product_code" => "DTLBOX0005",
                "fixed_price" => 1029,
                "advance_price" => 969,
            ),
            array(
                "product_code" => "DTLBOX0006",
                "fixed_price" => 829,
                "advance_price" => 769,
            ),
            array(
                "product_code" => "DTLBOX0007",
                "fixed_price" => 2159,
                "advance_price" => 2029,
            ),
            array(
                "product_code" => "DTLBOX0008",
                "fixed_price" => 819,
                "advance_price" => 769,
            ),
            array(
                "product_code" => "DTLBOX0009",
                "fixed_price" => 2159,
                "advance_price" => 2029,
            ),
            array(
                "product_code" => "DTLBOX0010",
                "fixed_price" => 819,
                "advance_price" => 769,
            ),
            array(
                "product_code" => "DTLBOX0011",
                "fixed_price" => 2159,
                "advance_price" => 2029,
            ),
            array(
                "product_code" => "DTLBOX0012",
                "fixed_price" => 819,
                "advance_price" => 769,
            ),
            array(
                "product_code" => "DTLBOX0013",
                "fixed_price" => 2159,
                "advance_price" => 2029,
            ),
            array(
                "product_code" => "DTLBOX0014",
                "fixed_price" => 819,
                "advance_price" => 769,
            ),
            array(
                "product_code" => "DTLBOX0015",
                "fixed_price" => 2159,
                "advance_price" => 2029,
            ),
            array(
                "product_code" => "DTLBOX0016",
                "fixed_price" => 1049,
                "advance_price" => 959,
            ),
            array(
                "product_code" => "DTLBOX0017",
                "fixed_price" => 1029,
                "advance_price" => 959,
            ),
            array(
                "product_code" => "DTLBOX0018",
                "fixed_price" => 449,
                "advance_price" => 399,
            ),
            array(
                "product_code" => "DTLBOX0019",
                "fixed_price" => 1539,
                "advance_price" => 1449,
            ),
            array(
                "product_code" => "DTLBOX0020",
                "fixed_price" => 1749,
                "advance_price" => 1639,
            ),
            array(
                "product_code" => "DTLBOX0021",
                "fixed_price" => 459,
                "advance_price" => 399,
            ),
            array(
                "product_code" => "DTLBOX0022",
                "fixed_price" => 1129,
                "advance_price" => 1049,
            ),
            array(
                "product_code" => "DTLBOX0023",
                "fixed_price" => 1129,
                "advance_price" => 1049,
            ),
            array(
                "product_code" => "DLTBWX0024",
                "fixed_price" => 3190,
                "advance_price" => 2990,
            ),
            array(
                "product_code" => "DLTBWX0025",
                "fixed_price" => 2690,
                "advance_price" => 2539,
            ),
            array(
                "product_code" => "BRTBOX0006",
                "fixed_price" => 3290,
                "advance_price" => 3190,
            ),
            array(
                "product_code" => "VITENX0061",
                "fixed_price" => 11590,
                "advance_price" => 10690,
            ),
            array(
                "product_code" => "FH2006-REG-VD1.2",
                "fixed_price" => 189900,
                "advance_price" => 189900,
            ),
            array(
                "product_code" => "FH2006-REG-VD2.1",
                "fixed_price" => 409000,
                "advance_price" => 409000,
            ),
            array(
                "product_code" => "FH2006-REG-AD.1",
                "fixed_price" => 609000,
                "advance_price" => 609000,
            ),
            array(
                "product_code" => "FH2006-REG-AD02",
                "fixed_price" => 1009000,
                "advance_price" => 1009000,
            ),
            array(
                "product_code" => "FH2006-REG-MD.1",
                "fixed_price" => 1009000,
                "advance_price" => 1009000,
            ),
            array(
                "product_code" => "FH2006-REG-MDO2",
                "fixed_price" => 1809000,
                "advance_price" => 1809000,
            ),
            array(
                "product_code" => "FH2006-REG-VDO2",
                "fixed_price" => 609000,
                "advance_price" => 609000,
            ),
            array(
                "product_code" => "VITENX0036",
                "fixed_price" => 16900,
                "advance_price" => 15590,
            ),
            array(
                "product_code" => "VITENX0062",
                "fixed_price" => 8990,
                "advance_price" => 8390,
            ),
            array(
                "product_code" => "AVNBWX0001",
                "fixed_price" => 4490,
                "advance_price" => 4290,
            ),
            array(
                "product_code" => "BYRENX0019",
                "fixed_price" => 2390,
                "advance_price" => 2290,
            ),
            array(
                "product_code" => "NTFSTX0003",
                "fixed_price" => 15900,
                "advance_price" => 13990,
            ),
            array(
                "product_code" => "FMLBOX001",
                "fixed_price" => 4990,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "MLMHWX001",
                "fixed_price" => 5090,
                "advance_price" => 4759,
            ),
            array(
                "product_code" => "MLMHWX002",
                "fixed_price" => 5090,
                "advance_price" => 4759,
            ),
            array(
                "product_code" => "MLMHWX003",
                "fixed_price" => 5090,
                "advance_price" => 4759,
            ),
            array(
                "product_code" => "MLMHWX004",
                "fixed_price" => 5090,
                "advance_price" => 4759,
            ),
            array(
                "product_code" => "MLMHWX005",
                "fixed_price" => 15290,
                "advance_price" => 14290,
            ),
            array(
                "product_code" => "PSLMSC001",
                "fixed_price" => 3690,
                "advance_price" => 3459,
            ),
            array(
                "product_code" => "PSLMSC002",
                "fixed_price" => 3190,
                "advance_price" => 2990,
            ),
            array(
                "product_code" => "PSLMSC003",
                "fixed_price" => 3390,
                "advance_price" => 3190,
            ),
            array(
                "product_code" => "PSLMSC004",
                "fixed_price" => 3190,
                "advance_price" => 2990,
            ),
            array(
                "product_code" => "STMORX001",
                "fixed_price" => 819,
                "advance_price" => 769,
            ),
            array(
                "product_code" => "STMORX002",
                "fixed_price" => 2059,
                "advance_price" => 1890,
            ),
            array(
                "product_code" => "STMORX003",
                "fixed_price" => 819,
                "advance_price" => 759,
            ),
            array(
                "product_code" => "STMORX004",
                "fixed_price" => 2059,
                "advance_price" => 1890,
            ),
            array(
                "product_code" => "STMORX005",
                "fixed_price" => 819,
                "advance_price" => 759,
            ),
            array(
                "product_code" => "STMORX006",
                "fixed_price" => 2059,
                "advance_price" => 1890,
            ),
            array(
                "product_code" => "STMORX007",
                "fixed_price" => 919,
                "advance_price" => 849,
            ),
            array(
                "product_code" => "STMORX008",
                "fixed_price" => 2359,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "STMORX009",
                "fixed_price" => 1129,
                "advance_price" => 1039,
            ),
            array(
                "product_code" => "STMORX010",
                "fixed_price" => 2990,
                "advance_price" => 2759,
            ),
            array(
                "product_code" => "STMORX011",
                "fixed_price" => 1029,
                "advance_price" => 939,
            ),
            array(
                "product_code" => "STMORX012",
                "fixed_price" => 2690,
                "advance_price" => 2469,
            ),
            array(
                "product_code" => "STMORX013",
                "fixed_price" => 1329,
                "advance_price" => 1239,
            ),
            array(
                "product_code" => "STMORX014",
                "fixed_price" => 2359,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "STMORX015",
                "fixed_price" => 1129,
                "advance_price" => 1039,
            ),
            array(
                "product_code" => "STMORX016",
                "fixed_price" => 2059,
                "advance_price" => 1890,
            ),
            array(
                "product_code" => "STMORX017",
                "fixed_price" => 1329,
                "advance_price" => 1229,
            ),
            array(
                "product_code" => "STMORX018",
                "fixed_price" => 2359,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "STMORX019",
                "fixed_price" => 1329,
                "advance_price" => 1229,
            ),
            array(
                "product_code" => "STMORX020",
                "fixed_price" => 2359,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "STMORX021",
                "fixed_price" => 1329,
                "advance_price" => 1229,
            ),
            array(
                "product_code" => "STMORX022",
                "fixed_price" => 2359,
                "advance_price" => 2190,
            ),
            array(
                "product_code" => "STMORX023",
                "fixed_price" => 1439,
                "advance_price" => 1329,
            ),
            array(
                "product_code" => "STMORX024",
                "fixed_price" => 1439,
                "advance_price" => 1329,
            ),
            array(
                "product_code" => "FMLBOX002",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "FMLBOX010",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "FMLBOX004",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "SRYIMX0001",
                "fixed_price" => 5500,
                "advance_price" => 5500,
            ),
            array(
                "product_code" => "SRYIMX0002",
                "fixed_price" => 5500,
                "advance_price" => 5500,
            ),
            array(
                "product_code" => "SRYIMX0003",
                "fixed_price" => 5500,
                "advance_price" => 5500,
            ),
            array(
                "product_code" => "BRKCLX0001",
                "fixed_price" => 15990,
                "advance_price" => 15890,
            ),
            array(
                "product_code" => "RBS-KDLIMX0022",
                "fixed_price" => 110940,
                "advance_price" => 103140,
            ),
            array(
                "product_code" => "FMLBOX005",
                "fixed_price" => 14970,
                "advance_price" => 11900,
            ),
            array(
                "product_code" => "FMLBOX0011",
                "fixed_price" => 5090,
                "advance_price" => 4690,
            ),
            array(
                "product_code" => "FMLBOX0012",
                "fixed_price" => 14970,
                "advance_price" => 11900,
            ),
            array(
                "product_code" => "FMLBOX0013",
                "fixed_price" => 15270,
                "advance_price" => 11900,
            ),
            array(
                "product_code" => "FMLBOX0014",
                "fixed_price" => 15270,
                "advance_price" => 11900,
            ),
            array(
                "product_code" => "KDLIMX0024",
                "fixed_price" => 5590,
                "advance_price" => 5290,
            ),
            array(
                "product_code" => "RBS-SWSENX0001",
                "fixed_price" => 31140,
                "advance_price" => 29340,
            ),
            array(
                "product_code" => "RBS-SWSENX0002",
                "fixed_price" => 62280,
                "advance_price" => 58680,
            ),
            array(
                "product_code" => "RBS-DMVBWX0002",
                "fixed_price" => 71880,
                "advance_price" => 68280,
            ),
            array(
                "product_code" => "RBS-LRPFWX0004",
                "fixed_price" => 95880,
                "advance_price" => 88680,
            ),
            array(
                "product_code" => "RBS-BLMENX0051",
                "fixed_price" => 95940,
                "advance_price" => 83940,
            ),
            array(
                "product_code" => "RBS-SWSENX0005",
                "fixed_price" => 23700,
                "advance_price" => 23070,
            ),
            array(
                "product_code" => "RBS-SWSENX0008",
                "fixed_price" => 94800,
                "advance_price" => 92280,
            ),
            array(
                "product_code" => "RBS-SWSENX0006",
                "fixed_price" => 47400,
                "advance_price" => 46140,
            ),
            array(
                "product_code" => "RBS-SWSENX0007",
                "fixed_price" => 47400,
                "advance_price" => 46140,
            ),
            array(
                "product_code" => "RBS-SWSENX0002-variation-size",
                "fixed_price" => 62280,
                "advance_price" => 58680,
            ),
            array(
                "product_code" => "RBS-LRPFWX0003",
                "fixed_price" => 47940,
                "advance_price" => 44340,
            ),
            array(
                "product_code" => "RBS-SWSENX0008-variation-size",
                "fixed_price" => 94800,
                "advance_price" => 92280,
            ),
            array(
                "product_code" => "RBS-BLMENX0051-variation-size",
                "fixed_price" => 95940,
                "advance_price" => 83940,
            ),
            array(
                "product_code" => "RBS-LRPFWX0004-variation-size",
                "fixed_price" => 95880,
                "advance_price" => 88680,
            ),
            array(
                "product_code" => "RBS-KDLIMX0023-variation-size",
                "fixed_price" => 110940,
                "advance_price" => 103140,
            ),
            array(
                "product_code" => "RBS-DMVBWX0002-variation-size",
                "fixed_price" => 71880,
                "advance_price" => 68280,
            ),
            array(
                "product_code" => "RBS-SWSENX0010-variation-size",
                "fixed_price" => 47400,
                "advance_price" => 46140,
            ),
            array(
                "product_code" => "RBS-DMVBWX0001",
                "fixed_price" => 35940,
                "advance_price" => 34140,
            ),
            array(
                "product_code" => "RBS-NTFENX0022",
                "fixed_price" => 154800,
                "advance_price" => 142680,
            ),
            array(
                "product_code" => "RBS-NTFENX0019",
                "fixed_price" => 38700,
                "advance_price" => 35670,
            ),
            array(
                "product_code" => "RBS-NTFENX0020",
                "fixed_price" => 77400,
                "advance_price" => 71340,
            ),
            array(
                "product_code" => "RBS-NTFENX0021",
                "fixed_price" => 77400,
                "advance_price" => 71340,
            ),
            array(
                "product_code" => "RBS-KDLIMX0020",
                "fixed_price" => 29940,
                "advance_price" => 28140,
            ),
            array(
                "product_code" => "RBS-KDLIMX0022",
                "fixed_price" => 59880,
                "advance_price" => 56280,
            ),
            array(
                "product_code" => "RBS-KDLIMX0023",
                "fixed_price" => 55470,
                "advance_price" => 51570,
            ),
            array(
                "product_code" => "RBS-BLMENX0053",
                "fixed_price" => 95940,
                "advance_price" => 83940,
            ),
            array(
                "product_code" => "RBS-BLMENX0050",
                "fixed_price" => 47970,
                "advance_price" => 41970,
            ),
            array(
                "product_code" => "RBS-SWSENX0009",
                "fixed_price" => 26940,
                "advance_price" => 26340,
            ),
            array(
                "product_code" => "RBS-SWSENX0010",
                "fixed_price" => 53880,
                "advance_price" => 52680,
            ),
            array(
                "product_code" => "RBS-KDLIMX0024",
                "fixed_price" => 110940,
                "advance_price" => 103140,
            ),
            array(
                "product_code" => "FMLIMX0002",
                "fixed_price" => 29500,
                "advance_price" => 26950,
            ),
            array(
                "product_code" => "FMLTS001",
                "fixed_price" => 6900,
                "advance_price" => 6900,
            ),
            array(
                "product_code" => "FMLTS002",
                "fixed_price" => 6900,
                "advance_price" => 6900,
            ),
            array(
                "product_code" => "FMLTS003",
                "fixed_price" => 6900,
                "advance_price" => 6900,
            ),
            array(
                "product_code" => "FMLTS004",
                "fixed_price" => 6900,
                "advance_price" => 6900,
            ),
            array(
                "product_code" => "FMLTS005",
                "fixed_price" => 6900,
                "advance_price" => 6900,
            ),
            array(
                "product_code" => "FMLTS006",
                "fixed_price" => 9900,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "FMLTS007",
                "fixed_price" => 9900,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "FMLTS008",
                "fixed_price" => 9900,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "FMLTS009",
                "fixed_price" => 9900,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "FMLTS010",
                "fixed_price" => 9900,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "FMLRCYC001",
                "fixed_price" => 200,
                "advance_price" => 200,
            ),
            array(
                "product_code" => "ECBENX0025",
                "fixed_price" => 13600,
                "advance_price" => 12900,
            ),
            array(
                "product_code" => "ECBENX0026",
                "fixed_price" => 7900,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "ECBENX0027",
                "fixed_price" => 13300,
                "advance_price" => 12900,
            ),
            array(
                "product_code" => "ECBENX0028",
                "fixed_price" => 7900,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "BJSREX001",
                "fixed_price" => 87800,
                "advance_price" => 59900,
            ),
            array(
                "product_code" => "BJSVIX002",
                "fixed_price" => 19900,
                "advance_price" => 18900,
            ),
            array(
                "product_code" => "BJSVIX003",
                "fixed_price" => 39800,
                "advance_price" => 24900,
            ),
            array(
                "product_code" => "BJSVIX004",
                "fixed_price" => 19900,
                "advance_price" => 18900,
            ),
            array(
                "product_code" => "BJSVIX005",
                "fixed_price" => 39800,
                "advance_price" => 24900,
            ),
            array(
                "product_code" => "BJSVIX006",
                "fixed_price" => 19900,
                "advance_price" => 18900,
            ),
            array(
                "product_code" => "BJSVIX007",
                "fixed_price" => 39800,
                "advance_price" => 24900,
            ),
            array(
                "product_code" => "BJSVIX008",
                "fixed_price" => 19900,
                "advance_price" => 18900,
            ),
            array(
                "product_code" => "BJSVIX009",
                "fixed_price" => 39800,
                "advance_price" => 24900,
            ),
            array(
                "product_code" => "BJSVIX010",
                "fixed_price" => 19900,
                "advance_price" => 18900,
            ),
            array(
                "product_code" => "BJSVIX011",
                "fixed_price" => 39800,
                "advance_price" => 24900,
            ),
            array(
                "product_code" => "FNNKYX012",
                "fixed_price" => 200,
                "advance_price" => 200,
            ),
            array(
                "product_code" => "FH2006-REG-VD2.2",
                "fixed_price" => 309000,
                "advance_price" => 309000,
            ),
            array(
                "product_code" => "FMLIMX0001",
                "fixed_price" => 5900,
                "advance_price" => 5390,
            ),
            array(
                "product_code" => "FMLIMX0003",
                "fixed_price" => 11800,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "MDPBOX0001",
                "fixed_price" => 1990,
                "advance_price" => 1790,
            ),
            array(
                "product_code" => "MDPBOX0002",
                "fixed_price" => 3590,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "MDPBOX0003",
                "fixed_price" => 3590,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "MDPBOX0004",
                "fixed_price" => 1190,
                "advance_price" => 1090,
            ),
            array(
                "product_code" => "MDPBOX0005",
                "fixed_price" => 3590,
                "advance_price" => 3290,
            ),
            array(
                "product_code" => "MDPBOX0006",
                "fixed_price" => 11090,
                "advance_price" => 9900,
            ),
            array(
                "product_code" => "FMLRCYC002_FOC",
                "fixed_price" => 1,
                "advance_price" => 1,
            ),
            array(
                "product_code" => "ECBENX0030",
                "fixed_price" => 13300,
                "advance_price" => 12900,
            ),
            array(
                "product_code" => "ECBENX0031",
                "fixed_price" => 7700,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "ECBENX0033",
                "fixed_price" => 7700,
                "advance_price" => 7590,
            ),
            array(
                "product_code" => "NTFHEX051",
                "fixed_price" => 16900,
                "advance_price" => 12900,
            ),
            array(
                "product_code" => "VITBRX0050",
                "fixed_price" => 14900,
                "advance_price" => 13390,
            ),
            array(
                "product_code" => "FMLORX003",
                "fixed_price" => 99800,
                "advance_price" => 59800,
            ),
            array(
                "product_code" => "FMLORX001",
                "fixed_price" => 49900,
                "advance_price" => 29900,
            ),
            array(
                "product_code" => "FMLORX002",
                "fixed_price" => 49900,
                "advance_price" => 29900,
            ),
        );

        $data = array();

        foreach ($panel_product_prices as $price) {
            $data[$price['product_code']] = $price;
        }

        return  $data;
    }
}
