
<form action="" >
    @if (isset($products) && isset($products->panelProduct))

   <div class="ft-cart-content">
    <div id="showCart">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-quantity-ft" class="ftitemCount">0</span>
     </div>
      {{-- @if ($products->panelProduct->attributes->count() < 1) --}}
      <div class="item-variation mobile">

         <div class="variation-title">
            <h4>Variation</h4>
         </div>
         <ul>
            @foreach ($products->panelProduct->attributes as $attributes)
            <li>
                @php
                    $productLimit = null;
                    $roleLimit = false;
                    $totalPurchase = 0;
                    $period = null;
                    $purchaseLeft = 0;

                    if (Auth::check() && isset($attributes) && $attributes->productLimit->isNotEmpty()) {
                        $productLimit = $attributes->productLimit->where('user_id', auth()->user()->id)->first();
                        if(!$productLimit)$productLimit = $attributes->productLimit->where('user_id', -1)->first();
                        if(!$productLimit) continue;
                        $result = $productLimit->checkPurchaseLimit(auth()->user()->id, $productLimit);

                        $totalPurchase = $result['total_purchase'];
                        $period = $result['period'];
                        $roleLimit = $result['purchase_limit_eligible'];
                        $purchase_left = $productLimit->amount - $totalPurchase;
                        if ($purchase_left < 0) $purchase_left = 0; 
                    }
                @endphp
               <div class="variation-box">
                  <label>
                     <input type="radio" name="attributesPrice" value="vrt-{{ $attributes->id }}"  id="attributesPrice{{ $attributes->id }}"
                        data-offer-price="{{ number_format(($attributes->getPriceAttributes('web_offer_price') /100),2) }}"
                        data-fixed-price="{{ number_format(($attributes->getPriceAttributes('fixed_price')/100),2) }}"
                        data-advance-price="{{ number_format(($attributes->getPriceAttributes('price')/100),2) }}"
                        data-premium-price="{{ number_format(($attributes->getPriceAttributes('member_price')/100),2) }}"
                        data-product-name="{{ $products->name }}"
                        data-variation-name="{{ $attributes->attribute_name }}"
                        data-sales-count="{{ $attributes->sales_count }} "
                        data-variation-id="{{ $attributes->id }}" {{ $loop->first ? 'checked' : '' }}
                        data-rbs-months=""
                        data-eligible-purchase="{{ ($productLimit && $roleLimit) ? $purchase_left : null }}"
                        data-purchase-limit-amount="{{ ($productLimit && $roleLimit) ? $productLimit->amount : null }}"
                        data-purchase-limit-period="{{ $period }}">
                     @if ($images->where('id',$attributes->color_images)->first())
                     <img src="{{asset('storage/'.$images->where('id',$attributes->color_images)->first()->path.$images->where('id',$attributes->color_images)->first()->filename)}}">
                        @else
                     <img src="{{asset('/images/product_item/test_productitem.png')}}">
                        @endif
                  </label>
                  <p>{{ $attributes->attribute_name }}</p>
               </div>
            </li>
            @endforeach
         </ul>
      </div>
      {{-- @endif --}}

      <div class="item-buy-section mobile">

         <div class="item-quality">
            <h6 class="rbsQuantity">Quantity</h6>
            <div class="item-buy-section-quantity" >
               <button class="product-add-btn" type="button" name="fml-add-btn">
                  <i class="fa fa-plus" aria-hidden="true"></i>
               </button>
               <input type="text" name="productQuantity" value="" id="varqty" class="varqty">
               <button class="product-minus-btn " type="button" name="fml-minus-btn">
                  <i class="fa fa-minus" aria-hidden="true"></i>
               </button>
            </div>
         </div>
         <div class="remind-bell" onclick="funcRemind()">
            <img src="{{ asset('/images/icons/rbsbell.png') }}" alt="" >
        </div>
         <div class="clickme-info-mb web" style="{{ $products->show_warning == 1 ? 'display: block':'display: none'}}">
            <a data-bs-toggle="modal" href="#waringInfo" id="warning-modal" data-button="Back" role="button">
               <img src="{{asset('/images/icons/clickme-icon.png')}}" width="80%" alt="">
            </a>
         </div>
         <div class="@if($products->show_warning == 1) item-buy warning @else item-buy @endif">
                  {{-- <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart()'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a> --}}

                  {{-- <a id="buynowft" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart()'}}" href="{{ $products->display_only == 1 ?  '#' : '/shop/cart'}}" class="link-product buy_now" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}">
                     <div id="showCart">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>TWO<span id="cart-quantity-ft" class="ftitemCount">0</span>ZXCZXC
                     </div>
                     <div class="p-m-add-btn"  >
                        Buy NowASDASDAS
                     </div>
                  </a> --}}

                  @if($products->show_warning == 1 && $products->display_only == 0)

                  <a id="warning-modal" data-button="Proceed" data-bs-toggle="modal" href="#waringInfo-buy-cart" role="button" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  <a class="buy-btn link-product buy_now warning" id="warning-modal" data-button="Proceed" data-bs-toggle="modal" href="#waringInfo-buy-cart" role="button">
                     Buy Now
                  </a>
                     {{-- <a class="buy-btn link-product buy_now" onclick="{{ $products->display_only == 1 ?  '' : "ajaxSaveCart(),setTimeout(function(){window.location.href = '/shop/cart'; }, 500);"}}" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}">
                        Buy Now
                        </a> --}}

                  @elseif($products->show_warning == 0 && $products->display_only == 1)

                  <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart(false)'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  <a href="#" class="buy-btn link-product buy_now one">
                     Buy Now
                  </a>

                  @elseif($products->display_only == 1 && $products->show_warning == 1)
                  <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart(false)'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  <a href="#"  class="buy-btn link-product buy_now one warning">
                     Buy Now
                  </a>

                  @else

                  <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart(false)'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  {{-- this is the show cart number start --}}
                  {{-- <div id="showCart">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-quantity-ft" class="ftitemCount">0</span>
                  </div> --}}
                  {{-- this is the show cart number end --}}

                  <a class="buy-btn link-product buy_now" onclick="{{ $products->display_only == 1 ?  '' : "ajaxSaveCart(true)" }}" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}">
                     Buy Now
                  </a>
                  @endif

                  {{-- <div id="showCart">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-quantity-ft" class="ftitemCount">0</span>
                  </div>
                  @if($products->show_warning == 1)
                  <a id="buynowft" class="link-product buy_now one" id="warning-modal" data-button="Proceed" data-bs-toggle="modal" href="#waringInfo" role="button">
                     Buy Now
                     </a>
                  @else
                  <a id="buynowft" type="button" class="link-product buy_now" onclick="{{ $products->display_only == 1 ?  '' : "ajaxSaveCart(),setTimeout(function(){window.location.href = '/shop/cart'; }, 500);"}}">
                     Buy Now
                     </a>
                  @endif --}}

         </div>
         <span class="noteRbs-add"></span>
         <span class="noteRbs-add-cart"></span>
      </div>


   </div>
   @endif

 </form>

 @push('style')
<style>
   .buy_now {
     white-space: nowrap;
   }

   @media only screen and (max-width:320px) {
      .remind-bell {
         margin-left: 25px;
         /* border: 3px solid red; */
      }

      .clickme-info-mb {
         margin-bottom: 20px;
         margin-top: 20px;
         object-fit: scale-down;
         /* border: 3px solid red; */
      }

      .item-quality {
         margin-left: 68px;
         /* border: 3px solid red; */
      }
   }


</style>
@endpush

