@extends('layouts.guest.main')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo" src="{{ asset('images/logo/formula-logo.png') }}" alt="">
        </div>
    </div>

    <div class="my-register-box my-guests-card">
        <div class="my-r-title-box">
            <h5><b>SCAN QR TO REGISTER</b></h5>
        </div>

        <div class="card-body">
            <div class="tab-content r-inner-box" id="myTabContent" >
                <div class="r-subt-box tab-pane fade show active " id="registration" role="tabpanel"
                        aria-labelledby="registration-tab" style="display: flex; align-items: center; justify-content: center;">
                        {!! QrCode::size(300)->generate("registration::" . $input); !!} 
                </div>
            </div>
        </div>
    </div>
@endsection