<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseBatchItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_batch_item', function (Blueprint $table) {
            $table->string('items_id',24)->nullable();
            $table->string('batch_id',24)->nullable();
            $table->integer('detail_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('shelf_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_batch_item');
    }
}
