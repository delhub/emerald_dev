<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.favicon')


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}
    @stack('style')



    <style>
        /* Make sure the background image covers the screen */
        html {
            height: 100%;
        }

        body .bg-md {
            height: 100%;
        }

        .remember-check {
            width: 15px;
            height: 15px;
            cursor: pointer;
        }

        .error {
            color: red;
        }
    </style>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!--Validate register fields -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>

</head>

@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp

<body>

    <div class="bg-md bg-sm ">
        <div class="row">
            <div class="col-6 offset-3 col-md-4 offset-md-4 text-center">
                <a href="/"><img class="mw-100 w-50-md" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
                </a>
            </div>
        </div>
        <div class="">
            <div class="card border-rounded-0 bg-bujishu-gold mt-4 guests-card" style="border-radius: 10px;">
                <h5 class="text-center bujishu-gold form-card-title " style="border-radius: 10px;"><b>APPLY FOR DC
                        CUSTOMER</b></h5>
                <h5 class="text-center bg-white form-card-title "><b>Identity Verification</b></h5>

                <div class="card-body">
                    <div class="col-12 bg-white">
                        <br>
                        <form>
                            @csrf
                            <div class="text-center pb-4">
                                <div class="form-check d-inline px-4">
                                    <input class="form-check-input" type="radio" name="DCRadio" id="DCRadio"
                                        value="existing" checked>
                                    <label class="form-check-label" for="DCRadio">
                                        <b>DC Customer</b>
                                    </label>
                                </div>
                                <div class="form-check d-inline px-4">
                                    <input class="form-check-input" type="radio" name="DCRadio" id="BJSRadio"
                                        value="new">
                                    <label class="form-check-label" for="BJSRadio">
                                        <b>New</b>
                                    </label>
                                </div>
                            </div>
                        </form>

                        <div class="existing box" id="existing box">
                            <form method="POST" action="{{ route('customer.check.membership') }}" id="existing-form"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="hidden" name="existingOrNew" value="Existing">
                                        <input type="hidden" name="user_id" value="{{ $user->userInfo->account_id }}">
                                        <input type="hidden" name="email" value="{{ $user->email }}">

                                        <label for="full_name">Full Name (as per NRIC)</label>
                                        <input type="text" name="full_name"
                                            class="form-control @error('full_name') is-invalid @enderror" id="full_name"
                                            required placeholder="Full Name" value="{{ old('full_name') }}">
                                        @error('full_name')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="contact_number_mobile">Contact Number (Mobile)</label>
                                        <input type="text" name="contact_number_mobile" id="contact_number_mobile"
                                            class="form-control @error('contact_number_mobile') is-invalid @enderror"
                                            placeholder="Mobile Contact Number"
                                            value="{{ old('contact_number_mobile') }}">
                                        @error('contact_number_mobile')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="year">Year becomes DC customer</label>
                                        <input type="text" name="year"
                                            class="form-control @error('year') is-invalid @enderror" id="year"
                                            placeholder="e.g 2020" value="{{ old('year') }}">
                                        @error('year')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="whatProduct">What product?</label>
                                        <select class="form-control" id="whatProduct" name="whatProduct">
                                            <option value="" disabled selected>Choose your product...</option>
                                            <option value="PR">PR</option>
                                            <option value="FS">FS</option>
                                            <option value="SFS">SFS</option>
                                            <option value="RW">RW</option>
                                            <option value="AP">AP</option>
                                            <option value="FC">FC</option>
                                            <option value="CL">CL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class=" form-group col-md-12">
                                        <label for="DCProductInvoice">DC product invoice
                                            <small class="form-text text-secondary">*To fasten the verification
                                                process please
                                                upload DC product invoice.</small></label>
                                        <input type="file" accept="image/*" class="form-control-file"
                                            id="DCProductInvoice" name="DCProductInvoice"
                                            data-msg-accept="File must be JPG, GIF or PNG">

                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class=" form-group col-md-12">
                                        <label for="referrer_name">DC/PRSB agent name</label>
                                        <input type="text" name="referrer_name" class="form-control">
                                        @error('referrer_name')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class=" form-group col-md-12">
                                        <label for="referrer_id">DC/PRSB agent ID</label>
                                        <input type="text" name="referrer_id" class="form-control" maxlength="9">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mb-1 text-right">
                                        <button onclick="window.location.href='/'" id="cancel"
                                            class="btn next-button bjsh-btn-gradient text-right"><b>Cancel</b></button>
                                        <button type="submit" id="next-btn"
                                            class="btn next-button bjsh-btn-gradient text-right"
                                            style=""><b>Submit</b></button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="new box" id="new box" style="display: none;">
                            <form method="POST" action="{{ route('customer.check.membership') }}" id="new-form">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="hidden" name="existingOrNew" value="New">
                                        <input type="hidden" name="user_id" value="{{ $user->userInfo->account_id }}">
                                        <input type="hidden" name="email" value="{{ $user->email }}">
                                        <label for="full_name">Full Name (as per NRIC)</label>
                                        <input type="text" name="full_name"
                                            class="form-control @error('full_name') is-invalid @enderror" id="full_name"
                                            required placeholder="Full Name" value="{{ old('full_name') }}">
                                        @error('full_name')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="contact_number_mobile">Contact Number (Mobile)</label>
                                        <input type="text" name="contact_number_mobile" id="contact_number_mobile"
                                            class="form-control @error('contact_number_mobile') is-invalid @enderror"
                                            placeholder="Mobile Contact Number"
                                            value="{{ old('contact_number_mobile') }}">
                                        @error('contact_number_mobile')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-12 mb-1 text-right">
                                        <button onclick="window.location.href='/'" id="cancel"
                                            class="btn next-button bjsh-btn-gradient text-right"><b>Cancel</b></button>
                                        <button type="submit" class="btn next-button bjsh-btn-gradient text-right"
                                            style=""><b>Submit</b></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

<script>
    $(document).ready(function(){


    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
    });


    // Validate registration tab before moving to the next tab
    $("#new-form").validate({
        rules: {

            full_name: {
                required: true,
                minlength: 3
            },

            contact_number_mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 15
            }

        },
        messages: {

            full_name: {
                required: "Please enter your full name.",
                minlength: "Your name must be more than 3 characters."
            },
            contact_number_mobile: {
                required: "Please enter your mobile number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            }
        }
    });

    $("#existing-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            full_name: {
                required: true,
                minlength: 3
            },
            year: {
                required: true,
                minlength: 4,
                maxlength: 4
            },
            contact_number_mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 15
            },
            referrer_id: {
                // required: true,
                digits: true,
            },
            referrer_name: {
                // required: true,
            },
            whatProduct: {
                required: true,
            },


        },
        messages: {
            email: {
                required: "Please enter an email.",
                email: "The email is not valid."
            },
            full_name: {
                required: "Please enter your full name.",
                minlength: "Your name must be more than 3 characters."
            },
            year: {
                required: "Please enter year you become DC agent",
                digits: "Please enter number only.",
                minlength: "Please enter year only",
                maxlength: "Please enter year only"
            },
            contact_number_mobile: {
                required: "Please enter your mobile number.",
                digits: "Please enter number only.",
                minlength: "Contact number must at least be 10 digits.",
                maxlength: "Please enter a valid contact number."
            },
            referrer_id: {
                required: "Please enter Agent ID.",
                digits: "Please enter number only."
            },
            referrer_name: {
                required: "Please enter an Agent name."
            },
            whatProduct: {
                required: ""
            },

        }
    });

    $('#cancel').on('click', function (e) {
        e.preventDefault();
        validate.resetForm();
        $('form').get(0).reset();
    });

    });

</script>

</html>