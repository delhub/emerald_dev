<?php

use App\Models\Purchases\Purchase;
use App\Models\ShippingInstallations\Fee;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeShippingFeeFormat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_fee', function (Blueprint $table) {
            //
        });

        $shippingFees = Fee::where('type','rebate_fee')->get();

        foreach ($shippingFees as $shippingFee) {
            $sInfo = json_decode($shippingFee->shipping_information);
            if (array_key_exists('details',$sInfo)) {

                $purchases = Purchase::where('id',$shippingFee->purchase_id)->first();

                foreach ($sInfo->details as $key => $details) {
                    $fee = new Fee;

                    $fee->name = ucwords('Discount Fee') . ' (' . $details->coupon_code . ')';
                    $fee->purchase_id = $purchases->id ?? 0;
                    $fee->type = 'rebate_fee';
                    $fee->amount = $details->coupon_amount * 100;
                    $fee->shipping_information = json_encode(['voucher_code' => $details->coupon_code , 'purchase_number' => $purchases->purchase_number ?? 0]);
                    //  {"voucher_code": "A2D9D6FD", "purchase_number": "FWS21-007514"} $voucher['shipping_information'];

                    $fee->save();
                }
                $shippingFee->delete();
            } else {
                if (!empty($sInfo)) {
                    $shippingFee->name = ucwords('Discount Fee') . ' (' . $sInfo->voucher_code . ')';
                    $shippingFee->save();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_fee', function (Blueprint $table) {
            //
        });
    }
}
