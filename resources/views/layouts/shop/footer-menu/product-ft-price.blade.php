<div class="productitem-info my-mb-3">
    <div class="ft-price-img">
        @if (count($images) > 0)
        <img src="{{asset('storage/'.$images[0]->path.$images[0]->filename)}}" alt="">
        @endif

        <div class="info">
            <h4>
                {{ $products->name }}
            </h4>
        </div>

        {{-- RBS Price List start --}}
        {{-- @if ($products->panelProduct->attributes->count() > 0) --}}
        <div class="rbs-active">
            <div class="rbs-price mobile">
                <ul>
                    <li>
                        <div class="title">Delivery frequency </div>
                        <div class="price month">: <span>0</span> Monthly</div>
                    </li>
                    <li>
                        <div class="title">Total times of delivery </div>
                        <div class="price time">: <span>0</span> times</div>
                    </li>
                </ul>
            </div>
            {{-- <hr> --}}
            <div class="rbs-col mb" style="display:block">
                <div class="add-pd">
                    <div class="rbs-total">
                        <div class="title">
                            <div class="tit-group">
                                <div class="icon-rbs">$</div>
                                <h3>Total RBS Price</h3>
                                <p class="rbs_total"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- add RBS notice --}}
            <div class="rbs-note"></div>
        </div>
        {{-- @endif --}}
        {{-- RBS Price List end --}}

    </div>

    {{-- @if ($products->panelProduct->attributes->count() < 0) --}}
    <div class="ft-price">
        <div class="item-pricelist">
            <div class="non-member-price">
                <p id="fixed_price" class="fixed_price"><span>{{ country()->country_currency }}</span>
                    {{ number_format(($products->panelProduct->attributes->first()->getPriceAttributes('fixed_price') / 100),2) }}</p>
                <p>Fixed Price</p>
            </div>
            <div class="offer-price">
                <img src="{{asset('/images/icons/huangmao.png')}}" class="" alt="">
                <p id="offer_price" class="offer_price"><span>{{ country()->country_currency }}</span>
                    {{ number_format(($products->panelProduct->attributes->first()->getPriceAttributes('web_offer_price') / 100),2) }}
                </p>
                <p>Offer Price</p>
            </div>
            {{-- @if ($userLevel == 6001)
            <div class="member-price">
                <img src="{{asset('/images/icons/huangmao.png')}}" class="" alt="">
                <p id="advance_price"><span>{{ country()->country_currency }}</span>{{ number_format(($products->panelProduct->attributes->first()->getPriceAttributes('price') / 100),2) }}</p>
                <p>Advance Price</p>
            </div>
            @endif --}}

            @if ($userLevel == 6002)
            <div class="member-price">
                <img src="{{asset('/images/icons/huangmao.png')}}" class="" alt="">
                <p id="premium_price"><span>{{ country()->country_currency }}</span>{{ number_format(($products->panelProduct->attributes->first()->getPriceAttributes('member_price') / 100),2) }}</p>
                <p>Premium Price</p>
            </div>
            @endif

        </div>

        <div class="item-pricelist">
            @if ($products->panelProduct->attributes->first()->getPriceAttributes('point') != 0)
            <div class="non-member-price">
                <p id="point">{{ number_format($products->panelProduct->attributes->first()->getPriceAttributes('point')) }}</p>
                <p>Points</p>
            </div>
            @endif
        </div>
    </div>
    {{-- @endif --}}

</div>
