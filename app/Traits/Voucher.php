<?php

namespace App\Traits;

use App\Models\Users\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Discount\Code;
use App\Models\Discount\Rule;
use App\Models\Discount\Usage;
use Facades\App\Repository\CoreCache;
use App\Models\Users\Customers\Cart;
use stdClass;
use Illuminate\Support\Facades\DB;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\ShippingInstallations\Fee;

trait Voucher
{
    public function generate_purchase_discount_code_v2($purchase, $cart)
    {

        if ($purchase->purchase_type == 'Bluepoint') return;

        $country_code = country()->country_id;
        $today = Carbon::now();

        $rules = Rule::where([
            ['country_id', $country_code],
            ['rule_type', 'generate_checkout'],
            ['start_date', '<=', $today],
            ['end_date', '>=', $today]
        ])->get();

        // dd($rules);

        if (count($rules) > 0) {
            $discount = 0;

            foreach ($rules as $rule) {

                if (count($cart) == 0) return;

                foreach ($cart as $val) {
                    $global_product_id[] = CoreCache::getProductId($val->product_id);
                    $product_id[] = $val->product_id;
                }

                //get limit category or product by rule type & limit id with cache data
                switch ($rule->limit_type) {
                    case 'ex_category':
                    case 'in_category':
                        $result = CoreCache::limit_category_product($rule->limit_type, $rule->limit_id);
                        $inlimit = collect($result)->whereIn('global_product_id', $global_product_id);
                        break;
                    case 'ex_product':
                    case 'in_product':
                        $result = CoreCache::limit_category_product($rule->limit_type, $rule->limit_id);
                        $inlimit = collect($result)->whereIn('id', $product_id);
                        break;
                    default: //na
                        $result = CoreCache::limit_category_product($rule->limit_type, json_encode($product_id));
                        $inlimit = collect($result)->whereIn('id', $product_id);
                }

                // dd($result,$inlimit);

                if ((count($inlimit) > 0)) {
                    foreach ($inlimit as $val) {
                        $products_id[] = $val->global_product_id;
                    }

                    //get product can generate voucher
                    $can_generate = CoreCache::getProductCanGenerate(json_encode($products_id));

                    if (count($can_generate) > 0) {
                        foreach ($can_generate as $val) {
                            $product_id_can_generate[] = $val->id;
                        }

                        // collect item can generate discount & auto apply discount = 0
                        $can_generate_discount = collect(json_decode($cart))->whereIn('product_id', $product_id_can_generate)->where('auto_apply', 0)->where('rebate', 0);
                        $amount = collect($can_generate_discount)->sum('subtotal_price');
                        $quantity = collect($can_generate_discount)->sum('quantity');

                        $amount = ($amount / 100);

                        if ($rule->discount_type == 'price') {
                            if ($amount > $rule->minimum) {
                                if ($rule->coupon_type == 'value') { // value calculation

                                    switch ($rule->general_type) {
                                        case 'breakdown':
                                            $step = floor($rule->value / $rule->step);
                                            $lastVal = $rule->value - ($step * $rule->step);

                                            $this->createNewVoucherCode($purchase, $rule, $rule->step, $step);

                                            if ($lastVal > 0) {
                                                $this->createNewVoucherCode($purchase, $rule, $lastVal, 1);
                                            }

                                            break;
                                        default:

                                            $max_voucher_value = $this->getMaxRuleVoucherValue($rule);
                                            $total_voucher = $this->getVoucherTotalValue($purchase->user_id, $rule->id);
                                            $available_step = ($max_voucher_value - $total_voucher) / $rule->value;

                                            if ($amount > $rule->maximum) $amount = $rule->maximum;

                                            $step = floor($amount / $rule->step);

                                            if (($step > 0) && ($step <= $available_step)) {
                                                $discount = $rule->value * $step;

                                                $this->createNewVoucherCode($purchase, $rule, $discount, 0);
                                            } else {
                                                if ($available_step > 0) {
                                                    $discount = $rule->value * $available_step;

                                                    $this->createNewVoucherCode($purchase, $rule, $discount, 0);
                                                }
                                            }
                                    }
                                } else { // percentage calculation
                                    $discount = $rule->value;

                                    $this->createNewVoucherCode($purchase, $rule, $discount, 0);
                                }
                            } else {
                                $discount = 0;
                            }
                        } else { // discount by quantity
                            if ($quantity > $rule->minimum) {
                                if ($rule->coupon_type == 'value') { // value calculation

                                    $max_voucher_value = $this->getMaxRuleVoucherValue($rule);
                                    $total_voucher = $this->getVoucherTotalValue($purchase->user_id, $rule->id);
                                    $available_step = ($max_voucher_value - $total_voucher) / $rule->value;

                                    if ($quantity > $rule->maximum) $quantity = $rule->maximum;

                                    $step = floor($quantity / $rule->step);

                                    if (($step > 0) && ($step <= $available_step)) {
                                        $discount = $rule->value * $step;

                                        $this->createNewVoucherCode($purchase, $rule, $discount, 0);
                                    } else {
                                        if ($available_step > 0) {
                                            $discount = $rule->value * $available_step;

                                            $this->createNewVoucherCode($purchase, $rule, $discount, 0);
                                        }
                                    }
                                } else { // percentage calculation
                                    $discount = $rule->value;

                                    $this->createNewVoucherCode($purchase, $rule, $discount, 0);
                                }
                            } else {
                                $discount = 0;
                            }
                        }
                    }
                } else {
                    $discount = 0;
                }
            }

            return $discount;
        }
    }

    public function generate_purchase_with_purchase($purchase)
    {
        $today = Carbon::now();

        $rule = Rule::where([
            ['rule_type', 'purchase'],
            ['start_date', '<=', $today],
            ['end_date', '>=', $today]
        ])->first();

        if($rule) {
            if($this->createNewPurchaseWithPurchase($purchase,$rule)){
                return true;
            }
        }
    }

    public function generate_refund_voucher($purchase,$items)
    {
        $rule = Rule::where([['rule_type', 'refund']])->first();

        if($rule) {
            if($this->createNewRefundVoucher($purchase,$items,$rule,1)){
                return true;
            }
        }
    }

    public function createNewPurchaseWithPurchase($purchase,$rule)
    {
        $total_purchase_amount = round($purchase->purchase_amount/100 ,2);

        $fees = $purchase->fees->where('type','shipping_fee')->first();
        $shippingFee = $fees['amount'] / 100;

        $purchase_amount = $total_purchase_amount - $shippingFee;

        if($purchase_amount >= $rule->minimum) {
            if ($purchase_amount > $rule->maximum) $purchase_amount = $rule->maximum;

            $step = floor($purchase_amount / $rule->step);

            // $discount = $rule->value * $step;
            $discount = $rule->value;

            if($this->createNewVoucherCodeWithStart($purchase, $rule, $discount, $step,"PW")) {
                return true;
            }
        }
    }

    public function generate_purchase_voucher_formula_v1($dealer, $package, $invoice_number)
    {
        // $rule = Rule::find($package->package_bp);
        $today = Carbon::now();

        $rule = Rule::where([
            ['id', $package->package_bp],
            ['rule_type', 'agent_registration'],
            ['start_date', '<=', $today],
            ['end_date', '>=', $today]
        ])->first();

        if ($rule) {
            switch ($rule->general_type) {
                case 'breakdown':
                    $step = floor($rule->value / $rule->step);
                    $lastVal = $rule->value - ($step * $rule->step);

                    $genA = $this->createNewVoucherCodeGen_V1($dealer, $package, $invoice_number, $rule, $rule->step, $step);

                    if ($lastVal > 0) {
                        $GenB = $this->createNewVoucherCodeGen_V1($dealer, $package, $invoice_number, $rule, $lastVal, 1);
                    }

                    return true;

                    break;
                case 'pairing':
                    $step = floor($rule->value / $rule->step);

                    if($rule->limit > $step) {
                        $step = $rule->limit;
                    }

                    $this->createNewVoucherCodeGen_V1($dealer, $package, $invoice_number, $rule, $rule->step, $step);

                    return true;

                    break;
                default:
            }
        }
    }

    public function createNewVoucherCode($purchase, $rule, $discount, $max_usage = 0)
    {
        $gen_code = Code::generateCode(8);
        $expiryDate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->endOfDay()->toDateTimeString();

        if ($gen_code) {

            $code = new Code;

            $code->user_id = $purchase->user_id;
            $code->country_id = country()->country_id;
            $code->coupon_code = $gen_code;
            $code->coupon_type = $rule->coupon_type;
            $code->coupon_amount = $discount;
            $code->coupon_rule_id = $rule->generate_rule_id > 0 ? $rule->generate_rule_id : $rule->id;
            $code->created_by_purchase = $purchase->inv_number;
            $code->coupon_status = $purchase->purchase_type == 'Offline' ? 'inactive' : 'valid';

            switch ($rule->expiry_date_type) {
                case 1:
                    $code->expiry = Carbon::parse($expiryDate)->addMonths($rule->breakdown_exp_date);
                    break;
                case 2:
                    $code->expiry = Carbon::parse($expiryDate)->addYear($rule->breakdown_exp_date);
                    break;
                default:
                    $code->expiry = Carbon::createFromFormat('Y-m-d', $rule->expiry_date)->endOfDay()->toDateTimeString();
            }

            $code->max_voucher_usage = $max_usage;

            if ($code->save()) {
                return true;
            }
        }
    }

    public function createNewVoucherCodeWithStart($purchase, $rule, $discount, $max_usage = 0, $startText)
    {
        $gen_code = Code::generateCode(6);
        $expiryDate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->endOfDay()->toDateTimeString();

        if ($gen_code) {

            $code = new Code;

            $code->user_id = $purchase->user_id;
            $code->country_id = country()->country_id;
            $code->coupon_code = $startText . $gen_code;
            $code->coupon_type = $rule->coupon_type;
            $code->coupon_amount = $discount;
            $code->coupon_rule_id = $rule->generate_rule_id > 0 ? $rule->generate_rule_id : $rule->id;
            $code->created_by_purchase = $purchase->purchase_number;
            $code->coupon_status = $purchase->purchase_type == 'Offline' ? 'inactive' : 'valid';

            switch ($rule->expiry_date_type) {
                case 1:
                    $code->expiry = Carbon::parse($expiryDate)->addMonths($rule->breakdown_exp_date);
                    break;
                case 2:
                    $code->expiry = Carbon::parse($expiryDate)->addYear($rule->breakdown_exp_date);
                    break;
                case 3:
                    $code->expiry = Carbon::now()->addDays($rule->breakdown_exp_date);
                    break;
                default:
                    $code->expiry = Carbon::createFromFormat('Y-m-d H:i:s', $rule->expiry_date)->endOfDay()->toDateTimeString();
            }

            $code->max_voucher_usage = $max_usage;

            if ($code->save()) {
                return true;
            }
        }
    }

    public function createNewVoucherCodeGen_V1($dealer, $package, $invoice_number, $rule, $discount, $max_usage = 0)
    {

        $gen_code = Code::generateCode(8);
        $expiryDate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->endOfDay()->toDateTimeString();

        if ($gen_code) {

            $code = new Code;

            $code->user_id = $dealer->user_id;
            $code->country_id = 'MY';
            $code->coupon_code = $gen_code;
            $code->coupon_type = $rule->coupon_type;
            $code->coupon_amount = $discount;
            $code->coupon_rule_id = $rule->generate_rule_id > 0 ? $rule->generate_rule_id : $rule->id;
            $code->created_by_purchase = $invoice_number;
            $code->coupon_status = 'valid';

            switch ($rule->expiry_date_type) {
                case 1:
                    $code->expiry = Carbon::parse($expiryDate)->addMonths($rule->breakdown_exp_date);
                    break;
                case 2:
                    $code->expiry = Carbon::parse($expiryDate)->addYear($rule->breakdown_exp_date);
                    break;
                default:
                    $code->expiry = Carbon::createFromFormat('Y-m-d', $rule->expiry_date)->endOfDay()->toDateTimeString();
            }

            $code->max_voucher_usage = $max_usage;

            if ($code->save()) {
                return true;
            }
        }
    }

    public function createNewRefundVoucher($purchase, $items, $rule, $max_usage=1)
    {
        $gen_code = Code::generateCode(7);
        $expiryDate = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->endOfDay()->toDateTimeString();
        $codeIsExist = Code::where([
            ['created_by_purchase',$purchase->receipt_number],
            ['coupon_amount',round($purchase->purchase_amount/100 ,2)],
            ['coupon_status','valid']
            ])->first();

        if ($gen_code && !$codeIsExist) {
            $code = new Code;

            $amount = 0;
            foreach ($items as $item) {
                $amount += $item->subtotal_price;
                if (array_key_exists('product_rbs_frequency',$item->product_information)) {
                    $amount = $item->subtotal_price;
                    break;
                }
            }

            if ($items->count() == $purchase->items->count()) {
                $shipping = $purchase->getFeeAmount('shipping_fee');
                $amount += $shipping;
            }


            $code->user_id = $purchase->user_id;
            $code->country_id = 'MY';
            $code->coupon_code = 'C'.$gen_code;
            $code->coupon_type = $rule->coupon_type;
            $code->coupon_amount = number_format(($amount/100), 2, '.', '');
            $code->coupon_rule_id = $rule->generate_rule_id > 0 ? $rule->generate_rule_id : $rule->id;
            $code->created_by_purchase = $purchase->receipt_number;
            $code->coupon_status = 'valid';

            switch ($rule->expiry_date_type) {
                case 1:
                    $code->expiry = Carbon::parse($expiryDate)->addMonths($rule->breakdown_exp_date);
                    break;
                case 2:
                    $code->expiry = Carbon::parse($expiryDate)->addYear($rule->breakdown_exp_date);
                    break;
                default:
                    $code->expiry = Carbon::createFromFormat('Y-m-d', $rule->expiry_date)->endOfDay()->toDateTimeString();
            }

            $code->max_voucher_usage = $max_usage;

            if ($code->save()) {
                $shippingFee = new Fee;
                $shippingFee->purchase_id = $purchase->id;
                $shippingFee->type = 'refund_fee';
                $shippingFee->name = 'Refund Fee';
                $shippingFee->amount = $code->coupon_amount*100;
                $shippingFee->shipping_information = '{"voucher_code": "'.$code->coupon_code.'", "purchase_number": "'.$purchase->purchase_number.'"}';

                if ($shippingFee->save()) {
                    return true;
                }
            }
        }
    }

    public static function CartSubtotalCanDiscount($cart)
    {
        if (count($cart) == 0) return;

        foreach ($cart as $val) {
            $global_product_id[] = CoreCache::getProductId($val->product_id);
            $product_id[] = $val->product_id;
        }

        //get product eligible_discount
        $eligible_discount = CoreCache::getProductEligibleDiscount(json_encode($global_product_id));

        if (count($eligible_discount) > 0) {
            foreach ($eligible_discount as $val) {
                $product_id_eligible_discount[] = $val->id;
            }

            // collect item can generate discount & auto apply discount = 0
            $eligible_discount_discount = collect(json_decode($cart))->whereIn('product_id', $product_id_eligible_discount)->where('rebate', 0);
            $amount = collect($eligible_discount_discount)->sum('subtotal_price');
            // $quantity = collect($eligible_discount_discount)->sum('quantity');

            $amount = ($amount / 100);

            return $amount;
        }
    }

    public static function calculateAutoApply($selectedCartItems)
    {
        $CartSubtotalCanDiscount = self::CartSubtotalCanDiscount($selectedCartItems);

        $selectedCartItemsOnly = collect($selectedCartItems)->where('selected', 1)->where('rebate', 0);
        // dd($selectedCartItemsOnly);
        $x = [];

        if (is_array($selectedCartItemsOnly) || is_object($selectedCartItemsOnly)) {
            foreach ($selectedCartItemsOnly as $val) {
                $x[$val->id] = self::AutoApplyDiscountMulti_v2($val,$CartSubtotalCanDiscount);
            }
        }

        if (isset($x) && !empty($x)) {
            $x['total'] = collect($x)->sum('discount');
        }

        return $x;
    }

    public static function AutoApplyDiscountMulti_v2($cart,$CartSubtotalCanDiscount)
    {
        $country_code = country()->country_id;
        $today = Carbon::now();

        $rules = Rule::where([
            ['country_id', $country_code],
            ['rule_type', 'auto_apply'],
            ['start_date', '<=', $today],
            ['end_date', '>=', $today]
        ])->get();

        $discount = 0;
        $discountRule = [];
        $data = ['product_order_discount' => 0];
        $cart_info = $cart->product_information;
        $cart_info = array_merge($cart->product_information, $data);

        $rule_gen = '';

        if (is_array($rules) || is_object($rules)) {
            foreach ($rules as $rule) {
                switch ($rule->limit_type) {
                    case 'ex_category':
                    case 'in_category':
                        $result = CoreCache::limit_category_product($rule->limit_type, $rule->limit_id);
                        $global_product_id = CoreCache::getProductId($cart->product_id);
                        $data = collect($result)->where('global_product_id', $global_product_id);
                        $inLimit = collect($data)->count();
                        break;
                    case 'ex_product':
                    case 'in_product':
                        $result = CoreCache::limit_category_product($rule->limit_type, $rule->limit_id);
                        $data = collect($result)->where('id', $cart->product_id);
                        $inLimit = collect($data)->count();
                        break;
                    default: //na
                        $inLimit = 1;
                }

                // dd($rules,$result,$inLimit,$cart);

                if (isset($inLimit) && !empty($inLimit) && $inLimit > 0) {
                    // $cart_subtotal_decimal = ($cart->subtotal_price / 100);

                    $cart_subtotal_decimal = $CartSubtotalCanDiscount;

                    //dd($cart->subtotal_price);

                    if ($rule->discount_type == 'quantity') {
                        if ($cart->quantity > $rule->minimum && $cart->quantity <= $rule->maximum) {
                            if ($rule->coupon_type == 'value') { // value calculation
                                $step = floor($cart->quantity / $rule->step);
                                $discount_amount = $rule->value * $step;

                                $discount += $discount_amount;
                                $discountRule[$rule->id] = round($discount_amount, 2);
                            } else { // percentage calculation
                                $discount_percentage = $cart_subtotal_decimal * ($rule->value / 100);
                                $discount += $discount_percentage;
                                $discountRule[$rule->id] = round($discount_percentage, 2);
                            }
                        } else {
                            $discount_amount = 0;
                            $discount += $discount_amount;
                            $discountRule[$rule->id] = round($discount_amount, 2);

                            //$this->updateProductOrderDiscount($cart->id,$cart_info);
                        }
                    } else {
                        if ($cart_subtotal_decimal > $rule->minimum) {
                            if ($rule->coupon_type == 'value') { // value calculation

                                if($cart_subtotal_decimal > $rule->maximum) $cart_subtotal_decimal = $rule->maximum;

                                $step = floor($cart_subtotal_decimal / $rule->step);

                                if($step > $cart->quantity) {
                                    $discount_amount = $rule->value * $cart->quantity;
                                }
                                else {
                                    $discount_amount = $rule->value * $step;
                                }

                                $discount += $discount_amount;
                                $discountRule[$rule->id] = round($discount_amount, 2);
                            } else { // percentage calculation
                                $discount_percentage = $cart_subtotal_decimal * ($rule->value / 100);
                                $discount += $discount_percentage;
                                $discountRule[$rule->id] = round($discount_percentage, 2);
                            }
                        } else {
                            $discount_amount = 0;
                            $discount += $discount_amount;
                            $discountRule[$rule->id] = $discount_amount;

                            //$this->updateProductOrderDiscount($cart->id,$cart_info);
                        }
                    }
                } else {
                    $discount_amount = 0;
                    $discount += $discount_amount;
                    $discountRule[$rule->id] = $discount_amount;
                }

                $discount = round($discount, 2);

                $cart_info['product_order_discount'] = ($discount * 100);

                self::updateProductOrderDiscount($cart->id, $cart_info);

                $rule_gen .= $rule->id;
            }
        }

        if (($cart_info['product_order_discount'] == 0)) {
            self::updateProductOrderDiscount($cart->id, $cart_info);
        }

        $discountResult = ['discount' => $discount, 'rule' => $discountRule];

        if($discount > 0 ) {

            $data = [];
            $counter = 0;

            $data['voucher'][$counter]['discount_amount'] = $discount;
            $data['voucher'][$counter]['discount_code'] = 'PP00' . $rule_gen;

            \Session::put('bjs_cart_autoApply', $data);
        }

        return $discountResult;
    }

    public static function updateProductOrderDiscount($id, $cart_info)
    {

        if ($cart_info['product_order_discount'] > 0) {
            $auto_apply = 1;
        } else {
            $auto_apply = 0;
        }

        $carts = Cart::find($id);

        $carts->product_information = $cart_info;
        $carts->auto_apply = $auto_apply;
        $carts->save();
    }

    public function getTradeInRule($id = 'all')
    {
        $country_code = country()->country_id;

        if ($id ==  'all') {
            $tradein = Rule::where([['country_id', $country_code], ['rule_type', 'tradeIn']])->get();

            if (count($tradein) > 0) {
                return $tradein;
            } else {
                return [];
            }
        } else {
            $tradein = Rule::where([['country_id', $country_code], ['rule_type', 'tradeIn'], ['id', $id]])->first(['id', 'country_id', 'name', 'rule_type', 'value']);

            if ($tradein) {
                return $tradein;
            } else {
                $value = country()->country_id == 'MY' ? 500 : 160;
                $rule = new stdClass();

                $rule->country_id = country()->country_id;
                $rule->rule_type = 'tradeIn';
                $rule->value = $value;
                return $rule;
            }
        }
    }

    public function getGlobalProductTradeInRule($productId)
    {
        $global_product_id = CoreCache::getProductId($productId);

        if ($global_product_id) {
            $rule_id = CoreCache::getGlobalProductTradeInRule($global_product_id);

            if ($rule_id) {
                $rule_data = $this->getTradeInRule($rule_id->tradein_rule);

                if ($rule_data) {
                    return $rule_data;
                }
            }
        }
    }

    public function CheckRule($quantity)
    {
        $country_code = country()->country_id;
        $today = Carbon::now()->isoFormat('YYYY-MM-DD');

        $rules = Rule::where([
            ['country_id', $country_code],
            ['rule_type', 'auto_apply'],
            ['minimum', '<', $quantity],
            ['maximum', '>=', $quantity],
            ['start_date', '<=', $today],
            ['end_date', '>=', $today]
        ])->get();

        return $rules;
    }

    public function checkAvailableRule($type)
    {

        $country_code = country()->country_id;

        $rule = Rule::where([
            ['country_id', $country_code],
            ['rule_type', $type]
        ])->first();

        if (isset($rule) && !empty($rule)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateVoucherCodeCount($coupon)
    {
        if(isset($coupon) && !empty($coupon)) {
            $code = Code::where('coupon_code', str_replace("-", "", $coupon))->first();
            $rule = Rule::where('id', $code->coupon_rule_id)->first();

            $voucher_count = $this->voucherUsageCount(str_replace("-", "", $coupon));

            if ($rule->general_type == 'breakdown' || $rule->general_type == 'pairing') {
                if ($code->usage_counter < $code->max_voucher_usage) {
                    $code->usage_counter = $voucher_count;
                    $code->save();

                    if ($code->usage_counter >= $code->max_voucher_usage) {
                        $code->coupon_status = 'used';
                        $code->save();
                    }
                }
            } else {
                if ($code->usage_counter < $rule->limit) {
                    $code->usage_counter = $voucher_count;
                    $code->save();

                    if ($code->usage_counter >= $rule->limit) {
                        $code->coupon_status = 'used';
                        $code->save();
                    }
                } else {
                    //
                }
            }
        }
    }

    public function voucherUsage($coupon)
    {
        $code = Code::where('coupon_code', str_replace("-", "", $coupon))->first();
        // $rule = Rule::where('id', $code->coupon_rule_id)->first();

        if ($code->usage_counter < $code->max_voucher_usage) {
            $code->usage_counter += 1;
            $code->save();

            if ($code->usage_counter >= $code->max_voucher_usage) {
                $code->coupon_status = 'used';
                $code->save();
            }
        } else {
            //
        }
    }

    public function voucherOfflinePaymentUpdate($purchase)
    {
        if ($purchase->purchase_status == 4002) {
            $code = Code::where([['created_by_purchase', $purchase->inv_number], ['usage_counter', 0]])->update(['coupon_status' => 'valid']);
        } else {
            $code = Code::where([['created_by_purchase', $purchase->inv_number]])->update(['coupon_status' => 'inactive']);
        }
    }

    // get voucher total value by userId per month
    public function getVoucherTotalValue($userId, $ruleId)
    {
        $firstMonth = Carbon::now();
        $endMonth = Carbon::now();

        $total = Code::where('user_id', $userId)
            ->where('coupon_rule_id', $ruleId)
            ->whereBetween('created_at', [$firstMonth->startOfMonth(), $endMonth->endOfMonth()])
            ->sum('coupon_amount');

        return $total;
    }

    public function monthlyUsageShipping($user_id,$coupon,$general_type) {

        $firstMonth = Carbon::now();
        $endMonth = Carbon::now();

        $code = Code::where('coupon_code', $coupon)->first();
        $data = Code::where('created_by_purchase', $code->created_by_purchase)->get();

        $collection = collect($data);
        $plucked = $collection->pluck('coupon_code');
        $coupons = $plucked->all();

        if($general_type == 'breakdown') {
            $voucher = fee::where(function($q) use ($coupons) {
                $q->where('shipping_information','LIKE','%'.$coupons[0].'%')
                ->orWhere('shipping_information','LIKE','%'.$coupons[1].'%');
                })->whereBetween('created_at',[$firstMonth->startOfMonth(),$endMonth->endOfMonth()])->get();
        }
        else {
            $voucher = fee::where(function($q) use ($coupon) {
                $q->where('shipping_information','LIKE','%'.$coupon.'%');
                })->whereBetween('created_at',[$firstMonth->startOfMonth(),$endMonth->endOfMonth()])->get();
        }

        if(count($voucher)) {
            foreach ($voucher as $k => $v) {
                $v->payment_status = $v->purchase->purchase_status;

                $data[] = $v;
            }

            $total = collect($data)->whereIn('payment_status',[4000,4002,4007,4009])->count();

            return $total;
        }
        else {
            return 0;
        }
    }

    public function voucherUsageCount($coupon) {

        if(isset($coupon) && !empty($coupon)) {
            $vouchers = fee::where('shipping_information','LIKE','%'.$coupon.'%')->get();

            if(count($vouchers) > 0) {

                foreach ($vouchers as $voucher) {

                    $shipping_information = json_decode($voucher->shipping_information);
                    $code = $shipping_information->voucher_code;

                    $voucher->payment_status = $voucher->purchase->purchase_status;

                    if (is_array($code) && count(array_unique($code)) < count($code)) {

                        $data[] = $voucher;
                    }

                    $data[] = $voucher;

                }

                $total = collect($data)->whereIn('payment_status',[4000,4002,4007,4009])->count();

                return $total;
            }
            else {
                return 0;
            }
        }

    }

    public function lastUsage($user_id,$coupon) {

        $shipping = fee::where('shipping_information','LIKE','%'.$coupon.'%')->orderBy('id','DESC')->first();

        if($shipping) {
            if(in_array($shipping->purchase->purchase_status,[4000,4002,4007,4009])) {
                return $shipping->created_at;
            }
            else {
                return NULL;
            }
        }
    }

    public function getCode()
    {
        $code = Code::groupBy('created_by_purchase')->orderBy('id', 'DESC')->get();

        if (count($code) > 0) {
            foreach ($code as $k => $v) {
                $v['voucher'] = $this->getCodeByPurchase($v['created_by_purchase']);

                $data[] = $v;
            }
            return response()->json($data);
        }
    }

    public function getCodeByPurchase($userId, $inv)
    {
        $code = Code::where('created_by_purchase', $inv)->get();

        if (count($code)) {
            foreach ($code as $val) {

                if ($this->lastUsage($userId, $val->coupon_code) == NULL) {
                    $val->lastUsage = '--';
                } else {
                    $val->lastUsage = Carbon::parse($this->lastUsage($userId, $val->coupon_code))->format('d/m/Y');
                }
                $val->coupon_code = implode('-', str_split($val->coupon_code, 4));
            }

            return $code;
        }
    }

    public function getUserVoucherList($userId, $countryId)
    {

        $code = Code::where([
            ['country_id', $countryId],
            ['user_id', $userId]
        ])
            ->whereNotIn('coupon_status', ['invalid', 'inactive'])
            ->groupBy('created_by_purchase')
            ->orderBy('id', 'desc')
            ->get();

        foreach ($code as $val) {

            $val->voucher = $this->getCodeByPurchase($userId, $val->created_by_purchase);
            $val->rule;

            $val->coupon_code = implode('-', str_split($val->coupon_code, 4));
            $val->expiryf = Carbon::parse($val->expiry)->format('d/m/Y');
        }

        return $code;
    }

    public function getUserVoucherListWithRule($userId, $countryId)
    {
        $code = Code::where([['discount_code.country_id', $countryId], ['discount_code.user_id', $userId]])
            ->join('discount_rule as r', 'r.id', '=', 'discount_code.coupon_rule_id')
            ->whereNotIn('discount_code.coupon_status', ['invalid', 'inactive'])
            ->groupBy('discount_code.created_by_purchase')
            ->orderBy('discount_code.id', 'desc')
            ->get();

        foreach ($code as $val) {

            $val->voucher = $this->getCodeByPurchase($userId, $val->created_by_purchase);
            $val->rule;

            $val->coupon_code = implode('-', str_split($val->coupon_code, 4));
            $val->expiryf = Carbon::parse($val->expiry)->format('d/m/Y');
        }

        return $code;
    }

    public function getMaxRuleVoucherValue($rule)
    {
        $max = $rule->maximum / $rule->step * $rule->value;

        return $max;
    }

    public function getDealerRegistrationVoucher()
    {
        $dealer = DB::table('dealer_registrations')->whereNotNull('agent_id')->get();

        if (count($dealer)) {
            return $dealer;
        }
    }

    public function getDealerRegistrationByID($agentId)
    {
        $dealer = DB::table('dealer_registrations')->where('agent_id', $agentId)->first();

        if ($dealer) {
            return $dealer;
        }
    }

    public function getDealerPackage($package_id)
    {
        $packageId = DB::table('dealer_packages')->where('id', $package_id)->first();

        if ($packageId) {
            return $packageId;
        }
    }

    //generate_purchase_voucher_formula_v1($rule_id)
    public function generateVoucherScript()
    {
        $dealers = $this->getDealerRegistrationVoucher();

        if (count($dealers)) {
            foreach ($dealers as $k=> $val) {
                $package = $this->getDealerPackage($val->package_id);
                $dealer_info = DealerInfo::where('account_id', $val->agent_id)->first();

                if ($package && $dealer_info) {
                    $gen[] = $this->generate_purchase_voucher_formula_v1($dealer_info, $package, $val->invoice_number);
                }
            }

            if (count($gen)) {
                return true;
            }
        }
    }

    public function generateVoucherAgentRegistration($registrationRecord)
    {
        // $registrationRecord->agent_id;
        $dealer = $this->getDealerRegistrationByID($registrationRecord->agent_id);

        if ($dealer) {
            $package = $this->getDealerPackage($dealer->package_id);
            $dealer_info = DealerInfo::where('account_id', $dealer->agent_id)->first();

            if ($package && $dealer_info) {
                if ($this->generate_purchase_voucher_formula_v1($dealer_info, $package, $dealer->invoice_number)) {
                    return true;
                }
            }
        }
    }

    public function extendsVoucherExpiryDate() {

        $voucher = Code::get();

        if(count($voucher)) {
            foreach ($voucher as $k => $v) {
                switch ($v->coupon_rule_id) {
                    case 1:
                        $v->expiry = Carbon::parse($v->expiry)->addMonths(1);
                    break;
                    case 2:
                        $v->expiry = Carbon::parse($v->expiry)->addMonths(3);
                    break;
                    case 3:
                        $v->expiry = Carbon::parse($v->expiry)->addMonths(4);
                    break;
                    case 4:
                        $v->expiry = Carbon::parse($v->expiry)->addMonths(6);
                    break;
                    case 5:
                        $v->expiry = Carbon::parse($v->expiry)->addMonths(5);
                    break;
                    case 6:
                        $v->expiry = Carbon::parse($v->expiry)->addMonths(6);
                    break;
                    default:
                }

                $this->updateVoucherDate($v);
            }

            return true;
        }

    }

    public function updateVoucherCount() {
        $code = Code::all();

        if(count($code)) {
            foreach ($code as $v) {
                $this->updateVoucherCodeCount($v->coupon_code);
            }

            return true;
        }
    }

    public function ResetVoucherCount() {
        $code = Code::where('coupon_status','used')->update(['coupon_status' => 'valid','usage_counter' => 0]);

        if($code) {
            return true;
        }
    }

    public function updateVoucherDate($data) {
        $code = Code::find($data->id);

        $code->expiry = $data->expiry;

        if($code->save()) {
            return true;
        }
    }






}
