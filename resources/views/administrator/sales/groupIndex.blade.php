@extends('layouts.management.main-management')
@section('content')

<style>
    html * {
        font-family: 'Lato', sans-serif;
    }


    .box {
        box-shadow: 0px 1px 5px 0px #ccc;
        margin: 0px 0px 40px 0px;
        background-color: #fff;
        border-radius: 10px;
        padding: 15px 25px;

    }

    /* .half-box {
      height: 49vh;
    } */

    .box-header {
        background-color: transparent;
        border: none;

    }

    .box-subtitle {
        text-align: right;
        color: #ffcc00;
        font-size: 15px;
        font-weight: 500;
        vertical-align: bottom;
    }

    .box-title {

        font-size: 16px;
        font-weight: 800;

    }

    .box-body {
        padding: 0px 20px 10px 20px;
    }

    .bar-legend,
    .donut-legend ul li {
        list-style: none;
        text-align: left;
        line-height: 1.9;

    }

    .agent-name,
    .group-name {
        text-align: start;
        font-size: 21px;
    }

    .agent-sales,
    .group-sales {
        text-align: end;
        font-size: 21px;
        font-weight: 800;
    }

    .bar-legend,
    .donut-legend ul li span {
        width: 15px;
        height: 15px;
        display: inline-block;
        margin-right: 5px;
        list-style-position: inside;
    }

    .apexcharts-toolbar {
        display: none;
    }



    .middle-box {
        /* height: 100vh; */
        box-shadow: 0px 1px 20px 0px grey;
        margin: 0px 0px 20px 0px;
        width: 95%;
        border-radius: 20px;
        padding: 20px;
    }

    .amountSale {
        line-height: 1;
        font-weight: 900;
        font-size: 22px;
        margin-bottom: 0;
    }

    .imgTotal {
        width: 85px;
    }

    @media screen and (max-width: 600px) {
        .sales-data .sales-product .col-md-4 {

            text-align: left;
        }

        .donut-legend {
            visibility: hidden;
            clear: both;
            float: left;
            margin: 10px auto 5px 20px;
            width: 28%;
            display: none;
        }

        .sale-list tr {
            border-bottom: 1px solid #d3d3d37a;
        }

        .amountSale {
            font-size: 25px;
        }

        .imgTotal {
            width: 65px;
        }

        .app-body {
            margin-top: 0;
        }
    }

    .numberCircle {
        border-radius: 100%;
        min-width: 20px;
        min-width: 20px;
        padding: 4px 6px;
        display: inline-block;
        background: #ffcc00;
        border: 2px solid #ffcc00;
        color: #000;
        text-align: center;
        line-height: 10px;
        margin-right: 5px;
        font: 10px Arial, sans-serif;
        font-weight: bolder
    }

    .itemTitle {

        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .uppercase {
        color: #ffcc00;
        text-transform: uppercase;
    }

    .dateTitle {
        font-size: 18px;
        color: #999;
    }

    .sales-data .sales-product .col-md-4,
    .sales-data .col-12 {
        margin-bottom: 0px !important;
    }

    .date {


        text-align: center;
        font-size: 120%;
        font-weight: bold;
    }

    .col-12.col-md-4 {
        margin: 0 auto;

    }
</style>

<div class="container sales-data">
    <div class="row">
        <div class="col-12 text-center">
            <h2><strong>{{$groupName}} Group Report by Date</strong></h3>
                <div class="col-12 col-md-4">
                    <div class="btn-group">
                        <input type="text" name="sales_date" id="sales_date" class="form-control date"
                            data-date="{{$currentday}}">
                        <input type="submit" id="date_submit"
                                class="btn btn-primary" value="Change Date">
                    </div>


                    @if(!$manager_view)
                    <div><a href="/management/sales/group/{{$group_id}}" class="btn">Back {{$groupName}} Group to Summary</a></div>
                    @else
                    <div><a href="/management/sales/groupsales" class="btn">Back {{$groupName}} Group to Summary</a></div>
                    @endif

                </div>
                <br />
        </div>
    </div>
    <div class="row">
        @foreach( $data as $key => $reportdata)
        <div class="col-12">
            <div class="box">
                <div class="row">
                    <div class="col-12">
                        <h3><strong>{{$key}} Report</strong> <span class="dateTitle">({{$reportdata['date']}})</span>
                        </h3>
                        <hr />
                    </div>

                    <div class="col-md-3">
                        <p class="box-title uppercase">Full Payment Sales</p>
                        <p class="amountSale">RM {{ number_format(($reportdata['saledata']['total']), 2) }}</p>
                        <p class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">{{$key}}
                            Revenue: {{$reportdata['saledata']['case']}} Case </p>
                    </div>
                    <div class="col-md-3">
                        <p class="box-title uppercase">Voucher Discounted</p>
                        <p class="amountSale">-RM {{ number_format(($reportdata['voucherdata']['total']), 2) }}</p>
                        <p class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">{{$key}}
                            Voucher Used: {{$reportdata['voucherdata']['case']}} Codes</p>
                    </div>
                    <div class="col-md-3">
                        <p class="box-title uppercase">Mes Pay Sales</p>
                        <p class="amountSale">RM {{ number_format(($reportdata['mpdata']['total']), 2) }}</p>
                        <p class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">{{$key}}
                            Mes Pay: {{$reportdata['mpdata']['case']}} Case</p>
                    </div>
                    <div class="col-md-3">
                        <p class="box-title uppercase">Recruitment</p>
                        <p class="amountSale">{{$reportdata['agents']}} NEW AGENTS
                        </p>
                        <p class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">{{$reportdata['custs']}}
                            New Customers<br /> </p>
                    </div>
                    @if($reportdata['saledata']['total'])
                    <div class="col-md-3">

                        <p class="box-title uppercase">Top Performance Agent</p>
                        <p class="amountSale">Coming Soon</p>
                        {{-- @if(current($reportdata['group']))
                        <p class="amountSale">RM {{ number_format( current($reportdata['group'])['total'], 2)}}
                        </p>
                        <p class="subSale" style="color:grey; font-size:16px; margin-bottom:0;">
                            {{$groups[current($reportdata['group'])['id']]}} Group
                            Revenue: {{current($reportdata['group'])['case']}} Case<br /> </p>
                        @endif --}}
                    </div>
                    <div class="col-12 sales-product">
                        <div class="row">
                            <div class="col-12">
                                <hr />
                                <p class="box-title uppercase">Top 20 Sales Products</p>
                            </div>

                            @foreach( $reportdata['actual_data'] as $datakey => $salesdata)
                            <div class="col-md-4">
                                <p style='font-size:12px' class="itemTitle"><span
                                        class="numberCircle">{{ $salesdata }}</span> <strong>{{ $datakey }}</strong>
                                </p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12 sales-product">
                        <div class="row">
                            <div class="col-12">
                                <hr />
                                <p class="box-title uppercase">Agent Performance</p>
                            </div>
                            <div class="col-md-4">
                                Coming Soon.
                            </div>
                            {{-- @foreach( $reportdata['group'] as $datakey => $groupdata)
                            <div class="col-md-4">
                                <p style='font-size:12px' class="itemTitle"><span
                                class="numberCircle">{{ $groupdata['case'] }}</span> <strong><a
                                    href="/management/sales/group/{{$datakey}}/{{$dateArray['day']}}/{{$dateArray['month']}}/{{$dateArray['year']}}">{{$groups[$datakey]}}
                                    Group</a> - RM{{ number_format($groupdata['total'], 2) }} - {{ $groupdata['new'] }}
                                New Agents</strong>
                            </p>
                        </div>
                        @endforeach
                        --}}
                    </div>
                </div>

                <div class="col-12 sales-product">
                    <div class="row">
                        <div class="col-12">
                            <hr />
                            <p class="box-title uppercase">Area Sales</p>
                        </div>

                        @foreach( $reportdata['area'] as $datakey => $areadata)
                        <div class="col-md-4">
                            <p style='font-size:12px' class="itemTitle"><span
                                    class="numberCircle">{{ $areadata['case'] }}</span> <strong> {{ $states[$datakey] }}
                                    - RM{{ number_format($areadata['total'], 2) }} </strong>
                            </p>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
</div>

@push('script')
<script>
    $( function() {
        $( ".date" ).datepicker({

          dateFormat: 'dd/mm/yy',

          onSelect: function(dateText) {

            @if(!$manager_view)
                location.href = '/management/sales/group/{{$group_id}}/'+dateText;
            @else
                location.href = '/management/sales/groupsales/'+dateText;
            @endif
             }
        });


       date = $( ".date" ).data("date") ;
        $( ".date" ).val(date);

        $("#date_submit").click(function(){

            $( ".date" ).datepicker("show");

        });
    });

</script>
@endpush


@endsection
