<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductPrices extends Model
{
    // Set table
    protected $table = 'panel_product_prices';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function model()
    {
        return $this->morphTo();
    }

    // Search
    public function scopesearchFilter($query,  $searchFilter, $selectSearch, $categories)
    {
        if (isset($searchFilter) && !empty($searchFilter)) {
            if (isset($selectSearch) && $selectSearch == 'NAME') {
                return $query
                    ->leftJoin('panel_product_attributes', 'panel_product_attributes.id', '=', 'panel_product_prices.model_id')
                    ->leftJoin('panel_products', 'panel_products.id', '=', 'panel_product_prices.model_id')
                    ->leftJoin('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
                    ->leftJoin('piv_category_product', 'piv_category_product.product_id', '=', 'panel_products.global_product_id')
                    ->orWhere('global_products.name', 'like', '%' . $searchFilter . '%')
                    //->orWhere('piv_category_product.category_id', '=', $categories)
                    ->orWhere('panel_product_attributes.attribute_name', 'like', '%' . $searchFilter . '%');
            } else {
                return $query
                    ->where('product_code', 'like', '%' . $searchFilter . '%');
            }
        } else {
            if (isset($categories) && !empty($categories)) {
                return $query
                    ->leftJoin('panel_product_attributes', 'panel_product_attributes.id', '=', 'panel_product_prices.model_id')
                    ->leftJoin('panel_products', 'panel_products.id', '=', 'panel_product_prices.model_id')
                    ->leftJoin('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
                    ->leftJoin('piv_category_product', 'piv_category_product.product_id', '=', 'panel_products.global_product_id')
                    ->orWhere('piv_category_product.category_id', '=', $categories);
            }
        }

        return $query;
    }
}
