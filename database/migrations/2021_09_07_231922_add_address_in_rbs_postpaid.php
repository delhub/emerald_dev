<?php

use App\Models\Rbs\RbsPostpaid;
use App\Models\Users\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressInRbsPostpaid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rbs_postpaid', function (Blueprint $table) {
            $table->longText('shipping_address')->after('active')->nullable();
        });

        $postpaids = RbsPostpaid::all();

        foreach ($postpaids as $postpaid) {
            $user = User::where('id',$postpaid->user_id)->first();
            $address = $user->userInfo->cartShippingAddress;

            // dd($address);

            $defaultAddress = (array(
                'address1' => $address->address_1,
                'address2' => $address->address_2,
                'address3' => $address->address3,
                'postcode' => $address->postcode,
                'city' => $address->city_key,
                'state' => $address->state_id,
                'name' => $user->userInfo->full_name,
                'phone' => $user->userInfo->mobileContact->contact_num,
            ));

            $postpaid->shipping_address = $defaultAddress;
            $postpaid->save();

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rbs_postpaid', function (Blueprint $table) {
            $table->dropColumn('shipping_address');
        });
    }
}
