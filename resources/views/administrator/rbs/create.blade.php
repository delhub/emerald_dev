@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/rbs" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">

                <div class="col-12">
                    <h4>
                        Create RBS
                    </h4>
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.rbs.store') }}" id="edit-rbs-form" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                Rbs Label<small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input type="" id="rbs_label" name="rbs_label" class="form-control" @if ($rbsReminder) value="{{ $rbsReminder->rbs_label }}" @else placeholder="Every 2 months" @endif >

                            @error('rbs_name')
                                <div class="form-text text-danger">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto">
                            <p>
                                RBS Months<small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <input id="rbs_month" name="rbs_month" class="form-control @error('no_bar') is-invalid @enderror" @if ($rbsReminder) value="{{ $rbsReminder->rbs_data }}" @else placeholder="2" @endif>

                            @error('rbs_month')
                                <div class="form-text text-danger">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 text-md-right my-auto" >
                            <p>
                                RBS Status<small class="text-danger">*</small>
                            </p>
                        </div>
                        <div class="col-12 col-md-8 form-group">
                            <select name="rbs_status" id="rbs_status" class="form-control" style="width: 100%;">
                                <option value="active" {{ $rbsReminder && $rbsReminder->rbs_status = 'active' ? 'selected' : '' }} >Active</option>
                                <option value="inactive" {{ $rbsReminder && $rbsReminder->rbs_status = 'active' ? 'selected' : '' }}>Inactive</option>
                                </select>
                        </div>
                    </div>
                    <input type="hidden" name="option_type" id="" value="{{ request()->type }}">
                    <input type="hidden" name="rbs_id" id="" value="{{ request()->id ?? 'new' }}">
                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

