<?php

namespace App\Http\Controllers\Warehouse\WarehouseAPIController;

use Illuminate\Http\Request;
use App\Models\Warehouse\Order\WarehouseOrder;
use App\Http\Controllers\API\ResponseController;
use App\Models\Products\ViewInventoriesStock;
use App\Http\Controllers\WarehouseAPI;
use App\Http\Controllers\Warehouse\Order\OrderController;
use App\Models\Products\ProductAttribute;
use App\Models\Products\WarehouseBatchItem;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundle;
use App\Http\Controllers\Warehouse\Bunbundle\BunbundleController;
use App\Models\Products\WarehouseInventories;
use App\Models\Warehouse\Bunbundle\WarehouseBunbundleItem;

class WarehouseAPIController extends ResponseController
{
    public function index()
    {
        //code
    }
    public function stockCheck(Request $request)
    {
        $success = array();
        $locationId = $request->location_id;

        $inventoryStock = ViewInventoriesStock::where('outlet_id', $locationId)->get();

        foreach ($inventoryStock as $key => $stock) {
            $success[] = [
                'product_code' => $stock->product_code,
                'virtual_stock' => $stock->virtual_stock,
            ];
        }

        return $this->sendResponse($success);

        return view('warehouse.location.index', compact('tables'));
    }

    public function batchOrder(Request $request)
    {
        $post_data =  $request->json()->all();
        $order_data = $arryaData = $items_id = array();

        foreach ($post_data as $key => $data) {

            $locationId = $data['outlet_id'];
            $inv_type = $data['inv_type'];

            $order_data['location_id'] = $locationId;
            $order_data['inv_type'] = $inv_type;
            $order_data['type']  = $data['type'];
            $order_data['date'] = $data['date'];
            $order_data['invoice_number'] = $data['invoice_number'];
            $order_data['delivery_order'] = $data['delivery_order'];

            $existingWarehouseOrder = WarehouseOrder::where('invoice_number', $data['invoice_number'])->exists();
            if ($existingWarehouseOrder) continue;

            $warehouseOrderId = OrderController::process_WarehouseOrder($order_data);
            $quantity = 0;
            foreach ($data['items'] as $item) {
                $inv_user = 0;
                $itemProductCode = $item['product_code'];
                $items_id = $item['items_id'];

                $repeatable = 0;
                $productAttribute = ProductAttribute::where('product_code', $itemProductCode)->first();
                if ($productAttribute && $productAttribute != NULL) $repeatable = $productAttribute->repeatable;

                if (empty($item['bundle'])) {
                    $quantity = count($item['items_id']);
                } else {
                    // auto bundle
                    $warehouseBatchItems = WarehouseBatchItem::whereIn('items_id', $item['items_id'])->get();

                    if ($warehouseBatchItems->isEmpty()){
                        $quantity =  $item['quantity'];
                        continue;
                    }

                    foreach ($warehouseBatchItems as $key => $warehouseBatchItem) {
                        $bundleProductCodeOnly[$warehouseBatchItem->batchDetail->product_code] = $warehouseBatchItem->batchDetail->product_code;
                    }

                    //need see this
                    $quantity = $item['quantity'];
                    $toProduct['product_code'] = $item['product_code'];

                    ///start here
                    $batchItemss = WarehouseBatchItem::whereIn('items_id', $item['items_id'])->get();

                    $warehouseBunbundle = new WarehouseBunbundle;
                    $warehouseBunbundle->user_id = 0;
                    $warehouseBunbundle->location_id = $locationId;
                    $warehouseBunbundle->type = 'bundle';
                    $warehouseBunbundle->from_product_code = json_encode($bundleProductCodeOnly);
                    $warehouseBunbundle->product_code =  $toProduct['product_code'];
                    $warehouseBunbundle->quantity = $item['quantity'];
                    $warehouseBunbundle->expiry_date = '';
                    $warehouseBunbundle->panel_id = 2011000000;
                    $warehouseBunbundle->bundle_type = 1;
                    $warehouseBunbundle->remark = 'auto' . $data['invoice_number'] . json_encode($bundleProductCodeOnly);

                    $warehouseBunbundle->save();

                    foreach ($batchItemss as $key => $bItem) {
                        $bdlItem = new WarehouseBunbundleItem;

                        $bdlItem->b_unbundle_key = $warehouseBunbundle->id;
                        $bdlItem->batch_item_id = $bItem->id;

                        $bdlItem->save();
                    }

                    //auto create bundle/batch/mark batch item
                    $request->stockTransferId = $warehouseBunbundle->id;
                    $request->picking_order_id = $warehouseOrderId;
                    $request->process = 'pos';
                    // $request->imAuto = 'yes'; 
                    BunbundleController::end($request);
                }

                $arryaData[] = [
                    'product_name' => $item['product_name'],
                    'product_code' => $itemProductCode,
                    'bundle' => $item['bundle'],
                    'quantity' => - ($quantity),
                    'items_id' => $item['items_id']
                ];

                $inventoryRecords[$itemProductCode]['outlet_id'] = $locationId;
                $inventoryRecords[$itemProductCode]['inv_type'] = $inv_type;
                $inventoryRecords[$itemProductCode]['inv_amount'] = - ($quantity);
                $inventoryRecords[$itemProductCode]['inv_user'] = $inv_user;
                $inventoryRecords[$itemProductCode]['repeatable'] = $repeatable;
                $inventoryRecords[$itemProductCode]['model_type'] = 'App\Models\Warehouse\Order\WarehouseOrder';
                $inventoryRecords[$itemProductCode]['model_id'] = $warehouseOrderId;
                $inventoryRecords[$itemProductCode]['product_code'] = $itemProductCode;
            }

            //update warehouse_batch_item
            $batchItems = WarehouseBatchItem::whereIn('items_id', $items_id)
                ->where('repeatable', 0)
                ->get();

            if ($batchItems && empty($item['bundle'])) {
                foreach ($batchItems as $key => $batchItem) {
                    $batchItem->picking_order_id = $warehouseOrderId;
                    $batchItem->type = 'pos';
                    $batchItem->save();
                }
            }

            $order_data['order_data'] = json_encode($arryaData);

            $WarehouseOrder = WarehouseOrder::where('id', $warehouseOrderId)->first();
            $WarehouseOrder->order_data = json_encode($arryaData);

            $WarehouseOrder->save();

            $existingWarehouseInventory = WarehouseInventories::where('model_type', get_class(new WarehouseOrder()))->where('model_id', $warehouseOrderId)->exists();
            if ($existingWarehouseInventory) continue;

            OrderController::createNew(array('inventoryRecords' => $inventoryRecords));

            unset($order_data, $arryaData, $inventoryRecords, $items_id);
        }

        return $post_data;
    }

    public function getBatch()
    {
        $batch = WarehouseAPI::getBatch();

        return $batch;
    }
}
