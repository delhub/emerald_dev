@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">RBS Prepaid</h1>
        </div>
    </div>
</div>

<form action="{{ route('administrator.ordertracking.rbs-prepaid') }}" method="GET">
    <div class="row mt-2 ml-2">
        <!-- Status -->
        <div class="col-md-auto px-1">
            <select name="status" class="col-12 text-left custom-select">
                <option value="all">All Status</option>
                <option value="active" {{ $request->status == 'active' ? 'selected' : ''}}>Active</option>
                <option value="completed" {{ $request->status == 'completed' ? 'selected' : ''}}>Completed</option>
                <option value="cancelled" {{ $request->status == 'cancelled' ? 'selected' : ''}}>Cancelled</option>
            </select>
        </div>

        <!-- Date filter from date to date-->
        <div class="col-md-auto px-1">
            <div class="input-group mb-3">
                <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                    aria-label="Username" aria-describedby="basic-addon1"
                    value="{{ ($request->datefrom) ? $request->datefrom : '' }}" autocomplete="off">
            </div>
        </div>

        <div class="col-md-auto px-1">
            <div class="input-group mb-3">
                <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                    aria-label="Username" aria-describedby="basic-addon1"
                    value="{{ ($request->dateto) ? $request->dateto : '' }}" autocomplete="off">
            </div>
        </div>

        <div class="col-md-auto ml-auto">
            <div class="row">
                <div class="col-md-8 p-0">
                    <select class="custom-select" id="selectSearch" name="selectSearch">
                        <option value="ALL" selected>Choose...</option>
                        <option value="BJN" {{ $request->selectSearch == 'BJN' ? 'selected' : '' }}>Order Number
                        </option>
                        <option value="PODO" {{ $request->selectSearch == 'PODO' ? 'selected' : '' }}>PO / DO Number
                        </option>
                        <option value="NAME" {{ $request->selectSearch == 'NAME' ? 'selected' : '' }}>Product Name
                        </option>
                        <option value="CODE" {{ $request->selectSearch == 'CODE' ? 'selected' : '' }}>Product Code
                        </option>
                    </select>

                    <input type="text" class="form-control" id="" name="searchBox"
                        value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                </div>
                <div class="col-md-4 ">
                    <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                </div>
            </div>
        </div>
    </div>

    <div style="text-align:center;">
        <div class="col-md-auto px-1">
            <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
            <button type="submit" class="btn btn-warning" style="color:black;"><a
                    href={{ route('administrator.ordertracking.rbs-prepaid') }} style="color:black;">Reset
                    Filter</a></button>
            {{-- <button type="submit" class="btn btn-warning" style="color:black;"><a
                    href="{{route('administrator.ordertracking.exportCsv', ['status'=> $request->status, 'states'=> $request->states, 'datefrom' => $request->datefrom, 'dateto' =>  $request->dateto, 'stock_filter' =>  $request->stock_filter, 'selectSearch' => $request->selectSearch , 'searchBox' => $request->searchBox])}}"
                    id="export" style="color:black; ">Download</a></button> --}}
        </div>
    </div>

    <br>

    <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Item) :
            <b>{{ count($rbsPrepaids) }}</b></span></p>
    <div class="row">
        <div class="col-auto ml-auto">
            {{-- {!! $customerOrders->render() !!} --}}
        </div>
    </div>
</form>

<div class="row">
    <div class="col-12">
        <div class="tableFixHead">
            <table class="table table-bordered table-striped text-center" id="sales-tracking" style="width:100%" cellspacing="0" cellpadding="0">
                <thead style="opacity: 1">
                        {{-- <th scope="col"><input type="checkbox" id="checkAll"/>Action</th> --}}
                        <th scope="col">Purchase Number</th>
                        <th scope="col">Purchase Date</th>
                        {{-- <th scope="col">RBS Status</th> --}}
                        <th scope="col">Product Details</th>
                        <th scope="col">DO Number</th>
                        <th scope="col">Recurring Booking Date</th>
                        <th scope="col">Ship Out Date</th>
                        <th scope="col">Received Date</th>
                </thead>
                <tbody>
                    @foreach ($rbsPrepaids as $key => $rbsPrepaid)
                    @php
                        $purchase = $allRbsPrepaids->where('id',$key)->first();
                        $items = $purchase ? $purchase->items->first() : null;
                        $product = $items ? $items->product->parentProduct : null;
                    @endphp
                    <tr>
                        <td rowspan="{{ count($rbsPrepaid) +1 }}">{{ $purchase->purchase_number  }}</td>
                        <td rowspan="{{ count($rbsPrepaid)+1 }}">{{ $purchase->purchase_date  }}</td>
                        <td rowspan="{{ count($rbsPrepaid)+1 }}">
                            <strong>
                                {{ $product ? $product->product_code: null }}
                            </strong>
                            <br>
                            {{ $product ? $product->name: null }}
                            <br>
                            @if ($items)
                            @endif
                        </td>
                    </tr>
                    @foreach ($rbsPrepaid as $rbsOrder)
                    <tr>
                        <td>{{ $rbsOrder->order_number }}</td>
                        <td>{{ $rbsOrder->order_date }}</td>
                        <td>{{ $rbsOrder->shipping_date }}</td>
                        <td>{{ $rbsOrder->collected_date }}</td>
                    </tr>
                    {{-- @endif --}}

                    @endforeach
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <div class="col-auto ml-auto py-3">
        {{-- {!! $customerOrders->render() !!} --}}
    </div>

</div>

@push('style')
<style>
    .text-bold {
        font-weight: bold;
    }

    .table .thead-light th {
        font-weight: bold;
        font-family: 'Nunito', sans-serif;
    }

    @media(max-width:767px) {
        .hidden-sm {
            display: none;
        }

    }

    @media(min-width:767px) {
        .hidden-md {
            display: none;
        }
    }


    .text-font-family {
        font-family: 'Nunito', sans-serif;
    }

    /* freeze header  */
    .tableFixHead {
        overflow-y: auto;
        height: 100vh;
    }

    .tableFixHead thead th {
        position: sticky;
        top: 0;
        z-index: 1;
        border: 1px solid lightgray;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        background: #eee;
    }
</style>
@endpush

@push('script')
<script>
    $(function() {
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
                });

                $(".estimatedateonly").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                changeMonth: true,
                changeYear: true
                });

                $(".filterDate").datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                changeMonth: true,
                changeYear: true
            });
        });

        function showButton(itemID) {
            var opt = document.getElementById("courier_name" + itemID).value;
            console.log('opt');
            console.log(opt);
            if(opt == 'POSLAJU'){
                $("#createShipment_" +itemID).show();
            }else{
                $("#createShipment_" +itemID).hide();
            }
        }

        $(document).ready(function () {

            //check all checkbox
            $("#checkAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Get checked id
            $("#bulkactionform").submit(function(){
                var checked = new Array();
                var ids = new Array();

                checked = $('input[name="itemsid[]"]:checked');

                for (i = 0; i < checked.length; i++) {
                    ids.push(checked[i].value);
                    }

                var stringids = ids.toString();
                $("#action_ids").val(stringids);
            });

            //show or hide when selected bulk action
            $("#bulkaction").click(function(){

                if(bulkaction.value == 'combine_do' || bulkaction.value == 'splitPOnDO'){

                document.getElementById("action_date").style.display = "none";
                }else if(bulkaction.value == 'mark_order_date'){

                    document.getElementById("action_date").className = "estimatedateonly form-control";

                    $(".estimatedateonly").datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else if(bulkaction.value == 'mark_actual_ship_date'){

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else{

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }
            });
        });

        function getShipmentData(itemID) {
            console.log(itemID);
            var element = document.getElementById("spinnerSubmit" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/create_shipment/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("createShipment_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Shipment Create";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        alert(result);


                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuCheckout(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/poslaju_checkout/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("checkout_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Checkout";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuShip(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/poslaju_ship/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        // button.disabled = true;
                        // button.innerHTML = "Checkout";
                        // $("#actualSubmit").submit();
                    } else {
                        // element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function exportTasks(_this) {
            let _url = $(_this).data('href');
            window.location.href = _url;
        }

</script>
@endpush

@endsection