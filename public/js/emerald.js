$(document).ready(() => {

    let productQlt = '';

    //cart plus and minus function
    const inputQlt = document.querySelectorAll('.varqty');
    const mainBodyget = document.querySelector('#body-content-collapse-sidebar');

    inputQlt.forEach(item => {
        productQlt = 1;
        item.value = productQlt;
        getValueQly(productQlt);
        //console.log(item.value);
        item.addEventListener('keyup', (e) => {
            e.preventDefault();
            productQlt = Number(item.value);
            //console.log(typeof productQlt, productQlt);
            if (productQlt >= 50) {
                item.value = 50;
                productQlt = item.value;
                popFuncOverLimit();
            } else {

            };

            getValueQly(productQlt);
        });
    })

    $('.product-minus-btn').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest('div').find('input');
        var value = parseInt($input.val());
        var $price = $this.parent().parent().parent().find('.subtotalPrice');
        var unitPrice = parseInt($price.data('unit-price'));

        if (value > 1) {
            value = value - 1;
        } else {
            value = 1;
        }

        $input.val(value);
        productQlt = value;
        //console.log(productQlt);
        getValueQly(productQlt);
        subtotalPrice = (unitPrice * value) / 100;
        $price.text(subtotalPrice.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    });

    // $('.product-add-btn').on('click', function (e) {
    $('.product-add-btn').on('click', function (e, notAdd) {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest('div').find('input');
        var value = parseInt($input.val());
        var $price = $this.parent().parent().parent().find('.subtotalPrice');
        var unitPrice = parseInt($price.data('unit-price'));

        let item = $this.parent().parent().parent().find('.itemChecked')
        let itemName = $this.parent().parent().parent().find('.the-product-name').text()
        let itemAttribute = $this.parent().parent().parent().find('.the-product-attribute').text().trim()
        let eligibleLimit = item.data('purchase-limit-eligible')
        let limitAmount = item.data('purchase-limit-amount')
        let limitPeriod = item.data('purchase-limit-period')
        let purchaseLeft = item.data('purchase-limit-purchase-left')

        if (purchaseLeft < 0) {
            purchaseLeft = 0
        }

        if (typeof notAdd !== 'undefined' && notAdd && value >= 50) {
            value = 50;
            popFuncOverLimit();
        } else if (typeof notAdd == 'undefined') {
            if (value < 50) {
                value = value + 1;
            } else {
                value = 50;
                popFuncOverLimit();
            }
        }

        if (eligibleLimit && value > purchaseLeft) {
            let itemData = {
                name: itemName,
                attribute: itemAttribute,
                amount: limitAmount,
                period: limitPeriod,
                purchaseLeft: purchaseLeft
            };
            let popupData = [itemData];
            popFuncPurchaseLimit(popupData);
            value = value - 1;
        }

        $input.val(value);
        productQlt = value;
        //console.log(productQlt);
        getValueQly(productQlt);
        subtotalPrice = (unitPrice * value) / 100;
        $price.text(subtotalPrice.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    });

    //if the item limit is over 999
    function popFuncOverLimit() {
        const limitOver = document.createElement('div');
        limitOver.innerHTML = `
        <div class="modal fade" id="abc" aria-hidden="true" aria-labelledby="limitProduct" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered edit-popup-bg">
                <div class="modal-content edit-info-modal-box">
                    <div class="modal-header">
                        <h1 class="modal-title edit-tittle" id="limitProduct"><span>Warning</span></h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body limit-over">
                        <p>Each person limit to 50 units</p>
                    </div>
                </div>
            </div>
        </div>
        `;
        mainBodyget.appendChild(limitOver);
        //console.log('item over');
        $('#abc').modal('show')
    }

    function getValueQly(productQlt) {
        gg = productQlt;
        getVuluetoProductQly(gg);
    }

    function getVuluetoProductQly(gg) {
        getAllQlyTot = gg;
    };

    //vedio images click
    $('#play-video').on('click', function (ev) {
        $('#play-video').css('display', 'none');
        $("#top-video").css('display', 'block');
        $("#top-video")[0].src += "&autoplay=1";
        ev.preventDefault();

    });

    $('#play-video02').on('click', function (ev) {
        $('#play-video02').css('display', 'none');
        $("#top-video02").css('display', 'block');
        $("#top-video02")[0].src += "&autoplay=1";
        ev.preventDefault();

    });

    //menu hide/show functions/ cover bg
    //footer menu add to cart
    const ftCartOpn = document.querySelector('.ft-cart-nav .item02');
    const ftBuynowOpn = document.querySelector('.ft-cart-nav .item03');
    const ftUserOpn = document.querySelector('.ft-cart-nav .item01');
    const ftCartCls = document.querySelectorAll('.close-cart');
    const ftCloseCls = document.querySelectorAll('.ft-tab');
    const ftCartOne = document.querySelector('.user-edit');
    const ftCart = document.querySelector('.ft-menu-mb');
    const menuToplayermb = document.querySelector('#coverBg');
    const menuGroupft = document.querySelector('.fml-head');
    const clsBn = document.querySelector('.link-product.buy_now');
    const clsAc = document.querySelector('.link-product.add_cart');
    const menuCOfunc = document.querySelector('#userContent');
    const menuUser = document.querySelector('.menuMain');

    allFuncRunCheck();

    function openMenuFunc(e) {
        if (menuCOfunc.className == 'mn') {
            menuCOfunc.classList.add('active');
            menuGroupft.style.zIndex = "301";
            menuToplayermb.className = "cover-bg";
            menuToplayermb.style.zIndex = "300";
        } else {
            menuCOfunc.classList.remove('active');
            menuGroupft.style.zIndex = "35";
            menuToplayermb.className = "";
            menuToplayermb.style.zIndex = "35";
        }
        e.preventDefault();
    }

    function ftOpen(e) {
        menuCOfunc.classList.remove('active');
        menuToplayermb.className = "cover-bg";
        menuToplayermb.style.zIndex = "120";
        menuGroupft.style.zIndex = "32";
    }
    function ftOpenFuncOne() {
        ftCartOne.style.left = '0';
        ftCartOne.style.zIndex = "999";
        ftOpen();
    }

    function ftOpenFunc(e) {
        if (ftBuynowOpn.className === 'item03 col warning display' || ftBuynowOpn.className === 'item03 col display') {

        } else {
            ftCart.style.left = '0';
            ftCart.style.zIndex = "999";
            clsAc.style.display = "block";
            clsBn.style.display = "none";
            ftOpen();
        }
    }

    function ftOpenFuncb(e) {
        if (ftBuynowOpn.className === 'item03 col warning display' || ftBuynowOpn.className === 'item03 col display') {

        } else {
            ftCart.style.left = '0';
            ftCart.style.zIndex = "999";
            clsBn.style.display = "block";
            clsAc.style.display = "none";
            ftOpen();
        }

        // e.preventDefault();
    }

    function ftCloseFunc() {
        menuCOfunc.classList.remove('active');
        menuToplayermb.className = "";
        menuToplayermb.style.zIndex = "98";
        menuGroupft.style.zIndex = "100";
        ftCloseCls.forEach(item => {
            item.style.left = '-150%';
        });
    }

    ftCartCls.forEach(cartBtn => {
        cartBtn.addEventListener('click', ftCloseFunc);
    })

    function allFuncRunCheck() {

        menuUser.addEventListener('click', openMenuFunc);
        menuToplayermb.addEventListener('click', ftCloseFunc);
        if (!ftBuynowOpn || !ftCartOpn || !ftUserOpn) {

        } else {
            ftBuynowOpn.addEventListener('click', ftOpenFuncb);
            ftCartOpn.addEventListener('click', ftOpenFunc);
            ftUserOpn.addEventListener('click', ftOpenFuncOne);
        }

    }

    //find store if select current location -want both chekbox work
    const labelBtn = document.querySelectorAll('.find-store .checkout-tick label');
    const btnCheck = document.querySelectorAll('.find-store .checkout-tick input[type="checkbox"]');

    labelBtn.forEach(item => {
        item.addEventListener('click', function (e) {
            allAddCheck();
            e.preventDefault();
        })
    });

    function allAddCheck() {
        btnCheck.forEach(itemBtn => {
            if (itemBtn.checked == true) {
                itemBtn.checked = false;
            } else {
                itemBtn.checked = true;
            }
        })
    }

    //payment method show active
    const methodActive = document.querySelectorAll('.pay-methodbox5 a');

    methodActive.forEach(item => {
        item.addEventListener('click', function () {
            item.children[0].children[0].children[0].checked = true;
        });
    });

    //icon animation
    const openBtn = document.querySelector('.open-icon');
    const subinfo = document.querySelector('.side-stickinfo.active');

    function rotateYobj(item) {

        if (subinfo.classList[1] == "active") {
            subinfo.classList.remove('active');
        } else {
            subinfo.classList.add('active');
        }

    }

    if (!openBtn) {

    } else {
        openBtn.addEventListener('click', rotateYobj);
    }

});


