<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Traits\API\ApiProduct;

class ApiCoreController extends Controller
{
    use ApiProduct;

    public function all()
    {
        return $this->getAllProduct();
    }

    public function attribute()
    {
        return $this->getAllProductAttribute();
    }

    public function price()
    {
        return $this->getAllProductPrice();
    }

    public function giftProduct()
    {
        return $this->getProductAttributeGift();
    }

    public function giftProductBy($id)
    {
        return $this->getProductAttributeGiftBy($id);
    }

    public function images()
    {
        return $this->getAllImages();
    }

    public function brand()
    {
        return $this->getAllBrand();
    }

    public function categories()
    {
        return $this->getAllCategories();
    }

    public function pivCategoryProduct()
    {
        return $this->getAllPivCategoryProduct();
    }

    public function productCatalog(Request $request)
    {
        return $this->getproductCatalog($request);
    }

    public function productSearch(Request $request)
    {
        return $this->getSearchTypeHead($request);
    }

    public function scancode(Request $request)
    {
        return $this->getScancode($request);
    }

    public function attributeBundle(Request $request)
    {
        return $this->getProductAttributeBundle($request);
    }

    public function attributeBundleConfirm(Request $request)
    {
        return $this->getProductAttributeBundleConfirm($request);
    }
}
