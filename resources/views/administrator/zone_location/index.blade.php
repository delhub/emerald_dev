@extends('layouts.administrator.main')

@section('content')

<h1>Zones Locations</h1>

    <div class="card shadow-sm">
        <div class="card-body">

            <form action="{{ route('administrator.zone_location.index') }}" method="GET">
                <div class="row">
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-12">
                                <select class="custom-select" id="selectSearch" name="selectSearch">
                                    <option value="ALL" selected>Choose...</option>
                                    <option value="LT" {{ $request->selectSearch == 'LT' ? 'selected' : '' }}>Location Type</option>
                                    <option value="LI" {{ $request->selectSearch == 'LI' ? 'selected' : '' }}>Location Id</option>
                                    <option value="ZI" {{ $request->selectSearch == 'ZI' ? 'selected' : '' }}>Zone Id</option>
                                </select>

                                <input type="text" class="form-control" id="" name="searchBox"
                                    value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <button type="submit" class="btn btn-warning" style="color:black; position:absolute; width:85%;">Search</button>
                    </div>

                    <div class="col-md-1">
                        <button type="submit" class="btn btn-warning" style="color:black; position:absolute; width:85%;"><a href={{ route('administrator.zone_location.index')}} style="color:black;">Reset Search</a></button>
                    </div>
            </form>

            <div class="table-responsive m-2">

                <table id="global-zones-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Location Type</td>
                            <td>Location Id</td>
                            <td>Zone Id</td>
                            <td>Actions</td>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($zone_locations as $zone_location)
                        <tr>
                            <td>{{$zone_location->id}}</td>
                            <td>{{$zone_location->location_type}}</td>
                            <td>{{$zone_location->location_id}}</td>
                            <td>{{$zone_location->zone_id}}</td>

                            {{-- <td style="width: 20%;">
                                <a style="color: white; font-style: normal; border-radius: 5px;" href="/administrator/zones/edit_zone_location/{{ $zone_location->id }}" class="btn btn-primary shadow-sm">Edit</a>
                            </td> --}}

                            <td style="width: 20%;">
                                <a style="color: white; font-style: normal; border-radius: 5px;" href="{{ route('administrator.zone_location.edit',[$zone_location->id]) }}" class="btn btn-primary shadow-sm">Edit</a>
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection