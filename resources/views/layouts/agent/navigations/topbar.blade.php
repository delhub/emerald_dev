@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp
<style>
    .profile-menu:active img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    .profile-menu:hover img {
        content:url("{{ asset('assets/images/icons/profile_g.png') }}");
    }

    /* .myDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .myDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .agentDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .agentDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .panelDashboard-menu:active, img{
    content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
} */
    .panelDashboard-menu:hover img {
        content:url("{{ asset('assets/images/icons/dashboard_g.png') }}");
    }

    /* .logout-menu:active, img{
    content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }*/

    .logout-menu:hover img {
        content:url("{{ asset('assets/images/icons/logout_g.png') }}");
    }

    @media (max-width: 767px) {
        /* .margin-logo-mobile {
            margin-right: -60px;
            margin-top: 10px;
        } */
    }
</style>
<header class="app-header navbar navbar-bg-color navbar-border-bottom-color nav-height ">
    <button class="navbar-toggler sidebar-toggler d-lg-none" type="button" data-toggle="sidebar-show">
        <i class="fa fa-bars navigation-icon"></i>
    </button>
    <a class="navbar-brand" href="/shop">
        {{-- @php
    dd(country()->country_id);
@endphp --}}

        <img class="navbar-brand-full margin-logo-mobile margin-tablet"
            src="{{ asset("/storage/logo/bujishu-logo-$country.png") }}" width="85" height="50" alt="Bujishu Logogg">
        <img class="navbar-brand-minimized" src="{{ asset("/storage/logo/bujishu-logo-$country.png") }}" width="75"
            height="45" alt="Bujishu Logo">
    </a>

    <h4 class="toptitle mytitlecolor hidden-sm mr-auto">@yield('page_title', '')</h4>

    <ul class="nav navbar-nav mr-3">
        <!-- <li class="nav-item d-md-down-none">
            <div class="applybut">
                <a href="{{ route('customer.membership') }}">
                </a>
            </div>
        </li> -->
        <li class="nav-item d-md-down-none dd">
            <a href="/shop/category/mattresses-bedsheets/bedsheets#scroll-to" class="nav-link" data-toggle="tooltip"
                title="Bedsheets">
                <img src="{{ asset('assets/images/icons/categories/bedsheet-icon-no-bg.png') }}" alt=""
                    style="max-height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/curtains#scroll-to" class="nav-link" data-toggle="tooltip" title="Curtains">
                <img src="{{ asset('assets/images/icons/categories/curtain-icon-no-bg.png') }}" alt=""
                    style="max-height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/lightings#scroll-to" class="nav-link" data-toggle="tooltip" title="Lightings">
                <img src="{{ asset('assets/images/icons/categories/lighting-icon-no-bg.png') }}" alt=""
                    style="max-height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/cabinets#scroll-to" class="nav-link" data-toggle="tooltip" title="Cabinets">
                <img src="{{ asset('assets/images/icons/categories/cabinet-icon-no-bg.png') }}" alt=""
                    style="max-height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/carpets#scroll-to" class="nav-link" data-toggle="tooltip" title="Carpets">
                <img src="{{ asset('assets/images/icons/categories/carpet-icon-no-bg.png') }}" alt=""
                    style="max-height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/products-and-services/paints#scroll-to" class="nav-link" data-toggle="tooltip" title="Paints">
                <img src="{{ asset('assets/images/icons/categories/paint-icon-no-bg.png') }}" alt=""
                    style="max-height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/mattresses-bedsheets/mattresses#scroll-to" class="nav-link " data-toggle="tooltip"
                title="Mattresses">
                <img src="{{ asset('storage/icons/mattress-icon-no-bg.svg') }}" alt="" style="height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/tables-and-chairs/sofas-armchairs#scroll-to" class="nav-link " data-toggle="tooltip"
                title="Sofas">
                <img src="{{ asset('storage/icons/sofa-icon-no-bg.svg') }}" alt="" style="height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/category/tables-and-chairs#scroll-to" class="nav-link " data-toggle="tooltip" title="Tables & Chairs">
                <img src="{{ asset('storage/icons/table-chair.svg') }}" alt="" style="height: 35px;">
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <div class="vertical-align text-center my-auto ">
                <form>
                    <div class="row grid-space-1">
                        <div class="my-auto display-same-row">
                            <div class="nav-content-sidebar-collapse">
                                <!-- <select class="form-control navigation-input input-lg w-25 hidden-sm border-left-rounded-10 margin-right-negative-with-border margin-right-border-color search-bar-size " name="category">
                                    <option value="all">All Categories </option>
                                    <optgroup label="Mens"></optgroup>
                                </select> -->
                                <input type="text" id="search-box" name="keyword"
                                    class="form-control navigation-input input-lg w-65-md w-80-sm margin-right-negative-with-border  margin-right-border-color border-rounded-0-md border-left-rounded-10-sm search-bar-size search-bar-size-sm"
                                    readonly>
                                <!-- <button id="search-button" class="btn navigation-input border-right-rounded-10" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-search"></i></button> -->
                                <button type="button"
                                    class="btn navigation-input border-right-rounded-10 search-button-updated"
                                    data-toggle="modal" data-target="#exampleModal"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </li>
        <li class="nav-item d-md-down-none">
            <a href="/shop/dashboard/wishlist/index" class="nav-link" data-toggle="tooltip" title="Perfect List">
                <img src="{{ asset('assets/images/icons/favorite.png') }}" alt="Perfect List" style="max-height: 25px;">
                <span id="wishlist-quantity" class="d-inline-block align-middle text-align-center"
                    style="font-size: .75rem; color: #000000; background-color: #ffffff; border-radius: 100%; min-width: 20px; min-height: 20px;">0</span>
            </a>
        </li>
        {{-- hide the db --}}
        <li class="nav-item d-md-down-none">
            <a href="/shop/cart" class="nav-link" data-toggle="tooltip" title="My Cart">
                <img src="{{ asset('assets/images/icons/cart.png') }}" alt="My Cart" style="max-height: 25px;">
                <span id="cart-quantity" class="d-inline-block align-middle text-align-center"
                    style="font-size: .75rem; color: #000000; background-color: #ffffff; border-radius: 100%; min-width: 20px; min-height: 20px;">0</span>
            </a>
        </li>
        {{-- hide the db --}}
        @if($country != 'SG')
        <li class="nav-item">
            <a href="#" class="nav-link">
                <span class="d-inline-block align-middle" style="color: #3490dc;"><strong
                        style="color: #60b7ff;">{{ $bpUsable }}</strong>/{{ $bpTotal }}</span> <img
                    src="{{ asset('assets/images/icons/blue-point.png') }}" alt="" style="max-height: 35px;">
            </a>
        </li>
        @else

        @endif
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                aria-expanded="false">
                <img class="img-avatar" src="{{asset('storage/avatar/avatar01.jpg')}}"
                    alt="{{ Auth::user()->userInfo->full_name }}">
                <i class="fa fa-caret-down" style="color: #ffcc00;"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right"
                style="background-color: #ffcc00; border-radius:30px; padding:0;">
                <a class="dropdown-item profile-menu" href="{{ route('shop.dashboard.dealer.profile') }}"
                    style="padding: 10px 0px 10px 20px;border-radius: 30px 30px 0 0;">
                    <img src="{{ asset('assets/images/icons/profile_b.png') }}" alt="Profile"
                        style="height:23px; padding:0 10px 0 0px;"> Profile
                </a>

                <a class="dropdown-item myDashboard-menu" href="{{ route('shop.dashboard.customer.home') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="My Dashboard"
                        style="height:23px; padding:0 10px 0 0px !important;">
                    My Dashboard
                </a>

                @hasrole('dealer')
                <a class="dropdown-item agentDashboard-menu" href="{{ route('agent.overview') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Agent Dashboard"
                        style="height:23px; padding:0 10px 0 0px !important;"> Agent Dashboard
                </a>
                @endhasrole

                @hasrole('panel')
                <a class="dropdown-item panelDashboard-menu" href="{{ route('management.panel.home') }}">
                    <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Panel Dashboard"
                        style="height:23px; padding:0 10px 0 0px !important;"> Panel Dashboard
                </a>
                @endhasrole
                @hasrole('redemption')
                <a href="{{ route('administrator.redemption') }}" class="dropdown-item redemptionDashboard-menu"><img
                        src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Administrator"
                        style="height:23px; padding:0 10px 0 0px !important;"> Redemption</a>
                <!-- <a href="/administrator/sales" class="dropdown-item saleSummary-menu"><img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Sale Summary" style="height:23px; padding:0 10px 0 0px !important;"> Sale Summary</a> -->
                @endhasrole

                <a class="dropdown-item logout-menu" href="{{ route('logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();" style="border-radius: 0 0 30px 30px;">
                    <img src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout"
                        style="height:23px; padding:0 10px 0 0px !important;"> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>