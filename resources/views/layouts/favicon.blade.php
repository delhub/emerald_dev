    <!-- Desktop browsers -->
    <link rel="shortcut icon" href="/assets/images/logos/favicon/48x48.ico">
    <!-- Modern browsers -->
    <link rel="icon" type="image/png" href="/assets/images/logos/favicon/192x192.png">
    <!-- iOS & other mobile devices -->
    <link rel="apple-touch-icon" href="/assets/images/logos/favicon/180x180.png">
