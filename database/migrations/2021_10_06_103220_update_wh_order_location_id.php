<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Warehouse\Order\WarehouseOrder;
use Illuminate\Support\Carbon;
use App\Models\Products\WarehouseInventories;

class UpdateWhOrderLocationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = Carbon::now();
        $beforeFirstOctober = Carbon::now()->subDays(5)->startOfDay(); // 1st Octber 2021

        $oldOrderToUpdate = WarehouseOrder::where('location_id', 1)->whereDate('date', '<', $beforeFirstOctober)->orderBy('date', 'DESC')->get();

        foreach ($oldOrderToUpdate as $key => $order) {
            $order->location_id = 999;
            $order->save();

            $inventories = WarehouseInventories::where('model_type', 'App\Models\Warehouse\Order\WarehouseOrder')->where('model_id', $order->id)->get();
            foreach ($inventories as $key => $inventory) {
                $inventory->outlet_id = 999;
                $inventory->save();
            }
        }

        $whOrder = WarehouseOrder::selectRaw('wh_order.*')
            ->join('orders', 'orders.delivery_order', '=', 'wh_order.delivery_order')
            ->whereDate('date', '<', $beforeFirstOctober)
            ->whereIn('orders.order_status', [1001, 1010])
            ->where('orders.pick_pack_status', 0)
            ->get();

        foreach ($whOrder as $key => $order) {
            $order->location_id = 1;
            $order->save();

            $inventories = WarehouseInventories::where('model_type', 'App\Models\Warehouse\Order\WarehouseOrder')->where('model_id', $order->id)->get();
            foreach ($inventories as $key => $inventory) {
                $inventory->outlet_id = 1;
                $inventory->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
