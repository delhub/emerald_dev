<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MassMailing extends Mailable
{
    use Queueable, SerializesModels;

    public $title;
    public $content;
    public $recipient_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $title, $content , $recipient_name )
    {
        $this->title = $title;
        $this->content = $content;
        $this->recipient_name  = $recipient_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->title)
            ->view('emails.mass-mailing')
            ->with('title', $this->title)
            ->with('content', $this->content)
            ->with('recipient_name', $this->recipient_name);

    }
}
