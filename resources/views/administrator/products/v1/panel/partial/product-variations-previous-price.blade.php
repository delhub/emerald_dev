
@if ($user->hasAnyRole('administrator|management|admin_product|formula_pic'))
    <div class="col-md-2 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Previous Price (version {{$price->price_key}})</label>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="previous_fixed_price" class="col-form-label font-weight-bold">Fixed Price (RM)</label>
                <input readonly type="text" name="previous_fixed_price[]" id="previous_fixed_price"
                class="form-control input-mask-price" value="{{$price->fixed_price}}">
            </div>
            <div class="col-md-3">
                <label for="previous_web_offer_price" class="col-form-label font-weight-bold">Web Offer Price (RM)</label>
                <input readonly type="text" name="previous_web_offer_price[]" id="previous_web_offer_price"
                class="form-control input-mask-price" value="{{$price->web_offer_price}}">
            </div>
            <div class="col-md-3">
                <label for="previous_outlet_price" class="col-form-label font-weight-bold">Outlet Fixed Price (RM)</label>
                <input readonly type="text" name="previous_outlet_price[]" id="previous_outlet_price"
                class="form-control input-mask-price" value="{{$price->outlet_price}}">
            </div>
            <div class="col-md-3">
                <label for="previous_outlet_offer_price" class="col-form-label font-weight-bold">Outlet Offer Price (RM)</label>
                <input readonly type="text" name="previous_outlet_offer_price[]" id="previous_outlet_offer_price"
                class="form-control input-mask-price" value="{{$price->outlet_offer_price}}">
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="product_measurement_depth" class="col-form-label font-weight-bold">Standard Member Price (RM) </label>
                <input readonly type="text" name="previous_member_price[]" id="previous_member_price"
                class="form-control input-mask-price" value="{{ $price->member_price }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_weight" class="col-form-label font-weight-bold">Advance Member Pric (RM)</label>
                <input readonly type="text" name="previous_offer_price[]" id="previous_offer_price"
                class="form-control input-mask-price" value="{{ $price->offer_price }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_weight" class="col-form-label font-weight-bold">Premier Member Price (RM)</label>
                <input readonly type="text" name="previous_premier_price[]" id="previous_premier_price"
                class="form-control input-mask-price" value="{{ $price->premier_price }}">
            </div>
        </div>
    </div>
    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="previous_product_sv" class="my-auto col-form-label font-weight-bold">Product SV </label>
                <input readonly type="text" name="previous_product_sv[]" id="previous_product_sv"
                class="form-control input-mask-price" value="{{ $price->product_sv}}">
            </div>
            <div class="col-md-6">
                <label for="previous_notes" class="my-auto col-form-label font-weight-bold">Notes</label>
                <textarea  type="text" name="previous_notes[]" id="previous_notes{{ $price->id }}"
                class="form-control" value="{{ $price->notes}}">{{ $price->notes}}</textarea>
            </div>
            <div class="col-md-3">
                <label for="previous_notes" class="my-auto col-form-label font-weight-bold"></label>
                <button type="button" class="btn btn-danger" style="width: 100%;"
                onclick="editPreviousPriceNotes({{ $price->id }});">
                Submit Edit(version {{$price->price_key}})
                <div class="spinner-border spinner-border-sm text-dark variationLoaderUpdateNotes" role="status" style="display:none;">
                    <span class="sr-only">Loading...</span>
                </div>
            </button>
            <p class="variationUpdateNotes{{ $price->id }}" style="display: none;font-size:23px">Price Notes<br>(version {{$price->price_key}})Updated</p>
            </div>
        </div>
    </div>
@endif

    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-md-2 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Product Point</label>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="fixed_price" class="col-form-label ">Point</label>
                <input readonly type="number" name="previous_point[]" id="previous_point"
                    class="form-control" value="{{ $price->point }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_width" class="col-form-label ">Discount Percentage (%)</label>
                <input readonly type="number" id="previous_discount_percentage" name="previous_discount_percentage[]"
                    class="form-control d-inline" value="{{ $price->discount_percentage }}">
            </div>
            @php
                $pointCash = $price && $price->point_cash != NULL ? json_decode($price->point_cash) : null;
            @endphp
            <div class="col-md-3">
                <label for="product_measurement_depth" class="col-form-label ">Point + Cash</label>
                <div class="input-group">
                    <input readonly type="text" name="previous_point_cash_point[]"  id="previous_point_cash_point" class="select2 form-control available-in d-inline" value="{{ $pointCash ? $pointCash->point : ''}}" placeholder="0">
                    <div class="input-group-text">
                        <span class="input-group-prepend">+</span>
                    </div>
                    <input readonly type="text" name="previous_point_cash_cash[]" id="previous_point_cash_cash" value="{{$pointCash ? $pointCash->cash : ''}}" placeholder="0" class="form-control input-mask-price">
                </div>
            </div>
        </div>
    </div>

    @if (!$inactive)
        <div class="ml-auto col-2 mr-4 mt-2">
            <button type="button" class="btn btn-danger" style="width: 100%;"
                onclick="activePrice({{ $price->id }});">
                Active this Price (version {{$price->price_key}})
                <div class="spinner-border spinner-border-sm text-dark variationLoaderPriceActive" role="status" style="display:none;">
                    <span class="sr-only">Loading...</span>
                    </div>
            </button>
            <small class="variationRefreshPriceActive" style="display: none;">Refresh to active this price</small>
        </div>
    @endif

