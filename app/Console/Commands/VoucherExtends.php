<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Traits\Voucher;

class VoucherExtends extends Command
{
    use Voucher;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:extends';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Extends voucher expiry date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->extendsVoucherExpiryDate()) {
            echo "Voucher Extends date --- OK \n";
        }
        else {
            echo "Error -- Extends date  Failed!...\n";
        }
    }
}
