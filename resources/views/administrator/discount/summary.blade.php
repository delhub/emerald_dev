@extends('layouts.administrator.mainVue')

@section('content')
@php

@endphp

<h1>Voucher Summary</h1>
<div class="card shadow-sm">
    <div class="card-header"> Voucher Summary - <small> Total generate & usage voucher</small>
        <div class="card-header-actions"><a class="card-header-action" href="#" target="_blank"><small class="text-muted">docs</small></a></div>
        </div>
    <div class="card-body">
        <div class="m-2">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

            <div class="col-lg-12">

                <div class="col-lg-12">
                    <div class="table-responsive m-2">
                        <div class="col-md-12">
                            <table class="table table-striped mt-3 voucher-tb" id="dtv">
                                <thead>
                                  <tr>
                                    <td>Month</td>
                                    <td>Total</td>
                                    <td>Package RM499</td>
                                    <td>Package RM1999</td>
                                    <td>Package RM2999</td>
                                    <td>Package RM3999</td>
                                    <td>Package RM4999</td>
                                    <td>Package RM5999</td>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $k=>$val)
                                    <tr>
                                        <td>{{$k}}</td>
                                        <td>
                                            Total Voucher: {{$val["Package_1"] + $val["Package_2"] + $val["Package_3"] + $val["Package_4"] + $val["Package_5"] + $val["Package_6"]}}
                                            <p>Total Used: {{$val["usage_1"] + $val["usage_2"] + $val["usage_3"] + $val["usage_4"] + $val["usage_5"] + $val["usage_6"]}}</p>
                                        </td>
                                        <td>Voucher: <strong>{{$val["Package_1"]}}</strong><p>Used: <Strong>{{$val["usage_1"]}}</Strong></p></td>
                                        <td>Voucher: <strong>{{$val["Package_2"]}}</strong><p>Used: <Strong>{{$val["usage_2"]}}</Strong></p></td>
                                        <td>Voucher: <strong>{{$val["Package_3"]}}</strong><p>Used: <Strong>{{$val["usage_3"]}}</Strong></p></td>
                                        <td>Voucher: <strong>{{$val["Package_5"]}}</strong><p>Used: <Strong>{{$val["usage_5"]}}</Strong></p></td>
                                        <td>Voucher: <strong>{{$val["Package_4"]}}</strong><p>Used: <Strong>{{$val["usage_4"]}}</Strong></p></td>
                                        <td>Voucher: <strong>{{$val["Package_6"]}}</strong><p>Used: <Strong>{{$val["usage_6"]}}</Strong></p></td>

                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                          </div>
                    </div>
                </div>

            </div>



        </div>
    </div>
</div>
@endsection

@push('script')
<script>
Vue.use(VueTables.ClientTable);

    var app = new Vue({
    el: '#app',
    data: {
        categories: [],
    },
    mounted() { //created // mounted //beforeMount
        this.getCategory()
    },
    methods: {
        getCategory() {
        axios
            .get('/administrator/discount/get-category')
            .then(response => {
                this.categories = response.data
                })
            .catch(error => console.log(error))
        }
    }
    });
</script>
@endpush
