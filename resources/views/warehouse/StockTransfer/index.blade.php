@inject('controller', 'App\Http\Controllers\Warehouse\StockTransfer\StockTransferController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>
    {{$controller::$header}} (OUT)
    @if ($boxInfo != NULL)
    - Box ({{$boxInfo->box_name}})
    @endif
</h1>
<div class="card shadow-sm">
    <div class="card-body">
        {{-- <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div>
        </div> --}}
        <div class="table-responsive m-2">

            <div class="col-12">
                <form action="{{route($controller::$transferStart)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            From Location<small class="text-danger">*</small>
                        </p>
                    </div>
                <div class="col-12 col-md-8 form-group">
                        <select name="from_location_id" class="form-control">
                            {{-- <option value="" >Please Select...</option> --}}
                            @foreach($fromLocations as $location)
                                {{-- <option value="{{ $location->id }}" >{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option> --}}
                                <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>

                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            To Location<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="to_location_id" class="form-control">
                            {{-- <option value="" >Please Select...</option> --}}
                            @foreach($toLocations as $location)
                                {{-- <option value="{{ $location->id }}" >{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option> --}}
                                <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>

                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Transfer Date<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                            <input type="date" name="transfer_datetime" id="transfer_datetime" class="form-control" placeholder="Stock In Date" autocomplete="off" value="{{old('transfer_datetime')}}"
                            data-parsley-error-message="This value is required." data-parsley-trigger="change" data-parsley-errors-container="#stockin-error-block" required>
                        </div>
                        <span id="stockin-error-block"></span>
                    </div>

                        <div class="col-12 col-md-4 text-md-left my-auto">
                            <p>
                                Transfer Type<small class="text-danger">*</small>
                            </p>
                        </div>
                    @if ($boxInfo == NULL)

                        <div class="col-12 col-md-8 form-group">
                            <select name="type" class="form-control" required>
                                <option value="" >Please Select...</option>
                                    <option value="1" >Standard Transfer (individual out / individual in)</option>
                                    <option value="0" >Express Transfer</option>
                            </select>
                        </div>
                    @else

                    <div class="col-12 col-md-8 form-group">
                        <input type="hidden" name="type" value="2">Batch Transfer
                        <input type="hidden" name="boxId" value="{{$boxInfo->id}}">
                    </div>

                    @endif


                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn btn-danger" style="float: center;"
                            onclick="clickSubmit({{ $user->id }});">Start Transfer</button>
                        </div>
                    </div>
                </form>
            </div>

            <hr>
            <div class="my-input-warehouse input-group input-group-lg mb-3">
                <input type="text" ref="scancode" v-model="scancode" name="scancode" class="bg-form2 form-control" id="my-input-warehouse"
                    placeholder="Use Scanner Here" autofocus autocomplete="off" @@change="qrProduct(scancode)">
                <div class="pop-btn">
                    <button type="button" class="qrbtn btn-primary">Submit</button>
                </div>
            </div>
           {{-- <p style="text-align: center"> Please selct Bin</p>
           <select name="bin_select" class="form-control">
               @foreach ($pickBins as $pickBin)
                <option value="{{$pickBin->id}}">{{$pickBin->pick_number}}<option>
               @endforeach
           </select> --}}
            {{-- <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p> --}}
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit(userId) {
    if (confirm('Are you sure start transfer?')) {
        document.getElementById("create-form-id").submit();
    }
}

// $('#stock_in_date').datepicker({
//     // uiLibrary: 'bootstrap4',
//     format: 'yyyy-mm-dd',
//     showOtherMonths: true,
//     selectOtherMonths: true
// });
</script>
@endpush
