<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductArticle extends Model
{
    // Set table
    protected $table = 'product_article';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'type',
        'category',
        'cat_id',
        'title',
        'data'
    ];

}
