<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddVoucherStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_statuses', function (Blueprint $table) {
            //
        });

        DB::table('global_statuses')->insert([
            [
                'id' => 7003,
                'name' => 'Half Voucher',
                'description' => 'Status for purchase(payment type)',
                'status_type' => 'payment_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 7004,
                'name' => 'Full Voucher',
                'description' => 'Status for purchase(payment type)',
                'status_type' => 'payment_type',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_statuses', function (Blueprint $table) {
            //
        });
    }
}
