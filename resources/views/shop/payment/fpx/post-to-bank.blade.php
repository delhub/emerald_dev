<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @include('layouts.favicon')
    <style>
        .loader {
            border: 16px solid #f3f3f3;
            /* Light grey */
            border-top: 16px solid #263A8B;
            /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>

<body>
    <div style="display: none;">
        <form id="b" name="b" method="POST" action="{{ $url }}">
            {{-- <form id="b" name="b" method="POST" action="{{ $responseUrl }}"> --}}
                @foreach ($fpx as $fieldName => $fieldValue)
                <input type=text value="{{ $fieldValue }}" name="{{ $fieldName }}">
                @endforeach
        </form>
    </div>

    <div class="loader" style="margin-top: 25vh; margin-left: auto; margin-right: auto;"></div>
    <div class="margin: 0 auto;">
        <p style="text-align: center; font-size: 1.2rem;">You will be redirected to selected bank website to complete
            your payment request.</p>
        <p style="text-align: center; font-size: 1.2rem;">Please do not close this window, it will just take a moment.
        </p>
    </div>

    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
             setTimeout(function() {
              document.getElementById('b').submit()
             }, 2000);
        });
    </script>
</body>

</html>