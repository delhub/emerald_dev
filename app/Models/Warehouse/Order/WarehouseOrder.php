<?php

namespace App\Models\Warehouse\Order;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;


class WarehouseOrder extends Model
{
    use TableLogs;

    protected $table = 'wh_order';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'location_id',
        'type',
        'inv_type',
        'date',
        'invoice_number',
        'delivery_order',
        'order_data',
        'inv_reference',
    ];

    /**
     * Get purchase of the WarehouseOrder.
     */
    public function purchase()
    {
        return $this->belongsTo('App\Models\Purchases\Purchase', 'invoice_number', 'purchase_number');
    }

    /**
     * Get order of the WarehouseOrder.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Purchases\Order', 'delivery_order', 'delivery_order');
    }

    /**
     * Warehouse Key Format
     */
    public function scopeWarehouseKeyFormat($query, $countryId, $stateId, $type)
    {
        $stateAbbreviation = State::where('id', $stateId)->first();

        $exist = $this->where('country_id', $countryId)->where('state_id', $stateId)->where('type', $type)->get();
        $count = count($exist);

        $result = $countryId . $stateAbbreviation->abbreviation . $type . str_pad($count + 1, 3, "0", STR_PAD_LEFT);

        return $result;
    }

    public function batchItems(){
        return $this->hasMany('App\Models\Products\WarehouseBatchItem', 'picking_order_id', 'id');
    }
}
