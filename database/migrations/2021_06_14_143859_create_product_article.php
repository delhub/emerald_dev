<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_article', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('type',32);            
            $table->string('category',64)->nullable();
            $table->integer('cat_id')->nullable();            
            $table->string('title',255)->nullable();          
            $table->text('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_article');
    }
}
