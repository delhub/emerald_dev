@component('mail::message')

Dear {{ $user->userInfo->full_name }},

Congratulations! Your Formula Healthcare account has been upgraded!
<br>
You can now enjoy purchasing with Advance Member price

@component('mail::button', ['url' => $url])
Shop now
@endcomponent

Regards,<br>
{{ config('app.name') }}

@endcomponent
