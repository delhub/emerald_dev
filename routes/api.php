<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/getUser', 'API\ApiUsersController@getUser')->name('api.getUser');
Route::post('/checkUser', 'API\ApiUsersController@checkUser')->name('api.checkUser');
Route::post('/getAgent', 'API\ApiUsersController@getAgent')->name('api.getAgent');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::group(['middleware' => ['guest:api']], function () {
        Route::post('/login', 'API\Auth\AuthController@login');

        // Customer
        Route::group(['prefix' => 'customer'], function () {
            // Register - POST
            Route::post('register', 'API\Auth\AuthController@customerRegister');
        });

        // Dealer
        Route::group(['prefix' => 'dealer'], function () {
            // Register - POST
            Route::post('register', 'API\Auth\AuthController@dealerRegister');
        });
    });

    // Require authentication token.
    Route::group(['middleware' => 'auth:api'], function () {
        // Logout - POST
        Route::post('/logout', 'API\Auth\AuthController@logout');
    });
});

Route::middleware('auth:api')->group(function () {
    // our routes to be protected will go in here
    Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
});

Route::group(['middleware' => ['cors', 'json.response']], function () {
    Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register','Auth\ApiAuthController@register')->name('register.api');
    Route::post('/register-new','Auth\ApiAuthController@registerNew')->name('registerNew.api');
    Route::post('/auth/disconnect','API\Auth\UserConnectedController@disconnect')->name('disconnect.api');
    Route::post('/auth/reconnect','API\Auth\UserConnectedController@reconnect')->name('reconnect.api');
});

/**
 * Public Routes
 * TODO: Move to auth:api middleware group.
 */
Route::get('/export/agents', 'API\Export\ExportController@agent_json');
Route::get('/export/customers', 'API\Export\ExportController@customer_json');
Route::get('/export/products', 'API\Export\ExportController@product_json');
Route::get('/export/orders', 'API\Export\ExportController@order_json');

Route::post('/export/download', 'API\Export\ExportController@download')->name('warehouse-download');

Route::group(['prefix' => 'categories'], function () {

    // Return all categories.
    Route::get('/', 'API\Shop\CategoryController@getCategories');

    // Return a category based on it's ID.
    Route::get('/{id}', 'API\Shop\CategoryController@getCategory');

    // Return the child of a category based on it's ID.
    Route::get('/{id}/childs', 'API\Shop\CategoryController@getCategoryWithChilds');
});

Route::group(['prefix' => 'products'], function () {
    // Return all products.
    Route::get('/', 'API\Shop\ProductController@getProducts');

    // Return a product based on it's ID.
    Route::get('/{id}', 'API\Shop\ProductController@getProduct');

    // Return a product based on it's ID with it's variations.
    Route::get('/{id}/{panel}', 'API\Shop\ProductController@getPanelProduct');
});

Route::group(['prefix' => 'globals'], function () {
    // States
    Route::group(['prefix' => 'states'], function () {
        Route::get('/', 'API\Master\StateController@getStates');
    });
});
Route::group(['prefix' => 'registration'], function () {
    Route::post('/create', 'API\ApiUsersController@register');
});
Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'guest:api'], function () {
        Route::group(['prefix' => 'users'], function () {
            Route::get('/all', 'API\ApiUsersController@all');
            Route::get('/updated', 'API\ApiUsersController@updated');
            Route::post('/update-user-level', 'API\ApiUsersController@updateUserLevel');
        });

        Route::group(['prefix' => 'agent'], function () {
            Route::get('/check', 'API\ApiAgentsController@checkAgent');
        });

        Route::group(['prefix' => 'voucher'], function () {
            Route::get('/coupon/{code}', 'API\ApiVoucherController@getCouponRetail');
            Route::post('/usage', 'API\ApiVoucherController@createUsage');
            Route::get('/rule', 'API\ApiVoucherController@getRule');
            Route::post('/generate-code', 'API\ApiVoucherController@generateVoucherCode');
        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('/all', 'API\ApiCoreController@all');
            Route::get('/attribute', 'API\ApiCoreController@attribute');
            Route::get('/price', 'API\ApiCoreController@price');
            Route::get('/images', 'API\ApiCoreController@images');
            Route::get('/brand', 'API\ApiCoreController@brand');
            Route::get('/categories', 'API\ApiCoreController@categories');
            Route::get('/pivcategory', 'API\ApiCoreController@pivCategoryProduct');
            Route::get('/catalog', 'API\ApiCoreController@productCatalog');
            Route::get('/search', 'API\ApiCoreController@productSearch');
            Route::get('/scancode', 'API\ApiCoreController@scancode');

            Route::get('/attribute-bundle', 'API\ApiCoreController@attributeBundle');
            Route::get('/attribute-bundle-confirm', 'API\ApiCoreController@attributeBundleConfirm');

            Route::get('/gift-product', 'API\ApiCoreController@giftProduct');
            Route::get('/gift-product-by/{id}', 'API\ApiCoreController@giftProductBy');
            Route::get('/gift-categories', 'API\ApiCoreController@getGiftCategories');

        });

        Route::group(['prefix' => 'wh'], function () {
            Route::get('/batch', 'API\ApiWHController@getBatch');
            Route::get('/detail', 'API\ApiWHController@getBatchDetail');
            Route::get('/item', 'API\ApiWHController@getBatchItem');
            Route::get('/checkItem/{id}', 'API\ApiWHController@checkBatchItem');
        });

        Route::group(['prefix' => 'redemption'], function () {
            Route::get('/get-dealer-stock', 'API\ApiRedemption@getDealerStock');
            Route::post('/updateStock', 'API\ApiRedemption@updateDealerStock');
        });
    });
});

