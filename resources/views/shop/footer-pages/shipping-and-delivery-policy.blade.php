@extends('layouts.shop.main')

@section('content')

    <div class="privacy-policy-bg-img">

        <div class="mt-5 d-flex"></div>
        <div class="d-md-none p-0">
            <img src="/images/shipping-and-delivery-policy/shipping-and-delivery-policy-1.jpg" style="margin-top: -22px;object-fit: cover;width: 100%;">
        </div>
        <div class="mt-md-5">

            <div class="d-flex justify-content-center">
                <div class="overflow-auto scrollbar privacy-policy" style="border-radius: 10px;">
                    <h1>Shipping and Delivery Policy</h1>
                    <div class="info-hrline-red privacy"></div>
                    <div class="mt-md-2"></div>
                    <ol class="pl-md-3 pl-3">
                        <li>
                            <p class="paragraph">
                                本網站送貨至全馬各地。<br>
                                Home Delivery services are available for purchases within Malaysia.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                依據不同送貨系統，西馬各地預計3到7天，東馬則至少需要14天的時間。<br>
                                Order will be delivered via courier service of our choice. Subject to different courier
                                service provider, please allow us three to seven working days for West Malaysia and
                                minimum fourteen working days for East Malaysia including Labuan processing from date of
                                Formula’s confirmation of acceptance of an Order within the aforesaid areas unless
                                otherwise notified by Formula. Please note that we will not accept delivery to a P.O.
                                Box address
                                Formula shall not be liable for any delay in its delivery services, if the delay has
                                been due to causes beyond the control of Formula.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                貨品下單出貨後，一律不準取消。<br>
                                Once ordered is shipped, no cancellation is allowed.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                客戶可以在戶口內查詢送貨進度。<br>
                                Customers can track delivery progress through their account.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                若送貨員嘗試三次送貨不成，客戶須另行支付送貨費用。<br>
                                If Customer is not available to receive the delivery, a notification card will be left
                                at the delivery address. Attempts will be made to contact the Customer for a second and
                                third delivery. If parcel is still not deliverable after third attempt, customer must
                                pay an additional delivery fee.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                若客戶不滿意貨品，請自行安排送回流程。<br>
                                If customer is dissatisfied with the goods delivered, he or she must arrange for the
                                return on their own and at their own cost.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                因促銷而獲得免費送貨，僅限一次安排，若客戶無法領貨，第二次須自行付費。<br>
                                Free shipping offered during any promotion is only valid for the first attempt. If the
                                customer is unable to receive the parcels, the customer is responsible for all costs
                                associated with the second delivery attempt.
                            </p>
                        </li>
                        <li>
                            <p class="paragraph">
                                公司有權在必要時修正本送貨條款<br>

                                Formula reserves the absolute right to amend any terms and conditions of delivery and
                                shipping
                            </p>
                        </li>
                    </ol>
                    <h1>Collection Procedure</h1>
                    <div class="info-hrline-red procedure"></div>
                    <div class="mt-md-2"></div>
                    <ol class="pl-md-3 pl-0">
                        <p class="paragraph">
                            領貨流程：
                        </p>
                        <ol>
                            <li>
                                <p class="paragraph">
                                    定時察看送貨流程進展 <br>
                                    Customer is to track their own delivery progress regularly.
                                </p>
                            </li>
                            <li>
                                <p class="paragraph">
                                    接聽送貨員電話 <br>
                                    Pick up calls by delivery agent
                                </p>
                            </li>
                            <li>
                                <p class="paragraph">
                                    領貨時必須掃瞄QRcode <br>
                                    Customers must scan QR code when receiving goods.
                                </p>
                            </li>
                            <li>
                                <p class="paragraph">
                                    若發現包裝有損壞，可要求當場查看貨品 <br>
                                    If the packaging is damaged or open, Customer have the option of having the
                                    goods checked on the spot.
                                </p>
                            </li>
                            <li>
                                <p class="paragraph">
                                    若在較後拆開驗貨，有何損壞，敬請電郵公司投訴 <br>
                                    If customer discovers goods are damaged after opening and inspecting the
                                    package, he or she may file a complaint with the company.
                                </p>
                            </li>
                            <li>
                                <p class="paragraph">
                                    所有商品必定是新貨，敬請查驗為實 <br>
                                    All goods delivered are new, customers are advised to check upon receiving.
                                </p>
                            </li>
                        </ol>
                    </ol>
                </div>

            </div>
        </div>
    </div>




    @push('style')
        <style>
            /*ol {*/
            /*    list-style-type: none;*/
            /*    counter-reset: item;*/
            /*    margin: 0;*/
            /*    !* padding: 0; *!*/
            /*}*/
            /*ol > li {*/
            /*    display: table;*/
            /*    counter-increment: item;*/
            /*    !*margin-bottom: 0.6em;*!*/
            /*}*/
            /*ol > li::before {*/
            /*    content: counters(item, ".") ".";*/
            /*    display: table-cell;*/
            /*    padding-right: 0.5em;*/
            /*}*/
            /*li ol > li {*/
            /*    margin: 0;*/
            /*}*/
            /*li ol > li::before {*/
            /*    content: counters(item, ".");*/
            /*}*/
            .info-hrline-red.privacy {
                position: relative;
                width: 27px;
                margin: auto;
            }
            .info-hrline-red.procedure {
                position: relative;
                width: 23px;
                margin: auto;
            }
            h1 {
                font-family: "Roboto", sans-serif;
                font-weight: 700;
                font-size: 28px;
                margin-bottom: 0px;
            }
            .privacy-policy-bg-img {
                margin-top: 82px;
                /*background-size: cover;*/
                object-fit: cover;
                background-size: 100% 100%;
                /* width: 100%; */
                /*height: 67.3vh;*/
            }

            .fml-footer {
                margin-top: 0;
            }

            .privacy-policy {
                background-color: white;
                border: 2px solid whitesmoke;
                padding: 0.75rem;
                max-height: 50vh;

            }

            /*Ultra large screen*/
            @media(min-width:2000px) {
                .privacy-policy {
                    max-width: 150vh;
                }
            }

            /*Desktop screen*/
            @media(min-width:1600px) and (max-width: 1999px){
                .privacy-policy {
                    max-width: 135vh;
                }
            }
            @media(min-width:768px) {
                .info-hrline-red.privacy {
                    right: 4.5%;
                }
                .info-hrline-red.procedure {
                    right: 16.7%;
                }
                .privacy-policy-bg-img {
                    background: url('/images/shipping-and-delivery-policy/shipping-and-delivery-policy.jpg')no-repeat center;
                    background-size: 100% 100%;
                    /* width: 100%; */
                    height: 71.3vh;
                }
            }

            /*Large laptop screen*/
            @media(min-width:1200px) and (max-width:1600px) {
                .privacy-policy {
                    max-width: 100vh;
                }
            }

            /*Laptop screen*/
            @media(min-width:800px) and (max-width:1200px) {
                .privacy-policy {
                    max-width: 70vh;
                }
            }

            /*Tablet screen*/
            @media(max-width:799px) {
                h1 {
                    text-align: center;
                }
                .info-hrline-red.privacy {
                    left: 42.2%;
                }
                .info-hrline-red.procedure {
                    left: 31.2%;
                }
                .privacy-policy {
                    max-height: 100%;
                    border: none;
                }
                /*.privacy-policy-bg-img {
                    height: 87.7vh;
                }*/
            }

            /*.header {
                font-weight: 700;
                text-decoration: underline;

            }*/

            .paragraph {
                font-family: "Roboto", sans-serif;
                font-size: 16px;
                text-align: justify;
                line-height: 1.3;
                font-weight: revert;
            }

            .sub-list {
                counter-reset: list;
            }

            .sub-list>li {
                list-style: none;
                position: relative;
            }

            .sub-list>li:before {
                counter-increment: list;
                content: "("counter(list, lower-alpha) ") ";
                position: absolute;
                left: -1.7em;
            }


            .scrollbar::-webkit-scrollbar {
                width: 15px;



            }

            .scrollbar::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
                border-radius: 10px;

            }

            .scrollbar::-webkit-scrollbar-thumb {
                border-radius: 10px;

                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
                background: #ef3842c9;

            }
        </style>
    @endpush

@endsection
