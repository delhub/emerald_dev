<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;

class PivCategory extends Model
{
    // Set table
    protected $table = 'piv_category_product';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'product_id',
        'category_id'
    ];


}
