<?php

namespace App\Http\Controllers\Administrator\Sale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Globals\State;
use App\Models\Dealers\DealerGroup;
use Illuminate\Support\Facades\Cache;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    static function compile_data($type, $date, $product_name, $group_id = 0)
    {
        $query = "SELECT * FROM view_purchases WHERE ";
        $dquerys = "SELECT * FROM view_dealers WHERE ";
        $uquerys = "SELECT COUNT(user_id) FROM view_customers WHERE ";
        switch ($type) {
            case 'day':
                $query .= "purchase_date = '{$date}'";
                $_date = strtotime(str_replace('/', '-', $date));
                $udate = date('Y-m-d',  $_date);
                $udatenext = date('Y-m-d', strtotime('+1 day',  $_date));
                $dquery = " created_at >= '{$udate}' and created_at < '{$udatenext}'";
                $dates = $date;
                break;
            case 'week':
                $q_string = '';

                foreach ($date as $in => $_date) {
                    $q_string .= '"' .  $_date . '"';
                    if ($in != 6) $q_string .= ",";
                }

                $query .= "purchase_date IN ({$q_string})";
                $dates =  $date[0] . '-' . $date[6];
                $udate = date('Y-m-d',  strtotime(str_replace('/', '-', $date[0])));
                $udatenext = date('Y-m-d',  strtotime(str_replace('/', '-', $date[6])));
                $dquery = " created_at >= '{$udate}' AND created_at < '{$udatenext} 23:59:59'";
                break;
            case 'month':
                $query .= "purchase_date LIKE '%/{$date}'";
                $dates = '01/' . $date . '-' . date('t/m/Y', strtotime(str_replace('/', '-', '01/' . $date)));

                $udate = date('Y-m-d',  strtotime(str_replace('/', '-', '01/' . $date)));
                $udatenext =  date('Y-m-t', strtotime(str_replace('/', '-', '01/' . $date)));
                $dquery = " created_at >= '{$udate}' AND created_at < '{$udatenext} 23:59:59'";
                break;
        }
        $gquery = ($group_id != 0) ? " AND group_id = {$group_id}" : "";
        $country = country()->country_id;
        $cquery = " AND country_id = '{$country}'";
        //  dd($query);
        $dcount = 'COUNT(user_id)';
        $all_purchases = self::cache_builder('sales_cache', $type, $dates, $query . $gquery . $cquery, $group_id);
        $dealers = self::cache_builder('dealers_cache', $type, $dates, $dquerys . $dquery . $gquery, $group_id);
        $custs = self::cache_builder('customers_cache', $type, $dates, $uquerys . $dquery . $gquery, $group_id);
        $items = 0;
        $total = 0;
        $purchases = $product_ids = array();
        foreach ($all_purchases  as $purchase) {
            $purchase->group_id = ($purchase->group_id) ?  $purchase->group_id : 13;
            $purchase->product_id = ($purchase->product_id) ?  $purchase->product_id : 1;
            $purchases[$purchase->purchase_number]['item'][] = $purchase;
            if (!isset($purchases[$purchase->purchase_number]['purchase_amount'])) $purchases[$purchase->purchase_number]['purchase_amount'] = 0;
            $purchases[$purchase->purchase_number]['purchase_amount'] = $purchase->total_fee;
            $purchases[$purchase->purchase_number]['date'] = $purchase->purchase_date;
            $purchases[$purchase->purchase_number]['area'] = $purchase->state_id;
            $purchases[$purchase->purchase_number]['group'] = $purchase->group_id;
            $purchases[$purchase->purchase_number]['type'] = $purchase->purchase_type;
            $purchases[$purchase->purchase_number]['rebate']['amount'] = 0;
            $purchases[$purchase->purchase_number]['rebate']['count'] = 0;
            $product_ids[] = $purchase->product_id;
        }
        $purchase_ids = array_keys($purchases);
        if ($purchase_ids) {
            $purchase_in = '';
            $numItems = count($purchase_ids);
            $i = 0;
            foreach ($purchase_ids as  $_purchase_ids) {
                $purchase_in .= "'" . $_purchase_ids . "'";
                if (++$i !== $numItems)  $purchase_in .= ",";
            }
            $rebate_query = "SELECT p.purchase_number, (f.amount / 100) as amount FROM shipping_fee f JOIN purchases p ON f.purchase_id = p.id AND f.type ='rebate_fee' WHERE p.purchase_number IN (" . $purchase_in . ")";
            $rebates = self::cache_builder('fee_cache', $type, $dates,  $rebate_query, $group_id);
            foreach ($rebates as $rebate) {
                $purchases[$rebate->purchase_number]['rebate']['amount'] += $rebate->amount;
                $purchases[$rebate->purchase_number]['rebate']['count'] += 1;
            }
        }
        $area_data = array();
        $dealer_count = 0;
        $group_data = array();
        $product_data = array();
        $mp_data = array('total' => 0, 'case' => 0);
        $sale_data = array('total' => 0, 'case' => 0);
        $voucher_data = array('total' => 0, 'case' => 0);
        $partial_data = array('total' => 0, 'case' => 0);
        foreach ($dealers as $dealer) {
            $dealer_count++;
            $dealer->group_id = ($dealer->group_id) ?  $dealer->group_id : 13;
            if (isset($group_data[$dealer->group_id])) {
                $group_data[$dealer->group_id]['new']++;
            } else {
                $group_data[$dealer->group_id]['case'] = 0;
                $group_data[$dealer->group_id]['total'] = 0;
                $group_data[$dealer->group_id]['mp_case'] = 0;
                $group_data[$dealer->group_id]['mp_total'] = 0;
                $group_data[$dealer->group_id]['new'] = 1;
                $group_data[$dealer->group_id]['id'] = $dealer->group_id;
            }
        }
        foreach ($purchases as $purchase) {


            if (isset($area_data[$purchase['area']])) {
                $area_data[$purchase['area']]['case']++;
                $area_data[$purchase['area']]['total'] += $purchase['purchase_amount'];
            } else {
                $area_data[$purchase['area']]['case'] = 1;
                $area_data[$purchase['area']]['total'] = $purchase['purchase_amount'];
            }

            if (isset($group_data[$purchase['group']])) {
                $group_data[$purchase['group']]['case']++;
                $group_data[$purchase['group']]['total'] += $purchase['purchase_amount'];
            } else {
                $group_data[$purchase['group']]['case'] = 1;
                $group_data[$purchase['group']]['total'] = $purchase['purchase_amount'];
                $group_data[$purchase['group']]['new'] = 0;
                $group_data[$purchase['group']]['id'] = $purchase['group'];
                $group_data[$purchase['group']]['mp_case'] = 0;
                $group_data[$purchase['group']]['mp_total'] = 0;
            }



            $voucher_data['total'] += $purchase['rebate']['amount'];
            $voucher_data['case']  += $purchase['rebate']['count'];

            //if ($purchase['type'] != 'Voucher') {
            $sale_data['total'] += $purchase['purchase_amount'];
            $sale_data['case']++;
            //}
            if ($purchase['type'] == 'Metapoint') {
                $mp_data['total'] += $purchase['purchase_amount'];
                $mp_data['case']++;

                $group_data[$purchase['group']]['mp_case']++;
                $group_data[$purchase['group']]['mp_total'] += $purchase['purchase_amount'];
            }

            foreach ($purchase['item'] as $item) {
                $item_quantity = $item->quantity;
                $i = 0;
                while ($item_quantity > $i) {
                    $i++;
                    $product_data[$item->product_id]['item'][] = $item;
                    $product_data[$item->product_id]['id'] =  $item->product_id;
                }
            }
        }

        //  dd($actual_data);
        $actual_data = array();
        foreach ($product_data as $data) {

            $actual_data[$product_name[$data['id']]] = count($data['item']);


            // $actual_data[] = $_actual_data;
        }
        //   dd( array('actual_data'=> $actual_data, 'total'=> $total, 'items'=> $items, 'date'=> $date));
        arsort($actual_data);
        array_splice($actual_data, 20);
        uasort($area_data, __CLASS__ . '::sortByCase');
        uasort($group_data, __CLASS__ . '::sortByCase');

        return array(
            'actual_data' => $actual_data, 'saledata' => $sale_data, 'mpdata' => $mp_data,
            'date' => $dates, 'agents' => $dealer_count, 'custs' => $custs[0]->$dcount, 'area' => $area_data,
            'group' => $group_data, 'voucherdata' => $voucher_data
        );
    }
    public static function sortByCase($a, $b)
    {

        return $b['case'] <=> $a['case'];
    }
    public function index(Request $request, $day, $month, $year)
    {
        $date_format = 'd/m/Y';
        $dateArray = array('day' => $day, 'month' => $month, 'year' => $year);
        $date = strtotime($year . '-' . $month . '-' . $day);
        $currentday = date($date_format,  $date);
        // $yesterday = date( $date_format , strtotime('-1 day',  time()));

        $thisweek = $this->weeklydate('this', $date);
        //$lastweek = $this->weeklydate('last');

        $thismonth = date('m/Y', $date);
        // $lastmonth = date( 'm/Y', strtotime('last month') );

        $product_name = $this->getProductNames();

        $thismonth_data = self::compile_data('month', $thismonth, $product_name);

        $today_data = self::compile_data('day', $currentday, $product_name);

        //$yesterday_data = self::compile_data('day', $yesterday, $product_name);

        $thisweek_data = self::compile_data('week', $thisweek, $product_name);

        // $lastweek_data = self::compile_data('week', $lastweek, $product_name);

        $thismonth_data = self::compile_data('month', $thismonth, $product_name);

        // $lastmonth_data = self::compile_data('month', $lastmonth, $product_name);

        $data = array(
            "Daily" => $today_data,
            //"Yesterday" => $yesterday_data,
            "Weekly" => $thisweek_data,
            // "Last Week" => $lastweek_data,
            "Monthly" => $thismonth_data
            //  "Last Month" => $lastmonth_dat
        );

        $states = State::pluck('name', 'id')->toArray();
        $groups = DealerGroup::pluck('group_name', 'id')->toArray();
        $currency = country()->country_currency;
        return view('administrator.sales.index', compact(['currentday', 'data', 'states', 'groups', 'dateArray', 'currency']));
    }
    public function groupIndex(Request $request, $group_id, $day, $month, $year)
    {

        $data = $this->groupDateDataCompile($group_id, $day, $month, $year);
        $data['manager_view'] = false;
        return view('administrator.sales.groupIndex',  $data);
    }

    public function singleGroupIndex(Request $request, $day, $month, $year)
    {
        $group_id = DealerGroup::getManagerGroupID();

        $data = $this->groupDateDataCompile($group_id, $day, $month, $year);
        $data['manager_view'] = true;
        return view('administrator.sales.groupIndex', $data);
    }

    public function groupDateDataCompile($group_id, $day, $month, $year)
    {

        $date_format = 'd/m/Y';
        $dateArray = ['day' => $day, 'month' => $month, 'year' => $year];

        $date = strtotime($year . '-' . $month . '-' . $day);
        $currentday = date($date_format,  $date);

        $thisweek = $this->weeklydate('this', $date);

        $thismonth = date('m/Y', $date);

        $product_name = $this->getProductNames();


        $today_data = self::compile_data('day', $currentday, $product_name, $group_id);

        $thisweek_data = self::compile_data('week', $thisweek, $product_name, $group_id);

        $thismonth_data = self::compile_data('month', $thismonth, $product_name, $group_id);

        $data = array(
            "Daily" => $today_data,
            //"Yesterday" => $yesterday_data,
            "Weekly" => $thisweek_data,
            // "Last Week" => $lastweek_data,
            "Monthly" => $thismonth_data
            //  "Last Month" => $lastmonth_dat
        );

        $states = State::pluck('name', 'id')->toArray();
        $groups = DealerGroup::pluck('group_name', 'id')->toArray();
        $groupName =  $groups[$group_id];
        return  compact(['currentday', 'data', 'states', 'groups', 'groupName', 'group_id']);
    }
    public function mainIndex()
    {
        $date_format = 'd/m/Y';

        $currentday = date($date_format);
        $thisweek = $this->weeklydate('this');
        $thismonth = date('m/Y');
        $product_name = $this->getProductNames();

        $monthdata = self::compile_data('month', $thismonth, $product_name);

        $daydata = self::compile_data('day', $currentday, $product_name);

        $weekdata = self::compile_data('week', $thisweek, $product_name);

        $registrations = array();
        $dcount = 'COUNT(user_id)';
        $country = country()->country_id;
        $cquery = " WHERE country_id = '{$country}'";
        $registrations['custs'] =  DB::select(DB::raw("SELECT COUNT(user_id) FROM view_customers"))[0]->$dcount;
        $registrations['agents'] =  DB::select(DB::raw("SELECT COUNT(user_id) FROM view_dealers" . $cquery))[0]->$dcount;


        $states = State::pluck('name', 'id')->toArray();
        $groups = DealerGroup::pluck('group_name', 'id')->toArray();

        $currency = country()->country_currency;
        return view('administrator.sales.mainIndex', compact(['currentday', 'monthdata', 'daydata', 'weekdata', 'states', 'groups', 'registrations', 'currency']));
    }

    public function mainGroupIndex(Request $request, $group_id)
    {

        $data = $this->groupDataCompile($group_id);
        $data['manager_view'] = false;

        return view('administrator.sales.mainGroupIndex', $data);
    }

    public function mainSingleGroupIndex(Request $request)
    {

        $group_id = DealerGroup::getManagerGroupID();

        $data = $this->groupDataCompile($group_id);
        $data['manager_view'] = true;
        return view('administrator.sales.mainGroupIndex', $data);
    }

    public function groupDataCompile($group_id)
    {


        $date_format = 'd/m/Y';

        $currentday = date($date_format);
        $thisweek = $this->weeklydate('this');
        $thismonth = date('m/Y');
        $product_name = $this->getProductNames();

        $monthdata = self::compile_data('month', $thismonth, $product_name, $group_id);

        $daydata = self::compile_data('day', $currentday, $product_name, $group_id);

        $weekdata = self::compile_data('week', $thisweek, $product_name, $group_id);

        $registrations = array();
        $dcount = 'COUNT(user_id)';
        $country = country()->country_id;
        $cquery = " AND country_id = '{$country}'";
        $registrations['custs'] =  DB::select(DB::raw("SELECT COUNT(user_id) FROM view_customers WHERE group_id = {$group_id}"))[0]->$dcount;
        $registrations['agents'] =  DB::select(DB::raw("SELECT COUNT(user_id) FROM view_dealers WHERE group_id = {$group_id}" . $cquery))[0]->$dcount;


        $states = State::pluck('name', 'id')->toArray();
        $groups = DealerGroup::pluck('group_name', 'id')->toArray();
        $groupName =  $groups[$group_id];
        $currency = country()->country_currency;
        return compact('currentday', 'monthdata', 'daydata', 'weekdata', 'states', 'groups', 'registrations', 'groupName', 'group_id', 'currency');
    }
    public function getProductNames()
    {

        $productnames = DB::select(DB::raw("SELECT * FROM view_product_name"));
        $product_name = array();
        foreach ($productnames as $productname) {
            $product_name[$productname->id] = $productname->name;
        }
        $product_name[1] = 'NOT SET';
        return $product_name;
    }

    public function weeklydate($which, $date = false, $format = 'd/m/Y')
    {

        if (!$date) {
            return array(
                date($format, strtotime("monday {$which} week")),
                date($format, strtotime("tuesday {$which} week")),
                date($format, strtotime("wednesday {$which} week")),
                date($format, strtotime("thursday {$which} week")),
                date($format, strtotime("friday {$which} week")),
                date($format, strtotime("saturday {$which} week")),
                date($format, strtotime("sunday {$which} week"))
            );
        } else {
            $week =  date('W', $date);
            $year = date('Y', $date);
            return array(

                date($format, strtotime("{$year}-W{$week}-1")),
                date($format, strtotime("{$year}-W{$week}-2")),
                date($format, strtotime("{$year}-W{$week}-3")),
                date($format, strtotime("{$year}-W{$week}-4")),
                date($format, strtotime("{$year}-W{$week}-5")),
                date($format, strtotime("{$year}-W{$week}-6")),
                date($format, strtotime("{$year}-W{$week}-7"))

            );
        }
    }

    public static function cache_builder($obj_name, $type, $dates, $query, $group_id = 0)
    {

        $group_name = ($group_id != 0) ? "_G" . $group_id : "";
        $dbcache_name = $obj_name . '_' . $type . '_' . str_replace(' ', '_', $dates) . $group_name . country()->country_id;
        if (Cache::has($dbcache_name) && $type != 'day') {
            $data = Cache::get($dbcache_name);
        } else {
            $data  = DB::select(DB::raw($query));
            Cache::put($dbcache_name,  $data, 60);
        }

        return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
