<?php

namespace App\Traits;

use App\Models\Users\User;
use App\Models\Users\UserInfo;
use App\Models\Globals\Status;
use App\Models\Purchases\Purchase;
use Carbon\Carbon;

trait Membership
{
    public function getMembershipSetting($id) {
        $membership = Status::where('status_type','user_level')->get();

        if(count($membership) > 0) {
            foreach ($membership as $key => $value) {
                $value->description = json_decode($value->description);
                $data[] = $value;
            }

            $mship = collect($data);
            $membershipData = $mship->where('id',$id)->first();

            return $membershipData;
        }
    }

    public function MembershipCron($limit=10) {
        $today  = date('Y-m-d');
        $AllUser = UserInfo::where('last_checked','<',$today)
            ->orWhere('last_checked', NULL)
            ->limit($limit)->get();

        if(count($AllUser) > 0) {
            foreach ($AllUser as $val) {
                if(isset($val->user_id) && !empty($val->user_id)){
                    $result[] = $this->checkMembership($val->user_id);
                }
            }

            return $result;
        }
        else {
            return ['status'=>'No user meet requirement to proceed'];
        }
    }

    public function checkYearlyPurchases($user_id) {
        $user = User::find($user_id);

        if($user) {
            $userInfo = $user->UserInfo;
            $membership_date = $userInfo->membership_date;

            if(!isset($membership_date)) {
                $firstMonth = Carbon::now();

                $userInfo->membership_date = $firstMonth->startOfMonth();
                $userInfo->save();
            }

            $userInfo = $user->UserInfo;
            $membership_date = $userInfo->membership_date;

            if(isset($membership_date) && !empty($membership_date)) {

                $yearly = $this->getNextYearDate($membership_date); //['user_id',$user_id]

                $purchase = Purchase::whereIn('purchase_status',[4002,4007])
                    ->whereBetween('created_at',[$membership_date,$yearly])
                    ->where('user_id',$user_id)
                    ->sum('purchase_amount');

                if($purchase) {
                    return number_format(($purchase / 100),2);
                }
                else {
                    return 0.00;
                }
            }
            else {
                $error = ['error'=>'Membership Date is not set','file'=>'app/Traits/Membership.php','user_id'=>$user->id];
                \Log::error($error);
                return $error;
            }
        }
    }

    public function getNextYearDate($membership_date,$future=1) {
        $year = Carbon::createFromFormat('Y-m-d H:i:s', $membership_date)->year;
        $month = Carbon::createFromFormat('Y-m-d H:i:s', $membership_date)->month;
        $day = Carbon::createFromFormat('Y-m-d H:i:s', $membership_date)->day;

        $newDate = ($year + $future). '-' .$month. '-' .$day. ' 00:00:00';

        return new Carbon($newDate);
    }

    public function checkMembership($user_id) {
        $yearlyPurchases = $this->checkYearlyPurchases($user_id);
        $advanced = $this->getMembershipSetting(6001);
        $premium = $this->getMembershipSetting(6002);

        $user = User::find($user_id);

        if($user) {
            $userInfo = $user->UserInfo;
            $membership_date = $userInfo->membership_date;

            $today = Carbon::now();
            $yearly = $this->getNextYearDate($membership_date);

            $purchase = Purchase::whereIn('purchase_status',[4002,4007])
                ->where('user_id',$user_id)->orderBy('id','DESC')->first('updated_at');

            $paid_date = $purchase->updated_at;
            $upgrade_date = Carbon::parse($paid_date)->addWeeks(1);

            if(($yearlyPurchases > $advanced->description->amount) && ($yearlyPurchases < $premium->description->amount)) {
                // upgrade to Advanced if user level < 6001
                if($userInfo->user_level == 6000) {
                    if($today > $upgrade_date) {
                        $this->upgradeMembership($user->id);
                    }
                }
            }
            elseif($yearlyPurchases > $premium->description->amount) {
                // upgrade to Premium if user level < 6002
                if($userInfo->user_level < 6002) {
                    if($today > $upgrade_date) {
                        // temporary disabled
                        // $this->upgradeMembership($user->id);
                    }
                }
            }
            elseif(($yearlyPurchases < $advanced->description->amount) && $today > $yearly) {
                // downgrade to Normal if user level = 6001
                if($userInfo->user_level == 6001) {
                    $this->downgradeMembership($user->id);
                }
            }
            elseif($yearlyPurchases < $premium->description->amount) {
                // downgrade to Advanced if user level = 6002 after 3 years
            }
            else {
                //
            }
            $userInfo = UserInfo::where('account_id',$user_id)->first();

            if($userInfo) {
                $userInfo->last_checked = Carbon::now();
                $userInfo->save();
            }
        }

    }

    public function upgradeMembership($user_id) {
        $userInfo = UserInfo::where('account_id',$user_id)->first();

        if($userInfo) {
            switch ($userInfo->user_level) {
                case 6001:
                    $userInfo->user_level = 6002;
                  break;
                case 6002:
                    $userInfo->user_level = 6002;
                  break;
                default:
                    $userInfo->user_level = 6001;
            }

            $userInfo->account_status = 1;
            $userInfo->membership_date = Carbon::now();
            $userInfo->save();
        }

    }

    public function downgradeMembership($user_id) {
        $userInfo = UserInfo::where('account_id',$user_id)->first();

        if($userInfo) {
            switch ($userInfo->user_level) {
                case 6001:
                    $userInfo->user_level = 6000;
                  break;
                case 6002:
                    $userInfo->user_level = 6001;
                  break;
                default:
                    $userInfo->user_level = 6000;
            }

            $today = Carbon::now();
            $year = Carbon::createFromFormat('Y-m-d H:i:s', $today)->year;

            $newDate = $year. '-01-01 00:00:00';

            $userInfo->account_status = 1;
            $userInfo->membership_date = new Carbon($newDate);
            $userInfo->save();
        }
    }

    // public function test() {
    //     $start = Carbon::create(date('Y'), date('m'), 1,0);
    //     $nextweek = Carbon::parse($start)->addWeeks(1);

    //     dd($start,$nextweek);
    // }






}
