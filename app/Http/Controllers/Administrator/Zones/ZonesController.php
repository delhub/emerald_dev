<?php

namespace App\Http\Controllers\Administrator\Zones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShippingInstallations\Zones;
use App\Models\Globals\City;
use App\Models\Globals\State;
use App\Models\ShippingInstallations\Locations;
use App\Models\ShippingInstallations\Rates;
use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\Globals\Countries;

class ZonesController extends Controller
{
    public function index(Request $request)
    {

        $states = State::with('city')->get();

        $new_zones = Zones::searchFilter($request->searchBox, $request->selectSearch)
            ->groupBy('id')
            ->paginate(20);

        $allStates = State::all();
        $allCities = City::all();
        $allCountry = Countries::all();

        //    $allZones = Zones::all();

        //    $zoneLocations = array();

        //    foreach ($new_zones as $zoneLocation) {
        //        foreach ($zoneLocation->locations as $location) {

        //         // return $location->location_type;


        //             if ($location->location_type == 'city') {
        //                 $zoneLocations[] = City::find($location->location_id)->name;
        //             } else {
        //                 $zoneLocations[] = State::find($location->location_id)->name;
        //             }

        //         }
        //     }

        // $new_zones = $new_zones->paginate(10);

        // return $zoneLocations;

        $new_zones->appends(request()->query());

        return view('administrator.zones.index')
            // ->with('zoneLocations', $zoneLocations)
            ->with('allStates', $allStates)
            ->with('allCities', $allCities)
            ->with('allCountry', $allCountry)
            ->with('new_zones', $new_zones)
            ->with('request', $request);
    }

    public function create()
    {


        $zones = Zones::all();

        $locations = self::locationLabels();

        $zone_locations = Locations::all();
        $shipCategorys = Ship_category::orderBy('cat_type', 'DESC')->get();
        $countries = Countries::get();

        return view('administrator.zones.create', compact(['zones','zone_locations', 'shipCategorys', 'locations', 'countries']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function locationLabels()
    {

        $states = State::with('city')->get();
        $cities = City::with('state')->get();
        $countries = Countries::get();

        $locations = array();

        foreach ($countries as $country) {
            $locations['country_' . $country->country_id] = $country->country_name;
        }

        foreach ($states as $state) {
            $locations['state_' . $state->id] = $state->name;
        }
        // return $locations;

        foreach ($cities as $city) {
            $locations['city_' . $city->id] = $locations['state_' . $city->state_id]  . " > " . $city->city_name;
        }
        asort($locations);
        return $locations;
    }
    public function store(Request $request)
    {
        //   $this->validate($request, [
        //     'zone_id' => 'required|unique:global_zones|max:25',
        //     'sequence' => 'required',

        //     'location_type' => 'required',
        //     'location_id' => 'required'
        // ]);

        // $this->validate($request, [
        //     'zone_id' => 'required',
        //     'location_type' => 'required',
        //     'location_id' => 'required'
        // ]);

        $zone = new Zones;
        $zone->zone_name = $request->input('zone_name');
        $zone->sequence = $request->input('sequence');
        $zone->country_id = $request->input('country_id');

        $zone->save();

        // dd($zone->id);

        foreach ($request->input('zoneLocations') as $key => $zone_location) {

            $location = explode("_", $zone_location);

            $locationTable = new Locations;

            $locationTable->zone_id = $zone->id;
            $locationTable->location_type =  $location[0];
            $locationTable->location_id = $location[1];

            // return $locationTable;
            $locationTable->save();
        }
        foreach ($request->input('rate_zone_id') as $key => $ratesInput) {
            $ratesTable = Rates::updateOrCreate(
                [
                    'id' =>  $request->input('rate_id')[$key],
                ],
                [
                    'zone_id' =>  $zone->id,
                    'category_id' =>  $request->input('category_id')[$key],
                    'condition' =>  $request->input('conditions')[$key],
                    'min_rates' => $request->input('conditions')[$key],
                    'max_rates' => $request->input('conditions')[$key],
                    'price' => str_replace('.', '', $request->input('price'))[$key],
                    'calculation_type' => $request->input('groupOrIndividual')[$key],
                ]
            );
        }

        return redirect()->route('administrator.zones.index')->with('success', 'Submit Done.');
    }

    public function edit($id)
    {
        $zones = Zones::where('id', $id)->with('locations')->first();
        $ratesTable = Rates::where('zone_id', $id)->get();

        $locations = self::locationLabels();

        $zoneLocations = array();

        foreach ($zones->locations as $zoneLocation) {
            $zoneLocations[] = $zoneLocation->location_type . '_' . $zoneLocation->location_id;
        }

        // return $zoneLocations;
        // return $zones->locations;
        // dd($id);

        // $zone_locations = Locations::where('zone_id',$zones->zone_id)->get();
        $shipCategorys = Ship_category::orderBy('cat_type', 'DESC')->get();
        $countries = Countries::get();

        return view('administrator.zones.edit')
            ->with('zones', $zones)
            ->with('zoneLocations', $zoneLocations)
            ->with('shipCategorys', $shipCategorys)
            ->with('locations', $locations)
            ->with('ratesTable', $ratesTable)
            ->with('countries', $countries);
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        // return $id;
        $this->validate($request, [
            // 'zone_id' => 'required',
            'zone_name' => 'required',
            'sequence' => 'required'
        ]);

        // $this->validate($request, [
        //     'zone_id' => 'required',
        //     'location_type' => 'required',
        //     'location_id' => 'required'
        // ]);

        $zone = Zones::find($id);

        // $zone->zone_id = $request->input('zone_id');
        $zone->zone_name = $request->input('zone_name');
        $zone->sequence = $request->input('sequence');
        $zone->country_id = $request->input('country_id');

        $zone->save();

        $oldLocations = Locations::where('zone_id', $id)->delete();
        foreach ($request->input('zoneLocations') as $key => $zone_location) {

            $location = explode("_", $zone_location);

            $locationTable = new Locations;

            $locationTable->zone_id = $request->input('rate_zone_id')[0];
            $locationTable->location_type =  $location[0];
            $locationTable->location_id = $location[1];
            $locationTable->save();
        }
        // return $request->input('rate_id');

        $rateZoneId = $request->input('rate_zone_id')[0];
        $updatedRates = $request->input('rate_id');

        $ratesCompares = Rates::where('zone_id', $rateZoneId)->whereNotIn('id', $updatedRates)->get();
        foreach ($ratesCompares as $ratesCompare) {
            $ratesCompare->delete();
        }
        foreach ($request->input('rate_zone_id') as $key => $ratesTable) {
            $ratesTable = Rates::updateOrCreate(
                [
                    'id' =>  $request->input('rate_id')[$key],
                ],
                [
                    'zone_id' =>  $request->input('rate_zone_id')[$key],
                    'category_id' =>  $request->input('category_id')[$key],
                    'condition' =>  $request->input('conditions')[$key],
                    'min_rates' => $request->input('min_rates')[$key],
                    'max_rates' => $request->input('max_rates')[$key],
                    'price' => str_replace('.', '', $request->input('price'))[$key],
                    'calculation_type' => $request->input('groupOrIndividual')[$key],
                    'set_status' => $request->input('set_status')[$key],
                    'active' => ($request->input('active')[$key])
                ]
            );
        }


        // $zone_locations->save();

        return redirect()->route('administrator.zones.index')->with('success', 'Zones Updates.');
    }
}
