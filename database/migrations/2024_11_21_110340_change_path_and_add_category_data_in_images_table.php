<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ChangePathAndAddCategoryDataInImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('images')
            ->where(function ($query) {
                $query->where('path', 'like', 'uploads/images/categories/%')
                      ->orWhere('path', 'like', '/storage/uploads/images/categories/%');
            })
            ->where('imageable_type', 'App\Models\Categories\Category')
            ->update(['path' => 'images/icons/category/']);

        $categories = DB::table('categories')->where('type', 'shop')->get();

        foreach ($categories as $category) {
            $exists = DB::table('images')
                ->where('imageable_id', $category->id)
                ->where('imageable_type', 'App\Models\Categories\Category')
                ->exists();

            if (!$exists) {
                DB::table('images')->insert([
                    'path' => 'images/icons/category/',
                    'filename' => $category->slug . '.png',
                    'default' => 0,
                    'brand' => 0,
                    'imageable_id' => $category->id,
                    'imageable_type' => 'App\Models\Categories\Category',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('images')
            ->where('path', 'like', 'images/icons/category%')
            ->where('imageable_type', 'App\Models\Categories\Category')
            ->update([
                'path' => DB::raw("REPLACE(path, 'images/icons/category/', 'uploads/images/categories/')")
            ]);

        DB::table('images')
            ->where('path', 'images/icons/category/')
            ->where('imageable_type', 'App\Models\Categories\Category')
            ->delete();
    }
}
