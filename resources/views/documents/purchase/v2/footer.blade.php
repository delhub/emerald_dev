<!-- Footer -->
<style>
    .footer{
        /* page-break-before: always; */
        position: absolute;
        width: 100%;
        bottom: 0;
        
    }
    .bottomBorder{
        /* position: absolute; */
        bottom: 0;
        width: 100%;
        border-top: 18px solid #ffcc00;
    }
    .footer-info{
        position: absolute;
        bottom: 30;
        width: 100%;
        text-align: center;
        margin: 0;
        font-size: .85rem;
        margin-bottom: 5px;
    }
</style>
<div class="footer">
    <p class="text-grey footer-info">This is a computer generated invoice, no signature is required.</p>
    <div class="bottomBorder"></div>
</div>

<!-- Footer -->
{{-- <p class="text-grey"
    style="position: absolute; bottom: 40; left: 0; right: 0; width: 100%; text-align: center; margin: 0; font-size: .85rem;">
    This is a computer generated invoice, no signature is required.</p>
<div style="position: absolute; bottom: 0; left: 0; right: 0; width: 100%; border-top: 32px solid #ffcc00;">
</div> --}}
<!-- End Footer -->

<!-- End Footer -->