<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.0.0-beta.0
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ config('app.name', 'Laravel') }} - Administrator</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.favicon')
    {{-- @include('layouts.google-tags') --}}
    <!-- Main styles for this application-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/slimselect.min.css') }}?v=1.0" rel="stylesheet" />

    <!-- Summernote Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">


    <style>
        .sidebar .sidebar-nav {
            width: 100%;
        }

        .sidebar .nav-link {
            color: #ffffff;
            letter-spacing: .015rem;
        }

        .sidebar .nav-link:hover {
            color: #ffffff;
            rgba(0, 0, 0, 0.15);
        }

        .sidebar .nav-link:hover .nav-icon {
            color: #ffffff;
        }

        .sidebar .nav-dropdown.open .nav-link {
            color: #ffffff;
            background-color: rgba(0, 0, 0, 0.15);
        }

        .sidebar .nav-dropdown.open .nav-link:hover {
            color: #ffffff;
        }

        ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 7px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: rgba(0, 0, 0, .5);
            box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }

        .input-container input {
            border: none;
            box-sizing: border-box;
            outline: 0;
            padding: .75rem;
            position: relative;
            width: 100%;
        }

        input[type="date"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
    @hasrole('management')
    <style>
    ul.nav > li:not(:nth-last-child(2))
    {
    display: none;
    }
    </style>
    @endhasrole
    @yield('css')
    @stack('style')

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    @include('layouts.administrator.navigations.topbar')
    <div class="">
        @include('layouts.administrator.navigations.sidebar')
        <main class="main">
            <!-- Breadcrumb-->
            @yield('breadcrumb')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Home</li>
                <li class="breadcrumb-item">
                    <a href="#">Admin</a>
                </li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <div class="container-fluid">
                <div id="app" class="animated">
                    @yield('content')
                </div>

                <div id="app2" class="animated">
                    @yield('content2')
                </div>
            </div>
        </main>
    </div>
    @include('layouts.administrator.navigations.footer')

    <!-- Main scripts for this application -->
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Summernote -->
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
    <script src="{{ asset('js/slimselect.min.js') }}"></script>

    @yield('js')
    @stack('script')
</body>

</html>
