<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertRefundVoucherToDiscountRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('discount_rule')->insert([
            'name' => 'Refund Voucher',
            'country_id' => 'MY',
            'rule_type' => 'refund',
            'coupon_type' => 'value',
            'minimum' => 1,
            'maximum' => 36000,
            'available_for' => 'all',
            'limit' => 1,
            'discount_type' => 'price',
            'expiry_date_type' => 1,
            'breakdown_exp_date' => 12,
            'remarks' => 'Refund Voucher',
            'created_at' => date('Y-m-d H:i.s'),
            'updated_at' => date('Y-m-d H:i.s')
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('discount_rule')->where('rule_type','refund')->delete();
    }
}
