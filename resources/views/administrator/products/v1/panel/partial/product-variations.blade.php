
<div id="colattr" class="row no-gutters {{ (($loop) && ($loop->last)) || (!$attribute) ? 'default-item':'' }} p-3 mb-1"
    style="border: 1px solid #b3b3b3; border-radius: 5px;">

    <input {{ $inactive ? 'readonly' : '' }} type="hidden" class="product_variation_id col-12"
        name="product_variation_id[]" id="newProductID" value="{{ $attribute ? $attribute->id : old('newProductID') }}"
        readonly>

    <div class="col-4">
        <div class="row">
            <label for="attriButeType" class="col-md-5 col-form-label font-weight-bold">Variation Type <small
                    class="text-danger">*</small></label>

            <div class="col-6 m-1">
                <select name="product_variation[]" id="attriButeType" class="select2 type-attr form-control "
                    onchange="variationDetails(this);" {{ $inactive ? 'disabled' : '' }}>
                    {{-- <option value="default">Select a variation type..</option> --}}
                    <option value="size" {{ ($attribute) && ($attribute->attribute_type == 'size') ? 'selected' : '' }}>
                        Size</option>
                    {{-- <option value="rbs" {{ ($attribute) && ($attribute->attribute_type == 'rbs') ? 'selected' : '' }}>
                        Recurring Booking Systems (RBS)</option> --}}
                </select>
            </div>
        </div>

        <div class="row">
            <label for="product_variation_name" class="col-md-5 col-form-label font-weight-bold">Variation Name<small
                    class="text-danger">*</small></label>

            <div class="col-6 m-1">
                <input {{ $inactive ? 'readonly' : '' }} type="text" id="product_variation_name"
                    name="product_variation_name[]" class="form-control" placeholder="Name"
                    value="{{ ($attribute) ? $attribute->attribute_name : '' }}">
            </div>
        </div>

        <div class="row">
            <label for="sales_count" class="col-md-5 col-form-label font-weight-bold">Online Sold:</label>
            <div class="col-6 m-1"> {{ ($attribute) ? $attribute->totalSold()['online'] : 0 }}</div>
        </div>

        <div class="row">
            <label for="sales_count" class="col-md-5 col-form-label font-weight-bold">Dummy Sold:</label>
            <div class="col-6 m-1"> {{ ($attribute) ? $attribute->totalSold()['dummy'] : 0 }}</div>
        </div>
    </div>

    <div class="col-4">
        <div class="row">
            <label for="product_variation_code" class="col-md-4 col-form-label font-weight-bold">Product Code<small
                    class="text-danger">*</small></label>

            <div class="col-7 m-1">
                <input {{ $inactive ? 'readonly' : '' }} {{ $inactive ? 'readonly' : '' }} type="text"
                    id="product_variation_code" name="product_variation_code[]" class="form-control d-inline"
                    placeholder="Product Code" value="{{ ($attribute) ? $attribute->product_code : '' }}">
            </div>
        </div>

        <div class="row">
            {{-- <label for="shipping_category" class="col-md-4 col-form-label font-weight-bold">Repeatable <small
                    class="text-danger">*</small></label>
            <div class="col-6 m-1">
                <select {{ $inactive ? 'disabled' : '' }} class="select2 type-attr form-control"
                    id="repeatable" name="repeatable[]">
                    <option value="0"
                        @if (isset($attribute) && $attribute->repeatable) {{$attribute->repeatable == 0 ? 'selected' : ''}} @endif
                        >No
                    </option>

                    <option value="1"
                        @if (isset($attribute) && $attribute->repeatable) {{$attribute->repeatable == 1 ? 'selected' : ''}} @endif
                        >Yes
                    </option>
                </select>
            </div> --}}
        </div>

        <div class="row">
            <label for="sales_count" class="col-md-5 col-form-label font-weight-bold">Retail Sold:</label>
            <div class="col-6 m-1"> {{ ($attribute) ? $attribute->totalSold()['retail'] : 0 }}</div>
        </div>
    </div>

    <div class="col-3">
        <div class="row">
            <label for="product_variation_code" class="col-md-4 col-form-label font-weight-bold">Physical Stock : </label>
            <div class="col-7 m-1">{{ ($attribute && $attribute->puchongWarehouseinventory ) ? $attribute->puchongWarehouseinventory->physical_stock : '0' }}</div>
        </div>

        <div class="row">
            <label for="product_variation_code" class="col-md-4 col-form-label font-weight-bold">Virtual stock</label>
            <div class="col-7 m-1">{{ ($attribute && $attribute->puchongWarehouseinventory ) ? $attribute->puchongWarehouseinventory->virtual_stock : '0' }}</div>
        </div>

        <div class="row">
            <label for="sales_count" class="col-md-4 col-form-label font-weight-bold">Total Sold:</label>
            <div class="col-7 m-1"> {{ ($attribute) ? $attribute->totalSold()['total'] : 0 }}</div>
        </div>
    </div>

    <div class="col-1">
        @if (isset($attribute) && $attribute->product_code)
            <img src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate( Request::getHost() . "/qr/item/?info=". $attribute->product_code )) }} ">
        @endif
    </div>


    {{-- <div class="col-md-1 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Product Bundle</label>
    </div> --}}
    <div class="col-md-12 noClone">
        <div class="col-11 m-md-4" style="border-top:1px solid lightgrey;">
        </div>

        <div class="row">
            <div class="col-3">
                <label for="product_bundle_yes_no" class="col-form-label font-weight-bold">Product Bundle Yes/No<small class="text-danger"> *</small></label>
                <select name="product_bundle_yes_no[]" class="form-control select2 pc"  {{ $inactive ? 'disabled' : '' }}>
                    <option value="0" {{(isset($attribute) && $attribute->productBundle->isNotEmpty()) ? '' : 'selected' }}>No</option>
                    <option value="1" {{(isset($attribute) && $attribute->productBundle->isNotEmpty() && $attribute->productBundle[0]->active ==1) ? 'selected' : '' }}>Yes (Normal)</option>
                    <option value="2" {{(isset($attribute) && $attribute->productBundle->isNotEmpty() && $attribute->productBundle[0]->active ==2) ? 'selected' : '' }}>Yes (Virtual Bundle)</option>
                </select>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12 productAttributeBundleContainer  attribute_{{$attribute ? $attribute->id : old('newProductID')}}">
                <div class="bundleRepeatableContainer">
                    @if ($attribute && $attribute->productBundle->where('active','>=',1)->isNotEmpty() && count($attribute->productBundle) > 0)
                        @foreach ($attribute->productBundle->where('active','>=',1) as $k =>$bundle)
                            @include('administrator.products.v1.panel.partial.product-variations-bundle')
                        @endforeach
                    @else
                        @if ($attribute)
                            <div class="default-item2" style="border: 1px solid #b3b3b3; border-radius: 5px;">
                                <input {{ $inactive ? 'readonly' : '' }} type="hidden" class="product_bundle_id col-12"
                                    name="product_bundle_id[{{ $attribute ? $attribute->id : old('newProductID') }}][]" id="productBundleID" value="{{ ($attribute && $attribute->productBundle->isNotEmpty()) ? $attribute->productBundle[0]->id : old('product_bundle_id') }}"
                                    readonly>

                                <div class="row" style="padding: 5px 10px 10px;">
                                    <div class="col-12 col-sm-12 col-md-10">
                                        <label for="product_bundle_primary_product_id" class="col-form-label font-weight-bold">Primary Product</label>
                                        <select name="product_bundle_primary_product_id[{{ $attribute ? $attribute->id : old('newProductID') }}][]" class="form-control select2 pc"  {{ $inactive ? 'disabled' : '' }}>
                                            <option value="">Please choose primary product code.</option>
                                            @foreach($productAttributes as $productAttribute)
                                                <option value="{{ $productAttribute->id }}"
                                                    {{(isset($attribute) && $attribute->productBundle->isNotEmpty() && $attribute->productBundle[0]->primary_product_code == $productAttribute->product_code && $attribute->productBundle[0]->active>=1) ? 'selected' : '' }}>
                                                    {{ $productAttribute->product_code }}({{ $productAttribute->product_status == 1 ? 'active' : 'inactive' }}) - {{ $productAttribute->name }} ({{ $productAttribute->attribute_name }})({{ $productAttribute->attribute_active == 1 ? 'active' : 'inactive' }})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-12 col-sm-12 col-md-2"></div> --}}
                                    <div class="col-12 col-sm-12 col-md-2">
                                        <label for="product_bundle_primary_quantity" class="col-form-label font-weight-bold">Primary Quantity</label>
                                        <input {{ $inactive ? 'readonly' : '' }} type="number" id="product_bundle_primary_quantity" name="product_bundle_primary_quantity[{{ $attribute ? $attribute->id : old('newProductID') }}][]"
                                            class="form-control input-mask-price d-inline" style="width: 100%;"
                                            value="{{ ($attribute) && ($attribute->productBundle->isNotEmpty() && $attribute->productBundle[0]->active>=1) ? $attribute->productBundle[0]->primary_quantity : '' }}">
                                    </div>
                                </div>
                                <button type="button" class="btn btn-danger d-none" style="width: 100%;" onclick="removeBundleDesignSetElement(this);">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        @endif
                    @endif
                </div>
                <button type="button" class="btn btn-success mt-1" style="width: 100%;" onclick="addMoreBundleDesignSetElement(this);">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>

        {{-- For Product Limit --}}
        {{-- <div class="col-11 m-md-4" style="border-top:1px solid lightgrey;">
        </div>
        <div class="card p-4">
            <div class="row">
                <div class="col-3">
                    <label for="product_limit_yes_no" class="col-form-label font-weight-bold">Product Purchase Limit Yes/No<small class="text-danger"> *</small></label>
                    <select name="product_limit_yes_no[]" class="form-control select2 pc"  {{ $inactive ? 'disabled' : '' }} data-id="{{ $attribute ? $attribute->id : old('newProductID')}}">
                        <option value="na" {{ isset($attribute) && $attribute->purchase_limit_type == 'no' ? 'selected' : '' }}>No</option>
                        <option value="roles" {{ isset($attribute) && $attribute->purchase_limit_type == 'roles' ? 'selected' : ''  }}>Roles Restrict</option>
                        <option value="agent" {{ isset($attribute) && $attribute->purchase_limit_type == 'agent' ? 'selected' : '' }}>Agent Restrict</option>
                    </select>
                </div>
            </div>
            @php
                $productLimit = null;
                if (isset($attribute) && $attribute->productLimit) {
                    $productLimit = $attribute->productLimit->first();
                }
            @endphp
            <div class="row mt-2 product-limit-yes">
                <div class="col-12 productAttributeBundleContainer  attribute_{{$attribute ? $attribute->id : old('newProductID')}}">
                    <div class="bundleRepeatableContainer">
                        <div class="default-item2" style="border: 1px solid #b3b3b3; border-radius: 5px;">
                            <input {{ $inactive ? 'readonly' : '' }} type="hidden" class="product_limit_id col-12"
                                name="product_limit_id[{{ $attribute ? $attribute->id : old('newProductID') }}][]" id="productLimitID" value="{{ ($attribute && $attribute->productLimit->isNotEmpty()) ? $attribute->productLimit[0]->id : old('product_limit_id') }}"
                                readonly>
                            <div class="row roles_restriction-{{ $attribute ? $attribute->id : old('newProductID') }}" style="padding: 5px 10px 10px;">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="product_limit_duration" class="col-form-label font-weight-bold">Duration</label>
                                    <select name="product_limit_duration[{{ $attribute ? $attribute->id : old('newProductID') }}][]" class="form-control select2 pc"  {{ $inactive ? 'disabled' : '' }}>
                                        <option value="" selected disabled >Please choose duration.</option>
                                        <option value="yearly" @if ($productLimit && $productLimit->duration == 'yearly') selected @endif>Yearly</option>
                                        <option value="monthly" @if ($productLimit && $productLimit->duration == 'monthly') selected @endif>Monthly</option>
                                        <option value="weekly" @if ($productLimit && $productLimit->duration == 'weekly') selected @endif>Weekly</option>
                                        <option value="daily" @if ($productLimit && $productLimit->duration == 'daily') selected @endif>Daily</option>
                                    </select>
                                </div>

                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="product_limit_roles" class="col-form-label font-weight-bold">Apply To</label>
                                    <select class="select2 form-control role" multiple="multiple" name="product_limit_roles[{{ $attribute ? $attribute->id : old('newProductID') }}][]">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->name }}"  @if ($productLimit && in_array($role->name, json_decode($productLimit->roles))) selected @endif >{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-4 col-sm-12 col-md-4">
                                    <label for="purchase_limit_amount" class="col-form-label font-weight-bold">Purchase Limit Quantity</label>
                                    <input type="number" name="purchase_limit_amount[{{ $attribute ? $attribute->id : old('newProductID') }}][]" class="form-control"
                                    value="{{ ($productLimit && $productLimit->amount) ? $productLimit->amount : '' }}">
                                </div>
                            </div>
                            <div id="agent-restriction-{{ $attribute ? $attribute->id : old('newProductID') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}

    </div>

    <div class="col-11 m-md-4" style="border-top:1px solid lightgrey;">
    </div>

    <div class="col-md-1 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Product Data</label>
    </div>

    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="product_measurement_length" class="col-form-label font-weight-bold">Length (CM)</label>
                <input {{ $inactive ? 'readonly' : '' }} type="text" id="product_measurement_length"
                    placeholder="00000.00" name="product_measurement_length[]"
                    class="form-control input-mask-price d-inline" style="width: 100%;"
                    value="{{ ($attribute) && ($attribute->product_measurement_length) ? number_format($attribute->product_measurement_length, 2, '.', '') : '' }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_width" class="col-form-label font-weight-bold">Width (CM)</label>
                <input {{ $inactive ? 'readonly' : '' }} type="text" id="product_measurement_width"
                    placeholder="00000.00" name="product_measurement_width[]"
                    class="form-control input-mask-price d-inline" style="width: 100%;"
                    value="{{ ($attribute) && ($attribute->product_measurement_width) ? number_format($attribute->product_measurement_width, 2, '.', '') : '' }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_depth" class="col-form-label font-weight-bold">Depth (CM)</label>
                <input {{ $inactive ? 'readonly' : '' }} type="text" id="product_measurement_depth"
                    placeholder="00000.00" name="product_measurement_depth[]"
                    class="form-control input-mask-price d-inline" style="width: 100%;" value="{{ ($attribute) && ($attribute->product_measurement_depth) ? number_format($attribute->product_measurement_depth, 2, '.', '') : '' }}
                ">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_weight" class="col-form-label font-weight-bold">Weight (KG)</label>
                <input {{ $inactive ? 'readonly' : '' }} type="text" id="product_measurement_weight"
                    placeholder="00000.00" name="product_measurement_weight[]"
                    class="form-control input-mask-price d-inline" style="width: 100%;"
                    value="{{ ($attribute) && ($attribute->product_measurement_weight) ? number_format($attribute->product_measurement_weight, 2, '.', '') : '' }}">

            </div>
        </div>
    </div>

    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    @php $attributeID = ($attribute) && ($attribute->id) ? $attribute->id : ''; @endphp
    <div class="col-md-2 my-auto">
        <label for="" class="my-auto col-form-label font-weight-bold">Product Price</label>
    </div>
    <div class="col-md-10">
        <nav>
            <div class="nav nav-tabs" id="nav-tab-price" role="tablist">
                <a class="nav-item nav-link active" id="nav-current-price{{$attributeID}}" data-toggle="tab"
                    href="#nav-href-current-price{{$attributeID}}" role="tab" aria-controls=""
                    aria-selected="true">Current Price</a>
                <a class="nav-item nav-link" id="nav-previous-price{{$attributeID}}" data-toggle="tab"
                    href="#nav-href-previous-price{{$attributeID}}" role="tab" aria-controls=""
                    aria-selected="false">Previous Price</a>
                @if ($inactive == FALSE)
                <a class="nav-item nav-link" id="nav-new-price{{$attributeID}}" data-toggle="tab"
                    href="#nav-href-new-price{{$attributeID}}" role="tab" aria-controls="" aria-selected="false">New
                    Price</a>
                @endif
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent-price">
            <div class="tab-pane fade show active" id="nav-href-current-price{{$attributeID}}" role="tabpanel-price"
                aria-labelledby="nav-current-price{{$attributeID}}" style="background-color: rgb(209, 226, 252)">
                @include('administrator.products.v1.panel.partial.product-variations-current-price')
            </div>

            <div class="tab-pane fade" id="nav-href-previous-price{{$attributeID}}" role="tabpanel-price"
                aria-labelledby="nav-previous-price{{$attributeID}}">
                @if($attribute && $attribute->getInactivePriceAttributes()->isNotEmpty())
                @foreach($attribute->getInactivePriceAttributes() as $key => $price)
                <div style="border-style:double;background-color: rgb(209, 226, 252)">
                    @include('administrator.products.v1.panel.partial.product-variations-previous-price')
                </div>
                <br>
                @endforeach
                @else
                <p style="color:rgb(158, 79, 79)"><b>NOT PREVIOUS PRICE</b></p>
                @endif
            </div>

            <div class="tab-pane fade" id="nav-href-new-price{{$attributeID}}" role="tabpanel-price"
                aria-labelledby="nav-new-price{{$attributeID}}" style="background-color: rgb(209, 226, 252)">
                @include('administrator.products.v1.panel.partial.product-variations-new-price')
            </div>
        </div>
    </div>


    <div class="col-11 m-md-4" style="border-bottom:1px solid lightgrey;">
    </div>


    <div class="col-12">
        <div class="row">
            <div class="col-12 col-md-2">
                <label for="shipping_category" class="font-weight-bold">Shipping Categories <small
                        class="text-danger">*</small></label>
            </div>
            <div class="col-12 col-md-10">
                <select {{ $inactive ? 'disabled' : '' }} class="select2 form-control productCategory"
                    id="shipping_category" name="shipping_category[]">
                    @foreach($shippingCategory->where('cat_type', 'shipping'); as $shippingcategory)
                    <option value="{{ $shippingcategory->id }}" @if (isset($attribute) && $attribute->shipping_category)
                        {{$shippingcategory->id == $attribute->shipping_category ? 'selected' : ''}}
                        @endif
                        >Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12 col-md-2">
                <label for="shipping_category_special" class="font-weight-bold;" style="color:red;">Shipping
                    Categories(Special)</label>
            </div>
            <div class="col-12 col-md-10">
                <select {{ $inactive ? 'disabled' : '' }} class="select2 form-control productCategory"
                    id="shipping_category_special" name="shipping_category_special[]">
                    <option value="0">NONE</option>
                    @foreach($shippingCategory->where('cat_type', 'shipping_special'); as $shippingcategory)
                    <option value="{{ $shippingcategory->id }}"
                        {{(($attribute)&&$shippingcategory->id == $attribute->shipping_category_special) ? 'selected' : ''}}>
                        Name : {{ $shippingcategory->cat_name }}, Description : {{ $shippingcategory->cat_desc}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    @if ($attribute)
        <div class="col-11 m-md-4 hideRBS" style="border-bottom:1px solid lightgrey;">
        </div>
        <div class="col-md-2 hideRBS">
            <label for="" class=" col-form-label font-weight-bold">Product RBS</label>
        </div>
        <div class="col-10 row hideRBS">
            @include('administrator.products.v1.panel.partial.product-variations-rbs')
        </div>
    @endif




    <div class="col-11 m-md-4" style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-12 mt-md-3">
        <div class="row">
            <label for="product_variation_sv" class="col-md-2 my-auto col-form-label font-weight-bold">Product Image
                <br> <a onclick="reloadImage({{ $attributeID }})" id="reloadImage"
                    style="cursor: pointer;color:blue;"><u>Reload Image</u></a></label>

            <div id="imgclr" class="imgclr-{{ $attributeID }}">
                @foreach ($images as $image)
                <label style="position: relative;">
                    <input {{ $inactive ? 'disabled' : '' }} type="checkbox" id="colorImages" name="productImage[]"
                        value="{{$image->id}}" style="width: 20px;height: 20px;position: absolute;top: 4px;"
                        {{ ($attribute) && ($image->id == $attribute->color_images)  ? "checked" : "" }}>
                    <img src="@if ($image) {{ asset('storage/' . $image->path . $image->filename) }} @else {{ asset('assets/images/errors/image-not-found.png') }} @endif"
                        style="width: 80px;object-fit: cover;height: 80px;border-radius: 100%;border: 2px solid #d8d8d8;margin: 5px;"
                        alt="">
                    <p style="text-align: center;">id-{{$image->id}}</p>
                </label>
                @endforeach
                <div id="mynote" style="display:none;">*Please select one images</div>
            </div>
        </div>
    </div>
    @if (!$inactive)
    <div class="ml-auto col-2 mr-4 mt-2">
        {{-- @if( $loop && !$loop->last)
            <button type="button" class="btn btn-danger" style="width: 100%;"
                onclick="removeProductVariationElement(this);">
                <i class="fa fa-minus"></i>
            </button>
            @else --}}
        {{-- d-none --}}

        <button type="button" class="btn btn-danger" style="width: 100%;"
            onclick="removeProductVariationElement(this);"> Disabled
            {{-- <i class="fa fa-minus"></i> --}}
        </button>
        <button type="button" class="btn btn-success d-none" style="width: 100%;"
            onclick="addMoreProductVariationElement();">
            <i class="fa fa-plus"></i>
        </button>
        @if( !$loop || $loop && $loop->last)
        <button type="button" class="btn btn-success" style="width: 100%;" onclick="addMoreProductVariationElement();">
            <i class="fa fa-plus"></i>
        </button>
        @endif

    </div>
    @else
    <div class="ml-auto col-2 mr-4 mt-2">
        <button type="button" class="btn btn-danger" style="width: 100%;"
            onclick="enableVariations({{ $attributeID }});">
            Enabled
            <div class="spinner-border spinner-border-sm text-dark variationLoader" role="status" style="display:none;">
                <span class="sr-only">Loading...</span>
            </div>
        </button>
        {{-- <small class="variationSaveFirst" style="">Any unsaved changes will be lost</small> --}}
        <small class="variationRefresh" style="display: none;">Refresh to edit this variations</small>
    </div>
    @endif


</div>

@push('variation-scripts')
<script>
    //
    $(document).ready(function(){
        var selections = $('select[name="product_limit_yes_no[]"]')
        selections.each(function() {
            var selection = $(this).val()
            
            var attr_id = $(this).attr('data-id')
            
            purchase_limit(selection, attr_id)
        });

    })

    $(document).on('change', 'select[name="product_limit_yes_no[]"]', function(){
        var selection = $(this).val()
        var attr_id = $(this).attr('data-id')
        
        purchase_limit(selection, attr_id)
    })

    $(document).on('click', '.agent_remove', function(){
        $(this).closest('.row.agent-data').remove()
    })

    $(document).on('click', '.agent_add', function(){
        var container = $(this).parent()
        var target = container.find('.agent-data').last()
        
        container.append(target.clone())
    })

    function purchase_limit(selection, attr_id){
            $.ajax({
                url: "{{ route('administrator.get-agent-list') }}",
                method: 'GET',
                data: {
                    selection: selection,
                    attr_id: attr_id
                },
                success: function(result) {
                    var html = result.html 
                    if(selection == 'agent'){
                        $('.roles_restriction-' + attr_id).hide()
                    }else{
                        $('.roles_restriction-' + attr_id).show()
                    }
                    $('#agent-restriction-' + attr_id).html(html)
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            })
    }
</script>

@endpush
