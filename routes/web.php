<?php

use App\Models\Users\Dealers\DealerInfo;

use App\Models\Users\UserInfo;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route to finalize order status to deliver state once customer scan the QR
//   Route::get('/QR-Completed','SomeControllerMethod');
// Show order confirmation when customer scan QR (invoice)
// Route::get('/orders/confirm/{purchase_num}', 'Order\OrderController@show')
//     ->name('confirm-order')
//     ->middleware('signed');

// Return invoice
// Route::get('/shop/invoice', 'Purchase\PurchaseController@invoiceCustomer');

// Return invoice in customer dashboard (Orders)
Route::get('/orders/invoice/{purchase_num}', 'Shop\ValueRecordsController@invoice');

//Return receipt in customer dashboard(Receipt)

Route::get('/orders/receipt/{purchase_num}', 'Shop\ValueRecordsController@receipt');

Route::get('/purchase-order', 'Panel\DashboardController@viewPurchaseOrder');
//Return Work In progress page
Route::view('/wip', 'errors.wip');


/**
 * Route for processing cc payemnt
 */
Route::post('/payment/gateway-response', 'Purchase\PaymentGatewayController@paymentGatewayResponse')->name('gateway.responses');

/**
 * Temporary routes.
 */
Route::get('/shop/purchase-order', 'Panel\DashboardController@viewPurchaseOrder');
//Return Work In progress page
Route::view('/wip', 'errors.wip');

//testing route
Route::get('/dev', 'Development\DevController@index')->name('dev.index');

Route::get('/qr/item/{id}', 'Product\QrcController@qrItemDetail')->name('qr.item.detail');
Route::get('/qr/item', 'Product\QrcController@qrItemDetailProductCode')->name('qr.item.detailPC');

Route::group(['prefix' => 'direct-pay'], function () {
    Route::get('/{directID}', 'Register\v1\DealerRegisterController@directPayRegistration')->name('agent.direct.pay');
    Route::get('/check/{directID}', 'Register\v1\DealerRegisterController@checkDirectPay')->name('agent.check.direct.pay');
    Route::post('/save', 'Register\v1\DealerRegisterController@savedirectPayData')->name('agent.post.direct.pay');
    Route::post('/payment/{directID}', 'Purchase\PaymentGatewayController@paymentGatewayRequest')->name('user.pay.direct.link');
    Route::get('/payment/{id}', 'Purchase\PaymentGatewayController@paymentStatus')->name('direct.pay.success');
});

Route::get('/label-info/{data}', 'Administrator\v1\Product\WarehouseController@labelInfo')->name('labelInfo');

Route::get('/warehouse-box-qrScanned/{boxId}', 'Warehouse\StockTransfer\StockTransferController@boxQrScanned')
    ->name('warehouse.StockTransfer.box-qrScanned');
/**
 *
 * Guests Routes.
 */
Route::group(['middleware' => ['guest']], function () {

    // Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
    // Account Data Error

    // Landing page.
    Route::get('/', 'Guest\GuestController@index');

    // Login page.
    Route::get('/login', 'Auth\LoginController@showLoginForm');

    //Temporary first page login routes.
    // Route::get('/', 'Auth\LoginController@showLoginForm');

    // Registration page.
    /*

    // Route::get('/register-dealer', 'Auth\RegisterController@showDealerRegistrationForm'); // Old route.
    // Dealer registration page first step.
    Route::get('/register-dealer-old', 'Register\DealerRegisterController@showFirstRegistrationForm')
        ->name('guest.register.dealer');

    // Dealer registration POST.
    Route::post('/register-dealer-old', 'Register\DealerRegisterController@dealerRegister')
        ->name('guest.register.dealer.post');
    */
    // Walk-in registration
    Route::get('/member-info/{agentID}', 'Auth\RegisterController@showRegisterationWalkInForm')
        ->name('guest.register.walk-in');

    Route::post('/member-info', 'Auth\RegisterController@createQRRegisterationWalkInForm')
        ->name('guest.register.walk-in.post');

    Route::get('/member-info-qr', 'Auth\RegisterController@showQRRegisterationWalkInForm')
        ->name('guest.register.walk-in-qr');

    // Walk-in registration
    Route::get('/member-info/{agentID}', 'Auth\RegisterController@showRegisterationWalkInForm')
        ->name('guest.register.walk-in');

    Route::post('/member-info', 'Auth\RegisterController@createQRRegisterationWalkInForm')
        ->name('guest.register.walk-in.post');

    Route::get('/register-dealer', 'Register\v1\DealerRegisterController@showRegistrationForm')
        ->name('guest.register.agent');

    Route::get('/register-dealer-launching', 'Register\v1\DealerRegisterController@showRegistrationFormSG')
        ->name('guest.register.agent-sg');

    Route::post('/register-dealer-launching', 'Register\v1\DealerRegisterController@dealerRegisterSG')
        ->name('guest.register.agent.post-sg');

    Route::post('/register-dealer', 'Register\v1\DealerRegisterController@dealerRegister')
        ->name('guest.register.agent.post');

    Route::get('/register', 'Auth\RegisterController@showPanelRegistrationForm');

    Route::get('/check-invoice/{invoice_no}', 'Auth\RegisterController@checkInvoice')->name('customer.check.invoice');

    // Route::get('/register-dealer/payment/try-again', 'Register\DealerRegisterController@paymentTryAgain');

    // Launch page.
    Route::get('/bujishu-launch', 'Guest\GuestController@bujishuLaunch');

    // Customer Registration POST
    Route::post('/register-customer', 'Register\CustomerRegisterController@store')
        ->name('guest.register.customer.post');
    // Customer Registration POST
    Route::get('/register-customer/resend-email', 'Register\CustomerRegisterController@verifyPage')
        ->name('guest.register.customer.get');

    Route::post('/check-id', 'Register\v1\DealerRegisterController@checkCustomerID')
        ->name('guest.check.customerid');

    Route::post('/get-invoice', 'Register\v1\DealerRegisterController@getInvoiceRegister')
        ->name('guest.get.invoice');

    Route::post('/get-postcode', 'PoslajuAPI@get_postcode_details')->name('shop.cart.get-postcode');
});
Route::post('/state-filter-city', 'Register\CustomerRegisterController@stateFilterCity')
    ->name('guest.register.customer.state-filter-city');

Route::post('/state-filter-company-city', 'Register\CustomerRegisterController@stateFilterCompanyCity')
    ->name('guest.register.customer.state-filter-company-city');

// Warranty


// Warranty Submit
Route::get('/warranty/waranty-submit', 'Guest\GuestController@warantySubmitted')
    ->name('guest.warranty.submit');

// Bed and Mattress
Route::get('/warranty/bed-mattress', 'Guest\GuestController@warrantyForm')
    ->name('guest.warranty.form');



/**
 * Public Signed Routes.
 */
Route::get('/order-received/{orderNum}', 'Purchase\PurchaseController@qrScanned')
    ->name('guest.order-received');

/**
 * Public Routes.
 */
Route::put('/order-received/{orderNumber}', 'Purchase\PurchaseController@qrSubmit')
    ->name('guest.order-received.post');



// Authentication routes. Verification is set to true.
Auth::routes(['verify' => true]);

Route::get('/account-error', 'Guest\GuestController@accountError')->name('account.error');

/**
 * Any routes that needs a user to be logged in without a verified account.
 */
Route::group(['middleware' => ['auth']], function () {
    // Payment gateway response.
    // Route::post(
    //     '/register-dealer/payment/response',
    //     'Register\DealerRegisterController@dealerRegisterPaymentGatewayResponse'
    // );
    // Customer registration information.
    Route::get('/register-customer/information', 'Register\CustomerRegisterController@customerInformationRegisterView');

    // Customer registration information POST
    Route::post('/register-customer/information', 'Register\CustomerRegisterController@customerInformationRegister')
        ->name('guest.register.customer.information.post');

    // Customer registration information.
    Route::get('/register-customer/information', 'Register\CustomerRegisterController@customerInformationRegisterView');

    // Customer registration information POST
    Route::post('/register-customer/information', 'Register\CustomerRegisterController@customerInformationRegister')
        ->name('guest.register.customer.information.post');

    // Dealer registration.
    Route::get('/register-dealer/information', 'Register\v1\DealerRegisterController@dealerInformationRegisterView');

    // Dealer registration information POST
    Route::post('/register-dealer/information', 'Register\v1\DealerRegisterController@dealerInformationRegister')
        ->name('guest.register.dealer.information.post');

    // Route::get('/register-dealer/payment/try-again', 'Register\DealerRegisterController@paymentTryAgain');

    // Route::post('/register-dealer/payment/try-again', 'Register\DealerRegisterController@paymentTryAgainPost')
    //     ->name('guest.register.dealer.payment.try.post');
});

/**
 * Any routes that needs a user to be logged in with a verified account,
 * place under this group.
 */

// sg-launch-package
Route::group(['prefix' => 'launch-package', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', 'Shop\SGLaunchPackage@index')->name('sg.launch.index');
    Route::post('/buy-now', 'Shop\SGLaunchPackage@buyNow')->name('sg.launch.buyNow');
    Route::get('/cashier', 'Purchase\PurchaseController@paymentOption')->name('sg.launch.payment');
    Route::get('/{id}', 'Purchase\PaymentGatewayController@paymentStatus')->name('payment.success');
});


// Payment
Route::group(['prefix' => 'payment', 'middleware' => ['auth', 'verified']], function () {
    // View for user to select payment option and provide their payment information.
    Route::get('/cashier', 'Purchase\PurchaseController@paymentOption')->name('payment.cashier');
    Route::get('/{id}', 'Purchase\PaymentGatewayController@paymentStatus')->name('payment.success');
    Route::put('/update-customer-details', 'Purchase\PurchaseController@updateCustomerPaymentDetail')->name('payment.update-details');

    // Handle POST request after user selected payment option and provided their payment information.
    Route::post('/', 'Purchase\PaymentGatewayController@paymentGatewayRequest');
    Route::get('/purchase-limit/check', 'Purchase\PurchaseController@purchaseLimitCheck')->name('purchase-limit.check');

    Route::get('/hitpay-requests/{id}', 'Purchase\HitpayGatewayController@paymentRequest')->name('hitpay.request');
});
// End Payment


Route::group(['middleware' => ['auth', 'verified', 'check.info']], function () {


    Route::get('/view-pdf/{pdf_type}/{purchase_number}/{order_number?}', 'Purchase\PaymentGatewayController@viewPDF')->name('global.view.pdf');
    // Management
    Route::group(['prefix' => 'management', 'middleware' => ['role:dealer|panel|administrator']], function () {
        // Dashboard-Panel
        Route::get('/panel/home', 'Management\ManagementController@index')->name('management.panel.home');
        Route::put('/panel/update-order/{order_num}', 'Management\ManagementController@updateOrder')->name('update.order.panel');
        Route::put('/panel/update-items/{do_number}/{product_code}', 'Management\ManagementController@updateItems')->name('update.items.panel');
        Route::put('/panel/update-qr-submitted/{order_num}', 'Purchase\PurchaseController@qrSubmit')->name('update.order.panel.received.date');
        Route::get('/panel/company-profile', 'Management\ManagementController@companyProfile')->name('management.company.profile');
        Route::get('/panel/company-profile-edit', 'Management\ManagementController@editProfile')->name('management.company.profile.edit');
        Route::patch('/panel/company-profile-update/{id}', 'Management\ManagementController@updateProfile')->name('management.company.profile.update');

        Route::post('/editcity', 'Management\ManagementController@editcity')
            ->name('management.company.panel.editcity');
        Route::post('/editbill', 'Management\ManagementController@editbill')
            ->name('management.company.panel.editbill');

        Route::post('/panel/company-profile-update/{id}', 'Management\ManagementController@updateProfile')->name('management.company.profile.update');

        Route::get('/panel/change-password', 'Management\ChangePasswordController@index');
        Route::post('/panel/change-password', 'Management\ChangePasswordController@store')->name('change.password');

        Route::get('/panel/orders/purchase-order-pdf/{order_num}', 'Management\ManagementController@viewPurchaseOrder');
        Route::get('/panel/orders/delivery-order-pdf/{delivery_order}', 'Management\ManagementController@viewDeliveryOrder');
        Route::get('/panel/value-tracking/index', 'Management\ManagementController@valueTracking')->name('management.panel.value-tracking');
        Route::get('/orders/all', 'Management\ManagementController@allOrders');
        Route::get('/orders/open', 'Management\ManagementController@openOrders');
        Route::get('/orders/in-progress', 'Management\ManagementController@inProgressOrders');
        Route::get('/orders/delivered', 'Management\ManagementController@deliveredOrders');
        Route::get('/orders/completed', 'Management\ManagementController@completedOrders');
        Route::get('/orders/cancelled', 'Management\ManagementController@cancelledOrders');
        Route::get('/panel/person-in-charge', 'Management\ManagementController@personInCharge');
        //Update items Tracking
        Route::put('/update-management.tracking/{id}', 'Management\ManagementController@updateTracking')
            ->name('update.management.items.tracking');

        // Temp statement layout
        Route::view('/statement', 'management.panel.statement');

        //Dashboard-Dealer (Group them in future)
        Route::get('/dealer/home', 'Management\ManagementController@homeDealer')->name('management.dealer.home');
        Route::get('/dealer/index', 'Management\ManagementController@indexDealer')->name('management.dealer.statement.home');
        Route::get('/dealer/profile', 'Management\ManagementController@dealerProfile')->name('shop.dashboard.dealer.profile');
        Route::get('/dealer/profile/edit', 'Management\ManagementController@editdealerProfile')->name('shop.dashboard.dealer.profile.edit');
        Route::post('/dealer/profile-update/{id}', 'Management\ManagementController@updateDealerProfile')->name('shop.dashboard.dealer.profile.update');
        Route::get('/password', 'Management\ManagementController@modifyPassword');
        Route::get('/dealer/statements/{month}/{month_num}/{year}', 'Management\ManagementController@statements')->name('dealer.statement');
        Route::get('/dealer/sales-summary', 'Management\ManagementController@salesSummary')->name('dealer.sales.summary');
    });
    // End Management

    // Agent
    Route::group(['prefix' => 'agent', 'middleware' => ['role:dealer', 'dealership:default']], function () {
        // Index
        Route::get('/', 'Agent\AgentController@index')
            ->name('agent.overview');

        Route::get('/yearly-income', 'Agent\AgentController@income')
            ->name('agent.yearly.income');

        //new agent dashboard 2021

        Route::get('/new_index', function () {
            return view('agent.new_index');
        });

        Route::get('/monthly-statement/{agent}-{id}.pdf', 'Agent\AgentController@monthlyStatement')
            ->name('agent.statement');

        Route::post('/cp58/{year}.pdf', 'Agent\AgentController@cp58Statement')
            ->name('agent.cp58');

        Route::post('/yearly-statement/{year}.pdf', 'Agent\AgentController@yearlyStatement')
            ->name('agent.yearly-statement');

        // Agent Comission
        Route::get('/monthly-income', 'Agent\AgentController@agentComission')
            ->name('agent.monthly.income');
        // Agent Comission
        Route::get('/monthly-income/{id}', 'Agent\AgentController@agentComissionBatch')
            ->name('agent.income.batch');

        // Membership Registration page
        Route::get('/membership-registration', 'Agent\AgentController@membershipRegistration')
            ->name('agent.membership-registration');

        // Anouncement page
        Route::get('/announcements', 'Agent\AgentController@announcements')
            ->name('agent.announcements');

        // Special task page
        Route::get('/task', 'Agent\AgentController@specialTask')
            ->name('agent.special.task');

        // Agent task update status
        Route::get('/task/update', 'Agent\AgentController@statusUpdate')->name('agent.special.task.update');

        // Agent filter state
        Route::POST('/state-filter-agent-city', 'Agent\AgentController@stateFilterAgentCity')
            ->name('state.filter.agent.city');

        Route::get('/customerVerification', 'Agent\AgentController@customerVerification')
            ->name('agent.customerVerification');

        Route::POST('/customerVerification/{id}', 'Agent\AgentController@customerVerificationAction')
            ->name('agent.customerVerification.action');

        Route::get('/agent-package-product-list', 'Agent\AgentController@agentProduct')
            ->name('agent.agent.package.product.list');

        Route::get('/agent-virtual-quantity-stock', 'Agent\AgentController@specialTask')
            ->name('agent.virtual.quantity.stock');

        //Bulk update
        Route::post('/update-bulk/', 'Agent\AgentController@createDealerPreBuyRedemption')
            ->name('agent.vqs.createDealerPreBuyRedemption');
    });
    // End Agent
    // Management
    Route::group(['prefix' => 'management', 'middleware' => ['role:management']], function () {

        Route::group(['prefix' => 'sales'], function () {
            // Index
            Route::get('/', 'Administrator\Sale\SaleController@mainIndex')
                ->name('management.sale');

            Route::get('/{day}/{month}/{year}', 'Administrator\Sale\SaleController@index')
                ->name('management.sale.detail');


            Route::get('/group/{group_id}', 'Administrator\Sale\SaleController@mainGroupIndex')
                ->name('management.groupsale');

            Route::get('/group/{group_id}/{day}/{month}/{year}', 'Administrator\Sale\SaleController@groupIndex')
                ->name('management.groupsale.detail');
        });
        // Route::get('administrator/ordertracking/logisticReport', 'Administrator\Ordertracking\LogisticReportController@index')
        //     ->name('administrator.ordertracking.logisticReport');
    });

    // Agent Manager
    Route::group(['prefix' => 'management', 'middleware' => ['role:agent_manager']], function () {
        Route::group(['prefix' => 'sales'], function () {

            Route::get('/groupsales', 'Administrator\Sale\SaleController@mainSingleGroupIndex')
                ->name('management.manager.groupsale');

            Route::get('/groupsales/{day}/{month}/{year}', 'Administrator\Sale\SaleController@singleGroupIndex')
                ->name('management.manager.groupsale.detail');
        });
    });

    // Administrator
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:administrator|formula_pic|management|redemption|admin_product']], function () {

        Route::get('/', 'Administrator\AdministratorController@index');
    });

    // different role for admin page
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:administrator|formula_pic|management|admin_product']], function () {

        Route::group(['prefix' => 'product'], function () {

            // Product index.
            Route::get('/', 'Administrator\Product\MProductController@index')
                ->name('administrator.product');
            // Return JSON response of all products
            // Route::get('/resource', 'WEB\Administrator\ProductJSONController@getProducts')
            //     ->name('administrator.products.json');

            // Create product.
            Route::post('/create', 'Administrator\Product\MProductController@create')
                ->name('administrator.product.create');

            // Edit
            Route::get('/editt/{productId}', 'Administrator\Product\MProductController@editCreate')
                ->name('administrator.product.editCreate');

            

            Route::group(['prefix' => 'restriction'], function () {

                Route::get('/', 'Administrator\Product\MProductController@productRestriction')
                    ->name('administrator.product.product-restriction');

                Route::post('/submit', 'Administrator\Product\MProductController@updateRestrict')
                    ->name('administrator.product.restriction-submit');

                Route::post('/remove', 'Administrator\Product\MProductController@removeRestrict')
                    ->name('administrator.product.restriction-remove');
                    
                Route::get('/get-agent-list', 'Administrator\Product\MProductController@getAgentList')
                ->name('administrator.get-agent-list');


            });


            // Update
            Route::put('/edit/{id}/update', 'Administrator\Product\MProductController@updateCreate')
                ->name('administrator.product.updateCreate');

            // // Publish Product
            Route::get('/product-publish/{productId}', 'Administrator\Product\MProductController@publishProduct')
                ->name('administrator.product.publish');

            // // Unpublish Product
            Route::get('/product-unpublish/{productId}', 'Administrator\Product\MProductController@unpublishProduct')
                ->name('administrator.product.unpublish');

            Route::post('/enabled-attributes', 'Administrator\Product\MProductController@enableAttributes')
                ->name('administrator.product.enable.attributes');

            Route::post('/attributes-add-new-price', 'Administrator\Product\MProductController@attributeAddNewPrice')
                ->name('administrator.product.attributes-add-new-price');

            Route::post('/attributes-active-price', 'Administrator\Product\MProductController@activePrice')
                ->name('administrator.product.attributes-active-price');

            Route::post('/update-previous-notes', 'Administrator\Product\MProductController@updatePreviousNotes')
                ->name('administrator.product.update-previous-notes');

            // Brand
            Route::group(['prefix' => 'brand'], function () {
                // Index
                Route::get('/', 'Administrator\Product\BrandController@index')
                    ->name('administrator.product.brand');

                // // Create
                // Route::post('/create', 'Administrator\Product\BrandController@create')
                //     ->name('administrator.product.brand.create');

                // Store
                Route::post('/store', 'Administrator\Product\BrandController@store')
                    ->name('administrator.product.brand.store');

                // Edit
                Route::get('/edit/{id}', 'Administrator\Product\BrandController@edit')
                    ->name('administrator.product.brand.edit');

                // Update
                Route::put('/update/{id}', 'Administrator\Product\BrandController@update')
                    ->name('administrator.product.brand.update');

                // Delete
                Route::delete('/delete/{id}', 'Administrator\Product\BrandController@destroy')
                    ->name('administrator.product.brand.delete');
            });

            // Brand
            Route::group(['prefix' => 'shippingCategory'], function () {

                // Index
                Route::get('/', 'Administrator\Product\AttributeShippingCategoryController@index')
                    ->name('administrator.product.shippingCategory');

                // Store
                Route::post('/store', 'Administrator\Product\AttributeShippingCategoryController@store')
                    ->name('administrator.product.shippingCategory.store');

                //Edit Product mark up
                Route::get('/edit/{id}', 'Administrator\Product\AttributeShippingCategoryController@editMarkup')
                    ->name('administrator.product.shippingCategory.edit');

                //Edit Product mark up
                Route::get('/edit-special/{id}', 'Administrator\Product\AttributeShippingCategoryController@editMarkupSpecial')
                    ->name('administrator.product.shippingCategory.edit.special');

                // Update Product mark up (Special)
                Route::get('/update/{id}', 'Administrator\Product\AttributeShippingCategoryController@updateMarkup')
                    ->name('administrator.product.shippingCategory.update');

                // Update Product mark up (Special)
                Route::get('/update-special/{id}', 'Administrator\Product\AttributeShippingCategoryController@updateMarkup')
                    ->name('administrator.product.shippingCategory.update-special');

                //Bulk update
                Route::post('/update-bulk/', 'Administrator\Product\AttributeShippingCategoryController@bulkUpdate')
                    ->name('bulkupdate.administrator.product.shippingCategory');

                Route::get('/decideShipping', 'Administrator\Product\AttributeShippingCategoryController@decideShippingCategory')
                    ->name('administrator.product.shippingCategory.decide-shipping');
            });
        });
    });
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:administrator|formula_pic|wh_supervisor|wh_warehouse|wh_cashier|admin_orderTracking']], function () {
        //Orders
        Route::get('/orders', 'Administrator\Ordertracking\DetailController@orders')
            ->name('administrator.ordertracking.orders');

        //Orders's Detail
        Route::get('/ordersDetail', 'Administrator\Ordertracking\DetailController@ordersDetail')->name('administrator.ordertracking.detail');

        Route::group(['prefix' => 'pick'], function () {
            // ALL Batch Index.
            Route::get('/', 'Administrator\Pick\PickBatchController@index')
                ->name('administrator.pickBatch.index');

            //regenerate combined pdf (sort)
            Route::get('/regenerate-combined-sort/{batchId}', 'Administrator\Pick\PickBatchController@regenerateCombineSort')
                ->name('administrator.pickBatch.regenerate-combined-sort');
        });
    });

    // different role for admin page
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:administrator|formula_pic|wh_picker|admin_orderTracking|management']], function () {

        //wh_picker
        Route::group(['prefix' => 'pick'], function () {

            // Start.
            Route::get('/start', 'Administrator\Pick\PickBatchController@start')
                ->name('administrator.pickBatch.start');

            Route::post('/start', 'Administrator\Pick\PickBatchController@start')
                ->name('administrator.pickBatch.start-redirect');

            // Start Work.
            Route::post('/start-work', 'Administrator\Pick\PickBatchController@startWork')
                ->name('administrator.pickBatch.start-work');

            // End.
            Route::get('/end', 'Administrator\Pick\PickBatchController@end')
                ->name('administrator.pickBatch.end');

            // End work.
            Route::post('/end-work', 'Administrator\Pick\PickBatchController@endWork')
                ->name('administrator.pickBatch.end-work');

            //Orders pick
            Route::get('/orders-pick', 'Administrator\Pick\PickBatchController@ordersPick')
                ->name('administrator.ordertracking.orders-pick');
        });
        Route::post('/update-bulk/', 'Administrator\Pick\PickBatchController@bulkUpdate')
            ->name('bulkupdate.ordertracking.picker');

        // Order Tracking
        Route::group(['prefix' => 'ordertracking'], function () {

            //Summary (panel)
            Route::get('/ordertrackingsummary', 'Administrator\Ordertracking\SummaryController@index')
                ->name('administrator.ordertracking.summary');

            //Summary (customer)
            Route::get('/ordertrackingsummary-customer', 'Administrator\Ordertracking\SummaryCustomerController@index')
                ->name('administrator.ordertracking.summary.customer');

            //old detail
            Route::get('/old-order-detail', 'Administrator\Ordertracking\DetailController@oldDetail')
                ->name('administrator.ordertracking.old-detail');

            // Recurring Booking System (RBS)
            Route::get('/rbs-prepaid', 'Administrator\Ordertracking\DetailController@rbsPrepaid')
                ->name('administrator.ordertracking.rbs-prepaid');

            // Recurring Booking System (RBS)
            Route::get('/rbs-postpaid', 'Administrator\Ordertracking\DetailController@rbsPostpaid')
                ->name('administrator.ordertracking.rbs-postpaid');

            Route::put('/update-poslaju/{order_num}', 'Administrator\Ordertracking\DetailController@updatePoslaju')
                ->name('update.reset.cancel.poslaju.consigment');

            Route::put('/update-order/{order_num}', 'Administrator\Ordertracking\DetailController@updateOrder')
                ->name('update.order.administrator.ordertracking.detail');

            Route::put('/update-items/{do_number}', 'Administrator\Ordertracking\DetailController@updateItems')
                ->name('update.ship.out.items');

            //Reset items
            Route::put('/update-reset-items/{do_number}', 'Administrator\Ordertracking\DetailController@updateresetItems')
                ->name('update.reset.ship.out.items');

            //Update Tracking
            Route::put('/update-tracking/{id}', 'Administrator\Ordertracking\DetailController@updateTracking')
                ->name('update.items.tracking');

            //Update Admin Remark
            Route::put('/update-adminremark/{id}', 'Administrator\Ordertracking\DetailController@updateAdminRemark')
                ->name('update.items.adminremark');

            //Update Panel Remark
            Route::put('/update-panelremark/{id}', 'Administrator\Ordertracking\DetailController@updatePanelRemark')
                ->name('update.items.panelremark');

            //Bulk update
            Route::post('/update-bulk/', 'Administrator\Ordertracking\DetailController@bulkUpdate')
                ->name('bulkupdate.ordertracking.detail');

            // exportCsv
            Route::get('/exportCsv', 'Administrator\Ordertracking\DetailController@exportCsv')
                ->name('administrator.ordertracking.exportCsv');
            // exportCsv Customer
            Route::get('/exportCsv/customer', 'Administrator\Ordertracking\SummaryCustomerController@exportCsvCustomer')
                ->name('administrator.ordertracking.exportCsv.customer');
            // exportCsv Panel
            Route::get('/exportCsv/panel', 'Administrator\Ordertracking\SummaryController@exportCsvPanel')
                ->name('administrator.ordertracking.exportCsv.panel');

            Route::get('/logisticReport', 'Administrator\Ordertracking\LogisticReportController@index')
                ->name('administrator.ordertracking.logisticReport');
        });
    });


    // different role for admin page
    Route::group(['prefix' => 'administrator'], function () {
        // Side Menu
        Route::group(['prefix' => 'sidemenus', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\sidemenu\SidemenusController@index')
                ->name('administrator.sidemenus.index');

            // show
            Route::get('/show', 'Administrator\sidemenu\SidemenusController@show')
                ->name('administrator.sidemenus.show');

            // Create.
            Route::get('/create', 'Administrator\sidemenu\SidemenusController@create')
                ->name('administrator.sidemenus.create');

            // Store.
            Route::post('/store', 'Administrator\sidemenu\SidemenusController@store')
                ->name('administrator.sidemenus.store');

            // edit.
            Route::get('/edit/{id}', 'Administrator\sidemenu\SidemenusController@edit')
                ->name('administrator.sidemenus.edit');

            // update.
            Route::post('/update/{id}', 'Administrator\sidemenu\SidemenusController@update')
                ->name('administrator.sidemenus.update');

            // destroy.
            Route::post('/destroy/{id}', 'Administrator\sidemenu\SidemenusController@destroy')
                ->name('administrator.sidemenus.destroy');
        });


        // agent admin control
        Route::group(['prefix' => 'agent', 'middleware' => ['role:administrator|formula_pic|management']], function () {
            // index
            Route::get('/', 'Agent\AgentController@admincontrol')
                ->name('administrator.agent.index');
            // update
            Route::post('/update/{id}', 'Agent\AgentController@update')
                ->name('administrator.agent.update');
        });

        // agent admin control
        Route::group(['prefix' => 'account'], function () {
            // index
            Route::get('sql', 'Administrator\Account\SqlController@index')
                ->name('administrator.account.sql.index');

            Route::post('invoice-export', 'Administrator\Account\SqlController@invoiceExport')
                ->name('administrator.account.sql.invoice');

            Route::post('payment-export', 'Administrator\Account\SqlController@paymentExport')
                ->name('administrator.account.sql.payment');
        });

        // Banner
        Route::group(['prefix' => 'testbanners', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\banner\TestbannersController@index')
                ->name('administrator.testbanners.index');

            // show
            Route::get('/show', 'Administrator\banner\TestbannersController@show')
                ->name('administrator.testbanners.show');

            // Create.
            Route::get('/create', 'Administrator\banner\TestbannersController@create')
                ->name('administrator.testbanners.create');

            // Store.
            Route::post('/store', 'Administrator\banner\TestbannersController@store')
                ->name('administrator.testbanners.store');

            // edit.
            Route::get('/edit/{id}', 'Administrator\banner\TestbannersController@edit')
                ->name('administrator.testbanners.edit');

            // update.
            Route::post('/update/{id}', 'Administrator\banner\TestbannersController@update')
                ->name('administrator.testbanners.update');

            // destroy.
            Route::post('/destroy/{id}', 'Administrator\banner\TestbannersController@destroy')
                ->name('administrator.testbanners.destroy');
        });

        // FAQ
        Route::group(['prefix' => 'faq', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\faq\FaqController@index')
                ->name('administrator.faq.index');

            // show
            // Route::get('/show', 'Administrator\faq\FaqController@show')
            // ->name('administrator.faq.show');

            // Create.
            Route::get('/create', 'Administrator\faq\FaqController@create')
                ->name('administrator.faq.create');

            // Store.
            Route::post('/store', 'Administrator\faq\FaqController@store')
                ->name('administrator.faq.store');

            // edit.
            Route::get('/edit/{id}', 'Administrator\faq\FaqController@edit')
                ->name('administrator.faq.edit');

            // update.
            Route::post('/update/{id}', 'Administrator\faq\FaqController@update')
                ->name('administrator.faq.update');

            // destroy.
            Route::post('/destroy/{id}', 'Administrator\faq\FaqController@destroy')
                ->name('administrator.faq.destroy');

            // Publish Faq
            Route::get('/faq-publish/{id}', 'Administrator\faq\FaqController@publishFaq');

            // Unpublish Faq
            Route::get('/faq-unpublish/{id}', 'Administrator\faq\FaqController@unpublishFaq');
        });

        //RBS
        Route::group(['prefix' => 'rbs', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            //Index
            Route::get('/', 'Administrator\rbs\RbsController@index')
                ->name('administrator.rbs.index');

            //Create
            Route::get('/create', 'Administrator\rbs\RbsController@create')
                ->name('administrator.rbs.create');

            Route::get('/create&edit/{type}/{id?}', 'Administrator\rbs\RbsController@createEdit')
                ->name('administrator.rbs.createEdit');

            //Create
            Route::get('/edit/{id}', 'Administrator\rbs\RbsController@edit')
                ->name('administrator.rbs.edit');

            Route::get('/send-email', 'Administrator\rbs\RbsController@sendEmail')
                ->name('administrator.rbs.send.email');

            // Store.
            Route::post('/store', 'Administrator\rbs\RbsController@store')
                ->name('administrator.rbs.store');

            // Disable
            Route::get('/disableRbs/{id}', 'Administrator\rbs\RbsController@disableRbs');

            // Enable
            Route::get('/enableRbs/{id}', 'Administrator\rbs\RbsController@enableRbs');
        });


        // Discount Code & rule
        Route::group(['prefix' => 'discount', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\Discount\DiscountController@index')
                ->name('administrator.discount.index');

            // create
            Route::get('/create', 'Administrator\Discount\DiscountController@createCode')
                ->name('administrator.discount.createCode');

            // store
            Route::post('/store', 'Administrator\Discount\DiscountController@store')
                ->name('administrator.discount.store');

            // generate Code
            Route::get('/generateCode', 'Administrator\Discount\DiscountController@generateCode')
                ->name('administrator.discount.generateCode');

            // get Discount Code
            Route::get('/getCode', 'Administrator\Discount\DiscountController@getCode')
                ->name('administrator.discount.getCode');

            // get Discount Code By Id
            Route::get('/getCodeById/{id}', 'Administrator\Discount\DiscountController@getCodeById')
                ->name('administrator.discount.getCodeById');

            // get Discount by code
            Route::get('/getCoupon/{coupon}', 'Administrator\Discount\DiscountController@getCoupon')
                ->name('administrator.discount.getCoupon');

            // update Code
            Route::post('/update/{id}', 'Administrator\Discount\DiscountController@updateCode')
                ->name('administrator.discount.updateCode');

            // delete Code
            Route::post('/delete/{id}', 'Administrator\Discount\DiscountController@deleteCode')
                ->name('administrator.discount.deleteCode');

            // rule
            Route::get('/rule', 'Administrator\Discount\DiscountController@rule')
                ->name('administrator.discount.rule');

            // usage
            Route::get('/usage', 'Administrator\Discount\DiscountController@usage')
                ->name('administrator.discount.usage');

            // summary
            /* Route::get('/summary', 'Administrator\Discount\DiscountController@summary')
                ->name('administrator.discount.summary'); */

            // selector
            Route::get('/product-selector', 'Administrator\Discount\DiscountController@selector')
                ->name('administrator.discount.selector');

            Route::post('/selector-post', 'Administrator\Discount\DiscountController@selectorPost')
                ->name('administrator.discount.selectorPost');

            Route::get('/usage-list', 'Administrator\Discount\DiscountController@usageList')
                ->name('administrator.discount.usagelist');

            // create rule
            Route::get('/create-rule', 'Administrator\Discount\DiscountController@createRule')
                ->name('administrator.discount.createRule');

            // store rule
            Route::post('/store-rule', 'Administrator\Discount\DiscountController@storeRule')
                ->name('administrator.discount.storeRule');

            // get Discount Rule
            Route::get('/getRule', 'Administrator\Discount\DiscountController@getRule')
                ->name('administrator.discount.getRule');

            // get Rule By Id
            Route::get('/getRuleById/{id}', 'Administrator\Discount\DiscountController@getRuleById')
                ->name('administrator.discount.getRuleById');

            // update Rule
            Route::post('/updateRule/{id}', 'Administrator\Discount\DiscountController@updateRule')
                ->name('administrator.discount.updateRule');

            // delete Rule
            Route::post('/deleteRule/{id}', 'Administrator\Discount\DiscountController@deleteRule')
                ->name('administrator.discount.deleteRule');

            // get-global-product
            Route::get('/get-global-product', 'Administrator\Discount\DiscountController@getGlobalProduct')
                ->name('administrator.discount.getGlobalProduct');

            // get-product-attribute
            Route::get('/get-product-attribute', 'Administrator\Discount\DiscountController@getProductAttribute')
                ->name('administrator.discount.getProductAttribute');

            // get-categories
            Route::get('/get-category', 'Administrator\Discount\DiscountController@getCategory')
                ->name('administrator.discount.getCategory');
        });

        // TAX
        Route::group(['prefix' => 'tax', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\Tax\TaxController@index')
                ->name('administrator.tax.index');

            // getTax All
            Route::get('/getTax', 'Administrator\Tax\TaxController@getTax')
                ->name('administrator.tax.getTax');

            // getTax By Id
            Route::get('/getTaxById/{id}', 'Administrator\Tax\TaxController@getTaxById')
                ->name('administrator.tax.getTaxById');

            // Create.
            Route::get('/create', 'Administrator\Tax\TaxController@create')
                ->name('administrator.tax.create');

            // Store.
            Route::post('/store', 'Administrator\Tax\TaxController@store')
                ->name('administrator.tax.store');

            // Update.
            Route::post('/update/{id}', 'Administrator\Tax\TaxController@update')
                ->name('administrator.tax.update');

            // Delete.
            Route::post('/delete/{id}', 'Administrator\Tax\TaxController@delete')
                ->name('administrator.tax.delete');
        });

        // import-export data
        Route::group(['prefix' => 'import', 'middleware' => ['role:administrator|formula_pic|management']], function () {
            // index
            Route::get('/', 'Administrator\Import\ImportController@index')->name('administrator.import.index');
            // upload
            Route::get('/upload-file', 'Administrator\Import\ImportController@createform')->name('Upload');
            Route::post('/upload-file', 'Administrator\Import\ImportController@fileUpload')->name('fileUpload');

            Route::get('/weight', 'Administrator\Import\ImportController@weight')->name('administrator.import.weight');
            Route::post('/upload-weight', 'Administrator\Import\ImportController@uploadWeight')->name('administrator.import.upload-weight');

            Route::get('/stock', 'Administrator\Import\ImportController@whStock')->name('administrator.import.stock');
            Route::post('/upload-stock', 'Administrator\Import\ImportController@uploadWhStock')->name('administrator.import.upload-stock');

            Route::get('/manufacture', 'Administrator\Import\ImportController@manufacture')->name('administrator.import.manufacture');
            Route::post('/upload-manufacture', 'Administrator\Import\ImportController@uploadManufacture')->name('administrator.import.upload-manufacture');

            Route::get('/price', 'Administrator\Import\ImportController@price')->name('administrator.import.price');
            Route::post('/upload-price', 'Administrator\Import\ImportController@uploadPrice')->name('administrator.import.upload-price');
        });

        // Archimedes administrator
        Route::group(['prefix' => 'vqs', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/all-agent', 'Administrator\Archimedes\ArchimedesController@index')
                ->name('administrator.vqs.index');

            // Admin approved
            Route::get('/approval/{status?}', 'Administrator\Archimedes\ArchimedesController@pendingApproval')
                ->name('administrator.vqs.pending.approval');

            // Admin approved
            Route::get('/approved-vqs/{id}', 'Administrator\Archimedes\ArchimedesController@submitApproval')
                ->name('administrator.vqs.submit.approval');


            // details
            Route::get('/details/{id}', 'Administrator\Archimedes\ArchimedesController@detailsArchimedes')
                ->name('administrator.vqs.details');

            Route::post('/topup', 'Administrator\Archimedes\ArchimedesController@topupACD')
                ->name('administrator.vqs.topup');
        });

        // Sale
        Route::group(['middleware' => ['role:administrator|formula_pic|management|account_manager']], function () {
            Route::get('/regenerate-invoice/{purchaseNumber}', 'Purchase\PaymentGatewayController@regenerateInvoice');
            Route::get('/regenerate-invoices/', 'Purchase\PaymentGatewayController@regenerateInvoices');
            Route::resource('freegift', 'Administrator\FreeGift\FreeGiftController');
        });
        // User
        Route::group(['prefix' => 'users', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            Route::get('/', 'Administrator\User\UserController@index')
                ->name('administrator.user');

            Route::get('/show/{id}', 'Administrator\User\UserController@show')
                ->name('administrator.users.show');

            Route::get('/edit/{id}', 'Administrator\User\UserController@edit')
                ->name('administrator.users.edit');

            Route::post('/update/{id}/{form_type}', 'Administrator\User\UserController@update')
                ->name('administrator.users.update');
            

            Route::get('/membership', 'Administrator\User\UserController@membership')
                ->name('administrator.user.membership');

            Route::post('/upgradeMembership', 'Administrator\User\UserController@upgradeMembership')
                ->name('administrator.user.upgradeMembership');

            Route::get('/points', 'Administrator\User\UserController@points')
                ->name('administrator.user.points');

            Route::post('/updateMembership', 'Administrator\User\UserController@updateMembership')
                ->name('administrator.user.updateMembership');

            Route::post('/points/update-minimum', 'Administrator\User\UserController@updatePoints')
                ->name('administrator.user.updateMinimum');

            Route::post('/update-user', 'Administrator\User\UserController@updateUser')
                ->name('administrator.update.user');

            // Panel.
            Route::group(['prefix' => 'panel'], function () {
                // Index.
                Route::get('/', 'Administrator\User\PanelController@index')
                    ->name('management.user.panel');

                // Create.
                Route::get('/create', 'Administrator\User\PanelController@create')
                    ->name('management.user.panel.create');

                // Store.
                Route::post('/store', 'Administrator\User\PanelController@store')
                    ->name('management.user.panel.store');
            });
        });

        // Payment
        Route::group(['prefix' => 'payments', 'middleware' => ['role:administrator|formula_pic|management|account_manager']], function () {
            // Index
            Route::get('/', 'Administrator\Payment\PaymentController@index')
                ->name('administrator.payment');

            // Update Status
            Route::put('/{id}/update', 'Administrator\Payment\PaymentController@update')
                ->name('administrator.payment.update');

            Route::get('/request-gateway-response/{id}', 'Purchase\FpxGatewayController@secondRequest')->name('payment.request');


            // Partial Refund
            Route::get('/order-list', 'Administrator\Payment\PaymentController@orderList')
                ->name('administrator.payment.order.list');

            Route::get('/summary', 'Administrator\Payment\PaymentController@summary')
                ->name('administrator.payment.summary');

                Route::get('/view-pdf/{purchase}', 'Administrator\Payment\PaymentController@viewPDF')
                ->name('administrator.view-pdf');
        });

        // Products
        Route::group(['prefix' => 'products', 'middleware' => ['role:administrator|formula_pic|management']], function () {
            // V1
            Route::group(['prefix' => 'v1'], function () {
                // // Index
                // Route::get('/', 'Administrator\v1\Product\ProductController@index')
                //     ->name('administrator.v1.products');

                // // Edit
                // Route::get('/edit/{id}', 'Administrator\v1\Product\ProductController@edit')
                //     ->name('administrator.v1.products.edit');

                // // Update
                // Route::put('/edit/{id}/update', 'Administrator\v1\Product\ProductController@update')
                //     ->name('administrator.v1.products.update');

                // Image Get (Dropzone)
                Route::get('/image-get/{id}', 'Administrator\v1\Product\ProductController@getImage')
                    ->name('administrator.v1.products.get-image');

                // Image upload (Dropzone).
                Route::post('/image-upload/{id}', 'Administrator\v1\Product\ProductController@storeImage')
                    ->name('administrator.v1.products.store-image');

                // Image delete (Dropzone).
                Route::post('/image-delete/{id}', 'Administrator\v1\Product\ProductController@deleteImage')
                    ->name('administrator.v1.products.delete-image');

                //  Product by panels.
                Route::group(['prefix' => 'panels'], function () {
                    // Index
                    Route::get('/', 'Administrator\v1\Product\ProductByPanelController@index')
                        ->name('administrator.v1.products.panels');

                    // Create
                    Route::post('/create', 'Administrator\v1\Product\ProductByPanelController@create')
                        ->name('administrator.v1.products.panels.create');

                    // Store
                    Route::post('/store', 'Administrator\v1\Product\ProductByPanelController@store')
                        ->name('administrator.v1.products.panels.store');

                    // Edit
                    Route::get('/edit/{id}', 'Administrator\v1\Product\ProductByPanelController@edit')
                        ->name('administrator.v1.products.panels.edit');

                    // Update
                    Route::put('/edit/{id}/update', 'Administrator\v1\Product\ProductByPanelController@update')
                        ->name('administrator.v1.products.panels.update');

                    // Panel Product Price Index
                    Route::get('/price', 'Administrator\v1\Product\PanelProductPriceController@index')
                        ->name('administrator.v1.products.panels.price');

                    Route::group(['middleware' => ['role:administrator|formula_pic|management']], function () {

                        //Edit  Product price
                        Route::get('/price-edit/{id}', 'Administrator\v1\Product\PanelProductPriceController@edit')
                            ->name('administrator.v1.products.panels.price.edit');

                        // Update Product price
                        Route::post('/price-update/{id}', 'Administrator\v1\Product\PanelProductPriceController@update')
                            ->name('administrator.v1.products.panels.price.update');

                        //Edit Product mark up
                        Route::get('/price-edit-markup/{id}', 'Administrator\v1\Product\PanelProductPriceController@editMarkup')
                            ->name('administrator.v1.products.panels.price.edit-markup');

                        // Update Product mark up
                        Route::get('/price-update-markup/{id}', 'Administrator\v1\Product\PanelProductPriceController@updateMarkup')
                            ->name('administrator.v1.products.panels.price.update-markup');
                    });

                    /* Mark Up Category*/
                    // index
                    Route::get('/markup-category', 'Administrator\v1\Product\PanelProductMarkupCategoryController@index')
                        ->name('administrator.products.v1.panel.markup-category.index');

                    // Create.
                    Route::get('/markup-category-create', 'Administrator\v1\Product\PanelProductMarkupCategoryController@create')
                        ->name('administrator.products.v1.panel.markup-category.create');

                    // Store.
                    Route::post('/markup-category-store', 'Administrator\v1\Product\PanelProductMarkupCategoryController@store')
                        ->name('administrator.products.v1.panel.markup-category.store');

                    // edit.
                    Route::get('/markup-category-edit/{id}', 'Administrator\v1\Product\PanelProductMarkupCategoryController@edit')
                        ->name('administrator.products.v1.panel.markup-category.edit');

                    // update.
                    Route::post('/markup-category-update/{id}', 'Administrator\v1\Product\PanelProductMarkupCategoryController@update')
                        ->name('administrator.products.v1.panel.markup-category.update');

                    // destroy.
                    // Route::post('/markup-category-destroy/{id}', 'Administrator\v1\Product\PanelProductMarkupCategoryController@destroy')
                    //     ->name('administrator.products.v1.panel.markup-category.destroy');
                    /* End Mark Up Category*/
                });

                // Reports.
                Route::get('/reports', 'Administrator\v1\Product\ProductController@reports')
                    ->name('administrator.products.reports');
            });

            // Product index.
            // Route::get('/', 'Administrator\Product\ProductController@index')
            //     ->name('administrator.products');
            // Return JSON response of all products
            // Route::get('/resource', 'WEB\Administrator\ProductJSONController@getProducts')
            //     ->name('administrator.products.json');

            // // Create product.
            // Route::get('/create', 'Administrator\Product\ProductController@create')
            //     ->name('administrator.products.create');

            // Image upload.
            Route::post('/image-upload/{productId}', 'Administrator\Product\ProductController@storeImage')
                ->name('administrator.products.store-image');

            // Image delete.
            Route::post('/image-delete/{productId}', 'Administrator\Product\ProductController@deleteImage')
                ->name('administrator.products.delete-image');

            // Store product.
            Route::post('/store', 'Administrator\Product\ProductController@store')
                ->name('administrator.products.store');

            // Edit product.
            Route::get('/edit/{productId}', 'Administrator\Product\ProductController@edit')
                ->name('administrator.products.edit');

            // Update product.
            Route::put('/update/{productId}', 'Administrator\Product\ProductController@update')
                ->name('administrator.products.update');

            // Publish Product
            Route::get('/product-publish/{productId}', 'Administrator\Product\ProductController@publishProduct');

            // Unpublish Product
            Route::get('/product-unpublish/{productId}', 'Administrator\Product\ProductController@unpublishProduct');

            // Panels
            Route::group(['prefix' => 'panels'], function () {
                // // Index
                // Route::get('/', 'Administrator\Product\PanelProductController@index')
                //     ->name('administrator.products.panels');

                // // Create
                // Route::post('/create', 'Administrator\Product\PanelProductController@create')
                //     ->name('administrator.products.panels.create');

                // // Store
                // Route::post('/store', 'Administrator\Product\PanelProductController@store')
                //     ->name('administrator.products.panels.store');

                // // Edit
                // Route::get('/edit/{productId}', 'Administrator\Product\PanelProductController@edit')
                //     ->name('administrator.products.panels.edit');

                // // Update
                // Route::put('/update/{productId}', 'Administrator\Product\PanelProductController@update')
                //     ->name('administrator.products.panels.update');

                // // Delete
                // Route::delete('/delete/{productId}', 'Administrator\Product\PanelProductController@destroy')
                //     ->name('administrator.products.panels.delete');
            });
        });

        // Report
        Route::group(['prefix' => 'report', 'middleware' => ['role:administrator|formula_pic|management']], function () {
            // Index
            Route::get('/', 'Administrator\Report\ReportController@index')
                ->name('administrator.report.panelreport');

            Route::get('/adminexport', 'API\Export\ExportController@adminexport')->name('adminexport');
        });

        Route::group(['prefix' => 'PickBin', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\PickBin\PickBinController@index')
                ->name('administrator.pickBin.index');

            // Create.
            Route::get('/create', 'Administrator\PickBin\PickBinController@create')
                ->name('administrator.pickBin.create');

            // Store.
            Route::post('/store', 'Administrator\PickBin\PickBinController@store')
                ->name('administrator.pickBin.store');

            Route::get('/qr-code/{bin_number}', 'Administrator\PickBin\PickBinController@qrScanned')
                ->name('administrator.pickBin.qr-code')
                ->middleware('signed');
        });

        // Shipping and Installations administrator
        Route::group(['prefix' => 'shipping-installations', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // Cities
            Route::group(['prefix' => 'cities'], function () {

                //index
                Route::get('/', 'Administrator\Cities\GlobalCitiesController@index')
                    ->name('administrator.cities.index');

                //create
                Route::get('/create', 'Administrator\Cities\GlobalCitiesController@create')
                    ->name('administrator.cities.create');

                //store
                Route::post('/store', 'Administrator\Cities\GlobalCitiesController@store')
                    ->name('administrator.cities.store');

                //edit
                Route::get('/edit/{id}', 'Administrator\Cities\GlobalCitiesController@edit')
                    ->name('administrator.cities.edit');

                //update
                Route::post('/update/{id}', 'Administrator\Cities\GlobalCitiesController@update')
                    ->name('administrator.cities.update');
            });

            // zones
            Route::group(['prefix' => 'zones'], function () {

                //index
                Route::get('/', 'Administrator\Zones\ZonesController@index')
                    ->name('administrator.zones.index');

                //create
                Route::get('/create', 'Administrator\Zones\ZonesController@create')
                    ->name('administrator.zones.create');

                //store
                Route::post('/store', 'Administrator\Zones\ZonesController@store')
                    ->name('administrator.zones.store');

                //edit
                Route::get('/edit/{id}', 'Administrator\Zones\ZonesController@edit')
                    ->name('administrator.zones.edit');

                //zone location edit
                // Route::get('/edit/{id}', 'Administrator\Zones\ZonesController@editZoneLocation')
                // ->name('administrator.zones.edit_zone_location');

                //update
                Route::post('/update/{id}', 'Administrator\Zones\ZonesController@update')
                    ->name('administrator.zones.update');
            });

            // zones location
            Route::group(['prefix' => 'zone_location', 'middleware' => ['role:administrator|formula_pic|management']], function () {

                //index
                Route::get('/', 'Administrator\ZoneLocations\ZonesLocationController@index')
                    ->name('administrator.zone_location.index');

                //edit
                Route::get('/edit/{id}', 'Administrator\ZoneLocations\ZonesLocationController@edit')
                    ->name('administrator.zone_location.edit');

                //update
                Route::post('/update/{id}', 'Administrator\ZoneLocations\ZonesLocationController@update')
                    ->name('administrator.zone_location.update');
            });

            // Rate Table
            Route::get('/rate-table', 'Administrator\ShippingInstallations\ShippingInstallationsController@rateTable')
                ->name('administrator.shipping-installations.rate-table');

            Route::group(['prefix' => 'ship_category'], function () {

                // index
                Route::get('/', 'Administrator\ShippingInstallations\ShipCategoryController@index')
                    ->name('administrator.shipping-installations.ship_category.index');

                // Create.
                Route::get('/create', 'Administrator\ShippingInstallations\ShipCategoryController@create')
                    ->name('administrator.shipping-installations.ship_category.create');

                // Store.
                Route::post('/store', 'Administrator\ShippingInstallations\ShipCategoryController@store')
                    ->name('administrator.shipping-installations.ship_category.store');

                // edit.
                Route::get('/edit/{id}', 'Administrator\ShippingInstallations\ShipCategoryController@edit')
                    ->name('administrator.shipping-installations.ship_category.edit');

                // update.
                Route::post('/update/{id}', 'Administrator\ShippingInstallations\ShipCategoryController@update')
                    ->name('administrator.shipping-installations.ship_category.update');

                // destroy.
                // Route::post('/destroy/{id}', 'Administrator\ShippingInstallations\ShipCategoryController@destroy')
                //     ->name('administrator.shipping-installations.ship_category.destroy');
            });
        });

        Route::group(['prefix' => 'countries', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\Countries\GlobalCountriesController@index')
                ->name('administrator.countries.index');

            // Create.
            Route::get('/create', 'Administrator\Countries\GlobalCountriesController@create')
                ->name('administrator.countries.create');

            // Store.
            Route::post('/store', 'Administrator\Countries\GlobalCountriesController@store')
                ->name('administrator.countries.store');

            // edit.
            Route::get('/edit/{id}', 'Administrator\Countries\GlobalCountriesController@edit')
                ->name('administrator.countries.edit');

            // update.
            Route::post('/update/{id}', 'Administrator\Countries\GlobalCountriesController@update')
                ->name('administrator.countries.update');

            // destroy.
            // Route::post('/destroy/{id}', 'Administrator\Countries\GlobalCountriesController@destroy')
            //     ->name('administrator.countries.destroy');
        });

        Route::group(['prefix' => 'panels', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\Panels\PanelsController@index')
                ->name('administrator.panels.index');

            // Create.
            Route::get('/create', 'Administrator\Panels\PanelsController@create')
                ->name('administrator.panels.create');

            // Store.
            Route::post('/store', 'Administrator\Panels\PanelsController@store')
                ->name('administrator.panels.store');

            // // edit.
            // Route::get('/edit/{id}', 'Administrator\Panels\PanelsController@edit')
            //     ->name('administrator.panels.edit');

            // // update.
            // Route::post('/update/{id}', 'Administrator\Panels\PanelsController@update')
            //     ->name('administrator.panels.update');

            // destroy.
            // Route::post('/destroy/{id}', 'Administrator\Countries\GlobalCountriesController@destroy')
            //     ->name('administrator.countries.destroy');
        });

        Route::group(['prefix' => 'outlet', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\Outlet\OutletController@index')
                ->name('administrator.outlet.index');

            // Create.
            Route::get('/create', 'Administrator\Outlet\OutletController@create')
                ->name('administrator.outlet.create');

            // Store.
            Route::post('/store', 'Administrator\Outlet\OutletController@store')
                ->name('administrator.outlet.store');

            // edit.
            Route::get('/edit/{id}', 'Administrator\Outlet\OutletController@edit')
                ->name('administrator.outlet.edit');

            // update.
            Route::post('/update/{id}', 'Administrator\Outlet\OutletController@update')
                ->name('administrator.outlet.update');

            // destroy.
            Route::post('/destroy/{id}', 'Administrator\Outlet\OutletController@destroy')
                ->name('administrator.outlet.destroy');
        });


        Route::group(['prefix' => 'courier', 'middleware' => ['role:administrator|formula_pic|management']], function () {

            // index
            Route::get('/', 'Administrator\Courier\CourierController@index')
                ->name('administrator.courier.index');

            // Create.
            Route::get('/create', 'Administrator\Courier\CourierController@create')
                ->name('administrator.courier.create');

            // Store.
            Route::post('/store', 'Administrator\Courier\CourierController@store')
                ->name('administrator.courier.store');

            // edit.
            Route::get('/edit/{id}', 'Administrator\Courier\CourierController@edit')
                ->name('administrator.courier.edit');

            // update.
            Route::post('/update/{id}', 'Administrator\Courier\CourierController@update')
                ->name('administrator.courier.update');

            // destroy.
            Route::post('/destroy/{id}', 'Administrator\Courier\CourierController@destroy')
                ->name('administrator.courier.destroy');

            Route::get('/create_shipment/{itemId}', 'Administrator\Courier\CourierController@createShipment')
                ->name('administrator.courier.create.shipment');

            Route::get('/poslaju_checkout/{itemId}', 'Administrator\Courier\CourierController@poslajuCheckout')
                ->name('administrator.courier.poslaju.checkout');

            Route::get('/poslaju_ship/{itemId}', 'Administrator\Courier\CourierController@poslajuShip')
                ->name('administrator.courier.poslaju.ship');
        });

        // Category
        Route::group(['prefix' => 'categories', 'middleware' => ['role:administrator|formula_pic|management']], function () {
            // Index
            Route::get('/', 'Administrator\Category\CategoryController@index')
                ->name('administrator.categories');

            // Create
            Route::get('/create', 'Administrator\Category\CategoryController@create')
                ->name('administrator.categories.create');

            // Store
            Route::post('/store', 'Administrator\Category\CategoryController@store')
                ->name('administrator.categories.store');

            // Edit
            Route::get('/edit/{id}', 'Administrator\Category\CategoryController@edit')
                ->name('administrator.categories.edit');

            // Update
            Route::put('/update/{id}', 'Administrator\Category\CategoryController@update')
                ->name('administrator.categories.update');

            // Delete
            Route::delete('/delete/{id}', 'Administrator\Category\CategoryController@destroy')
                ->name('administrator.categories.delete');

            Route::post('/status-update', 'Administrator\Category\CategoryController@updateCategory')
                ->name('administrator.categories.statusUpdate');
        });

        // Setting
        Route::group(['prefix' => 'setting', 'middleware' => ['role:administrator|formula_pic|management']], function () {
            // Index
            Route::get('/', 'Administrator\GlobalSettings\GlobalSettingsController@index')
                ->name('administrator.settings');

            // // Create
            // Route::get('/create', 'Administrator\Category\CategoryController@create')
            //     ->name('administrator.categories.create');

            // Store
            Route::post('/store', 'Administrator\GlobalSettings\GlobalSettingsController@store')
                ->name('administrator.settings.store');

            // // Edit
            // Route::get('/edit/{id}', 'Administrator\Category\CategoryController@edit')
            //     ->name('administrator.categories.edit');

            // // Update
            // Route::put('/update/{id}', 'Administrator\Category\CategoryController@update')
            //     ->name('administrator.categories.update');

            // // Delete
            // Route::delete('/delete/{id}', 'Administrator\Category\CategoryController@destroy')
            //     ->name('administrator.categories.delete');
        });

        // End Administrator
    });

    //wh_warehouse
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:wh_warehouse|administrator|formula_pic|management']], function () {
        Route::group(['prefix' => 'warehouse'], function () {
            Route::group(['prefix' => 'inventory'], function () {
                // Index
                Route::get('/', 'Administrator\Warehouse\InventoryController@index')
                    ->name('administrator.warehouse.inventory.index');

                Route::get('/overall', 'Administrator\Warehouse\InventoryController@overall')
                    ->name('administrator.warehouse.inventory.overall');

                Route::get('/overall-management', 'Administrator\Warehouse\InventoryController@overallManagement')
                    ->name('administrator.warehouse.inventory.overall-management');

                Route::get('/overall-management-v2', 'Administrator\Warehouse\InventoryController@overallManagementv2')
                    ->name('administrator.warehouse.inventory.overall-management-v2');

                Route::get('/export/download', 'Administrator\Warehouse\InventoryController@download')
                    ->name('warehouse-download');

                Route::get('/details/{product_code}', 'Administrator\Warehouse\InventoryController@details')
                    ->name('administrator.warehouse.inventory.details');

                Route::post('/details/{product_code}/update', 'Administrator\Warehouse\InventoryController@update')
                    ->name('administrator.warehouse.inventory.update');

                // product new batch
                Route::get('/new-batch', 'Administrator\v1\Product\WarehouseController@newBatch')
                    ->name('administrator.v1.products.warehouse.newBatch');

                Route::post('/add-batch', 'Administrator\v1\Product\WarehouseController@addBatch')
                    ->name('administrator.v1.products.warehouse.addBatch');

                Route::post('/print-label', 'Administrator\v1\Product\WarehouseController@printLabel')
                    ->name('administrator.v1.products.warehouse.printLabel');

                Route::get('/get-batch', 'Administrator\v1\Product\WarehouseController@getBatch')
                    ->name('administrator.v1.products.warehouse.getBatch');

                Route::get('/qr-batch/{id}', 'Administrator\v1\Product\WarehouseController@qrBatch')
                    ->name('administrator.v1.products.warehouse.qrBatch');

                Route::get('/qr-batch-v2/{id}', 'Administrator\v1\Product\WarehouseController@qrBatch_v2')
                    ->name('administrator.v1.products.warehouse.qrBatch_v2');

                Route::get('/qr-batch-v3/{id}', 'Administrator\v1\Product\WarehouseController@qrBatch_v3')
                    ->name('administrator.v1.products.warehouse.qrBatch_v3');

                Route::get('/qr-batch-v4/{id}', 'Administrator\v1\Product\WarehouseController@qrBatch_v4')
                    ->name('administrator.v1.products.warehouse.qrBatch_v4');

                // product batch
                Route::get('/batch-list', 'Administrator\v1\Product\WarehouseController@index')
                    ->name('administrator.v1.products.warehouse.index');

                Route::get('/batch-label', 'Administrator\v1\Product\WarehouseController@label')
                    ->name('administrator.v1.products.warehouse.label');

                Route::get('/reprint-qr-item', 'Administrator\v1\Product\WarehouseController@reprintQrItem')
                    ->name('administrator.v1.products.warehouse.reprintQrItem');

                Route::post('/reprint-qr-item', 'Administrator\v1\Product\WarehouseController@reprintQrItem')
                    ->name('administrator.v1.products.warehouse.reprintQrItem');

                //batch item list
                Route::get('/batch-item-list', 'Administrator\v1\Product\WarehouseController@batchItemList')
                    ->name('administrator.v1.products.warehouse.batch-item-list');

                Route::get('/batch-expiry', 'Administrator\v1\Product\WarehouseController@batchExpiry')
                    ->name('administrator.v1.products.warehouse.batch-expiry');

                Route::get('/get-batch-info/{batch_id}', 'Administrator\v1\Product\WarehouseController@getBatchInfo')
                    ->name('get-batch-info');

                Route::post('/edit-notes/{batchID}', 'Administrator\v1\Product\WarehouseController@editNotes')
                    ->name('administrator.v1.products.warehouse.edit-notes');
            });
        });
    });

    //wh_sorter
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:wh_sorter|administrator']], function () {
        Route::group(['prefix' => 'pick'], function () {
            // Sort.
            Route::get('/sortStart', 'Administrator\Pick\PickBatchController@sortStart')
                ->name('administrator.pickBatch.sortStart');

            Route::post('/sortStart', 'Administrator\Pick\PickBatchController@sortStart')
                ->name('administrator.pickBatch.sortStart-redirect');

            // Sort Start Work.
            Route::post('/sortStart-work', 'Administrator\Pick\PickBatchController@sortStartWork')
                ->name('administrator.pickBatch.sortStart-work');

            // Sort End.
            Route::get('/sortEnd', 'Administrator\Pick\PickBatchController@sortEnd')
                ->name('administrator.pickBatch.sortEnd');

            // Sort End work.
            Route::post('/sortEnd-work', 'Administrator\Pick\PickBatchController@sortEndWork')
                ->name('administrator.pickBatch.sortEnd-work');

            // Sort End process.
            Route::get('/sortEnd-toScanDo/{again}/{batch_id}', 'Administrator\Pick\PickBatchController@sortEndToScanDo')
                ->name('administrator.pickBatch.sortEnd-toScanDo');

            // Sort End process. Done redirect
            Route::post('/sortEnd-toScanDo/{again}/{batch_id}', 'Administrator\Pick\PickBatchController@sortEndToScanDo')
                ->name('administrator.pickBatch.sortEnd-toScanDo-redirect');

            // Sort End process. Show picking Order
            Route::get('/sortEnd-showPickingOrder/{pickingOrderId}', 'Administrator\Pick\PickBatchController@sortEndshowPickingOrder')
                ->name('administrator.pickBatch.sortEnd-showPickingOrder');
        });
    });

    //wh_packer
    Route::group(['prefix' => 'administrator', 'middleware' => ['role:wh_packer|administrator']], function () {
        Route::group(['prefix' => 'pick'], function () {
            // Pack Start.
            Route::get('/pack-start', 'Administrator\Pick\PickBatchController@packStart')
                ->name('administrator.pickBatch.pack-start');

            // Pack Start Work.
            Route::post('/pack-start-work', 'Administrator\Pick\PickBatchController@packStartWork')
                ->name('administrator.pickBatch.pack-start-work');

            // Pack End.
            Route::get('/packEnd', 'Administrator\Pick\PickBatchController@packEnd')
                ->name('administrator.pickBatch.packEnd');

            // Pack End work.
            Route::post('/packEnd-work', 'Administrator\Pick\PickBatchController@packEndWork')
                ->name('administrator.pickBatch.packEnd-work');
        });
    });


    Route::group(['prefix' => 'administrator', 'middleware' => ['role:voucher_summary']], function () {
        // summary
        Route::get('/summary', 'Administrator\Discount\DiscountController@summary')
            ->name('administrator.discount.summary');
    });

    Route::group(['prefix' => 'administrator', 'middleware' => ['role:redemption']], function () {
        // Edit
        Route::get('/redemption', 'Administrator\Redemption\RedemptionController@index')
            ->name('administrator.redemption');

        Route::get('/redemption/{do}', 'Administrator\Redemption\RedemptionController@logRedemption')
            ->name('administrator.redemption.do');
    });


    // Delhub Digital
    Route::get('/delhub-digital', 'Shop\ShopController@delhubdigital')->name('delhub.digital');



    // Shop
    Route::group(['prefix' => 'shop'], function () {
        //for bank test
        Route::get('/product_pagebank', 'Shop\ShopController@indexthemebank')->name('shop.product_pagebank');

        //payment failed
        Route::get('/payment_fail', function () {
            return view('shop.payment_fail');
        });

        //payment seccuss
        Route::get('/payment_thankyou', function () {
            return view('shop.payment_thankyou');
        });

        //coming soon
        Route::get('/coming_soon', 'Shop\ShopController@comingsoon')->name('shop.product_comingsoon');

        //cart soon
        Route::get('/cart_page', 'Shop\ShopController@cartpage')->name('shop.cart_page');

        //payment page
        Route::get('/payment_page', 'Shop\ShopController@paymentpage')->name('shop.payment_page');

        //new product page
        Route::get('/product_page', 'Shop\ShopController@indextheme')->name('shop.product_pageitem');

        //new product multineed page
        Route::get('/product_multineed', 'Shop\ShopController@productmultineed')->name('shop.product_multineed');
        //new product multineed page refferent
        Route::get('/article_page', function () {
            return view('shop.article_page');
        });

        // Home/Index page for shop.
        Route::get('/', 'Shop\ShopController@index')->name('shop.product_page');
        Route::get('/', 'Shop\ShopController@index')->name('shop.index');

        //return form
        Route::get('/return-form', 'Shop\ShopController@returnForm')->name('shop.return.form');

        // Route::get('/set-country', 'Shop\ShopController@countryUpdate')->name('shop.update-country');

        // Toggle item to perfect list
        Route::post('/perfect-list/toggle', 'Shop\WishListController@ajaxToggle')->name('shop.toggle-perfect-list');

        // Add item to perfect list
        Route::post('/perfect-list/{product_id}', 'Shop\WishListController@store')->name('shop.add-perfect-list');

        //Get total quantity of perfect list
        Route::get('/get-quantity-perfect-list/{id}', 'Shop\WishListController@getQuantity')->name('shop.perfect-list.quantity');

        //Remove perfect list
        Route::delete('/remove/{product_id}', 'Shop\WishListController@destroy')->name('shop.perfect-list.destroy');

        // About Us Page
        Route::get('/about-us', 'Shop\ShopController@aboutUs')->name('shop.about.us');

        //Privacy Policy Page
        Route::get('/privacy-policy', 'Shop\ShopController@privacyPolicy')->name('shop.privacy.policy');

        //Return and Refund Policy Page
        Route::get('/return-and-refund-policy', 'Shop\ShopController@returnRefundPolicy')->name('shop.return.and.refund.policy');

        //Shipping and Delivery Policy Page
        Route::get('/shipping-and-delivery-policy', 'Shop\ShopController@shippingDeliveryPolicy')->name('shop.shipping.and.delivery.policy');

        //Membership Terms and Condition Page
        Route::get('/membership-tnc', 'Shop\ShopController@membershipTermsNCondition')->name('shop.membership.terms.and.condition');

        //Workforce Page
        Route::get('/workforce', 'Shop\ShopController@workforce')->name('shop.workforce');

        //Bujishu Service Page
        Route::get('/bujishu-service', 'Shop\ShopController@bujishuService')->name('shop.bujishu.service');

        //FAQ Page
        Route::get('/faq', 'Shop\ShopController@faq')->name('shop.faq');

        //Our Vision, Culture and Value
        Route::get('vision-culture-value', 'Shop\ShopController@visionCultureValue')->name('shop.vision');

        //Work In Progress Page
        Route::get('/work-in-progress', 'Shop\ShopController@workInProgress')->name('shop.wip');



        //add rbs postpaid
        Route::post('/addRbsPostpaid', 'Shop\ShopController@addRbsPostpaid')->name('shop.addRbsPostpaid');

        //Category Level On Page
        Route::group(['prefix' => 'category'], function () {


            Route::get(
                '/renovations',
                'Shop\ShopController@renovationCategory'
            );
            // Route::get(
            //     '/{category}/{sub}/{quality}',
            //     'Shop\ShopController@categoryList'
            //     )->name('shop.category.list');


            // Route::get('/category', ['uses' => 'Shop\ShopController@index', 'as' => 'shop.index']);





            // Route::get(
            //     '/{topLevelCategorySlug}/{secondLevelCategorySlug}',
            //     'Shop\ShopController@secondLevelCategory'
            // )->name('shop.category.second');

            // Route::get(
            //     '/{topLevelCategorySlug}/{secondLevelCategorySlug}/{thirdLevelCategory}',
            //     'Shop\ShopController@thirdLevelCategory'
            // )->name('shop.category.third');

            // route get ({category}/{quality} , Shop Conttroller)->name(')
        });

        Route::get('/dashboard/profile/index', 'Shop\ProfileController@index')->name('shop.dashboard.customer.profile');

        Route::get('/dashboard/profile/edit', 'Shop\ProfileController@edit')->name('shop.dashboard.customer.profile.edit');

        Route::post('dashboard/profile/update/{id}', 'Shop\ProfileController@updateProfile')->name('profile.update');

        Route::get('/dashboard/home', 'Shop\ValueRecordsController@homePageCustomer')->name('shop.dashboard.customer.home');

        //test value page
        Route::get('/dashboard/value-record', 'Shop\ValueRecordsController@valuePageCustomer')->name('shop.dashboard.customer.value-record');

        //2021 new my dashboard
        Route::get('/dashboard/home_new', function () {
            return view('shop.customer-dashboard.home_new');
        });
        // Route::get('/dashboard/home_new', 'Shop\ValueRecordsController@homePageCustomer')->name('shop.dashboard.customer.home_new');

        // Return Customer ->All Orders
        Route::get('/dashboard/orders/index', 'Shop\ValueRecordsController@customerAllOrders')->name('shop.customer.orders');
        // Return Customer -> Open Orders
        Route::get('/dashboard/orders/open-orders', 'Shop\ValueRecordsController@openOrders')->name('shop.customer.open-orders');

        // Return Customer -> Orders Status
        Route::get('/dashboard/orders/order-status', 'Shop\ValueRecordsController@orderStatus')->name('shop.customer.order-status');

        // Return My Perfect List
        Route::get('/dashboard/wishlist/index', 'Shop\ValueRecordsController@wishlist')->name('shop.wishlist.home');

        Route::get('/dashboard/promotion/index', 'Shop\ValueRecordsController@promotion')->name('shop.promotion.index');

        Route::get('/dashboard/voucher-list', 'Shop\ValueRecordsController@voucher')->name('shop.voucher.list');

        Route::get('/dashboard/promo-order', 'Shop\ValueRecordsController@promotion')->name('shop.promo.list');

        //RBS list
        Route::group(['prefix' => 'dashboard/rbs'], function () {

            Route::group(['prefix' => 'postpaid'], function () {
                Route::get('/', 'Shop\ValueRecordsController@rbsRemind')->name('shop.rbs.remind');
                Route::post('/update', 'Shop\ValueRecordsController@updateRBSMonthReminder')->name('shop.rbs.product-tracking-reminder-update');
            });

            Route::group(['prefix' => 'prepaid'], function () {
                Route::get('/', 'Shop\ValueRecordsController@rbsProductTracking')->name('shop.rbs.product-racking');
            });
        });

        // Route::get('/dashboard/rbs-postpaid-deactive', 'Shop\ValueRecordsController@inactiveRBSMonthReminder')->name('shop.rbs.rbs-postpaid-deactive');


        Route::get('/dashboard/change-password', 'Shop\ChangePasswordController@index')->name('shop.change.password.form');

        Route::post('/dashboard/change-password', 'Shop\ChangePasswordController@store')->name('shop.change.password');

        Route::get('/dashboard/reset-password', 'Shop\ForgotPasswordController@sendEmailReset')->name('shop.forgot.password');

        // TODO: Temporary. For interior-design payment.
        Route::get('/product/temp/renovation', 'Shop\ShopController@interiorDesign');

        // TODO: Temporary. For interior-design post payment.
        Route::post('/product/interior-design/store', 'Shop\ShopController@interiorDesignStore');

        // Route::get('/product/{productNameSlug}', 'Shop\ShopController@product')->name('shop.product');

        Route::put('/product/edit-address', 'Shop\ShopController@productChangeAddress')
            ->name('shop.product.edit-address');

        Route::group(['prefix' => 'cart'], function () {

            Route::post('/add-freegift', 'Shop\CartController@cartAddFreegift')->name('shop.freegift');

            Route::get('/', 'Shop\CartController@index')->name('cart.index');
            Route::post('/update-cart', 'Shop\CartController@updateCart')->name('shop.cart.update-cart');
            Route::get('/clear-voucher/{key?}', 'Shop\CartController@clearVoucher')->name('shop.cart.clear-voucher');
            Route::post('/get-postcode', 'PoslajuAPI@get_postcode_details')->name('shop.cart.get-postcode');
            Route::get('/get-city/{id}', 'Shop\CartController@getCity')->name('shop.cart.get-city');
            Route::post('/update-address', 'Shop\CartController@cartAddress')->name('shop.cart.update-address');
            // Checkout cart items.
            Route::post('/checkout', 'Purchase\PurchaseController@checkoutItems')->name('shop.cart.checkout');

            // POST route for adding item to cart.
            Route::post('/add-item', 'Shop\CartController@store')->name('shop.cart.add-item');

            Route::post('/buy-now', 'Shop\v1\CartControllers@store')->name('shop.cart.buy-now');

            // Save Delivery Method.
            Route::get('/update-deliveryMethod/{method}', 'Shop\CartController@updateDeliveryMethod')
                ->name('shop.cart.update-deliveryMethod');

            Route::get('/update-deliveryOutletId/{outletId}', 'Shop\CartController@updateDeliveryOutletId')
                ->name('shop.cart.update-deliveryOutletId');

            Route::get('/get-outletInfo/{outletId}', 'Shop\CartController@getOutletInfo')
                ->name('shop.cart.get-outletInfo');

            Route::get('/checkOutModal', 'Shop\CartController@checkOutModal')
                ->name('shop.cart.check-out-modal');

            // DELETE route for deleting cart item.
            // Route::put('/delete-item/{id}', 'Shop\v1\CartControllers@destroy')->name('shop.cart.delete-item');// Route::get('/checkout/offline', 'Purchase\PurchaseController@offlinePayment');// Route::post('/checkout/offline', 'Purchase\PurchaseController@offlinePaymentStore');
        });

        Route::group(['prefix' => 'wishlist'], function () {
            Route::get('/', 'Development\ComingSoonController@index');
        });

        // Order
        Route::prefix('order')->group(function () {
            // Order history page.
            Route::get('/', 'Shop\OrderController@index')->name('shop.order');

            // POST route for checking out cart item and placing order.
            Route::post('/checkout', 'Shop\OrderController@store')->name('shop.order.checkout');
        });
        // End Order

        // Membership
        Route::prefix('membership')->group(function () {
            // check membership page
            Route::get('/', 'Shop\ProfileController@membership')->name('customer.membership');
            // Check membership
            Route::post('/validation', 'Shop\ProfileController@checkMembership')
                ->name('customer.check.membership');
            // POST route for checking out cart vc\OrderController@store')->name('shop.order.checkout');
        });
        // End Order

    });
    // End Shop

    // Web
    Route::group(['prefix' => 'web'], function () {
        Route::group(['prefix' => 'cart'], function () {
            // Get user's cart items.
            Route::get('/', 'WEB\Shop\CartController@index')
                ->name('web.shop.cart');

            // Get total items in user's cart.
            Route::get('/get-quantity/{id}', 'WEB\Shop\CartController@getTotalCartQuantity')
                ->name('web.shop.cart.quantity');

            // Remove user's cart item.
            Route::put('/remove/{id}', 'WEB\Shop\CartController@remove');

            Route::post('/update-quantity/{id}', 'WEB\Shop\CartController@updateQuantity')
                ->name('web.shop.cart.update-quantity');

            // Remove user's cart quantity.
            // Route::put('/remove-cart/{id}', 'WEB\Shop\CartController@removeCartQuantity');

            // Toggle checked state.
            Route::post('/select-item/{id}', 'WEB\Shop\CartController@toggleSelectItem')
                ->name('web.shop.cart.toggle-select');

            Route::post('/cart/edit-address', 'WEB\Shop\CartController@productChangeAddress')
                ->name('web.shop.cart.edit-address');
            Route::post('/state-filter-city', 'WEB\Shop\CartController@stateFilterCity')
                ->name('web.shop.cart.state-filter-city');
            Route::post('/state-filter-billing-city', 'WEB\Shop\CartController@stateFilterBillingCity')
                ->name('web.shop.cart.state-filter-billing-city');

            Route::post('/state-filter-city', 'WEB\Shop\CartController@stateFilterCity')
                ->name('web.shop.cart.state-filter-city');
            Route::post('/state-filter-billing-city', 'WEB\Shop\CartController@stateFilterBillingCity')
                ->name('web.shop.cart.state-filter-billing-city');


            Route::post('/state-filter-billing-city', 'WEB\Shop\CartController@stateFilterBillingCity')
                ->name('web.shop.cart.state-filter-billing-city');

            // Get coupon.
            Route::get('/getCoupon/{coupon}/{counter?}', 'WEB\Shop\CartController@getCoupon')
                ->name('web.shop.cart.getCoupon');
        });

        Route::group(['prefix' => 'shop'], function () {

            // Get category.
            Route::get('/category/{categorySlug}', 'WEB\Shop\ShopController@category')
                ->name('web.shop.category');

            // Category filter.
            Route::post('/category/{categorySlug}', 'WEB\Shop\ShopController@categoryFilter')
                ->name('web.shop.category.filter');

            // Get product.
            Route::get('/product/{productSlug}', 'WEB\Shop\ShopController@product')
                ->name('web.shop.product');

            // Product filter.
            Route::post('/product/{productSlug}', 'WEB\Shop\ShopController@productFilter')
                ->name('web.shop.product.filter');
        });
    });
    // End Web
});

Route::group(['prefix' => 'shop','middleware' => ['auth', 'verified', 'check.info']], function () { // remove middleware to enable guest view
    // Emerald routes
    Route::get('product/{category}/{productSlug}', 'Shop\ShopController@productPage')->name('shop.main.product.page');

    Route::get('/search', 'Shop\ShopController@search')->name('shop.search');

    Route::group(['prefix' => 'category'], function () {
        //Navigation-bar-csutomer search bar
        Route::get(
            '/{topLevelCategorySlug?}',
            'Shop\ShopController@topLevelCategory'
        )->name('shop.category.first');
    });
    
});