@extends('layouts.management.main-dealer')
@section('content')
<div class="row mt-5" style="">
	<div class="col-8 ">
		<div class="row ml-0 ">
			<img class="hidden-sm" src="{{asset('/storage/dealer/banners/welcome-banner.png')}}" class="welcome-banner" style="width:98%;">
		</div>
		<br>
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="card border-radius-card shadow today-sales-tabs">
					<div class="card-body" style="height: 175px;">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h4 class="card-title-today-sales" style="font-weight:bold;">Today Sales</h4>
							</div>
							<div class="col-md-6 ">
								<p class="card-title float-right " style="color:#ffcc00;" s>Show more</p>
							</div>
						</div>
						<div class="row ">
							<div class="col-2 col-md-2 col-sm-12 col-xs-12 ml-3 ">
								<img class="sales-image sales-image-today" src="{{asset('/storage/dealer/icons/sales-icon.png')}}" alt="sales-icon">
							</div>
							<div class="col-7 col-md-6 ml-3 mt-2 today-sales">
								<p class="card-title ">
									<b>RM {{ number_format(($todaySales / 100), 2) }}</b>
								</p>
								<p class="today-sales-date">Daily Revenue: <br> {{ date('d/m/yy') }} </p>
							</div>
						</div>
					</div>
				</div>
				<div class="card border-radius-card mb-5 shadow summary-sales-tab ">
					<div class="card-body" style="height: 175px;">
						<div class="row">
							<div class="col-6 col-md-6">
								<h4 class="card-title-summary-sales" style="font-weight:bold;">Sales Summary</h4>
							</div>
							<div class="col-6 col-md-6">
								<p class="card-title float-right" style="color:#ffcc00;">Show more</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2 ml-3">
								<img class="sales-image sales-image-summary" src="{{asset('/storage/dealer/icons/sales-icon.png')}}" alt="sales-icon">
							</div>
							<div class="col-md-8 ml-3 mt-2 summary-sales">
								<p class="card-title">
									<b>RM {{ number_format(($monthlySales / 100), 2) }}</b>
								</p>
								<p>Month Revenue: <br> {{ date('F Y') }}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="card border-radius-card shadow mychart-tabs">
					<div class="card-body" style="height: 375px;">
						<div class="row">
							<div class="col-6 col-md-6">
								<strong>
									<h4 class="card-title-mychart-tabs " style="font-weight:bold">Monthly Income</h4>
								</strong>
								<h5 style="font-weight:bold">Year {{ date(' Y') }}</h5>
							</div>
							<div class="col-6 col-md-6">
								<p class="card-title   hidden-sm" style="float: right; color:#ffcc00;">Show more</p>
							</div>
						</div>
						<div class="row">
							<canvas class="pl-3 pr-3" id="myChart"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6 col-md-6 col-sm-12 col-xs-12">
				<div class="card border-radius-card shadow group-performance-tabs">
					<div class="card-body">
						<div class="row">
							<div class="col-8 col-md-8 ">
								<h4 class="card-title-group-performance " style="font-weight:bold; font-size:15pt;">Group Performance</h4>
							</div>
							<div class="col-4 col-md-4 ">
								<p class="card-title  hidden-sm" style="float: right; color:#ffcc00;">Show more</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h6 class="ml-1">
									<b>Member A</b>
								</h6>
								<div class="card pl-2 group-performance">
									<div class="card-body">
										<div style="border-bottom: 1px solid #000;">
											<h5>
												<strong>RM 5,888</strong>
											</h5>
											<p>
												<b>Sales Revenue | Today</b>
											</p>
										</div>
										<div class="mt-3">
											<h5>
												<strong>RM 20,888</strong>
											</h5>
											<p>
												<b>Sales Revenue | Month</b>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h6 class="ml-1">
									<b>Member A</b>
								</h6>
								<div class="card pl-2 group-performance">
									<div class="card-body">
										<div style="border-bottom: 1px solid #000;">
											<h5>
												<strong>RM 8,888</strong>
											</h5>
											<p>
												<b>Sales Revenue | Today</b>
											</p>
										</div>
										<div class="mt-3">
											<h5>
												<strong>RM 30,888</strong>
											</h5>
											<p>
												<b>Sales Revenue | Month</b>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="card border-radius-card shadow business-progress-tabs" style="height: 300px;">
					<div class="card-body">
						<div class="row">
							<div class="col-6 col-md-6">
								<h4 class="card-title-business-progress" style="font-weight:bold; font-size:15pt;">Business Progress</h4>
							</div>

							<div class="col-6 col-md-6">
								<p class="card-title float-right hidden-sm" style="color:#ffcc00;" s>Show more</p>
							</div>
						</div>
						<div class="row mt-5 ">
							<div class="col-4 col-md-4">
								<center>
									<div class="card business-progress">
										<div class="card-body mt-2 ">
											<h5>
												<strong>118</strong>
											</h5>
										</div>
									</div>
									<h6>
										<strong>Total Sales</strong>
									</h6>
								</center>
							</div>
							<div class="col-4 col-md-4">
								<center>
									<div class="card business-progress">
										<div class="card-body mt-2 ">
											<h5>
												<strong>118</strong>
											</h5>
										</div>
									</div>
									<h6>
										<strong>New Order</strong>
									</h6>
								</center>
							</div>
							<div class="col-4 col-md-4">
								<center>
									<div class="card business-progress">
										<div class="card-body mt-2 ">
											<h5>
												<strong>118</strong>
											</h5>
										</div>
									</div>
									<h6>
										<strong>New Members</strong>
									</h6>
								</center>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-6 ">
		<h4><strong>Announcement</strong></h4>
		<div class="row">
			<div class="col-12 col-md-12 announcement-tabs">
				<div class="news">
					<span class="datetime" style="">4 minutes ago</span>
					<h5 class="title"><b>Message Title</b></h5>
					<p class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
					<p style="border-bottom:1px solid black;"></p>

					<span class="datetime" style="">4 minutes ago</span>
					<h5 class="title"><b>Message Title</b></h5>
					<p class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
					<p style="border-bottom:1px solid black;"></p>

					<span class="datetime" style="">4 minutes ago</span>
					<h5 class="title"><b>Message Title</b></h5>
					<p class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
					<p style="border-bottom:1px solid black;"></p>

					<center>
						<p class="" style="color:#ffcc00;" s>Show more</p>
					</center>
				</div>

			</div>
		</div>


		<div class="card border-radius-card shadow feedback">
			<div class="card-body">
				<div class="row">
					<div class="col-6 col-md-6">
						<h4><strong>Feedback</strong></h4>
					</div>
					<div class="col-6 col-md-6 ">
						<p class="card-title float-right" style="color:#ffcc00;" s>Show more</p>
					</div>
				</div>
				<div class="row feedback-tabs">
					<div class="col-10 col-md-12">
						<div class="news">
							<span class="datetime" style="">4 minutes ago</span>
							<h5 class="title"><b>Message Title</b></h5>
							<p class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
							<p>
								<span class="fa fa-star" style="color:#ffcc00;"></span>
								<span class="fa fa-star" style="color:#ffcc00;"></span>
								<span class="fa fa-star" style="color:#ffcc00;"></span>
								<span class="fa fa-star-o"></span>
								<span class="fa fa-star-o"></span>
							</p>
							<p class="mb-3 mt-3" style="border-bottom:1px solid black;"></p>

							<span class="datetime" style="">4 minutes ago</span>
							<h5 class="title"><b>Message Title</b></h5>
							<p class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
							<p>
								<span class="fa fa-star" style="color:#ffcc00;"></span>
								<span class="fa fa-star" style="color:#ffcc00;"></span>
								<span class="fa fa-star" style="color:#ffcc00;"></span>
								<span class="fa fa-star-o"></span>
								<span class="fa fa-star-o"></span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script type="text/javascript">
	window.onload = function() {

		// Graph curve chart - Desktop
		var ctx = document.getElementById('myChart').getContext("2d")
		var gradientStroke = ctx.createLinearGradient(300, 0, 5, 0);


		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Jun", "Jul", "Aug", "Sep", "Oct", "Nov", ],
				datasets: [{
					label: "Sales",
					backgroundColor: '#ffcc00',
					data: [41000, 46000, 34000, 34000, 46000, 39000, ]
				}]
			},
			options: {
				legend: {
					position: "bottom",
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							fontColor: "rgba(0,0,0)",
							fontStyle: "bold",
							// beginAtZero: 10,
							suggestedMin: 10,
							suggestedMax: 50,
							maxTicksLimit: 5,
							padding: 0
						},
						gridLines: {
							drawTicks: false,
							display: true,
							drawBorder: false,
						}
					}],
					xAxes: [{
						gridLines: {
							zeroLineColor: "transparent",
							display: false
						},
						ticks: {
							padding: 0,
							fontColor: "rgba(0,0,0)",
						}
					}]
				}
			}
		});
	}
</script>
@endpush