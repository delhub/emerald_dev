<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Shipping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->bigInteger('shipping_category_special')->default(0)->after('shipping_category');
        });

        Schema::table('shipping_categories', function (Blueprint $table) {
            $table->bigInteger('active')->default(0)->after('cat_type');
            $table->string('for_user')->nullable()->after('active');
            $table->string('start_date')->nullable()->after('for_user');
            $table->string('end_date')->nullable()->after('start_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
