<?php

use App\Models\Purchases\Item;
use App\Models\Purchases\Order;
use App\Models\Purchases\RBS;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class GenerateOldRbsPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases_rbs', function (Blueprint $table) {
            //
        });

        $oldRBS = DB::table('purchases_rbs')->where('rbs_status','active')->get();

        // $oldRBS = RBS::where('rbs_status','active')->get();

        foreach ($oldRBS as $rbs) {
            $oldOrder = Order::where('order_number',$rbs->first_po)->first();
            $oldItem = Item::where('order_number',$rbs->first_po)->first();
            for ($i=2; $i <= ($rbs->total_months /$rbs->reminder_interval_months) ; $i++) {
                $newOrder = $oldOrder->replicate();
                $newOrder->id = 'new';
                $newOrder->order_number = $oldOrder->order_number.'-'.$i;
                $newOrder->delivery_order = $oldOrder->delivery_order.'-'.$i;
                $newOrder->order_status = 1001;
                $newOrder->delivery_info = null;
                $newOrder->delivery_date = 'Pending';
                $newOrder->courier_name = null;
                $newOrder->tracking_number = null;
                $newOrder->shipment_key = null;
                $newOrder->order_date = Carbon::parse($oldOrder->est_shipping_date)->addMonth($rbs->reminder_interval_months+($i-1))->format('d-m-Y');
                $newOrder->est_shipping_date = 'Pending';
                $newOrder->shipping_date = 'Pending';
                $newOrder->collected_date = 'Pending';
                $newOrder->created_at = Carbon::now();
                $newOrder->updated_at = Carbon::now();
                $newOrder->save();

                $newItem = $oldItem->replicate();
                $newItem->id='new';
                $newItem->order_number = $oldItem->order_number.'-'.$i;
                $newItem->delivery_order = $oldItem->delivery_order.'-'.$i;
                $newItem->item_order_status = 1001;
                $newItem->ship_date = 'Pending';
                $newItem->actual_ship_date = 'Pending';
                $newItem->collected_date = 'Pending';
                $newItem->created_at = Carbon::now();
                $newItem->updated_at = Carbon::now();
                $newItem->save();
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases_rbs', function (Blueprint $table) {
            //
        });
    }
}
