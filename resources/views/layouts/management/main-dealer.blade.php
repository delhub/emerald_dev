<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- JQUERY for datepicker -->
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

    <!--Bootstrap style for table form -->

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?v=1.7" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('assets/css/dealer.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Tangerine&display=swap" rel="stylesheet">

    @stack('style')


</head>

<body class="app header-fixed aside-menu-fixed sidebar-lg-show">
    @include('layouts.management.navigations.navigation-bar-dealer')
    <div class="">
        @include('layouts.management.navigations.side-bar-dealer')
        <main class="main">

            @yield('breadcrumbs')
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <!-- Content Here -->
                    @yield('content')
                </div>
            </div>
        </main>

    </div>
    @include('layouts.shop.footer.footer')
    <div id="coverBg"></div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript">
        function onPageLoadGetQuantity() {
            let getQuantityURL = "{{ route('web.shop.cart.quantity', ['id' => Auth::user()->id]) }}";
            const cartQuantityElement = $('#cart-quantity');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    cartQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        // function to get perfect list quantity
        function onPageLoadGetPerfectListQuantity() {
            let getQuantityURL = "{{ route('shop.perfect-list.quantity', ['id' => Auth::user()->id]) }}";
            const perfectListQuantityElement = $('#wishlist-amount');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    perfectListQuantityElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });


        }

        $(document).ready(function() {
            onPageLoadGetQuantity();
            onPageLoadGetPerfectListQuantity();
        });
    </script>

    <script>
        $(document).ready(function() {
            $("a").tooltip();
        });
    </script>



    @stack('script')

</body>

</html>