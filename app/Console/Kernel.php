<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\shippingCheck',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('shipping:check')->dailyAt('00:00');
        $schedule->command('voucher:check')->daily();
        $schedule->command('userpoints:check')->dailyAt('00:00');
        $schedule->command('rbs:postpaid')->dailyAt('01:00');
        // $schedule->command('syn:xilnex')->everyFiveMinutes();
        // $schedule->command('poslaju:check')->everyTenMinutes();
        $schedule->command('membership:check')->dailyAt('03:00');
        $schedule->command('productbrandlogs:new')->monthly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
