<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePanelInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_infos', function (Blueprint $table) {
            $table->string('ssm_number')->nullable()->change();
            $table->string('company_email')->nullable()->change();
            $table->string('company_phone')->nullable()->change();
            $table->string('pic_name')->nullable()->change();
            $table->string('pic_contact')->nullable()->change();
            $table->string('pic_nric')->nullable()->change();
            $table->string('pic_email')->nullable()->change();
            $table->string('name_for_display')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
