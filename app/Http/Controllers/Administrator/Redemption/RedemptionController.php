<?php

namespace App\Http\Controllers\Administrator\Redemption;

use App\Http\Controllers\Controller;
use App\Models\Globals\Status;
use App\Models\Purchases\Item;
use App\Models\Purchases\Purchase;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class RedemptionController extends Controller
{
    public function index(Request $request)
    {

        $statuses = Status::where('id', '1001')->whereNotIn('id', ['1000'])->get(); // remove status record created

        $customerOrders = Item::with(['order.purchase'])
            ->selectRaw('items.*, items.id')
            ->join('orders', 'orders.order_number', '=', 'items.order_number')
            // ->join('purchases', 'purchases.id', '=', 'orders.purchase_id')
            ->whereIn('item_order_status', ['1001'])
            ->whereIn('product_code', [
                'BU0121-0103-0006-Q2',
                'BU0718-0103-0001',
                'BU0718-0103-0001.2',
                'BU0718-0103-0002',
                'BU0718-0103-0002.2',
                'BU0920-0103-0006-K-2',
                'BU0920-0103-0006-Q',
                'BU0920-0103-0006-K',
                'BU0221-0103-0021-PK2',
                'BU1220-0103-0020',
                'BJS1020-0101-0004',
                'BU1020-0101-0004-Q',
                'BU1020-0101-0004-K',
                'BJS1020-0101-0005',
                'BJS1020-0101-0005-Q',
                'BJS1020-0101-0005-K',
                'BU2103-0101-0005-K',
                'BU2103-0101-0005-Q',
                'BU2103-0101-0006-K',
                'BU2103-0101-0006-Q',
                'BJS1120-0101-0011-PK2'
            ]) // selected product
            // ->whereIn('purchases.ship_state_id',[7])
            //->panelFilter($request->panel, $request->states) //pass $request->states for checking empty only
            //->statusFilter($request->status, $request->date, $request->period)
            ->statesFilter(7) // penang state only
            //->datebetweenFilter($request->datefrom, $request->dateto)
            // ->datebetweenFilter(date('Y-m-d'), date('Y-m-d'))
            // ->where('items.created_at' , '<' , Carbon::parse('2021-03-22 00:00:00'))
            ->searchFilter($request->searchBox, $request->selectSearch)
            //->findID($request->manyID)
            ->orderBy('items.created_at', 'DESC');

        // Post::with('user')->with(['order' => function($query) {
        //     $query->whereNotIn('order.order_status', ['1000']);
        // }])->get();
        // return $customerOrders;

        $ForCountcustomerOrders = $customerOrders->get();


        $customerOrders = $customerOrders->paginate(25);
        $customerOrders->appends(request()->query());

        return view('administrator.redemption.redemption')
            ->with('customerOrders', $customerOrders)
            ->with('statuses', $statuses)
            ->with('request', $request)
            ->with('ForCountcustomerOrders', $ForCountcustomerOrders);
    }
    public function logRedemption($do)
    {

        $redemption = array();

        $redemption['do_number'] = $do;
        $redemption['user'] = Auth::user()->id;

        Log::channel("redemption")->info($redemption);

        return redirect('/management/panel/orders/delivery-order-pdf/' . $do);
    }
}
