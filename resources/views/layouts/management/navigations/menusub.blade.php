@foreach($childs as $child)
 <li class="text-hover mynewhover">
     <a class="dropdown-item {{ count($child->childs) ? 'dropdown-toggle' :'' }}" href="{{$child->menu_link}}">{{ $child->menu_title }}</a>
     {{-- if have third sub sub menu use it! start --}}
       {{-- @if(count($child->childs))
          <ul class="list-unstyled components " style="padding: 0px 0;">
              <li class="text-hover" style="font-size:15pt;">
                 <a href="#" style="color: black;">
                       @include('layouts.shop.navigation.menusub',['childs' => $child->childs])
                    </a>
                </li>
            </ul>
        @endif --}}
    {{-- if have third sub sub menu use it! end  --}}
   </li>
 @endforeach