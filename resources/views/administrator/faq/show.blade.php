@extends('layouts.shop.main')

@section('content')

<h1>FAQ2</h1>


<div class="container mt-5" style="min-height: 75vh;">
    <div class="row">
        <div class="col-12 offset-md-4 col-md-6">
            <h1 style="color:#ffcc00; font-weight:700;">We're here to help</h1>
            <h5 class="ml-md-4" style="color:#ffcc00;">Frequently asked Questions</h5>
        </div>
    </div>
    <div class="row mt-md-4">
        <div class="col-12">
            <div class="accordion " id="faqExample">
                <div class="card" style="border-radius:15px;">
                    <div class="card-header p-2" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link stretched-link" style="color:black; float:left;" type="button" data-toggle="collapse" data-target="#collapseOne" 
                            aria-expanded="true" aria-controls="collapseOne">
                              
                            </button>
                          </h5>
                    </div>

                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#faqExample">
                        <div class="card-body">
                       
                        </div>
                    </div>
                </div>              
            </div>            
        </div>
    </div>
</div>


<style>
.card-border-radius{
        border-radius: 15px;    
    }
@media(max-width:500px){
        .button-left-margin{
            margin-left: -1rem;
    }
}
</style>
@endsection
