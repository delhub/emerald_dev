@extends('layouts.administrator.main')

@section('content')

    <h1>Cities</h1>

    <div class="card shadow-sm">
        <div class="card-body">

            <form action="{{ route('administrator.cities.index') }}" method="GET">
                <div class="row">

                        <div class="col-md-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="custom-select" id="selectSearch" name="selectSearch">
                                        <option value="ALL" selected>Choose...</option>
                                        <option value="SI" {{ isset($request) && $request->selectSearch == 'SI' ? 'selected' : '' }}>State</option>
                                        <option value="CI" {{ isset($request) && $request->selectSearch == 'CI' ? 'selected' : '' }}>City Key</option>
                                        <option value="CD" {{ isset($request) && $request->selectSearch == 'CD' ? 'selected' : '' }}>City Name</option>
                                        <option value="Country" {{ isset($request) && $request->selectSearch == 'Country' ? 'selected' : '' }}>Country Name</option>
                                    </select>

                                    <input type="text" class="form-control" id="" name="searchBox"
                                        value="{{ isset($request) && ($request->searchBox) ? $request->searchBox : '' }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-warning" style="color:black; position:absolute; width:85%;">Search</button>
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-warning" style="color:black; position:absolute; width:85%; margin-left:15px;"><a href={{ route('administrator.cities.index')}} style="color:black;">Reset Search</a></button>
                        </div>

                        <div class="col-md-8">
                                <a href="{{ route('administrator.cities.create') }}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark float-right">Create New Cities</a>
                        </div>

                </div>

                <div class="row float-right">
                    <div class="col-md-12 " >
                {!! $cities->render() !!}
                    </div>
                </div>
            </form>

            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a  href="/administrator/cities/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Cities</a>
                </div>
            </div>

            <div class="row">
                <div class="col-auto ml-auto">
                    {!! $cities->render() !!}
                </div>
            </div> --}}

            <div class="table-responsive m-2">

                @if(count($cities) > 0)

                <table id="global-cities-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Country</td>
                            <td>State</td>
                            <td>City Key</td>
                            <td>City Name</td>
                            <td>City Data</td>
                            <td>Actions</td>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($cities as $city)

                        <tr>
                            <td>{{$city->id}}</td>
                            <td>{{$city->country_id}}</td>
                            <td>{{$city->state->name}}</td>
                            <td>{{$city->city_key}}</td>
                            <td>{{$city->city_name}}</td>
                            <td>{{$city->city_data}}</td>

                            <td style="width: 20%;">
                                <a style="color: white; font-style: normal; border-radius: 5px;" href="{{ route('administrator.cities.edit',$city->id) }}" class="btn btn-primary shadow-sm">Edit</a>
                            </td>
                        </tr>

                        @endforeach

                    </tbody>

                </table>

                @endif

            </div>
        </div>
    </div>

@endsection
