@extends('layouts.shop.main')

@section('content')

@section('page_title')
{{ "" }}
@endsection
<div class="cart-home">
  <div class="fml-container">
      <div class="subtitle-namelist">
          <ul>
              <li><a href="/">Home</a></li>
              <li><span>/</span></li>
              <li><a href="#">Cart Page</a></li>
          </ul>
      </div>
              <section>
                      <div class="row footer-btm">
                          <div class="col-md-5">
                              <img class="thankyou-3dimg" src="{{asset('/images/formula/payment-fail-bg.png')}}" alt="">
                          </div>
                          <div class="col-md-7">
                              <div class="error-tittle">
                                  <div class="thankyou-text">
                                      <h5>Opps, Looks Like Something Went Wrong. Please Try Again.</h5>
                                      <p>Fail to process your payment.</p>
                                  </div>
                                  <div class="eror-text">
                                      <p>Error Code :102</p>
                                      <p>Bank Reason: Invalid Fields “card_number”</p>
                                      <p>Error Code :102</p>
                                      <p>Bank Reason: Invalid Fields “card_number”</p>
                                  </div>
                                  <div class="edit-opt-btn btn-space2">
                                      <button class="savechg-btn btn-primary">Try Again</button>
                                  </div>
                              </div>
                          </div>
                      </div>
              </section>
  </div>
</div>
@endsection


@push('script')
<script>
    
</script>
@endpush