<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\Products\WarehouseBatch;

class CreateNewRolesWhCashier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //warehouse cashier for redemption
        DB::table('roles')->insert(['name' => 'wh_cashier', 'guard_name' => 'web', 'created_at' =>  Carbon::now(),  'updated_at' =>  Carbon::now()]);

        Schema::table('wh_batch', function (Blueprint $table) {
            $table->bigInteger('location_id')->after('batch_type');
        });
        DB::table('wh_batch')->update(['location_id' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('');
    }
}
