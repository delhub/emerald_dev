<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Models\Dealers\DealerDirectPay;
use App\Models\Globals\Cards\Issuer;
use App\Models\Globals\PaymentGateway\MerchantID;
use App\Models\Globals\PaymentGateway\Response;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use App\Models\Users\Customers\PaymentInfo;
use App\Models\Users\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use App\Models\Globals\Countries;
use App\Models\Purchases\RBS;
use App\Models\Users\UserPaymentToken;
use App\Models\Users\UserPoints;

class CybersourceGatewayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function submitForm($request, $purchase, $user, $paymentOption)
    {

        // return $user->email;
        // dd($request);
        // if ($purchase->installment_data) {
        //     $payment_amount = $purchase->installment_data->totalpayment;
        // } else {
        $payment_amount = $purchase->purchase_amount / 100;
        // };
        if (country()->country_id == 'SG') $payment_amount = $purchase->purchase_amount / 100 *  3.15 * 1.07;

        // dd($request->card_type);
        switch ($request->card_type) {
            case 'visa':
                $request->card_type = '001';
                break;
            case 'mastercard':
                $request->card_type = '002';
                break;
            case 'amex':
                $request->card_type = '003';
                break;
            default:
                $request->card_type = null;
                break;
        }

        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        // dd($purchase->email);
        $secret_key = env('SECRET_KEY','5405c2b88aee494ca2778ea0bafb914e7a532fb8359a4fe0bae9a1f2684326fa194f415314a741af9838f0f78d5d325a37c44db1b3e74db1b822764882aa325b668ead6c2e124886b4afde50bf8053936f4ee7562a22427b8bf053f059c8495ef915d4da5c374be3b7f3219e612e6eca42bb8847fc414ece989e072810b65b0c');
        $unique_id =  uniqid();

        $cybersource = array();
        $cybersource['override_custom_receipt_page'] = URL::to('/payment/gateway-response?process=cybersource');
        $cybersource['override_custom_cancel_page'] = URL::to('/payment/gateway-response?process=cybersource');
        $cybersource['signed_field_names'] = 'override_custom_receipt_page,override_custom_cancel_page,access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency';
        $cybersource['unsigned_field_names'] = '_token,payment_method';

        // dd($request->input());

        if($request->cardChoices != 'newCard' && !$request->directID){

            $paymentToken = UserPaymentToken::where('id',$request->cardChoices)->first();

            if ($paymentToken) {
                $cybersource['payment_token'] = $paymentToken->token_id;
                $cybersource['transaction_type'] = 'sale';
                $cybersource['card_type'] = $paymentToken->card_type;
            } else {
                $response = array();
                $response['response_code'] = $request->input('reason_code');
                $response['description'] =  'FAILED';
                return view('shop.payment.failed')
                ->with('errorMessage', $response);
            }

            $cybersource['signed_field_names'] .= ",payment_token,card_type";

        } else { // first time/ manual checkout

            $cybersource['card_number'] = $request->card_number;
            $cybersource['card_type'] =  $request->card_type;
            $cybersource['card_cvn'] = $request->card_cvv;
            $cybersource['card_expiry_date'] = substr_replace($request->card_expiry, "-20", "2", 0);
            $address = json_decode($request->address);
            $cybersource['bill_to_address_country'] = 'MY';
            $cybersource['bill_to_forename'] = trim($address->name);
            $cybersource['bill_to_surname'] = 'Mr/Ms';
            $cybersource['bill_to_email'] = trim($user->email);
            $cybersource['bill_to_phone'] = trim(preg_replace('/[^\d]/', '', $address->mobile));
            $cybersource['bill_to_address_line1'] = trim(substr($address->address_1, 0, 40));
            $cybersource['bill_to_address_line2'] = trim(substr($address->address_2, 0, 40));
            $cybersource['bill_to_address_city'] = $address->city != null ? trim($address->city) : 'Kuala Lumpur';
            $cybersource['bill_to_address_postal_code'] = null;

            $cybersource['unsigned_field_names'] .= ',bill_to_address_country,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_line2,bill_to_address_city,bill_to_address_postal_code';
            $cybersource['signed_field_names'] .= ",card_number,card_type,card_cvn,card_expiry_date";

            if ($request->transaction_type != 'sale' && !$request->directID) { // opt-in tokenization


                $cybersource['transaction_type'] = 'sale,create_payment_token';
                $cc = $cybersource['card_number'];
                $updatePaymentToken = UserPaymentToken::where('user_id',$user->id)->where('card_number',str_pad(substr($cc, -4), strlen($cc), 'X', STR_PAD_LEFT))->where('card_type',$request->card_type)->first();
                if ($updatePaymentToken) {
                    $cybersource['payment_token'] = $updatePaymentToken->token_id;
                    $cybersource['card_type'] = $updatePaymentToken->card_type;
                    $cybersource['transaction_type'] = 'sale,update_payment_token';
                    $cybersource['signed_field_names'] .= ",payment_token,card_type";
                }
                $cybersource['consumer_id'] = $user->userInfo->account_id;
                $cybersource['unsigned_field_names'] .= ',consumer_id';

            }  else{ // dont want tokenization
                $cybersource['transaction_type'] = 'sale';
            }
        }



        $cybersource['access_key'] = env('ACCESS_KEY','60b6e627f4a43789aa8b287014298649');
        $cybersource['profile_id'] = env('PROFILE_ID','0FF4998F-E2EB-4A53-B768-B7C61AA44762');
        $cybersource['transaction_uuid'] = $purchase->purchase_number . '-' . $unique_id;
        $cybersource['signed_date_time'] = gmdate("Y-m-d\TH:i:s\Z");
        $cybersource['locale'] = 'MY';
        $cybersource['reference_number'] = $purchase->purchase_number . '-' . $unique_id;
        $cybersource['amount'] = country()->country_id == 'MY' ? $payment_amount : str_replace(',', '', $payment_amount);
        $cybersource['currency'] = 'MYR';
        $cybersource['_token'] = $request->_token;
        $cybersource['payment_method'] = 'card';

        $cybersource['signature'] = $this->sign($cybersource, $secret_key);

            // return $cybersource;

        $payment = new Payment;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->purchase_uuid = $purchase->purchase_number . '-' . $unique_id;
        $payment->gateway_response_code = 'PENDING';
        $payment->amount = $purchase->purchase_amount;
        $payment->point_amount = $purchase->point_amount;
        $payment->local_amount =  (country()->country_id == 'SG') ? round($payment_amount * 100) : 0;
        $payment->gateway_string_result = '-';
        $payment->auth_code = '-';
        $payment->last_4_card_number = substr($request->card_number, -4);
        $payment->expiry_date = '-';
        $payment->gateway_eci = '-';
        $payment->gateway_security_key_res = '-';
        $payment->gateway_hash = '-';
        $payment->save();


        $paymentGatewayResponse = new PaymentGatewayController;

        if ($payment->point_amount != 0) {

            $points = array();
            $points['point_type'] = 'debit';
            $points['transaction_type'] = 'purchase';
            $points['status'] = 'pending';
            $points['amount'] = '-' . $payment->point_amount;
            $points['balance'] = $user->userInfo->user_points - $payment->point_amount;

            $paymentGatewayResponse->saveUserPoints($payment, $points);
        }

        $logdata = $cybersource;
        if (isset( $cybersource['card_number'])){
            $cc = $cybersource['card_number'];
            $logdata['card_number'] = str_pad(substr($cc, -4), strlen($cc), 'X', STR_PAD_LEFT);

        }
        Log::channel('cybersource')->info($logdata);

        $endpoint = env('CYBERSOURCE_BASEURL').'/pay';

        return view('shop.payment.card.post-to-gateway-cybersource')
            ->with('cybersource', $cybersource)
            ->with('endpoint', $endpoint);
    }

    public function gatewayResponse($request)
    {
        // dd($request);

        foreach ($request->request as $name => $value) {
            $params[$name] = $value;
        }

        Log::channel('cybersource')->info($params);

        $paymentGatewayResponse = new PaymentGatewayController;

        // cubersource payment gateway

        $payment = Payment::where('purchase_uuid', $request->input('req_reference_number'))->first();
        if (isset($payment)) $userPoints = UserPoints::where('purchase_id', $payment->purchase->id)->first();

        $errorMessage = array();
        $errorMessage['id'] = 0;
        $errorMessage['response_code'] = 'FAILED';

        if (!isset($payment)) {
            $errorMessage['description'] = "Payment Not Found.";

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $purchase = $payment->purchase;

        $directPayTables = DealerDirectPay::where('purchase_id', $purchase->id)->first();

        if (isset($directPayTables)) {
            $errorMessage['directPay_uuid'] = '/direct-pay/' . $directPayTables->uuid_link;
        }

        if (in_array($payment->gateway_response_code, ['00', '100', 'BP']) || $request->input('reason_code') == '104') {

            $errorMessage['description'] = "Duplicated Payment Detected.";

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        // Hash Checking
        $scretKeyy = env('SECRET_KEY','5405c2b88aee494ca2778ea0bafb914e7a532fb8359a4fe0bae9a1f2684326fa194f415314a741af9838f0f78d5d325a37c44db1b3e74db1b822764882aa325b668ead6c2e124886b4afde50bf8053936f4ee7562a22427b8bf053f059c8495ef915d4da5c374be3b7f3219e612e6eca42bb8847fc414ece989e072810b65b0c');

        if (strcmp($params["signature"], $this->sign($params, $scretKeyy)) != 0) {

            $errorMessage['description'] = "Hash Validation Failed.";

            $payment->gateway_response_code = 'FAILED';
            $payment->save();

            $purchase->purchase_status = 4006;
            $purchase->save();

            if ($userPoints) {
                $userPoints->status = 'cancelled';
                $userPoints->save();
            }

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        $user = User::find($purchase->user_id);
        $response = $request->input('reason_code');

        // return$purchase->user_id;

        $errorMessage = $this->processCyberSourcePayment($request, $purchase,  $payment);

        if ($response != '100') {

            $payment->gateway_response_code = $response ?? $request->input('decision');
            $payment->save();

            if (isset($directPayTables)) {
                $errorMessage['directPay_uuid'] = '/direct-pay/' . $directPayTables->uuid_link;
            }

            // dd($errorMessage);

            return view('shop.payment.failed')
                ->with('errorMessage', $errorMessage);
        }

        // dd($response);

        $paymentGatewayResponse->updateGroupSales($user, $purchase);

        $check_registration = true;

        $purchase->increment('invoice_version');

        $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration, false, false);

        // dd($purchase, $user->email);
        GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);

        $gateway = 'CC';
        $transaction_id = 'Card ' .  substr($request->input('req_card_number'), -4) . ' - ' .  $request->input('auth_code');

        $paymentGatewayResponse->sendToMarketingServer($user->userInfo->account_id, $purchase->getFormattedNumber(),  $purchase, $purchase->purchase_amount / 100, $gateway, $transaction_id);

        return $paymentGatewayResponse->markOrderComplete($purchase, $user);
    }

    public function processCyberSourcePayment($request, $purchase, $payment)
    {
        $user = User::find($purchase->user_id);
        $userPoints = UserPoints::where('purchase_id', $payment->purchase->id)->first();


        if (($request->input('reason_code')) && ($request->input('reason_code') == '100')) {

            $payment->gateway_string_result = $request->input('transaction_id');
            $payment->gateway_response_code = $request->input('reason_code');
            $payment->auth_code = $request->input('auth_code');
            $payment->last_4_card_number = substr($request->input('req_card_number'), -4);
            $payment->expiry_date = substr($request->input('req_card_expiry_date'), 0, 2) . substr($request->input('req_card_expiry_date'), -2);
            $payment->amount = $request->input('req_amount') * 100;
            $payment->gateway_eci = '-';
            $payment->gateway_security_key_res = $request->input('signature');
            $payment->gateway_hash = $request->input('signature');

            $payment->save();

             $response['response_code'] =  $request->input('reason_code') ;
             $response['description'] = 'Successful transaction';
             $purchase->purchase_status = in_array($purchase->payment_type,['7003','7004']) ? 4007 : 4002;

            //  $rbs = RBS::where('purchases_id',$purchase->id)->where('rbs_status','pending')->first();

            //     if (isset($rbs)) {
            //         $rbs->rbs_status = $purchase->purchase_status == 4002 ? 'active' : 'cancel';
            //         $rbs->save();
            //     }


            if ($userPoints) {

                $userPoints->status = 'approved';

                $userInfo = $user->userInfo;
                $userInfo->user_points = $userPoints->balance;
                $userInfo->save();

                if ($payment->point_amount == 0) {

                    $paymentGatewayResponse = new PaymentGatewayController;

                    $points = array();
                    $points['point_type'] = 'credit';
                    $points['transaction_type'] = 'purchase';
                    $points['status'] = 'pending';
                    $points['amount'] = '+' . $payment->amount;

                    $points['balance'] = $userInfo->user_points + $payment->amount;

                    $paymentGatewayResponse->saveUserPoints($payment, $points);
                }
            }
            // dd($request->input('req_transaction_type'));
            if ($request->input('req_transaction_type') != 'sale') {

                $paymentToken = UserPaymentToken::where('user_id',$user->id)->where('card_number',$request->input('req_card_number'))->where('card_type',$request->input('req_card_type'))->first();

                if ($paymentToken) {
                    $paymentToken->token_id = $request->input('req_payment_token');
                } else {
                    $paymentToken = new UserPaymentToken();
                    $paymentToken->user_id = $user->id;
                    $paymentToken->token_id = $request->input('payment_token');
                    $paymentToken->card_number = $request->input('req_card_number');
                    $paymentToken->card_type = $request->input('req_card_type');
                }
                $paymentToken->save();
            }
        } else {

            switch ($request->input('decision')) {
                case 'REVIEW':
                    $response['response_code'] =  $request->input('reason_code');
                    $response['description'] = 'Under Review';
                    break;
                case 'DECLINE':
                    $response['response_code'] =  $request->input('reason_code');
                    $response['description'] = self::declineCode($request['reason_code']);
                    break;
                case 'ERROR':

                    $response['response_code'] =  $request->input('reason_code');

                    if ($request->input('invalid_fields')) {
                        $response['description'] = 'Invalid Fields ' . '"' . $request->input('invalid_fields') . '"';
                    } else {
                        $response['description'] = self::errorCode($request['reason_code']);
                    }
                    break;
                case 'CANCEL':
                    $response['response_code'] =  'CANCELLED';
                    $response['description'] = 'Cancelled by customer';
                    break;
                default:
                    $response['response_code'] = $request->input('reason_code');
                    $response['description'] =  'FAILED';
                    break;
            }
            if ($userPoints) {
                $userPoints->status = 'cancelled';
            }
            $purchase->purchase_status = 4006;
        }
        if ($userPoints) {
            $userPoints->save();
        }
        $purchase->save();


        Log::channel('transactions')->info($payment);
        return $response;
    }

    public function sign($params, $secret_key)
    {

        $signedFieldNames = explode(",", $params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . "=" . $params[$field];
        }
        $buildDataToSign = implode(",", $dataToSign);

        return base64_encode(hash_hmac('sha256', $buildDataToSign, $secret_key, true));
    }

    public static function errorCode($code)
    {
        switch ($code) {
            case '102':
                $reason = 'Invalid Fields';
                break;
            case '104':
                $reason = 'Not Unique Transaction';
                break;
            case '110':
                $reason = 'Only partial Amount approved,';
                break;
            case '150':
                $reason = 'System Failed';
                break;
            case '151':
                $reason = 'Server Timeout';
                break;
            case '152':
                $reason = 'Service Timeout';
                break;

            default:
                $reason = 'Unknown Error';
                break;
        }


        return $reason;
    }

    public static function declineCode($code)
    {
        switch ($code) {
            case '476':
                $reason = 'Payer authentication could not be authenticated.';
                break;
            case '208':
                $reason = 'Inactive card or card not authorized';
                break;
            default:
                $reason = 'Declined by Bank';
                break;
        }

        return $reason;
    }
}
