<?php

namespace App\Models\Testbanners;

use Illuminate\Database\Eloquent\Model;

class Testbanner extends Model
{
    // Table Name
    protected $table = 'testbanners';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestemps = true;
}
