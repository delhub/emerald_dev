@extends('layouts.administrator.main')

@section('content')
<br>
<h4>Pending Approval</h4>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {{-- <h5 class="card-title">Card title</h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> --}}
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Agent</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Redeemed Invoice (qty)</th>
                            <th scope="col">Total Amount</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dealerPreBuys as $dealerPreBuyKey => $dealerPreBuy)
                        @foreach ($dealerPreBuy->itemsProductCode() as $itemKey => $item)
                        {{-- @dd($dealerPreBuy->itemsProductCode(),$itemKey,$dealerPreBuy) --}}
                        @php
                        $rowspan = count($dealerPreBuy->itemsProductCode());
                        @endphp
                        <tr class="{{ $dealerPreBuyKey % 2 == 0 ? 'grey' : '' }}">
                            @if ($loop->first)
                            <th scope="row" rowspan="{{ $rowspan }}">{{ $loop->iteration }}</th>
                            <td rowspan="{{ $rowspan }}">

                                {{ $dealerPreBuy->dealer_id }}
                                <br>
                                @if ($dealerPreBuy->dealerInfo)
                                {{ $dealerPreBuy->dealerInfo->full_name }}
                                @endif
                            </td>
                            @endif
                            <td>
                                {{ $item['product_name']}}
                            </td>
                            <td>
                                {{ $dealerPreBuy->dealerData($dealerPreBuy->dealer_id,$itemKey) }}
                                /
                                {{ $dealerPreBuy->dealerTotalStock($dealerPreBuy->dealer_id,$itemKey) }}
                            </td>
                            <td>
                                @foreach ($item['item'] as $eachitem)
                                <a href="{{ route('global.view.pdf',['pdf_type'=>'invoice','purchase_number'=>$eachitem->order->purchase->getFormattedNumber()]) }}" target="_blank">{{ $eachitem->order->purchase->purchase_number }}</a>
                                     ({{ $eachitem->quantity }})
                                    <br>
                                @endforeach
                                {{-- <hr>
                                Total : {{ array_sum(array_column($item['item'], 'quantity')) }} --}}
                            </td>
                            @if ($loop->first)
                            <td rowspan="{{ $rowspan }}"> RM {{ number_format(($dealerPreBuy->total_amount/100),2) }}
                            </td>
                            <td rowspan="{{ $rowspan }}">
                                <a href="{{ route('administrator.vqs.submit.approval',['id'=>$dealerPreBuy->id]) }}" class="{{ $dealerPreBuy->prebuy_status != 5007 ?: 'disabled'}} vqs-button btn btn-primary text-white">{{ $dealerPreBuy->prebuy_status != 5007 ?'Approve': 'Approved'}} </a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection
@push('style')
<style>
  .disabled.btn.btn-primary.text-white.vqs-button{
      background: grey;
      pointer-events: none;
        cursor: default;
  }
  tr.grey {
  background: rgb(231, 231, 231);
}
</style>

@endpush
