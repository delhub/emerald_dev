<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Users\Dealers\DealerInfo;
use Illuminate\Http\Request;

class ApiAgentsController extends Controller
{
    public function checkAgent(Request $request)
    {
        //
        $dealer = DealerInfo::where('account_id', $request->id)->first();

        if ($dealer) {
            $response = ['result' => 'success', 'dealer_name' => $dealer->full_name, 'group_id' => $dealer->group_id];
        } else {
            $response = ['result' => 'failed', 'dealer_name' => 'Agent Not Found', 'group_id' => 'Agent Not Found'];
        }

        return response($response, 200);
    }
}
