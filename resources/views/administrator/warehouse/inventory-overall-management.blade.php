@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<h1>FML Inventory Summary</h1>

<div class="card shadow-sm">
    <div class="card-header">FML Inventory Summary<small> (from 1st Oct 2021)</small></div>
    <div class="card-body">

        <form action="{{ route('administrator.warehouse.inventory.overall-management') }}" method="GET" class="col-md-6">
            <div class="row mt-2 ml-2">
                <div class="col-5 pr-0">
                    <input type="text" class="form-control d-inline" id="searchBox" name="search_box"
                        placeholder="Product Code / Name"
                        value="{{ ($request->search_box) ?? '' }}">
                </div>
                    <div class="col-5 pr-0">
                        <select class="form-control" name="quality">
                        <option value="0"{{($request->quality == 0) ? 'selected' :''}}>All Quality</option>
                        <option value="1"{{($request->quality == 1) ? 'selected' :''}}>Domestic</option>
                        <option value="2"{{($request->quality == 2) ? 'selected' :''}}>Import</option>
                        <option value="3"{{($request->quality == 3) ? 'selected' :''}}>Exclusive</option>
                        </select>
                </div>
                <div class="col-5 pr-0">
                    <select class="form-control" name="location">
                        <option value="all">All Location</option>
                        @foreach ($locations as $location)
                            <option value="{{$location->id}}"{{($request->location == $location->id) ? 'selected' :''}}>{{$location->location_name}}</option>
                        @endforeach
                    </select>
                </div>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="col-2 px-0">
                    <button type="submit" class="btn btn-outline-primary d-inline"
                        style="color:black;">Search</button>
                </div>
            </div>
        </form>

        <div class="row mt-2 ml-2">
            {{ $lists->links() }}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <form action="{{ route('administrator.warehouse.inventory.overall-management') }}" method="GET">
                <button type="submit" class="btn btn-outline-primary" style="color:black;">Reset</button>
            </form>
        </div>

        <table class="table table-sm table-hover table-bordered">
            <thead>
                <tr>
                    <th scope="col" class="align-middle">#</th>
                    <th scope="col" class="align-middle">Product Code</th>
                    <th scope="col" class="align-middle">Product Name</th>
                    <th scope="col" class="align-middle">Product Attribute</th>
                    <th scope="col" class="align-middle">Product Quanlity</th>


                    <th scope="col" class="align-middle">In (Stock In)</th>
                    <th scope="col" class="align-middle">Online Sold</th>
                    <th scope="col" class="align-middle">Retail Sold</th>
                    <th scope="col" class="align-middle">Out</th>
                    <th scope="col" class="align-middle">Balance Physical</th>
                    <th scope="col" class="align-middle">Balance Virtual</th>


                    <th scope="col" class="align-middle">
                        Inventory
                        {{-- <i class="bi bi-arrow-right-circle" onclick="showRow()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-right" viewBox="0 0 16 16">
                                <path d="M6 12.796V3.204L11.481 8 6 12.796zm.659.753 5.48-4.796a1 1 0 0 0 0-1.506L6.66 2.451C6.011 1.885 5 2.345 5 3.204v9.592a1 1 0 0 0 1.659.753z"/>
                            </svg>
                        </i> --}}
                    </th>
                </tr>
            </thead>

            <tbody>
                @foreach ($lists as $product_code => $list)
                <tr>
                    <td scope="row" class="text-center">{{ $loop->iteration }}</td>
                    <td>{{ $product_code }}</td>
                    <td>{{ $list['global_product_name'] }}</td>
                    <td>{{ $list['product_name'] }}</td>
                    <td>{{ showGlobalProductQuality($list['quality']) }}</td>

                    <td class="text-success">{{ $list['total_stockIn'] }}</td>
                    <td class="text-danger">{{ $list['online_sold'] }}</td>
                    <td class="text-danger">{{ $list['pos_completed'] }}</td>
                    <td class="text-danger">{{ $list['total_sold'] }}</td>
                    <td class="text-success">{{ $list['overallPhysical'] }}</td>
                    <td class="text-success">{{ $list['virtual_stock'] }}</td>

                    <td class="text-center">
                        <form action="{{ route('administrator.warehouse.inventory.index') }}" method="GET">
                            <input type="hidden" class="form-control d-inline" id="searchBox" name="search_box"  value="{{ ($product_code) ?? '' }}">
                            <button type="submit" class="btn btn-primary">Inventory</button>
                        </form>

                    </td>


                    <td colspan="6" id="row_" class="bg-light" style="display: none">
                        Virtual stock:
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="row mt-2 ml-2">
            {{ $lists->links() }}
        </div>

    </div>
</div>
@endsection

@push('script')

<script>
    function showRow() {
        var row_ = document.querySelectorAll('#row_');

        row_.forEach(item => {
            if (item.style.display == "none") {
                item.style.display = "block";
            }else{
                item.style.display = "none";
            }
        });
    }
</script>

@endpush
