@extends('layouts.administrator.mainVue')

@section('pageTitle', 'Gift rules')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.css" rel="stylesheet"></link>
<style type="text/css">
    input[type="date"]::-webkit-calendar-picker-indicator {
        background: transparent;
        bottom: 0;
        color: transparent;
        cursor: pointer;
        height: auto;
        left: 0;
        position: absolute;
        right: 0;
        top: 0;
        width: auto;
    }
</style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.js"></script>
@env('local')
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
@endenv
@env('prod')
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.14/vue.min.js" crossorigin="anonymous"
    referrerpolicy="no-referrer"></script>
@endenv
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.4/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js"></script>
@endsection

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light"> Free Gift / </span> Create new
    </h4>

    <div class="row" v-cloak>
        <div class="col-sm-12 col-md-8 col-lg-6">
          <div class="card mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Free Gift rules</h2>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary mb-3" href="{{ route('freegift.index') }}"> Back</a>
                        </div>
                    </div>
                </div>

                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                  </div>
                @endif

                {!! Form::open(array('route' => 'freegift.store','method'=>'POST','id'=>'gift')) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Outlet:</strong>
                            {!! Form::select('outlet_id', $data,[],['class' => 'form-control']); !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" id="focus" v-model="gift_name" name="gift_name" class="form-control" placeholder="Gift rule name" required>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Gifted By:</strong>
                            <select name="gifted" class="form-control" v-model="gifted" @@change="setGifted(gifted)" required>
                                <option value="product">Product A + B..</option>
                                {{-- <option value="price">Product Price</option> --}}
                                <option value="brand">Product Brand</option>
                                <option value="category">Product Category</option>
                                <option value="quantity">Product Quantity</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group" v-show="gifted == 'quantity'">
                            <strong>Cointains Product:</strong>
                            <select name="contains" class="multi" v-model="contains_qty">
                                <option v-for="(p,i) in product" :value="p.product_code">@{{p.product_name}}</option>
                            </select>
                        </div>
                        <div class="form-group" v-show="gifted == 'product'">
                            <strong>Cointains Product:</strong>
                            <select name="contains[]" class="multi" multiple v-model="contains_product">
                                <option v-for="(p,i) in product" :value="p.product_code">@{{p.product_name}} (@{{p.attribute_name}}) - @{{p.product_code}}</option>
                            </select>
                        </div>
                        <div class="form-group" v-show="gifted == 'brand'">
                            <strong>Cointains Product Brand:</strong>
                            <select name="contains[]" class="multi" multiple v-model="contains_brand">
                                <option v-for="(b,i) in brand" :value="b.id">@{{b.name}}</option>
                            </select>
                        </div>
                        <div class="form-group" v-show="gifted == 'category'">
                            <strong>Cointains Product Category:</strong>
                            <select name="contains[]" class="multi" multiple v-model="contains_category">
                                <option v-for="(c,i) in category" :value="c.id">@{{c.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12" v-if="gifted == 'quantity'">
                        <div class="form-group">
                            <strong>Product Quantity:</strong>
                            <input type="text" v-model="quantity" name="quantity" class="form-control" placeholder="Product quantity">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Free gift type:</strong>
                            {!! Form::select('gift_type', array('product' => 'Product','multiproduct' => 'Multi Product A or B', 'price_value' => 'Price Value', 'price_percentage' => 'Price Percentage'),[],['class' => 'form-control','v-model' => 'gift_type']); !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" v-show="gift_type =='multiproduct'">
                        <div class="form-group">
                            <strong>Free gift (Multi Product):</strong>
                            <select name="gift_product[]" class="multi" multiple v-model="gift_product">
                                <option v-for="(p,i) in product" :value="p.product_code">@{{p.product_name}} (@{{p.attribute_name}}) - @{{p.product_code}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" v-show="gift_type =='product'">
                        <div class="form-group">
                            <strong>Free gift (Product):</strong>
                            <select name="gift_product" class="multi" v-model="gift_quantity">
                                <option v-for="(p,i) in product" :value="p.product_code">@{{p.product_name}} (@{{p.attribute_name}}) - @{{p.product_code}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12"  v-show="gift_type =='price_value' || gift_type =='price_percentage'">
                        <div class="form-group" v-show="gift_type =='price_value'">
                            <strong>Free gift Price (Value):</strong>
                            <input type="text" v-model="gift_price" name="gift_price_value" class="form-control" placeholder="Gift price value">
                        </div>
                        <div class="form-group" v-show="gift_type =='price_percentage'">
                            <strong>Free gift Price (Percentage):</strong>
                            <input type="text" v-model="gift_price" name="gift_price_percentage" class="form-control" placeholder="Gift price percentage">
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label class="form-label">Start Date</label>
                        <input name="start_date" type="date" class="form-control" placeholder="date" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-label">End Date</label>
                        <input name="end_date" type="date" class="form-control" placeholder="date" required>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <hr>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
// import SlimSelect from 'slim-select'
const app = new Vue({
    el: '#app',
    data: {
        form: [],
        gift_name: '',
        gifted: '',
        contains_product: [],
        contains_qty: '',
        contains_brand: [],
        contains_category: [],
        product: [],
        brand: [],
        category: [],
        quantity: '',
        gift_product: [],
        gift_quantity: '',
        gift_type: 'product',
        gift_price: '',

    },
    mounted() {
        this.getProduct()
        // this.getBrand()
        if(!this.isset(this.gifted)) {
            this.gifted = "product"
        }
    },
    methods: {
        getProduct() {
            axios
                .get('/api/v1/product/gift-product')
                .then(response => {
                    this.product = response.data
                    })
                .catch(error => console.log(error))
        },
        getBrand() {
            axios
                .get('/api/v1/product/brand')
                .then(response => {
                    this.brand = response.data
                    })
                .catch(error => console.log(error))
        },
        getCategory() {
            axios
                .get('/api/v1/product/gift-categories')
                .then(response => {
                    this.category = response.data
                    })
                .catch(error => console.log(error))
        },
        setGifted(gifted) {
            switch (gifted) {
                case 'brand':
                    this.getBrand()
                    break;
                case 'category':
                    this.getCategory()
                    break;
                default:
                    this.getProduct()

            }
        },
        isset(_var){
            return !!_var; // converting to boolean.
        }

    },
    computed: {

    }

});

$("#gift").parsley({
    errorClass: 'is-invalid text-danger',
    successClass: 'is-valid',
    errorsWrapper: '<span class="form-text text-danger"></span>',
    errorTemplate: '<span></span>',
    trigger: 'change'
});

const selects = document.querySelectorAll('.multi')
selects.forEach((selectElement) => {
  new SlimSelect({
    select: selectElement
  })
});

// setInterval(function(){ document.getElementById("focus").focus(); }, 1000);

</script>

@endpush
