<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink"
    style="padding:0;background-color:#ffcc00;border-radius:30px;">
    <a class="dropdown-item profile-menu" href="/shop/dashboard/profile/index"
        style="padding: 10px 0px 10px 20px;border-radius: 30px 30px 0 0;">
        <img src="{{ asset('assets/images/icons/profile_b.png') }}" alt="Profile"
            style="height:23px; padding:0 10px 0 0px;"> Profile
    </a>

    <a class="dropdown-item myDashboard-menu" href="{{ route('shop.dashboard.customer.home') }}">
        <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="My Dashboard"
            style="height:23px; padding:0 10px 0 0px !important;">
        My Dashboard
    </a>

    @hasrole('dealer')
    <a class="dropdown-item agentDashboard-menu" href="{{ route('agent.overview') }}">
        <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Agent Dashboard"
            style="height:23px; padding:0 10px 0 0px !important;"> Agent Dashboard
    </a>
    @endhasrole
    @hasrole('panel')
    <a class="dropdown-item panelDashboard-menu" href="{{ route('management.panel.home') }}">
        <img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Panel Dashboard"
            style="height:23px; padding:0 10px 0 0px !important;"> Panel Dashboard
    </a>
    @endhasrole
    @hasrole('redemption')
    <a href="{{ route('administrator.redemption') }}" class="dropdown-item redemptionDashboard-menu"><img
            src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Administrator"
            style="height:23px; padding:0 10px 0 0px !important;"> Redemption</a>
    <!-- <a href="/administrator/sales" class="dropdown-item saleSummary-menu"><img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Sale Summary" style="height:23px; padding:0 10px 0 0px !important;"> Sale Summary</a> -->
    @endhasrole
    @hasrole('administrator')
    <a href="/administrator" class="dropdown-item adminDashboard-menu"><img
            src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Administrator"
            style="height:23px; padding:0 10px 0 0px !important;"> Administrator</a>
    <!-- <a href="/administrator/sales" class="dropdown-item saleSummary-menu"><img src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Sale Summary" style="height:23px; padding:0 10px 0 0px !important;"> Sale Summary</a> -->
    @endhasrole


    @hasrole('management')
    <a href="{{route('management.sale')}}" class="dropdown-item saleSummary-menu"><img
            src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Sale Dashboard"
            style="height:23px; padding:0 10px 0 0px !important;"> Sale Dashboard</a>
    @endhasrole

    @hasrole('redemption')
                <a href="{{ route('administrator.redemption') }}" class="dropdown-item redemptionDashboard-menu"><img
                        src="{{ asset('assets/images/icons/dashboard_b.png') }}" alt="Administrator"
                        style="height:23px; padding:0 10px 0 0px !important;"> Redemption</a>
        @endhasrole





    <a class="dropdown-item logout-menu" href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();" style="border-radius: 0 0 30px 30px;">
        <img src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout"
            style="height:23px; padding:0 10px 0 0px !important;"> {{ __('Logout') }}
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>