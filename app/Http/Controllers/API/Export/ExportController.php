<?php

namespace App\Http\Controllers\API\Export;

use App\Http\Controllers\API\ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;
use App\Models\Products\Product as PanelProduct;
use App\Models\Globals\Countries;
use App\Models\Registrations\Dealer\Registration;
use App\Models\Registrations\Dealer\Packages;
use App\Http\Controllers\ExcelExport;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Dealers\DealerGroup;
use App\Models\Globals\Status;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Purchase;
use App\Models\Purchases\Item;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use setasign\Fpdi\Fpdi;
use ZipArchive;
use App\Models\Categories\PivCategory;
use App\Models\Outlet\Outlet;
use App\Models\Categories\Category;



class ExportController extends ResponseController
{
    public function index()
    {
        //

        $country = strtolower(country()->country_id);
        $branches = Outlet::get();

        return view('administrator.export-manager.index')
            ->with('country', $country)
            ->with('branches', $branches);

    }

    public function adminexport() {

        return view('administrator.export-manager.admin-export');
    }

    public function admin_report_csv($report_name,Request $request)
    {
        switch ($report_name){
            case 'egg':
                $data = $this->eggreport_csv($request);
                $excelName = 'Download Egg Order';
            break;
        }

        $excelExport = new ExcelExport();
        $excelExport->setData($data);

        $excelExport->setHeadings(array(
            'Purchase Date Time', 'Purchase Number', 'DO Number', 'Outlet Name', 'Purchase To', 'Group Name', 'Product Name', 'Product Code', 'Product Quantity'
        ));

        $filename = date('Ymd');
        return $excelExport->download("{$excelName}-{$filename}.xlsx");
    }


    public function eggreport_csv(Request $request)
    {
        $data = $catproductid = [];

        $categories = Category::where('slug', 'egg-report')->with('products.panelProduct')->first();

        $categories->products->each(function($product) use (&$catproductid){
            $catproductid[] = $product->panelProduct->id;
        });

        $dateFrom = $request->input('datefrom');
        $dateTo = $request->input('dateto');

        $items = Item::whereIn('product_id', $catproductid)
            ->whereHas('order.purchase', function ($q) use ($dateFrom, $dateTo) {
                $q->whereIn('purchase_status', ['4002', '4007'])
                    ->datebetweenFilter($dateFrom, $dateTo);
            })->with(['order.purchase.user.userInfo.agentGroup', 'order.outlet'])->get();

        foreach ($items as $item) {
            $order = $item->order;
            $purchase = $order->purchase;
            $group = $order->purchase->user->userInfo->agentGroup;

            if ($order->delivery_outlet != 0) {
                $outletName = $order->outlet->outlet_name;
            } else {
                $outletName = 'Delivery';
            }

            $purchaseDate = $purchase->purchase_date;
            $createdAtTime = Carbon::parse($purchase->created_at)->format('H:i:s');
            // Collect data for export
            $data[] = [
                'Purchase Date Time' => $purchaseDate . ' ' . $createdAtTime,
                'Purchase Number' => $purchase->purchase_number,
                'DO Number' => $order->delivery_order,
                'Outlet Name' => $outletName,
                'Purchase To' => $purchase->ship_full_name,
                'Group Name' => $group->group_name,
                'Product Name' => $item->product->parentProduct->name,
                'Product Code' => $item->product_code,
                'Product Quantity' => "" . $item->quantity
            ];
        }

        return $data;
    }


    public function customer_csv($country_id, Request $request)
    {
        $dealerGroups = DealerGroup::all();
        $data = $this->query_customer($request, $country_id);

        foreach ($data as &$_data) {

            $_data->group_id = ($_data->group_id) ? $dealerGroups->find($_data->group_id)->group_name : 'NOT SET';

            $toUpper = array('full_name', 'group_id', 'referrer_name');

            foreach ($toUpper as $field) $_data->$field =  strtoupper($_data->$field);
            $_data = (array) $_data;
        }

        $excelExport = new ExcelExport();
        $excelExport->setData($data);

        $excelExport->setHeadings(array(
            'Customer ID',  'Name', 'IC Number', 'Referrer ID', 'Referrer Name', "Email Address", "Group"
        ));
        $filename = date('Ymd');
        return $excelExport->download("formula-customer-{$country_id}-{$filename}.xlsx");
    }

    public function product_csv($country_id, Request $request)
    {
        $panels = PanelInfo::all();
        $data = $this->query_product($request, $country_id);

        foreach ($data as &$_data) {

            $toUpper = array('name', 'attribute_name');
            foreach ($toUpper as $field) $_data->$field =  strtoupper($_data->$field);

            $price = array('member_price', 'product_sv');
            foreach ($price as $pfield)  $_data->$pfield = (is_numeric($_data->$pfield) && $_data->$pfield != 0) ?   $_data->$pfield : 'NOT SET';
            $_data = (array) $_data;
        }

        $excelExport = new ExcelExport();
        $excelExport->setData($data);

        $excelExport->setHeadings(array(
            'Product Date',  'Attribute ID',  'Panel Product ID', 'Product Name', 'Attribute Name', 'Product Code', "Product SV", "Member Price", 'Fixed Price', "Web Offer Price", "Outlet Price", "Advance Price", 'Premier Price'

        ));
        $filename = date('Ymd');
        return $excelExport->download("formula-product-{$country_id}-{$filename}.xlsx");
    }
    public function agent_csv($country_id, Request $request)
    {

        $dealerGroups = DealerGroup::all();

        $data = $this->query_agent($request, $country_id);


        foreach ($data as &$_data) {

            $_data->group_id = ($_data->group_id) ? $dealerGroups->find($_data->group_id)->group_name : 'NOT SET';

            $toUpper = array('full_name', 'group_id', 'bank_acc_name', 'referrer_name');

            foreach ($toUpper as $field) $_data->$field =  strtoupper($_data->$field);
            $_data = (array) $_data;
        }

        $excelExport = new ExcelExport();
        $excelExport->setData($data);
        $excelExport->setHeadings(array(
            'Agent ID', 'IC Number', 'Name', 'Referrer ID', 'Referrer Name', "Group", "Customer ID", "Bank", "Bank Acc Num", "Bank Acc Name", "Bank Acc ID/RegID"
        ));
        $filename = date('Ymd');
        return $excelExport->download("formula-agent-{$country_id}-{$filename}.xlsx");
    }
    public function order_csv($country_id, Request $request)
    {
        if ($request->input('m') == null)  $request->merge(array('m' => date('m')));
        if ($request->input('y') == null)  $request->merge(array('y' => date('Y')));
        $data = $this->query_order($request, $country_id);
        $status = Status::all();
        foreach ($data as &$_data) {

            $_data->purchase_status = ($_data->purchase_status) ? $status->find($_data->purchase_status)->name : 'NOT SET';

            $toUpper = array('full_name', 'name', 'purchase_status', 'purchase_type');

            foreach ($toUpper as $field) $_data->$field =  strtoupper($_data->$field);

            $price = array('unit_price', 'delivery_fee', 'installation_fee', 'subtotal_price', 'purchase_amount');
            foreach ($price as $pfield)  $_data->$pfield = (is_numeric($_data->$pfield)) ?   $_data->$pfield / 100 : 'NOT SET';

            $_data = (array) $_data;
        }

        $excelExport = new ExcelExport();
        $excelExport->setData($data);

        $excelExport->setHeadings(array(
            'Order Date',  'Purchase Number', 'Order Number', 'Customer Name', 'Customer ID', "Email Address", "Payment Type", "Order Amount",
            "Product", "Product Details", "Quantity", "Single Unit Price", "Delivery Fee", "Installation Fee", "Line Subtotal", "Product Code",
            "Payment Auth", "Fee Tables Data"
        ));
        $filename = date('Ymd');
        return $excelExport->download("formula-order-{$country_id}-{$filename}.xlsx");
    }
    public function agent_json(Request $request)
    {
        try {
            $result = $this->query_agent($request);
            return $this->sendResponse($result);
        } catch (ModelNotFoundException $exception) {
            $error = 'The requested resource couldn\'t be found.';
            return $this->sendError($error, 404);
        }
    }

    public function customer_json(Request $request)
    {
        try {
            $result = $this->query_customer($request);
            return $this->sendResponse($result);
        } catch (ModelNotFoundException $exception) {
            $error = 'The requested resource couldn\'t be found.';
            return $this->sendError($error, 404);
        }
    }

    public function product_json(Request $request)
    {
        try {
            $result = $this->query_product($request);
            return $this->sendResponse($result);
        } catch (ModelNotFoundException $exception) {
            $error = 'The requested resource couldn\'t be found.';
            return $this->sendError($error, 404);
        }
    }
    public function order_json(Request $request)
    {
        try {
            $result = $this->query_order($request);
            return $this->sendResponse($result);
        } catch (ModelNotFoundException $exception) {
            $error = 'The requested resource couldn\'t be found.';
            return $this->sendError($error, 404);
        }
    }
    public function query_agent(Request $request)
    {
        $country = country()->country_id;
        $result =  DB::select(DB::raw("
                SELECT a.account_id agent_id, a.nric, a.full_name, SUBSTRING(d.referrer_id,3) referrer_id, a.referrer_name, a.group_id, c.account_id customer_id, k.bank_code, k.bank_acc, k.bank_acc_name, k.account_regid
                FROM dealer_infos a LEFT JOIN users b ON b.id = a.user_id LEFT JOIN user_infos c ON b.id = c.user_id LEFT JOIN dealer_country d on a.account_id = d.account_id
                LEFT JOIN dealer_bank k ON k.account_id = a.account_id WHERE a.account_status <> 9 AND a.full_name is not NULL AND d.country_id = '{$country}' AND a.nric IS NOT NULL ORDER BY agent_id
            "));
        $packages = Packages::pluck('id')->all();
        $ranks = Packages::pluck('rank', 'id')->all();
        $registrations = Registration::whereIn('package_id', $packages)->get();

        foreach ($result as &$_result) {
            $_result = (array) $_result;
            $registration = $registrations->where('agent_id', $_result['agent_id'])->first();

            $_result['rank'] = (isset($registration) && $ranks[$registration->package_id]) ? $ranks[$registration->package_id] : 'NOT SET';
            $_result = (object) $_result;
        }

        return $result;
    }

    public function query_customer(Request $request)
    {

        $country = country()->country_id;
        $result =  DB::select(DB::raw("
                SELECT a.account_id,a.full_name,a.nric, SUBSTRING(d.referrer_id,3) referrer_id, a.referrer_name, b.email, a.group_id
                FROM user_infos a LEFT JOIN users b ON b.id = a.user_id
                LEFT JOIN user_country d on a.account_id = d.account_id
                WHERE a.full_name is not NULL AND account_status <> 9 AND d.country_id = '{$country}'
            "));

        return $result;
    }


    public function query_product(Request $request)
    {

        $country = country()->country_id;
        $date = Carbon::now();
        $lastMonth =  $date->subMonth()->startOfMonth()->format('Y-m-d');
        $result =  DB::select(DB::raw("
                SELECT attributes.id id, panel.id panel_id,
                (case when attributes.product_code != '' then attributes.product_code else  globals.product_code END) product_code,
                globals.name,
                attributes.attribute_name,
                attributes.created_at as product_date
                FROM panel_products panel
                JOIN panel_product_attributes attributes ON panel.id = attributes.panel_product_id
                JOIN global_products globals
                ON globals.id = panel.global_product_id
                AND (globals.product_status = 1 OR globals.created_at > '{$lastMonth}')
                ORDER BY panel.id
            "));
        $i = 0;
        $attributes_dat = $panels_dat = $attributes = $panels = $attr_name = $panel_attributes = $malaysia_result = $new_result = array();

        foreach ($result as $_result) {
            $attributes[] =  $_result->id;
            $panels[] =  $_result->panel_id;
        }


        $panelProducts = PanelProduct::whereIn('id', $panels)->get();

        $attributes_dat = self::parsePrice('App\Models\Products\ProductAttribute', 'MY', $attributes);
        $panels_dat = self::parsePrice('App\Models\Products\Product', 'MY', $panels);

        foreach ($result as $_result) {
            $panel_attributes[$_result->panel_id][] = $attributes_dat[$_result->id]['member_price'];

            $panel_attributes[$_result->panel_id][] = $attributes_dat[$_result->id]['fixed_price'];
            $panel_attributes[$_result->panel_id][] = $attributes_dat[$_result->id]['web_offer_price'];
            $panel_attributes[$_result->panel_id][] = $attributes_dat[$_result->id]['outlet_price'];
            $panel_attributes[$_result->panel_id][] = $attributes_dat[$_result->id]['advance_price'];
            $panel_attributes[$_result->panel_id][] = $attributes_dat[$_result->id]['premier_price'];
        }

        // dd($panel_attributes);


        foreach ($panel_attributes as $panel_id => $panel_attribute) {
            $is_all_empty = true;

            foreach ($panel_attribute as $attr_price) {

                if ($attr_price != 0)   $is_all_empty = false;
            }

            if (!$is_all_empty)    unset($panel_attributes[$panel_id]);
        }

        foreach ($result as $key => $_result) {

            if ((!isset($attributes_dat[$_result->id]) || $attributes_dat[$_result->id]['member_price'] == 0) && !isset($panel_attributes[$_result->panel_id])) {

                continue;
            }

            $result_data = new \stdClass;
            $result_data->product_date = explode(" ", $_result->product_date)[0];
            $result_data->id =  $_result->id;
            $result_data->panel_id = $_result->panel_id;
            $result_data->name = $_result->name;
            $result_data->attribute_name = $_result->attribute_name;
            $result_data->product_code = $_result->product_code;
            $result_data->product_sv = $attributes_dat[$_result->id]['product_sv'];

            $result_data->member_price = $attributes_dat[$_result->id]['member_price'];

            $result_data->fixed_price = $attributes_dat[$_result->id]['fixed_price'];
            $result_data->web_offer_price = $attributes_dat[$_result->id]['web_offer_price'];
            $result_data->outlet_price = $attributes_dat[$_result->id]['outlet_price'];
            $result_data->advance_price = $attributes_dat[$_result->id]['advance_price'];
            $result_data->premier_price = $attributes_dat[$_result->id]['premier_price'];

            $my_result[$result_data->id] = $result_data;

            $new_result[$result_data->id] = self::prepareUnique($result_data);
        }


        if ($country != 'MY') {
            $attributes_datC =  self::parsePrice('App\Models\Products\ProductAttribute', $country, $attributes);
            $panels_datC = self::parsePrice('App\Models\Products\Product', $country, $panels);
            $countryTable = Countries::where('country_id', $country)->first();

            foreach ($result as $key => $_result) {

                if ((!isset($attributes_datC[$_result->id]) || $attributes_datC[$_result->id]['member_price'] == 0) && !isset($my_result[$_result->id])) continue;

                $result_data = new \stdClass;
                $result_data->id =  $_result->id;
                $result_data->panel_id = $_result->panel_id;
                $result_data->name = $_result->name;
                $result_data->attribute_name = $_result->attribute_name;
                $result_data->product_code = $_result->product_code;

                if (isset($attributes_datC[$_result->id]) && $attributes_datC[$_result->id]['product_sv'] != 0) {

                    $result_data->product_sv = $attributes_datC[$_result->id]['product_sv'];
                } else if (isset($panels_datC[$_result->panel_id]) && $panels_datC[$_result->panel_id]['product_sv'] != 0  && ($result_data->member_price == $panels_dat[$_result->panel_id]['member_price'])) {

                    $result_data->product_sv = $panels_datC[$_result->panel_id]['product_sv'];
                } else if (isset($my_result[$_result->id]) && $my_result[$_result->id]->product_sv != 0) {

                    $malaysiaSV = $my_result[$_result->id]->product_sv;

                    $panelProduct = (isset($panelProduct)) ? $panelProduct : $panelProducts->find($_result->panel_id);
                    $singaporePrice = $result_data->member_price;
                    $malaysiaPrice = $my_result[$_result->id]->member_price;
                    $result_data->product_sv =  $panelProduct::defaultSVCalculation($malaysiaSV,  $singaporePrice, $malaysiaPrice);
                } else {
                    $result_data->product_sv = 0;
                };

                if ((isset($attributes_datC[$_result->id]) && $attributes_datC[$_result->id]['member_price'] != 0)) {
                    $result_data->member_price = $attributes_datC[$_result->id]['member_price'];
                } else if (isset($panels_datC[$_result->panel_id]) && $panels_datC[$_result->panel_id]['member_price'] != 0) {

                    $result_data->member_price = $panels_datC[$_result->panel_id]['member_price'];
                } else {
                    $MYprice = $my_result[$_result->id]->member_price;
                    $panelProduct = $panelProducts->find($_result->panel_id);
                    $result_data->member_price =  $panelProduct::defaultPriceCalculate($MYprice,  $countryTable, $panelProduct)['firm'];
                }




                $country_result[$result_data->id] = $result_data;

                $new_result[$result_data->id] = self::prepareUnique($result_data);
            }

            $actual_result = $country_result;
        } else {

            $actual_result = $my_result;
        }


        $new_result = array_unique($new_result);

        foreach ($actual_result as $key => &$_result) {
            if (!isset($new_result[$key])) {
                unset($actual_result[$key]);
                continue;
            }
            $_result->member_price =  $_result->member_price / 100;

            $_result->fixed_price =  $_result->fixed_price / 100;
            $_result->web_offer_price =  $_result->web_offer_price / 100;
            $_result->outlet_price =  $_result->outlet_price / 100;
            $_result->advance_price =  $_result->advance_price / 100;
            $_result->premier_price =  $_result->premier_price / 100;
        }

        return $actual_result;
    }

    public static function prepareUnique($result)
    {
        $item = new \stdClass;
        $item->product_code = $result->product_code;
        $item->name = $result->name;
        $item->member_price = $result->member_price;
        $item->product_sv = $result->product_sv;

        return json_encode($item);
    }

    public function query_order(Request $request)
    {
        $country = country()->country_id;
        $y = $request->input('y');
        $m = $request->input('m');
        $ids = array();

        $packages = Packages::pluck('id')->all();
        $registrations = Registration::whereIn('package_id', $packages)->pluck('agent_id', 'invoice_number');

        $result =  DB::select(DB::raw("
                SELECT a.id, d.account_id , e.order_number, d.full_name, a.purchase_status, a.purchase_date, a.purchase_number, a.purchase_type, a.purchase_amount, g.name, e.product_information, e.quantity, e.unit_price, e.delivery_fee, e.installation_fee, e.subtotal_price, e.product_code, b.auth_code
                FROM purchases a
                LEFT JOIN user_infos d ON a.user_id = d.user_id
                LEFT JOIN payments b ON a.purchase_number = b.purchase_number
                LEFT JOIN orders c ON a.id = c.purchase_id
                LEFT JOIN items e ON c.order_number = e.order_number
                LEFT JOIN panel_products f ON e.product_id = f.id
                LEFT JOIN global_products g ON f.global_product_id = g.id
                WHERE a.purchase_status in (4002,4003, 4007,3001,3013)  and (a.purchase_type IS NULL or  a.purchase_type != 'bluepoint')
                AND a.purchase_date  LIKE '%{$m}/{$y}' AND e.bundle_id = 0 and a.country_id = '{$country}'
            "));



        foreach ($result as $_result) {
            $ids[$_result->id] = $_result->id;
        }

        $fees =   DB::table('shipping_fee')->whereIn('purchase_id', $ids)->get();

        foreach ($result as &$_result) {
            $_result = (array) $_result;
            // $ids[$_result->id] = $_result->id;
            if (isset($registrations[$_result['purchase_number']])) $_result['for_agent'] = $registrations[$_result['purchase_number']];
            $types = array();
            foreach ($fees->where('purchase_id', $_result['id']) as $fee) {

                $type = $fee->type;
                $types[$type] = $type;
                if(isset($_result[$type])){
                    $_result[$type]['name']  .= ', ' . $fee->name;
                    $_result[$type]['total']  +=  $fee->amount;
                } else{
                    $_result[$type] = ['name' => $fee->name, 'total' => $fee->amount];
                }
            }
            foreach ($types as $_type){
                $_result[$_type] = json_encode( $_result[$_type]);
            }
            unset($_result['id']);
            $_result = (object) $_result;
        }

        return $result;
    }

    public static function parsePrice($type, $country_id, $model_ids)
    {
        $attributes_dat = array();
        $attr1 =  DB::table('panel_product_prices')
            ->where('country_id',  $country_id)
            ->where('model_type', $type)
            ->whereIn('model_id', $model_ids)
            ->where('active', 1)
            ->get();


        foreach ($attr1 as $_attr1) {

            $attributes_dat[$_attr1->model_id]['member_price'] = (float) $_attr1->price;
            $attributes_dat[$_attr1->model_id]['product_sv'] = (float) $_attr1->product_sv;
            $attributes_dat[$_attr1->model_id]['fixed_price'] = (float) $_attr1->fixed_price;
            $attributes_dat[$_attr1->model_id]['web_offer_price'] = (float) $_attr1->web_offer_price;
            $attributes_dat[$_attr1->model_id]['outlet_price'] = (float) $_attr1->outlet_price;
            $attributes_dat[$_attr1->model_id]['advance_price'] = (float) $_attr1->advance_price;
            $attributes_dat[$_attr1->model_id]['standard_price'] = (float) $_attr1->standard_price;
            $attributes_dat[$_attr1->model_id]['premier_price'] = (float) $_attr1->web_offer_price;
            $attributes_dat[$_attr1->model_id]['outlet_offer_price'] = (float) $_attr1->outlet_offer_price;
        }

        return $attributes_dat;
    }

    public function download(Request $request)
    {
        $excelExport = new ExcelExport();

        $data = $request->input('data');
        $data = $excelExport::prepareDataImport($data);
        $excelExport->setData($data);

        $excelExport->setHeadings(array(
            'Product Code', 'Product Name', 'Physical Stock', 'Virtual Stock (Reserved)', 'Virtual Stock (Reserved Offline)', 'Virtual Stock'
        ));
        $filename = date('Ymd');
        return $excelExport->download("formula-warehouse-{$filename}.xlsx");
    }

    public function payment_csv($country_id, Request $request)
    {

        $datefrom = date('dMY', strtotime($request->datefrom));
        $dateto = date('dMY', strtotime($request->dateto));

        if ($request->downloadType == 'invoice') {
            return $this->downloadInvoice($request->datefrom, $request->dateto);
        } elseif ($request->downloadType == 'payment') {
            $pdfName = 'fml_account_payment_' . $datefrom . '-' . $dateto . ' (website).xlsx';
        } else {
            $pdfName = 'FORMULA - SALES & ITEMISE REPORT FORMAT (' . $datefrom . '-' . $dateto . ').xlsx';
        }


        return (new PaymentExport($request))->download($pdfName);
    }

    public function downloadInvoice($oridatefrom, $oridateto)
    {

        $datefrom = date('d/m/Y', strtotime($oridatefrom));
        $dateto = date('d/m/Y', strtotime($oridateto));

        $purchases = DB::select(DB::raw("
        SELECT * FROM purchases a WHERE a.purchase_status IN (3001,3002,3003,3013,4001,4002,4007) AND (str_to_date(a.purchase_date, '%d/%m/%Y') >= str_to_date('{$datefrom}', '%d/%m/%Y') AND str_to_date(a.purchase_date, '%d/%m/%Y') <= str_to_date('{$dateto}', '%d/%m/%Y'))
        "));

        if (!empty($purchases)) {

            $files = array();
            $domain = request()->server->get('DOCUMENT_ROOT');

            foreach ($purchases as $purchase) {
                $invoice_number = ltrim($purchase->purchase_number, "0");
                $purchaseVersion = $purchase->invoice_version;
                if ($purchaseVersion == 0) {
                    $pdfUrl = $domain . '/storage/documents/invoice/' . $invoice_number . '/' . $invoice_number . '.pdf';
                } else {
                    $pdfUrl = $domain . '/storage/documents/invoice/' . $invoice_number . '/v' . $purchaseVersion . '/' . $invoice_number . '.pdf';
                }

                //check server folder/file
                if (file_exists($pdfUrl)) {

                    //file path
                    $files[] = $pdfUrl;
                    //file name
                    $fname[] = $invoice_number;
                } else {
                    $not_combine[] = $invoice_number;
                }
            }

            $datefrom = date('d M Y', strtotime($oridatefrom));
            $dateto = date('d M Y', strtotime($oridateto));

            $zip      = new ZipArchive;
            $fileName = 'FML-Invoice (' . $datefrom . '-' . $dateto . ').zip';
            $destinationPath = 'storage/documents/temp/';

            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0755, true);
            }

            if ($zip->open(public_path($destinationPath . $fileName), ZipArchive::CREATE) === TRUE) {

                foreach ($files as $key => $value) {
                    $relativeName = basename($value);
                    $zip->addFile($value, $relativeName);
                }
                $zip->close();
            }
            return response()->download(public_path($destinationPath . $fileName));
        } else {
            return redirect()->back()->withErrors(['pdf' => ['No Invoices Found']]);
        }
    }
}
