@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/administrator/faq" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Edit FAQ
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{ route('administrator.faq.update',['id' => $faq->id]) }}" id="edit-faq-form" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Sequence<small class="text-danger">*</small>
                                </p>
                            </div>

                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="id" name="id" class="form-control" value="{{ $faq->id }}">
                            </div>

                            <div class="valid-feedback feedback-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="invalid-feedback feedback-icon">
                                <i class="fa fa-times"></i>
                            </div>

                            
                        </div>

                        {{-- <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    FAQ Type <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="faq_type" name="faq_type" class="form-control" value="{{ $faq->faq_type }}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div> --}}

                         <!--Filter Type -->

                         <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    FAQ Type <small class="text-danger">*</small>
                                </p>
                            </div>
                            
                            <div class="col-12 col-md-8 form-group">
                                <select name="id_status" id="id_status" class="select2 form-control" style="width: 100%;">

                                

                                @if(count($statuses) > 0)
                                @foreach($statuses as $status)

                                {{-- <option value="{{ $status->id }}" selected>{{ $status->name }}</option> --}}

                                <option value="{{ $status->id }}">{{ $status->name }}</option>

                                @endforeach
                                @endif

                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                                    
                                </select>
                            </div>   
                        </div>


                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Question <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="question" name="question" class="form-control" value="{{ $faq->question }}">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Answer <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="Answer" name="Answer" class="form-control" value="">
                                <div class="valid-feedback feedback-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="invalid-feedback feedback-icon">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                        </div> -->

                        <div class="row">
                                <div class="col-12 col-md-4 text-md-right my-auto">
                                    <p>
                                        Answer <small class="text-danger">*</small>
                                    </p>
                                </div>
                                <div class="col-12 col-md-8 form-group">
                                    <textarea name="answer" id="answer" cols="30" rows="10"
                                        class="form-control summernote" value="{{ $faq->answer }}">
                                        {!! $faq->answer !!}
                                    </textarea>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    FAQ Status
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="faq_status" value="1" id="faq_status" {{ ($faq->faq_status == "1") ? "checked" : "" }}>
                                    <label class="custom-control-label" for="faq_status">Publish after saving.</label>
                                    <br>
                                    <small>Uncheck if you want the product to not be published after saving.</small>
                                </div>
                            </div>
                        </div>

                                
                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@push('script')
<script>
$('.summernote').summernote({
            height: 200, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
        });
</script>
@endpush

