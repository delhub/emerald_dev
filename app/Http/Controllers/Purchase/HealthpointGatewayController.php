<?php

namespace App\Http\Controllers\Purchase;

use App\Http\Controllers\Controller;
use App\Jobs\PDF\GeneratePdfInvoiceAndReceiptEmail;
use App\Models\Purchases\Payment;
use App\Http\Controllers\Purchase\Log;
use Illuminate\Support\Facades\Cookie;
use App\Models\Globals\Countries;
use App\Models\Purchases\RBS;
use Illuminate\Support\Facades\Session;

class HealthpointGatewayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function submitForm($request, $purchase, $user, $paymentOption)
    {
        $session = Session::get('bjs_cart_discount');



        $paymentGatewayResponse = new PaymentGatewayController;

        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        $roundedAmount = $purchase->purchase_amount / 100 / $conversion_rate_markup;

        $roundedAmount = ceil($roundedAmount);

        // dd($purchase->purchase_amount);
        $gateway = 'hp';

        if ($session) {
            $gateway = 'voucher';
        }

        // if ($user->userInfo->user_points >= $request->purchase_point) {
        // }

        $paymentGatewayResponse->sendToMarketingServer($user->userInfo->account_id, $purchase->getFormattedNumber(),  $purchase, $roundedAmount, $gateway);

        // $responseStatus = $request->getStatusCode();
        // return $request;
        // if ($responseStatus == 200) {
        // $response = json_decode($request->getBody(), true);
        $purchase->offline_reference = 0;
        // $purchase->offline_reference = $response['transactionID'];
        $purchase->offline_payment_amount = $session ? $request->finalTotal : $request->finalTotalPoint;

        $purchase->payment_type = $session ? 7004 : 7002;

        $purchase->purchase_status = 4007; // Paid

        // return $purchase;

        $purchase->save();

        $this->processBluePointPayment($purchase, $user, $gateway);

        $paymentGatewayResponse->updateGroupSales($user, $purchase);

        $check_registration = false;

        $purchase->increment('invoice_version');

        $paymentGatewayResponse->createOrderStoreRegistration($purchase, $user,  $check_registration, $paymentOption,false );

        GeneratePdfInvoiceAndReceiptEmail::dispatch($purchase, $user->email);
        // } else {

        //     $purchase->purchase_status = 4006; //Failed
        //     $response = json_decode($request->getBody(), true);

        //     $purchase->save();

        //     $errorMessage = array();
        //     $errorMessage['id'] = 0;
        //     $errorMessage['response_code'] = 'FAILED';
        //     $errorMessage['description'] = "Bluepoint Transaction Failed: " . $response['message'];


        //     return view('shop.payment.failed')->with('errorMessage', $errorMessage);
        // }

        return $paymentGatewayResponse->markOrderComplete($purchase, $user, $cookie = null);
    }

    public function processBluePointPayment($purchase, $user, $gateway)
    {

        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        $rebates = $purchase->getFeeDetails('rebate_fee');
        $auth_code = array();
        foreach ($rebates as $rebate) {
            $auth_code[] = json_decode($rebate->shipping_information)->voucher_code;
        }

        $payment = new Payment;
        $payment->purchase_number = $purchase->purchase_number;
        $payment->gateway_string_result = '-';
        $payment->gateway_response_code = strtoupper($gateway);
        $payment->transaction_id = uniqid();
        $payment->auth_code = implode(', ', $auth_code);
        $payment->last_4_card_number = '-';
        $payment->expiry_date = '-';
        $payment->amount = $purchase->purchase_amount;
        $payment->point_amount = $purchase->point_amount;
        $payment->local_amount = $payment->amount / $conversion_rate_markup;
        $payment->gateway_eci = '-';
        $payment->gateway_security_key_res = '-';
        $payment->gateway_hash = '-';
        $payment->country_id = country()->country_id;
        $payment->save();

        $paymentGatewayResponse = new PaymentGatewayController;

        $points = array();
        $points['point_type'] = 'debit';
        $points['transaction_type'] = 'purchase';
        $points['status'] = 'approved';
        $points['amount'] = '-' . $payment->point_amount;
        $points['balance'] = $user->userInfo->user_points - $payment->point_amount;

        $userPoints = $paymentGatewayResponse->saveUserPoints($payment, $points);

        $userInfo = $user->userInfo;
        $userInfo->user_points = $userPoints->balance;
        $userInfo->save();

        // $rbs = RBS::where('purchases_id',$purchase->id)->where('rbs_status','pending')->first();
        // $rbs->rbs_status = 'active';
        // $rbs->save();

    }

    public function check_hash()
    {
        return;
    }

    public function check_duplicate()
    {
        return;
    }

    public function check_status()
    {
        return;
    }
}
