<?php

use App\Models\Globals\Products\Product;
use App\Models\Products\ProductAttribute;
use App\Models\Products\WarehouseInventories;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('wh_inventories', function (Blueprint $table) {
            $table->id();
            $table->integer('outlet_id');
            $table->integer('inv_type');
            // $table->string('inv_status');
            $table->integer('inv_amount');
            $table->integer('inv_user');
            $table->string('model_type');
            $table->integer('model_id');
            $table->string('product_code');
            $table->string('inv_reference')->nullable();
            $table->timestamps();
        });

        // $attributes = ProductAttribute::all();
        // foreach ($attributes as $attribute) {

        //     DB::table('wh_inventories')->insert([
        //         'outlet_id' => 1,
        //         'inv_type' => 8010,
        //         // 'inv_status' => 'complete',
        //         'inv_amount' => $attribute->stock,
        //         'inv_user' => 2,
        //         'model_type' => 'App\Models\Products\ProductAttribute',
        //         'model_id' => $attribute->id,
        //         'product_code' => $attribute->product_code,
        //         'inv_reference' => null,
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now(),
        //     ]);
        // }

        Schema::create('wh_logs', function (Blueprint $table) {
            $table->id();
            $table->string('model_type');
            $table->integer('model_id');
            $table->string('log_status');
            $table->integer('log_user');
            $table->longText('log_data');
            $table->timestamps();
        });

        Schema::rename('warehouse_batch', 'wh_batch');
        Schema::rename('warehouse_batch_detail', 'wh_batch_detail');
        Schema::rename('warehouse_batch_item', 'wh_batch_item');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wh_inventories');
        Schema::dropIfExists('wh_logs');
        Schema::rename('wh_batch', 'warehouse_batch');
        Schema::rename('wh_batch_detail', 'warehouse_batch_detail');
        Schema::rename('wh_batch_item', 'warehouse_batch_item');
    }
}
