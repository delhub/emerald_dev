<?php

namespace App\Http\Controllers\Administrator\Countries;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Globals\Countries;

class GlobalCountriesController extends Controller
{
    public static $home;
    public static $header;

    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        self::$home = 'administrator.countries.index';
        self::$header = 'Country';

        self::$create = 'administrator.countries.create';
        self::$store = 'administrator.countries.store';

        self::$edit = 'administrator.countries.edit';
        self::$update = 'administrator.countries.update';

        self::$destroy = 'administrator.countries.destroy';

        // link to view page
        // view('administrator.countries.index');
    }


    public function index()
    {
        $tables = Countries::orderBy('country_name', 'ASC')->paginate(10);

        return view(self::$home, compact('tables', 'header'));
    }

    public function create()
    {
        return view(self::$create);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'country_id' => 'required|string|max:2',
            'country_name' => 'required',
            'country_currency' => 'required',
            'conversion_rate' => 'required',
            'mark_up' => 'required',
        ]);

        $existingdata  = Countries::where('country_name', $request->input('company_name'))->first();

        if (!$existingdata) {
            $data = new Countries;
            $data->country_id = strtoupper($request->input('country_id'));
            $data->country_name = $request->input('country_name');
            $data->country_currency = strtoupper($request->input('country_currency'));
            $data->conversion_rate = $request->input('conversion_rate');
            $data->mark_up = $request->input('mark_up');
            $data->company_name = $request->input('company_name');
            $data->company_address = $request->input('company_address');
            $data->company_reg = $request->input('company_reg');
            $data->company_phone = $request->input('company_phone');
            $data->company_email = $request->input('company_email');

            $data->save();

            return redirect()->route(self::$home)->with('success', 'submit done.');
        } else {
            return back()->with(['error' => self::$header . ' already exist  !!! ']);
        }
    }

    public function edit($id)
    {
        $data = Countries::find($id);

        return view(self::$edit)->with('data', $data);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'country_id' => 'required|string|max:2',
            'country_name' => 'required|string|max:50',
            'country_currency' => 'required|string|max:10',
            'conversion_rate' => 'required',
            'mark_up' => 'required',
        ]);

        $data = Countries::find($id);
        $data->country_id = strtoupper($request->input('country_id'));
        $data->country_name = $request->input('country_name');
        $data->country_currency = strtoupper($request->input('country_currency'));
        $data->conversion_rate = $request->input('conversion_rate');
        $data->mark_up = $request->input('mark_up');
        $data->company_name = $request->input('company_name');
        $data->company_address = $request->input('company_address');
        $data->company_reg = $request->input('company_reg');
        $data->company_phone = $request->input('company_phone');
        $data->company_email = $request->input('company_email');

        $data->save();

        return redirect()->route(self::$home)->with('success', self::$header . ' Updates');
    }

    // public function destroy($id)
    // {
    //     $data = Countries::find($id);

    //     $data->delete();

    //     return redirect()->route(self::$home)->with('success', self::$header . ' Remove');
    // }
}
