@extends('layouts.guest.main')
<div class="container-left" style="padding: 10px 10px;">
    <div class="ri-btn">
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
         class="btn gbtn r-color-btn "><img src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout"
         style="height:23px; padding:0 10px 0 0px !important;"></a>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</div>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body text-center">
                    @if (session('resent') || session()->has('resend'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    @if ((request()->input() && request()->input()['referrer_choices'] == 'no_referrer') || (request()->user() && request()->user()->userInfo->user_level == 6000) || session()->has('resend'))
                        {{ __('Before proceeding, please check your email for a verification link.') }}
                        <br>
                        {{ __('If you did not receive the email please click on the resend button') }}
                        <form action="{{ route('verification.resend') }}" method="post" class="row text-center" style="justify-content: space-around;">
                            @csrf
                            <input type="hidden" name="email" value="{{ request()->input() ? request()->input()['email'] : session()->get('resend') }}">
                            <button type="submit" class="col-5 p-1 r-btn mt-2">Resend Verification Email</button>
                        </form>
                    @else
                    {{ __('Before proceeding, please request your referral agent to verify your account.') }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
