<?php

namespace App\Models\ShippingInstallations;

use Illuminate\Database\Eloquent\Model;

class Rates extends Model
{
     // Set table
     protected $table = 'shipping_rates';

     // Set timestamps
     public $timestamps = true;

     // Set primary key
     protected $primaryKey = 'id';

     // Set mass assignable columns
     protected $fillable = [
         'zone_id',
         'category_id',
         'condition',
         'min_rates',
         'max_rates',
         'price',
         'calculation_type',
         'set_status',
         'active'
     ];

     // Cast column to another datatype.
     protected $casts = [];

}
