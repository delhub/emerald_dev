<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Administrator\Archimedes\ArchimedesController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use View;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Globals\State;
use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Users\Dealers\DealerSales;
use PDF;
use App\Http\Controllers\BujishuAPI;
use App\Models\Sidemenus\Sidemenu;
use Illuminate\Support\Facades\DB;
use App\Models\Testbanners\Testbanner;
use App\Models\Menus\Menu;
use App\Models\Categories\Category;
use App\Models\Dealers\DealerData;
use App\Models\Purchases\Purchase;
use App\Models\Users\UserInfo;
use App\Models\Dealers\DealerTask;
use App\Models\Globals\Status;
use App\Models\Purchases\Item;
use App\Models\Globals\Image;
use App\Models\Purchases\Order;
use App\Http\Controllers\Agent\AgentStockController as StockController;
use App\Models\Dealers\DealerStockLedger;
use App\Models\Globals\Products\Product;
use App\Models\Globals\City;
use App\Models\Dealers\DealerPrebuyRedemption;
use App\Models\Products\ProductAttribute;
use App\Traits\Paginate;

class AgentController extends Controller
{
    use Paginate;


    protected $bpAmount = array();
    protected $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {


        $this->middleware(function ($request, $next) {
            $this->dealer = Auth::user()->dealerInfo;

            // Return the request.
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner_county = 'MY';
        $country = country()->country_id;

        if ($country == 'SG') {
            $banner_county = 'SG';
        }
        // Get dealer account id
        $user = User::find(Auth::user()->id);
        $dealerID = $user->dealerInfo->account_id;
        $dealerRefferrer = $user->dealerInfo->referrer_id;

        // Dynamic menu
        $menusides = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $menudata = Sidemenu::all();

        $menus = Sidemenu::where('menu_parent', '=', 0)->get();
        $menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $allMenus = Sidemenu::pluck('menu_title', 'id')->all();

        // Get today sales for dealer
        $todaySales = DealerSales::where('account_id', $dealerID)
            ->whereDate('created_at', Carbon::today())
            ->sum('order_amount');

        // Get current month
        $month = Carbon::now()->format('m');

        // Declare Carbon
        $announcementYear = 2020;
        $announcementMonth = 9;
        $announcementDay = 9;
        $announcementHour = 11;
        $announcementMinute = 30;
        $announcementSecond = 15;
        $tz = 'Asia/Kuala_Lumpur';
        $timeCarbon = Carbon::create($announcementYear, $announcementMonth, $announcementDay, $announcementHour, $announcementMinute, $announcementSecond, $tz)->diffForHumans();
        //
        $announcementYear2 = 2020;
        $announcementMonth2 = 9;
        $announcementDay2 = 2;
        $announcementHour2 = 11;
        $announcementMinute2 = 30;
        $announcementSecond2 = 15;
        $tz = 'Asia/Kuala_Lumpur';
        $timeCarbon2 = Carbon::create($announcementYear2, $announcementMonth2, $announcementDay2, $announcementHour2, $announcementMinute2, $announcementSecond2, $tz)->diffForHumans();
        //
        $announcementYear3 = 2020;
        $announcementMonth3 = 7;
        $announcementDay3 = 9;
        $announcementHour3 = 11;
        $announcementMinute3 = 30;
        $announcementSecond3 = 15;
        $tz = 'Asia/Kuala_Lumpur';
        $timeCarbon3 = Carbon::create($announcementYear3, $announcementMonth3, $announcementDay3, $announcementHour3, $announcementMinute3, $announcementSecond3, $tz)->diffForHumans();

        //Get monthly sales for panel
        $monthlySales = DealerSales::where('account_id', $dealerID)
            ->whereMonth('created_at', $month)
            ->sum('order_amount');

        $totalSales = DealerSales::count();

        // BP Usage

        $bpUsable =  0;
        $bpTotal =  0;


        // Agent banner
        $agentBanner = Testbanner::orderBy('id', 'desc')->get();
        // Agent Categories Sales Report Bar Chart
        // $categoryListtwo = $dealerID;

        $parent_category = Category::where('slug', 'agent-report')->first();
        $parent_category = (isset($parent_category)) ? $parent_category->id : 0;

        $report_category =  Category::where('parent_category_id', $parent_category)->get();

        $categoryNames = array();

        foreach ($report_category as $_reportcat) {

            $categoryNames[$_reportcat->id]['catName'] = $_reportcat->name;
            $categoryNames[$_reportcat->id]['product_name'] = array();
            $categoryNames[$_reportcat->id]['totalSum'] = 0;
            $categoryNames[$_reportcat->id]['totalqlt'] = 0;
            $categoryNames[$_reportcat->id]['item'] = array();
        }

        $agent_downline = DB::select(DB::raw("SELECT a.account_id, a.full_name as agent_name
        FROM dealer_infos a
        JOIN dealer_country b ON a.account_id = b.account_id
        WHERE a.referrer_id = $dealerID
        AND b.country_id = '{$country}' "));

        $downline_items = array();
        foreach ($agent_downline as $downline) {

            $downline_items[] = $downline->account_id;
        }
        $downline_items = implode(",", $downline_items);
        if (!$downline_items) $downline_items = 0;

        $_categoryNames = DB::select(DB::raw("SELECT SUM(d.subtotal_price / 100) AS totalSum,SUM(d.quantity) AS totalqlt, a.name AS catName, a.id AS id, a.parent_category_id AS parent_categories, d.product_code
        FROM categories a
        INNER JOIN piv_category_product b ON a.id = b.category_id
        INNER JOIN panel_products c ON c.global_product_id = b.product_id
        INNER JOIN items d ON c.id = d.product_id
        INNER JOIN orders e ON d.order_number = e.order_number
        INNER JOIN purchases f ON e.purchase_id = f.id
        WHERE a.parent_category_id = {$parent_category} AND
        d.item_order_status IN (1001,1002,1003,1005) AND
        f.user_id IN (SELECT g.user_id
        FROM user_infos g
        WHERE g.referrer_id IN ($dealerID , $downline_items))
        AND (
        month(str_to_date(f.purchase_date, '%d/%m/%Y')) =  MONTH(CURDATE()) AND
        year(str_to_date(f.purchase_date, '%d/%m/%Y')) =  year(CURDATE())
        )
        AND f.country_id = '{$country}'
        GROUP BY a.id,d.product_code
        "));

        foreach ($_categoryNames as $_catnames) {

            $categoryNames[$_catnames->id];
            $categoryNames[$_catnames->id]['product_name'][] = $_catnames->product_code;
            $categoryNames[$_catnames->id]['totalSum'] += $_catnames->totalSum;
            $categoryNames[$_catnames->id]['totalqlt'] += $_catnames->totalqlt;
            $categoryNames[$_catnames->id]['item'][] = $_catnames;
        }

        // 2020 New get personal sale day, month, year start

        $_agentTop  = DB::select(DB::raw("SELECT *
        FROM view_dealer_psales_curr_mth v
        WHERE v.account_id IN ($downline_items)
        AND v.country_id = '{$country}'
        ORDER BY v.totAmount DESC
        LIMIT 10
        "));

        $agentTop = $_agentTopArray = array();
        foreach ($_agentTop as $_agentTp) {
            $_agentTopArray[$_agentTp->account_id] = $_agentTp->totAmount;
        }

        foreach ($agent_downline as $downline_item) {
            if (isset($_agentTopArray[$downline_item->account_id])) {
                $downline_item->totAmount = $_agentTopArray[$downline_item->account_id];
            } else {
                $downline_item->totAmount = 0;
            }
            $agentTop[] = $downline_item;
        }
        usort($agentTop, function ($a, $b) {
            return strcmp($b->totAmount, $a->totAmount);
        });

        // // Agent group month total sale
        // $groupTotalM = DB::select(DB::raw("SELECT SUM(v.totAmount) AS grouptot
        // FROM view_dealer_psales_curr_mth v  WHERE v.account_id IN ($dealerID , $downline_items) AND v.country_id = '{$country}'
        // "));

        // // Agent group week total sale
        // $groupTotalW = DB::select(DB::raw("SELECT SUM(v.totAmount) AS grouptot
        // FROM view_dealer_psales_curr_wk v  WHERE v.account_id IN ($dealerID ,$downline_items) AND v.country_id = '{$country}'
        // "));

        // // Agent group day total sale
        // $groupTotalD = DB::select(DB::raw("SELECT SUM(v.totAmount) AS grouptot
        // FROM view_dealer_psales_curr_day v  WHERE v.account_id IN ($dealerID , $downline_items) AND v.country_id = '{$country}'
        // "));

        // // Personal Get month
        // $personalSaleM = DB::select(DB::raw("SELECT * FROM view_dealer_psales_curr_mth v WHERE v.account_id = {$dealerID} AND v.country_id = '{$country}'"));
        // // Personal Get Week
        // $personalSaleW = DB::select(DB::raw("SELECT * FROM view_dealer_psales_curr_wk v WHERE v.account_id = {$dealerID} AND v.country_id = '{$country}'"));

        // // Personal Get Day
        // $personalSaleD = DB::select(DB::raw("SELECT * FROM view_dealer_psales_curr_day v WHERE v.account_id = {$dealerID} AND v.country_id = '{$country}'"));


        /*********************  Sales  **************************************/
        $startOfDay = Carbon::now()->startOfDay();
        $endOfDay = Carbon::now()->endOfDay();

        $oneWeekBefore = Carbon::now()->subDays(7)->startOfDay();
        // $startOfWeek = Carbon::now()->startOfWeek();
        // $endOfWeek = Carbon::now()->endOfWeek();

        $firstOfMonth = Carbon::now()->firstOfMonth();
        $endOfMonth = Carbon::now()->endOfMonth();

        // //Personal
        $personalSaleD = $this->salesQuery($startOfDay, $endOfDay)
            ->where('dealer_infos.account_id', $dealerID)
            ->sum('purchases.purchase_amount');

        $personalSaleW = $this->salesQuery($oneWeekBefore, $endOfDay)
            ->where('dealer_infos.account_id', $dealerID)
            ->sum('purchases.purchase_amount');

        $personalSaleM = $this->salesQuery($firstOfMonth, $endOfMonth)
            ->where('dealer_infos.account_id', $dealerID)
            ->sum('purchases.purchase_amount');


        // // Group
        $groupTotalD = $this->salesQuery($startOfDay, $endOfDay)
            ->whereIn('dealer_infos.account_id', [$dealerID, $downline_items])
            ->sum('purchases.purchase_amount');

        $groupTotalW = $this->salesQuery($oneWeekBefore, $endOfDay)
            ->whereIn('dealer_infos.account_id', [$dealerID, $downline_items])
            ->sum('purchases.purchase_amount');

        $groupTotalM = $this->salesQuery($firstOfMonth, $endOfMonth)
            ->whereIn('dealer_infos.account_id', [$dealerID, $downline_items])
            ->sum('purchases.purchase_amount');

        $today = Carbon::now()->format('d/m/Y');
        $sevenDaysBefore = Carbon::now()->subDays(7)->startOfDay()->format('d/m/Y');
        $month = Carbon::now()->format('F Y');
        /*********************  End Sales  **************************************/


        // dd($categoryNames);

        return view('agent.index', compact([
            // return view('agent.new', compact([

            'todaySales', 'monthlySales', 'totalSales', 'bpUsable', 'bpTotal', 'timeCarbon', 'timeCarbon2', 'timeCarbon3', 'categoryNames', 'agentBanner',
            'personalSaleM', 'personalSaleW', 'personalSaleD', 'groupTotalM', 'groupTotalM', 'groupTotalW', 'groupTotalD', 'user'
        ]))
            ->with('banner_county', $banner_county)
            ->with('menudata', $menudata)
            ->with('menusides', $menusides)
            ->with('allMenus', $allMenus)
            ->with('menus', $menus)
            ->with('country', $country)
            ->with('agentTop', $agentTop)
            ->with('categoryNames', $categoryNames)

            ->with('personalSaleM', $personalSaleM)
            ->with('personalSaleW', $personalSaleW)
            ->with('personalSaleD', $personalSaleD)
            ->with('groupTotalM', $groupTotalM)
            ->with('groupTotalW', $groupTotalW)
            ->with('groupTotalD', $groupTotalD)
            ->with('today', $today)
            ->with('sevenDaysBefore', $sevenDaysBefore)
            ->with('month', $month);
    }

    public function salesQuery($startDate, $endDate)
    {
        return DealerInfo::join('user_infos', 'user_infos.referrer_id', '=', 'dealer_infos.account_id')
            ->join('purchases', 'user_infos.user_id', '=', 'purchases.user_id')
            ->whereBetween('purchases.created_at', [$startDate, $endDate])
            ->whereIn('purchases.purchase_status', [3001, 3002, 3003, 4000, 4002])
            // ->groupBy('purchases.country_id', 'dealer_infos.account_id')
            ->where('country_id', country()->country_id);
    }
    public function customerVerification()
    {
        // $bp = $this->bpTopbar();
        // $bpUsable = $bp['usable'];
        // $bpTotal = $bp['total'];

        $user = User::find(Auth::user()->id);
        $dealerID = $user->dealerInfo->account_id;

        $agentDownLineUser = User::select('users.*', 'user_infos.full_name', 'users.email', 'users.created_at', 'user_country.country_id')
            ->join('user_infos', 'user_infos.user_id', '=', 'users.id')
            ->join('user_country', 'user_country.account_id', '=', 'user_infos.account_id')
            ->where('user_infos.referrer_id', $dealerID)
            ->where('users.email_verified_at', NULL)
            ->whereNotIn('user_infos.account_status', [9])
            ->where('user_country.country_id', country()->country_id)
            ->orderby('created_at', 'DESC')
            ->get();

        return view('agent.customerVerification')
            // ->with('bpUsable', $bpUsable)
            // ->with('bpTotal', $bpTotal)
            ->with('agentDownLineUser', $agentDownLineUser);
    }

    public function customerVerificationAction(Request $request)
    {
        $user = User::find($request->input('user_id'));

        if ($request->input('action') == 'approve') {

            $user->email_verified_at = Carbon::now();

            $user->save();

            return back()->with(['successful_message' => 'Approved ' . $user->userInfo->full_name . ' ']);
        } elseif ($request->input('action') == 'disapprove') {

            $reason = $request->input('reason');

            if ($reason == NULL) return back()->with(['error_message' => 'Please give a reason!']);

            $user->email = $user->email . '-disqualify';
            $user->notes = Carbon::now()->format('d/m/Y') . ' , Reason : ' . $reason;

            $user->save();


            $userInfo = $user->userInfo;

            $userInfo->account_status = '9';

            $userInfo->save();

            return back()->with(['successful_message' => 'Disapproved ' . $user->userInfo->full_name . ' ']);
        }
    }

    // agent Monthly Statement
    public function monthlyStatement($agent, $id)
    {
        $user = User::find(Auth::user()->id);
        $dealer = $user->dealerInfo;
        $state = State::all();
        $dealerID = $dealer->account_id;
        if ($agent != $dealerID) return abort(404);
        $dealerAddress = $this->dealerAddress($dealer->billingAddress);

        $page = ($id) ? $id : 'current';
        // $dealerContact = $dealer->dealerMobileContact;
        // json agent API
        $responses = BujishuApi::get_monthly_statement($agent, $page);

        //return $pdf = view('documents.statement.agent-monthly-statement', compact(['responses', 'user', 'dealer', 'dealerID', 'dealerAddress', 'stateName', 'dealerContact']));

        return $pdf = PDF::loadView('documents.statement.agent-monthly-statement', compact(['responses', 'user', 'dealer', 'dealerID', 'dealerAddress', 'stateName', 'dealerContact']))
            ->setPaper('a4')
            ->stream('monthly-statement');
    }

    public function agentComission()
    {
        return $this->commssion();
    }

    public function agentComissionBatch($id)
    {
        return $this->commssion($id);
    }

    public function commssion($id = 0)
    // agent monthly statement
    {

        // BP Usage
        $page = ($id) ? $id : 'current';
        $user = User::find(Auth::user()->id);
        $dealer = $user->dealerInfo;

        $dealerID = $dealer->account_id;

        $dealerAddress = $this->dealerAddress($dealer->billingAddress);

        $responses = BujishuApi::get_monthly_statement($dealerID, $page);
        if ((!$responses) || ($responses['date'] == '1 January 1970')) return view('agent.no-data');
        return view('agent.monthly-income', compact('dealerID', 'dealer', 'dealerAddress', 'responses', 'id'));
    }

    function dealerAddress($address)
    {

        $dealerAddress = array();

        $dealerAddress['address_1'] = $address->address_1;
        $dealerAddress['address_2'] = $address->address_2;
        $dealerAddress['address_3'] = $address->address_3;

        if ($address->city_key == '0') {
            $city = $address->city;
        } else {
            $cityTable = City::where('city_key', $address->city_key)->first();
            $city = $cityTable ? $cityTable->city_name : '';
        }

        $stateTable = State::where('id', $address->state_id)->first();
        $state = $stateTable ? $stateTable->name : '';

        $dealerAddress['city'] = $city;
        $dealerAddress['state'] = $state;
        $dealerAddress['postcode'] = $address->postcode;

        return $dealerAddress;
    }

    public function income()
    { // Agent Yearly Income

        $user = User::find(Auth::user()->id);
        $dealer = DealerInfo::find(Auth::user()->id);

        $dealerID =  $dealer->account_id; // HARDCODE AGENT ID

        $yearly_data = BujishuApi::get_yearly_available($dealerID);
         // $yearly_data = array(2022 => "2022", 2021 => "2021"); // Test send multiple year
        if ((!$yearly_data)) return view('agent.no-data');
        return view('agent.yearly-income', compact(['dealerID', 'yearly_data']));
    }

    public function membershipRegistration()
    {
        $user = User::find(Auth::user()->id);
        $dealer = DealerInfo::find(Auth::user()->id);
        return view('agent.membership-registration')
            ->with('dealer', $dealer);
    }


    /**
     * Agent Announcement
     *
     *
     */
    public function announcements()
    {
        return view('agent.announcements');
    }

    /* Agent dashboard add controller */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admincontrol()
    {

        $categoryList = Category::all()->toArray();
        // Bar Chart

        $categoryNames = DB::select(DB::raw("SELECT SUM(d.subtotal_price) AS totalSum,SUM(d.quantity) AS totalqlt, a.name AS catName, a.id AS id, a.parent_category_id AS parent_categories, d.product_code AS productName
        FROM categories a
        INNER JOIN piv_category_product b ON a.id = b.category_id
        INNER JOIN panel_products c ON c.global_product_id = b.product_id
        INNER JOIN items d ON c.id = d.product_id
        INNER JOIN orders e ON d.order_number = e.order_number
        INNER JOIN purchases f ON e.purchase_id = f.id
        WHERE a.parent_category_id = 103 AND
        d.item_order_status IN (1001,1002,1003) AND
        f.user_id IN (SELECT g.user_id
        FROM user_infos g
        WHERE g.referrer_id = 1911000140)
        GROUP BY a.id,d.product_code
        LIMIT 20;
        "));

        // $categoryList1 = DB::table('categories')->get('id');

        // Return View
        return view('administrator.agent.index', compact('categoryNames', 'categoryList'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public  function specialTask()
    {
        $bp = $this->bpTopbar();
        $bpUsable = $bp['usable'];
        $bpTotal = $bp['total'];
        $specialAgent = $summaryArchimedes = null;
        $customer = null;
        $taskLists = array();
        $imageable_ids = array();

        $statusLabel = Status::all();
        $parentProduct = Product::all();
        $productAttribute = ProductAttribute::get();

        $itemTable = Item::get();
        $agent_id = DealerInfo::where('user_id', Auth::user()->id)->first()->account_id;
        $vqsProducts = Product::where('product_type', 'vqs')->get();
        $product_code = array();
        foreach ($vqsProducts as $vqsProduct) {
            foreach ($vqsProduct->panelProduct->attributes as $attributes) {
                foreach ($attributes->productBundle as $productBundle) {
                    if ($productBundle->active != 0) {
                        $product_code[] = $productBundle->primary_product_code;
                    }
                }
            }
        }
        $specialAgent = DealerData::getAllAgentData($agent_id);

        if ($specialAgent != '0') {
            // only product that agent have bought
            $selectedProduct = $specialAgent->pluck('name')->toArray();
            $today = Carbon::now();
            if ($today->copy()->format('d') < 3) {
                //last month 3rd
                $from = Carbon::now()->startOfMonth()->addDays(2)->subMonth()->toDateString();
                //this month 3rd
                $to = $today->toDateString();
            } else {
                //this month 3rd
                $from =  $today->copy()->firstOfMonth()->addDays(2);
                //end of this month
                $to =  $today->copy()->endOfMonth();
            }

            $taskLists = DealerTask::where('referrer_id', $agent_id)->whereIn('product_code', $selectedProduct)->whereBetween('created_at', [$from, $to])->get()->sortByDesc('created_at');


            $archimedesController = new ArchimedesController;
            $taskLists =  $archimedesController->paginate($taskLists, 5);

            $taskLists->setPath('');
            // $taskLists->pluck('primary_product_code');

            // Summary Details
            $summaryArchimedes = StockController::agentProductStock($product_code, $agent_id);
        }
        if ($taskLists) {
            foreach ($taskLists as $taskList) {
                $imageable_ids[] = $taskList->global_product_id;
            }
        }

        $images = ($imageable_ids) ? Image::where('imageable_id', $imageable_ids)->get() : null;
        $productImage = Image::all();

        $dealerDatas = DealerData::where('dealer_id', $agent_id)
            ->where('type', 'vqs')
            ->get();

        return view('agent.agent-task', compact(['bpUsable', 'bpTotal', 'specialAgent', 'taskLists', 'statusLabel', 'images', 'summaryArchimedes', 'parentProduct', 'productAttribute', 'itemTable', 'productImage', 'dealerDatas']));
    }

    public function createDealerPreBuyRedemption(Request $request)
    {
        $id = $request->input('action_ids');
        $arrayid = explode(",", $id);

        $items = Item::whereIn('id', $arrayid)->get();
        if ($items->isEmpty()) return back()->with('error_message', 'Please select product to redeem.');

        $agent_id = DealerInfo::where('user_id', Auth::user()->id)->first()->account_id;
        $channel = 'website';
        $status_type = 5006;


        $dealerDatas = DealerData::where('dealer_id', $agent_id)
            ->where('type', 'vqs')
            ->get();

        foreach ($dealerDatas as $dealerData) {
            if (isset($dealerData->name)) {

                if (!isset($agentProduct[$dealerData->name])) {
                    $agentProduct[$dealerData->name]['quantity'] = 0;
                }
                $agentProduct[$dealerData->name]['quantity'] += $dealerData->value;
            }
        }

        //checking
        $totalAmount = 0;
        $can = 1;
        foreach ($items as $key => $item) {
            if (!isset($selectProduct[$item->product_code])) {
                $selectProduct[$item->product_code]['item_id'] = 0;
                $selectProduct[$item->product_code]['quantity'] = 0;
                $selectProduct[$item->product_code]['subtotal_price'] = 0;
                $selectProduct[$item->product_code]['product_name'] = null;
            }
            $selectProduct[$item->product_code]['item_id'] = $item->id;
            $selectProduct[$item->product_code]['quantity'] += $item->quantity;
            $selectProduct[$item->product_code]['subtotal_price'] += $item->subtotal_price;
            $selectProduct[$item->product_code]['product_name'] = $item->product->parentProduct->name;

            $totalAmount += $item->subtotal_price;
            $arrayID[] = $item->id;

            if (!isset($agentProduct[$item->product_code])) $agentProduct[$item->product_code]['quantity'] = 0;

            if ($agentProduct[$item->product_code]['quantity'] < $selectProduct[$item->product_code]['quantity']) {
                $can = 0;
                return back()->with('error_message', '' . $selectProduct[$item->product_code]['product_name'] . ' redemption allow : ' . $agentProduct[$item->product_code]['quantity'] . ' quantity. Selected : ' . $selectProduct[$item->product_code]['quantity'] . 'quantity.');
            }
        }

        if ($request->input('goSummaryPage') == 'yeah') {
            $parentProduct = Product::all();
            $productImage = Image::all();
            $itemTable = Item::get();
            $productAttribute = ProductAttribute::get();

            $successful_message = 'Success, please double check and click confirm button';
            return view('agent.agent-task-post-summary', compact(['selectProduct', 'arrayID', 'totalAmount', 'parentProduct', 'productImage', 'itemTable', 'productAttribute', 'successful_message']));
        }

        //if correct and pass then create
        if ($request->input('create') == 'yeah') {
            $dealerPrebuy = new DealerPrebuyRedemption;

            $dealerPrebuy->dealer_id = $agent_id;
            $dealerPrebuy->channel = $channel;
            $dealerPrebuy->prebuy_status = $status_type;
            $dealerPrebuy->total_amount = $totalAmount;
            $dealerPrebuy->redemption_data = json_encode($arrayid);

            $dealerPrebuy->save();

            $request->request->set('create', null);

            foreach ($items as $key => $item) {
                $item->prebuy_redemption = 1;
                $item->save();
            }
            foreach ($selectProduct as $productCode => $quantity) {
                $theDealerData = DealerData::where('dealer_id', $agent_id)
                    ->where('type', 'vqs')
                    ->where('name', $productCode)
                    ->first();

                $existingQty = (int)$theDealerData->value;
                $theDealerData->value = ($existingQty - $quantity['quantity']);
                $theDealerData->save();
            }
        }


        //dealer_data quantity minus update
        return redirect()->route('agent.virtual.quantity.stock')->with('successful_message', 'Succeefully redemption, please wait admin to approve.');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bpTopbar()
    {
        $bp = array();
        $bp['usable'] = 0;
        $bp['total'] = 0;

        return $bp;
    }

    public function statusUpdate(Request $request)
    {


        $itemStatus = Item::where('id', $request->item_table_id)->first();
        $orderStatus = Order::where('delivery_order', $request->delivery_order)->first();

        if ($itemStatus->item_order_status == 1001) {

            if ($request->submit == "deliver") {

                $itemStatus->item_order_status = 1008;
            } else if ($request->submit == "self_deliver") {

                $itemStatus->item_order_status = 1011;
            }

            $itemStatus->actual_ship_date = Carbon::now();

            $itemStatus->save();

            StockController::recordSales($itemStatus->id);
        }

        if ($orderStatus->order_status == 1001) {

            if ($request->submit == "deliver") {

                $orderStatus->order_status = 1008;
            } else if ($request->submit == "self_deliver") {

                $orderStatus->order_status = 1011;
            }

            $orderStatus->save();
        }

        return redirect()->back();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vqs(Request $request)
    {
        return view('agent.virtual-quantity-stock');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $categoryList = DB::table('categories')->find($id);

        // $categoryList->active = ($request->input('category_status') != 1) ? 0 : 1;

        // $categoryList->save();


        // return redirect('/administrator/agent/')->with('success', 'Chart Updates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function cp58Statement(Request $request, $year)
    {
        $dealer = $this->dealer;
        $address = '';
        $_address = $dealer->billingAddress;

        if ($_address) {
            $lines = array();
            $lines[] = rtrim($_address->address_1, ' , ');
            if ($_address->address_2) $lines[] = rtrim($_address->address_2, ' , ');
            if ($_address->address_3) $lines[] = rtrim($_address->address_3, ' , ');
            if ($_address->city_label) $lines[] =  $_address->postcode . ' ' . $_address->city_label;
            if ($_address->state_label) $lines[] = $_address->state_label;
            $address = implode(', ', $lines) . '.';
        }
        $data = BujishuApi::get_yearly_statement($dealer->account_id, $year);
        // return View('documents.statement.cp-cukai', compact('dealer', 'address', 'data'));
        $pdf = PDF::loadView('documents.statement.cp-cukai', compact('dealer', 'address', 'data'));
        return $pdf->setPaper('a4')->stream('CP58_form' . '.pdf');
    }

    public function yearlyStatement(Request $request, $year)
    {
        // dd('here');

        $dealer = $this->dealer;
        $dealerAddress = array();
        $_address = $dealer->billingAddress;

        if ($_address) {
            $lines = array();
            $lines[] = $_address->address_1 . ',';
            if ($_address->address_2) $lines[] = $_address->address_2 . ',';
            if ($_address->address_3) $lines[] = $_address->address_3 . ',';
            if ($_address->city_label) $lines[] =  $_address->postcode . ' ' . $_address->city_label . ',';
            if ($_address->state_label) $lines[] = $_address->state_label . '.';
            $address = $lines;
        }

        $data = BujishuApi::get_yearly_statement($dealer->account_id, $year);
        return PDF::loadView('documents.statement.yearly-statement', compact('data', 'dealer', 'address'))
            ->setPaper('a4')
            ->stream('yearly-statement');
    }
    public static function metaHead($string)
    {

        $render = '';

        foreach (str_split($string) as $char) $render .= "<td class='meta-head'>{$char}</td>";

        return $render;
    }
    public static function giveSpace($num)
    {
        $space = '';
        $count = 0;
        while ($count < $num) {
            $count++;
            $space .= '&nbsp;';
        }
        return $space;
    }

    public static function stateFilterAgentCity(Request $request)
    {
        $state_id = 0;

        if ($request->input('state_id') != NULL) {
            $state_id = $request->input('state_id');
        } elseif ($request->input('dealer_company_state') != NULL) {
            $state_id = $request->input('dealer_company_state');
        }
        $cities = City::where('state_id', $state_id)->get();

        foreach ($cities as  $city) {
            $City[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $City[] = '<option value="0" "selected">Others</option>';

        return $City;
    }

    public function agentProduct()
    {
        //
        return redirect()->route('shop.category.first', ['topLevelCategorySlug' => 'agent-package-product']);
    }
}
