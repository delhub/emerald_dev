<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapDevelopmentRoutes();

        $this->mapWarehouseRoutes();

        $this->mapExportRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "developement" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapDevelopmentRoutes()
    {
        Route::prefix('development')
            ->middleware(['web', 'role:administrator'])
            ->namespace($this->namespace)
            ->group(base_path('routes/development.php'));
    }

    protected function mapWarehouseRoutes()
    {
        Route::prefix('warehouse')
            ->namespace($this->namespace)
            ->group(base_path('routes/warehouse.php'));
    }

    protected function mapExportRoutes()
    {
        Route::prefix('administrator/export')
             ->middleware(['web','role:export_manager|account_manager|administrator|formula_pic'])
             ->namespace($this->namespace)
             ->group(base_path('routes/export.php'));
    }
}
