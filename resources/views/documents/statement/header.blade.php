<!DOCTYPE html>
<html>

<head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+SC:wght@200;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/monthly-statement.css') }}">
     @include('layouts.favicon')

</head>

<body class="topheader" onload="subst()">
    <table class="header">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr class="cpn-address">
                                    <td width="35%">
                                        <img class="cpn-logo" src="{{ asset('images/formula/post-img/logo.png') }}">
                                    </td>
                                    <td width="65%">
                                        <p>Formula Healthcare Sdn. Bhd.<span>(1318429-V)</p>
                                        <p>1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                        <p>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                        <table>
                                            <tr>
                                                <td>
                                                    <p class="cpn-email">
                                                        <img
                                                            src="{{ asset('images/formula/post-img/do-c-icon1.png') }}">
                                                        formula-cs@delhubdigital.com
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="cpn-weblink">
                                                        <img
                                                            src="{{ asset('images/formula/post-img/do-c-icon3.png') }}">
                                                        formula2u.com
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="page-title">
                                        <h4>YEARLY STATEMENT {{ $data['biz_year'] }}</h4>
                                        <hr class="dbl-line">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="user-info">
                        <td width='60%'>
                            <p><strong>{{ $dealer->full_name }}</strong></p>
                               @foreach($address as $line)
                                        {{$line}} <br/>
                                @endforeach
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p>
                                            Date
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $data['date_generated'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Business Year
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $data['biz_year'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Agent Code
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $dealer->account_id }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>
                                            Grade
                                        </p>
                                    </td>
                                    <td>
                                        <span>: {{ $data['rank'] }}</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
    <!-- End Second Header -->
    </thead>
    <script>
        function subst() {
            var vars = {};
            var query_strings_from_url = document.location.search.substring(1).split('&');
            for (var query_string in query_strings_from_url) {
                if (query_strings_from_url.hasOwnProperty(query_string)) {
                    var temp_var = query_strings_from_url[query_string].split('=', 2);
                    vars[temp_var[0]] = decodeURI(temp_var[1]);
                }
            }
            var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate',
                'time', 'title', 'doctitle', 'sitepage', 'sitepages'
            ];
            for (var css_class in css_selector_classes) {
                if (css_selector_classes.hasOwnProperty(css_class)) {
                    var element = document.getElementsByClassName(css_selector_classes[css_class]);
                    for (var j = 0; j < element.length; ++j) {
                        element[j].textContent = vars[css_selector_classes[css_class]];
                    }
                }
            }
        }
    </script>

</body>
