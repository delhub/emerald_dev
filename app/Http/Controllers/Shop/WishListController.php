<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\Customers\Favorite;
use App\Models\Users\User;
use Auth;
use App\Models\Products\Product;

class WishListController extends Controller
{

    /***Add to item to perfect wishlist  ****/
    public function store($product_id)
    {
        // return $request;
        // Get user id
        $userID = Auth::user()->id;

        $perfect_list = Favorite::updateOrCreate(
            ['user_id' =>  $userID , 'product_id' => $product_id]
        );
        $perfect_list->save();

        $perfect_list =  $perfect_list->where('user_id',$userID)->where('product_id',$product_id)->first();
        $perfect_list->update();

        return back();

        // if ($perfect_list) {
        //     return back()->with(['successful_message' => 'Added to perfect list ']);
        // } else {
        //     return back()->with(['error_message' => 'Please try again']);
        // }
    }


    // Get quantity of perfect list

    public function getQuantity($id){

        $user = User::find($id);
        $perfectListQuantity = $user->favorites->where('user_id', $id)->count();

        $data['status'] = 'OK';
        $data['message'] = 'Get request successful.';
        $data['data'] = $perfectListQuantity;

        return response()->json($data, 200);

    }


    //Remove the specified item from storage.
    public function destroy($product_id) {
        // $userID = Auth::user()->id;
        // $perfectList = new Favorite;
       // $perfectList = $perfectList->where('user_id',$userID)->where('product_id',$product_id)->take(1);
       Favorite::where('user_id', Auth::id())->where('product_id', $product_id)->firstOrFail()->delete();
       return back();
    }

    public function ajaxToggle(Request $request){
  
       $product_id = $request->id;

        if (!Auth::id() || !$product_id) return response()->json(['message' => "Not allowed"], 500);

        $wishlist = Favorite::firstOrNew(['user_id' => Auth::id(), 'product_id' => $product_id]);

        if ($wishlist->exists){
            $liked = false;
            $wishlist->delete();
        }else {
            $liked = true;
            $wishlist->save();
        }
        $product = Product::find($product_id);

        $likes = $product->likes;
        $product->total_like = $likes;
        $product->save();
        $data['status'] = 'OK';
        $data['message'] = 'Update Successful';
        $data['data'] = compact('liked', 'likes');

        return response()->json($data, 200);
    }

}
