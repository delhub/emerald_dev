<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

use App\Models\Products\ProductAttribute;


class ProductAttributeBundle extends Model
{
    // Set table
    protected $table = 'panel_attribute_bundle';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Cast column to another datatype.
    protected $casts = [];

    // Set mass assignable columns
    protected $fillable = [
        'id'
    ];

    /**
     * Get an attribute's product.
     */
    public function productAttribute()
    {
        return $this->belongsTo(ProductAttribute::class, 'primary_attribute_id');
    }

    public function parentProductAttribute()
    {
        return $this->belongsTo(ProductAttribute::class, 'attribute_id');
    }
    public function productAttMain()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_code', 'product_code');
    }

}
