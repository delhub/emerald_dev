<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGlobalPanelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->tinyInteger('show_warning')->default(1)->after('ingredients');
        });

        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->bigInteger('point')->default(0)->after('default_price');
            $table->text('point_cash')->nullable()->after('point');
            $table->integer('discount_percentage')->default(0)->after('point_cash');
            $table->bigInteger('shipping_category')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
