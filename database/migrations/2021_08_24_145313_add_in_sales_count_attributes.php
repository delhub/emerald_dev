<?php

use App\Models\Products\ProductAttribute;
use App\Models\Purchases\Item;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddInSalesCountAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('panel_product_attributes', function (Blueprint $table) {
        //     //
        // });
        // DB::table('users')
        // ->select(DB::raw('count(*) as user_count, status'))
        // ->where('status', '<>', 1)
        // ->groupBy('status')
        // ->get();
        // $items = DB::raw("SELECT SUM(quantity) AS SUM,product_code FROM items WHERE item_order_status IN ('1001', '1002', '1003') GROUP BY product_code");

        $items = Item::groupBy('product_code')
        ->selectRaw('sum(quantity) as sum, product_code')
        ->whereIn('item_order_status',[1001, 1002, 1003,1005,1010])
        ->pluck('sum','product_code');

        foreach ($items as $key => $item) {
            $attributes = ProductAttribute::where('product_code',$key)->first();
            if ($attributes) {
                $attributes->sales_count = $item;
                $attributes->save();
            }
        }



        // Item::where('product_code', $attributes->product_code)->whereIn('item_order_status', [1001, 1002, 1003])->sum('quantity');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            //
        });
    }
}
