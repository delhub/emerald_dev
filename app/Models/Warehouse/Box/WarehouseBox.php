<?php

namespace App\Models\Warehouse\Box;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;
use App\Models\Warehouse\StockTransfer\StockTransfer;

class WarehouseBox extends Model
{
    // use TableLogs;

    protected $table = 'wh_box';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [];

    public function theUser1()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'pic_1');
    }

    public function theUser2()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'pic_2');
    }

    public function theUser3()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'pic_3');
    }

    public function stockTransfer()
    {
        return $this->belongsTo(StockTransfer::class, 'using');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Warehouse\Location\Wh_location', 'location_id', 'id');
    }

    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if ($selectSearch == 'name') {

                return $query
                    ->where('box_name', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }

    public function scopeLocationFilter($query, $location_id)
    {
        if ($location_id != '0' && $location_id != NULL) {
            return $query->where('location_id', $location_id);
        }
    }
}
