<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToGlobalProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->string('moh_code',64)->nullable()->after('product_code');
            $table->text('data')->nullable()->after('tradein_rule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_products', function (Blueprint $table) {
            $table->dropColumn('moh_code');
            $table->dropColumn('data');
        });
    }
}
