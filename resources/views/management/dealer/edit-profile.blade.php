@extends('layouts.management.main-customer')

@section('content')

@section('page_title')
{{ "Welcome to Agent Profile" }}
@endsection

@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{route('shop.dashboard.dealer.profile.update',[$dealerProfile->account_id])}}" method="POST" id="formid">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-12 col-md-10">
            <div class="row col-12 mywebmenu2">
                @unlessrole('dealer')
                <div class="my-profile-opt-btn">
                    <i class="fa fa-user mr-1"></i> <a href="{{route('shop.dashboard.customer.profile.edit')}}" class="text-color-header "><strong>Edit My Profile</strong></a>
                </div>
                @endunlessrole
                @hasrole('dealer')
                <div class="my-profile-opt-btn active">
                    <i class="fa fa-address-book-o mr-1"></i> <a href="{{route('shop.dashboard.dealer.profile.edit')}}" class="text-color-header "><strong>Edit
                            Agent Profile</strong></a>
                </div>
                @endhasrole
                @hasrole('panel')
                {{-- <div class="my-profile-opt-btn">
                    <i class="fa fa-building-o mr-1"></i><a href="{{route('management.company.profile.edit')}}" class="text-color-header "><strong> Edit Panel Profile</strong></a>
                </div> --}}
                @endhasrole
            </div>
            <div class="col-12 mymobilemenu2">
                <button class="btn btn-secondary mybut3 bjsh-btn-gradient" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:rgb(250, 172, 24); color:black;">
                    Edit Agent Profile<i style="font-size: 10px;" class="fa fa-arrow-down"> </i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{route('shop.dashboard.customer.profile.edit')}}">Edit My Profile</a>
                    @hasrole('dealer')
                    <a class="dropdown-item" href="#">Edit Agent Profile</a>
                    @endhasrole
                    @hasrole('panel')
                    <a class="dropdown-item" href="{{route('management.company.profile.edit')}}">Edit Panel Profile</a>
                    @endhasrole
                </div>
            </div>

            @dealershipcheck
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="form-group row ">
                        <div class="col-md-4 m-0">
                            <h4>Agent Information</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row p-form-box">
                        <label for="dealer_id" class="col-md-3 col-form-label">Agent ID</label>
                        <div class="col-md-4 m-0">
                            <input type="text" name="dealer_id" id="dealer_id" value="{{ country()->country_id == "SG" ? "SG" : "" }}{{$dealerProfile->account_id}}" class="form-control @error('dealer_id') is-invalid @enderror" value="{{ old('dealer_id') }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="gender" class="col-md-3 col-form-label">Gender</label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="gender" id="gender" @if($dealerProfile->gender_id===1)
                            value="Male"
                            @else
                            value="Female"
                            @endif
                            class="form-control @error('gender') is-invalid @enderror"
                            value="{{ old('gender') }}" readonly>
                        </div>
                        @error('gender')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="marital_id" class="col-md-3 col-form-label">Marital Status</label>
                        <div class="col-md-9">
                            <select name="marital_id" id="marital_id" class="form-control text-capitalize">
                                @foreach($maritals as $marital)
                                <option value="{{ $marital->id }}" {{ ($marital->id == $dealerProfile->marital_id)? 'selected' : '' }}>
                                    {{ $marital->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('marital_id')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <div class="col-md-4 m-0 mt-md-2">
                            <h4>Employment Details</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row p-form-box">
                        <label for="dealer_company_name" class="col-md-3 col-form-label">Company Name <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="dealer_company_name" id="dealer_company_name" value="{{($dealerProfile->employmentAddress) ? $dealerProfile->employmentAddress->company_name : '' }}" class="form-control @error('dealer_company_name') is-invalid @enderror" value="{{ old('dealer_company_name') }}">
                        </div>
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="dealer_company_address_1" class="col-md-3 col-form-label">Company Address <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="dealer_company_address_1" id="dealer_company_address_1" value="{{($dealerProfile->employmentAddress) ? $dealerProfile->employmentAddress->company_address_1 : '' }} " class="form-control @error('dealer_company_address_1') is-invalid @enderror" value="{{ old('dealer_company_address_1') }}">
                        </div>
                        @error('dealer_company_address_1')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row pbox-2 p-form-box">
                        <div class="pbox-2 offset-md-3 col-md-9">
                            <input type="text" name="dealer_company_address_2" id="dealer_company_address_2" value="{{($dealerProfile->employmentAddress) ? $dealerProfile->employmentAddress->company_address_2 : '' }}" class="form-control @error('dealer_company_address_2') is-invalid @enderror" value="{{ old('dealer_company_address_2') }}">
                        </div>
                        @error('dealer_company_address_2')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group pbox-2 row p-form-box">
                        <div class="offset-md-3 col-md-9">
                            <input type="text" name="dealer_company_address_3" id="dealer_company_address_3" value="{{($dealerProfile->employmentAddress) ? $dealerProfile->employmentAddress->company_address_3 : '' }}" class="form-control @error('dealer_company_address_3') is-invalid @enderror" value="{{ old('dealer_company_address_3') }}">
                        </div>
                        @error('dealer_company_address_3')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="company_city" class="col-md-3 col-form-label">Company City</label>
                        <div class="col-md-4 m-0 p-pb">
                            <select name="dealer_company_city" id="dealer_company_city"  class="form-control text-capitalize" style="width: 100%; @error('dealer_company_city') is-invalid @enderror" value="{{ old('dealer_company_city') }}">
                                @foreach($cities as $city)
                                    <option value="{{ $city->city_key }}" {{ ($city->city_key == $dealer_company_city) ? 'selected' : '' }}>{{ $city->city_name }}</option>
                                @endforeach
                                    <option value="0" {{ ($dealer_company_city  == '0') ? 'selected' : '' }}>Others</option>
                            </select>
                        </div>
                        <div class="col-md-4 m-0 p-e-city p-form-box">
                            <input type="text" id="company_city" name="company_city"
                            value="{{(isset($user->dealerInfo->employmentAddress->company_city)) ? $user->dealerInfo->employmentAddress->company_city : ''}}"
                            placeholder="Entry Your City Here">
                            @error('company_city')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror

                        </div>
                      </div>
                    <div class="form-group row p-form-box p-form-box">
                        <label for="dealer_company_postcode" class="col-md-3 col-form-label">Company Postcode <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="dealer_company_postcode" id="dealer_company_postcode" value="{{($dealerProfile->employmentAddress) ? $dealerProfile->employmentAddress->company_postcode : '' }}" 
                            class="form-control @error('dealer_company_postcode') is-invalid @enderror" value="{{ old('dealer_company_postcode') }}">
                        </div>
                        @error('dealer_company_postcode')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="dealer_company_state" class="col-md-3 col-form-label"> Company State <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                        <select name="dealer_company_state" id="dealer_company_state" class="form-control text-capitalize">
                            <option disabled selected>Choose your state..</option>
                            @foreach($states as $state)
                            <option value="{{ $state->id}}" {{ ($state->id == $stateId)? 'selected' : '' }}>{{$state->name}}</option>
                                @endforeach
                            </select>
                            </div>
                            @error('dealer_company_state')
                            <small class="form-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    

                    <div class="form-group row p-form-box">
                        <div class="col-md-4  m-0 mt-md-2">
                            <h4>Spouse Information</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row p-form-box">
                        <label for="spouse_name" class="col-md-3 col-form-label">Spouse Name</label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="spouse_name" id="spouse_name" value="{{($dealerProfile->dealerSpouse) ? $dealerProfile->dealerSpouse->spouse_name : '' }}" class="form-control @error('spouse_name') is-invalid @enderror" value="{{ old('spouse_name') }}">
                        </div>
                        @error('spouse_name')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="spouse_occupation" class="col-md-3 col-form-label">Spouse Occupation</label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="spouse_occupation" id="spouse_occupation" value="{{($dealerProfile->dealerSpouse) ? $dealerProfile->dealerSpouse->spouse_occupation : '' }}" class="form-control @error('spouse_occupation') is-invalid @enderror" value="{{ old('spouse_occupation') }}">
                        </div>
                        @error('spouse_occupation')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="spouse_contact" class="col-md-3 col-form-label">Spouse Contact</label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="spouse_contact" id="spouse_contact" value="{{($dealerProfile->dealerSpouse) ? $dealerProfile->dealerSpouse->spouse_contact_mobile : '' }}" class="form-control @error('spouse_contact') is-invalid @enderror" value="{{ old('spouse_contact') }}">
                        </div>
                        @error('spouse_contact')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="spouse_email" class="col-md-3 col-form-label">Spouse Email</label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="spouse_email" id="spouse_email" value="{{ ($dealerProfile->dealerSpouse) ? $dealerProfile->dealerSpouse->spouse_email : '' }}" class="form-control @error('spouse_email') is-invalid @enderror" value="{{ old('spouse_email') }}">
                        </div>
                    </div>
                    <div class="form-group row p-form-box">
                        <div class="col-md-12 m-0 mt-md-2">
                            <br>
                            <h4>Bank Details</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row p-form-box">
                        <label for="bank_name" class="col-md-3 col-form-label">Bank Name</label>
                        <div class="col-md-9">
                            <select name="bank_name" id="bank_name" class="form-control text-capitalize">
                                @foreach($bankNames as $bankName)
                                <option value="{{ $bankName->bank_code }}" {{ ($dealerProfile->dealerBankDetails && $bankName->bank_code == $dealerProfile->dealerBankDetails->bank_code)? 'selected' : '' }}>
                                    {{$bankName->bank_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('bank_name')
                        <small class="form-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="account_holder_name" class="col-md-3 col-form-label">Account Holder's Name <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="account_holder_name" id="account_holder_name" value="{{ old('account_holder_name' ,  ($dealerProfile->dealerBankDetails) ? $dealerProfile->dealerBankDetails->bank_acc_name : '') }}" class="form-control @error('account_holder_name') is-invalid @enderror">
                        </div>
                    </div>

                    <div class="form-group row p-form-box">
                        <label for="bank_account_number" class="col-md-3 col-form-label">Bank Account Number <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="bank_account_number" id="bank_account_number" value="{{ old('bank_account_number' , ($dealerProfile->dealerBankDetails) ? $dealerProfile->dealerBankDetails->bank_acc : '' )}}" class="form-control @error('bank_account_number') is-invalid @enderror">
                        </div>
                    </div>
                    <div class="form-group row p-form-box">
                        <label for="account_regid" class="col-md-3 col-form-label">Account Holder NRIC/ Company Registration <small class="text-danger">*</small></label>
                        <div class="col-md-9 m-0">
                            <input type="text" name="account_regid" id="account_regid" value="{{ old('account_regid' , ($dealerProfile->dealerBankDetails) ? $dealerProfile->dealerBankDetails->account_regid : '') }}" class="form-control @error('account_regid') is-invalid @enderror">
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group p-btn-btm row ">
                        <div class="btn-p-update offset-5 offset-md-8 col-md-5 p-pl">
                            <input type="submit" class="btn" value="Update Profile">
                        </div>
                    </div>
                </div>
            </div>
            @enddealershipcheck
        </div>
    </div>
</form>


<style>
    .text-color-header {
        color: black;
        font-size: 12pt;
    }

    .text-bold {
        font-weight: bold;
    }

    .font-family-style {
        font-family: cursive;
    }

    @media(min-width:300px) and (max-width:699px) {
        .margin-left-phone {
            margin-left: 30px;
        }
    }

    @media(min-width:700px) and (max-width:999px) {
        .margin-left-tablet {
            margin-left: 30px;
        }
    }

    @media(min-width:1000px) and (max-width:1199px) {
        .margin-left-xs {
            margin-left: 70px;
        }
    }

    @media(min-width:1200px) and (max-width:1599px) {
        .margin-left-sm {
            margin-left: 185px;
        }
    }

    @media(min-width:1600px) and (max-width:2000px) {
        .margin-left-md {
            margin-left: 330px;
        }
    }
</style>

@push('script')
<script>
    $(document).ready(function() {
        // Disable spouse information if marital status is single or divorced
        $(document).on('change', 'select[name=marital_id]', function(e) {
            var el = $(this);
            if (el.val() == '1') {
                $('input[name=spouse_name').val('');
                $('input[name=spouse_occupation').val('');
                $('input[name=spouse_contact').val('');
                $('input[name=spouse_email').val('');
                $('#spouse_name').prop('readonly', true);
                $('#spouse_occupation').prop('readonly', true);
                $('#spouse_contact').prop('readonly', true);
                $('#spouse_email').prop('readonly', true);
            }

            if (el.val() == '2') {
                $('#spouse_name').prop('readonly', false);
                $('#spouse_occupation').prop('readonly', false);
                $('#spouse_contact').prop('readonly', false);
                $('#spouse_email').prop('readonly', false);
            }

            if (el.val() == '3') {
                $('input[name=spouse_name').val('');
                $('input[name=spouse_occupation').val('');
                $('input[name=spouse_contact').val('');
                $('input[name=spouse_email').val('');
                $('#spouse_name').prop('readonly', true);
                $('#spouse_occupation').prop('readonly', true);
                $('#spouse_contact').prop('readonly', true);
                $('#spouse_email').prop('readonly', true);
            }
        });

        if ($('#marital_id').val() == '1') {
            $('#spouse_name').prop('readonly', true);
            $('#spouse_occupation').prop('readonly', true);
            $('#spouse_contact').prop('readonly', true);
            $('#spouse_email').prop('readonly', true);
        }

        if ($('#marital_id').val() == '2') {
            $('#spouse_name').prop('readonly', false);
            $('#spouse_occupation').prop('readonly', false);
            $('#spouse_contact').prop('readonly', false);
            $('#spouse_email').prop('readonly', false);
        }

        if ($('#marital_id').val() == '3') {
            $('#spouse_name').prop('readonly', true);
            $('#spouse_occupation').prop('readonly', true);
            $('#spouse_contact').prop('readonly', true);
            $('#spouse_email').prop('readonly', true);
        }
    });

    $(document).ready(function() {

/* City */
var agentcityKey = document.getElementById("dealer_company_city");
var agentcity = document.getElementById("company_city");
// var classCityKey = document.getElementById("class_city_key");

//console.log(agentcityKey.value);

displayagentCity();

$('#dealer_company_city').on('change', function( ) {
    displayagentCity();
});

$("#dealer_company_state").on("change",function(){
    var variableID = $(this).val();
console.log('here');
    if(variableID){
        $.ajax({
            type:"POST",
            url:'{{route("state.filter.agent.city")}}',
            data:$("#formid").serialize(),
            success:function(toajax){
                $("#dealer_company_city").html(toajax);
                displayagentCity();
            }
        });
    }

});

function displayagentCity(){
    if(agentcityKey.value == '0'){
        agentcity.style.display = "block";
        // classCityKey.className ="col-12 col-md-6 form-group";
    }else {
        agentcity.style.display = "none";
        // classCityKey.className ="col-12 col-md-12 form-group";
    }
}
/* End City */

});

//side menu disable and layout size
const hideLeftNMenu = document.querySelector('.row.db-width.hide .col-md-3');
  const hideRightNMenu = document.querySelector('.row.db-width.hide .col-md-9');
  hideLeftNMenu.style.display = 'none';
  hideRightNMenu.className = 'col-md-12 my-box-pd';

</script>
@endpush

@endsection