@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

{{-- {{ $paginator->count() }} --}}
<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">Order Tracking Detail</h1>
        </div>
    </div>

    {{-- <div class="row mt-2 mb-3">
        <div class="col-12">
            <a href="/management/panel/orders" class="orders-text-style "
                style="border-bottom: 2px solid rgb(250, 172, 24);"><strong>All Orders</strong></a>
            <a href="#" class="orders-text-style"><strong>New Orders</strong></a>
            <a href="#" class="orders-text-style"><strong>Pending Shipping</strong></a>
            <a href="#" class="orders-text-style"><strong>Pending Receiving</strong></a>
            <a href="#" class="orders-text-style"><strong>Completed</strong></a>
        </div>
    </div> --}}


    <form action="{{ route('administrator.redemption') }}" method="GET">
        <div class="row mt-2 ml-2">
            <!-- Status -->
            {{-- <div class="col-md-auto px-1">
                <select name="status" class="col-12 text-left custom-select">
                    <option value="all">All Status</option>
                    @foreach ($statuses as $status)
                    <option value="{{ $status->id }}" {{ $request->status == $status->id ? 'selected' : ''}}>
                        {{ $status->name }}</option>
                        @endforeach
                </select>
            </div> --}}

            <!-- States -->
            {{-- <div class="col-md-auto px-1">
                <select name="states" class="col-12 text-left custom-select">
                    <option value="all">All States</option>

                </select>
            </div> --}}

             <!-- Date filter from date to date-->
             {{-- <div class="col-md-auto px-1">
                <div class="input-group mb-3">
                    <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                        aria-label="Username" aria-describedby="basic-addon1"
                        value="{{ ($request->datefrom)?$request->datefrom:'' }}" autocomplete="off">
                </div>
            </div>

            <div class="col-md-auto px-1">
                <div class="input-group mb-3">
                    <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                        aria-label="Username" aria-describedby="basic-addon1"
                        value="{{ ($request->dateto)?$request->dateto:'' }}" autocomplete="off">
                </div>
            </div>

            <div class="col-md-auto px-1">
                <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
            </div>

            <div>
                <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.redemption') }} style="color:black;">Reset Filter</a></button>
            </div> --}}

            <div class="col-md-auto ml-auto">
                <div class="row">
                    <div class="col-md-8 p-0">
                        <select class="custom-select" id="selectSearch" name="selectSearch">
                            <option value="ALL" selected>Choose...</option>
                            <option value="BJN" {{ $request->selectSearch=='BJN'?'selected':'' }}>Order Number</option>
                            <option value="PODO" {{ $request->selectSearch=='PODO'?'selected':'' }}>DO Number</option>
                            <option value="NAME" {{ $request->selectSearch=='NAME'?'selected':'' }}>Product Name</option>
                            <option value="CODE" {{ $request->selectSearch=='CODE'?'selected':'' }}>Product Code</option>
                        </select>

                        <input type="text" class="form-control" id="" name="searchBox"
                            value="{{ ($request->searchBox)?$request->searchBox:'' }}">
                    </div>
                    <div class="col-md-4 ">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Item) : <b>{{ count($ForCountcustomerOrders) }}</b></span></p>
        <div class="row">
            <div class="col-auto ml-auto">
                {!! $customerOrders->render() !!}
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-bordered text-center table-striped " id="sales-tracking" style="width:100%">
                    <thead style="background-color:	#E8E8E8;">
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Order No.</th>
                            <th class="thead-font" scope="col">DO No.</th>
                            <th scope="col">Product Code</th>
                            <th scope="col">Product Name </th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Item Status</th>
                            <th scope="col">States</th>
                            <th scope="col">Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customerOrders as $customerOrder)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a target="_blank"
                                    href="{{ URL::asset('storage/documents/invoice/' . $customerOrder->order->purchase->getFormattedNumber() . (($customerOrder->order->purchase->invoice_version != 0) ? '/v'.$customerOrder->order->purchase->invoice_version.'/' : '/') . $customerOrder->order->purchase->getFormattedNumber() . '.pdf') }}">{{ $customerOrder->order->purchase->getFormattedNumber() }}</a>
                                </td>
                                <td>
                                    {{-- {{ route('administrator.redemption.do',('{{ $customerOrder->delivery_order }}')) }} --}}
                                    <a href="{{ route('administrator.redemption.do',['do' => $customerOrder->delivery_order]) }}" target="_blank">{{$customerOrder->delivery_order}}</a>
                                </td>
                                <td>
                                    {!! ($customerOrder->product_code) ? $customerOrder->product_code : '<center>-</center>' !!}
                                </td>
                                <td>
                                    @if(is_object( $customerOrder->product))
                                    {{ $customerOrder->product->parentProduct->name }}
                                    @endif

                                    @if(array_key_exists('product_color_name', $customerOrder->product_information))
                                    <p class="text-capitalize text-secondary m-0">Color:
                                        {{ $customerOrder->product_information['product_color_name'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_size', $customerOrder->product_information))
                                    <p class="text-capitalize text-secondary m-0">Size:
                                        {{ $customerOrder->product_information['product_size'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_curtain_size', $customerOrder->product_information))
                                    <p class="text-capitalize text-secondary m-0">Curtain Model:
                                        {{ $customerOrder->product_information['product_curtain_size'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_miscellaneous', $customerOrder->product_information))
                                    <p class="text-capitalize text-secondary m-0">
                                        {{ $customerOrder->product_information['product_miscellaneous'] }}
                                    </p>
                                    @endif
                                    @if(array_key_exists('product_temperature', $customerOrder->product_information))
                                    <p class="text-capitalize text-secondary m-0">Color Temperature:
                                        {{ $customerOrder->product_information['product_temperature'] }}</p>
                                    @endif
                                    @if(array_key_exists('product_preorder_date', $customerOrder->product_information))
                                    <p class="text-capitalize text-secondary m-0">Pre Order Delivery:
                                        {{ $customerOrder->product_information['product_preorder_date'] }}</p>
                                    @endif
                                    @if ($customerOrder->bundle_id != 0)
                                    <p class="text-capitalize text-secondary m-0">In Bundle :
                                        ({{$customerOrder->getParentName($customerOrder->bundle_id)}})
                                    </p>
                                    @endif
                                </td>
                                <td>
                                    {{ ($customerOrder->order->purchase->created_at->format('d/m/Y')) ? $customerOrder->order->purchase->created_at->format('d/m/Y') : '' }}
                                </td>
                                <td>
                                    {{$customerOrder->quantity }}
                                </td>

                                <td>
                                    {{ str_replace("Order","Item", $statuses->where('id',$customerOrder->item_order_status)->first()->name ) }}
                                </td>
                                <td>
                                    {{ $customerOrder->order->purchase->state->name }}
                                </td>
                                <td>
                                    {{-- remark --}}
                                @if(array_key_exists('product_order_remark', $customerOrder->product_information))
                                <p class="text-capitalize">User Remarks:
                                    {{ $customerOrder->product_information['product_order_remark'] }}</p>
                                @endif

                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-auto ml-auto py-3">
        </div>
    </div>

</div>


@push('style')

@endpush

@push('script')
<script>
    $(function() {
        $(".date").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
            });

            $(".estimatedateonly").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0,
            changeMonth: true,
            changeYear: true
            });

            $(".filterDate").datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: 0,
            changeMonth: true,
            changeYear: true
            });
    });

    </script>
@endpush
@endsection
