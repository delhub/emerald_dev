<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReasonToUserMembershipLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_memberships_logs', function (Blueprint $table) {
            //
            $table->integer('admin_id')->after('total_expenses')->nullable();
            $table->integer('image_id')->after('total_expenses')->nullable();
            $table->text('reason')->after('total_expenses')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_memberships_logs', function (Blueprint $table) {
            //
        });
    }
}
