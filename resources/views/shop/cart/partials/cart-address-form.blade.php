
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="modal-body">
        <div class="mb-3 cart-form1">
            <label for="exampleInputEmail0" class="form-label">Name<small
                    class="text-danger">*</small></label>
            <input type="text"
                class="checkout-box2 form-control @error('exampleInputEmail0') is-invalid @enderror"
                id="exampleInputEmail0" aria-describedby="emailHelp"
                placeholder="Your Name" name="name" value="{{ $cookie->name }}">
            @error('exampleInputEmail0')
                <small class="error form-text text-danger">{{ $message }}</small>
            @enderror
            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3 cart-form1">
            <label for="exampleInputEmail1" class="form-label">Contact (Mobile)<small
                    class="text-danger">*</small></label>
            <input type="text"
                class="checkout-box2 form-control @error('exampleInputEmail1') is-invalid @enderror"
                id="exampleInputEmail1" aria-describedby="emailHelp"
                placeholder="Contact Number" name="phone"
                value="{{ $cookie->mobile }}">
            @error('exampleInputEmail1')
                <small class="form-text text-danger">{{ $message }}</small>
            @enderror
            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3 cart-form1">
            <label for="exampleFormControlInput0" class="form-label">Address 1<small
                    class="text-danger">*</small></label>
            <input type="text"
                class="checkout-box2 form-control @error('exampleFormControlInput0') is-invalid @enderror"
                id="exampleFormControlInput0" placeholder="Address 1" name="address1"
                value="{{ $cookie->address_1 }}">
            @error('exampleFormControlInput0')
                <small class="form-text text-danger">{{ $message }}</small>
            @enderror
        </div>
        <div class="mb-3 cart-form1">
            <label for="exampleFormControlInput1" class="form-label">Address 2</label>
            <input type="text" class="checkout-box2 form-control"
                id="exampleFormControlInput1" placeholder="Address 2" name="address2"
                value="{{ $cookie ? $cookie->address_2 : '' }}">
        </div>
        <div class="mb-3 cart-form1">
            <label for="exampleFormControlInput1" class="form-label">Address 3</label>
            <input type="text" class="checkout-box2 form-control"
                id="exampleFormControlInput1" placeholder="Address 3" name="address3"
                value="{{ $cookie ? $cookie->address_3 : '' }}">
        </div>
        <div class="mb-3 cart-form1">
            <label for="exampleFormControlInput2" class="form-label">Postcode<small
                    class="text-danger">* </small> <span
                    class="form-text text-danger ajaxMessage d-inline"
                    style="display: none"></span></label>
            <input type="text"
                class="checkout-box2 form-control @error('exampleFormControlInput2') is-invalid @enderror"
                id="exampleFormControlInput2" placeholder="Postcode" name="postcode"
                value="{{ $cookie->postcode }}">


            @error('exampleFormControlInput2')
                <small class="form-text text-danger ">{{ $message }}</small>
            @enderror
        </div>
        <div class="cart-edit-location">
            <div class="mb-3 cart-form3">
                <label for="stateChoices">State<small
                        class="text-danger">*</small></label>
                <select
                    class="form-select checkout-box3 @error('stateChoices') is-invalid @enderror"
                    aria-label="Default select example" id="stateChoices"
                    onchange="editAddressForm(this)" name="state">
                    <option selected disabled>Select...</option>
                    @foreach ($states as $state)
                        <option value="{{ $state->id }}"
                            {{ $cookie && $cookie->state_id == $state->id && !in_array($cookie->state_id,[10,11,15]) ? 'selected' : '' }}
                            {!! in_array($state->id,[10,11,15]) ? 'style="color:lightgrey;" disabled' : '' !!}>
                            {{ $state->name }}</option>
                    @endforeach
                </select>
                @error('stateChoices')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="mb-3 cart-form2">
                <label for="cityChoices">City<small
                        class="text-danger">*</small></label>
                <select
                    class="form-select checkout-box3 @error('cityChoices') is-invalid @enderror"
                    aria-label="Default select example" id="cityChoices" name="city">
                    <option selected disabled>Select...</option>
                    @foreach ($cities as $city)
                        <option value="{{ $city->city_key }}"
                            {{ $cookie && $cookie->city_key == $city->city_key ? 'selected' : '' }}>
                            {{ $city->city_name }}
                        </option>
                    @endforeach
                </select>
                @error('cityChoices')
                    <small class="form-text text-danger">{{ $message }}</small>
                @enderror
            </div>
        </div>

    </div>

