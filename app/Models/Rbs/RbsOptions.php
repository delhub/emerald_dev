<?php

namespace App\Models\Rbs;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RbsOptions extends Model
{
     //use HasFactory;

     // Table Name
     protected $table = 'global_rbs_options';
     // Primary Key
     public $primaryKey = 'id';
     // Timestamps

     protected $fillable = [
        'rbs_name',
        'rbs_data',
        'option_type',
        'option_status'
    ];
}
