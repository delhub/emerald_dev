@extends('layouts.shop.main')

@section('content')

@section('page_title')
    {{ '' }}
@endsection


<div class="product-home">
    <div class="fml-container">
        <div class="subtitle-namelist {{ isset($paymentLink) ? 'd-none' : '' }}">
            <ul>
                <li style="text-align: justify;"><a href="/">Home</a></li>
                <li style="text-align: justify;"><span>/</span></li>
                <li style="text-align: justify;"><a href="#">Cart Page</a></li>
            </ul>
        </div>

        <div class="alert alert-primary" role="alert" style="display: {{ $isfreegift > 0 ? 'block' : 'none' }}">
            This product have Free Gift, Click here to get your Free Gift <button type="button" class="btn red-btn"
                data-toggle="modal" data-target="#freegift">Free Gift</button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="freegift" tabindex="-1" role="dialog" aria-labelledby="freegiftModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{ route('shop.freegift') }}" method="POST">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="freegiftModalLabel">Free Gift</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                onclick="hideModal('freegift')">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @if (count($freegift))
                                @foreach ($freegift as $val)
                                    @if ($val['status'] == true && $val['gift_type'] == 'multiproduct')
                                        @php
                                            $multitem = $val['freegift'];
                                        @endphp
                                        -- Choose One only
                                        @foreach ($val['freegift'] as $k => $item)
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="item"
                                                    id="{{ $item }}" value="{{ $item }}"
                                                    {{ $user->carts->where('status', 2001)->where('freegift', 1)->whereIn('product_code', $multitem)->count() > 0? 'disabled': '' }}>
                                                <label class="form-check-label" for="{{ $item }}">
                                                    {{ $loop->iteration }}.
                                                    {{ $val['product'][$k]->product2->parentProduct->name }}
                                                    ({{ $item }})
                                                </label>
                                            </div>
                                        @endforeach
                                    @elseif ($val['status'] == true && $val['gift_type'] == 'product')
                                        @php
                                            $item = $val['freegift'];
                                        @endphp
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="item"
                                                id="{{ $val['freegift'] }}" value="{{ $val['freegift'] }}"
                                                {{ $user->carts->where('status', 2001)->where('product_code', $item)->where('freegift', 1)->count() > 0? 'disabled': '' }}>
                                            <label class="form-check-label" for="{{ $val['freegift'] }}">
                                                {{ $val['product']->product2->parentProduct->name }}
                                                ({{ $val['freegift'] }})
                                            </label>
                                        </div>
                                    @else
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="rule" value="{{ json_encode($freegift) }}">
                            <input type="hidden" id="fg_popup" name="fg_popup" value="0">
                            <button type="button" class="btn red-btn" data-dismiss="modal"
                                onclick="hideModal('freegift')">Close</button>
                            <button type="submit" class="btn blue-btn" onclick="getfreegift(1)">Get Free Gift
                                item</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <section>
            <div class="card-home">
                <section class="checkout-page content">
                    <div class="fml-container">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <div class="checkout-box yellow-ball-bg">
                                    <div class="shopping-tittle blue-box-bg">
                                        <p>Payment Method</p>
                                    </div>
                                    <div class="hr-line-cart"></div>
                                    <div class="payment-box my-mt-1 my-mb-2 px-0">
                                        {{-- <div class="form-row pay-methodbox5">
                                            <ul class="nav nav-tabs form-row pay-methodbox5 px-3 " id="myTab"
                                                role="tablist">
                                                <li
                                                    class="nav-item col pay my-px-2 {{ $cartsItems['finalTotal'] >= 0 ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-" id="payVoucher-tab" data-toggle="tab"
                                                            href="#payVoucher" role="tab" aria-controls="payVoucher"
                                                            aria-selected="false">
                                                            <div class="pay-itembox">
                                                                <div class="pay checkout-tick">
                                                                    <input type="radio" name="payment-mh" value="pay00"
                                                                        id="pay00"
                                                                        {{ $cartsItems['finalTotal'] <= 0 ? 'checked' : '' }}><label
                                                                        for="pay00"></label>
                                                                </div>
                                                                <img class="{{ $cartsItems['finalTotal'] >= 0 ? 'paymentDisabled' : '' }}"
                                                                    src="{{ asset('/images/icons/dashboard-icon1.png') }}"
                                                                    alt="">
                                                                <h5>Voucher</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li
                                                    class="nav-item col pay my-px-2 {{ $cartsItems['finalTotal'] <= 0 ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-0 {{ $cartsItems['finalTotal'] == 0 ? 'disabled' : '' }}"
                                                            id="payOne-tab" data-toggle="tab" href="#payOne" role="tab"
                                                            aria-controls="payOne" aria-selected="false">
                                                            <div class="pay-itembox">
                                                                <div class="pay checkout-tick">
                                                                    <input type="radio" name="payment-mh" value="pay01"
                                                                        id="pay01"
                                                                        {{ $cartsItems['finalTotal'] >= 0 ? 'checked' : '' }}><label
                                                                        for="pay01"></label>
                                                                </div>
                                                                <img class="{{ $cartsItems['finalTotal'] == 0 ? 'paymentDisabled' : '' }}"
                                                                    src="{{ asset('/images/icons/pay_icon03.svg') }}"
                                                                    alt="">
                                                                <h5>Credit/ Debit Card</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li
                                                    class="nav-item col disabled pay my-px-2 {{ $cartsItems['finalTotal'] <= 0 ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-0 disabled" id="payTwo-tab"
                                                            data-toggle="tab" href="#payTwo" role="tab"
                                                            aria-controls="payTwo" aria-selected="false">
                                                            <div class="pay-itembox2">
                                                                <div class="pay checkout-tick-g">
                                                                    <input type="radio" name="payment-mh" value="pay02"
                                                                        id="pay02" /><label for="pay02"></label>
                                                                </div>
                                                                <img class="paymentDisabled"
                                                                    src="{{ asset('/images/icons/pay_icon01.svg') }}"
                                                                    alt="">
                                                                <h5>FPX Online Banking<br>(Coming Soon)</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li
                                                    class="nav-item col pay my-px-2 {{ $cartsItems['finalTotal'] <= 0 ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-0 {{ array_sum(array_column($cartsItems, 'subtotalPrice')) == 0 ? 'disabled' : '' }}"
                                                            id="paythree-tab" data-toggle="tab" href="#paythree"
                                                            role="tab" aria-controls="pay1three" aria-selected="false">
                                                            <div class="pay-itembox">
                                                                <div class="pay checkout-tick">
                                                                    <input type="radio" name="payment-mh" value="pay03"
                                                                        id="pay03" /><label for="pay03"></label>
                                                                </div>
                                                                <img class="{{ array_sum(array_column($cartsItems, 'subtotalPrice')) == 0 ? 'paymentDisabled' : '' }}"
                                                                    src="{{ asset('/images/icons/pay_icon02.svg') }}"
                                                                    alt="">
                                                                <h5>Offline Payment</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li
                                                    class="nav-item col disabled pay my-px-2 disabled {{ $cartsItems['finalTotal'] <= 0 ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-0 disabled" id="payFour-tab"
                                                            data-toggle="tab" href="#payFour" role="tab"
                                                            aria-controls="payFour" aria-selected="false">
                                                            <div class="pay-itembox2">
                                                                <div class="pay checkout-tick-g">
                                                                    <input type="radio" name="payment-mh" value="pay04"
                                                                        id="pay04" /><label for="pay04"></label>
                                                                </div>
                                                                <img class="paymentDisabled"
                                                                    src="{{ asset('/images/icons/hp.png') }}" alt="">
                                                                <h5>Health Points<br>(Coming Soon)</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li
                                                    class="nav-item col pay my-px-2 disabled {{ $cartsItems['finalTotal'] <= 0 ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-0 disabled {{ array_sum(array_column($cartsItems, 'subtotalPrice')) == 0 ? 'disabled' : '' }}"
                                                            id="payFive-tab" data-toggle="tab" href="#payFive"
                                                            role="tab" aria-controls="payFive" aria-selected="false">
                                                            <div class="pay-itembox">
                                                                <div class="pay checkout-tick-g">
                                                                    <input type="radio" name="payment-mh" value="pay05"
                                                                        id="pay05" /><label for="pay05"></label>
                                                                </div>
                                                                <img class="paymentDisabled {{ array_sum(array_column($cartsItems, 'subtotalPrice')) == 0 ? 'paymentDisabled' : '' }}"
                                                                    src="{{ asset('/images/icons/duitnow.svg') }}"
                                                                    alt="">
                                                                <h5>Duit now</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li
                                                    class="nav-item col-md-3 pay my-px-2 {{ !isset($agent) ? 'd-none' : '' }}">
                                                    <div class="jj">
                                                        <a class="nav-link px-0" id="payDirect-tab" data-toggle="tab"
                                                            href="#payDirect" role="tab" aria-controls="payDirect"
                                                            aria-selected="false">
                                                            <div class="pay-itembox">
                                                                <div class="pay checkout-tick">
                                                                    <input type="radio" name="payment-mh" value="pay06"
                                                                        id="pay06" /><label for="pay06"></label>
                                                                </div>
                                                                <img class=""
                                                                    src="{{ asset('/images/icons/external-link.svg') }}"
                                                                    alt="" style="width: 50px;">
                                                                <h5 class="pt-2">Link Payment</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div> --}}
                                        <div class="ff pay-methodbox5">
                                            <ul id="myTab">
                                                @php $minimumPointNeeded = 0; @endphp
                                                @if ($cartsItems['finalTotal'] <= 0)
                                                    <li class="">
                                                        <div class="paybox-1">
                                                            <a class="nav-link px-0" id="payVoucher-tab"
                                                                data-toggle="tab" href="#payVoucher" role="tab"
                                                                aria-controls="payVoucher" aria-selected="false">
                                                                <div class="pay-itembox">
                                                                    <div class="pay checkout-tick">
                                                                        <input type="radio" name="payment-mh"
                                                                            value="pay00" id="pay00" checked><label
                                                                            for="pay00"></label>
                                                                    </div>
                                                                    <img class="voucher-i"
                                                                        src="{{ asset('/images/icons/dashboard-icon1.png') }}"
                                                                        alt="">
                                                                    <h5>Voucher</h5>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </li>
                                                @else
                                                    <li class="">
                                                        <div class="paybox-1">
                                                            <a class="nav-link px-0" id="payOne-tab" data-toggle="tab"
                                                                href="#payOne" role="tab" aria-controls="payOne"
                                                                aria-selected="false">
                                                                <div class="pay-itembox">
                                                                    <div class="pay checkout-tick">
                                                                        <input type="radio" name="payment-mh"
                                                                            value="pay01" id="pay01"><label
                                                                            for="pay01"></label>
                                                                    </div>
                                                                    <img class=""
                                                                        src="{{ asset('/images/icons/pay_icon03.svg') }}"
                                                                        alt="">
                                                                    <h5>Credit/ Debit Card</h5>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="">
                                                        <div class="paybox-1">
                                                            <a class="nav-link px-0 {{ $disableFpx ? 'disabled' : ''  }}" id="payTwo-tab" data-toggle="tab"
                                                                href="#payTwo" role="tab" aria-controls="payTwo"
                                                                aria-selected="false">
                                                                <div class="pay-itembox">
                                                                    <div class="pay {{ $disableFpx ? 'checkout-tick-g' : 'checkout-tick' }}">
                                                                        <input type="radio" name="payment-mh"
                                                                            value="pay02" id="pay02"><label
                                                                            for="pay02"></label>
                                                                    </div>
                                                                    <img class="{{ $disableFpx ? 'paymentDisabled' : ''  }}"
                                                                        src="{{ asset('/images/icons/fpx-logo.png') }}"
                                                                        alt="">
                                                                    <h5>FPX Online Banking</h5>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    @if ($user->userInfo->acc_code == 'bypass')
                                                        <li
                                                            class="{{ $cartsItems['finalTotal'] <= 0 ? 'd-none' : '' }}">
                                                            <div class="paybox-1">
                                                                <a class="nav-link px-0 {{ array_sum(array_column($cartsItems['items'], 'subtotalPrice')) == 0 ? 'disabled' : '' }}"
                                                                    id="paythree-tab" data-toggle="tab" href="#paythree"
                                                                    role="tab" aria-controls="pay1three"
                                                                    aria-selected="false">
                                                                    <div class="pay-itembox">
                                                                        <div class="pay checkout-tick">
                                                                            <input type="radio" name="payment-mh"
                                                                                value="pay03" id="pay03" /><label
                                                                                for="pay03"></label>
                                                                        </div>
                                                                        <img class="{{ array_sum(array_column($cartsItems['items'], 'subtotalPrice')) == 0 ? 'paymentDisabled' : '' }}"
                                                                            src="{{ asset('/images/icons/pay_icon02.svg') }}"
                                                                            alt="">
                                                                        <h5>Offline Payment</h5>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    @endif
                                                    {{-- <li class="disabled ">
                                                        <div class="paybox-1">
                                                            <a class="nav-link px-0 disabled" id="payFour-tab"
                                                                data-toggle="tab" href="#payFour" role="tab"
                                                                aria-controls="payFour" aria-selected="false">
                                                                <div class="pay-itembox2">
                                                                    <div class="pay checkout-tick-g">
                                                                        <input type="radio" name="payment-mh"
                                                                            value="pay04" id="pay04" /><label
                                                                            for="pay04"></label>
                                                                    </div>
                                                                    <img class="paymentDisabled"
                                                                        src="{{ asset('/images/icons/hp.png') }}"
                                                                        alt="">
                                                                    <h5>Health Points<br>(Coming Soon)</h5>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </li> --}}

                                                    <li class="">
                                                        <div class="paybox-1">
                                                            <a class="nav-link px-0 {{ array_sum(array_column($cartsItems, 'subtotalPrice')) == 0 ? 'disabled' : '' }}"
                                                                id="payFive-tab" data-toggle="tab" href="#payFive"
                                                                role="tab" aria-controls="payFive"
                                                                aria-selected="false">
                                                                <div class="pay-itembox">
                                                                    <div class="pay checkout-tick-g">
                                                                        <input type="radio" name="payment-mh"
                                                                            value="pay05" id="pay05" /><label
                                                                            for="pay05"></label>
                                                                    </div>
                                                                    <img class=" {{ array_sum(array_column($cartsItems, 'subtotalPrice')) == 0 ? 'paymentDisabled' : '' }}"
                                                                        src="{{ asset('/images/icons/duitnow.svg') }}"
                                                                        alt="">
                                                                    <h5>Duit now</h5>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    @if (config('app.metapoint') && $cartsItems['discount'] == 0 && !isset($paymentLink))
                                                        <li class="">
                                                            <div class="paybox-1">
                                                                <a class="nav-link px-0 {{ $cartsItems['discount'] != 0 || $mespay_purchaseable != 0  ? 'disabled' : '' }}"
                                                                    id="paySix-tab" data-toggle="tab" href="#paySix"
                                                                    role="tab" aria-controls="paySix"
                                                                    aria-selected="false">
                                                                    <div class="pay-itembox">
                                                                        <div class="pay checkout-tick{{$mespay_purchaseable == 1 ? '-g' : ''}}">
                                                                            <input type="radio" name="payment-mh"
                                                                                value="pay06" id="pay06"><label
                                                                                for="pay06"></label>
                                                                        </div>
                                                                        <img class="{{$mespay_purchaseable == 1 ? ' paymentDisabled' : ''}}"
                                                                            src="{{ asset('storage/icons/payments/meta-point-01.png') }}" alt="">
                                                                        <h5>MES</h5>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    @endif
                                                    {{-- @php
                                                    // $ff = $cartsItems['discount'];
                                                    dd($cartsItems);
                                                @endphp --}}
                                                    {{-- @if ($ff !== 'payment/cashier')

                                                @else --}}
                                                @if ((isset($agent) || $cartsItems['discount'] < 0) && !isset($paymentLink))
                                                <li {{-- class="{{ !isset($agent) ? 'd-none' : '' || !isset($ff) ? 'd-none' : '' }}" --}}>
                                                    <div class="paybox-1">
                                                        <a class="nav-link px-0 gg" id="payDirect-tab"
                                                            data-toggle="tab" href="#payDirect" role="tab"
                                                            aria-controls="payDirect" aria-selected="false">
                                                            <div class="pay-itembox">
                                                                <div class="pay checkout-tick">
                                                                    <input type="radio" name="payment-mh"
                                                                        value="pay06" id="pay06" /><label
                                                                        for="pay06"></label>
                                                                </div>
                                                                <img class="link-pay-i"
                                                                    src="{{ asset('/images/icons/external-link.svg') }}"
                                                                    alt="">
                                                                <h5 class="pt-2">Link Payment</h5>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </li>
                                                @endif

                                                    {{-- @endif --}}
                                                @endif



                                            </ul>
                                        </div>

                                        <div class="full-hrline-grey hr-paymargin"></div>

                                        <div class="form-row tab-content px-4">
                                            @if ($cartsItems['finalTotal'] <= 0)
                                                {{-- Voucher Form --}}
                                                <div class="col-12 tab-pane {{ $cartsItems['finalTotal'] <= 0 ? 'active' : '' }}"
                                                    id="payVoucher" role="tabpanel" aria-labelledby="payVoucher-tab">
                                                    <form id="voucher-form"
                                                        action="{{ isset($paymentLink) ? $paymentLink : '/payment' }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="py-3 text-left">
                                                            <p class="col-md-12">
                                                                This order will be paid using voucher. Important! Any
                                                                unused
                                                                value will be forfeited.
                                                            </p>
                                                        </div>
                                                        @include(
                                                            'shop.payment.bottom-payment-form',
                                                            ['type' => 'Voucher']
                                                        )
                                                    </form>

                                                </div>
                                            @else
                                                {{-- Card Form --}}
                                                <div class="col-12 tab-pane {{ $cartsItems['finalTotal'] >= 0 ? 'active' : '' }}"
                                                    id="payOne" role="tabpanel" aria-labelledby="payOne-tab">
                                                    @if (!isset($paymentLink))
                                                        <div class="select-pay">
                                                            <h5>Select A Card</h5>

                                                        </div>
                                                    @endif

                                                    <form id="cybersource-form"
                                                        action="{{ isset($paymentLink) ? $paymentLink : '/payment' }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        @if (!isset($paymentLink))
                                                            <div class="saveCard pay-methodbox5">
                                                                <!-- Nav tabs -->
                                                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                                    @foreach ($savedCards as $savedCard)
                                                                        <li class="nav-item">
                                                                            <div class="paybox-1">
                                                                                <a class="nav-link active"
                                                                                    id="token{{ $savedCard->id }}-tab"
                                                                                    data-toggle="tab"
                                                                                    href="#token{{ $savedCard->id }}"
                                                                                    role="tab"
                                                                                    aria-controls="token{{ $savedCard->id }}"
                                                                                    aria-selected="true">
                                                                                    <div class="card-itembox">
                                                                                        <div class="pay checkout-tick">
                                                                                            <input type="radio"
                                                                                                name="cardChoices"
                                                                                                value="{{ $savedCard->id }}"
                                                                                                id="card{{ $savedCard->id }}"><label
                                                                                                for="card{{ $savedCard->id }}"></label>
                                                                                        </div>
                                                                                        <h5><img src="{{ asset('/images/icons/card-token/' . $savedCard->card_type . '.png') }}"
                                                                                                alt="">
                                                                                            {{ str_replace('x', '*', $savedCard->card_number) }}
                                                                                        </h5>
                                                                                    </div>
                                                                                </a>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                    <li class="nav-item">
                                                                        <div class="paybox-1">
                                                                            <a class="nav-link active" id="newcard-tab"
                                                                                data-toggle="tab" href="#newcard"
                                                                                role="tab" aria-controls="newcard"
                                                                                aria-selected="true">
                                                                                <div class="card-itembox">
                                                                                    <div class="pay checkout-tick">
                                                                                        <input type="radio"
                                                                                            name="cardChoices"
                                                                                            value="newCard" id="newCard"
                                                                                            checked><label
                                                                                            for="newCard"></label>
                                                                                    </div>
                                                                                    <h5 class="py-1">New Card
                                                                                    </h5>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <!-- Tab panes -->
                                                                <div class="tab-content">
                                                                    @foreach ($savedCards as $savedCard)
                                                                        <div class="tab-pane fade"
                                                                            id="token{{ $savedCard->id }}"
                                                                            role="tabpanel"
                                                                            aria-labelledby="token{{ $savedCard->id }}-tab">
                                                                        </div>
                                                                    @endforeach
                                                                    <div class="tab-pane active" id="newcard"
                                                                        role="tabpanel" aria-labelledby="newcard-tab">
                                                                        @include(
                                                                            'shop.payment.main-cybersource-form'
                                                                        )
                                                                        {{-- save credit/debit card --}}
                                                                        <input type="hidden" name="transaction_type"
                                                                            value="sale">
                                                                        <div class="save-cc-box form-check col-md-12">
                                                                            <input class="form-check-input"
                                                                                type="checkbox" name="transaction_type"
                                                                                value="sale,create_payment_token" id="">
                                                                            <label class="form-check-label"
                                                                                for="flexCheckDefault">
                                                                                Save credit card to use later
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            @include(
                                                                'shop.payment.main-cybersource-form'
                                                            )
                                                        @endif


                                                        <div class="full-hrline-grey hr-paymargin2 hr-line-row"></div>
                                                        @include(
                                                            'shop.payment.bottom-payment-form',
                                                            ['type' => 'Cybersource']
                                                        )
                                                    </form>
                                                </div>

                                                {{-- FPX Form --}}
                                                @if(!$disableFpx)
                                                <div class="col-12 tab-pane px-0 px-md-3" id="payTwo" role="tabpanel"
                                                    aria-labelledby="payTwo-tab">
                                                    You will be redirected to selected bank website to complete the
                                                    payment.
                                                    <br>
                                                    <br>
                                                    <form id="fpx-form"
                                                        action="{{ isset($paymentLink) ? $paymentLink : '/payment' }}"
                                                        method="POST" enctype="multipart/form-data" required>
                                                        @csrf
                                                        <div class="fpxTab pay-methodbox5 mb-5">
                                                            <!-- Nav tabs -->

                                                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                                <li class="nav-item">
                                                                    <small class="errorfpxbank">Please select
                                                                        one</small>

                                                                    <div class="paybox-1">
                                                                        <a class="nav-link" id="b2c-tab"
                                                                            data-toggle="tab" href="#b2c" role="tab"
                                                                            aria-controls="b2c" aria-selected="true">
                                                                            <div class="fpx-itembox">
                                                                                <div class="pay checkout-tick">
                                                                                    <input type="radio"
                                                                                        name="bankRadioOptions"
                                                                                        value="b2c" id="b2cRadio"
                                                                                        class="bankRadioOptions"><label
                                                                                        for="b2cRadio"></label>
                                                                                </div>
                                                                                <h5>Retail Banking (B2C)</h5>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <small
                                                                        class="errorfpxbank">{!! '&nbsp;' !!}</small>

                                                                    <div class="paybox-1">
                                                                        <a class="nav-link" id="b2b1-tab"
                                                                            data-toggle="tab" href="#b2b1" role="tab"
                                                                            aria-controls="b2b1" aria-selected="true">
                                                                            <div class="fpx-itembox">
                                                                                <div class="pay checkout-tick">
                                                                                    <input type="radio"
                                                                                        name="bankRadioOptions"
                                                                                        value="b2b1" id="b2b1Radio"
                                                                                        class="bankRadioOptions"><label
                                                                                        for="b2b1Radio"></label>
                                                                                </div>
                                                                                <h5>Corporate Banking (B2B1)</h5>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div class="tab-pane fade p-0" id="b2c" role="tabpanel"
                                                                    aria-labelledby="b2c-tab">
                                                                    <select class="form-control"
                                                                        name="bankSelectedb2c" id="bankSelectedb2c"
                                                                        style=" overflow: visible !important;" required>
                                                                        <option class="defaultbank" id="defaultbank"
                                                                            value="default">Select Retail Banking (B2C)
                                                                            ...</option>
                                                                        @foreach ($cartsItems['bank']['b2c'] as $key => $B2Clist)
                                                                            <option class="bankList_b2c"
                                                                                style="display: ;"
                                                                                value="{{ $key }}"
                                                                                {{ $B2Clist['status'] != null ? 'disabled' : '' }}>
                                                                                {{ $B2Clist['name'] }}
                                                                                {{ $B2Clist['status'] }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="tab-pane fade p-0" id="b2b1"
                                                                    role="tabpanel" aria-labelledby="b2b1-tab">
                                                                    <select class="form-control"
                                                                        name="bankSelectedb2b1" id="bankSelectedb2b1"
                                                                        style=" overflow: visible !important;" required>
                                                                        <option class="defaultbank" id="defaultbank"
                                                                            value="default">Select Corporate Banking
                                                                            (B2B1) ...</option>
                                                                        @foreach ($cartsItems['bank']['b2b1'] as $key => $B2B1list)
                                                                            <option class="bankList_b2b1"
                                                                                value="{{ $key }}"
                                                                                {{ $B2B1list['status'] != null ? 'disabled' : '' }}>
                                                                                {{ $B2B1list['name'] }}
                                                                                {{ $B2B1list['status'] }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-5 p-0">
                                                                <small class="errorfpxbanklists text-left">Please
                                                                    select one..</small>
                                                            </div>


                                                        </div>
                                                        <div class="row">
                                                            @include(
                                                                'shop.payment.bottom-payment-form',
                                                                ['type' => 'Fpx']
                                                            )
                                                        </div>

                                                    </form>

                                                </div>
                                                @endif

                                                {{-- Offline Form --}}
                                                @if ($user->userInfo->acc_code == 'bypass')
                                                    <div class="col-12 tab-pane" id="paythree" role="tabpanel"
                                                        aria-labelledby="paythree-tab">
                                                        <form id="offline-form"
                                                            action="{{ isset($paymentLink) ? $paymentLink : '/payment' }}"
                                                            method="POST" enctype="multipart/form-data">
                                                            @csrf
                                                            {{-- @include('shop.payment.hidden-address') --}}
                                                            {{-- Offline and duitnow share form --}}
                                                            <div class="form-row my-pt-1 red-ball-bg">
                                                                <div class="form-group col-md-6 my-pb-1">
                                                                    <label for="cart-num">Reference
                                                                        Number<span>*<span></label>
                                                                    <input type="text" class="form-control"
                                                                        id="cart-num" name="reference_number"
                                                                        placeholder="Reference num">
                                                                </div>
                                                                <div class="form-group col-md-6 my-pb-1 upload-box">
                                                                    <label for="formFileLg"
                                                                        class="form-label">Upload
                                                                        Payment
                                                                        Proof<span>*<span></label>
                                                                    <input type="file" name="upload" accept="image/*"
                                                                        id="fileUpload" />
                                                                </div>
                                                                <div class="form-group col-md-6 my-pb-1">
                                                                    <label for="cart-name">Payment Amount
                                                                        (MYR)<span>*<span></label>
                                                                    <input type="text" class="form-control"
                                                                        id="cart-name" name="amount"
                                                                        placeholder="Payment Amount" value="">
                                                                </div>
                                                            </div>
                                                            @include(
                                                                'shop.payment.bottom-payment-form',
                                                                ['type' => 'Offline']
                                                            )
                                                        </form>
                                                    </div>
                                                @endif

                                                {{-- BluePoint Form --}}
                                                <div class="col-12 tab-pane " id="payFour" role="tabpanel"
                                                    aria-labelledby="payFour-tab">
                                                    <form id="bluepoint-form"
                                                        action="{{ isset($paymentLink) ? $paymentLink : '/payment' }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="form-row my-pt-1 red-ball-bg">
                                                            <div class="form-group col-md-6 my-pb-1">
                                                                <label for="cart-num">Total Point
                                                                    Needed<span>*<span></label>
                                                                <input type="text" class="form-control"
                                                                    id="purchase_point" name="purchase_point"
                                                                    placeholder="0 Blue Points" readonly
                                                                    value="{{ array_sum(array_column($cartsItems, 'point')) }}">
                                                            </div>
                                                            <div class="form-group col-md-6 my-pb-1">
                                                                <label for="cart-num">Your Availabe
                                                                    Point<span>*<span></label>
                                                                <input type="text" class="form-control"
                                                                    id="available_point" name="available_point"
                                                                    placeholder="0 Blue Points" readonly
                                                                    value="{{ isset($user) ? $user->userInfo->user_points : null }}">
                                                            </div>
                                                            <div class="form-group col-md-6 my-pb-1">
                                                                <label for="cart-num">Your Total Blue
                                                                    Point<span>*<span></label>
                                                                <input type="text" class="form-control" id="cart-num"
                                                                    placeholder="10 Blue Points" disabled ca>
                                                            </div>
                                                        </div>
                                                        @include(
                                                            'shop.payment.bottom-payment-form',
                                                            ['type' => 'Healthpoint']
                                                        )
                                                    </form>
                                                </div>

                                                {{-- Duitnow QR --}}
                                                <div class="col-12 tab-pane" id="payFive" role="tabpanel"
                                                    aria-labelledby="col-12 payFive-tab">
                                                    <form id="duitnow-form"
                                                        action="{{ isset($paymentLink) ? $paymentLink : '/payment' }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="form-row my-pt-1 red-ball-bg">
                                                            <div class="form-group col-md-6 my-pb-1">
                                                                <img class="col-md-11"
                                                                    src="{{ asset('/images/icons/duitnow_formula_healthcare.jpg') }}"
                                                                    alt="">
                                                            </div>
                                                            <div class="form-group col-md-6 my-pb-1 ">
                                                                <label for="cart-num">Reference
                                                                    Number<span>*<span></label>
                                                                <input type="text" class="form-control mb-4"
                                                                    id="cart-num" name="reference_number"
                                                                    placeholder="Reference Number">
                                                                <label for="formFileLg" class="form-label">Upload
                                                                    Payment
                                                                    Proof<span>*<span></label>
                                                                <input type="file" name="upload" accept="image/*"
                                                                    id="fileUpload" placeholder="jpg/png"
                                                                    class="mb-4" />
                                                                <label for="cart-num">Payment
                                                                    Amount<span>*<span></label>
                                                                <input type="text" class="form-control"
                                                                    id="cart-amount" name="amount_duitnow"
                                                                    placeholder="Payment Amount">
                                                            </div>
                                                        </div>
                                                        @include(
                                                            'shop.payment.bottom-payment-form',
                                                            ['type' => 'Duitnow']
                                                        )
                                                    </form>
                                                </div>

                                                <!-- Metapoint Form -->

                                                <div class="col-12 tab-pane" id="paySix" role="tabpanel"
                                                    aria-labelledby="paySix-tab">
                                                    <div>
                                                        <div class="row">
                                                            <div class="col-11 p-0 mx-auto">
                                                                @if (isset($canMetaPoint) && $canMetaPoint)
                                                                    <form id="meta-point-form" action="/payment"
                                                                        method="POST" enctype="multipart/form-data">
                                                                        @csrf

                                                                        <div class="form-row my-2">
                                                                            <div class="col-12 mb-1 form-group">
                                                                                <label class="m-0"
                                                                                    for="reference_number">
                                                                                    <b>{!! $mpAmount['breakdown'] !!}</b>
                                                                                </label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-row my-2">
                                                                            <div class="col-12 mb-1 form-group">
                                                                                @include(
                                                                                    'shop.payment.hidden-address'
                                                                                )
                                                                                <label class="m-0"
                                                                                    for="reference_number"><b>Your Total
                                                                                        Available
                                                                                        MES eVoucher Cash</b></label>
                                                                                <input type="text"
                                                                                    class="form-control"
                                                                                    name="reference_number2"
                                                                                    id="reference_number2"
                                                                                    value="{{ number_format($mpAmount['total'] / 100, 2) }}"
                                                                                    readonly>
                                                                                <div
                                                                                    class=" valid-feedback feedback-icon">
                                                                                    <i class="fa fa-check"></i>
                                                                                </div>
                                                                                <div
                                                                                    class="invalid-feedback feedback-icon">
                                                                                    <i class="fa fa-times"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-row my-2">
                                                                            <div class="col-12 mb-1 form-group">
                                                                                <label class="m-0"
                                                                                    for="payment_proof"><b>Your MES
                                                                                        Voucher
                                                                                        Usage For This
                                                                                        Purchase</b></label>
                                                                                <?php
                                                                                $purchaseRounded = $cartsItems['finalTotal'] / 100;

                                                                                $minimumPointNeeded = $purchaseRounded;
                                                                                ?>

                                                                                <input type="text"
                                                                                    class="form-control"
                                                                                    value="{{ number_format(($cartsItems['finalTotal'] <= 0 ? 0 : $cartsItems['finalTotal']) / 100, 2) }}"
                                                                                    readonly>
                                                                            </div>
                                                                        </div>

                                                                        <div id="meta-point-not-enough"
                                                                            class="form-row mt-2"
                                                                            style="display: none;">
                                                                            <div class="col-12">
                                                                                <p class="mb-0 text-danger"
                                                                                    style="font-size: .9rem;">
                                                                                    We're sorry, your MES eVoucher Cash
                                                                                    is
                                                                                    insufficient for this purchase.
                                                                                    Please choose another payment method
                                                                                    or top up your
                                                                                    <a target="_blank"
                                                                                        href="{{ config('app.metapoint_baseurl') }}/shop/category/mes"
                                                                                        target="_blank">
                                                                                        MES eVoucher Cash </a>
                                                                                    to continue.
                                                                                    <a target="_blank"
                                                                                        href="{{ config('app.metapoint_baseurl') }}/shop/category/mes"
                                                                                        target="_blank">
                                                                                        Click here </a> to top up.
                                                                                </p>

                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" name="payment_option"
                                                                            value="Metapoint">
                                                                        <input type="hidden" name="meta_user_id"
                                                                            value="{{ $mpAmount['meta_user_id'] }}">

                                                                        <input type="hidden" name="payment_option"
                                                                            value="Metapoint">
                                                                        @include(
                                                                            'shop.payment.bottom-payment-form',
                                                                            ['type' => 'Metapoint']
                                                                        )

                                                                    </form>
                                                                @else
                                                                    <div class="form-row tab-content px-4">
                                                                        <div class="col-12 mb-1 form-group">
                                                                            <a href="{{ config('app.metapoint_baseurl') }}/shop/dashboard/connect-platform"
                                                                                target="_blank"
                                                                                class="btn btn-warning btn-block btn-payment-submit" style="width:auto">
                                                                                @if ($user_expired)
                                                                                <p class="mt-3">As a highly secure platform, MetaPoint has implemented a connection rechecking process to enhance online safety awareness for our customers.</p>
                                                                                <p>We kindly request you <b>login to your MES Account</b> to reconnect your MES account for our security token consolidation purpose.</p>
                                                                                @else
                                                                                Click To Join MES
                                                                                eVoucher Cash
                                                                                @endif
                                                                            </a>

                                                                            <div class=" valid-feedback feedback-icon">
                                                                                <i class="fa fa-check"></i>
                                                                            </div>
                                                                            <div
                                                                                class="invalid-feedback feedback-icon">
                                                                                <i class="fa fa-times"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                {{-- link Form --}}
                                                <div class="col-12 tab-pane" id="payDirect" role="tabpanel"
                                                    aria-labelledby="payDirect-tab">
                                                    <div id="generateLinkDiv">
                                                        <button type="button" class="btn red-btn"
                                                            onclick="generatePaymentLink({{ json_encode($cartsItems) }})">Generate
                                                            Payment Link</button>
                                                    </div>
                                                    <div id="spinnerDiv" class="spinner-grow text-primary my-3"
                                                        style="width: 3rem; height: 3rem;display:none;" role="status">
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                    <div id="copyLinkDiv" class="row red-ball-bg py-3"
                                                        style="display:none;">
                                                        <div class="col-md-12 col-12">
                                                            <i class="fas fa-check-circle fa-3x pb-2"></i>
                                                            <h5>Link Created Successfully</h5>
                                                            <p>Share this link to your customer</p>
                                                        </div>
                                                        <div class="col-xl-5 text-xl-right col-lg-5 text-lg-right col-md-4 col-12"
                                                            id="qrLink">
                                                        </div>
                                                        <div class="col-xl-4 col-lg-5 my-lg-auto col-md-12 col-12">

                                                            <input type="text" class="my-md-2 my-1 col-12"
                                                                id="payDirectLink" value="" readonly>
                                                            <a id='copy-button' href='#' class='my-tool-tip'
                                                                data-toggle="tooltip" data-placement="center"
                                                                title="Copied">
                                                                <i class='glyphicon glyphicon-link'></i>
                                                            </a>

                                                            <button type="button" class="btn red-btn" id=""
                                                                onclick="copyPayDirectLink()">Copy link</button>

                                                        </div>
                                                        <div class="col-md-12">


                                                        </div>

                                                    </div>
                                                </div>
                                            @endif

                                        </div>


                                    </div>
                                </div>
                                <br>
                                {{-- <br><div class="checkout-box"> --}}
                                <div id="available_banks" class="checkout-box"
                                    style="background: #f8fafc;display:none">
                                    <div class="col-12 py-3" style="text-align:center">
                                        <p style="color: grey">Available Banks and eWallets</p>
                                        @foreach ($availableBanks as $availableBank)
                                            <img style="width:100px;"
                                                src="{{ asset('storage/banks/') . '/' . $availableBank }}" alt="">
                                        @endforeach
                                    </div>
                                    <div class="col-12 p-3 mx-auto" style="text-align:center">
                                        <p style="color: grey">Launching soon</p>
                                        @foreach ($launchingBanks as $launchingBank)
                                            <img style="width:100px;"
                                                src="{{ asset('storage/launching-banks/') . '/' . $launchingBank }}"
                                                alt="">
                                        @endforeach
                                    </div>
                                </div>
                                {{-- </div> --}}

                            </div>
                            <div class="col-12 col-md-4">
                                <div class="checkout-box">
                                    <div class="order-box pay red-ball-bg">
                                        <div class="shopping-tittle">
                                            <p>Order Summary</p>
                                        </div>
                                    </div>
                                    <div class="total-price light-bg my-mb-1">
                                        <table class="table tb-price table-hover">
                                            <tbody>
                                                <tr>
                                                    <td class="price-details-1">Subtotal (Price)</td>
                                                    <td class="price-details-2">MYR
                                                        {{ number_format(array_sum(array_column($cartsItems['items'], 'subtotalPrice')) / 100, 2) }}
                                                    </td>
                                                </tr>
                                                {{-- <tr>
                                                    <td class="price-details-1">Subtotal (Points)</td>
                                                    <td class="price-details-2">
                                                        {{ array_sum(array_column($cartsItems,'subtotalPoint')) }} pts
                                                    </td>
                                                </tr> --}}
                                                <tr>
                                                    <td class="price-details-1">Shipping Fee</td>
                                                    <td class="price-details-2">MYR
                                                        {{ number_format($cartsItems['shippingFee']['amount'] / 100, 2) }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="price-details-1">Discount</td>
                                                    <td class="price-details-2">- MYR
                                                        {{ number_format($cartsItems['discount'] / 100, 2) }}
                                                    </td>
                                                </tr>

                                                {{-- <tr>
                                                    <td class="price-details-1">Available Points</td>
                                                    <td class="price-details-2">
                                                        {{ number_format($user->userInfo->user_points) }}
                                                    </td>
                                                </tr> --}}
                                                <tr class="hr-line-cart">
                                                    <td class="price-details-red1">Total </td>
                                                    <td class="price-details-red2">MYR
                                                        {{ number_format(($cartsItems['finalTotal'] <= 0 ? 0 : $cartsItems['finalTotal']) / 100, 2) }}
                                                        {{-- $cartsItems['finalTotalPoint'] --}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="pay-pd-scroll">
                                        @foreach ($cartsItems['items'] as $key => $cartsItem)
                                            <div class="row pay-itemlist m-0">
                                                <div class="col-4 col-sm-4">
                                                    <img src="{{ asset('/storage' . $cartsItem['images']) }}"
                                                        class="product-itemimg" alt="">
                                                </div>
                                                <div class="col-8 col-sm-8">
                                                    <ul>
                                                        <li>
                                                            <p>{{ $cartsItem['name'] }}</p>
                                                        </li>
                                                        <li>
                                                            <p>{{ $cartsItem['attributes']['product_size'] }}

                                                            </p>
                                                            {{-- <p class="m-0" id="point">{{ ( $cartsItem['point']) }} </p>
                                                                        <p class="m-0" id="half_point">{{ ( $cartsItem['half_point']) }} </p>
                                                                        <p class="m-0" id="half_cash">{{ ( $cartsItem['half_cash']) }} </p> --}}

                                                        </li>
                                                        <li>
                                                            <p>
                                                                @if (array_key_exists('product_rbs_frequency', $cartsItem['attributes']))
                                                                    <br>
                                                                    <span style="color:red;">*</span> Deliver every
                                                                    {{ $cartsItem['attributes']['product_rbs_frequency'] }}
                                                                    month(s) for
                                                                    {{ $cartsItem['attributes']['product_rbs_times'] }}
                                                                    times.
                                                                @endif
                                                            </p>
                                                            {{-- <p class="m-0" id="point">{{ ( $cartsItem['point']) }} </p>
                                                                        <p class="m-0" id="half_point">{{ ( $cartsItem['half_point']) }} </p>
                                                                        <p class="m-0" id="half_cash">{{ ( $cartsItem['half_cash']) }} </p> --}}
                                                        </li>
                                                        <li class="price-qlt">
                                                            <p>X<span>{{ $cartsItem['quantity'] }}</span></p>
                                                            <p>
                                                                @if ($cartsItem['subtotalPrice'] != 0)
                                                                    <span
                                                                        class="itemSubtotalPrice{{ $key }}">{{ 'MYR ' . number_format($cartsItem['subtotalPrice'] / 100, 2) }}</span>
                                                                @elseif ($cartsItem['freegift'] == 1)
                                                                    <span class="text-bold">Free Gift ~ </span>
                                                                    MYR 0.00
                                                                @else
                                                                @endif
                                                                {{-- {{ $cartsItem['subtotalPrice'] != 0 && $cartsItem['subtotalPoint'] != 0  ? '+' :''}}
                                                            @if ($cartsItem['subtotalPoint'] != 0)
                                                            <span class="itemSubtotalPoint{{ $key }}">{{ $cartsItem['subtotalPoint'] . ' pts' }}</span>
                                                            @endif --}}
                                                            </p>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="hr-line-ccpay"></div>
                                            </div>
                                        @endforeach


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="termsNconditions" aria-hidden="true" aria-labelledby="termsNconditionsLabel"
    tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
        <div class="modal-content edit-info-modal-box">
            <div class="modal-header">
                <h1 class="modal-title edit-tittle" id="termsNconditionsLabel">Return and Refund Policy</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body pb-0">
                <p style="text-align: justify;">
                <ol>
                    <li style="text-align: justify;">
                        This Return and Refund policy shall only be applicable to home delivery shopping via FHSB Online
                        only.
                    </li>
                    <li style="text-align: justify;">
                        FORMULA HEALTHCARE SDN BHD (FHSB) reserves the rights to cancel any orders if the Product is not
                        available for any reasons whatsoever. We will notify you and refund any payment that you have
                        made. The refund will be credited into your credit card or bank account depending on the period
                        of time your financial institution required to arrange for the refund.
                    </li>
                    <li style="text-align: justify;">
                        You are not allowed to cancel the order after an order confirmation email has been sent to you
                        and/or the parcel has been collected by the courier rider.
                    </li>
                    <li style="text-align: justify;">
                        FHSB will use its best efforts to ensure that orders are correctly fulfilled. Should there be
                        any discrepancy of the Products delivered and the Customer wishes to return and to seek for
                        refund, please notify us by completing and submitting the online form posted in our website
                        within seven (7) days upon receiving the Products, failing which we reserve the rights to reject
                        the claim/request.
                    </li>
                    <li style="text-align: justify;">
                        We will contact the Customer within seven (7) days after receipt of the duly completed online <a
                            href="/shop/return-form" style="color:red;">form</a> and will inform the Customer of the
                        procedures in respect of return of the Products and refund of payment. Therefore, the Customer
                        is advised to wait for our instruction for the next course of action. In the event that the
                        Customer does not receive any reply from us after the aforesaid period, please contact us
                        immediately either by e-mail at formula2u-cs@delhubdigital.com for enquiries.
                    </li>
                    <li style="text-align: justify;">
                        Upon confirmation of the Return/Refund item, the Customer is required to send back the relevant
                        Product after making the necessary arrangement with our Customer Service Centre.
                    </li>
                    <li style="text-align: justify;">
                        You are fully responsible for the risks and the condition of the Product (to be returned) until
                        it is received by us. We shall hold no liability for any loss or damage to the Product before
                        the Product is received by us. You are advised to pack the Product safely to prevent any loss or
                        damage to the Product or its box or its original packaging.
                    </li>
                    <li style="text-align: justify;">
                        All return/exchange of Products or refund can be arranged under the following reasons:
                        <ul>
                            <li style="text-align: justify;">
                                If the Product delivered is damaged or in defective condition: or
                            </li>
                            <li style="text-align: justify;">
                                If the Product is near to expiry or has expired; or
                            </li>
                            <li style="text-align: justify;">
                                If the Product is different from the order made by the Customer. The Products shall be
                                returned in its original condition, quantity and packing as it first delivered to the
                                Customer together with proof of purchase.
                            </li>
                        </ul>
                    </li>
                    <li style="text-align: justify;">
                        Notwithstanding to clause 8 above, the return of a Product by a Customer does not automatically
                        warrant for a refund. If one or more of the conditions in clause 8 above is fulfilled, you are
                        entitled to elect either to request for exchange of the Product (subject to availability of the
                        Product in our store), or to seek for a refund to be made to you through your payment account
                        (credit card/debit card/bank account), within 60 days from the approved return date. FHSB shall
                        not be liable to any loss, damage, cost or expense that you or any person may incur as a result
                        of any delay caused/contributed by your financial institution in processing the said refund.
                    </li>
                    <li style="text-align: justify;">
                        FHSB will reimburse its Customer of the courier charges/expenses incurred for returning of the
                        Products to us provided always that the courier payment slip/receipt is furnished to us upon
                        seeking for refund.
                    </li>
                    <li style="text-align: justify;">
                        Products/Items that are classified under medical devices, disposable items, one time used items
                        are strictly not exchangeable or returnable for hygienic purposes.
                    </li>
                    <li style="text-align: justify;">
                        Any Product (which has been ordered and the relevant invoice has also been sent to the Customer)
                        is not included in the delivery, FHSB shall either refund the Customer the amount paid for that
                        particular Product, or replace it with any Product with same or lesser value selected by the
                        Customer.
                    </li>
                    <li style="text-align: justify;">
                        If the Customer has been charged for a Product which has not been delivered, FHSB will refund
                        the money paid by the Customer..
                    </li>
                    <li style="text-align: justify;">
                        Any Product which is not included in the Customer's Order and has been delivered to the
                        Customer, FHSB shall collect the relevant Product from the Customer.
                    </li>
                    <li style="text-align: justify;">
                        If a Customer is charged for more than the value of a Product as displayed on the site, FHSB
                        will refund the differential sum. to the Customer.
                    </li>
                    <li style="text-align: justify;">
                        The Customer is required to send back the affected Product(s) to us via a reliable courier
                        service company to our FHSB Distribution Center at the following address:-
                    </li>
                </ol>
                </p>
                <p class="d-block d-sm-none" style="text-align: center;">
                    1-26-05, Menara Bangkok Bank,<br>Berjaya Central Park,<br>No 105, Jalan Ampang,<br>50450 Kuala
                    Lumpur, Malaysia.
                </p>
                <p class="d-none d-sm-block" style="text-align: center;">
                    1-26-05, Menara Bangkok Bank, Berjaya Central Park,<br>No 105, Jalan Ampang, 50450 Kuala Lumpur,
                    Malaysia.
                </p>
            </div>
            <div class="edit-opt-btn modal-footer">
                <button class="savechg-btn btn-primary" data-bs-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<!-- Spanner Modal -->
<div class="modal fade" id="spannerModel" tabindex="-1" role="dialog" aria-labelledby="spannerModelLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="spanner">
                <div class="loader"></div>
                <p>Preparing your order, please be patient.</p>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                Modal body..
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!-- Link Expired -->
{{-- <div class="modal fade" id="expiredLink" tabindex="-1" role="dialog" aria-labelledby="expiredLinkLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <p>Seems like your link is already expired. Please ask your agent to create new link.</p>
        </div>
    </div>
</div> --}}

<div class="modal fade py-2" id="expiredLink" aria-hidden="true" aria-labelledby="expiredLinkLabel" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered edit-popup-bg">
        <div class="modal-content edit-info-modal-box pt-5">
            <div class="modal-body pb-0">
            </div>
            <div class="edit-opt-btn modal-footer">
                <h5 class="pb-4 pt-4">Seems like your link is already expired. Please ask your agent to create new
                    link.
                </h5>
            </div>
        </div>
    </div>
</div>

<div class=""></div>
@endsection

@push('style')
<style>
    .errorfpxbank {
        display: none;
        color: red;
    }

    .errorfpxbanklists {
        display: none;
        color: red;
    }

    .paymentDisabled {
        filter: grayscale(100%);
    }

    /* .pay-itembox {
        height: 120px;
    } */
    .spanner {

        text-align: center;
        color: #000;

    }

    .loader,
    .loader:before,
    .loader:after {
        border-radius: 50%;
        width: 2.5em;
        height: 2.5em;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation: load7 1.8s infinite ease-in-out;
        animation: load7 1.8s infinite ease-in-out;
    }

    .loader {
        color: #263A8B;
        font-size: 10px;
        margin: 80px auto;
        position: relative;
        text-indent: -9999em;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }

    .loader:before,
    .loader:after {
        content: '';
        position: absolute;
        top: 0;
    }

    .loader:before {
        left: -3.5em;
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }

    .loader:after {
        left: 3.5em;
    }




    @-webkit-keyframes load7 {

        0%,
        80%,
        100% {
            box-shadow: 0 2.5em 0 -1.3em;
        }

        40% {
            box-shadow: 0 2.5em 0 0;
        }
    }

    @keyframes load7 {

        0%,
        80%,
        100% {
            box-shadow: 0 2.5em 0 -1.3em;
        }

        40% {
            box-shadow: 0 2.5em 0 0;
        }
    }

    .errorleft {
        text-align: left;
    }

</style>
@endpush

@push('script')
<script>
    $(document).ready(function() {

        let fg_popup = 0;

        if (localStorage.fg_popup) {
            fg_popup = localStorage.getItem('fg_popup');
        }

        listProduct = JSON.parse($('#listProduct').val());
        if (listProduct.length < 1) {
            window.location.href = "/shop/cart";
        }
        $('#spannerModel').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#expiredLink').modal({
            backdrop: 'static',
            keyboard: false
        });

        if ('{{ $expiredLink }}') {
            $('#expiredLink').modal('show');
        }

        if ({{ $isfreegift }} > 0 && fg_popup == 0) {
            $('#freegift').modal('show');
        }

        //Metapoint
        let metaPointEnough = 0;
        let currentMetaPoint = "{{ $mpAmount['total'] / 100 }}";
        let requiredMetaPoint = "{{ $minimumPointNeeded }}";
        console.log(currentMetaPoint);
        if (+currentMetaPoint >= +requiredMetaPoint) {
            metaPointEnough = 1;
        } else {
            metaPointEnough = 0;

        }
        if (metaPointEnough == 0) {
            $('#submit_button').prop('disabled', true);
            $('#meta-point-not-enough').show();

        }
    });

    // Form Validation - Credit/Debit Card.
    let cardNumber = null;
    let ccIcon = $('.valid-feedback.feedback-icon.with-cc-icon i');
    // Card Number.
    $('#card_number').on('input', function() {
        cardNumber = $(this).val();
        cardType = detectCardType(cardNumber);

        $('#card-type').val(cardType);
        // card-type

        if ($(this).val().length < 7 || cardType == undefined) {
            // $(this).removeClass('is-valid');
            // $(this).addClass('is-invalid');
            $('#card-type').val(0);

        } else {
            // $(this).removeClass('is-invalid');
            // $(this).addClass('is-valid');
            ccIcon.removeClass();
            ccIcon.addClass('fa fa-cc-' + cardType).css('font-size', '30px');
            $('#card_type').val(cardType);
        }
    });

    $('#card-type').on('change', function() {
        //     //
        cardNumber = $('#card_number').val();
        cardType = detectCardType(cardNumber);

        cardChoice = $(this).val();

        if ($(this).val() != cardType) {
            $('#card_number').removeClass('is-valid');
            $('#card_number').addClass('is-invalid');
        } else {
            $('#card_number').removeClass('is-invalid');
            $('#card_number').addClass('is-valid');
        }
    });

    function detectCardType(number) {
        var re = {
            // electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            // maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            // dankort: /^(5019)\d+$/,
            // interpayment: /^(636)\d+$/,
            // unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/,
            // amex: /^3[47][0-9]{13}$/,
            // diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            // discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            // jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        }

        for (var key in re) {
            if (re[key].test(number)) {
                return key
            }
        }
    }
    $('.clickOnce').click(function() {
        var form = $(this).data('form');
        if ($('.radio' + form).is(':checked')) {
            $('.aggreement' + form).hide();

        } else {
            $('.aggreement' + form).show();
            return false;
        }
    });
    // Form Validation card

    $("#voucher-form").validate({
        submitHandler: function(form) {
            $(this).prop('disabled', true);
            $('#spannerModel').modal('show');
            setTimeout(function() {
                form.submit()
            }, 1000);
        },
        messages: {
            // card_number: {
            //     required: "Please enter a card number.",
            // }
        },
    });

    $("#cybersource-form").validate({
        rules: {
            card_number: {
                required: true,
                digits: true,
                minlength: 7
            },
            card_holder: {
                required: true
            },
            card_expiry: {
                required: true,
                date: true
            },
            card_cvv: {
                required: true,
                minlength: 3,
                digits: true
            },
            tncRadio_Cybersource: { // <- NAME of every radio in the same group
                required: true
            }
        },
        submitHandler: function(form) {
            submitForm(form);
        },
        messages: {
            // card_number: {
            //     required: "Please enter a card number.",
            // }
        },
    });

    // Form Validation offline
    $("#offline-form").validate({
        rules: {
            reference_number: {
                required: true
            },
            amount: {
                required: true,
                number: true
            },
            upload: {
                required: true
            }

        },
        messages: {
            reference_number: {
                required: "Please enter reference number.",
                digits: "Please enter number only."
            }
        },
        submitHandler: function(form) {
            submitForm(form);
        }
    });

    // Form Validation duitnow
    $("#duitnow-form").validate({
        rules: {
            duitnow: {
                required: true
            },
            amount_duitnow: {
                required: true,
                number: true
            },
            upload: {
                required: true
            }
        },
        messages: {
            duitnow: {
                required: "Please enter reference number.",
                digits: "Please enter number only."
            }
        },
        submitHandler: function(form) {
            submitForm(form);
        }
    });

    // Form Validation duitnow
    $("#fpx-form").validate({
        debug: true,
        rules: {
            //
        },
        messages: {
            //
        },
        submitHandler: function(form) {
            var submitFPX = checkBankSelected();
            if (submitFPX == true) {
                submitForm(form);
            }
        }
    });

    function submitForm(form) {
        $(this).prop('disabled', true);
        $('#spannerModel').modal('show');
        checkPaymentLink(form);
    }

    function checkBankSelected() {
        var bankRadioOptions = $('input[name=bankRadioOptions]:checked').val();
        var fpxTNC = $('#fpx_tncFpx');

        if (bankRadioOptions == undefined) {
            $('.errorfpxbank').show();
            return false;
        } else {
            $('.errorfpxbank').hide();
            var bankDropdown = $('#bankSelected' + bankRadioOptions).val();
            if (bankDropdown == 'default') {
                $('.errorfpxbanklists').show();
                return false;
            } else {
                $('.errorfpxbanklists').hide();
                return true;
            }
        }
    }

    function getfreegift(val) {
        fg_popup = val;
        localStorage.setItem('fg_popup', fg_popup);
    }

    function hideModal(id) {
        $('#' + id).modal('hide');
    }

    $('.pay-itembox').click(function name() {

        $('input[name=payment-mh]').removeAttr('checked');

        let input = $(this).find('input').attr('checked', true);

        if ($(this).find('input').val() == 'pay05') {
            $('#available_banks').show();
        } else {
            $('#available_banks').hide();
        }
    });

    $('.card-itembox').click(function name() {

        $('input[name=cardChoices]').removeAttr('checked');

        let input = $(this).find('input').attr('checked', true);

    });
    $('.fpx-itembox').click(function name() {

        $('input[name=bankRadioOptions]').removeAttr('checked');

        let input = $(this).find('input').attr('checked', true);

    });

    function copyPayDirectLink() {
        var copyText = document.getElementById("payDirectLink");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        $("a.my-tool-tip").attr('data-original-title', 'Copied');
        $("a.my-tool-tip").tooltip('show');
        // alert("Copied the text: " + copyText.value);
    }

    function checkPaymentLink(form) {

        var directLink = '{{ request()->directID }}'
        console.log(directLink);
        if (directLink !== '') {
            $.ajax({
                url: '/direct-pay/check/{{ request()->directID }}',
                type: 'GET',
                beforeSend: function() {
                    //
                },
                success: function(success) {
                    //
                    console.log(success);

                    if (success !== 'true') {
                        $('#spannerModel').hide();
                        $('#expiredLink').modal('show');
                    } else {
                        setTimeout(function() {
                            form.submit()
                        }, 1000);
                    }
                },
                error: function() {
                    alert("error in loading");
                }

            });
        } else {
            setTimeout(function() {
                form.submit()
            }, 1000);
        }



    }

    function generatePaymentLink(cartItems) {
        $.ajax({
            url: '/direct-pay/save',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "cartItems": cartItems,
                //
            },
            beforeSend: function() {
                $('#generateLinkDiv').hide();
                $('#spinnerDiv').show();
            },
            success: function(success) {
                //
                console.log(success);

                $('#qrLink').append(success.paymentLink);
                $('#payDirectLink').val(success.inputLink);

                setTimeout(function() {
                    $('#spinnerDiv').hide();
                    $('#copyLinkDiv').show();
                }, 1000);


            },
            error: function() {
                alert("error in loading");
            }

        });
    }
</script>
@endpush
