@extends('layouts.administrator.mainVue')

@section('pageTitle', 'Free Gift List')

@section('content')
<div class="container-fluid flex-grow-1 container-p-y">

    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light"> Outlet / </span> List
    </h4>

    <div class="row">
        <div class="col-md-12">
          <div class="card mb-4">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h2>Free Gift List</h2>
                        </div>
                        <div class="float-right mb-3">
                            <a class="btn btn-success" href="{{ route('freegift.create') }}"> Create New</a>
                        </div>
                    </div>
                </div>

                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                  <span>{{ $message }}</span>
                </div>
                @endif

                <table class="table table-bordered table-striped">
                 <tr>
                   <th>No</th>
                   <th>Outlet ID</th>
                   <th>Name</th>
                   <th>Gifted By</th>
                   <th>Contains</th>
                   <th>Quantity</th>
                   <th>Type</th>
                   <th>Gift Product</th>
                   <th width="280px">Action</th>
                 </tr>
                 @foreach ($data as $k => $v)
                  <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $v->outlet_id }}</td>
                    <td>{{ $v->gift_name }}</td>
                    <td>{{ $v->gifted }}</td>
                    <td>{{ $v->contains }}</td>
                    <td>{{ $v->quantity }}</td>
                    <td>{{ $v->gift_type }}</td>
                    <td>{{ $v->gift_product }}</td>
                    <td>
                       <a class="btn btn-primary" href="{{ route('freegift.edit',$v->id) }}">Edit</a>
                        {!! Form::open(['method' => 'PUT','route' => ['freegift.update', $v->id],'style'=>'display:inline']) !!}
                            <input class="btn {{ $v->status == 1 ? 'btn-danger' : 'btn-info' }}" type="submit" value="{{ $v->status == 1 ? 'Disabled' : 'Enabled' }}">
                        {!! Form::close() !!}
                    </td>
                  </tr>
                 @endforeach
                </table>

                {!! $data->render() !!}

            </div>
            </div>
        </div>
    </div>
</div>
@endsection
