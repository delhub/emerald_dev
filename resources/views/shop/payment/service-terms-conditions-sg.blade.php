<h5 class="header">Formula Healthcare Sdn Bhd Terms & Condition</h5>

<p class="paragraph">
    The following terms and conditions shall govern the service(s) to be provided and/or product(s) to be sold to you by Formula Healthcare Sdn Bhd ("the Company") and the approved vendors of the Company (the Company and the approved vendors of the Company shall collectively be referred to as “the Vendor(s)”) under the Company’s programme/platform known as "<strong>Formula Healthcare Sdn Bhd</strong>". Please read through the following terms and conditions before placing your order. Any payment made to us shall be construed as your acceptance to all terms and conditions stated below:
</p>

<ol class="px-3 py-2">
    <li>
        <p class="paragraph">
            All product(s) and/or service(s) made available through <strong>Formula Healthcare Sdn Bhd</strong> may be designed for, and only appropriate for, specialized uses; accordingly, you may only use them as intended by, and in compliance with all instructions provided by the relevant Vendor(s) .
        </p>
    </li>

    <li>
        <p class="paragraph">
            Upon receiving an order form/invoice from you, the Company will forward your request to the relevant Vendor(s), indicating specific product(s) and services(s), quantity, price, total purchase price, method of delivery, requested delivery dates, intended commencement and completion dates (for services), and any other special instructions (collectively be referred to as “Purchase Order”). Thereafter, the relevant Vendor(s) will liaise with the customer directly to confirm the order and to resolve technical and/or other relevant issues (if any). Then, the Company shall receive an order confirmation from both the relevant Vendor(s) and the customer. <strong>No cancellation shall be allowed after an order has been confirmed by the relevant Vendor(s). All payments made by the customers are strictly non-refundable and non-transferable for any reason whatsoever.</strong>
        </p>
    </li>

    <li>
        <p class="paragraph">
        Upon receiving full payment from the customer, the Company will notify the relevant Vendor(s) to arrange for delivery of product(s) or commence of the service(s) based on the terms agreed between parties. All purchases made by the customer through Formula Healthcare Sdn Bhd are direct sale and purchase between you and the relevant Vendor(s). Where product(s) are delivered to you directly by the relevant Vendor(s), the  relevant Vendor(s) will call you to discuss a delivery date. Deliveries of product(s) will generally take a minimum of fourteen (14) working days. Delivery of larger product(s) is made between Monday to Friday during normal working hours. Therefore, it is your responsibility to ensure the delivery address is ready and able to accept delivery of the product(s), in particular that there is space for any delivery vehicle to make the delivery. As such, the customer shall cooperate with the relevant Vendor(s) at all times.
        </p>
    </li>

    <li>
        <p class="paragraph">
        The Vendor(s) will use all reasonable endeavours to meet any delivery dates for the service(s) to be provided to you but such dates are estimates only and, unless otherwise expressly agreed by the Vendor(s) in writing, are not binding on the Vendor(s).
        </p>
    </li>

    <li>
        <p class="paragraph">
        The customer shall at all times during continuance of the agreement:-
        <ol class="sub-list">
            <li>
                <p class="paragraph">
                obtain and maintain all consents, permissions and licenses necessary to enable the relevant Vendor(s) (which includes its employees, agents and sub-contractors) to perform its obligations under this confirmed order;
                </p>
            </li>

            <li>
                <p class="paragraph">
                provide sufficient and accurate information and materials to the relevantVendor(s) as reasonably requested by the relevant Vendor(s) (which includes its employees, agents and sub-contractors) in the provisions of the service(s) and performance of its obligation under this confirmed order;
                </p>
            </li>

            <li>
                <p class="paragraph">
                provide access to premises, systems and other facilities which may be reasonably required by the relevant Vendor(s) (which includes its employees, agents and sub-contractors); and
                </p>
            </li>

            <li>
                <p class="paragraph">
                    ensure that all necessary safety and security precautions are in place.
                </p>
            </li>
        </ol>
        </p>
    </li>

    <li>
        <p class="paragraph">
        All product(s) and service(s) are subject to inspection and test by the Company to ensure that they comply with the requirements/specifications set by the Company.


        </p>
    </li>

    <li>
        <p class="paragraph">
        The Company reserves the right to request the relevant Vendor(s) to change/reschedule any delivery of goods or performance of service(s), or cancel any confirmed order fourteen (14) days before confirmed delivery/performance date. In such event, the Company shall not be subject to any charges, compensation or damages as a result of such cancellation or changes.
        </p>
    </li>

    <li>
        <p class="paragraph">
        Further, the Company reserves the right to cancel an order at any time the relevant Vendor(s) fails to comply with the terms and conditions hereof, the specific instructions as per the confirmed order, the relevant Vendor becomes bankrupt or insolvent, the business of the relevant Vendor(s) is placed under a receiver, assignee or trustee.
        </p>
    </li>

    <li>
        <p class="paragraph">
        Any complaints in relation to the product(s) and/or service(s) shall be made directly to the relevant Vendor(s) in writing within fourteen (14) days from the date of acceptance of the products(s) and/or completion of the service(s). The customer shall give sufficient details of any manufacturing defects on the products or errors to the relevant Vendor(s) and the relevant Vendor(s) shall use its reasonable endeavours to remedy such manufacturing defects based on the same standard provided to all customers or errors within agreed period of time provided that the customer shall have complied with these terms and conditions at all times.
        </p>
    </li>

    <li>
        <p class="paragraph">
        Except as expressly otherwise provided herein, no warranty, condition, undertaking or term, expressed or implied, statutory or otherwise as to the condition, quality, effect, performance or fitness for purpose of the product(s) or service(s) is given by the Vendor(s).
        </p>
    </li>

    <li>
        <p class="paragraph">
        The Company shall not be responsible/liable for all matters in relation to purchase, manufacture, fabricate, produce, provide, supply and delivery of all product(s) and/or service(s) purchased by the customers from the approved vendorsthrough <strong>Formula Healthcare Sdn Bhd</strong>. Further, the Company shall not be held liable for any claims due to delays, defects, disputes arising between the customers and the approved vendors. The customers shall also release the Company (including its directors, officers, agents, subsidiaries, associates and employees) from claims, demands and damages arising out of or in any way related to the disputes with the approved vendors.
        </p>
    </li>

    <li>
        <p class="paragraph">
        We respect your privacy and therefore, we will use the information that you provide to us for purposes of fulfilment of this order, accounting, billing and auditing, checking credit or other payment cards, administrative and legal purposes, statistical analysis, and helping in any future dealings with you only. For these purposes, by placing this order with us, you authorise us to retain and use your personal information and to transmit it to our offices, authorized dealers and third party business associates, government agencies or the providers of the services mentioned above. In any event, we will not disclose your personal information to any other third party (other than the above mentioned) unless required to do so under the law.
        </p>
    </li>

    <li>
        <p class="paragraph">
        All content such as images, photos, videos, graphics, code, information or data uploaded, displayed, transmitted or made available to you though <strong>Formula Healthcare Sdn Bhd</strong> remains the property of the Company and are protected by the copyright laws. As such, you are not permitted to copy, publish, manipulate or otherwise reproduce any content appears on <strong>Formula Healthcare Sdn Bhd</strong> at all times.
        </p>
    </li>

    <li>
        <p class="paragraph">
        The terms and conditions set forth the entire agreement between the parties pertaining to the subject matter hereof and supersedes all other oral and/or written agreements and understandings, express or implied. If any of the terms and conditions above is determined to be invalid, illegal, or unenforceable, such provisions shall be modified only to the extent necessarily to make such provisions enforceable, and the remaining provisions are still valid, legal, and enforceable.
        </p>
    </li>

    <li>
        <p class="paragraph">
        The Company reserves the right, without any liability, to amend, vary or add any of these terms and conditions without prior notice to you. Therefore, we recommend our customers to contact us directly should they have any queries.
        </p>
    </li>

    <li>
        <p class="paragraph">
        These terms and conditions shall be read together with the order form/invoice submitted or to be submitted by you and other relevant documents of the Company, if any. These terms and conditions shall be supplemental to our order form/invoice.
        </p>
    </li>

    <li>
        <p class="paragraph">
        Neither party shall be liable for any delay or failure in performance or obligations due to events outside the defaulting party´s reasonable control under this Agreement due to any Act of God, fire, casualty, flood, earthquake, war, strike, terrorism, lockout, epidemic, destruction of production facilities, riot, actions of government entities, insurrection, material unavailability, or any other cause beyond the reasonable control of the party invoking this section, and if such party shall have used its commercially reasonable efforts to mitigate its effects, such party shall give prompt written notice to the other, its performance shall be excused, and the time for the performance shall be extended for the period of delay or inability to perform due to such occurrences. Regardless of the excuse of Force Majeure, if a party is not able to perform within ninety (90) calendar days after such event, the other may terminate this Agreement. This does not apply to outstanding invoices.
        </p>
    </li>

    <li>
        <p class="paragraph">
        All requests or complaints shall be made to us in writing and the same shall be served at our main office as follows:-
        <p class="paragraph ml-2">
            <strong>{{country()->company_name}}</strong>
            <br>
            {!!country()->company_address!!}
        </p>
        </p>
    </li>

    <li>
        <p class="paragraph">
        You may not assign or sub-contract any of your rights or obligations under these terms to any person without our prior written consent. No third party shall be entitled to enforce any of these terms.
        </p>
    </li>

    <li>
        <p class="paragraph">
        The terms and conditions stated above shall be governed by and interpreted in accordance with the laws of Singapore and each party hereby agrees to submit to the exclusive jurisdiction of the Courts of Singapore.
        </p>
    </li>
</ol>
