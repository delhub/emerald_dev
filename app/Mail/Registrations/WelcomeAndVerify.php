<?php

namespace App\Mail\Registrations;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeAndVerify extends Mailable
{
    use Queueable, SerializesModels;

    public $url;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($url, $user)
    {
        $this->url = $url;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to Formula Healthcare!')
            ->markdown('emails.registrations.welcome-and-verify')
            ->with('url' . $this->url)
            ->with('user', $this->user);
    }
}
