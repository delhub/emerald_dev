<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\Purchases\Order;
use App\Http\Controllers\Administrator\Warehouse\InventoryController;

class CreateWhPickingTableInsertSomeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->tinyInteger('pick_pack_status')->after('payment_type')->default(0);
        });

        Schema::create('pick_bin', function (Blueprint $table) {
            $table->id();
            $table->string('bin_number');
            $table->tinyInteger('using')->default(0);
            $table->bigInteger('picker')->nullable();
            $table->timestamps();
        });

        Schema::create('wh_picking', function (Blueprint $table) {
            $table->id();
            $table->string('batch_number');
            $table->string('bin_id');
            $table->bigInteger('picker');
            $table->bigInteger('packer')->default(0);
            $table->tinyInteger('pick_pack_status')->default(0);
            $table->timestamps();
        });

        Schema::create('wh_picking_order', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('key');
            $table->string('delivery_order');
            $table->timestamps();
        });

        //insert pick counter
        DB::table('counters')->insert(
            array(
                array('counter_types' => 'pick', 'country_id' => 'MY', 'counter_value' => '0', 'counter_prefix' => 'WHP', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        //update old orders the 5 (ignore)
        $orders = Order::whereIn('orders.order_status', ['1000', '1002', '1003', '1004'])->get();
        foreach ($orders as $key => $order) {
            $order->pick_pack_status = 5;
            $order->save();
        }

        //update all old orders to reserved investory
        $orders = Order::whereNotIn('orders.order_status', ['1000', '1002', '1003', '1004'])->get();

        foreach ($orders as  $order) {
            foreach ($order->items as  $item) {

                $productIds[] = $item->product_id;

                $inventoryRecords[$item->product_code]['outlet_id'] = $order->delivery_outlet;
                $inventoryRecords[$item->product_code]['inv_type'] = ($order->order_status == 1001) ? 8001 : 8011;
                // $inventoryRecords[$item->product_code]['inv_status'] = 'incomplete';
                $inventoryRecords[$item->product_code]['inv_amount'] = - ($item->quantity);
                $inventoryRecords[$item->product_code]['inv_user'] = $order->purchase_id;
                $inventoryRecords[$item->product_code]['model_type'] = 'App\Models\Purchases\Order';
                $inventoryRecords[$item->product_code]['model_id'] = $order->id;
                $inventoryRecords[$item->product_code]['product_code'] = $item->product_code;
                $inventoryRecords[$item->product_code]['inv_reference'] = 'starting migration';

                //manual create wh_inventories
                DB::table('wh_inventories')->insert([
                    'outlet_id' => $order->delivery_outlet,
                    'inv_type' => ($order->order_status == 1001) ? 8001 : 8011,
                    'inv_amount' => - ($item->quantity),
                    'inv_user' => $order->purchase_id,
                    'model_type' => 'App\Models\Purchases\Order',
                    'model_id' => $order->id,
                    'product_code' => $item->product_code,
                    'inv_reference' => 'starting migration',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            //manual create wh_log
            $logs_table = DB::table('wh_logs')->insert([
                'model_type' => 'App\Models\Products\WarehouseInventories',
                'model_id' => $order->id,
                'log_status' => 'created',
                'log_user' => $order->purchase->user_id,
                'log_data' => json_encode($inventoryRecords),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }


        //insert new global_statuses
        DB::table('global_statuses')->insert(
            array(
                array('id' => 8011, 'name' => 'Purchase Order - Stock Hold (Offline)', 'description' => 'Product for purchase order to blocked / hold stock (Offline Payment)', 'status_type' => 'inventory_status', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()),
            )
        );

        DB::statement("
        CREATE OR REPLACE VIEW view_inventories_stock
        AS
        SELECT product_code,

        sum(case when inv_type = '8010' OR inv_type = '8000'
        then inv_amount
        else 0
        END)
        AS physical_stock,

        sum(case when inv_type = '8001'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved,

        sum(case when inv_type = '8011'
        then inv_amount
        else 0
        end)
        as virtual_stock_reserved_offline,

        sum(case when inv_type = '8000' OR inv_type = '8001' OR inv_type = '8011' OR inv_type = '8010'
        then inv_amount
        else 0
        end)
        as virtual_stock

        from wh_inventories
        GROUP BY product_code
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('pick_pack_status');
        });

        Schema::dropIfExists('pick_bin');
        Schema::dropIfExists('wh_picking');
        Schema::dropIfExists('wh_picking_order');
    }
}
