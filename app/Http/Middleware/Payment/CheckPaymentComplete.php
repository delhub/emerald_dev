<?php

namespace App\Http\Middleware\Payment;

use Closure;

use App\Models\Users\User;
use Auth;

class CheckPaymentComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::find(Auth::user()->id);

        if ($user->hasRole('dealer')) {
            $dealerInfo = $user->dealerInfo;

            if ($dealerInfo->account_status == 9) {
                return redirect('/register-dealer/payment/try-again?r=no-payment');
            }
        }

        return $next($request);
    }
}
