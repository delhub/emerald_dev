<?php

namespace App\Http\Controllers\Administrator\Import;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\File\File;
use Excel;
use App\Imports\ProductImport;
use App\Imports\ProductWithMultipleSheets;
use App\Imports\ImageImport;
use App\Exports\ProductExport;
use Illuminate\Support\Facades\Storage;
use App\Models\Products\ProductBrand;
use App\Models\Categories\Category;
use App\Models\Globals\Products\Product as GP;
use App\Imports\ProductWeightImport;
use App\Imports\ProductStockImport;
use App\Traits\ImportProduct;
use App\Traits\ImportImage;
use App\Imports\ProductManufactureImport;
use App\Imports\ProductPriceImport;

class ImportController extends Controller
{
    use ImportProduct, ImportImage;

    public function index(Request $req)
    {
        if (isset($req->col) && !empty($req->col) && $req->excel > 0) {
            $req->col = 'auto';
        } else {
            $req->col = '5';
        }

        if ($req->import == 'success') {
            $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $tempRoot = $storagePath . 'public/uploads/temp';
            $tempFile = $storagePath . 'public/uploads/file';

            $this->removeDirectory($tempRoot);
            $this->removeDirectory($tempFile );
        }

        if (isset($req->fname) && !empty($req->fname) && $req->excel == 1) {
            $file = File::where('name', $req->fname)->first('file_path');
            $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $array = Excel::toArray(new ProductImport, $storagePath . 'public/' . $file->file_path);

            \Session::put('excel', $array[0]);
        } else {
            if ($req->excel == 2) {
                $file = File::where('name', $req->fname)->first('file_path');
                $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
                $tempFolder = $storagePath . 'public/uploads/temp';

                \Madzipper::make($storagePath . 'public/' . $file->file_path)->extractTo($tempFolder);

                $files = Storage::disk('public')->allFiles('uploads');
                // $files = Storage::disk('local')->allFiles('public');

                if (count($files) > 0) {
                    foreach ($files as $val) {
                        $ff[] = basename($val);
                    }
                }

                if (in_array('Product.xlsx', $ff)) {
                    $key = array_search('Product.xlsx', $ff);
                }

                $array = Excel::toArray(new ProductWithMultipleSheets, $tempFolder . "/formula2u/Product.xlsx");

                // dd($array);

                if (count($array[0]) > 0) {
                    foreach ($array[0] as $k => $v) {
                        foreach ($array[0][0] as $k1 => $v1) {
                            if ($k1 > 0) {
                                $b[$v1] = $v[$k1];
                            }
                        }

                        $a[] = $b;
                    }

                    unset($a[0]);

                    foreach ($a as $val) {
                        $this->addProductBrand($val);
                        $this->addProductCategories($val);
                    }

                    \Session::put('brand', $a);
                }

                if (count($array[1]) > 0) {
                    foreach ($array[1] as $k => $v) {
                        foreach ($array[1][0] as $k1 => $v1) {
                            if ($k1 > 0) {
                                $bb[$v1] = $v[$k1];
                            }
                        }

                        $aa[] = $bb;
                    }

                    unset($aa[0]);

                    foreach ($aa as $val) {
                        $this->addOriginCategories($val);
                    }
                }

                if (count($array[4]) > 0) {
                    foreach ($array[4] as $k => $v) {
                        foreach ($array[4][0] as $k1 => $v1) {
                            $bc[$v1] = $v[$k1];
                        }

                        $ac[] = $bc;
                    }

                    unset($ac[0]);

                    \Session::put('ingredients', $ac);
                }

                $filter = collect($array[1])->whereNotNull(1);

                if($req->launching == 1) {
                    foreach ($filter as $k => $v) {
                        if($k > 0) {
                            $v[33] = 'Launching';
                        }

                        $newfilter[] = $v;
                    }

                    $filter = $newfilter;

                    \Session::put('excel', $filter);
                }
                else {
                    \Session::put('excel', $filter->toArray());
                }

                \Session::put('xilnex', $array[0]);
            } else {
                $filter = [];
            }
        }

        return view('administrator.import.index')
            ->with('data', $filter)
            ->with('req', $req);
    }

    public function createForm(Request $req)
    {

        if (isset($req->fname) && !empty($req->fname) && $req->excel == 1) {
            $file = File::where('name', $req->fname)->first('file_path');
            $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            $array = Excel::toArray(new ProductImport, $storagePath . 'public/' . $file->file_path);

            \Session::put('excel', $array[0]);
        } else {
            $array = [0 => []];
        }

        return view('administrator.import.file-upload')
            ->with('data', $array[0])
            ->with('req', $req);
    }

    public function weight(Request $req)
    {

        if (isset($req->col) && !empty($req->col) && $req->excel > 0) {
            $req->col = 'auto';
        } else {
            $req->col = '5';
        }

        return view('administrator.import.weight')
            ->with('req', $req);
    }

    public function uploadWeight(Request $req)
    {
        $filename = $req->file->getClientOriginalName();
        $filePath = $req->file('file')->storeAs('uploads/file', $filename , 'public');

        Excel::import(new ProductWeightImport, storage_path('/app/public/uploads/file/' . $filename));

        return redirect()->route('administrator.import.weight', ['col' => 'auto', 'import' => 'success'])
                    ->with('success', 'Data has been imported successfully...');
    }

    public function whStock(Request $req)
    {

        if (isset($req->col) && !empty($req->col) && $req->excel > 0) {
            $req->col = 'auto';
        } else {
            $req->col = '5';
        }

        return view('administrator.import.stock')
            ->with('req', $req);
    }

    public function uploadWhStock(Request $req)
    {
        $filename = $req->file->getClientOriginalName();
        $filePath = $req->file('file')->storeAs('uploads/file', $filename , 'public');

        Excel::import(new ProductStockImport, storage_path('/app/public/uploads/file/' . $filename));

        return redirect()->route('administrator.import.stock', ['col' => 'auto', 'import' => 'success'])
                    ->with('success', 'Data has been imported successfully...');
    }

    public function manufacture(Request $req)
    {

        if (isset($req->col) && !empty($req->col) && $req->excel > 0) {
            $req->col = 'auto';
        } else {
            $req->col = '5';
        }

        return view('administrator.import.manufacture')
            ->with('req', $req);
    }

    public function uploadManufacture(Request $req)
    {
        $filename = $req->file->getClientOriginalName();
        $filePath = $req->file('file')->storeAs('uploads/file', $filename , 'public');

        Excel::import(new ProductManufactureImport, storage_path('/app/public/uploads/file/' . $filename));

        return redirect()->route('administrator.import.manufacture', ['col' => 'auto', 'import' => 'success'])
                    ->with('success', 'Data has been imported successfully...');
    }

    public function price(Request $req)
    {

        if (isset($req->col) && !empty($req->col) && $req->excel > 0) {
            $req->col = 'auto';
        } else {
            $req->col = '5';
        }

        return view('administrator.import.price')
            ->with('req', $req);
    }

    public function uploadPrice(Request $req)
    {
        $filename = $req->file->getClientOriginalName();
        $filePath = $req->file('file')->storeAs('uploads/file', $filename , 'public');

        Excel::import(new ProductPriceImport, storage_path('/app/public/uploads/file/' . $filename));

        return redirect()->route('administrator.import.price', ['col' => 'auto', 'import' => 'success'])
                    ->with('success', 'Data has been imported successfully...');
    }

    public function fileUpload(Request $req)
    {
        switch ($req->import) {
            case 1:
                $excel = \Session::get('excel');

                $d = array_only($excel, $req->data);
                $e = array_values($d);

                //store export temp file
                Excel::store(new ProductExport($e), 'temp.csv');

                // Excel::import(new ProductImport, $e);
                Excel::import(new ProductImport, storage_path('/app/temp.csv'));

                echo "<pre>";
                print_r($e);

                dd($d);
                break;
            case 2:
                $excel = \Session::get('excel');

                if (count($excel) > 0) {
                    foreach ($excel as $k => $v) {
                        foreach ($excel[0] as $k1 => $v1) {
                            if ($k1 >= 0) {
                                $bb[$v1] = $v[$k1];
                            }
                        }
                        $aa[] = $bb;
                    }
                }

                $d = array_only($aa, $req->data);
                $e = array_values($d);

                $importProduct = $this->importProduct($d);
                $importImage = $this->importImage($d);

                //store export temp file
                // Excel::store(new ProductExport($e), 'temp_product.csv');

                //import product
                // Excel::import(new ProductImport, storage_path('/app/temp_product.csv'));

                //import images
                // Excel::import(new ImageImport, storage_path('/app/temp_product.csv'));

                $brands = \Session::get('brand');

                if (count($brands) > 0) {
                    foreach ($brands as $val) {
                        $this->setProductBrand($val);
                    }
                }

                $ingredients = \Session::get('ingredients');

                if (count($ingredients) > 0) {
                    $this->addIngredients($ingredients);
                }

                return redirect()->route('administrator.import.index', ['col' => 'auto', 'import' => 'success'])
                    ->with('success', 'Data and Images has been imported...');

                break;
            default:
                //
        }

        $req->validate([
            'file' => 'required|mimes:zip,csv,txt,xlx,xls,xlsx,ods,jpg,jpeg,gif,png|max:204800'
        ]);

        $file_ext = $req->file->getClientOriginalExtension();
        $excel = ['csv', 'xlx', 'xls', 'xlsx', 'ods'];

        if (in_array($file_ext, $excel)) {
            $data_is_excel = 1;
            $msg = " has been uploaded.";
        } elseif ($file_ext == 'zip') {
            $data_is_excel = 2;
            $msg = " has been uploaded and extracted.";
            $req->withHead = 'on';
        } else {
            $data_is_excel = 0;
            $msg = " has been uploaded.";
        }

        // dd($req->file->getMimeType(),$req->file->getClientOriginalExtension() );

        $fileModel = new File;

        if ($req->file()) {
            $fileName = time() . '_' . $req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads/file', $fileName, 'public');

            $fileModel->name = time() . '_' . $req->file->getClientOriginalName();
            $fileModel->file_path = $filePath;
            $fileModel->save();

            return redirect()->route('administrator.import.index', ['fname' => $fileName, 'excel' => $data_is_excel, 'withHeading' => $req->withHead, 'launching' => $req->launching, 'col' => 'auto'])
                ->with('success', $fileName . $msg)
                ->with('file', $fileName);
        }
    }

    function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }

    public function removeDirectory($path)
    {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            is_dir($file) ? $this->removeDirectory($file) : unlink($file);
        }

        $this->deleteDirectory($path);
        // rmdir($path);
        return;
    }

    public function addProductBrand($data)
    {

        if (isset($data['Brand']) && !empty($data['Brand'])) {

            $brand = ProductBrand::where('name', $data['Brand'])->first();

            if (!$brand) {
                $brand = new ProductBrand;
            }

            $brand->name = $data['Brand'];
            $brand->slug = str_slug($data['Brand']);

            $brand->save();
        }
    }

    public function setProductBrand($data)
    {

        if (isset($data['Brand']) && !empty($data['Brand'])) {

            $brand = ProductBrand::where('name', $data['Brand'])->first();
            $global_product = GP::where('name_slug', str_slug($data['Item Name']))->first();
            if ($brand && $global_product) {

                $global_product->brand_id = $brand->id;
                $global_product->save();
            }
        }
    }

    public function addProductCategories($data)
    {

        if (isset($data['Category']) && !empty($data['Category'])) {

            $categories = Category::where('name', $data['Category'])->first();

            if (!$categories) {
                $categories = new Category;
            }

            $categories->name = $data['Category'];
            $categories->slug = str_slug($data['Category']);
            $categories->parent_category_id = 0;

            $categories->save();
        }
    }

    public function addOriginCategories($data)
    {

        if (isset($data['place_of_origin']) && !empty($data['place_of_origin'])) {

            $categories = Category::where([['name', $data['place_of_origin']], ['type', 'origin_country']])->first();

            if (!$categories) {
                $categories = new Category;
            }

            $categories->name = $data['place_of_origin'];
            $categories->slug = str_slug($data['place_of_origin']);
            $categories->parent_category_id = 0;
            $categories->type = 'origin_country';

            $categories->save();
        }
    }

    public function saveIngredientsData($product_code, $ingredients)
    {

        foreach ($ingredients as $key => $v) {
            $product[$v['title']] = $v['value'];
        }

        $global_product = GP::where('product_code', $product_code)->first();

        if($global_product) {
            $global_product->ingredients = json_encode($product);

            $global_product->save();
        }
    }

    public function addIngredients($data)
    {
        $ingredients = collect($data);

        $plucked = $ingredients->pluck('product_code_main');
        $product_codes = $plucked->unique();

        foreach ($product_codes as $val) {
            $products = $ingredients->where('product_code_main', $val);

            $this->saveIngredientsData($val, $products);
        }
    }
}
