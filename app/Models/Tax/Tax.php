<?php

namespace App\Models\Tax;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    // Table Name
    protected $table = 'global_tax';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestemps = true;
}