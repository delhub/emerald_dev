<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Discount\Code;
use App\Models\Discount\Rule;
use App\Traits\Voucher;
use App\Models\Discount\Usage;

class ApiVoucherController extends Controller
{
    use Voucher;

    public function getCouponRetail($coupon)
    {
        $country_code = 'MY';
        $today = date('Y-m-d');

        $coupon = str_replace("-", '', $coupon);

        $code = Code::select('discount_code.*', 'discount_code.coupon_type as code_coupon_type', 'b.*')
            ->join('discount_rule as b', 'discount_code.coupon_rule_id', '=', 'b.id')
            ->where([
                ['discount_code.country_id', $country_code],
                ['discount_code.coupon_code', $coupon],
                ['discount_code.platform', 2],
                ['discount_code.expiry', '>=', $today],
                ['discount_code.coupon_status', 'valid']
            ])
            ->first();

        if ($code) {
            if ($code->general_type == 'breakdown') {
                $monthlyUsage = $this->monthlyUsageShipping($user->id, $coupon);

                if ($monthlyUsage >= $code->limit) {
                    return ['status' => 'error', 'error' => "This voucher only can be use {$code->limit} time per month"];
                } elseif ($code->usage_counter >= $code->max_voucher_usage) {
                    return ['status' => 'error', 'error' => 'Coupon already max usage or not valid'];
                } else {
                    if ($code->code_coupon_type == 'value') {

                        return $code;
                    } else {
                        $code->coupon_amount_percentage = $code->coupon_amount;
                        $code->coupon_amount = number_format($amount * ($code->coupon_amount / 100), 2);

                        return $code;
                    }
                }
            } else {
                if ($code->usage_counter >= $code->max_voucher_usage) {
                    return ['status' => 'error', 'error' => 'Coupon already max usage or not valid'];
                } else {
                    if ($code->code_coupon_type == 'value') {
                        return $code;
                    } else {
                        $code->coupon_amount_percentage = $code->coupon_amount;
                        $code->coupon_amount = number_format($amount * ($code->coupon_amount / 100), 2);

                        return $code;
                    }
                }
            }
        } else {
            return ['status' => 'error', 'error' => 'Coupon is not valid!.'];
        }
    }

    public function createUsage(Request $request)
    {
        $this->voucherUsage($request->coupon_code);

        $usage = new Usage;

        $usage->outlet_id = $request->outlet_id;
        $usage->order_number = $request->order_number;
        $usage->coupon_code = $request->coupon_code;
        $usage->customer_id = $request->customer_id;

        if ($usage->save()) {
            return true;
        }
    }

    public function generateVoucherCode(Request $request)
    {
        if($this->createVoucherCode($request)) {
            return true;
        }
    }

    public function createVoucherCode($data) {

        $code = new Code;
        $code->country_id = $data->country_id;
        $code->user_id = -1;
        $code->coupon_code = $data->coupon_code;
        $code->coupon_type = $data->coupon_type;
        $code->coupon_amount = $data->coupon_amount;
        $code->coupon_rule_id = $data->coupon_rule_id;
        $code->created_by_purchase = isset($data->created_by_purchase) ? $data->created_by_purchase : 'ADM-' .time();
        $code->coupon_status = $data->coupon_status;
        $code->max_voucher_usage = $data->max_voucher_usage;
        $code->platform = $data->platform;
        $code->outlet_id = $data->outlet_id;
        // $code->expiry = $data->input('expiry');
        $code->expiry = $data->expiry;

        if($code->save()) {
            return $code;
        }

    }

    public function getRule()
    {
        $rule = Rule::all();

        if(count($rule)) {
            return $rule;
        }
    }


}
