<?php

namespace App\Http\Controllers\Administrator\Tax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tax\Tax;
use App\Models\Globals\Countries;
use DB;

class TaxController extends Controller
{
    public function index() {
        $country = Countries::get();

        return view('administrator.tax.indexvue')
            ->with('country', $country);
    }

    public function getTax() {

        $tax = Tax::get();
        return response()->json($tax);
    }

    public function getTaxById($id) {

        $tax = Tax::where('id',$id)->first();

        return response()->json($tax);
    }

    public function create() {

        $country = Countries::get();

        return view('administrator.tax.create')
            ->with('country', $country);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $this->validate($request, [
            'country_id' => 'required',
            'tax_name' => 'required',
            'tax_percentage' => 'required',
            'tax_reg_number' => 'required'
        ]);

        $tax = new Tax;

        $tax->country_id = $request->input('country_id');
        $tax->tax_name = $request->input('tax_name');
        $tax->tax_percentage = $request->input('tax_percentage');
        $tax->tax_reg_number = $request->input('tax_reg_number');

        $tax->save();

        return redirect('/administrator/tax')->with('success', 'submit done.');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'country_id' => 'required',
            'tax_name' => 'required',
            'tax_percentage' => 'required',
            'tax_reg_number' => 'required'
        ]);

        $tax = new Tax;

        $tax->country_id = $request->input('country_id');
        $tax->tax_name = $request->input('tax_name');
        $tax->tax_percentage = $request->input('tax_percentage');
        $tax->tax_reg_number = $request->input('tax_reg_number');

        DB::update("UPDATE `global_tax` SET `country_id` = ?,`tax_name` = ? , `tax_percentage`=?, `tax_reg_number`=?  WHERE `id` = ?",[$tax->country_id, $tax->tax_name, $tax->tax_percentage, $tax->tax_reg_number, $id]);

    }

    public function delete($id)
    {
        DB::table('global_tax')->where('id', '=', $id)->delete();
    }




}
