<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldToWarehouseBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_batch', function (Blueprint $table) {
            $table->string('inv_number',32)->nullable()->after('po_number');
            $table->string('vendor_desc',255)->nullable()->after('inv_number');
            $table->string('batch',24)->nullable()->after('vendor_desc');
            $table->string('lot',24)->nullable()->after('batch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_batch', function (Blueprint $table) {
            $table->dropColumn(['inv_number']);
            $table->dropColumn(['vendor_desc']);
            $table->dropColumn(['batch']);
            $table->dropColumn(['lot']);
        });
    }
}
