<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.0.0-beta.0
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ config('app.name', 'Laravel') }} - Administrator</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- Main styles for this application-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}?v=2.4" rel="stylesheet">
    <link href="{{ asset('css/slimselect.min.css') }}?v=1.0" rel="stylesheet" />

    <!-- Summernote Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">


    <style>
        .sidebar .sidebar-nav {
            width: 100%;
        }

        .sidebar .nav-link {
            color: #ffffff;
            letter-spacing: .015rem;
        }

        .sidebar .nav-link:hover {
            color: #ffffff;
            rgba(0, 0, 0, 0.15);
        }

        .sidebar .nav-link:hover .nav-icon {
            color: #ffffff;
        }

        .sidebar .nav-dropdown.open .nav-link {
            color: #ffffff;
            background-color: rgba(0, 0, 0, 0.15);
        }

        .sidebar .nav-dropdown.open .nav-link:hover {
            color: #ffffff;
        }

        ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 7px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: rgba(0, 0, 0, .5);
            box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }

        .input-container input {
            border: none;
            box-sizing: border-box;
            outline: 0;
            padding: .75rem;
            position: relative;
            width: 100%;
        }

        input[type="date"]::-webkit-calendar-picker-indicator {
            background: transparent;
            bottom: 0;
            color: transparent;
            cursor: pointer;
            height: auto;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: auto;
        }
    </style>
    @hasrole('management')
    <style>
    ul.nav > li:not(:nth-last-child(2))
    {
    display: none;
    }
    </style>
    @endhasrole
    @stack('style')

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    @include('layouts.administrator.navigations.topbar')
    <div class="">
        @include('layouts.administrator.navigations.sidebar')
        <main class="main">
            <!-- Breadcrumb-->
            @yield('breadcrumb')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Home</li>
                <li class="breadcrumb-item">
                    <a href="#">Admin</a>
                </li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            <div class="container-fluid">
                <div id="app" class="animated">
                    @yield('content')
                </div>

                <div id="app2" class="animated">
                    @yield('content2')
                </div>
            </div>
        </main>
    </div>
    @include('layouts.administrator.navigations.footer')

    {{-- vue --}}
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.22.0/axios.min.js"></script>
    <script src="/js/warehouse.js?v=1.{{ time() }}"></script>

    <!-- Main scripts for this application -->
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Summernote -->
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
    <script src="{{ asset('js/slimselect.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            const myInput = $('.my-input-warehouse');
            myInput.find('[autofocus]').focus();

            <?php if (Session::has('success')) {
                echo 'toastr.success("' . Session::get('success') . '");';
            }
            if (Session::has('error')) {
                echo 'toastr.error("' . Session::get('error') . '");';
            }
            ?>
        });
    </script>
<script>
    // DOM Elements
    const editButton = document.querySelector('#enableEditing');
    const submitBtn = document.querySelector('#submitBtn');

    @if(isset($cities)) const allCities = {!! json_encode($cities) !!} @endif  

    // Event Listeners
    editButton.addEventListener('click', enableEditing);
    submitBtn.addEventListener('click', submitVisibleForm);

    document.querySelectorAll('.state').forEach(stateDropdown => {
        stateDropdown.addEventListener('change', function () {
            filterCitiesByState(this); // Pass the current state dropdown to the function
        });
    });
    window.onload = function() {
        var activeSection = document.getElementById('activeSection').value;
        if (activeSection) {
            showSection(activeSection);
            @if(isset($error)) enableEditing();@endif
        }
        
    };

    // Function to find the currently visible section
    function getActiveSection() {
        return document.querySelector('.user_information[style*="display: block"]');
    }

    // Function to show a specific section and hide others
    function showSection(sectionId) {
        document.getElementById('activeSection').value = sectionId;
        // Hide all sections and reset headers and tabs
        document.querySelectorAll('.user_information').forEach(section => {
            section.style.display = 'none';
        });
        document.querySelectorAll('.pull-left h2').forEach(header => {
            header.style.display = 'none';
        });
        document.querySelectorAll('.tab').forEach(tab => {
            tab.classList.remove('active');
        });
        document.querySelectorAll('.info-detail').forEach(detail => {
            detail.style.display = 'none';
        });

        submitBtn.style.display = 'none';
        editButton.style.display = 'block';

        if (sectionId === 'dealer_info' || sectionId === 'bank_info') {
            document.getElementById('dealer_info_name').style.display = 'block';
        } else {
            document.getElementById('customer_info_name').style.display = 'block';
        }

        // Show the selected section and update header and tab state
        document.getElementById(sectionId).style.display = 'block';
        document.getElementById(sectionId + '_header').style.display = 'block';
        // document.getElementById(sectionId + '_name').style.display = 'block';
        document.getElementById(sectionId + 'Tab').classList.add('active');

        // Add readonly to all inputs in the section
        document.querySelectorAll('.user_information').forEach(section => {
            section.querySelectorAll('input').forEach(input => {
                if (!input.hasAttribute('readonly')) {
                    input.setAttribute('readonly', true);
                }
            });
            section.querySelectorAll('select').forEach(select => {
                if (!select.hasAttribute('disabled')) {
                    select.setAttribute('disabled', true);
                }
            });
        });
    }

    // Function to enable editing on the currently visible section
    function enableEditing() {
        const activeSection = getActiveSection();
        if (activeSection) {
            // Remove readonly and enable dropdowns in the active section
            activeSection.querySelectorAll('input[readonly]').forEach(input => {
                input.removeAttribute('readonly');
            });
            activeSection.querySelectorAll('select').forEach(select => {
                select.removeAttribute('disabled');
            });

            // Update button state for editing mode
            editButton.style.display = 'none';
            submitBtn.style.display = 'block';
        } else {
            console.error('No active section found!');
        }
    }

    // Function to submit the currently visible form
    function submitVisibleForm() {
        const forms = document.querySelectorAll('form');
        forms.forEach(form => {
            const userInfo = form.querySelector('.user_information');
            if (userInfo && userInfo.style.display === 'block') {
                console.log('Submitting form because the section is visible');
                form.submit();
            }
        });
    }

    // Function to filter cities based on selected state
    function filterCitiesByState(stateDropdown) {
        const selectedStateId = stateDropdown.value;
        
        const cityDropdown = stateDropdown.closest('.d-flex').querySelector('.city'); 

        // Enable the city dropdown
        cityDropdown.removeAttribute('disabled');

        // Clear existing options
        cityDropdown.innerHTML = '<option value="" disabled selected>Select a City</option>';

        // Add cities that match the selected state
        allCities
            .filter(city => city.state_id == selectedStateId) // Match state_id
            .forEach(city => {
                const option = document.createElement('option');
                option.value = city.city_key; // City key for backend
                option.textContent = city.city_name; // Display city name
                cityDropdown.appendChild(option);
            });

        // Reset city selection
        cityDropdown.value = "";
    }
    function toggleInput() {
        const selectSearch = document.getElementById('selectSearch').value;
        const searchBox = document.getElementById('searchBox');
        const rolesBox = document.getElementById('rolesBox');

        if (selectSearch === 'account_type') {
            searchBox.style.display = 'none';
            rolesBox.style.display = 'block';
        } else {
            searchBox.style.display = 'block';
            rolesBox.style.display = 'none';
        }
    }

    // Trigger on page load based on server-rendered state
    document.addEventListener('DOMContentLoaded', () => {
        toggleInput();
    });

</script>
    @stack('script')

</body>

</html>
