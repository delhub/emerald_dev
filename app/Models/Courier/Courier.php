<?php

namespace App\Models\Courier;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    // Table Name
    protected $table = 'courier_info';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestemps = true;



    public static function trackingURL($courier_name, $tracking_number)
    {
        switch ($courier_name) {

            case 'POSLAJU':
                $url = 'https://www.tracking.my/poslaju/';
                break;
            case 'J&T':
                $url = 'https://www.tracking.my/jt/';
                break;

            case 'CITY LINK EXPRESS':
                $url = 'https://www.tracking.my/citylink/';
                break;
            case 'TRANSLINK EXPRESS':
            default:
                return '#';
                break;
        }
        return $url . $tracking_number;
    }
}
