<?php

namespace App\Models\Warehouse\StockTransfer;

use Illuminate\Database\Eloquent\Model;
use App\Models\Globals\State;
use App\Traits\TableLogs;
use App\Models\Warehouse\Box\WarehouseBox;

class StockTransfer extends Model
{
    use TableLogs;

    protected $table = 'wh_transfer';

    // Set primary key
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        // 'user',
        // 'session_start_time',
        // 'session_end_time',
        // 'from_location_id',
        // 'to_location_id',
        // 'model_type',
        // 'model_id',
        // 'transfer_datetime',
    ];

    /**
     * Get purchase of the WarehouseOrder.
     */
    public function stockTransferItems()
    {
        return $this->hasMany('App\Models\Warehouse\StockTransfer\StockTransferItem', 'key', 'id')
            ->orderBy('received', 'asc');
    }

    public function fromLocation()
    {
        return $this->belongsTo('App\Models\Warehouse\Location\Wh_location', 'from_location_id', 'id');
    }

    public function toLocation()
    {
        return $this->belongsTo('App\Models\Warehouse\Location\Wh_location', 'to_location_id', 'id');
    }

    public function theUser()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'user');
    }

    public function theUserReceive()
    {
        return $this->hasone('App\Models\Users\User', 'id', 'user_receive');
    }

    public function transferBox()
    {
        return $this->hasOne(WarehouseBox::class, 'using', 'id');
    }

    public function scopefromLocationFilter($query, $location_id)
    {
        if ($location_id != '0' && $location_id != NULL) {
            return $query
                ->where('from_location_id', $location_id);
        }
    }

    public function scopetoLocationFilter($query, $location_id)
    {
        if ($location_id != '0' && $location_id != NULL) {
            return $query
                ->where('to_location_id', $location_id);
        }
    }

    public function scopetypeFilter($query, $transfer_type)
    {
        if ($transfer_type != NULL) {
            $transfer_type = $transfer_type - 1;
            return $query
                ->where('type', $transfer_type);
        }
    }

    public function scopestatusFilter($query, $transfer_status)
    {
        if ($transfer_status != NULL) {
            return $query
                ->where('status', $transfer_status);
        }
    }
}
