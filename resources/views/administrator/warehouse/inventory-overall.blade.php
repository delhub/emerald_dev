@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')
<h1>Warehouse Inventory Overall (Without Dummy)</h1>

<div class="card shadow-sm">

    <div class="card-header">Inventory Overall<small></small></div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-7  mb-3 row">
                <form action="{{ route('administrator.warehouse.inventory.overall') }}" method="GET" class="col-md-6">
                    <div class="row">
                        <div class="col-10 pr-0">
                            <input type="text" class="form-control d-inline" id="searchBox" name="search_box"
                                placeholder="Product Code / Name"
                                value="{{ ($request->search_box) ?? '' }}">
                        </div>
                        <div class="col-2 px-0">
                            <button type="submit" class="btn btn-outline-primary d-inline"
                                style="color:black;">Search</button>
                        </div>
                    </div>

                </form>
                <form action="{{ route('administrator.warehouse.inventory.overall') }}" method="GET" class="pr-2">
                    <button type="submit" class="btn btn-outline-primary" style="color:black;">Reset</button>
                </form>
            </div>
            <div class="col-auto ml-auto">
                {{ $lists->links() }}
            </div>
        </div>

        <table class="table table-sm table-hover table-bordered">
            <thead>
                <tr>
                    <th scope="col" class="align-middle">#</th>
                    <th scope="col" class="align-middle">Product Code</th>
                    <th scope="col" class="align-middle">Product Name</th>
                    <th scope="col" class="align-middle">Product Attribute</th>
                    <th scope="col" class="align-middle">Physical Stock</th>
                    <th scope="col" class="align-middle">Virtual Stock (Reserved)</th>
                    {{-- <th scope="col" class="align-middle">Virtual Stock (Reserved Offline)</th> --}}
                    <th scope="col" class="align-middle">Virtual Stock</th>
                    <th scope="col" class="align-middle">Retail Sold</th>
                    <th scope="col" class="align-middle">Online Sold</th>
                    <th scope="col" class="align-middle">Total Sold</th>
                    <th scope="col" class="align-middle">Inventory</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($lists as $product_code => $list)
                <tr>
                    <th scope="row" class="text-center">{{ $loop->iteration }}</th>
                    <td>{{ $product_code }}</td>
                    <td>{{ $list['global_product_name'] }}</td>
                    <td>{{ $list['product_name'] }}</td>
                    <td class="text-danger">{{ $list['overallPhysical'] }}</td>
                    <td class="text-danger">{{ $list['virtual_stock_reserved'] }}</td>
                    {{-- <td class="text-danger">{{ $list['virtual_stock_reserved_offline'] }}</td> --}}
                    <td class="text-success">{{ $list['virtual_stock'] }}</td>
                    <td class="text-danger">{{ $list['pos_completed'] }}</td>
                    <td class="text-danger">{{ $list['online_sold'] }}</td>
                    <td class="text-danger">{{ $list['total_sold'] }}</td>
                    <td class="text-center">
                        <form action="{{ route('administrator.warehouse.inventory.index') }}" method="GET">
                            <input type="hidden" class="form-control d-inline" id="searchBox" name="search_box"  value="{{ ($product_code) ?? '' }}">
                            <button type="submit" class="btn btn-primary">Inventory</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="row">
            <div class="col-auto ml-auto">
                {{ $lists->appends($_GET)->links() }}
            </div>
        </div>
    </div>
</div>
@endsection


