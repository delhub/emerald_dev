@inject('controller', 'App\Http\Controllers\Warehouse\StockAdjustment\StockAdjustmentController')

@extends('layouts.administrator.main')

@section('content')

<h1>{{$controller::$headerTransferList}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div> --}}
    </div>
        <div class="table-responsive m-2">
            @if(count($tables) > 0)
            <table id="table-id" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Location</th>
                        <th>User</th>
                        <th>Product Code</th>
                        <th>Adjust Type</th>
                        <th>quantity</th>
                        <th>Adjust Date</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($tables as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{$row->location->location_name}}</td>
                        <td>{{$row->theUser->userInfo->full_name}}</td>
                        <td>{{$row->product_code}}</td>
                        <td>{{$row->adjustType->name}}</td>
                        <td>{{$row->quantity}}</td>
                        <td>{{$row->created_at}}</td>
                        <td>{{$row->remark}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- {{$tables->links()}} --}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$headerTransferList}} Found</p>
            @endif
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#table-id').DataTable();
    });

function editNotes(batchID) {
    const notes =$("#notes"+batchID).val();

    if (confirm('Are you sure want to edit this notes? Any unsaved changes will be lost')) {
        if (notes=='' ) {
            $(".variationThisEmpty"+batchID).show();
        } else {
            $.ajax({
                // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
                url:"{{ route('warehouse.StockTransfer.stockTransferList.edit-notes', ['batchID' => "+batchID+"]) }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "batchID": batchID,
                    "notes": notes,
                },
                beforeSend: function () {
                    $(".variationLoader"+batchID).show();
                },
                success: function (success) {
                    $(".variationLoader"+batchID).hide();
                        $(".variationRefreshPrice"+batchID).show();
                },
                error: function () {
                    alert("error in loading");
                }
            });
        }
    }
}
</script>
@endpush
