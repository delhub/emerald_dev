<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ShippingInstallations\Ship_category;
use Illuminate\Support\Facades\DB;

class AddLaunchingShippingCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $shipping_category = new Ship_category;
        $shipping_category->cat_name = 'For Launching Shipping Category only';
        $shipping_category->cat_desc = 'For Launching Shipping Category only';
        $shipping_category->cat_type = 'shipping';

        $shipping_category->active = '1';
        $shipping_category->for_user = '["6000"]';
        $shipping_category->start_date = '';
        $shipping_category->end_date = '';
        $shipping_category->save();

        DB::table('panel_product_attributes')
            ->update(
                ['shipping_category_special' => '3']
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
