@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Ship Category</h1>

<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route('administrator.shipping-installations.ship_category.create')}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Product</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 ml-auto">
                <form action="{{ route('administrator.shipping-installations.ship_category.index') }}" method="GET">
                    <div class="row">
                        <div class="input-group col-md-8 p-0 ml-auto mx-4">
                            <div class="input-group-append col-md-9 p-0">
                                <input type="text" class="form-control" id="search" name="search"
                                    value="{{ ($request->search) ? $request->search : '' }}">
                            </div>
                            <div class="input-group-append col-md-2 p-0">
                                <button type="submit" class="btn btn-outline-warning"
                                    style="color:black;">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive m-2">
            @if(count($shipCategorys) > 0)
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>Category Name</td>
                        <td>Category Description</td>
                        <td>Category Type</td>
                        <td>for_user</td>
                        <td>start_date</td>
                        <td>end_date</td>
                        <td>active</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($shipCategorys as $key => $shipCategory)
                    <tr>
                        <td>{{$shipCategory->id}}</td> 
                        <td>{{$shipCategory->cat_name}}</td>
                        <td>{{$shipCategory->cat_desc}}</td>
                        <td>{{$shipCategory->cat_type}}</td>
                        <td>
                            @foreach ($userLevels as $level)
                                @if (in_array($level->id, json_decode($shipCategory->for_user)))
                                    {{ $level->name }} /
                                @endif
                            @endforeach
                        </td>
                        <td>{{$shipCategory->start_date}}</td>
                        <td>{{$shipCategory->end_date}}</td>
                        <td>{{$shipCategory->showActive()}}
                        </td>
                        <td>
                            <a style="color: white; font-style: normal; border-radius: 2px;"
                            href="{{ route('administrator.shipping-installations.ship_category.edit',['id' => $shipCategory->id]) }}" 
                            class="btn btn-primary shadow-sm">Edit</a>

                            @if (isset($list[$shipCategory->id]) && isset($list[$shipCategory->id]['attributeId']) && $list[$shipCategory->id]['attributeId'] != NULL)
                                <a style="color: white; font-style: normal; border-radius: 2px;"
                                href="/administrator/product/shippingCategory/?manyID={{ implode("," , $list[$shipCategory->id]['attributeId'] )}}"
                                class="btn btn-primary shadow-sm"> Detail </a>
                            @endif

                            @if (isset($list[$shipCategory->id]) && isset($list[$shipCategory->id]['attributeId_special']) && $list[$shipCategory->id]['attributeId_special'] != NULL)
                                <a style="color: white; font-style: normal; border-radius: 2px;"
                                href="/administrator/product/shippingCategory/?manyID={{ implode("," , $list[$shipCategory->id]['attributeId_special'] )}}"
                                class="btn btn-primary shadow-sm"> Detail </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$shipCategorys->links()}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No courier found</p>
            @endif
        </div>
    </div>
</div>
@endsection
