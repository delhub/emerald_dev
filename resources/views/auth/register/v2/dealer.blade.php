@extends('layouts.guest.main')

@section('content')


    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 my-pt-3 my-pb-3">
            <img class="r-f-logo" src="{{ asset("images/logo/formula-logo.png") }}" alt="">
        </div>
    </div>
   {{-- @if($errors->any())
        {{ implode('', $errors->all('<div>:message</div>')) }}
        @endif--}}

        <div class="my-register-box my-guests-card">
            <div class="my-r-title-box">
                <h5><b>AGENT REGISTRATION</b></h5>
            </div>
            <div id="rtab-opt" class=" my-r-nav">
                <ul class="nav nav-tabs nav-fill" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active " id="home-tab" data-toggle="tab" href="#registration"
                            role="tab" aria-controls="registration" aria-selected="true">Registration</a>
                    </li>
                <li class="nav-item">
                    <a class="nav-link register-tab-active " id="profile-tab" data-toggle="tab" href="#information"
                        role="tab" aria-controls="profile" aria-selected="false">Introducer Information</a>
                </li>
            </ul>

            <div class="card-body ">
                <!-- Dealer Registration Form -->
                <form method="POST" action="{{ route('guest.register.agent.post') }}" id="register-form" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content r-inner-box" id="myTabContent">
                        <!-- Registration  Tab-->
                        <div class="r-subt-box tab-pane fade show active " id="registration" role="tabpanel"
                            aria-labelledby="registration-tab">
                            <h5>Account Particulars</h5>
                            <div class="form-row">

                                <div class="form-group col-md-12">
                                    <label for="validate-customer">Existing Formula customer?</label>
                                    <div class="validate-customer">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="bjs_customer"
                                                id="bjs_customer_1" value="1" @if(old('bjs_customer')=='1' ) checked
                                                @endif>
                                            <label class="form-check-label" for="bjs_customer">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="bjs_customer"
                                                id="bjs_customer_0" value="0" @if(old('bjs_customer')==null ||
                                                old('bjs_customer')=='0' ) checked @endif>
                                            <label class="form-check-label" for="bjs_customer">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row register-info">
                                <div class="form-group col-md-12">
                                    <label for="invoice_number"> Invoice Number <small
                                            class="text-danger">*</small></label>
                                    <input type="text" name="invoice_number"
                                        onchange="checkInvoice();"
                                        class="form-control @error('invoice_number') is-invalid @enderror"
                                        id="invoice_number" placeholder="{{$inv_placehold}}"
                                        value="{{ old('invoice_number') }}">
                                    @error('invoice_number')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group  col-md-12 if-bjs" style="display:none">
                                    <label for="bjs_customer_id">Formula2u Customer Email <small
                                            class="text-danger">*</small></label>
                                    <div class="input-group mb-0">
                                        <input type="text" name="bjs_customer_id"
                                            class="form-control @error('bjs_customer_id') is-invalid @enderror"
                                            id="bjs_customer_id"
                                            value="{{ old('bjs_customer_id') }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" id="check_customer_id"
                                                onclick="checkCustomerID();" type="button">Check ID <span
                                                    class="spinner-border spinner-border-sm" style="display: none;"
                                                    role="status" aria-hidden="true"></span></button>

                                        </div>
                                    </div>

                                    <div id="bjs_info">
                                        @error('bjs_customer_id')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group col-md-12 if-notbjs">
                                    <label for="email">Email <small class="text-danger">*</small></label>
                                    <input type="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror" id="email"
                                        placeholder="Your Email Address" value="{{ old('email') }}">
                                    @error('email')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="password">Password <small class="text-danger">*</small></label>
                                    <input type="password" name="password" class="form-control" id="password">
                                    @error('password')
                                    <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12 if-notbjs">
                                    <label for="password-confirm">Confirm Password <small
                                            class="text-danger">*</small></label>
                                    <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" autocomplete="new-password">
                                </div>
                            </div>
                        </div>
                            <!-- Next Button -->
                            <div class="r-next-btn">
                                <a class="btn r-color-btn "
                                    id="next-btn"><b>Next</b></a>
                            </div>

                            <div id="error-div-1" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Information Tab -->
                        <div class="tab-pane fade r-subt-box" id="information" role="tabpanel" aria-labelledby="information-tab">
                            <!-- Personal Particulars -->
                            <h5>Introducer Information</h5>
                            <div class="form-row register-info">
                                <div class="form-group col-md-6">
                                    <label for="referrer_name">Introducer Name <small
                                    class="text-danger">*</small></label>

                                    <input type="text" name="referrer_name" id="referrer_name" class="form-control" value="{{ old('referrer_name') }}">

                                </div>
                                <div class="col-12 col-md-6 form-group">
                                    <label for="referrer_id">Introducer ID <small class="text-danger">*</small></label>
                                    <input type="text" name="referrer_id" id="referrer_id"
                                        class="form-control  @error('referrer_id') is-invalid @enderror "
                                        value="{{ old('referrer_id') }}">

                                    @error('referrer_id')
                                    <small class="form-text text-danger"><i class="fa fa-times"> </i>
                                        {{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row register-info">
                                <div class="form-group col-md-12">
                                    <label for="agent_group">Agent Group <small class="text-danger">*</small></label>
                                    <select name="agent_group" id="agent_group" class="form-control">
                                        <option value="">Select a group..</option>
                                        <optgroup label="Kuala Lumpur">
                                            <option value="1" @if ("1"==old('agent_group')) selected="selected" @endif>
                                                Chiew Wei Ping @ Alex</option>
                                            <option value="2" @if ("2"==old('agent_group')) selected="selected" @endif>
                                                Chow Siok Koon @ Tiffany</option>
                                            <option value="3" @if ("3"==old('agent_group')) selected="selected" @endif>
                                                Kong Kim Leng @ June</option>
                                            <option value="4" @if ("4"==old('agent_group')) selected="selected" @endif>
                                                Liew Wei Chyan @ Dave</option>
                                            <option value="5" @if ("5"==old('agent_group')) selected="selected" @endif>
                                                Yeap Lay Yan</option>
                                            <option id="kklee" value="14" @if ("14"==old('agent_group')) selected="selected"
                                            @endif>FML</option>
                                        </optgroup>
                                        <optgroup label="Penang">
                                            <option value="6" @if ("6"==old('agent_group')) selected="selected" @endif>
                                                Lee Kean Guan @ Sampson</option>
                                            <option value="7" @if ("7"==old('agent_group')) selected="selected" @endif>
                                                Ch'ng Saw Boay @ May</option>
                                            <option value="8" @if ("8"==old('agent_group')) selected="selected" @endif>
                                                Khong Bee Kim @ Kim</option>
                                            <option value="9" @if ("9"==old('agent_group')) selected="selected" @endif>
                                                Tan Teng Siew @ Darren</option>
                                            <option value="10" @if ("10"==old('agent_group')) selected="selected"
                                                @endif>Tang Hui Chin</option>
                                            <option value="11" @if ("11"==old('agent_group')) selected="selected"
                                                @endif>NDV</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <!--
                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package1" name="package" value="1" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="package1">
                                        <p class="font-weight-bold">配套1 (DC客戶申請代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-1.jpg') }}" alt="Package 2">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 6,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            1,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem; color: #ffffff;">
                                                            RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX RMXXXX
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package2" name="package" value="2" class="custom-control-input">
                                    <label class="custom-control-label" for="package2">
                                        <p class="font-weight-bold">配套2 (PJA申請代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-2.jpg') }}" alt="Package 2">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 6,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            3,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM29,000 指定生意
                                                            <br>
                                                            額外獲得 RM1,000 現金花紅
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package3" name="package" value="3" class="custom-control-input">
                                    <label class="custom-control-label" for="package3">
                                        <p class="font-weight-bold">配套3 A級創利方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-3.jpg') }}" alt="Package 3">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 7,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            7,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：60天內完成 RM19,000指定生意
                                                            <br>
                                                            額外獲得 RM1,000 現金花紅
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package4" name="package" value="4" class="custom-control-input">
                                    <label class="custom-control-label" for="package4">
                                        <p class="font-weight-bold">配套3 A+級擢昇方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-4.jpg') }}" alt="Package 4">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 17,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            17,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM39,000 指定生意
                                                            <br>
                                                            額外獲得 RM2,500 現金花紅
                                                            <br>
                                                            <span class="text-danger">僅限99個位子</span>
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="card p-2">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="package5" name="package" value="5" class="custom-control-input">
                                    <label class="custom-control-label" for="package5">
                                        <p class="font-weight-bold">配套3 A++領導方案 (DC或POR代理)</p>
                                        <div class="row no-gutters">
                                            <div class="col-4 col-md-3 px-1">
                                                <img class="mw-100" src="{{ asset('assets/images/miscellaneous/packages/package-5.jpg') }}" alt="Package 5">
                                            </div>
                                            <div class="col-8 col-md-9 px-1">
                                                <ol class="px-3 py-0 list-unstyled">
                                                    <li>
                                                        <p class="mb-0" style="font-size: 1.1rem; font-weight: 600;">
                                                            RM 38,090
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 my-auto" style="font-size: 1.1rem; font-weight: 600;">
                                                            38,000
                                                            <span>
                                                                <img class="mw-100 my-auto" src="{{ asset('storage/icons/payments/blue-point-01.png') }}" alt="" style="max-height: 2.1rem;">
                                                            </span>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0" style="font-size: .8rem;">
                                                            *特別奬勵：90天完成 RM59,000 指定生意
                                                            <br>
                                                            額外獲得 RM4,000 現金花紅
                                                            <br>
                                                            <span class="text-danger">僅限50個位子</span>
                                                        </p>
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                -->

                            <!-- <div class="form-row mt-2" id="two-checkbox"> -->
                            {{-- <div class="form-row mt-2">
                                <div class="col-12" style="padding-left: .7rem;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="credit-debit-agree">
                                        <label class="custom-control-label" for="credit-debit-agree">
                                            I have read, understand and accepted
                                            the <a href="" data-toggle="modal"
                                                data-target="#purchaseAgreementModal">terms and conditions</a>.
                                        </label>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="form-row mt-2">
                                <div class="col-12" style="padding-left: .7rem;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="compliance">
                                        <label class="custom-control-label" for="compliance">
                                            I will comply and ensure strict compliance with guidelines as set by
                                            Formula Healthcare Sdn Bhd.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->

                            <!-- Next Button -->
                            <div class="text-right">
                            <div class="edit-opt-btn r-next-btn">
                                <!-- <a class="btn btn-secondary next-button" id="agreement-tab" data-toggle="tab" href="#agreement" role="tab" aria-controls="profile" aria-selected="false">Next</a> -->
                                {{-- <a class="btn btn-secondary next-button bjsh-btn-gradient " id="next-btn2"><b>Next</b></a> --}}
                                <!-- Submit Button -->

                                <input type="hidden" name="registrationFor" value="customer">
                                <button type="submit" id="submit" class="btn r-color-btn" style="color:#fff; border-radius: 20px;font-size:14px; padding:10px 20px;"><b>Sign Up</b></button>
                            </div>
                        </div>
                        </div>
                            <div id="error-div-2" class="row mt-2" style="display:none">
                                <div class="col-12">
                                    <p class="text-danger">Please make sure all fields are filled in.</p>
                                </div>
                            </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="purchaseAgreementModal" tabindex="-1" role="dialog" aria-labelledby="purchaseAgreementModal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-12 mb-0">
                        <div class="overflow-auto"
                            style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                            @if (country()->country_id == 'MY')
                                @include('shop.payment.service-terms-conditions-my')
                            @else
                                @include('shop.payment.service-terms-conditions-sg')
                            @endif


                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="emailErrorModal" tabindex="-1" role="dialog" aria-labelledby="emailErrorModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="emailErrorModalLabel">Email Is Currently Used</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.1rem;">
                    The email you entered has been used.
                    <br>
                    <br>
                    Please send an email to
                    <br>
                    <a style="font-size: 1.15rem; font-weight: 600;"
                        href="mailto:formula2u-cs@delhubdigital.com">formula2u-cs@delhubdigital.com</a> for further
                    assistance.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>

    $(document).ready(function () {
        bjsCustomerSelect();
    });

    function bjsCustomerSelect() {

    bjs_customer = $('input[name="bjs_customer"]:checked').val();

        if (bjs_customer == '1') {

            $('.if-bjs').show();
            $('.if-notbjs').hide();

        } else {

            $('.if-bjs').hide();
            $('.if-notbjs').show();
        }

    }
    $('input[name="bjs_customer"]').click(bjsCustomerSelect);

{{--    @error('email')--}}
{{--    $('#emailErrorModal').modal('show');--}}
{{--    @enderror--}}

  //  let paymentType = $('#paymentType');

    // const paymentCardDiv = $('#paymentCardDiv');
    // const paymentOfflineDiv = $('#paymentOfflineDiv');

    // $('#offline-payment-button').on('click', function() {
    //     paymentCardDiv.hide();
    //     paymentOfflineDiv.show();
    //     $('#offline-payment-button').hide();
    //     $('#card-payment-button').show();
    //     paymentType.val('offline');
    // });

    // $('#card-payment-button').on('click', function() {
    //     paymentOfflineDiv.hide();
    //     paymentCardDiv.show();
    //     $('#card-payment-button').hide();
    //     $('#offline-payment-button').show();
    //     paymentType.val('card');
    // });

    let priceSelectors = document.getElementsByClassName('input-mask-price');

    let priceInputMask = new Inputmask({
        'mask': '99999.99',
        'numericInput': true,
        'digits': 2,
        'digitsOptional': false,
        'placeholder': '0'
    });

    for (var i = 0; i < priceSelectors.length; i++) {
        priceInputMask.mask(priceSelectors.item(i));
    }

    // Credit/Debit Card JS
    // Credit card pattern recognition algorithm.
    // function detectCardType(number) {
    //     var re = {
    //         // electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
    //         // maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
    //         // dankort: /^(5019)\d+$/,
    //         // interpayment: /^(636)\d+$/,
    //         // unionpay: /^(62|88)\d+$/,
    //         visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
    //         mastercard: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/,
    //         // amex: /^3[47][0-9]{13}$/,
    //         // diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
    //         // discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
    //         // jcb: /^(?:2131|1800|35\d{3})\d{11}$/
    //     }

    //     for (var key in re) {
    //         if (re[key].test(number)) {
    //             return key
    //         }
    //     }
    // }

    $('#introducer_name').on('keyup', function() {
        if ($(this).val().length == 0 || $(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#introducer_id').on('keyup', function() {
        if ($(this).val().length < 10 || $(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#agent_group').on('change', function() {
        if ($(this).val() == 'default') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    // // Form Validation - Credit/Debit Card.
    // let cardNumber = null;
    // let ccIcon = $('.valid-feedback.feedback-icon.with-cc-icon i');

    // $('#card_number').on('keyup', function() {
    //     cardNumber = $(this).val();
    //     cardType = detectCardType(cardNumber);

    //     if ($(this).val().length < 7 || cardType == undefined) {
    //         $(this).removeClass('is-valid');
    //         $(this).addClass('is-invalid');
    //     } else {
    //         $(this).removeClass('is-invalid');
    //         $(this).addClass('is-valid');
    //         ccIcon.removeClass();
    //         ccIcon.addClass('fa fa-cc-' + cardType).css('font-size', '30px');
    //         $('#card_type').val(cardType);
    //     }
    // });

    // $('#expiry_date').on('keyup', function() {
    //     if ($(this).val().length != 4 || !$.isNumeric($(this).val())) {
    //         $(this).removeClass('is-valid');
    //         $(this).addClass('is-invalid');
    //     } else {
    //         $(this).removeClass('is-invalid');
    //         $(this).addClass('is-valid');
    //     }
    // });

    // $('#cvv').on('keyup', function() {
    //     if ($(this).val().length < 3 || !$.isNumeric($(this).val())) {
    //         $(this).removeClass('is-valid');
    //         $(this).addClass('is-invalid');
    //     } else {
    //         $(this).removeClass('is-invalid');
    //         $(this).addClass('is-valid');
    //     }
    // });

    // End Form Validation - Credit/Debit Card
    // End Credit/Debit Card JS

    // // Form Validation - Offline Payment
    // $('#offline_reference').on('keyup', function() {
    //     if ($(this).val().length == 0 || $(this).val() == '') {
    //         $(this).removeClass('is-valid');
    //         $(this).addClass('is-invalid');
    //     } else {
    //         $(this).removeClass('is-invalid');
    //         $(this).addClass('is-valid');
    //     }
    // });

    // $('#offline_amount').on('keyup', function() {
    //     if ($(this).val() == '') {
    //         $(this).removeClass('is-valid');
    //         $(this).addClass('is-invalid');
    //     } else {
    //         $(this).removeClass('is-invalid');
    //         $(this).addClass('is-valid');
    //     }
    // });


    // End Form Validation - Offline Payment

    let currentTab = $('.nav-tabs > .active');
    let nextTab = currentTab.next('li');

    // Handles tabs click.
    $('.nav-link').click(function() {
        currentTab = $(this).parent();
        $('.nav-tabs > .active').removeClass('active');
        currentTab.addClass('active');
        nextTab = currentTab.next('li');
    });

    // Form Validation
    $("#register-form").validate({
        rules: {
            invoice_number: {
                required: true,
                minlength: 14,
                maxlength: 14
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8,
            },
            password_confirmation: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            },
            referrer_id: {
                digits:true
            }
        },
        messages: {
            bjs_customer_id: {
                required: ""
            },
            invoice_number: {
                required: "Please enter an invoice number.",
                email: "The invoice number is not valid."
            },
            email: {
                required: "Please enter an email.",
                email: "The email is not valid."
            },
            password: {
                required: "Please enter a password.",
                minlength: "Password must be minimum of 8 characters."
            },
            password_confirmation: {
                required: "Please confirm your password",
                minlength: "Password must must be minimum of 8 characters",
                equalTo: "Password must be same as above"
            },
        }
    });

    // Validate fields in 1st tab.
    $('#next-btn').click(function() {
        if ($("#bjs_customer_1").is(':checked')) {
            if (
                $("#register-form").validate().element('#bjs_customer_id') &&
                $("#register-form").validate().element('#invoice_number') &&
                $("#register-form").validate().element('#password')
            )
            {
                $('#error-div-1').hide();
                nextTab.find('a').trigger('click');
            } else {
                $('#error-div-1').show();
            }
            if ($('#bjs_customer_id').val() === '') {
                $('#bjs_customer_id').addClass('is-invalid').focus();
            } else {
                $('#bjs_customer_id').removeClass('is-invalid');
            }
        } else {
            if (
                $("#register-form").validate().element('#email') &&
                $("#register-form").validate().element('#invoice_number') &&
                $("#register-form").validate().element('#password') &&
                $("#register-form").validate().element('#password-confirm')
            ) {
                $('#error-div-1').hide();
                nextTab.find('a').trigger('click');
            } else {
                $('#error-div-1').show();
            }
        }
    });

    // Validate fields in 2nd tab.
    $('#next-btn2').click(function() {
        if (
            $('#introducer_name').val() != '' &&
            $('#agent_group').val() != 'default'
        ) {
            $('#error-div-2').hide();
            nextTab.find('a').trigger('click');
        } else {
            $('#error-div-2').show();
            $('#introducer_name').addClass('is-invalid');
            $('#agent_group').addClass('is-invalid');
        }
    });

    // Validate form before submit.
    $('#register-form').on('submit', function(e) {
        let error = 0;

        if (
            $("#register-form").validate().element('#invoice_number')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#register-form").validate().element('#email')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $("#register-form").validate().element('#password')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (

            $("#register-form").validate().element('#password-confirm')
        ) {
            error = error;
        } else {
            error = error + 1;
        }

        if (
            $('#introducer_name').val().length == 0 || $('#introducer_name').val() == ''
        ) {
            error = error + 1;
            $('#introducer_name').addClass('is-invalid');
        }

        if ($('#agent_group').val() == 'default') {
            error = error + 1;
            $('#agent_group').addClass('is-invalid');
        }

        // if (paymentType.val() == 'card') {

        //     if (
        //         $('#card_number').val().length < 7 ||
        //         cardType == undefined
        //     ) {
        //         error = error + 1;
        //         $('#card_number').addClass('is-invalid');
        //         $('#card_number').focus();
        //     }

        //     if ($('#expiry_date').val().length != 4 || !$.isNumeric($('#expiry_date').val())) {
        //         error = error + 1;
        //         $('#expiry_date').addClass('is-invalid');
        //         $('#expiry_date').focus();
        //     }

        //     if ($('#cvv').val().length < 3 || !$.isNumeric($('#cvv').val())) {
        //         error = error + 1;
        //         $('#cvv').addClass('is-invalid');
        //         $('#cvv').focus();
        //     }
        // }

        // if (paymentType.val() == 'offline') {
        //     if ($('#offline_reference').val() == '' || $('#offline_reference').val().length == 0) {
        //         error = error + 1
        //         $('#offline_reference').addClass('is-invalid');
        //     }
        //     if ($('#payment_proof').get(0).files.length == 0) {
        //         error = error + 1;
        //         $('#no-file-error').show();
        //     }
        // }

        if (error == 0) {
            $('#error-div-3').hide();
            return true;
        } else {
            $('#error-div-3').show();
            return false;
        }
    });
    // End Form Validation

    //1
    $('#credit-debit-agree').on('change', function() {
        if ($('#credit-debit-agree').prop('checked', true)) {
            //$('#pay-now-button').prop('disabled', false);

            $('#compliance').on('change', function() {
             if ($('#compliance').prop('checked', true)) {
                 $('#pay-now-button').prop('disabled', false);
             } else {
                 $('#pay-now-button').prop('disabled', true);
             }
            });

        } else {
            $('#pay-now-button').prop('disabled', true);
        }
    });

    $('#compliance').on('change', function() {
        if ($('#compliance').prop('checked', true)) {
            //$('#pay-now-button').prop('disabled', false);

            $('#credit-debit-agree').on('change', function() {
             if ($('#credit-debit-agree').prop('checked', true)) {
                 $('#pay-now-button').prop('disabled', false);
             } else {
                 $('#pay-now-button').prop('disabled', true);
             }
            });

        } else {
            $('#pay-now-button').prop('disabled', true);
        }
    });


    function checkCustomerID(obj){


        let button = $(obj);
        let customer_id =  $('#bjs_customer_id').val();
        let loader = $('.spinner-border');

        //let count = button.find('.liked-btn');
        $.ajax({
            async: true,
            beforeSend: function() {
                loader.show();
            },
            complete: function() {
                loader.hide();
            },
            url: '{{route("guest.check.customerid")}}',
            type: "POST",
            data: {
                id: customer_id,
                _token: "{{ csrf_token() }}"
            },
            success: function(result) {
                $('#bjs_info').html('<small class="text-uppercase text-primary">' + result.data + '</small>');
                $('#bjs_customer_id').removeClass('is-invalid');
                $('#next-btn').removeClass('disabled');

            },
            error: function(result) {

                $('#bjs_info').html('<small class="form-text text-danger" >'+result.responseJSON.message+'.</small>');
                $('#bjs_customer_id').addClass('is-invalid');
                $('#next-btn').addClass('disabled');

            }
        });
    }



function checkInvoice(obj){

let input = $(obj);
let invoice_number =  $('#invoice_number').val();
let loader = $('.spinner-border');

//let count = button.find('.liked-btn');
$.ajax({
        async: true,
        beforeSend: function() {
            loader.show();
        },
        complete: function() {
            loader.hide();
        },
        url: '{{route("guest.get.invoice")}}',
        type: "POST",
        data: {
            invoice: invoice_number,
            _token: "{{ csrf_token() }}"
        },
        success: function(result) {

            const a = result.dealer_info;
            const b = a.agent_group;

            // console.log(result);
            // console.log(result.buyer_id);
            console.log(a.full_name);
            // console.log(a.referrer_id);
            // console.log(b.group_name);
            // console.log(a.group_id);

            // js get array
            // $('#bjs_info').html('<small class="text-uppercase text-primary">' + result.data + '</small>');
            // $('#bjs_customer_id').removeClass('is-invalid');

            //add introducer_id & agent_group

            // document.getElementById("referrer_name").value = result['buyer_id'];


            document.getElementById("referrer_name").value = a.full_name;
            document.getElementById("referrer_id").value = a.account_id;
            document.getElementById("agent_group").value =  a.group_id;

            if ( a.account_id == '3911000009') {
                $('#kklee').attr('selected', true);
            }

        }
    });
}

    // $('#credit-debit-agree').prop('checked', function() {
    //     if ($('#credit-debit-agree').prop('checked', true) && $('#compliance').prop('checked', true)) {
    //         $('#pay-now-button').prop('disabled', false);
    //     } else {
    //         $('#pay-now-button').prop('disabled', true);
    //     }
    // });

    //2
    // $('#credit-debit-agree').on('change', function() {
    //     if ($('#credit-debit-agree').prop('checked', true)) {
    //         if($('#compliance').prop('checked', true)){
    //             $('#pay-now-button').prop('disabled', false);
    //         } else{
    //             $('#pay-now-button').prop('disabled', true);
    //         }
    //     } else {
    //         $('#pay-now-button').prop('disabled', true);
    //     }
    // });

    //3
    // $('#compliance').on('change', function() {
    //     if ($('#compliance').prop('checked', true)) {
    //         $('#pay-now-button').prop('disabled', false);
    //     } else {
    //         $('#pay-now-button').prop('disabled', true);
    //     }
    // });

    //4
    // $('#credit-debit-agree').on('change', function() {
    //     if ($('#credit-debit-agree').prop('checked', true) && $('#compliance').prop('checked', true)) {
    //          $('#pay-now-button').prop('disabled', false);
    //     } else {
    //         $('#pay-now-button').prop('disabled', true);
    //     }
    // });

    //5
    // $('#credit-debit-agree').on('change', function(){
    //     var checkboxx = [];
    //         $.each($("input[name='2checkbox']:checked"), function(){
    //             $('#pay-now-button').prop('disabled', false);
    //         });
    // })

</script>
@endpush

@push('style')
<style>
    .btn.r-color-btn {
        width: auto;
        height: auto;
    }
    .btn.btn-outline-secondary {
        height: auto;
        margin-left: 0.1rem;
        width: auto;
        border: 1px solid #6c757d;
        border-top-right-radius: 30px;
        border-bottom-right-radius: 30px;
        font-size: smaller;
    }
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .error {
        color: red;
    }
</style>
@endpush
