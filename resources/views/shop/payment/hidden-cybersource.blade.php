<input type="hidden" name="access_key" value="{{ env('ACCESS_KEY', '38c31ee8397d33a799fef7b79b2f5307') }}">
<input type="hidden" name="profile_id" value="{{ env('PROFILE_ID', '725A4CC8-D06E-41F1-8217-9379CC6A3DED') }}">
<input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">
<input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency">
<input type="hidden" name="unsigned_field_names">
<input type="hidden" name="signed_date_time" value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
<input type="hidden" name="locale" value="MY">
<input type="hidden" name="transaction_type" value="sale">
<input type="hidden" name="reference_number" id="reference_number" value="<?php echo uniqid(); ?>">
<input type="hidden" name="amount" id="amount" value="{{ country()->country_id == 'SG' ? $launchCart->purchase_amount_myr : $purchase->purchase_amount }}">
<input type="hidden" name="currency" value="MYR">