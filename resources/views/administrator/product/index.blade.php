@extends('layouts.administrator.main')

@section('breadcrumbs')

@endsection

@section('content')

<div style="font-size:small;">
            <div class="row">
                <div class="col-12 text-right p-2">

                    <a style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark" data-toggle="modal" data-target="#newProduct" href="#newProduct">Create New Product</a>
                    {{-- <a href="{{ route('administrator.product.create') }}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Product</a> --}}
                </div>
            </div>

        <div class="col-md-4 offset-md-4" style="float:right;">&nbsp;</div>

        <form action="{{ route('administrator.product') }}" method="GET">
            <div class="col-md-4 offset-md-4" style="float:right;">
                <div class="col-auto ml-auto">
                    <select class="custom-select" id="selectSearch" name="selectSearch">
                        <option value="ALL" selected>Choose...</option>
                        <option value="name" {{ $request->selectSearch == 'name' ? 'selected' : '' }}>Product Name</option>
                        <option value="code" {{ $request->selectSearch == 'code' ? 'selected' : '' }}>Product Code</option>
                        <option value="category" {{ $request->selectSearch == 'category' ? 'selected' : '' }}>Category</option>
                    </select>

                    <input type="text" class="form-control" name="searchBox"
                    value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                </div>
                <div style="text-align:center;">
                    <div class="col-md-auto px-1">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                        <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.product') }} style="color:black;">Reset Filter</a></button>
                    </div>
                </div>
            </div>
        </form>

        <div class="col-md-4 offset-md-4" style="float:right;">&nbsp;</div>

        </div>

        <div class="col-12 mb-3">
            <p style="text-align:left;"><span style="font-size:20px;"> TOTAL Global Product : <b>{{ count($ForCountproducts)}}</b></span></p>
        </div>

        <div class="col-md-4-md-12" style="float:left;">
            {!! $products->render() !!}
        </div>

        <div class="table-responsive m-2">
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>Product Code</td>
                        <td>Image</td>
                        <td>Product Name</td>
                        <td>Categories</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td style="width: 5%;">{{ $loop->iteration }}</td>
                        <td style="width: 15%;">{{ $product->product_code }}</td>
                        <td style="width: 10%">

                            @if(isset($product->defaultImage))
                            <img class="mw-100" style="border-radius: 100%;" src="{{ asset('storage/' . $product->defaultImage->path . $product->defaultImage->filename) }}" alt="">
                            @endif
                        </td>
                        <td style="width: 35%;">{{ $product->name }}</td>
                        <td style="width: 15%;">
                            <ul class="list-unstyled">
                                @foreach($product->categories as $category)
                                <li>
                                    {{ $category->name }}
                                </li>
                                @endforeach
                            </ul>
                        </td>
                        <td style="width: 20%;">
                            <a style="color: white; font-style: normal; border-radius: 5px;" href="{{ route('administrator.product.editCreate', ['productId' => $product->id]) }}" class="btn btn-primary shadow-sm">Edit</a>

                            @if($product->product_status == 1)
                                <a style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-danger shadow-sm"
                                href="{{ route('administrator.product.unpublish', ['productId' => $product->id]) }}">Disable</a>
                            @endif

                            @if($product->product_status == 2)
                                <a style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-info shadow-sm"
                                href="{{ route('administrator.product.publish', ['productId' => $product->id]) }}">Enable</a>
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</div>

<div class="col-md-4-md-12" style="float:left;">
    {!! $products->render() !!}
</div>

<!-- Modal -->
<div class="modal fade" id="newProduct" tabindex="-1" role="dialog" aria-labelledby="newProductLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="newProductLabel">Prodct Code</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('administrator.product.create') }}" method="POST">
        <div class="modal-body row">
                @csrf
                <div class="col-12 col-md-4 text-md-right my-auto">
                    <p>
                       New Product Code
                    </p>
                </div>
                <div class="col-12 col-md-6 form-group">
                    <input type="text" name="productCode" id="productCode" class="form-control"
                        value="">
                    <div class="valid-feedback feedback-icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="invalid-feedback feedback-icon">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>

      </div>
    </div>
  </div>
@endsection

{{-- @push('script')
<script>
    $(document).ready(function() {
        $('#global-products-table').DataTable();
    });
</script>
@endpush --}}
