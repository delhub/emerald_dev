<?php

namespace App\Http\Controllers\Administrator\Archimedes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Agent\AgentStockController;
use App\Models\Dealers\DealerData;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Dealers\DealerTask;
use Illuminate\Support\Facades\DB;
use App\Models\Dealers\DealerStockLedger;
use App\Models\Globals\Image;
use App\Models\Globals\Products\Product;
use App\Models\Globals\Status;
use Carbon\Carbon;
use App\Http\Controllers\Agent\AgentStockController as StockController;
use App\Models\Dealers\DealerPrebuyRedemption;
use App\Models\Purchases\Item;
use App\Traits\Paginate;

class ArchimedesController extends Controller
{
    use Paginate;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $dealerName = DealerInfo::all();
        $archimedesDatas = array();
        $eachProductCodes = null;

        // get all dealer data
        $dealerDatas = DealerData::searchFilter($request->search)->get()->groupBy('dealer_id');

        // archimedes maindata = archimedes project
        foreach ($dealerDatas as $key => $dealerData) {
            $archimedesData = $product_code =  array();

            // $archimedesData['acd_main'] = $dealerData->where('name','archimedes_project')->first();
            $archimedesData['acd_main'] = $dealerData->first();

            if ($archimedesData['acd_main']) {
                // get all acd product from dealer data + value
                $archimedesData['acd_product'] = $dealerData->where('name', '!=', 'archimedes_project');

                // switch case to get product code
                foreach ($archimedesData['acd_product'] as  $acd_product) {
                    $product_code[] = $this->dealerDataCaseSwitch($acd_product->name);
                }

                //if (empty($product_code)) dd($key);
                // dd($product_code);
                // get all quantity from dealer stockledger

                // return $key;
                $archimedesData['stock_ledger'] = DealerStockLedger::whereIn('product_code', $product_code)->where('agent_id', $key)->get();



                $archimedesDatas[] = $archimedesData;

                // get each product code for preg match
                $eachProductCodes = $archimedesData['stock_ledger']->groupBy('product_code');
            }
        }
        $archimedesDatas = $this->paginate($archimedesDatas);
        //  change href pagination from /?page=2 to ?page=2
        $archimedesDatas->setPath('');

        $vqsProducts = Product::where('product_type', 'vqs')->get();

        return view('administrator.vqs.index', compact(['archimedesDatas', 'dealerName', 'eachProductCodes', 'request', 'vqsProducts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailsArchimedes($id)
    {

        // get all dealer data
        $dealerDatas = DealerData::all()->groupBy('dealer_id');
        $parentProduct = Product::all();
        $statusLabel = Status::all();
        $productImage = Image::all();

        $archimedesData = array();

        // archimedes maindata = archimedes project
        foreach ($dealerDatas as $key => $dealerData) {

            $archimedesData['acd_product'] = $dealerData->where('name', '!=', 'archimedes_project');
            // switch case to get product code
            foreach ($archimedesData['acd_product'] as  $acd_product) {
                $product_code[] = $this->dealerDataCaseSwitch($acd_product->name);
            }
        }

        $summaryArchimedes = StockController::agentProductStock($product_code, $id);


        return view('administrator.vqs.details', compact(['summaryArchimedes', 'productImage', 'parentProduct', 'statusLabel']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dealerDataCaseSwitch($request)
    {
        // switch ($request) {
        //     case 'tss-king':
        //         $product_code = 'BU0920-0103-0006-K';
        //         break;

        //     case 'tss-queen':
        //         $product_code = 'BU0920-0103-0006-Q';
        //         break;

        //     case 'silkvenus':
        //         $product_code = 'BU1220-0103-0020';
        //         break;

        //     default:
        //     $product_code = '0';
        //         break;
        // }

        return $request;
        // return $product_code;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function topupACD(Request $request)
    {
        foreach ($request->topup as $key => $topup) {
            if ($topup != 0) {
                $stock = new DealerStockLedger;
                $stock->type = ($topup > 0) ? 5002 : 5004;
                $stock->product_code = $this->dealerDataCaseSwitch($key);
                $stock->quantity = $topup;
                // $stock->balance =  - AgentStockController::agentStockQuantity($stock->product_code , $stock->quantity);
                $stock->balance =  AgentStockController::agentStockBalance($stock->product_code, $request->dealer_id) + $stock->quantity;
                $stock->agent_id = $request->dealer_id;
                $stock->date = Carbon::now();

                $stock->save();
                // return $stock;
            }
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function addDealerData($itemIDs, $ledgerStatus = null)
    {
        if ($ledgerStatus) {
            $items = Item::whereIn('id', $itemIDs)->get();
            $qty = 0;
            $itemsID = array();
            foreach ($items as $item) {
                if ($item->attributeBundle != NULL && isset($item->attributeBundle)) {
                    $dealerData = DealerData::firstOrNew(
                        [
                            'dealer_id' => $item->order->purchase->user->dealerInfo->account_id,
                            'name' => $item->attributeBundle->primary_product_code
                        ]
                    );

                    if ($dealerData) {
                        $qty = $dealerData->value;
                    }
                    $product_type = 'undefined';
                    // $dealerData->dealer_id = $item->order->purchase->dealerInfo->account_id;
                    if ($item->attributes && $item->attributes->product2 && $item->attributes->product2->parentProduct) {
                        $product_type = $item->attributes->product2->parentProduct->product_type;
                    }

                    $dealerData->type = $product_type;
                    $dealerData->name = $item->attributeBundle->primary_product_code;
                    $dealerData->value = $qty + ($item->attributeBundle ? ($item->attributeBundle->primary_quantity * $item->quantity) : $item->quantity);
                    $dealerData->save();

                    $itemsID[] = $item->id;
                }
            }
        }

        self::addDealerStockLedger($itemsID, $ledgerStatus, 0);
    }

    public static function addDealerStockLedger($itemIDs, $ledgerStatus, $prebuy_redemption_id = 0)
    {
        $items = Item::whereIn('id', $itemIDs)->get();

        foreach ($items as $item) {

            switch ($item->item_order_status) {
                case 1010:
                    $type = 5003;
                    break;
                default:
                    $type = 5001;
                    break;
            }

            // $ledgerStatus is refill
            $stock = new DealerStockLedger;
            $stock->ref_id = $item->order->purchase->getFormattedNumber();
            $stock->type = $ledgerStatus ? '5002' : $type;
            $stock->product_code = $ledgerStatus ? $item->attributeBundle->primary_product_code : $item->product_code;
            $stock->prebuy_redemption_id = $prebuy_redemption_id;
            $stock->item_id = $item->id;
            $stock->quantity = $ledgerStatus ? ($item->attributeBundle ? ($item->attributeBundle->primary_quantity * $item->quantity) : $item->quantity) : -$item->quantity;

            $stock->balance = $ledgerStatus ? $stock->quantity : (AgentStockController::agentStockBalance($item->product_code, $item->order->purchase->user->userInfo->agentInformation->account_id) + $stock->quantity);

            $stock->agent_id = $ledgerStatus ? $item->order->purchase->user->dealerInfo->account_id : $item->order->purchase->user->userInfo->referrer_id;
            $stock->date = Carbon::now();

            $stock->save();
        }
    }

    public static function checkPrePurchaseProduct($order)
    {
        //
        $prePurchase = array(
            'normal' => array(),
            'vqs' => array(),
            'buyVqs' => array(),
            'none-above' => array(),

        );

        foreach ($order->items as  $item) {
            if ($item->product && $item->product->parentProduct && $item->product->parentProduct->product_type == 'single') {
                $prePurchase['normal'][] = $item->product_code;
            } elseif ($item->product && $item->product->parentProduct && $item->product->parentProduct->product_type == 'vqs') {
                $prePurchase['vqs'][] = $item->id;
            } else {
                $prePurchase['none-above'][] = $item->product_code;
            }
        }

        return $prePurchase;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pendingApproval(Request $request)
    {
        //
        switch ($request->status) {
            case 'approved':
                $status = 5007;
                break;

            default:
                $status = 5006;
                break;
        }
        $dealerPreBuys = DealerPrebuyRedemption::all();
        $preBuyRedemptions = array();
        // foreach ($dealerPreBuys as $dealerPreBuy) {
        //     $preBuyRedemptions['dealer_id'] = $dealerPreBuy->dealer_id;

        //     $item_id = json_decode($dealerPreBuy->redemption_data);
        //     foreach ($item_id as $item_id) {
        //         # code...
        //     }
        // }
        // dd($preBuyRedemptions);

        return view('administrator.vqs.prebuy', compact(['dealerPreBuys']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function submitApproval($id)
    {
        $dealerPreBuys = DealerPrebuyRedemption::where('id', $id)->first();
        $dealerPreBuys->prebuy_status = 5007;
        $dealerPreBuys->save();

        $itemIDs = json_decode($dealerPreBuys->redemption_data);

        self::addDealerStockLedger($itemIDs, null, $dealerPreBuys->id);

        return redirect()->route('administrator.vqs.pending.approval');
    }
}
