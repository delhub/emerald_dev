@php

use Illuminate\Support\Facades\Auth;
use App\Models\Sidemenus\Sidemenu;
use App\Models\Dealers\DealerData;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Globals\Countries;

$banner_county = 'MY';
$country = country()->country_id;
                    
if($country == 'SG'){
    $banner_county = 'SG';
}

$menus = Sidemenu::orderBy('menu_arrangement','ASC')->get();
$allMenus = Sidemenu::pluck('menu_title','id')->all();

@endphp
<div class="sidebar sidebar-bg-color-dealer" style=" width: 240px;">
    {{-- {{$menudata}} --}}
    <nav class="sidebar-nav">
        <ul class="list-unstyled components">

            <br>
            <h4 style="color:black; padding: 10px 20px;"><strong>Dashboard</strong></h4>

            <!-- Dynamic menu start -->
            @foreach($menus as $menu)
            @if($menu->menu_group == "AM" && $menu->menu_show == 1 && $menu->menu_parent == "0"  && $menu->country_id == $banner_county)
            <li class="text-hover">

                <a href="{{$menu->menu_link}}" class="sidebar-text-color ">
                    {{ $menu->menu_title }}
                </a>
                <ul class="list-unstyled" style="padding: 0px 0;">
                    @if(count($menu->childs))
                    @include('layouts.agent.navigations.menusub',['childs' => $menu->childs])
                    @endif
                </ul>

            </li>
            @endif
            @endforeach

            <!-- Dynamic menu end -->

            {{-- <li class="text-hover">
                <a href="{{route('agent.overview')}}" class="sidebar-text-color ">Overview</a>
            </li>
            <li class="text-hover">
                <a href="{{route('shop.wip')}}" class="sidebar-text-color ">Sales Summary</a>
            </li>
            <li class="text-hover">
                <a href="{{route('shop.wip')}}" class="sidebar-text-color ">Group Performance</a>
            </li>
            <li class="text-hover">
                <a href="{{route('agent.income')}}" class="sidebar-text-color ">Monthly Income</a>
            </li>
            <li class="text-hover">
                <a href="{{route('shop.wip')}}" class="sidebar-text-color ">Business Progress</a>
            </li>
            <li class="text-hover">
                <a href="{{route('agent.announcements')}}" class="sidebar-text-color ">Announcements</a>
            </li>
            <li class="text-hover">
                <a href="{{route('shop.wip')}}" class="sidebar-text-color ">Feedbacks</a>
            </li>
            <li class="text-hover">
                <a href="{{route('shop.wip')}}" class="sidebar-text-color ">Promotion/Special Offer</a>
            </li> --}}
            {{-- @hasrole('panel')
         <li class="text-hover">
            <a href="{{route('management.panel.home')}}" class="sidebar-text-color ">Panel Dashboard</a>
            </li>
            @endhasrole --}}
            <li class="text-hover">
                <a href="/shop" class="sidebar-text-color ">Continue Shopping</a>
            </li>
        </ul>
    </nav>
</div>


{{-- <ul class="nav sidebar-margin-top">

            <li class="nav-item" >
              <div class="icon-text-align">
                 <a class="nav-link " href="/shop/dashboard/orders/index">
                    <img class="img-avatar" src="{{asset('/storage/panel-dashboard-icons/value-orders.png')}}" alt="My
Orders" style="height: 100px; width: 100px; border-radius:0px;">
</a>

</div>
</li>


<li class="nav-item">
    <div class="icon-text-align">
        <a class="nav-link" href="/shop/dashboard/wishlist/index">
            <img class="img-avatar" src="{{asset('/storage/panel-dashboard-icons/perfect-list.png')}}" alt="Wish List"
                style="height: 100px; width: 100px; border-radius:0px;">
        </a>

    </div>
</li>

<li class="nav-item">
    <div class="icon-text-align">
        <a class="nav-link" href="/shop/dashboard/change-password">
            <img class="img-avatar" src="{{asset('/storage/panel-dashboard-icons/change-password.png')}}"
                alt="Change Password" style="height: 100px; width: 100px; border-radius:0px;">
        </a>

    </div>
</li>


<li class="nav-item">
    <div class="icon-text-align">
        <a class="nav-link" href="/shop">
            <img class="img-avatar" src="{{asset('/storage/panel-dashboard-icons/shop-icon.png')}}" alt="Shop"
                style="height: 100px; width: 100px; border-radius:0px;">
        </a>

    </div>
</li>

<li class="nav-item">
    <div class="icon-text-align">
        <a class="nav-link" href="/management/dealer/sales-summary">
            <img class="img-avatar" src="{{asset('/storage/panel-dashboard-icons/panel.png')}}" alt="Sales"
                style="height: 100px; width: 100px; border-radius:0px;">

        </a>
        <p style="font-size: small;">Sales Summary</p>


    </div>
</li>


@hasrole('panel')
<li class="nav-item">
    <div class="icon-text-align">
        <a class="nav-link" href="/management/panel/orders">
            <img class="img-avatar" src="{{asset('/storage/panel-dashboard-icons/panel-page.png')}}" alt="Panel"
                style="height: 100px; width: 100px; border-radius:0px;">
        </a>

    </div>
</li>
@endhasrole


</ul> --}}

{{-- <button class="sidebar-minimizer brand-minimizer" type="button"></button> --}}






<style>
    .text-hover {
        padding-bottom: 5px;
        padding-top: 5px;
        width: 250px;
        margin-left: -10px;
    }

    .text-hover:hover {
        color: #000;
        background: #fbcc34;
        text-decoration: none !important;
    }

    .sidebar-text-color {
        font-size: 14pt;
        color: black;
        margin: 0 0 0 30px;
    }

    .sidebar-text-color:hover {
        color: black;
    }
</style>