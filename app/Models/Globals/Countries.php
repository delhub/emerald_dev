<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;

class Countries extends Model
{
    protected $table = 'global_countries';

    // Set primary key
    protected $primaryKey = 'country_id';
    public $incrementing = false;


    public static function currentCountry()
    {

        $cookies = self::siteCountry();

        return self::where('country_id', $cookies)->first();
    }

    public static function currentConvertionRateMarkup()
    {
        $data = self::currentCountry();

        $conversion_rate_markup = $data->conversion_rate;

        return $conversion_rate_markup;
    }

    public static function siteCountry()
    {

        $domain = preg_replace("~^www\.~", "", Request::getHost());

        switch ($domain) {
            case env('SG_DOMAIN', 'bujishu_web.test.sg'):
                $country = 'SG';
                break;
            case env('MY_DOMAIN', 'bujishu_web.test'):
            default:
                $country = 'MY';
                break;
        }

        return $country;
    }


    public static function getAll()
    {

        $countries = self::all();
        $return = array();
        foreach ($countries as $country) {
            $return[$country->country_id] = $country;
        }
        return $return;
    }

    public function getUrlAttribute()
    {

        return env($this->country_id . '_DOMAIN', 'www.bujishu.com');
    }
}
