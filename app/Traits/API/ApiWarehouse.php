<?php

namespace App\Traits\API;

use App\Models\Products\WarehouseBatch;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Products\WarehouseBatchItem;

trait ApiWarehouse
{
    public function getWHbatch()
    {
        $data = WarehouseBatch::all();

        if(count($data)) {
            return json_encode($data);
        }
    }

    public function getWHbatchDetail()
    {
        $data = WarehouseBatchDetail::all();

        if(count($data)) {
            return json_encode($data);
        }
    }

    public function getWHbatchItem()
    {
        $data = WarehouseBatchItem::all();

        if(count($data)) {
            return json_encode($data);
        }
    }

    public function checkWHbatchItem($id)
    {
        $data = WarehouseBatchItem::where('items_id',$id)->first();

        if($data) {
            if(is_null($data->picking_order_id) && is_null($data->type)) {
                return json_encode(true);
            }
            else {
                return json_encode(false);
            }
        }
    }

}
