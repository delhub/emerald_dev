<?php

namespace App\Http\Controllers\Administrator\Ordertracking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Purchases\Item;

class SummaryController extends Controller
{
    public static function getProduct($request){
        //Panels
        $panels = PanelInfo::all();
        //Items
        $items = Item::selectRaw('items.*, items.id')
            ->join('orders', 'orders.order_number', '=', 'items.order_number')
            ->whereNotIn('items.item_order_status', ['1000']) // remove status record created
            ->whereNotIn('orders.order_status', ['1000']) // remove order status record created
            ->panelFilter($request->panel)
            ->statusFilter($request->status, $request->date, $request->period)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->orderBy('items.product_code', 'DESC')
            ->orderBy('items.created_at', 'DESC');

        $ForCountcustomerOrders = $items->get();

        $items = $items->paginate(1000);
        $items->appends(request()->query());

        $list = array();

        foreach ($items as $item) {

            if (isset($item->product_code) && !empty($item->product_code)) {
                $productnumber = $item->product_code;
            } else {
                $productnumber = $item->product_id;
            }

            $product_info = $item->product_information;
            $shipdate = $item->actual_ship_date;

            if (!isset($list[$productnumber])) {

                //$list[$productnumber]['description'] = '';
                $list[$productnumber]['not_preorder']['case'] = 0;
                $list[$productnumber]['not_preorder']['qty'] = 0;
                // $list[$productnumber]['not_preorder']['items'] = array();
                $list[$productnumber]['not_preorder']['itemsid'] = array();
                $list[$productnumber]['not_preorder']['total_ordered'] = 0;
                $list[$productnumber]['not_preorder']['ordered_quantity'] = 0;

                $list[$productnumber]['preorder']['case'] = 0;
                $list[$productnumber]['preorder']['qty'] = 0;
                // $list[$productnumber]['preorder']['items'] = array();
                $list[$productnumber]['preorder']['itemsid'] = array();
                $list[$productnumber]['preorder']['total_ordered'] = 0;
                $list[$productnumber]['preorder']['ordered_quantity'] = 0;

                $list[$productnumber]['got_remark']['case'] = 0;
                $list[$productnumber]['got_remark']['qty'] = 0;
                // $list[$productnumber]['got_remark']['items'] = array();
                $list[$productnumber]['got_remark']['itemsid'] = array();
                $list[$productnumber]['got_remark']['total_ordered'] = 0;
                $list[$productnumber]['got_remark']['ordered_quantity'] = 0;
                $list[$productnumber]['got_remark']['id'] = 0;

                $list[$productnumber]['total']['case'] = 0;
                $list[$productnumber]['total']['qty'] = 0;
                // $list[$productnumber]['total']['items'] = array();
                $list[$productnumber]['total']['itemsid'] = array();
                $list[$productnumber]['total']['total_ordered'] = 0;
                $list[$productnumber]['total']['ordered_quantity'] = 0;
            }

            if (array_key_exists('product_order_remark', $product_info)) {
                // got remark
                $list[$productnumber]['got_remark']['case']++;
                $list[$productnumber]['got_remark']['qty'] += $item->quantity;
                // $list[$productnumber]['got_remark']['items'][] = $item;
                $list[$productnumber]['got_remark']['itemsid'][] = $item->id;

                if ($shipdate != 'Pending') {
                    $list[$productnumber]['got_remark']['total_ordered']++;
                    $list[$productnumber]['got_remark']['ordered_quantity'] += $item->quantity;
                }
            } else if (array_key_exists('product_preorder_date', $product_info)) {
                // got preorder
                $list[$productnumber]['preorder']['case']++;
                $list[$productnumber]['preorder']['qty'] += $item->quantity;
                // $list[$productnumber]['preorder']['items'][] = $item;
                $list[$productnumber]['preorder']['itemsid'][] = $item->id;

                if ($shipdate != 'Pending') {
                    $list[$productnumber]['preorder']['total_ordered']++;
                    $list[$productnumber]['preorder']['ordered_quantity'] += $item->quantity;
                }
            } else {
                //  no preorder
                $list[$productnumber]['not_preorder']['case']++;
                $list[$productnumber]['not_preorder']['qty'] += $item->quantity;
                // $list[$productnumber]['not_preorder']['items'][] = $item;
                $list[$productnumber]['not_preorder']['itemsid'][] = $item->id;
                if ($shipdate != 'Pending') {
                    $list[$productnumber]['not_preorder']['total_ordered']++;
                    $list[$productnumber]['not_preorder']['ordered_quantity'] += $item->quantity;
                }
            }

            $list[$productnumber]['total']['case']++;
            $list[$productnumber]['total']['qty'] += $item->quantity;
            // $list[$productnumber]['total']['items'][] = $item;
            $list[$productnumber]['total']['itemsid'][] = $item->id;

            if ($shipdate != 'Pending') {
                $list[$productnumber]['total']['total_ordered']++;
                $list[$productnumber]['total']['ordered_quantity'] += $item->quantity;
            }
            $list[$productnumber]['information'] = $product_info;
            $list[$productnumber]['item'] = $item;
        }

        $product['panels'] = $panels;
        $product['items'] = $items;
        $product['list'] = $list;
        $product['ForCountcustomerOrders'] = $ForCountcustomerOrders;
        return $product;
    }
    public function index(Request $request)
    {
        $product = self::getProduct($request);

        $panels = $product['panels'];
        $items = $product['items'];
        $list = $product['list'];
        $ForCountcustomerOrders = $product['ForCountcustomerOrders'];

        return view('administrator.ordertracking.summary')
            ->with('panels', $panels)
            ->with('request', $request)
            ->with('items', $items)
            ->with('list', $list)
            ->with('ForCountcustomerOrders', $ForCountcustomerOrders);
    }
    //Download Customer
    public function exportCsvPanel(Request $request)
    {
        $fileName = 'Order Tracking Details (Customer).csv';

        $product = self::getProduct($request);

        $panels = $product['panels'];
        $items = $product['items'];
        $list = $product['list'];
        $ForCountcustomerOrders = $product['ForCountcustomerOrders'];

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array(
            'Product Code/ID',
            'Product Name',
            'Panel',
            'Instant Order (Total Item to Panel)',
            'Instant Order (Total Item)',
            'Instant Order (Total Qty to Panel)',
            'Instant Order (Total Qty)',
            'Pre-order (Total Item to Panel)',
            'Pre-order (Total Item)',
            'Pre-order (Total Qty to Panel)',
            'Pre-order (Total Qty)',
            'Remarks (Total Item to Panel)',
            'Remarks (Total Item)',
            'Remarks (Total Qty to Panel)',
            'Remarks (Total Qty)',
            'Total (Total Item to Panel)',
            'Total (Total Item)',
            'Total (Total Qty to Panel)',
            'Total (Total Qty)'
        );

        $callback = function () use ($list, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($list as $key => $listresult) {

                $row['Product Code']    = $key;
                $row['Product Name']    = is_object( $listresult['item']->product) ? $listresult['item']->product->parentProduct->name : '';
                $row['Panel']    = is_object( $listresult['item']->product) ? $listresult['item']->product->parentProduct->name : '';

                $row['Instant Order (Total Item to Panel)']    = is_object( $listresult['item']->product) ? $listresult['not_preorder']['total_ordered'] : '';
                $row['Instant Order (Total Item)']    = is_object( $listresult['item']->product) ? $listresult['not_preorder']['case'] : '';
                $row['Instant Order (Total Qty to Panel)']    = is_object( $listresult['item']->product) ? $listresult['not_preorder']['ordered_quantity'] : '';
                $row['Instant Order (Total Qty)']    = is_object( $listresult['item']->product) ? $listresult['not_preorder']['qty'] : '';

                $row['Pre-order (Total Item to Panel)']    = is_object( $listresult['item']->product) ? $listresult['preorder']['total_ordered'] : '';
                $row['Pre-order (Total Item)']    = is_object( $listresult['item']->product) ? $listresult['preorder']['case'] : '';
                $row['Pre-order (Total Qty to Panel)']    = is_object( $listresult['item']->product) ? $listresult['preorder']['ordered_quantity'] : '';
                $row['Pre-order (Total Qty)']    = is_object( $listresult['item']->product) ? $listresult['preorder']['qty'] : '';

                $row['Remarks (Total Item to Panel)']    = is_object( $listresult['item']->product) ? $listresult['got_remark']['total_ordered'] : '';
                $row['Remarks (Total Item)']    = is_object( $listresult['item']->product) ? $listresult['got_remark']['case'] : '';
                $row['Remarks (Total Qty to Panel)']    = is_object( $listresult['item']->product) ? $listresult['got_remark']['ordered_quantity'] : '';
                $row['Remarks (Total Qty)']    = is_object( $listresult['item']->product) ? $listresult['got_remark']['qty'] : '';

                $row['Total (Total Item to Panel)']    = is_object( $listresult['item']->product) ? $listresult['total']['total_ordered'] : '';
                $row['Total (Total Item)']    = is_object( $listresult['item']->product) ? $listresult['total']['case'] : '';
                $row['Total (Total Qty to Panel)']    = is_object( $listresult['item']->product) ? $listresult['total']['ordered_quantity'] : '';
                $row['Total (Total Qty)']    = is_object( $listresult['item']->product) ? $listresult['total']['qty'] : '';

                fputcsv($file, array(
                    $row['Product Code'],
                    $row['Product Name'],
                    $row['Instant Order (Total Item to Panel)'],
                    $row['Instant Order (Total Item)'],
                    $row['Instant Order (Total Qty to Panel)'],
                    $row['Instant Order (Total Qty)'],

                    $row['Pre-order (Total Item to Panel)'],
                    $row['Pre-order (Total Item)'] ,
                    $row['Pre-order (Total Qty to Panel)'],
                    $row['Pre-order (Total Qty)'],

                    $row['Remarks (Total Item to Panel)'],
                    $row['Remarks (Total Item)'],
                    $row['Remarks (Total Qty to Panel)'],
                    $row['Remarks (Total Qty)'],

                    $row['Total (Total Item to Panel)'],
                    $row['Total (Total Item)'],
                    $row['Total (Total Qty to Panel)'],
                    $row['Total (Total Qty)']
                ));
            }
                    fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
