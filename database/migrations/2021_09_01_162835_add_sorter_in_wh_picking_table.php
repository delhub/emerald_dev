<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Purchases\Order;

class AddSorterInWhPickingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wh_picking', function (Blueprint $table) {
            $table->bigInteger('sorter')->default(0)->after('picker');
        });

        Schema::table('pick_bin', function (Blueprint $table) {
            $table->bigInteger('picker')->default(0)->nullable(false)->change();
            $table->bigInteger('sorter')->default(0)->after('picker');
            $table->bigInteger('packer')->default(0)->after('sorter');
        });

        //update old orders from 5 update to 9 (ignore)
        $orders = Order::where('orders.pick_pack_status', 5)->get();
        foreach ($orders as $key => $order) {
            $order->pick_pack_status = 9;
            $order->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wh_picking', function (Blueprint $table) {
            $table->dropColumn('sorter');
        });

        Schema::table('pick_bin', function (Blueprint $table) {
            $table->dropColumn('sorter');
            $table->dropColumn('packer');
        });
    }
}
