<?php

namespace App\Jobs\PDF;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\Emails\SendPurchaseOrderEmail;
use PDF;
use App;
use Illuminate\Support\Facades\File;
use App\Models\Globals\Countries;
use Illuminate\Support\Facades\Storage;
use setasign\Fpdi\Fpdi;
use stdClass;


class GeneratePdfPoDoEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    protected $emailAddress;
    public $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order, $emailAddress = null)
    {
        $this->order = $order;
        $this->emailAddress = $emailAddress;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        self::generatePoDoPdf($this->order);

        // Queue PO email to customer.
        if ($this->emailAddress) {
            //SendPurchaseOrderEmail::dispatch($this->emailAddress, $this->order);
        }
    }

    public static function generatePoDoPdf($order)
    {
        if (!App::environment('localhost')) {

            $countries = Countries::getAll();
            $country = $countries[$order->country_id];

            // Generate PO PDF.
            // Generate PDF.
            // $pdf = PDF::loadView('documents.purchase.v2.purchase-order', compact('order', 'country'));
            // // Get PDF content.
            // $content = $pdf->download()->getOriginalContent();
            // // Set path to store PDF file.
            // $pdfDestination = public_path('/storage/documents/invoice/' . $order->purchase->getFormattedNumber() . '/purchase-orders/');
            // // Set PDF file name.
            // $pdfName = $order->order_number;
            // // Check if directory exist or not.
            // if (!File::isDirectory($pdfDestination)) {
            //     // If not exist, create the directory.
            //     File::makeDirectory($pdfDestination, 0777, true);
            // }
            // // Place the PDF into directory.
            // File::put($pdfDestination . $pdfName . '.pdf', $content);

            // Generate DO
            $orderDeliveryInfo = json_decode($order->delivery_info);
            // $poslaju = $orderDeliveryInfo->checkout[0];
            $poslaju = $orderDeliveryInfo = [];
            // Regenerate DO PDF
            $pdf = PDF::loadView('documents.purchase.v2.delivery-order', compact('order', 'country', 'poslaju', 'orderDeliveryInfo'))->setpaper('a5');
            // Get PDF content.
            $content = $pdf->download()->getOriginalContent();
            // Set path to store PDF file.
            $pdfDestination = public_path('/storage/documents/invoice/' . $order->purchase->getFormattedNumber() . (($order->purchase->invoice_version != 0) ? '/v' . $order->purchase->invoice_version : '') . '/delivery-orders/');
            // Set PDF file name.
            $pdfName = $order->delivery_order;
            // Check if directory exist or not.
            if (!File::isDirectory($pdfDestination)) {
                // If not exist, create the directory.
                File::makeDirectory($pdfDestination, 0777, true);
            }


            // Place the PDF into directory.
            File::put($pdfDestination . $pdfName . '.pdf', $content);

            $orderDeliveryInfo = json_decode($order->delivery_info);

            // dd($orderDeliveryInfo);

            if (isset($orderDeliveryInfo) && $orderDeliveryInfo->consignment_note != new stdClass()) {
                $poslaju = $orderDeliveryInfo->checkout[0];
                $poslajuPdf =  Storage::path('public/uploads/poslaju/' . $orderDeliveryInfo->consignment_note);
                // return PDF::loadFile(response()->file($poslajuPdf));
                // return response()->file($poslajuPdf);
                $newpdf = new Fpdi();

                $newpdf->setSourceFile($poslajuPdf);

                $tplIdx = $newpdf->importPage(1);

                // here i add the $pageCount to count all page total
                $pageCount = $newpdf->setSourceFile($pdfDestination . $pdfName . '.pdf', $content);

                // add new code for poslaju
                for ($pageNum = 1; $pageNum <= $pageCount; $pageNum++) {

                    $pageId = $newpdf->importPage($pageNum);

                    $size = $newpdf->getTemplateSize($pageId);

                    $newpdf->AddPage($size['orientation'], $size);

                    $lastPageNum = $pageCount;

                    if ($orderDeliveryInfo != NULL && $pageNum == $lastPageNum) {
                        $newpdf->useTemplate($tplIdx, 0, null,300, 210, false);
                    } else {
                    }
                    // add it
                    $newpdf->useTemplate($pageId);
                }

                // add new page if the page number is single
                if ($pageCount == 3 || $pageCount == 5 || $pageCount == 7 || $pageCount == 9) {
                    $newpdf->AddPage($size['orientation'], $size);
                } else {
                }
                // add new code for poslaju end

                //$newpdf->AddPage('L');

                //$newpdf->useTemplate($tplIdx);
                //$pageId = $newpdf->importPage(1);

                // add it
                //$newpdf->useTemplate($pageId);

                // $newpdf->Output();

                File::put($pdfDestination . $pdfName . '.pdf', $newpdf->output('S'));
            }
        } else {
            \Log::info('simulate.po.do.pdf.created');
        }
    }
}
