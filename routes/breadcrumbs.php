<?php


use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Home
Breadcrumbs::for('shop.Home', function ($trail) {
    $trail->push('Home', route('shop.index'));
});

//new add chuah - shop > home
// Breadcrumbs::for('shop.index', function ($trail) {
//     $trail->push('index', route('shop.index'));
// });


// Home > [Category]
Breadcrumbs::for('shop.category.first', function ($trail, $category) {
    $trail->parent('shop.Home');
    $trail->push($category->name, route('shop.category.first', $category->slug));
});

// Home > [Category] > [Subcategory]
Breadcrumbs::for('shop.category.second', function ($trail, $category) {
    $trail->parent('shop.Home');
    $trail->push($category->parentCategory->name, route('shop.category.first', $category->parentCategory->slug));
    $trail->push($category->name, route(
        'shop.category.second',
        [
            'topLevelCategorySlug' => $category->parentCategory->slug,
            'secondLevelCategorySlug' => $category->slug
        ]
    ));
});

// Home > [Category] > [Subcategory] > [Product Type]
Breadcrumbs::for('shop.category.third', function ($trail, $category, $parentCategory ) {
    $trail->parent('shop.Home');
    $trail->push($parentCategory, route('shop.category.first', $parentCategory));
    $trail->push($category->parentCategory->name, route(
        'shop.category.second',
        [
            'topLevelCategorySlug' => $parentCategory,
            'secondLevelCategorySlug' => $category->parentCategory->slug
        ]
    ));
    $trail->push($category->name, route(
        'shop.category.third',
        [
            'topLevelCategorySlug' => $parentCategory,
            'secondLevelCategorySlug' => $category->parentCategory->slug,
            'thirdLevelCategorySlug' => $category->slug
            // 'productTypeSlug' => '123'
        ]
    ));
    
});

include('breadcrumbs/management.php');
