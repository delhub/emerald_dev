<?php

namespace App\Http\Controllers\Administrator\Ordertracking;

use setasign\Fpdi\Fpdi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Jobs\PDF\GeneratePdfPoDoEmail;
use App\Models\Purchases\Order;
use App\Models\Purchases\Item;
use App\Models\Globals\Status;
use App\Models\Globals\State;
use App\Models\Courier\Courier;
use App\Models\Dealers\DealerStockLedger;
use App\Models\Purchases\Purchase;
use App\Models\Purchases\RBSPurchase;
use App\Models\Rbs\RbsPostpaid;
use Carbon\Carbon as CarbonCarbon;
use Illuminate\Support\Facades\Auth;



class DetailController extends Controller
{

    //orders (Pick)
    public function orders(Request $request)
    {
        $customerOrder = array();

        //query all search selection
        $allOrders = $this->allOrders($request);

        $statuses = Status::where('id', 'like', '%' . '10' . '%')->whereNotIn('id', ['10,1000'])->get(); // remove status record created
        $states = State::all();
        $couriers = Courier::all();

        //warehouse user
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        if ($userLocationId) {
            $userOutletLocationId = $user->user_warehouse_outlet;
            $allOrders = $allOrders->whereIn('delivery_outlet', $userOutletLocationId);
        }

        $allOrders = $allOrders->get();
        foreach ($allOrders as $key => $orders) {
            if ($orders->order_type == 'rbs' && $orders->order_date != 'Pending') {
                if (CarbonCarbon::createFromFormat('d/m/Y', $orders->order_date)->lt(CarbonCarbon::now())) {
                    $customerOrder[] = $orders;
                }
            } elseif ($orders->order_type == 'rbs' && $orders->order_date == 'Pending') {
                if (strlen($orders->delivery_order) == 12) {
                    $customerOrder[] = $orders;
                }
            } else {
                $customerOrder[] = $orders;
            }
        }


        $customerOrders = $this->paginate($customerOrder, 25);
        $customerOrders->setPath('');
        $customerOrders->appends(request()->query());

        $countTotal = count($customerOrder);

        return view('administrator.ordertracking.orders')
            ->with('request', $request)
            ->with('statuses', $statuses)
            ->with('states', $states)
            ->with('couriers', $couriers)
            ->with('customerOrders', $customerOrders)
            ->with('countTotal', $countTotal);
    }

    public function allOrders($request)
    {
        return Order::with(['purchase', 'items'])
            ->selectRaw('orders.*, orders.order_number')
            ->join('items', 'orders.order_number', '=', 'items.order_number')
            ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            ->whereNotIn('orders.order_status', ['1000']) // remove order status record created
            ->statusFilter($request->status)
            ->statesFilter($request->states)
            ->stockFilter($request->stock_filter)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->findID($request->manyID)
            ->orderBy('orders.created_at', 'ASC')
            ->groupBy('orders.id');
    }

    // pagination array
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    //Orders Detail
    public function ordersDetail(Request $request)
    {
        //query all search selection
        $customerOrders = $this->allOrdersDetail($request);
        $statuses = Status::where('id', 'like', '%' . '10' . '%')->whereNotIn('id', ['1000'])->get(); // remove status record created
        $states = State::all();
        $couriers = Courier::all();

        $ForCountcustomerOrders = $customerOrders->get();

        $customerOrders = $customerOrders->paginate(25);
        $customerOrders->appends(request()->query());

        return view('administrator.ordertracking.detail')->with('customerOrders', $customerOrders)
            ->with('ForCountcustomerOrders', $ForCountcustomerOrders)
            ->with('statuses', $statuses)
            ->with('states', $states)
            ->with('couriers', $couriers)
            ->with('request', $request);
    }

    public function allOrdersDetail($request)
    {
        return Item::with(['order.purchase'])
            ->selectRaw('items.*, items.id')
            ->join('orders', 'orders.order_number', '=', 'items.order_number')
            ->whereNotIn('orders.order_status', ['1000']) // remove order status record created
            ->stockFilter($request->stock_filter)
            ->findID($request->manyID)
            ->orderBy('items.created_at', 'ASC');
    }

    //old detail tracking but using
    public function oldDetail(Request $request)
    {
        $customerOrder = array();

        $allOrders = $this->oldAllDetail($request);

        $statuses = Status::where('id', 'like', '%' . '10' . '%')->whereNotIn('id', ['10,1000'])->get(); // remove status record created
        $states = State::all();
        $couriers = Courier::all();
        $itemQtyTotal = 0;

        foreach ($allOrders as $key => $items) {
            if ($items->order->order_type == 'rbs' && $items->order->order_date != 'Pending') {
                if (CarbonCarbon::createFromFormat('d/m/Y', $items->order->order_date)->lt(CarbonCarbon::now())) {
                    $customerOrder[] = $items;
                }
            } elseif ($items->order->order_type == 'rbs' && $items->order->order_date == 'Pending') {
                if (strlen($items->order->delivery_order) == 12) {
                    $customerOrder[] = $items;
                }
            } else {
                $customerOrder[] = $items;
            }
            $itemQtyTotal += $items->quantity;
        }

        $customerOrders = $this->paginate($customerOrder, 25);
        $customerOrders->setPath('');
        $customerOrders->appends(request()->query());

        $ForCountcustomerOrders = count($customerOrder);

        return view('administrator.ordertracking.olddetail')->with('customerOrders', $customerOrders)
            ->with('ForCountcustomerOrders', $ForCountcustomerOrders)
            ->with('statuses', $statuses)
            ->with('states', $states)
            ->with('couriers', $couriers)
            ->with('request', $request)
            ->with('itemQtyTotal', $itemQtyTotal);
    }

    public function oldAllDetail($request)
    {
        $allOrders = Item::with(['order.purchase'])
            ->selectRaw('items.*, items.id')
            ->join('orders', 'orders.order_number', '=', 'items.order_number')
            ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('no_stockProduct', 0)
            ->whereNotIn('item_order_status', ['1000']) // remove status record created
            ->whereNotIn('orders.order_status', ['1000']) // remove order status record created
            ->statusFilter($request->status, $request->date, $request->period)
            ->statesFilter($request->states)
            ->stockFilter($request->stock_filter)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->findID($request->manyID)
            ->orderBy('items.created_at', 'ASC')
            ->get();

        return $allOrders;
    }

    public function rbsPrepaid(Request $request)
    {
        $rbsPrepaids = $status = array();

        $allRbsPrepaids = Purchase::where('order_type', 'rbs')
            // ->rbsStatusFilter($request->status)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->get();

        if ($allRbsPrepaids) {
            foreach ($allRbsPrepaids as $allRbsPrepaid) {
                foreach ($allRbsPrepaid->orders as $orders) {

                    switch ($request->status) {
                        case 'completed':
                            $collected_order = $allRbsPrepaid->orders->pluck('collected_date');
                            if (!in_array('Pending', $collected_order->toArray())) {
                                $rbsPrepaids[$allRbsPrepaid->id][] = $orders;
                            }
                            break;
                            // case 'cancelled':
                            //     $cancel_order = $allRbsPrepaid->orders->pluck('order_status');
                            //     if (!in_array('Pending',$collected_order->toArray())) {
                            //         $rbsPrepaids[$allRbsPrepaid->id][] = $orders;
                            //     }
                            // break;

                        default:
                            $rbsPrepaids[$allRbsPrepaid->id][] = $orders;
                            break;
                    }
                }
            }
        }
        // dd($rbsPrepaids);

        return view('administrator.ordertracking.rbs-prepaid')
            ->with('request', $request)
            ->with('rbsPrepaids', $rbsPrepaids)
            ->with('allRbsPrepaids', $allRbsPrepaids);
    }
    public function rbsPostpaid(Request $request)
    {
        $rbsPostpaids = RbsPostpaid::with('atrributeProduct')->get();
        return view('administrator.ordertracking.rbs-postpaid')
            ->with('request', $request)
            ->with('rbsPostpaids', $rbsPostpaids);
    }

    // Update estimate delivery date (orders)
    public function updateOrder(Request $request, $order_num)
    {
        $order = new Order();

        $order = Order::findOrFail($order_num);
        $order->delivery_date = $request->input('delivery_date');

        $order->order_status = 1002;

        $order->save();

        if ($order) {
            return back()->with(['successful_message' => 'Estimate delivery date updated successfully']);
        } else {
            return back()->with(['error_message' => 'Failed to update estimate delivery date']);
        }
    }

    // Update estimate ship out date (items)
    public function updateItems(Request $request, $po_number)
    {
        $itemsid = $request->input('lineItem_id');
        $item = Order::where('order_number', $po_number)->where('order_number', $request->input('lineItem_id'))->first();

        if ($request->input('ship_out_date') != null) {

            $checkkeyindate = date('Y-m-d', strtotime($request->input('ship_out_date')));

            //check item's order date
            $checkitemorderdate = Order::where('order_number', $itemsid)->whereDate('created_at', '>', $checkkeyindate)->first();
            if ($checkitemorderdate) return back()->with(['error_message' =>  "Logic Problem : Estimate ship out date is faster than order date!!! - $item->delivery_order"]);

            //check item's actual ship date
            $checkactualshipdate = Order::where('order_number', $itemsid)->whereDate('shipping_date', '<', $checkkeyindate)->first();
            if ($item->shipping_date != 'Pending' && $checkactualshipdate) return back()->with(['error_message' =>  "Logic Problem : Estimate ship out date is faster than actual ship out date!!! - $item->delivery_order"]);


            $item->est_shipping_date  = $request->input('ship_out_date');
        } elseif ($request->input('actual_ship_out_date') != null) {

            $checkkeyindate = date('Y-m-d', strtotime($request->input('actual_ship_out_date')));

            //check item's order date
            $checkitemorderdate = Order::where('order_number', $itemsid)->whereDate('created_at', '>', $checkkeyindate)->first();
            if ($checkitemorderdate) return back()->with(['error_message' =>  "Logic Problem : Actual ship out date is faster than order date!!! - $item->delivery_order"]);

            // //check item's collected date
            $checkcollecteddate = Order::where('order_number', $itemsid)->whereDate('collected_date', '<', $checkkeyindate)->first();
            if ($item->collected_date != 'Pending' && $checkcollecteddate) return back()->with(['error_message' =>  "Logic Problem : Actual ship out date is slower than collected date !!! - $item->delivery_order"]);


            $item->shipping_date = $request->input('actual_ship_out_date');
        } elseif ($request->input('collected_date') != null) {

            $checkkeyindate = date('Y-m-d', strtotime($request->input('collected_date')));

            //check item's order date
            $checkitemorderdate = Order::where('order_number', $itemsid)->whereDate('created_at', '>', $checkkeyindate)->first();
            if ($checkitemorderdate) return back()->with(['error_message' =>  "Logic Problem : Item Collected date is faster than order date!!! - $item->delivery_order"]);

            //check item's actual ship date
            $checkactualshipdate = Order::where('order_number', $itemsid)->whereDate('shipping_date', '>', $checkkeyindate)->first();
            if ($item->collected_date != 'Pending' && $checkactualshipdate) return back()->with(['error_message' =>  "Logic Problem : Collected date is slower then actual ship out date !!! - $item->delivery_order"]);


            $item->collected_date = $request->input('collected_date');
        }

        if ($request->input('actual_ship_out_date')) {
            if ($item->collected_date == 'Pending') {
                $item->order_status = self::updateStatus('submit', 'actual_ship_out_date', $item->order_status, false, false);
            }
        } elseif ($request->input('collected_date')) {
            $item->order_status = self::updateStatus('submit', 'collected_date', $item->order_status, false, false);
        }

        $item->save();

        if (($item) && ($request->input('ship_out_date'))) {
            return back()->with(['successful_message' => 'Estimate Ship out date updated successfully ']);
        } elseif (($item) && ($request->input('actual_ship_out_date'))) {
            return back()->with(['successful_message' => 'Actual ship out date updated successfully']);
        } elseif (($item) && ($request->input('collected_date'))) {
            return back()->with(['successful_message' => 'Collected date updated successfully']);
        } else {
            return back()->with(['error_message' => 'Failed to update']);
        }
    }

    // // Update reset estimate ship out date (items)
    public function updateresetItems(Request $request, $po_number)
    {
        $item = Order::where('order_number', $po_number)->where('order_number', $request->input('lineItem_id'))->first();

        if ($request->input('reset_ship_out_date') == 1) {
            if ($item->items[0]->product->estimate_ship_out_date != 0) {
                $orderdate = $item->created_at;
                $estimate_dates =  $item->items[0]->product->estimate_ship_out_date;

                $item->est_shipping_date = date('Y-m-d', strtotime($orderdate . ' + ' . $estimate_dates . ' days'));
            } else {
                $item->est_shipping_date = 'Pending';
            }
        } elseif ($request->input('reset_actual_ship_out_date') == 1) {

            if ($item->collected_date != 'Pending') return back()->with(['error_message' =>  "Item already collected!!! - $item->delivery_order"]);

            $item->shipping_date = 'Pending';
            $previous_status = $item->order_status;
            $item->order_status = self::updateStatus('reset', 'actual_ship_out_date', $item->order_status, false, $item);
            // $item->order_status = '1001';
        } elseif ($request->input('reset_collected_date') == 1) {
            $item->collected_date = 'Pending';
            $item->order_status = self::updateStatus('reset', 'collected_date', $item->order_status, $item->shipping_date, $item);
        }

        $item->save();

        if (($item) && ($request->input('reset_ship_out_date'))) {
            return back()->with(['successful_message' => 'Reset Estimate Ship Out Date Successfully']);
        } elseif (($item) && ($request->input('reset_collected_date'))) {
            return back()->with(['successful_message' => 'Reset Collected Date Successfully']);
        } elseif (($item) && ($request->input('reset_actual_ship_out_date'))) {
            return back()->with(['successful_message' => 'Reset Actual Ship Out Date Successfully']);
        } else {
            return back()->with(['error_message' => 'Failed to reset']);
        }
    }

    public function updateTracking(Request $request, $id)
    {
        $item = Order::where('id', $id)->first();
        $item->courier_name = ($request->input('courier_name') != null) ? $request->input('courier_name') : $item->courier_name;
        $item->tracking_number = ($request->input('tracking_number') != null) ? $request->input('tracking_number') : $item->tracking_number;
        $item->save();

        $inventoryData['model_type'] = 'App\Models\Purchases\Order';
        $inventoryData['model_id'] = $item->id;
        $inventoryData['changes'] = $item;

        InventoryController::findData($inventoryData);

        return back()->with(['successful_message' => 'Update Courier name & Tracking number Successfully Successfully']);
    }

    public function updateAdminRemark(Request $request, $id)
    {
        $item = Order::where('order_number', $id)->first();
        $item->admin_remarks = ($request->input('admin_remarks') != null) ? $request->input('admin_remarks') : $item->admin_remarks;
        $item->save();

        return back()->with(['successful_message' => 'Update Admin Remark Successfully']);
    }

    public function updatePanelRemark(Request $request, $id)
    {
        $item = Item::where('id', $id)->first();
        $item->panel_remarks = ($request->input('panel_remarks') != null) ? $request->input('panel_remarks') : $item->panel_remarks;
        $item->save();

        return back()->with(['successful_message' => 'Update Panel Remark Successfully']);
    }

    // Update bulk
    public function bulkUpdate(Request $request)
    {
        $action =  $request->input('bulkaction');
        $id = $request->input('action_ids');
        $arrayid = explode(",", $id);

        switch ($action) {
            case 'mark_order_date':

                $orderdate = $request->input('action_date');

                if (!empty($orderdate)) {
                    if (isset($id) && !empty($id)) {
                        $not_updated = array();

                        foreach ($arrayid as $key => $id) {
                            $shipdatepending = DB::table('orders')
                                ->where('orders.order_number', $id)
                                ->where('orders.est_shipping_date', 'Pending')
                                ->first();

                            if ($shipdatepending) {
                                $this->markOrderDate($id, $orderdate);
                            } else {
                                $gotshipdate = DB::table('orders')
                                    ->where('orders.order_number', $id)
                                    ->first();

                                $not_updated[] = $gotshipdate->order_number;
                            }
                        }

                        if (count($not_updated) > 0) {
                            $error_message = "Partially Marked. Due to Estimate Ship out Date already exist, these order item are not marked " . implode(', ', $not_updated);
                        } else {
                            $success_message = "Mark Estimate Ship Out Date Successfully.";
                            $result = 'success';
                        }
                    } else {
                        $error_message = "Please select item to update";
                    }
                } else {
                    $error_message = "Please select date to update";
                }

                break;

            case 'mark_actual_ship_date';

                $actualshipdate = $request->input('action_date');

                if (!empty($actualshipdate)) {
                    if (isset($id) && !empty($id)) {
                        $not_updated = array();

                        foreach ($arrayid as $key => $id) {
                            $actualshipdatepending = DB::table('orders')
                                ->where('orders.order_number', $id)
                                ->where('orders.shipping_date', '=', 'Pending')
                                ->first();

                            if ($actualshipdatepending) {
                                $this->markActualShipDate($id, $actualshipdate);
                            } else {
                                $gotshipdate = DB::table('orders')
                                    ->where('orders.order_number', $id)
                                    ->first();

                                $not_updated[] = $gotshipdate->order_number;
                            }
                        }

                        if (count($not_updated) > 0) {
                            $error_message = "Partially Marked. Due to Actual Ship Out Date already exist, these order item are not marked " . implode(', ', $not_updated);
                        } else {
                            $success_message = "Mark Actual Ship Out Date Successfully.";
                            $result = 'success';
                        }
                    } else {
                        $error_message = "Please select item to update";
                    }
                } else {
                    $error_message = "Please select date to update";
                }

                break;

            case 'mark_item_collected_date';

                $collecteddate = $request->input('action_date');

                if (!empty($collecteddate)) {
                    if (isset($id) && !empty($id)) {
                        $not_updated = array();

                        foreach ($arrayid as $key => $id) {
                            $collecteddatepending = DB::table('orders')
                                ->where('orders.order_number', $id)
                                ->where('orders.collected_date', '=', 'Pending')
                                ->first();

                            if ($collecteddatepending) {
                                $this->markCollectedDate($id, $collecteddate);
                            } else {
                                $gotshipdate = DB::table('orders')
                                    ->where('orders.order_number', $id)
                                    ->first();

                                $not_updated[] = $gotshipdate->order_number;
                            }
                        }

                        if (count($not_updated) > 0) {
                            $error_message = "Partially Marked. Due to Collected Date already exist, these order item are not marked " . implode(', ', $not_updated);
                        } else {
                            $success_message = "Mark Item Collected Date Successfully.";
                            $result = 'success';
                        }
                    } else {
                        $error_message = "Please select item to update";
                    }
                } else {
                    $error_message = "Please select date to update";
                }

                break;

            case 'combine_do':
                if (isset($id) && !empty($id)) {

                    $items = Order::with(['items'])
                        ->whereIn('orders.order_number', $arrayid)
                        ->whereNotNull('orders.delivery_order')
                        ->WhereNotNull('tracking_number')
                        ->get();

                    //check DO number in database
                    if (!$items->isEmpty()) {
                        $files = array();
                        $domain = $request->server->get('DOCUMENT_ROOT');

                        $not_combine = array();

                        foreach ($items as $item) {

                            $invoice_number = $item->purchase->getFormattedNumber('purchase_number');
                            $DO_number = $item->delivery_order;
                            //check server folder/file
                            if (file_exists($domain . '/storage/documents/invoice/' . $invoice_number  .
                                (($item->purchase->invoice_version != 0) ? '/v' . $item->purchase->invoice_version . '/' : '/') .  'delivery-orders/' . $DO_number . '.pdf')) {

                                //file path
                                $files[] = $domain . '/storage/documents/invoice/' . $invoice_number . (($item->purchase->invoice_version != 0) ? '/v' . $item->purchase->invoice_version . '/' : '/') . 'delivery-orders/' . $DO_number . '.pdf';
                                //file name
                                $fname[] = $DO_number;

                                //update order's status and shipping date
                                //$checkout = PoslajuAPI::checkout($item->shipment_key);

                                $dt = Carbon::now();
                                $time = $dt->toTimeString(); //14:15:16
                                $day = $dt->addDays(1);

                                if ($time > '11:45:00') {
                                    $item->est_shipping_date = $day->isoFormat('YYYY-MM-DD');
                                    $item->shipping_date = $day->isoFormat('YYYY-MM-DD');
                                } else {
                                    $item->est_shipping_date = $dt->isoFormat('YYYY-MM-DD');
                                    $item->shipping_date = $dt->isoFormat('YYYY-MM-DD');
                                }

                                $item->order_status = 1002;

                                $item->save();
                            } else {

                                $not_combine[] = $item->delivery_order;
                            }
                        }

                        if (count($not_combine) > 0) {
                            $error_message = "These DO are not available - " . implode(', ', $not_combine);
                            return back()->with(['error_message' => $error_message]);
                        }

                        // initiate FPDI
                        $pdf = new FPDI('l');
                        $pageCount = 0;

                        // iterate over array of files and merge
                        foreach ($files as $file) {
                            //if no file, dont combine
                            if (!empty($file)) {
                                $pageCount = $pdf->setSourceFile($file);
                                for ($i = 0; $i < $pageCount; $i++) {
                                    $tpl = $pdf->importPage($i + 1, '/MediaBox');
                                    $pdf->addPage();
                                    $pdf->useTemplate($tpl);
                                }
                            }
                        }

                        if (!empty($files)) {
                            $filename = 'combined' . implode(",", $fname) . '.pdf';

                            $pdf->Output($filename, 'D');
                        }
                    } else {
                        $error_message = "Please select DO Number(that exist Tracking Number) to combine";
                    }
                } else {
                    $error_message = "Please select DO to combine";
                }
                break;

            case 'combine_po':

                if (isset($id) && !empty($id)) {

                    $items = Item::with(['order'])
                        ->whereIn('items.id', $arrayid)
                        ->whereNotNull('items.order_number')
                        ->get();

                    //check DO number in database
                    if (!$items->isEmpty()) {

                        $files = array();
                        $domain = $request->server->get('DOCUMENT_ROOT');

                        $not_combine = array();

                        foreach ($items as $item) {
                            $invoice_number = $item->order->purchase->getFormattedNumber('purchase_number');
                            $PO_number = $item->order_number;

                            //check server folder/file
                            if (file_exists($domain . '/storage/documents/invoice/' . $invoice_number . (($item->order->purchase->invoice_version != 0) ? '/v' . $item->order->purchase->invoice_version . '/' : '/') .  'purchase-orders/' . $PO_number . '.pdf')) {

                                //file path
                                $files[] = $domain . '/storage/documents/invoice/' . $invoice_number . (($item->order->purchase->invoice_version != 0) ? '/v' . $item->order->purchase->invoice_version . '/' : '/') . 'purchase-orders/' . $PO_number . '.pdf';
                                //file name
                                $fname[] = $PO_number;
                            } else {

                                $not_combine[] = $item->order_number;
                            }
                        }

                        if (count($not_combine) > 0) {
                            $error_message = "These DO are not available - " . implode(', ', $not_combine);
                            return back()->with(['error_message' => $error_message]);
                        }

                        // initiate FPDI
                        $pdf = new FPDI();
                        $pageCount = 0;

                        // iterate over array of files and merge
                        foreach ($files as $file) {
                            //if no file, dont combine
                            if (!empty($file)) {
                                $pageCount = $pdf->setSourceFile($file);
                                for ($i = 0; $i < $pageCount; $i++) {
                                    $tpl = $pdf->importPage($i + 1, '/MediaBox');
                                    $pdf->addPage();
                                    $pdf->useTemplate($tpl);
                                }
                            }
                        }

                        if (!empty($files)) {
                            $filename = 'combined' . implode(",", $fname) . '.pdf';

                            $pdf->Output($filename, 'D');
                        }
                    } else {
                        $error_message = "Please select DO Number to combine";
                    }
                } else {
                    $error_message = "Please select DO to combine";
                }
                break;

            case 'splitPOnDO';

                if (isset($id) && !empty($id)) {

                    $getPoNumber = Item::whereIn('items.id', $arrayid)->groupBy('items.order_number')->pluck('order_number');

                    $checkSelectedPO = COUNT($getPoNumber);
                    if ($checkSelectedPO > 1) return back()->with(['error_message' =>  "Please select either one to split - $getPoNumber)"]);

                    $items = Item::where('items.order_number', $getPoNumber)->get();

                    $checkSelectedItems = COUNT($items);
                    if ($checkSelectedItems == 1) return back()->with(['error_message' =>  "Please select more than 2 items to split - $getPoNumber)"]);


                    $orderA = Order::where('order_number', $getPoNumber)->first();

                    $addA = 'A';
                    $addB = 'B';

                    foreach ($items as $item) {
                        if (in_array($item->id, $arrayid)) {
                            $updateSelecteditem = Item::where('id', $item->id)->first();
                            $updateSelecteditem->order_number = ($item->order_number . $addA);
                            $updateSelecteditem->delivery_order = ($item->delivery_order . $addA);
                            // $updateSelecteditem->created_at = now();
                            // $updateSelecteditem->updated_at = now();
                            $updateSelecteditem->save();
                        } else {
                            $updateNotSelecteditem = Item::where('id', $item->id)->first();
                            $updateNotSelecteditem->order_number = ($item->order_number . $addB);
                            $updateNotSelecteditem->delivery_order = ($item->delivery_order . $addB);
                            // $updateNotSelecteditem->created_at = now();
                            // $updateNotSelecteditem->updated_at = now();
                            $updateNotSelecteditem->save();
                        }
                    }

                    //Insert new data order - $orderB
                    $orderB = new Order;
                    $orderB->order_number = ($orderA->order_number . $addB); //+b
                    $orderB->delivery_order = ($orderA->delivery_order . $addB); //+b
                    $orderB->purchase_id = $orderA->purchase_id;
                    $orderB->panel_id = $orderA->panel_id;
                    $orderB->order_status = $orderA->order_status;
                    $orderB->order_amount = $orderA->order_amount;
                    $orderB->delivery_date = $orderA->delivery_date;
                    $orderB->received_date = $orderA->received_date;
                    $orderB->claim_status = $orderA->claim_status;
                    $orderB->country_id = $orderA->country_id;

                    $orderB->save();

                    //Update existing order - $orderA
                    $orderA->order_number = ($orderA->order_number . $addA); //+b
                    $orderA->delivery_order = ($orderA->delivery_order . $addA); //+b

                    $orderA->save();

                    GeneratePdfPoDoEmail::dispatch($orderB);
                    GeneratePdfPoDoEmail::dispatch($orderA);

                    $success_message = "Split PO And DO Successful - ($orderA->order_number , $orderA->delivery_order) and ($orderB->order_number ,$orderB->delivery_order)";
                    $result = 'success';
                } else {
                    $error_message = "Please select item to split";
                }
                break;

            default:
                $error_message = "Please choose at least one action";
        }

        if (!empty($result) && $result == "success") {
            return back()->with(['successful_message' => $success_message]);
        } else {
            return back()->with(['error_message' =>  $error_message]);
        }
    }

    public function markOrderDate($id, $orderdate)
    {
        $item = Order::where('order_number', $id)->first();

        $item->est_shipping_date = $orderdate;

        $item->save();

        return 'success';
    }

    public function markActualShipDate($id, $actualshipdate)
    {
        $item = Order::where('order_number', $id)->first();

        $item->shipping_date = $actualshipdate;
        $item->order_status = self::updateStatus('submit', 'actual_ship_out_date', $item->order_status, false, false);

        $item->save();

        return 'success';
    }

    public function markCollectedDate($id, $collecteddate)
    {
        $item = Order::where('order_number', $id)->first();

        $item->collected_date = $collecteddate;
        $item->order_status = self::updateStatus('submit', 'collected_date', $item->order_status, false, false);

        $item->save();

        return 'success';
    }

    public static function updateStatus($buttonName, $column, $previousStatus, $shipOutDate, $item)
    {
        // 1001 = order place
        // 1002 = order ship
        // 1003 = Order Delivered
        // 1004 = Order Cancelled
        // 1005 = Order Pending Payment
        // 1008 = ACD Delivered
        // 1009 = ACD Collected
        // 1010 = ACD Ship Out

        // button submit
        if ($buttonName == 'submit') {

            if ($column == 'actual_ship_out_date') {

                if ($previousStatus == '1008') {
                    $newStatus = '1010';
                } else if ($previousStatus == '1011') {
                    $newStatus = '1010';
                } else {
                    $newStatus = '1002';
                }
            } else if ($column == 'collected_date') {
                if ($previousStatus >= '1008') {
                    $newStatus = '1009';
                } else {
                    $newStatus = '1003';
                }
            }

            // button reset
        } else {

            if ($column == 'actual_ship_out_date') {

                if ($previousStatus >= '1008') {
                    $acd = DealerStockLedger::where('item_id', $item->id)->first()->type;
                    // dd($acd);
                    if ($acd == '5003') {
                        $newStatus = '1011';
                    } else {
                        $newStatus = '1008';
                    }
                } else {
                    $newStatus = '1001';
                }
            } else if ($column == 'collected_date') {

                if ($shipOutDate == 'Pending') {

                    if (($previousStatus >= '1008')) {
                        $newStatus = '1008';
                    } else {
                        $newStatus = '1001';
                    }
                } else {
                    if (($previousStatus >= '1008')) {
                        $newStatus = '1010';
                    } else {
                        $newStatus = '1002';
                    }
                }
            }
        }

        return $newStatus;
    }

    //Excel CSV Download
    public function exportCsv(Request $request)
    {
        $statuses = Status::where('id', 'like', '%' . '100' . '%')->whereNotIn('id', ['1000'])->get(); // remove status record created
        $customerOrders = $this->oldAllDetail($request);

        $fileName = 'Order Tracking Details.csv';

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Invoice No.', 'Order No.', 'PO No.', 'DO No.', 'Product Code', 'Product Name', 'Product Size', 'Self Collection', 'Panel', 'Order Date', 'Quantity', 'Estimate Ship Out Date', 'Pending Days', 'DO Status', 'DO Scan Date', 'Item Status', 'States', 'Shipping Address', 'Full Name', 'Contact No.', 'Courier', 'Tracking No.', 'Estimate Ship Out Date(new)', 'Actual Ship Out Date', 'Item Collected Date', 'Remarks', 'Group');
        $columns = array(
            'Invoice No.', 'Order No.', 'PO No.', 'DO No.', 'Product Code', 'Product Name', 'Product Attribute', 'Self Collection', 'Pre Order Date', 'Panel', 'Order Date', 'Quantity', 'Estimate Ship Out Date', 'Pending Days', 'DO Status', 'DO Scan Date', 'Item Status',
            'Shipping Address', 'Post Code', 'City', 'States', 'Full Name', 'Contact No.', 'Courier', 'Tracking No.', 'Estimate Ship Out Date(new)', 'Actual Ship Out Date', 'Item Collected Date', 'Remarks', 'Group'
        );


        $callback = function () use ($customerOrders, $statuses, $columns) {

            $file = fopen('php://output', 'z');
            fputcsv($file, $columns);

            foreach ($customerOrders as $customerOrder) {

                //Product Attribute
                $color = (array_key_exists('product_color_name', $customerOrder->product_information) ? 'Color:' . $customerOrder->product_information['product_color_name'] : '');
                $size = (array_key_exists('product_size', $customerOrder->product_information) ? 'Size:' . $customerOrder->product_information['product_size'] : '');
                $curtainModel = (array_key_exists('product_curtain_size', $customerOrder->product_information) ? 'Curtain Model:' . $customerOrder->product_information['product_curtain_size'] : '');
                $colorTemperature = (array_key_exists('product_temperature', $customerOrder->product_information) ? 'Color Temperature:' . $customerOrder->product_information['product_temperature'] : '');
                $miscellaneous = (array_key_exists('product_miscellaneous', $customerOrder->product_information) ? 'Miscellaneous:' . $customerOrder->product_information['product_miscellaneous'] : '');
                $invoiceNumber = (array_key_exists('invoice_number', $customerOrder->product_information) ? 'Order Number:' . $customerOrder->product_information['invoice_number'] : '');

                //Self Collection
                if ($customerOrder->order->order_status = 1010 && isset($customerOrder->order->outlet)) {
                    $selfCollect = 'Yes - ' . $customerOrder->order->outlet->outlet_name;
                } else {
                    $selfCollect = 'No';
                }

                //Pre Order Date
                if (array_key_exists('product_preorder_date', $customerOrder->product_information)) {
                    $preOrder = $customerOrder->product_information['product_preorder_date'];
                } else {
                    $preOrder = 'No';
                }

                //DO Status
                $statuse = $statuses->where('id', $customerOrder->order_status)->first();

                //City
                if (isset($customerOrder->order->purchase->ship_address_1)) {
                    if ($customerOrder->order->purchase->cityKey != NULL && $customerOrder->order->purchase->ship_city_key != 0) {
                        $city = $customerOrder->order->purchase->cityKey->city_name;
                    } else {
                        $city = $customerOrder->order->purchase->ship_city;
                    }
                } else {
                    $city = '';
                }


                $row['Invoice No.'] = $customerOrder->order->purchase->inv_number;
                $row['Order No.'] = $customerOrder->order->purchase->getFormattedNumber();
                $row['PO No.']    = $customerOrder->order_number;
                $row['DO No.']    = $customerOrder->delivery_order;
                $row['Product Code']  = ($customerOrder->product_code) ? $customerOrder->product_code : '-';
                $row['Product Name']  = $customerOrder->product  ? ($customerOrder->product->parentProduct ? $customerOrder->product->parentProduct->name : '') : '';
                $row['Product Attribute']  = $color . $size . $curtainModel . $colorTemperature . $miscellaneous . $invoiceNumber;
                $row['Self Collection']  = $selfCollect;
                $row['Pre Order Date']  = $preOrder;
                $row['Panel']  = $customerOrder->order->panel->company_name;
                $row['Order Date']  = $customerOrder->order->purchase->created_at->format('d/m/Y');
                $row['Quantity']  = $customerOrder->quantity;
                $row['Estimate Ship Out Date']  = $customerOrder->order->delivery_date;
                $row['Pending Days']  = $customerOrder->getPendingAttribute();
                $row['DO Status']  = $statuses->where('id', $customerOrder->order->order_status)->first() ? $statuses->where('id', $customerOrder->order->order_status)->first()->name : '';
                $row['DO Scan Date']  = ($customerOrder->order->received_date) ? $customerOrder->order->received_date : 'Not Scan';
                $row['Item Status']  =  $statuse ? str_replace("Order", "Item", $statuse->name) : '';
                $row['Shipping Address']    = $customerOrder->order->purchase->shippingAddress();
                $row['Post Code']  = $customerOrder->order->purchase->ship_postcode;
                $row['City']  = $city;
                $row['States']  = $customerOrder->order->purchase->state ?  $customerOrder->order->purchase->state->name : '';
                $row['Full Name']    = $customerOrder->order->purchase->ship_full_name;
                $row['Contact No.']    = $customerOrder->order->purchase->ship_contact_num;
                $row['Courier']  = ($customerOrder->courier_name ? $customerOrder->courier_name : '-');
                $row['Tracking No.']  = ($customerOrder->tracking_number ? $customerOrder->tracking_number : '-');
                $row['Estimate Ship Out Date(new)']  = $customerOrder->ship_date;
                $row['Actual Ship Out Date']  = $customerOrder->shipping_date;
                $row['Item Collected Date']  = $customerOrder->collected_date;
                $row['Remarks']  = $customerOrder->admin_remarks ? $customerOrder->admin_remarks : '-';
                $users = $customerOrder->order->purchase->user;
                if ($users) {
                    $agentGroup = $users->userInfo->agentGroup;
                    $groupName = $agentGroup ? $agentGroup->group_name : '-';
                }
                $row['Group']  = $agentGroup ? $groupName : '-';
                // $row['Group']  = $customerOrder->order->purchase->user->userInfo->agentGroup->group_name;

                fputcsv($file, array(
                    $row['Invoice No.'], $row['Order No.'], $row['PO No.'],  $row['DO No.'], $row['Product Code'], $row['Product Name'], $row['Product Attribute'], $row['Self Collection'], $row['Pre Order Date'], $row['Panel'], $row['Order Date'], $row['Quantity'], $row['Estimate Ship Out Date'], $row['Pending Days'], $row['DO Status'], $row['DO Scan Date'], $row['Item Status'],
                    $row['Shipping Address'], $row['Post Code'], $row['City'], $row['States'], $row['Full Name'], $row['Contact No.'], $row['Courier'], $row['Tracking No.'], $row['Estimate Ship Out Date(new)'], $row['Actual Ship Out Date'], $row['Item Collected Date'], $row['Remarks'], $row['Group']
                ));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
