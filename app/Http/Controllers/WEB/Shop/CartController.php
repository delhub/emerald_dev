<?php

namespace App\Http\Controllers\WEB\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\Customers\Cart;
use App\Models\Users\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Products\Product as PanelProduct;
use App\Models\Products\ProductDelivery;
use App\Models\Products\ProductAttribute;
use App\Models\Products\ProductBundle;
use App\Models\Globals\State;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Models\Globals\Image;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\ShippingInstallations\Rates;
use App\Models\ShippingInstallations\Locations;
use App\Models\Globals\City;
use Illuminate\Support\Facades\DB;
use App\Models\Discount\Code;
use App\Models\Tax\Tax;
use App\Models\Globals\Countries;
use App\Traits\Voucher;
use Facades\App\Repository\CoreCache;
use stdClass;

use function GuzzleHttp\json_decode;

class CartController extends Controller
{
    use Voucher;
    const LESS_NO_PURCHASE = 1;
    const LESS_CAN_PURCHASE = 2;
    const BETWEEN_PRICE = 3;
    const MAX_FREE_DELIVERY = 4;
    var $itemCount = array();
    /**
     * Get user's cart.
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);

        $userInfo = $user->userInfo;

        self::updateCartItems($user);

        $userShipping = $userInfo->shippingAddress;

        // Coookie
        $shipCookie = json_decode(Cookie::get('addressCookie'));
        $country_code = country()->country_id;

        if (isset($shipCookie)) {
            $stateDelivery = $shipCookie->state_id;
        }

        $selectedCartItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->whereNotIn('disabled', [5])
            ->where('selected', 1)
            ->where('country_id', $country_code)
            ->get();

        // $can_generate_discount = collect(json_decode($selectedCartItems))->whereIn('product_id', [161,166,191,63,130])->where('auto_apply',0);

        // dd($can_generate_discount);

        $autoApply = $this->calculateAutoApply($selectedCartItems);
        //$autoGenerate = $this->calculateAutoGenerate($selectedCartItems);

        if (isset($_REQUEST['autoApply']) && $_REQUEST['autoApply'] == 1) {
            return response()->json($autoApply);
            // dd($selectedCartItems);
        }
        if (isset($_REQUEST['autoGen']) && $_REQUEST['autoGen'] == 1) {
            return response()->json($autoGenerate);
        }

        //get tax and country currency & rate
        $tax = Tax::where('country_id', $country_code)->first();
        $country = Countries::where('country_id', $country_code)->first();
        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        $currency = $country->country_currency;
        $tax_rate = $tax->tax_percentage;
        $tax_name = $tax->tax_name . " $tax_rate%";

        foreach ($selectedCartItems as $selectedCartItem) {

            $product = $selectedCartItem->product;

            $panel = $product->panel;

            $productCategories = $product->parentProduct->categories;

            $productCategoriesArray = [];

            foreach ($productCategories as $key => $productCategory) {
                $productCategoriesArray[$key] = $productCategory->id;
            }

            $freeDeliveryMinPrices = NULL;
            if ($country_code == 'MY') {
                $freeDeliveryMinPrices = $product
                    ->panel
                    ->categoriesWithMinPrice
                    ->whereIn('category_id', $productCategoriesArray)
                    ->all();
            }

            $totalSubtotalPrice = 0;

            $activeCartItems = Cart::where('user_id', $user->id)
                ->where('status', 2001)
                ->whereNotIn('disabled', [5])
                ->where('selected', 1)
                // ->where('bundle_id', 0)
                ->where('country_id', $country_code)
                ->whereHas('product.panel', function ($q) use ($panel) {
                    $q->where('account_id', $panel->account_id);
                })
                ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                    $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                        $q->whereIn('categories.id', $productCategoriesArray);
                    });
                })
                ->get();

            foreach ($activeCartItems as $activeCartItem) {
                if ($activeCartItem->product->parentProduct->product_status != 2) {
                    $totalSubtotalPrice = $totalSubtotalPrice + $activeCartItem->subtotal_price;
                }
            }

            $isDeliveryFree = 0;
            $fixDeliveryFee = 0;

            // dd($totalSubtotalPrice);

            if ($freeDeliveryMinPrices) {

                foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {
                    // More than min, less than max, no purchase
                    if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                        $group = $freeDeliveryMinPrice->group_or_individual;
                        $isDeliveryFree = self::LESS_NO_PURCHASE;
                        $freeStates = $freeDeliveryMinPrice->free_state;
                        break;
                    }
                    // Less than min min equal to max, can purchase
                    if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                        $group = $freeDeliveryMinPrice->group_or_individual;
                        $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                        $isDeliveryFree = self::LESS_CAN_PURCHASE;
                        $freeStates = $freeDeliveryMinPrice->free_state;
                        break;
                    }
                    // More than min, less than max, can purchase
                    if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                        $group = $freeDeliveryMinPrice->group_or_individual;
                        $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                        $isDeliveryFree = self::BETWEEN_PRICE;
                        $freeStates = $freeDeliveryMinPrice->free_state;
                        break;
                    }
                    // More than max, min equal to max, no purchase
                    if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                        $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                        $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                        $group = $freeDeliveryMinPrice->group_or_individual;
                        $isDeliveryFree = self::MAX_FREE_DELIVERY;
                        $freeStates = $freeDeliveryMinPrice->free_state;
                        break;
                    }
                }
            }

            $item = 1;
            foreach ($activeCartItems as $activeCartItem) {

                // if ($freeStates) {
                //     $deliveryFeeForProduct = in_array($stateDelivery,json_decode($freeStates));
                // } else {
                $deliveryFeeForProduct = $activeCartItem->product->deliveries->where('state_id', $stateDelivery)->first();
                // }


                if ($deliveryFeeForProduct) {

                    switch ($isDeliveryFree) {
                        case self::LESS_NO_PURCHASE:
                            $fee = 0;
                            if ($activeCartItem->product->parentProduct->product_status != 2) {
                                $disabled = 1;
                            } else {
                                $disabled = 0;
                            }
                            break;
                        case self::LESS_CAN_PURCHASE:
                            if ($group == 'g') {
                                $fee = ($item == 1) ? $fixDeliveryFee : 0;
                            } else {
                                $fee = $deliveryFeeForProduct->delivery_fee * $activeCartItem->quantity;
                            }
                            $disabled = 0;
                            break;
                        case self::BETWEEN_PRICE:
                            if ($group == 'g') {
                                $fee = ($item == 1) ? $fixDeliveryFee : 0;
                            } else {
                                $fee = $deliveryFeeForProduct->delivery_fee * $activeCartItem->quantity;
                            }
                            $disabled = 0;
                            break;
                        case self::MAX_FREE_DELIVERY:

                            if ($group == 'g') {
                                if (in_array($stateDelivery, json_decode($freeStates))) {
                                    $fee =  0;
                                    $disabled = 0;
                                } else {
                                    $fee = ($item == 1) ? $fixDeliveryFee : 0;
                                    $disabled = 3;
                                }
                            } else {
                                $fee = 0;
                                $disabled = 0;
                            }
                            break;
                        default:
                            $fee =  $deliveryFeeForProduct->delivery_fee * $activeCartItem->quantity;
                            $disabled = 0;
                    }


                    if ($activeCartItem->bundle_id == 0) {

                        // $activeCartItem->delivery_fee = $fee;
                        $this_selfcollect = (isset($activeCartItem->product_information['product_order_selfcollect']) && $activeCartItem->product_information['product_order_selfcollect']);
                        $activeCartItem->delivery_fee = ($this_selfcollect) ? 0 : $fee;

                        $activeCartItem->disabled =  $disabled;

                        if ($activeCartItem->disabled == 0) {
                            $activeCartItem =  $this->checkStateAvailability($activeCartItem, $stateDelivery);
                        }
                    } else {
                        $activeCartItem->delivery_fee = 0;
                        // $activeCartItem->disabled =  $disabled;
                    }

                    $item++;

                    $activeCartItem->save();
                } else {

                    $fee = 0;

                    // if ($freeStates) {
                    //     $disabled = 3;
                    // } else {
                    $disabled = 2;
                    // }
                    $activeCartItem->disabled =  $disabled;

                    $activeCartItem->save();
                }
            }
        }



        $cart = $user->carts->where('status', 2001)->where('country_id', country()->country_id);
        $cartItems = array();

        $cartTotals = array();

        // if ($carts->product->display_panel_name != 'default') {
        //     $cartPanelId = $carts->product->display_panel_name;
        // } else {
        //     $cartPanelId = $carts->product->panel->company_name;
        // }
        // $panelDetails = $carts->product->panel;

        $zoneId = self::getZone($user);
        // //see if no zone id, what to do
        // if ($zoneId == 0){
        //     $message =
        // };
        $firstChecks = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('country_id', country()->country_id)
            ->whereNotIn('disabled', [5])
            ->get();
        self::shippingCalculate($zoneId, $firstChecks);

        $carts = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->where('selected', 1)
            ->where('country_id', country()->country_id)
            ->whereIn('disabled', [0, 3])
            ->get();

        // dd($carts);
        // $cartTotals = $this->shippingCalculate($zoneId, $cart)['installation'];
        $cartTotals = $this->shippingCalculate($zoneId, $carts);

        $installationCookie = json_encode(array('cartTotals' => $cartTotals));
        $cartItems = $user->carts->where('status', '2001')->where('country_id', country()->country_id);

        $cartData = json_decode($selectedCartItems, true);

        $data = [];
        foreach ($cartData as $val) {
            $dat = [];
            $dat['eligible_discount'] = $val['product']['parent_product']['eligible_discount'];
            $dat['generate_discount'] = $val['product']['parent_product']['generate_discount'];
            $dat['subtotal_price'] = $val['subtotal_price'];
            $dat['cart_id'] = $val['id'];

            $data[] = $dat;
        }

        // $data['cart_total'] = $cartTotals['total_new'];

        Session::put('bjs_cart_discount', $data);

        $shippingCategory = Ship_category::all();

        $response = new Response(view('shop.cart.partials.shopping-cart-item', compact(['cartTotals', 'cartItems', 'zoneId'])));
        //$response->withCookie(cookie('bjs_cartTotal', json_encode(array('cartTotals' => $cartTotals)), 10));
        Session::put('bjs_cartTotal', json_encode(array('cartTotals' => $cartTotals)));

        return $response;

        // return view('shop.cart.partials.shopping-cart-item')
        //     ->with('cartItems', $cart)
        //     ->with('images', $images)
        //     ->with('cartTotals', $cartTotals)
        //     ->with('shippingCategory', $shippingCategory);
    }
    public static function shippingCalculate($zoneId, $carts)
    {
        $cartTotals = array(
            'subtotal' => 0,
            'fee' => array(
                'shipping' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
                'installation' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
                'rebate' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                ),
                'tax' => array(
                    'amount' => 0,
                    'shipping_information' => array(),
                )
            ),
            'total' => 0,

            'tax_rate' => 0,
            'tax_name' => 0,

            'shipping_new' => 0,
            'total_old' => 0,
        );
        if (empty($zoneId)) {
            $zoneId = 0;
        }

        $country_code = country()->country_id;

        $tax = Tax::where('country_id', $country_code)->first();
        $country = Countries::where('country_id', $country_code)->first();

        $conversion_rate_markup = Countries::currentConvertionRateMarkup();

        $currency = $country->country_currency;
        $tax_rate = $tax->tax_percentage;
        $tax_name = $tax->tax_name . " $tax_rate%";

        $moreThanHundred = FALSE;

        //cart separate into category
        $shippingByCategories = $installationByCategories = $disabled_cart_ids = array();
        foreach ($carts as $cart) {
            if ((array_key_exists('product_order_selfcollect', $cart->product_information) && $cart->product_information['product_order_selfcollect']) == 0) {

                //Shipping
                if (!empty($cart->product->shipping_category) && $cart->product->shipping_category != 0) {

                    if (!empty($zoneId)) {
                        foreach ($zoneId as $key => $zone_id) {
                            $zone[] = $zone_id->id;
                        }
                        $Srates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $cart->product->shipping_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                    }

                    if (isset($Srates) && $Srates->isNotEmpty()  && ($Srates->first()->calculation_type == 'per-shipping-category')) {

                        if (!isset($shippingByCategories[$cart->product->shipping_category])) {
                            $shippingByCategories[$cart->product->shipping_category]['qty'] = 0;
                            $shippingByCategories[$cart->product->shipping_category]['subtotalPrice'] = 0;
                        }

                        $shippingByCategories[$cart->product->shipping_category]['qty'] += $cart->quantity;
                        $shippingByCategories[$cart->product->shipping_category]['subtotalPrice']  += $cart->subtotal_price;
                        $shippingByCategories[$cart->product->shipping_category]['item'] =  $cart;
                        $shippingByCategories[$cart->product->shipping_category]['items'][] =  $cart;
                    } elseif (isset($Srates) && $Srates->isNotEmpty()  && ($Srates->first()->calculation_type == 'per-line-quantity')) {

                        if (!isset($shippingByCategories[$cart->product->shipping_category][$cart->product_id])) {
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] = 0;
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] += $cart->quantity;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['item'] =  $cart;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['items'][] =  $cart;
                    } elseif (isset($Srates) && $Srates->isNotEmpty()  && ($Srates->first()->calculation_type == 'per-category-quantity')) {

                        if (!isset($shippingByCategories[$cart->product->shipping_category][$cart->product_id])) {
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] = 0;
                            $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['qty'] += $cart->quantity;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['item'] =  $cart;
                        $shippingByCategories[$cart->product->shipping_category][$cart->product_id]['items'][] =  $cart;

                        if (!isset($checkLightSubTotal['light'])) $checkLightSubTotal['light']['subtotalPrice'] = 0;

                        $checkLightSubTotal['light']['subtotalPrice']  += $cart->subtotal_price;
                    }
                }

                //Installation
                if ((array_key_exists('product_installation', $cart->product_information) && $cart->product_information['product_installation']) != 0) {

                    if (!empty($zoneId)) {
                        foreach ($zoneId as $key => $zone_id) {
                            $zone[] = $zone_id->id;
                        }
                        $iRates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $cart->product_information['product_installation'])->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                    }

                    if (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-shipping-category')) {

                        if (!isset($installationByCategories[$cart->product_information['product_installation']])) {
                            $installationByCategories[$cart->product_information['product_installation']]['qty'] = 0;
                            $installationByCategories[$cart->product_information['product_installation']]['subtotalPrice'] = 0;
                        }

                        $installationByCategories[$cart->product_information['product_installation']]['qty'] += $cart->quantity;
                        $installationByCategories[$cart->product_information['product_installation']]['subtotalPrice']  += $cart->subtotal_price;
                        $installationByCategories[$cart->product_information['product_installation']]['item'] =  $cart;
                        $installationByCategories[$cart->product_information['product_installation']]['items'][] =  $cart;
                    } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-line-quantity')) {

                        if (!isset($installationByCategories[$cart->product_information['product_installation']][$cart->product_id])) {
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] = 0;
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] += $cart->quantity;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['item'] =  $cart;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['items'][] =  $cart;
                    } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-category-quantity')) {

                        if (!isset($installationByCategories[$cart->product_information['product_installation']][$cart->product_id])) {
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] = 0;
                            $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice'] = 0;
                        }

                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['qty'] += $cart->quantity;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['subtotalPrice']  += $cart->subtotal_price;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['item'] =  $cart;
                        $installationByCategories[$cart->product_information['product_installation']][$cart->product_id]['items'][] =  $cart;

                        if (!isset($checkLightSubTotal['light'])) $checkLightSubTotal['light']['subtotalPrice'] = 0;

                        $checkLightSubTotal['light']['subtotalPrice']  += $cart->subtotal_price;
                    }
                }
            }
            $cartTotals['fee']['shipping']['amount'] += $cart->delivery_fee;

            $cartTotals['subtotal'] += $cart->subtotal_price;

            if (array_key_exists('product_order_tradein', $cart->product_information) && $cart->product_information['product_order_tradein'] != 0) {
                $cartTotals['fee']['rebate']['amount'] += ($cart->product_information['product_order_tradein'] * $cart->quantity);
                $cartTotals['fee']['rebate']['shipping_information'][] += ($cart->product_information['product_order_tradein'] * $cart->quantity);
            }

            //get tax and country currency & rate
            $cartTotals['currency'] = $currency;
            $cartTotals['tax_rate'] = $tax_rate;
            $cartTotals['tax_name'] = $tax_name;

            $cartTotals['unit_price'] = $cart->unit_price * $conversion_rate_markup;
            $cartTotals['subtotal_price'] = $cart->subtotal_price * $conversion_rate_markup;
            //end get tax and country currency & rate
        }

        if (isset($checkLightSubTotal['light'])) {
            if ($checkLightSubTotal['light']['subtotalPrice'] > '10000') {
                $moreThanHundred = True;
            }
        }

        // get array, loop for calculate fee
        foreach ($shippingByCategories as $shipping_category => $shipping_carts) {

            if (!empty($zoneId)) {
                foreach ($zoneId as $key => $zone_id) {
                    $zone[] = $zone_id->id;
                }
                $Srates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $shipping_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
            }

            if (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-shipping-category')) {
                $calculatedFee = self::calculateFee($shipping_carts['item'], $shipping_carts['qty'], $shipping_carts['subtotalPrice'], $zoneId, $shipping_category, $shipping_carts['items'], 'shipping');

                $cartTotals['shipping_new'] +=   $calculatedFee['fee'];
                $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];

                foreach ($shipping_carts['items'] as $groupIitem) {
                    if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                        $disabled_cart_ids[] = $groupIitem->id;
                        // $disabled_cart_shipngAmount += $cartTotals['fee']['shipping']['amount'];
                        // $disabled_cart_installationAmount += $cartTotals['fee']['installation']['amount'];
                    }
                }
            } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-line-quantity')) {

                foreach ($shipping_carts as $shipping_cart) {
                    $calculatedFee = self::calculateFee($shipping_cart['item'], $shipping_cart['qty'], $shipping_cart['subtotalPrice'], $zoneId, $shipping_category, $shipping_cart['items'], 'shipping');

                    $cartTotals['shipping_new'] +=   $calculatedFee['fee'];
                    $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];
                    if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                        $disabled_cart_ids[] = $shipping_cart['item']['id'];
                    }
                }
            } elseif (isset($Srates) && $Srates->isNotEmpty() && ($Srates->first()->calculation_type == 'per-category-quantity') && $moreThanHundred == FALSE) {

                foreach ($shipping_carts as $shipping_cart) {
                    $calculatedFee = self::calculateFee($shipping_cart['item'], $shipping_cart['qty'], $shipping_cart['subtotalPrice'], $zoneId, $shipping_category, $shipping_cart['items'], 'shipping');

                    $cartTotals['shipping_new'] +=   $calculatedFee['fee'];
                    $cartTotals['fee']['shipping']['shipping_information'][] = $calculatedFee['shipping_information'];
                    if ($calculatedFee['carts']['disabled'] == 1 || $calculatedFee['carts']['disabled'] == 2 || $calculatedFee['carts']['disabled'] == 6 || $calculatedFee['carts']['disabled'] == 7) {
                        $disabled_cart_ids[] = $shipping_cart['item']['id'];
                    }
                }
            }
        }
        if (!empty($installationByCategories)) {
            foreach ($installationByCategories as $installation_category => $installation_carts) {
                if (!empty($zoneId)) {
                    foreach ($zoneId as $key => $zone_id) {
                        $zone[] = $zone_id->id;
                    }
                    $iRates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $installation_category)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
                }

                if (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-shipping-category')) {

                    if (in_array($installation_carts['item']['id'], $disabled_cart_ids)) continue;

                    foreach ($installation_carts['items'] as $key => $i) {
                        if (in_array($i->id, $disabled_cart_ids)) {
                            $installation_carts['qty'] -=  $i->quantity;
                            $installation_carts['subtotalPrice'] -=  $i->subtotal_price;
                            $cartTotals['subtotal'] -= $i->subtotal_price;
                            unset($installation_carts['items'][$key]);
                        }
                    }

                    $calculatedFee = self::calculateFee($installation_carts['item'], $installation_carts['qty'], $installation_carts['subtotalPrice'], $zoneId, $installation_category, $installation_carts['items'], 'installation');
                    $cartTotals['fee']['installation']['amount'] +=   $calculatedFee['fee'];
                    $cartTotals['fee']['installation']['shipping_information'][] = $calculatedFee['shipping_information'];
                } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-line-quantity')) {

                    foreach ($installation_carts as $installation_cart) {
                        if (in_array($installation_cart['item']['id'], $disabled_cart_ids)) continue;

                        $calculatedFee = self::calculateFee($installation_cart['item'], $installation_cart['qty'], $installation_cart['subtotalPrice'], $zoneId, $installation_category, $installation_cart['items'], 'installation');
                        $cartTotals['fee']['installation']['amount'] += $calculatedFee['fee'];
                        $cartTotals['fee']['installation']['shipping_information'][] = $calculatedFee['shipping_information'];
                    }
                } elseif (isset($iRates) && $iRates->isNotEmpty() && ($iRates->first()->calculation_type == 'per-category-quantity') && $moreThanHundred == FALSE) {

                    foreach ($installation_carts as $installation_cart) {
                        if (in_array($installation_cart['item']['id'], $disabled_cart_ids)) continue;

                        $calculatedFee = self::calculateFee($installation_cart['item'], $installation_cart['qty'], $installation_cart['subtotalPrice'], $zoneId, $installation_category, $installation_cart['items'], 'installation');
                        $cartTotals['fee']['installation']['amount'] += $calculatedFee['fee'];
                        $cartTotals['fee']['installation']['shipping_information'][] = $calculatedFee['shipping_information'];
                    }
                }
            }
        }

        if ($disabled_cart_ids > 1) {
            $minusOutWrongsubtotalFromShipping  = Cart::whereIn('id', $disabled_cart_ids)->get();
            foreach ($minusOutWrongsubtotalFromShipping as $key => $cartMinus) {
                $cartTotals['subtotal'] -= $cartMinus->subtotal_price;
                $cartTotals['fee']['shipping']['amount'] -= $cartMinus->delivery_fee;
            }

            // $cartTotals['fee']['shipping']['amount'] -= $disabled_cart_shipngAmount;
            // $cartTotals['fee']['installation']['amount'] -= $disabled_cart_installationAmount;
        }

        $cartTotals['total'] = (($cartTotals['subtotal'] + $cartTotals['fee']['shipping']['amount'] + $cartTotals['fee']['installation']['amount']) - $cartTotals['fee']['rebate']['amount']) * $tax_rate;
        $cartTotals['total_new'] = ($cartTotals['subtotal'] + $cartTotals['shipping_new'] + $cartTotals['fee']['installation']['amount']) - $cartTotals['fee']['rebate']['amount'];

        $cartTotals['fee']['tax']['amount'] = $cartTotals['total'] * $tax_rate / 100;
        $cartTotals['fee']['tax']['shipping_information'] = ['cart_total' => $cartTotals['total'], 'tax_rate' => $tax_rate, 'formula' => 'cart_total * tax_rate %', 'total tax' => ($cartTotals['total'] * $tax_rate / 100)];

        return $cartTotals;
    }

    public static function calculateFee($carts, $qty, $subTotalPrice, $zoneId, $categoryId, $items, $type)
    {
        $fee = 0;
        $cartItems =  $shipping_information = array();

        if (!isset($cartItems[$carts->product_id])) {
            $cartItems[$carts->product_id]['totalQty'] = 0;
            $cartItems[$carts->product_id]['totalPrice'] = 0;
        }

        $cartItems[$carts->product_id]['totalQty'] += $qty;
        $cartItems[$carts->product_id]['totalPrice'] += $subTotalPrice;

        //get rates
        if (!empty($zoneId)) {
            foreach ($zoneId as $key => $zone_id) {
                $zone[] = $zone_id->id;
            }
            $rates = Rates::selectRaw('shipping_rates.*')->join('shipping_zones', 'shipping_zones.id', '=', 'shipping_rates.zone_id')->whereIn('zone_id', $zone)->where('category_id', $categoryId)->where('active', 'Y')->orderBy('shipping_zones.sequence', 'ASC')->get();
        }


        if (isset($rates) && $rates->isNotEmpty()) {

            foreach ($rates as $rate) {
                $minimun = $rate->min_rates;
                $maximum = $rate->max_rates;

                //check condition first, then check TOTAL qty|price between minimun and maximun or not, then check what is the calculation type
                if ($rate->condition == 'quantity') {

                    if ($cartItems[$carts->product_id]['totalQty'] >= $minimun && $cartItems[$carts->product_id]['totalQty'] <= $maximum) {

                        if ($rate->calculation_type == 'per-shipping-category') {

                            if ($rate->set_status != 0) {
                                // $carts->disabled = $rate->set_status;
                            } else {
                                // $carts->disabled = 0;
                            }

                            // $carts->disabled = 0;
                            $fee = $rate->price;
                        } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                            if ($rate->set_status != 0) {
                                // $carts->disabled = $rate->set_status;
                            } else {
                                // $carts->disabled = 0;
                            }

                            // $carts->disabled = 0;
                            $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                        }
                    } else {


                        if ($cartItems[$carts->product_id]['totalQty'] >= $maximum && $minimun == $maximum) {

                            if ($rate->calculation_type == 'per-shipping-category') {

                                if ($rate->set_status != 0) {
                                    // $carts->disabled = $rate->set_status;
                                } else {
                                    // $carts->disabled = 0;
                                }

                                // $carts->disabled = 0;
                                $fee = $rate->price;
                            } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                                if ($rate->set_status != 0) {
                                    // $carts->disabled = $rate->set_status;
                                } else {
                                    // $carts->disabled = 0;
                                }

                                // $carts->disabled = 0;
                                $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                            }
                        } else {

                            if ($type == 'shipping') {

                                if ($cartItems[$carts->product_id]['totalQty'] <= $minimun) {
                                    // $carts->disabled = 1;
                                } else {
                                    // $carts->disabled = 2;
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalQty'] <= $minimun) {
                                    // $carts->disabled = 7;
                                } else {
                                    // $carts->disabled = 6;
                                }
                            }

                            $fee = 0;
                        }
                    }
                } elseif ($rate->condition == 'price') {


                    if ($cartItems[$carts->product_id]['totalPrice'] >= $minimun && $cartItems[$carts->product_id]['totalPrice'] <= $maximum) {

                        if ($rate->calculation_type == 'per-shipping-category') {

                            if ($rate->set_status != 0) {
                                // $carts->disabled = $rate->set_status;
                            } else {
                                // $carts->disabled = 0;
                            }

                            // $carts->disabled = 0;
                            $fee = $rate->price;
                        } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                            if ($rate->set_status != 0) {
                                // $carts->disabled = $rate->set_status;
                            } else {
                                // $carts->disabled = 0;
                            }

                            // $carts->disabled = 0;
                            $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                        }
                    } else {

                        if ($cartItems[$carts->product_id]['totalPrice'] >= $maximum && $minimun == $maximum) {

                            if ($rate->calculation_type == 'per-shipping-category') {

                                if ($rate->set_status != 0) {
                                    // $carts->disabled = $rate->set_status;
                                } else {
                                    // $carts->disabled = 0;
                                }

                                // $carts->disabled = 0;
                                $fee = $rate->price;
                            } elseif ($rate->calculation_type == 'per-line-quantity' || $rate->calculation_type == 'per-category-quantity') {

                                if ($rate->set_status != 0) {
                                    // $carts->disabled = $rate->set_status;
                                } else {
                                    // $carts->disabled = 0;
                                }

                                // $carts->disabled = 0;
                                $fee = $rate->price * $cartItems[$carts->product_id]['totalQty'];
                            }
                        } else {

                            if ($type == 'shipping') {
                                if ($cartItems[$carts->product_id]['totalPrice'] <= $minimun) {
                                    // $carts->disabled = 1;
                                } else {
                                    // $carts->disabled = 2;
                                }
                            } else {
                                if ($cartItems[$carts->product_id]['totalPrice'] <= $minimun) {
                                    // $carts->disabled = 7;
                                } else {
                                    // $carts->disabled = 6;
                                }
                            }

                            $fee = 0;
                        }
                    }
                }

                if ($fee != 0 && isset($fee) && $fee > 0) {
                    $qty = $cartItems[$carts->product_id]['totalQty'];
                    $price = $cartItems[$carts->product_id]['totalPrice'];
                    $shipping_information[] = "Fee: {RM" . number_format(($fee / 100), 2) . "} - RateID: {$rate->id}, Condition: {$rate->condition}, Min: {$minimun}, Max: {$maximum}, Type: {$rate->calculation_type}, Qty: {$qty}, Price: {RM" . number_format(($price / 100), 2) . "}";
                    break;
                }
            }
        } else {

            if ($type == 'shipping') {
                // $carts->disabled = 2;
            } else {
                // $carts->disabled = 6;
            }

            $fee = 0;
        }
        $carts->save();

        //update same catgeory items disabled
        foreach ($items as $item) {
            $id[] = $item->id;
        }

        $updates = Cart::whereIn('id', $id)
            ->where('bundle_id', 0)
            ->where('status', 2001)
            ->where('selected', 1)
            ->get();

        if (isset($updates)) {
            foreach ($updates as $update) {
                if ((array_key_exists('product_order_selfcollect', $update->product_information) && $update->product_information['product_order_selfcollect']) == 0) {
                    $update->disabled = $carts->disabled;
                    $update->save();
                }
            }
        }
        //end update same catgeory items disabled

        //update bundle child item
        self::updateBundleChild();
        // end update bundle child item

        return compact('fee', 'carts', 'shipping_information');
    }

    public static function updateBundleChild()
    {
        $user = User::find(Auth::user()->id);

        $selectedBundleChilds = Cart::where('user_id', $user->id)
            ->where('selected', 1)
            ->where('status', 2001)
            ->where('bundle_id', '!=', 0)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->get();

        // check and sync child status with parent status for bundle
        foreach ($selectedBundleChilds as $selectedBundleChild) {
            $parentBundle = Cart::where('user_id', $user->id)->where('id', $selectedBundleChild->bundle_id)->first();
            $selectedBundleChild->selected = $parentBundle->selected;
            $selectedBundleChild->disabled = $parentBundle->disabled;
            $selectedBundleChild->save();
        }
    }

    public static function maybeCreateCookieAddress($user)
    {
        $zone['city_key'] = 0;
        $zone['state_id'] = 0;

        $shipCookie = json_decode(Cookie::get('addressCookie'));

        if (isset($shipCookie)) {

            $zone['city_key'] = (isset($shipCookie->city_key)) ? $shipCookie->city_key : NULL;
            $zone['state_id'] = (isset($shipCookie->state_id)) ? $shipCookie->state_id : 0;
        } else {

            if (isset($user->userInfo->shippingAddress->city)) $zone['city_key'] =  $user->userInfo->shippingAddress->city;
            if (isset($user->userInfo->shippingAddress->state_id)) $zone['state_id'] =  $user->userInfo->shippingAddress->state_id;
        }

        return $zone;
    }

    public static function getZone($user)
    {
        //get user profile city and state
        $zone = self::maybeCreateCookieAddress($user);

        $state_id = $zone['state_id'];
        $city_key = $zone['city_key'];

        $userCity = City::where('city_key', $city_key)->pluck('id')->first();
        $userState = $state_id;

        $userCity = (isset($userCity)) ? $userCity : NULL;
        $userState = (isset($userState)) ? $userState : NULL;

        $locations = Locations::join('shipping_zones', 'shipping_zones.id', '=', 'shipping_locations.zone_id')
            ->whereIn('shipping_locations.location_id', [$userCity, $userState])
            ->get();

        $array = array();
        foreach ($locations as $location) {
            $array[$location->sequence] = $location;
        }
        if (!empty($array)) {
            ksort($array);
        }

        return $array;
    }

    /**
     * Refresh cart
     */
    public static function updateCartItems(User $user)
    {

        $user = User::find(Auth::user()->id);
        $userInfo = $user->userInfo;

        $selectedCartItems = Cart::where('user_id', $user->id)
            ->where('selected', 1)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->get();

        $selectedBundleChilds = Cart::where('user_id', $user->id)
            ->where('selected', 1)
            ->where('status', 2001)
            ->where('bundle_id', '!=', 0)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->get();

        // Check Bundle Child
        // dd($selectedBundleChild);


        //check if user ini not dc customer
        if ($userInfo->account_status == '0') {
            // return;
            return;
        } else {

            // for each selected cart item
            foreach ($selectedCartItems as $selectedCartItem) {

                $img_link = $selectedCartItem->product_information;

                $img_link['product_color_img'] = $selectedCartItem->getImageUrlAttribute($selectedCartItem->product->parentProduct->id);

                $selectedCartItem->product_information = $img_link;

                $selectedCartItem->save();


                /* ---------------------------------- Rebate  ---------------------------------- */
                if (($selectedCartItem->rebate != 0) && ($selectedCartItem->product->parentProduct->can_tradein != 1)) {

                    $selectedCartItem->rebate = 0;

                    $attr = $selectedCartItem->product_information;

                    $attr['product_order_tradein'] = 0;

                    $selectedCartItem->product_information = $attr;

                    $selectedCartItem->save();
                }

                /* ---------------------------------- Disabled Product  ---------------------------------- */
                if ($selectedCartItem->product->parentProduct->product_status == 2) {

                    $selectedCartItem->selected = 0; // disabled parent bundle

                    $bundleChilds = Cart::where('bundle_id', $selectedCartItem->id)->get(); // find bundle's child

                    foreach ($bundleChilds as $bundleChild) {
                        // dd($bundleChild->selected);
                        $bundleChild->selected = 0; //unselected bundle child
                        $bundleChild->save();
                    }

                    $selectedCartItem->save();
                }

                /* ---------------------------------- size ---------------------------------- */
                if (isset($selectedCartItem->product_information['product_size_id'])) {
                    // return $selectedCartItem;

                    $selectedPrice = $selectedCartItem->product_information['product_size_id'];
                    //  $selectedPrice = $selectedCartItem->product_information['product_size_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);
                    // cart item price = dc customer price
                    // if member price is not 0
                    if (!empty($selectedProductID->getPriceAttributes('member_price'))) {
                        if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                            $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                        } else {
                            $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                        }
                    }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;


                    /* ------------------------------- temperature ------------------------------ */
                } elseif (isset($selectedCartItem->product_information['product_temperature_id'])) {

                    $selectedPrice = $selectedCartItem->product_information['product_temperature_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);

                    // cart item price = dc customer price
                    // if member price is not 0
                    if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                        if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                            $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                        } else {
                            $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                        }
                    }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;

                    /* ------------------------------ curtain size ------------------------------ */
                } elseif (isset($selectedCartItem->product_information['product_curtain_size_id'])) {

                    $selectedPrice = $selectedCartItem->product_information['product_curtain_size_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);

                    // cart item price = dc customer price
                    // if member price is not 0
                    if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                        if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                            $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                        } else {
                            $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                        }
                    }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;

                    /* ---------------------------------- color --------------------------------- */
                } elseif (isset($selectedCartItem->product_information['product_color_id'])) {

                    $selectedPrice = $selectedCartItem->product_information['product_color_id'];

                    // get item punya global product -> dc customer price
                    $selectedProductID = ProductAttribute::find($selectedPrice);

                    // cart item price = dc customer price
                    // if member price is not 0
                    if (!empty($selectedProductID->getPriceAttributes('member_price'))) {

                        if ($selectedProductID->getPriceAttributes('member_price') != 0) {
                            $selectedCartItem->unit_price = $selectedProductID->getPriceAttributes('member_price');
                        } else {
                            $selectedCartItem->unit_price = $selectedCartItem->product->getPriceAttributes('member_price');
                        }
                    }
                    $selectedCartItem->subtotal_price = $selectedCartItem->unit_price * $selectedCartItem->quantity;
                }

                // cart item -> save()
                $selectedCartItem->save();
            }
        }

        // check and sync child status with parent status for bundle
        foreach ($selectedBundleChilds as $selectedBundleChild) {
            $parentBundle = Cart::where('user_id', $user->id)->where('id', $selectedBundleChild->bundle_id)->first();
            $selectedBundleChild->selected = $parentBundle->selected;
            $selectedBundleChild->disabled = $parentBundle->disabled;
            $selectedBundleChild->save();
        }

        return;
    }

    /**
     * Get total item count in a user's cart.
     */
    public function getTotalCartQuantity($id)
    {
        $user = User::find($id);

        $cartQuantity = $user->carts->where('status', 2001)->where('bundle_id', 0)->where('country_id', country()->country_id)->sum('quantity');

        $data['status'] = 'OK';
        $data['message'] = 'Get request successful.';
        $data['data'] = $cartQuantity;

        return response()->json($data, 200);
    }

    /**
     * Delete a cart item.
     */
    public function remove($id)
    {
        $cartItem = Cart::find($id);
        $cartItem->status = 2002;
        $cartItem->save();

        //bundle
        $cartSubItems = Cart::find($id)->where('bundle_id', $id)->get();

        if (isset($cartSubItems)) {
            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->status = 2002;
                $cartSubItem->save();
            }
        }
        //bundle end

        $this->index();

        return $cartItem;
    }

    /**
     * Update cart quantity.
     */
    public function updateQuantity(Request $request, $id)
    {
        //bundle
        $cartSubItems = Cart::where('bundle_id', $id)->get();
        $main = Cart::find($id);

        if (isset($cartSubItems)) {
            $currentValue = $request->input('quantity');

            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->quantity = $cartSubItem->quantity / $main->quantity * $currentValue;

                $cartSubItem->save();
            }
        }
        //bundle end

        $cartItem = Cart::find($id);

        // Get user
        $user = User::find(Auth::user()->id);

        // Get product
        $product = PanelProduct::find($cartItem->product_id);

        //
        $userInfo = $user->userInfo;

        //
        $userShipping = $userInfo->shippingAddress;

        // Coookie
        $shipCookie = json_decode(Cookie::get('addressCookie'));
        if (isset($shipCookie)) {
            $stateDelivery = $shipCookie->state_id;
        } else {
            $stateDelivery = $userShipping->state_id;
        }

        $deliveryFee = $product->deliveries->where('state_id', $stateDelivery)->first();

        $cartItem->quantity = $request->input('quantity');
        // Trade In
        $cartItem->rebate = ($cartItem->rebate != 0) ? 50000 * $request->input('quantity') : 0;
        $cartItem->subtotal_price = $cartItem->quantity * $cartItem->unit_price;

        $this_selfcollect = (isset($cartItem->product_information['product_order_selfcollect']) && $cartItem->product_information['product_order_selfcollect']);
        if (!$this_selfcollect) {

            $cartItem->delivery_fee = isset($deliveryFee->delivery_fee) ? $deliveryFee->delivery_fee * $request->input('quantity') : 0;
        } else {
            $cartItem->delivery_fee = 0;
        }





        // $this_tradein = (isset($cartItem->product_information['product_order_tradein']) && $cartItem->product_information['product_order_tradein']);
        // if ($this_tradein){

        //     $cartItem->rebate =  $cartItem->rebate * $request->input('quantity');

        // }

        $cartItem->save();

        $stateDelivery = $this->getStateCookies($user);



        $currentCartItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->get();

        foreach ($currentCartItems as $currentCartItem) {

            $currentCartItem =  $this->checkStateAvailability($currentCartItem, $stateDelivery);


            // $currentCartItem->product->parentProduct->product_status == 2;

            if ($currentCartItem->disabled == 0) {

                if ($currentCartItem->selected == 1 && $currentCartItem->product->parentProduct->product_status != 2) {

                    $product = $currentCartItem->product;

                    $panel = $product->panel;

                    $productCategoriesArray = [];

                    $productCategories = $product->parentProduct->categories;

                    foreach ($productCategories as $key => $productCategory) {
                        $productCategoriesArray[$key] = $productCategory->id;
                    }

                    $totalSubtotalPrice = $this->getSubtotals($panel, $productCategoriesArray, $product, $user);

                    $freeDeliveryMinPrices = NULL;
                    if (country()->country_id == 'MY') {
                        $freeDeliveryMinPrices = $product
                            ->panel
                            ->categoriesWithMinPrice
                            ->whereIn('category_id', $productCategoriesArray)
                            ->all();
                    }


                    if ($freeDeliveryMinPrices) {

                        $currentCartItem =  $this->calculateFreeDelivery($stateDelivery, $currentCartItem, $freeDeliveryMinPrices, $totalSubtotalPrice);
                    }
                }
            }
        }


        $this->index();

        return $cartItem;
    }

    /**
     * Update checked status.
     */
    public function toggleSelectItem(Request $request, $id)
    {
        $cartItem = Cart::findOrFail($id);

        if ($cartItem->selected == 0) {
            $cartItem->selected = 1;
        } else {
            $cartItem->selected = 0;
        }
        $cartItem->save();

        //bundle
        $cartSubItems = Cart::where('bundle_id', $id)->get();
        if (isset($cartSubItems)) {
            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->selected = $cartItem->selected;

                $cartSubItem->save();
            }
        }
        //bundle end

        /*------------------------------ Recalculate fee ------------------------*/

        //new calculation
        // $user = User::find(Auth::user()->id);

        // $zoneId = self::getZone($user);

        // $cartItems = Cart::where('user_id', $user->id)
        //     ->where('status', 2001)
        //     ->where('bundle_id', 0)
        //     ->whereNotIn('disabled', [5])
        //     ->get();

        // $this->shippingCalculate($zoneId, $cartItems);
        //end new calculation


        // old calculation
        // Get user
        $user = User::find(Auth::user()->id);
        $stateDelivery = $this->getStateCookies($user);



        $currentCartItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->get();

        foreach ($currentCartItems as $currentCartItem) {

            $currentCartItem =  $this->checkStateAvailability($currentCartItem, $stateDelivery);


            // $currentCartItem->product->parentProduct->product_status == 2;

            if ($currentCartItem->disabled == 0) {

                if ($currentCartItem->selected == 1 && $currentCartItem->product->parentProduct->product_status != 2) {

                    $product = $currentCartItem->product;

                    $panel = $product->panel;

                    $productCategoriesArray = [];

                    $productCategories = $product->parentProduct->categories;

                    foreach ($productCategories as $key => $productCategory) {
                        $productCategoriesArray[$key] = $productCategory->id;
                    }

                    $totalSubtotalPrice = $this->getSubtotals($panel, $productCategoriesArray, $product, $user);

                    $freeDeliveryMinPrices = NULL;
                    if (country()->country_id == 'MY') {
                        $freeDeliveryMinPrices = $product
                            ->panel
                            ->categoriesWithMinPrice
                            ->whereIn('category_id', $productCategoriesArray)
                            ->all();
                    }


                    if ($freeDeliveryMinPrices) {

                        $currentCartItem =  $this->calculateFreeDelivery($stateDelivery, $currentCartItem, $freeDeliveryMinPrices, $totalSubtotalPrice);
                    }
                }
            }
        }
        //end old calculation

        $this->index();

        return response()->json($cartItem, 200);
    }
    /**
     * Update address.
     */
    public function getStateCookies($user)
    {
        $userInfo = $user->userInfo;

        $userShipping = $userInfo->shippingAddress;

        // Coookie
        $shipCookie = json_decode(Cookie::get('addressCookie'));
        if (isset($shipCookie)) {
            $stateDelivery = $shipCookie->state_id;
        } else {
            $stateDelivery = $userShipping->state_id;
        }

        return $stateDelivery;
    }

    public function checkStateAvailability($currentCartItem, $stateDelivery)
    {
        $deliveryFeeForProduct = $currentCartItem
            ->product
            ->deliveries
            ->where('state_id', $stateDelivery)
            ->whereNotIn('disabled', [5])
            ->first();
        if ($currentCartItem->product->parentProduct->product_status != 2) {
            if ($deliveryFeeForProduct) {
                $selected = $currentCartItem->selected;
                $disabledStatus = 0;
            } else {
                $selected = 0;
                $disabledStatus = 2;
            }
        } else {
            $selected = 0;
            $disabledStatus = 4;
        }

        $currentCartItem->selected = $selected;
        $currentCartItem->disabled = $disabledStatus;
        $currentCartItem->save();

        $cartSubItems = Cart::where('bundle_id', $currentCartItem->id)->get();
        if (isset($cartSubItems)) {
            foreach ($cartSubItems as $cartSubItem) {
                $cartSubItem->disabled = $disabledStatus;
                $cartSubItem->selected = $currentCartItem->selected;
                $cartSubItem->save();
            }
        }

        return  $currentCartItem;
    }

    public function getSubtotals($panel, $productCategoriesArray, $product, $user)
    {
        $totalSubtotalPrice = 0;

        $deliveryItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->where('disabled', 0)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->whereHas('product.panel', function ($q) use ($panel) {
                $q->where('account_id', $panel->account_id);
            })
            ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                    $q->whereIn('categories.id', $productCategoriesArray);
                });
            })
            ->get();


        foreach ($deliveryItems as $deliveryItem) {
            $totalSubtotalPrice = $totalSubtotalPrice + $deliveryItem->subtotal_price;
        }

        return $totalSubtotalPrice;
    }

    public function calculateFreeDelivery($stateDelivery, $currentCartItem, $freeDeliveryMinPrices, $totalSubtotalPrice)
    {
        $isDeliveryFree = 0;
        $category_id = null;
        foreach ($freeDeliveryMinPrices as $freeDeliveryMinPrice) {
            // More than min, less than max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 1) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_NO_PURCHASE;
                break;
            }
            // Less than min min equal to max, can purchase
            if ($totalSubtotalPrice <= $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $group = $freeDeliveryMinPrice->group_or_individual;
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $isDeliveryFree = self::LESS_CAN_PURCHASE;
                break;
            }
            // More than min, less than max, can purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->min_price && $totalSubtotalPrice <= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $isDeliveryFree = self::BETWEEN_PRICE;
                break;
            }
            // More than max, min equal to max, no purchase
            if ($totalSubtotalPrice >= $freeDeliveryMinPrice->max_price && $freeDeliveryMinPrice->max_price == $freeDeliveryMinPrice->min_price && $freeDeliveryMinPrice->no_purchase == 0) {
                $fixDeliveryFee = $freeDeliveryMinPrice->delivery_fee;
                $group = $freeDeliveryMinPrice->group_or_individual;
                $freeStates = json_decode($freeDeliveryMinPrice->free_state);
                $isDeliveryFree = self::MAX_FREE_DELIVERY;
                break;
            }

            $category_id = $freeDeliveryMinPrice->category_id;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 1;
        }

        if (!isset($this->itemCount[$category_id])) {
            $this->itemCount[$category_id] = 0;
        }

        $this->itemCount[$category_id]++;

        return $deliveryFeeForProduct = $currentCartItem
            ->product
            ->deliveries
            ->where('state_id', $stateDelivery)
            ->first();



        switch ($isDeliveryFree) {
            case self::LESS_NO_PURCHASE:
                $fee = 0;
                $disabled = 1;
                break;
            case self::LESS_CAN_PURCHASE:
                if ($group == 'g') {
                    $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                } else {
                    return $fee = $deliveryFeeForProduct->delivery_fee * $currentCartItem->quantity;
                }
                $disabled = 0;
                break;
            case self::BETWEEN_PRICE:
                if ($group == 'g') {
                    $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                } else {
                    $fee = $deliveryFeeForProduct->delivery_fee * $currentCartItem->quantity;
                }
                $disabled = 0;
                break;
            case self::MAX_FREE_DELIVERY:

                if ($freeStates) {
                    if (in_array($stateDelivery, $freeStates)) {
                        $disabled = 0;
                        $fee =  0;
                    } else {
                        $disabled = 3;
                        $fee = (($this->itemCount[$category_id]) == 1) ? $fixDeliveryFee : 0;
                    }
                } else {
                    $disabled = 0;
                    $fee = 0;
                }

                break;
            default:
                $fee =  $deliveryFeeForProduct->delivery_fee * $currentCartItem->quantity;
                $disabled = 0;
        }
        $this_selfcollect = (isset($currentCartItem->product_information['product_order_selfcollect']) && $currentCartItem->product_information['product_order_selfcollect']);
        $currentCartItem->delivery_fee = ($this_selfcollect) ? 0 : $fee;

        $currentCartItem->delivery_fee = $fee;
        $this->itemCount[$category_id]++;
        $currentCartItem->save();
        return $currentCartItem;
    }

    public function productChangeAddress(Request $request)
    {
        $user = User::find(Auth::user()->id);
        // $shipTo = $user->userInfo->cartShippingAddress;

        // if ($shipTo == NULL) {
        //     $shipTo = new UserAddress;
        //     $shipTo->account_id = $user->userInfo->account_id;
        //     $shipTo->address_1 = $request->input('address_1');
        //     $shipTo->address_2 = $request->input('address_2');
        //     $shipTo->address_3 = $request->input('address_3');
        //     $shipTo->postcode = $request->input('postcode');
        //     $shipTo->city_key = $request->input('city_key');
        //     if ($request->input('city_key') == 0) $shipTo->city = $request->input('city');
        //     $shipTo->country_id = country()->country_id;
        //     $shipTo->state_id = $request->input('state_id');

        //     $shipTo->is_shipping_address = 1;

        //     $shipTo->save();
        // }


        $city = NULL;
        if ($request->input('city_name') != NULL && $request->input('city_key') == 0) $city = $request->input('city_name');

        $new_shipCookie = json_encode(array(
            'address_1' => $request->input('address_1'),
            'address_2' => $request->input('address_2'),
            'address_3' => $request->input('address_3'),
            'postcode' => $request->input('postcode'),
            'city' => $city,
            'city_name' => $request->input('city_key') != '0' ? City::where('city_key', $request->input('city_key'))->first() : $request->input('city_name'),
            'city_key' => $request->input('city_key'),
            'state_name' => State::find($request->input('state_id')),
            'state_id' => $request->input('state_id'),
            'name' => $request->input('name'),
            'mobile' => $request->input('mobile')
        ));

        $stateDelivery = $request->input('state_id');

        $currentCartItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('bundle_id', 0)
            ->whereNotIn('disabled', [5])
            ->get();


        foreach ($currentCartItems as $currentCartItem) {

            $currentCartItem =  $this->checkStateAvailability($currentCartItem, $stateDelivery);

            if ($currentCartItem->disabled == 0) {

                if ($currentCartItem->selected == 1) {

                    $product = $currentCartItem->product;

                    $panel = $product->panel;

                    $productCategoriesArray = [];

                    $productCategories = $product->parentProduct->categories;

                    foreach ($productCategories as $key => $productCategory) {
                        $productCategoriesArray[$key] = $productCategory->id;
                    }

                    $totalSubtotalPrice = $this->getSubtotals($panel, $productCategoriesArray, $product, $user);

                    $freeDeliveryMinPrices = $product
                        ->panel
                        ->categoriesWithMinPrice
                        ->whereIn('category_id', $productCategoriesArray)
                        ->all();

                    if ($freeDeliveryMinPrices) {

                        $currentCartItem =  $this->calculateFreeDelivery($stateDelivery, $currentCartItem, $freeDeliveryMinPrices, $totalSubtotalPrice);
                    }
                }
            }
        }

        return redirect()->back()->withCookie(Cookie::make('addressCookie', $new_shipCookie, 3600));
    }

    // backend checking bundle
    public static function doubleCheckingSubitem($panelProductId, $parentqty, $getProductBundleID, $bundleqty)
    {
        $BundleSets = array();
        $UserInputs = array();

        $fromBundleSets = ProductBundle::where('panel_product_id', $panelProductId)->get(); // From Bundle Set
        $fromUserInputs = ProductBundle::whereIn('id', $getProductBundleID)->get(); // From User Input

        foreach ($fromBundleSets as $fromBundleSet) {
            if (!isset($BundleSets[$fromBundleSet->bundle_id])) {
                $BundleSets[$fromBundleSet->bundle_id]['checkingqty'] = $fromBundleSet->bundle_qty * $parentqty;
            }
        }

        foreach ($fromUserInputs as $fromUserInput) {
            if (!isset($UserInputs[$fromUserInput->bundle_id])) {
                $UserInputs[$fromUserInput->bundle_id]['checkingqty'] = 0;
            }
            $UserInputs[$fromUserInput->bundle_id]['checkingqty'] += $bundleqty[$fromUserInput->id];
        }

        //compare 2 array
        if ($BundleSets != $UserInputs) return 1;
        // return back()->with(['error_message' => 'cannot add to cart !']);

        // double check whether sub items are in bundle or not
        $checkingSubItem = ProductBundle::where('panel_product_id', $panelProductId)->whereIn('id', $getProductBundleID)->get();
        if ($checkingSubItem->isEmpty()) return 1;
        // return back()->with(['error_message' => 'Wrong sub item !']);

    }

    public static function stateFilterCity(Request $request)
    {
        $state_id = 0;

        if ($request->input('state_id') != NULL) {
            $state_id = $request->input('state_id');
        } elseif ($request->input('shipping_state') != NULL) {
            $state_id = $request->input('shipping_state');
        }

        $cities = City::where('state_id', $state_id)->orderBy('city_name')->get();

        foreach ($cities as  $city) {
            $City[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $City[] = '<option value="0" "selected">Others</option>';

        return $City;
    }
    public static function stateFilterBillingCity(Request $request)
    {
        $state_id = 0;

        if ($request->input('state_id') != NULL) {
            $state_id = $request->input('state_id');
        } elseif ($request->input('billing_state') != NULL) {
            $state_id = $request->input('billing_state');
        }

        $billcities = City::where('state_id', $state_id)->get();

        foreach ($billcities as  $city) {
            $City[] = '<option value="' . $city->city_key . ' " >' . $city->city_name . ' </option>';
        }

        $City[] = '<option value="0" "selected">Others</option>';

        return $City;
    }

    public function getCoupon($coupon, $counter = 0)
    {
        $user = User::find(Auth::user()->id);
        $country_code = country()->country_id;

        $today = date('Y-m-d');
        $sameCode = false;
        $data = Session::get('bjs_cart_discount');

        if (!isset($coupon) || empty($coupon)) {
            $data['voucher'][$counter]['discount_amount'] = 0;
            Session::put('bjs_cart_discount', $data);

            $code = new stdClass();
            $code->coupon_amount = '0.00';

            return response()->json($code);
        } else {
            // $this->updateVoucherCodeCount($coupon);
        }

        $selectedCartItems = Cart::where('user_id', $user->id)
            ->where('status', 2001)
            ->where('selected', 1)
            ->whereNotIn('disabled', [5])
            ->where('country_id', country()->country_id)
            ->get();

        if (count($selectedCartItems)) {
            foreach ($selectedCartItems as $v) {
                $v['eligible_discount'] = $v->product->parentProduct['eligible_discount'];
                $selectedCart[] = $v;
            }
        }

        $cart_count = collect($selectedCart)->count();
        $eligible_discount = collect($selectedCart)->where('eligible_discount', 0)->count();
        $eligible_discount_count = collect($eligible_discount)->count();

        $amount = collect($eligible_discount)->sum('subtotal_price');
        $amount = ($amount / 100);

        $isPWP = substr($coupon, 0, 2);

        if ($eligible_discount === $cart_count || ($isPWP == 'PW')) {
            $code = DB::table('discount_code as a')
                ->select('a.*', 'a.coupon_type as code_coupon_type', 'b.*')
                ->join('discount_rule as b', 'a.coupon_rule_id', '=', 'b.id')
                ->where([
                    ['a.country_id', $country_code],
                    ['a.coupon_code', $coupon],
                    ['a.expiry', '>=', $today],
                    ['a.coupon_status', 'valid']
                ])
                ->first();
            if (isset($data['voucher'])) {
                foreach ($data['voucher'] as $voucher) {

                    $firstInvoice = Code::where('coupon_code', $voucher['discount_code'])->first();
                    $secondInvoice = Code::where('coupon_code', $coupon)->first();

                    if ($coupon == $voucher['discount_code'] || $firstInvoice->created_by_purchase == $secondInvoice->created_by_purchase) {
                        $sameCode = true;
                    }
                }
            }

            if ($code) {
                $code->usage_counter = $code->usage_counter + (isset($data['voucher']) ? count($data['voucher']) : 0);
                // dd($code->general_type);
                if ($code->general_type == 'breakdown') {
                    // $monthlyUsage = $this->monthlyUsage($user->id,$code->created_by_purchase);
                    $monthlyUsage = $this->monthlyUsageShipping($user->id, $coupon, $code->general_type);
                    // dd($code->usage_counter);
                    if (($sameCode && ($monthlyUsage + 1) >= $code->limit) || $monthlyUsage >= $code->limit) {
                        return ['status' => 'error', 'error' => "This voucher only can be use {$code->limit} time per month"];
                    } elseif (($sameCode && $code->usage_counter >= ($code->max_voucher_usage + 1)) || $code->usage_counter >= $code->max_voucher_usage) {
                        return ['status' => 'error', 'error' => 'Coupon already max usage or not valid'];
                    } else {
                        if ($code->code_coupon_type == 'value') {

                            $data['voucher'][$counter]['discount_amount'] = $code->coupon_amount;
                            $data['voucher'][$counter]['discount_code'] = $code->coupon_code;

                            Session::put('bjs_cart_discount', $data);
                            // dd($data);

                            Session::get('bjs_cart_discount');

                            return response()->json($code);
                        } else {
                            $code->coupon_amount_percentage = $code->coupon_amount;
                            $code->coupon_amount = number_format($amount * ($code->coupon_amount / 100), 2);

                            $data['voucher'][$counter]['discount_amount'] = $code->coupon_amount;
                            $data['voucher'][$counter]['discount_code'] = $code->coupon_code;

                            Session::put('bjs_cart_discount', $data);
                            // dd($data);
                            Session::get('bjs_cart_discount');
                            return response()->json($code);
                        }
                    }
                } elseif ($code->general_type == 'pairing') {
                    $code->limit_type;

                    $limit_product_code = json_decode($code->limit_id);

                    // $product = PanelProduct::where('global_product_id',json_decode($code->limit_id))->first();
                    // $inCart = collect($selectedCartItems)->where('product_id',$product->id)->first();

                    $inCart = collect($selectedCartItems)->where('product_code', $limit_product_code[0])->first();

                    $monthlyUsage = $this->monthlyUsageShipping($user->id, $coupon, $code->general_type);

                    if (($sameCode && (($monthlyUsage + 1) >= $code->limit)) || $monthlyUsage >= $code->limit) {
                        return ['status' => 'error', 'error' => "This voucher only can be use {$code->limit} time per month"];
                    } elseif ($code->available_for == 'personal' && $user->id != $code->user_id) {
                        return ['status' => 'error', 'error' => 'Coupon valid for personal use only.'];
                    } else {
                        if ($inCart && $inCart->quantity >= $code->minimum) {

                            $discount_amount = ($inCart->unit_price * $code->minimum);

                            $code->coupon_amount = number_format(($discount_amount / 100), 2);

                            $data['voucher'][$counter]['discount_amount'] = $code->coupon_amount;
                            $data['voucher'][$counter]['discount_code'] = $code->coupon_code;
                            Session::put('bjs_cart_discount', $data);

                            return response()->json($code);
                        } else {
                            if ($inCart) {
                                return ['status' => 'error', 'error' => 'Coupon not available for this quantity... minimum (' . (int) $code->minimum . ')'];
                            } else {
                                return ['status' => 'error', 'error' => 'Coupon expired or not valid'];
                            }
                        }
                    }
                } else {
                    if (($sameCode && $code->max_voucher_usage == 1) || $code->usage_counter >= $code->limit) {
                        return ['status' => 'error', 'error' => 'Coupon expired or not valid'];
                    } elseif ($code->available_for == 'personal' && $user->id != $code->user_id) {
                        return ['status' => 'error', 'error' => 'Coupon valid for personal use only.'];
                    } elseif (!$this->checkLimit($selectedCartItems, $code)) {
                        return ['status' => 'error', 'error' => 'Coupon is not valid for this product.'];
                    } else {
                        if ($code->code_coupon_type == 'value') {
                            $data['voucher'][$counter]['discount_amount'] = $code->coupon_amount;
                            $data['voucher'][$counter]['discount_code'] = $code->coupon_code;
                            Session::put('bjs_cart_discount', $data);

                            return response()->json($code);
                        } else {
                            $code->coupon_amount_percentage = $code->coupon_amount;
                            $code->coupon_amount = number_format($amount * ($code->coupon_amount / 100), 2);

                            $data['voucher'][$counter]['discount_amount'] = $code->coupon_amount;
                            $data['voucher'][$counter]['discount_code'] = $code->coupon_code;
                            Session::put('bjs_cart_discount', $data);

                            return response()->json($code);
                        }
                    }
                }
            } else {
                return ['status' => 'error', 'error' => 'Coupon already use or expired.'];
            }
        } else {
            return ['status' => 'error', 'error' => 'One or more product is not eligible for discount'];
        }
    }

    public function checkLimit($cart, $rule)
    {

        if (count($cart) == 0) return;

        foreach ($cart as $val) {
            $global_product_id[] = CoreCache::getProductId($val->product_id);
            $product_id[] = $val->product_id;
        }

        switch ($rule->limit_type) {
            case 'ex_category':
            case 'in_category':
                $result = CoreCache::limit_category_product($rule->limit_type, $rule->limit_id);
                $inlimit = collect($result)->whereIn('global_product_id', $global_product_id);
                break;
            case 'ex_product':
            case 'in_product':
                $result = CoreCache::limit_category_product($rule->limit_type, $rule->limit_id);
                $inlimit = collect($result)->whereIn('id', $product_id);
                break;
            default: //na
                $result = CoreCache::limit_category_product($rule->limit_type, json_encode($product_id));
                $inlimit = collect($result)->whereIn('id', $product_id);
        }


        if ((count($inlimit) > 0)) {
            return true;
        } else {
            return false;
        }
    }
}
