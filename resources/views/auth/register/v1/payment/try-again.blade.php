@php
    $countryObj = country();
    $country = $countryObj->country_id;
@endphp

@extends('layouts.guest.v1.main')

@section('content')
<div class="bg-md bg-sm ">
    <div class="row">
        <div class="col-6 offset-3 col-md-2 offset-md-5 mb-0 pt-2 pb-3">
            <img class="mw-100" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="">
        </div>
    </div>
    <div>
        <div class="card border-rounded-0 bg-bujishu-gold guests-card " style="border-radius: 10px;">
            <h5 class="text-center bujishu-gold form-card-title " style="border-radius: 10px;">Agent Registration</h5>
            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link register-tab-active active " id="contact-tab" data-toggle="tab" href="#introducer" role="tab" aria-controls="contact" aria-selected="false">Payment Information</a>
                </li>
            </ul>
            <div class="card-body ">
                <!-- Dealer Registration Form -->
                <form method="POST" action="{{ route('guest.register.dealer.payment.try.post') }}" id="register-form" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content" id="myTabContent">
                        <!-- Registration  Tab-->
                        <div class="tab-pane fade show active" id="registration" role="tabpanel" aria-labelledby="registration-tab">
                            <!-- Introducer Particular -->
                            <h5 class="text-center" style="background-color: #303030; color: #ffffff; padding: .5rem; border: 1px solid #e5e5e5;">Payment Information</h5>
                            <!-- Card div -->
                            <div id="paymentCardDiv" style="display:none;">
                                <div class="form-row">
                                    <div class="col-12 mb-1 form-group">
                                        <label for="card_number">Card Number <small class="text-danger">*</small></label>
                                        <input type="text" class="form-control" name="card_number" id="card_number" placeholder="Card Number">
                                        <input type="hidden" name="card_type" id="card_type" value="">
                                        <div class="valid-feedback feedback-icon with-cc-icon">
                                            <i class="fa"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon with-helper">
                                            <i class="fa fa-times"></i>
                                        </div>
                                        <div class="invalid-feedback invalid-helper">
                                            <small class="text-danger">Card number must be 16.</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12 col-md-6 mb-1 form-group">
                                        <label for="expiry_date">Expiration Date <small class="text-danger">*</small></label>
                                        <input type="text" class="form-control" name="expiry_date" id="expiry_date" placeholder="MMYY">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 mb-1 form-group">
                                        <label for="cvv">CVV <small class="text-danger">*</small></label>
                                        <input type="text" class="form-control" name="cvv" id="cvv" placeholder="CVV">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Offline payment div -->
                            <div id="paymentOfflineDiv">
                                <div class="form-row">
                                    <div class="col-12 col-md-6 form-group">
                                        <label for="offline_reference">Reference Number</label>
                                        <input type="text" name="offline_reference" id="offline_reference" class="form-control">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 form-group">
                                        <label for="offline_amount">Payment Amount</label>
                                        <input type="text" name="offline_amount" id="offline_amount" class="form-control input-mask-price">
                                        <div class="valid-feedback feedback-icon">
                                            <i class="fa fa-check"></i>
                                        </div>
                                        <div class="invalid-feedback feedback-icon">
                                            <i class="fa fa-times"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-12">
                                        <label for="exampleFormControlFile1">Payment Proof <small>(Please upload payment receipt.)</small></label>
                                        <input type="file" name="payment_proof" id="payment_proof" class="form-control-file @error('payment_proof') is-invalid @enderror">
                                        @error('payment_proof')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                        <small style="display: none;" id="no-file-error" class="form-text text-danger">Please upload payment proof.</small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row mt-2">
                                <div class="col-12" style="padding-left: .7rem;">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="credit-debit-agree">
                                        <label class="custom-control-label" for="credit-debit-agree">
                                            I have read, understand and accepted
                                            the <a href="" data-toggle="modal" data-target="#purchaseAgreementModal">terms and conditions</a>.
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <!--
                                <div class="col-6">
                                    <button type="button" id="offline-payment-button" class="btn btn-secondary text-right">Offline Payment</button>
                                    <button type="button" id="card-payment-button" class="btn btn-secondary text-right" style="display: none;">Debit/Credit Payment</button>
                                </div>
                                -->
                                <div class="col-12 text-right">
                                    <!-- Submit button -->
                                    <input type="hidden" name="paymentType" value="offline" id="paymentType">
                                    <input type="hidden" name="purchaseNumber" value="{{ $invoiceNo }}">
                                    <button type="submit" id="pay-now-button" class="btn bjsh-btn-gradient text-right font-weight-bold" disabled>Sign Up</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="purchaseAgreementModal" tabindex="-1" role="dialog" aria-labelledby="purchaseAgreementModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-12 mb-0">
                        <div class="overflow-auto" style="max-height: 70vh; background-color: #ffffff; border: 2px solid #e6e6e6; padding: 0.75rem;">
                             @if (country()->country_id == 'MY')
                                @include('shop.payment.service-terms-conditions-my')
                            @else
                                @include('shop.payment.service-terms-conditions-sg')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="tryAgainNotification" tabindex="-1" role="dialog" aria-labelledby="tryAgainNotificationLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if(!empty($tryAgain))
                <p>
                    {{ $tryAgain['reason'] }}
                    <br>
                    <br>
                    {{ $tryAgain['message'] }}
                </p>
                @endif
                We're sorry for the inconvenience.
                <br>
                For further assistance, send an email to <a class="font-weight-bold" href="mailto:{{country()->company_email}}">{{country()->company_email}}</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    @if(!empty($tryAgain))
    $('#tryAgainNotification').modal('show');
    @endif

    let paymentType = $('#paymentType');

    const paymentCardDiv = $('#paymentCardDiv');
    const paymentOfflineDiv = $('#paymentOfflineDiv');

    $('#offline-payment-button').on('click', function() {
        paymentCardDiv.hide();
        paymentOfflineDiv.show();
        $('#offline-payment-button').hide();
        $('#card-payment-button').show();
        paymentType.val('offline');
    });

    $('#card-payment-button').on('click', function() {
        paymentOfflineDiv.hide();
        paymentCardDiv.show();
        $('#card-payment-button').hide();
        $('#offline-payment-button').show();
        paymentType.val('card');
    });

    let priceSelectors = document.getElementsByClassName('input-mask-price');

    let priceInputMask = new Inputmask({
        'mask': '99999.99',
        'numericInput': true,
        'digits': 2,
        'digitsOptional': false,
        'placeholder': '0'
    });

    for (var i = 0; i < priceSelectors.length; i++) {
        priceInputMask.mask(priceSelectors.item(i));
    }

    // Credit/Debit Card JS
    // Credit card pattern recognition algorithm.
    function detectCardType(number) {
        var re = {
            // electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            // maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            // dankort: /^(5019)\d+$/,
            // interpayment: /^(636)\d+$/,
            // unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/,
            // amex: /^3[47][0-9]{13}$/,
            // diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            // discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            // jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        }

        for (var key in re) {
            if (re[key].test(number)) {
                return key
            }
        }
    }


    // Form Validation - Credit/Debit Card.
    let cardNumber = null;
    let ccIcon = $('.valid-feedback.feedback-icon.with-cc-icon i');

    $('#card_number').on('keyup', function() {
        cardNumber = $(this).val();
        cardType = detectCardType(cardNumber);

        if ($(this).val().length < 7 || cardType == undefined) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
            ccIcon.removeClass();
            ccIcon.addClass('fa fa-cc-' + cardType).css('font-size', '30px');
            $('#card_type').val(cardType);
        }
    });

    $('#expiry_date').on('keyup', function() {
        if ($(this).val().length != 4 || !$.isNumeric($(this).val())) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#cvv').on('keyup', function() {
        if ($(this).val().length < 3 || !$.isNumeric($(this).val())) {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    // End Form Validation - Credit/Debit Card
    // End Credit/Debit Card JS

    // Form Validation - Offline Payment
    $('#offline_reference').on('keyup', function() {
        if ($(this).val().length == 0 || $(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });

    $('#offline_amount').on('keyup', function() {
        if ($(this).val() == '') {
            $(this).removeClass('is-valid');
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            $(this).addClass('is-valid');
        }
    });
    // End Form Validation - Offline Payment

    let currentTab = $('.nav-tabs > .active');
    let nextTab = currentTab.next('li');

    // Handles tabs click.
    $('.nav-link').click(function() {
        currentTab = $(this).parent();
        $('.nav-tabs > .active').removeClass('active');
        currentTab.addClass('active');
        nextTab = currentTab.next('li');
    });

    // Validate form before submit.
    $('#register-form').on('submit', function(e) {
        let error = 0;

        if (paymentType.val() == 'card') {

            if (
                $('#card_number').val().length < 7 ||
                cardType == undefined
            ) {
                error = error + 1;
                $('#card_number').addClass('is-invalid');
                $('#card_number').focus();
            }

            if ($('#expiry_date').val().length != 4 || !$.isNumeric($('#expiry_date').val())) {
                error = error + 1;
                $('#expiry_date').addClass('is-invalid');
                $('#expiry_date').focus();
            }

            if ($('#cvv').val().length < 3 || !$.isNumeric($('#cvv').val())) {
                error = error + 1;
                $('#cvv').addClass('is-invalid');
                $('#cvv').focus();
            }
        }

        if (paymentType.val() == 'offline') {
            if ($('#offline_reference').val() == '' || $('#offline_reference').val().length == 0) {
                error = error + 1
                $('#offline_reference').addClass('is-invalid');
            }

            if ($('#offline_amount').val() == '') {
                errror = error + 1;
                $('#offline_amount').addClass('is-invalid');
            }

            if ($('#payment_proof').get(0).files.length == 0) {
                error = error + 1;
                $('#no-file-error').show();
            }
        }

        if (error == 0) {
            return true;
        } else {
            return false;
        }
    });
    // End Form Validation

    $('#credit-debit-agree').on('change', function() {
        if ($('#credit-debit-agree').prop('checked', true)) {
            $('#pay-now-button').prop('disabled', false);
        } else {
            $('#pay-now-button').prop('disabled', true);
        }
    });
</script>
@endpush

@push('style')
<style>
    .valid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .valid-feedback.feedback-icon.with-cc-icon {
        position: absolute;
        width: auto;
        bottom: 3px;
        right: 10px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon {
        position: absolute;
        width: auto;
        bottom: 10px;
        right: 15px;
        margin-top: 0;
    }

    .invalid-feedback.feedback-icon.with-helper {
        position: absolute;
        width: auto;
        bottom: 32px;
        right: 15px;
        margin-top: 0;
    }

    .error {
        color: red;
    }
</style>
@endpush
