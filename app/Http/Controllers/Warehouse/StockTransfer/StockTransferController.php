<?php

namespace App\Http\Controllers\Warehouse\StockTransfer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator\Warehouse\InventoryController;
use App\Models\Globals\City;
use App\Models\Globals\Countries;
use App\Models\Globals\Outlet;
use App\Models\Globals\State;
use App\Models\Warehouse\Location\Wh_location;
use App\Models\Warehouse\Order\WarehouseOrder;
use App\Models\Warehouse\StockTransfer\StockTransfer;
use App\Models\Warehouse\StockTransfer\StockTransferItem;
use Illuminate\Support\Facades\Auth;
use App\Models\Users\User;
use Illuminate\Support\Carbon;
use App\Models\Products\WarehouseInventories;
use App\Models\Warehouse\Box\WarehouseBox;

class StockTransferController extends Controller
{
    public static $home;
    public static $header;
    public static $transferStart;

    public static $headerTransferList;



    public static $create;
    public static $store;

    public static $edit;
    public static $update;

    public static $destroy;

    public function __construct()
    {
        // self::$home = 'warehouse.location.index';
        self::$header = 'Stock Transfer';

        self::$transferStart = 'warehouse.StockTransfer.transferStart';

        self::$headerTransferList = 'Stock Transfer';


        // self::$create = 'warehouse.location.create';
        // self::$store = 'warehouse.location.store';

        // self::$edit = 'warehouse.location.edit';
        // self::$update = 'warehouse.location.update';

        // self::$destroy = 'warehouse.location.destroy';

        // link to view page
        // view('warehouse.location.index');
    }

    public function createBox()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $location = wh_location::whereIn('id', $userLocationId)->first();

        // if ($user = Auth::user() && !$user->hasRole('wh_supervisor')) $locations = $locations->whereIn('id', $userLocationId);

        return view('warehouse.StockTransfer.createBox', compact('location'));
    }

    public function printLabel(Request $request)
    {
        $boxId = 0;
        $user = User::find(Auth::user()->id);

        if ($request->method == 'create') {
            $warehouseBox = WarehouseBox::where('box_name', $request->box_name)->first();

            if ($warehouseBox != NULL) {

                return back()->with('error', 'Box name repeated!');
            } else {

                $warehouseBox = new WarehouseBox;

                $warehouseBox->type = 'wh_transfer';
                $warehouseBox->box_name = $request->box_name;
                $warehouseBox->location_id = $request->location_id;
                $warehouseBox->notes = $request->notes;
                $warehouseBox->pic_1 = $user->id;

                $warehouseBox->save();

                $boxId = $warehouseBox->id;
            }
        } elseif ($request->method == 'reprint') {
            $boxId = $request->id;
        }

        $boxInfo = WarehouseBox::find($boxId);

        $collection = collect($request)->forget(['_token', 'submit'])->toArray();

        $detail = base64_encode(\json_encode($collection));

        return view('documents.warehouse.box-info')
            ->with('detail', $detail)
            ->with('request', $request)
            ->with('boxInfo', $boxInfo)
            ->with('pageTitle', $request->box_name)
            ->with('notes', $request->notes);
    }

    public function boxList(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $tables = WarehouseBox::orderBy('id', 'desc');

        if ($user = Auth::user() && !$user->hasRole('wh_supervisor'))  $tables = $tables->whereIn('location_id', $userLocationId);
        $tables = $tables
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->locationFilter($request->location);

        $tables = $tables->paginate(10);
        $tables->appends(request()->query());


        $tranfer = array();

        $locations = wh_location::get();


        return view('warehouse.StockTransfer.boxList', compact('request', 'tables', 'tranfer', 'locations'));
    }

    public function editBoxNotes(Request $request)
    {
        $batch = WarehouseBox::where('id', $request->batchID)->first();
        $batch->notes = $request->notes;
        $batch->save();

        return true;
    }


    public function boxQrScanned($boxId)
    {
        $boxInfo = WarehouseBox::where('id', $boxId)->first();
        if ($boxInfo == NULL) return back()->with('error', 'Wrong QR scanned, no this Box!');

        if ($user = Auth::user()) {

            $userLocationId = $user->user_warehouse_location;
            if (empty($boxInfo->pic_2)) {
                if ($user->hasRole('wh_transferrer')) {
                    if (in_array($boxInfo->location_id, $userLocationId)) {

                        if ($boxInfo->using != 0) return back()->with('error', 'This box used before, please use another box to transfer!');

                        return redirect(route('warehouse.StockTransfer.index', ['boxId' => $boxId]))->with('success', 'Correct box, please select transfer information');
                    } else {
                        if (!empty($boxInfo->using)) {
                            return back()->with('error', 'This box belong to location : ' . $boxInfo->location->location_name);
                        }
                    }
                } elseif ($user->hasRole('wh_receiver')) {
                    if (isset($boxInfo->stockTransfer) && in_array($boxInfo->stockTransfer->to_location_id, $userLocationId)) {

                        if (isset($boxInfo->stockTransfer)) {
                            $stockTransferId = $boxInfo->stockTransfer->id;
                            return redirect(route('warehouse.StockTransfer.receiveStart', ['stockTransferId' => $stockTransferId]));
                        }
                    } else {
                        if (!empty($boxInfo->using)) {
                            return back()->with('error', 'This box receive belong to location : ' . $boxInfo->location->location_name);
                        }
                    }
                }
            } else {
                return back()->with('error', 'Box location : ' . $boxInfo->location->location_name . ' ,This box receiving by : ' . $boxInfo->theUser2->userInfo->full_name);
            }
        }

        if (isset($boxInfo->stockTransfer)) {
            $stockTransferCheck = StockTransfer::find($boxInfo->stockTransfer->id);

            return view('warehouse.StockTransfer.scanned-box-info', compact('stockTransferCheck'));
        }


        return back()->with('error', 'Box empty or not completed');
    }

    public function index($boxId = NULL)
    {
        if ($boxId != NULL) {
            $boxInfo = WarehouseBox::find($boxId);
        } else {
            $boxInfo = NULL;
        }

        // $tables = StockTransfer::orderBy('id', 'ASC')->paginate(10);

        $user = User::find(Auth::user()->id);

        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;
        // $fromLocations = wh_location::whereIn('id', $userLocationId)->get();
        $fromLocations = wh_location::orderBy('location_name', 'desc');

        if (in_array(1, $userLocationId)) {
            $fromLocations = $fromLocations->orWhere('id', 999)->orWhereIn('id', $userLocationId)->get();
        } else {
            $fromLocations = $fromLocations->whereIn('id', $userLocationId)->get();
        }


        // $toLocations = Wh_location::whereNotIn('id', $userLocationId)->whereNotIn('location_name', ['DUMMY'])->get();
        $toLocations = wh_location::orderBy('location_name', 'desc');

        if (in_array(1, $userLocationId)) {
            // $toLocations = $toLocations->whereNotIn('id', [999])->whereNotIn('id', $userLocationId)->get();
            $toLocations = $toLocations->get();
        } else {
            $toLocations = $toLocations->whereNotIn('location_name', ['DUMMY'])->whereNotIn('id', $userLocationId)->get();
        }


        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user', $user->id)
            // ->whereNULL('session_end_time')
            ->whereIn('status', [1])
            ->first();

        if ($stockTransferCheck != NULL) {
            return view('warehouse.StockTransfer.scanProduct', compact('stockTransferCheck'));
        } else {
            return view('warehouse.StockTransfer.index', compact('tables', 'user', 'fromLocations', 'toLocations', 'boxInfo'));
        }
    }

    public function transferStart(Request $request)
    {
        $user = User::find(Auth::user()->id);

        //1th Oct 2021 only
        // if ($request->input('from_location_id') != 999 && $request->input('from_location_id')  != 2) {
        //     return back()->with('error', 'DUMMY to Retail Shop only!');
        // }

        $request->validate([
            'from_location_id' => ['required'],
            'to_location_id' => ['required'],
            'transfer_datetime' => ['required'],
            'type' => ['required'],
        ]);
        if ($request->boxId == NULL) {
            $request->validate([
                'type' => ['required'],
            ]);
        }

        if ($request->input('from_location_id') == $request->input('to_location_id')) {
            return back()->with('error', 'Please transfer  different location!');
        }

        $stockTransfer = new StockTransfer;
        $stockTransfer->user = $user->id;
        $stockTransfer->session_start_time = Carbon::now();
        $stockTransfer->from_location_id = $request->input('from_location_id');
        $stockTransfer->to_location_id = $request->input('to_location_id');
        $stockTransfer->transfer_datetime = $request->input('transfer_datetime');
        $stockTransfer->type = $request->input('type');
        $stockTransfer->status = 1;
        $stockTransfer->save();

        if ($request->boxId != NULL) {
            $boxInfo = WarehouseBox::find($request->boxId);
            $boxInfo->using = $stockTransfer->id;
            $boxInfo->save();
        }
        return redirect()->route('warehouse.StockTransfer.scanProduct')->with('success', 'Start Scan Product want to transfer');
    }

    public function scanProduct()
    {
        $user = User::find(Auth::user()->id);

        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user', $user->id)
            // ->join('wh_transfer_item', 'wh_transfer_item.key', '=', 'wh_transfer.id')
            // ->whereNULL('session_end_time')
            ->whereIn('status', [1])
            ->first();
        return view('warehouse.StockTransfer.scanProduct', compact('stockTransferCheck'));
    }

    public function transferEnd(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user', $user->id)
            ->join('wh_transfer_item', 'wh_transfer_item.key', '=', 'wh_transfer.id')
            ->where('wh_transfer.id', $request->stockTransferId)
            // ->whereNULL('session_end_time')
            ->whereIn('status', [1])
            ->first();

        if ($stockTransferCheck == NULL) {
            return back()->with('error', 'Please scan product!');
        }

        foreach ($stockTransferCheck->stockTransferItems as $key => $item) {
            // echo $item->batchItem->batchDetail->product_code;
            // echo '<br>';

            if (!isset($data[$item->batchItem->batchDetail->product_code])) {
                $data[$item->batchItem->batchDetail->product_code] = array();
                // $data[$item->batchItem->batchDetail->product_code]['product_code'] = $item->batchItem->batchDetail->product_code;
                $data[$item->batchItem->batchDetail->product_code]['qty'] = 0;
            }

            $data[$item->batchItem->batchDetail->product_code]['qty']++;

            //update batch item location id
            $item->batchItem->location_id = $stockTransferCheck->to_location_id;
            $item->batchItem->save();
        }

        foreach ($data as $productCode => $value) {

            //Plus (IN) ,  to location
            $inventoryRecords[$productCode]['outlet_id'] = $stockTransferCheck->to_location_id;
            $inventoryRecords[$productCode]['inv_type'] = ($stockTransferCheck->type != 0) ?  8021 : 8013;
            $inventoryRecords[$productCode]['inv_amount'] = $value['qty'];
            $inventoryRecords[$productCode]['inv_user'] = 0;
            $inventoryRecords[$productCode]['model_type'] = StockTransfer::class;;
            $inventoryRecords[$productCode]['model_id'] = $stockTransferCheck->id;
            $inventoryRecords[$productCode]['product_code'] = $productCode;

            //Minus (OUT),  from location
            $inventoryRecordsMinus[$productCode]['outlet_id'] = $stockTransferCheck->from_location_id;
            $inventoryRecordsMinus[$productCode]['inv_type'] = ($stockTransferCheck->type != 0) ?  8021 : 8014;
            $inventoryRecordsMinus[$productCode]['inv_amount'] = - ($value['qty']);
            $inventoryRecordsMinus[$productCode]['inv_user'] = 0;
            $inventoryRecordsMinus[$productCode]['model_type'] = StockTransfer::class;;
            $inventoryRecordsMinus[$productCode]['model_id'] = $stockTransferCheck->id;
            $inventoryRecordsMinus[$productCode]['product_code'] = $productCode;
        }

        InventoryController::insertData($inventoryRecords);
        InventoryController::insertData($inventoryRecordsMinus);

        if ($stockTransferCheck->type != 0) {
            //Correct Way
            $stockTransferCheck->status = 3;
        } else {
            //auto
            $stockTransferCheck->session_end_time = Carbon::now();
            $stockTransferCheck->status = 5;
        }
        $stockTransferCheck->save();

        return redirect()->route('warehouse.StockTransfer.index')->with('success', 'Stock transfer completed!');
    }

    public function transferCancel(Request $request, $id)
    {
        $request->validate([
            'cancel_notes' => ['required'],
        ]);

        $stockTransferCheck = StockTransfer::find($id);

        $stockTransferCheck->session_end_time = Carbon::now();
        $stockTransferCheck->cancel_notes = $request->cancel_notes;
        $stockTransferCheck->status = -1;

        $stockTransferCheck->save();

        $stockTransferCheck->transferBox->using = 0;
        $stockTransferCheck->transferBox->save();

        return redirect()->route('warehouse.StockTransfer.index')->with('success', 'Stock transfer Canceled! ID : ' . $stockTransferCheck->id);
    }

    public function stockTransferList(Request $request)
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $tables = StockTransfer::orderBy('id', 'desc');

        if (!$user->hasRole('wh_supervisor')) {
            // dd(4, $tables->get());
            $tables = $tables->orWhereIn('from_location_id', $userLocationId)->orWhereIn('to_location_id', $userLocationId);
        }

        $tables = $tables
            ->typeFilter($request->transfer_type)
            ->statusFilter($request->transfer_status)

            ->fromLocationFilter($request->from_location)
            ->toLocationFilter($request->to_location);

        $locations = wh_location::get();
        if (!$user->hasRole('wh_supervisor')) $locations = $locations->whereIn('id', $userLocationId);

        $toLocations = wh_location::get();


        $tables = $tables->paginate(10);
        $tables->appends(request()->query());

        $tranfer = array();

        return view('warehouse.StockTransfer.transferList', compact('request', 'tables', 'tranfer', 'locations', 'toLocations'));
    }

    public function editNotes(Request $request)
    {
        $batch = StockTransfer::where('id', $request->batchID)->first();
        $batch->notes = $request->notes;
        $batch->save();

        return true;
    }

    public function receive()
    {
        $user = Auth::user();
        $userLocationId = $user->user_warehouse_location;

        $tables = StockTransfer::orderBy('id', 'ASC')
            ->where('status', 3)
            // ->whereNotIn('type', [2])
            ->whereIn('to_location_id', $userLocationId)
            ->paginate(500);

        $user = User::find(Auth::user()->id);
        $locations = Wh_location::get();

        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user_receive', $user->id)
            ->where('status', 4)
            ->first();

        if ($stockTransferCheck != NULL) {

            return view('warehouse.StockTransfer.receiveScanProduct', compact('stockTransferCheck'));
        } else {
            return view('warehouse.StockTransfer.receive', compact('tables', 'user', 'locations'));
        }
    }

    public function receiveStart(Request $request)
    {
        if ($request->stockTransferId == null) return back()->with('error', 'Please select!');
        $user = User::find(Auth::user()->id);

        $stockTransfer = StockTransfer::where('id', $request->stockTransferId)->where('status', 3)->first();
        $stockTransfer->user_receive = $user->id;
        $stockTransfer->status = 4;
        $stockTransfer->save();

        $stockTransfer->transferBox->pic_2 = $user->id;
        $stockTransfer->transferBox->save();

        if ($stockTransfer->type == 2) {
            return redirect()->route('warehouse.StockTransfer.receiveBatch')->with('success', 'Start count Product and key in quantity.');
        } else {
            return redirect()->route('warehouse.StockTransfer.receiveScanProduct')->with('success', 'Start Scan Product.');
        }
    }

    public function receiveScanProduct()
    {
        $user = User::find(Auth::user()->id);

        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user_receive', $user->id)
            ->join('wh_transfer_item', 'wh_transfer_item.key', '=', 'wh_transfer.id')
            ->where('wh_transfer.status', 4)
            ->first();

        return view('warehouse.StockTransfer.receiveScanProduct', compact('stockTransferCheck'));
    }

    public function receiveBatch()
    {
        $user = User::find(Auth::user()->id);

        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user_receive', $user->id)
            ->join('wh_transfer_item', 'wh_transfer_item.key', '=', 'wh_transfer.id')
            ->where('wh_transfer.status', 4)
            ->first();

        return view('warehouse.StockTransfer.receiveBatch', compact('stockTransferCheck'));
    }

    public function receiveBatchChecking(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $stockTransferCheck = StockTransfer::selectRaw('wh_transfer.*')
            ->where('user_receive', $user->id)
            ->join('wh_transfer_item', 'wh_transfer_item.key', '=', 'wh_transfer.id')
            ->where('wh_transfer.status', 4)
            ->first();

        foreach ($stockTransferCheck->stockTransferItems as $row) {

            if (!isset($array[$row->batchItem->batchDetail->product_code])) {
                $array[$row->batchItem->batchDetail->product_code]['qty'] = 0;
            }

            $array[$row->batchItem->batchDetail->product_code]['qty']++;
        }

        foreach ($request->input('received_quantity') as $product_code => $received_quantity) {

            if ($array[$product_code]['qty'] != (int)$received_quantity) {
                return back()->with('error', 'Wrong received quantity! Please count again or inform PIC');
            }
        }

        $stockTransferCheck->status = 5;
        $stockTransferCheck->session_end_time = Carbon::now();
        $stockTransferCheck->save();

        $stockTransferCheck->transferBox->pic_2 = $user->id;
        $stockTransferCheck->transferBox->save();

        StockTransferController::receiveDone($stockTransferCheck->id);

        return redirect(route('warehouse.StockTransfer.receive'))->with('success', 'Completed receving, correct received quantity');
    }

    public static function receiveDone($stockTransferId)
    {
        $stockTransfer = StockTransfer::find($stockTransferId);

        $inventoryIn = WarehouseInventories::where('model_type', StockTransfer::class)
            ->where('model_id', $stockTransferId)
            ->where('outlet_id', $stockTransfer->to_location_id)
            ->get();

        $inventoryOut = WarehouseInventories::where('model_type', StockTransfer::class)
            ->where('model_id', $stockTransferId)
            ->where('outlet_id', $stockTransfer->from_location_id)
            ->get();

        foreach ($inventoryIn as $in) {
            $in->inv_type = 8013;
            $in->save();
        }

        foreach ($inventoryOut as $out) {
            $out->inv_type = 8014;
            $out->save();
        }

        return true;
    }
}
