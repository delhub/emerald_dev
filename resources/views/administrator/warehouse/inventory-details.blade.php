@extends('layouts.administrator.main')

@section('breadcrumbs')
@endsection

@section('content')
    <h1 class="my-4">{{ $global_products ? $global_products->product_code : '' }} -
        {{ $global_products ? $global_products->global_product_name : '' }}
        ({{ $global_products ? $global_products->attribute_name : '' }})</h1>
    <h5> Inventory total = {{ $inv_total }} </h5>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="card shadow-sm">
        <div class="card-header container-fluid">
            <div class="row my-3">
                <div class="col-md-6">
                    <strong></strong>
                </div>
                <div class="col-md-1 ml-auto">
                    <a class="btn btn-info" href="{{ route('administrator.warehouse.inventory.index') }}">Back</a>
                </div>
            </div>
        </div>
        <div class="card-body container-fluid">
            <div class="row my-4">
                <div class="col-md-8">
                    <form
                        action="{{ route('administrator.warehouse.inventory.details', ['product_code' => $global_products->product_code]) }}"
                        method="get" class="row">
                        <div class="col-md-3">
                            <select class="custom-select" name="inventory_outlet" id="inventory_outlet">
                                <option value="0" selected disabled>Filter Outlet...</option>
                                @foreach ($outlets as $outlet)
                                    <option {{ $request->inventory_outlet == $outlet->id ? 'selected' : '' }}
                                        value="{{ $outlet->id }}">{{ $outlet->location_name }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Please select inventory type.
                            </div>
                        </div>
                        <div class="col-md-3">
                            <select class="custom-select" name="inventory_status" id="inventory_status">
                                <option value="0" selected disabled>Filter inventory status...</option>
                                @foreach ($statuses->where('id', '>=', 8000) as $status)
                                    <option {{ $request->inventory_status == $status->id ? 'selected' : '' }}
                                        value="{{ $status->id }}">{{ $status->name }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Please select inventory type.
                            </div>
                        </div>
                        <button class="btn btn-info text-light" type="submit">Filter</button>
                        <a class="btn btn-info text-light rounded mx-md-2"
                            href="{{ route('administrator.warehouse.inventory.details', ['product_code' => $global_products->product_code]) }}">Reset</a>
                    </form>
                </div>
                {{ $wh_inventories->links() }}
            </div>

            <div class="row">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Inventory Type</th>
                            {{-- <th scope="col">Inventory Status</th> --}}
                            <th scope="col">Inventory Amount</th>
                            <th scope="col">Reference Number</th>
                            <th scope="col">Outlet</th>
                            <th scope="col">Created Date</th>
                            <th scope="col">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($wh_inventories as $wh_inventory)
                            <tr>
                                <th scope="row">
                                    {{ $loop->iteration }}
                                    {{-- {{ ($wh_inventories->currentpage()-1) * $wh_inventories ->perpage() + $loop->index + 1 }} --}}
                                </th>
                                <td>{{ $statuses->where('id', $wh_inventory->inv_type)->first()? $statuses->where('id', $wh_inventory->inv_type)->first()->name: '' }}
                                </td>
                                <td
                                    class="{{ in_array($wh_inventory->inv_type, ['stock_in', 'transfer_in']) }}
                    {{ in_array($wh_inventory->inv_type, ['outlet_sale', 'transfer_out', 'return', 'damaged']) }}">
                                    {{ $wh_inventory->inv_amount }}</td>
                                <td>{{ $wh_inventory->model }}</td>
                                <td>{{ $outlets->where('id', $wh_inventory->outlet_id)->first()? $outlets->where('id', $wh_inventory->outlet_id)->first()->location_name: '-' }}
                                </td>
                                <td>{{ $wh_inventory->created_at->format('d/m/Y') }}</td>
                                <td>
                                    <a class="btn btn-info text-light rounded mx-md-2"
                                        onclick="showRow({{ $wh_inventory->id }})">Details</a>
                                </td>
                            </tr>
                            <tr id="row_{{ $wh_inventory->id }}" class="d-none bg-light">
                                <td colspan="6">
                                    <table>
                                        @php
                                            $inventoryDetails = $wh_inventory->inventoryDetails;
                                            // dd($inventoryDetails->batch_id);
                                        @endphp
                                        <tr>
                                            {{-- Order Table --}}
                                            {!! $inventoryDetails->purchase ? '<td>Invoice Number</td>' : '' !!}
                                            {!! $inventoryDetails->delivery_order ? '<td>Delivery Order</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>Order Status</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>Delivery Method</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>Tracking Number</td>' : '' !!}
                                            {!! $inventoryDetails->order && $inventoryDetails->order->tracking_number ? '<td>Shipping Date</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>Collect Date</td>' : '' !!}
                                            {!! $inventoryDetails->date ? '<td>Order Date</td>' : '' !!}
                                            {!! $inventoryDetails->batchItems && $inventoryDetails->type == 'pos' ? '<td>Batch Item</td>' : '' !!}


                                            {{-- Batch Table --}}
                                            {!! $inventoryDetails->batch_id ? '<td>Batch ID</td>' : '' !!}
                                            {!! $inventoryDetails->mainBatch ? ($inventoryDetails->mainBatch->po_number ? '<td>PO Number</td>' : '') : '' !!}
                                            {!! $inventoryDetails->expiry_date ? '<td>Expiry Date</td>' : '' !!}
                                            {!! $inventoryDetails->mainBatch ? ($inventoryDetails->mainBatch->stock_in_date ? '<td>Sock In Date</td>' : '') : '' !!}

                                            {{-- Stock Trasfer --}}
                                            {!! $inventoryDetails->transfer_datetime ? '<td>Stock Transfer ID</td>' : '' !!}
                                            {!! $inventoryDetails->transfer_datetime ? '<td>Stock Transfer Date</td>' : '' !!}
                                            {!! $inventoryDetails->from_location_id ? '<td>From Location</td>' : '' !!}
                                            {!! $inventoryDetails->to_location_id ? '<td>To Location</td>' : '' !!}
                                        </tr>
                                        <tr>
                                            {{-- Order Table --}}
                                            {!! $inventoryDetails->purchase ? '<td>' . $inventoryDetails->purchase['purchase_number'] . '</td>' : '' !!}
                                            {!! $inventoryDetails->delivery_order ? '<td>' . $inventoryDetails->delivery_order . '</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>' . $statuses->where('id', $inventoryDetails->order->order_status)->first()->name . '</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>' . $inventoryDetails->order->delivery_method . '</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>' . $inventoryDetails->order->tracking_number . '</td>' : '' !!}
                                            {!! $inventoryDetails->order && $inventoryDetails->order->tracking_number ? '<td>' . $inventoryDetails->order->ordershipping_date . '</td>' : '' !!}
                                            {!! $inventoryDetails->order ? '<td>' . $inventoryDetails->order->collected_date . '</td>' : '' !!}
                                            {!! $inventoryDetails->date ? '<td>' . date('d/m/Y', strtotime($inventoryDetails->date)) . '</td>' : '' !!}
                                            @if ($inventoryDetails->batchItems && $inventoryDetails->type == 'pos')
                                                <td>
                                                    @foreach ($inventoryDetails->batchItems as $key => $item)
                                                        {{ $item->items_id }}
                                                        @if (!$loop->last)
                                                            ,
                                                        @endif
                                                    @endforeach
                                                </td>
                                            @endif

                                            {{-- Batch Table --}}
                                            {!! $inventoryDetails->batch_id ? '<td>' . $inventoryDetails->batch_id . '</td>' : '' !!}
                                            {!! $inventoryDetails->mainBatch ? ($inventoryDetails->mainBatch->po_number ? '<td>' . $inventoryDetails->mainBatch->po_number . '</td>' : '') : '' !!}
                                            {!! $inventoryDetails->expiry_date ? '<td>' . date('d/m/Y', strtotime($inventoryDetails->expiry_date)) . '</td>' : '' !!}
                                            {!! $inventoryDetails->mainBatch ? '<td>' . date('d/m/Y', strtotime($inventoryDetails->mainBatch->stock_in_date)) . '</td>' : '' !!}

                                            {{-- Stock Transfer --}}
                                            {!! $inventoryDetails->transfer_datetime ? '<td>' . $inventoryDetails->id . '</td>' : '' !!}
                                            {!! $inventoryDetails->transfer_datetime ? '<td>' . $inventoryDetails->transfer_datetime . '</td>' : '' !!}
                                            {!! $inventoryDetails->from_location_id ? '<td>' . showLocationName($inventoryDetails->from_location_id) . '</td>' : '' !!}
                                            {!! $inventoryDetails->to_location_id ? '<td>' . showLocationName($inventoryDetails->to_location_id) . '</td>' : '' !!}
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $wh_inventories->links() }}

            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        function showRow(inventory_id) {
            var row = $('#row_' + inventory_id);

            if ($(row).hasClass('d-none')) {
                $(row).removeClass('d-none');
            } else {
                $(row).addClass('d-none');
            }
            console.log(row);
        }

        function submit_inventory() {
            var inv_amount = $('#inv_amount');
            var outlet_id = $('#outlet_id');
            var inv_type = $('#inv_type');
            var error = 0;

            if ($(inv_amount).val() === '') {
                inv_amount.addClass('is-invalid');
                error = 1;
            } else {
                inv_amount.removeClass('is-invalid');
            }

            if ($(outlet_id).val() === null) {
                outlet_id.addClass('is-invalid');
                error = 1;
            } else {
                outlet_id.removeClass('is-invalid');
            }

            if ($(inv_type).val() === null) {
                inv_type.addClass('is-invalid');
                error = 1;
            } else {
                inv_type.removeClass('is-invalid');
            }

            if (error !== 1) {
                $('#inventory_update').submit();
            }
        }
    </script>
@endpush
