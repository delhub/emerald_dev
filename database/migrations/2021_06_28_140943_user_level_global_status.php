<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserLevelGlobalStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('global_statuses')->insert(
            array(
                [
                    'id' => '6000',
                    'name' => 'Normal',
                    'description' => '{"amount":0,"range_yearly":1,"maintenance_fee":0,"discount":0}',
                    'status_type' => 'user_level',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
                [
                    'id' => '6001',
                    'name' => 'Full Advance',
                    'description' => '{"amount":500,"range_yearly":1,"maintenance_fee":699,"discount":5}',
                    'status_type' => 'user_level',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
                [
                    'id' => '6002',
                    'name' => 'Premium',
                    'description' => '{"amount":1000,"range_yearly":3,"maintenance_fee":799,"discount":10}',
                    'status_type' => 'user_level',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('global_statuses')->whereIn('id',[6000,6001,6002])->delete();
    }
}
