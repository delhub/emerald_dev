@extends('layouts.management.main-agent')

@section('content')

@section('page_title')
    {{ 'Agent - Membership Registration' }}
@endsection
@if (isset($successful_message))
    <div class="alert alert-success">
        {{ $successful_message }}
    </div>
@endif

<div class="fml-container pb-3">
    <div class="row">
        <h4>Confirm Redemption
            <a href="{{ route('agent.virtual.quantity.stock') }}" style=" float: right;width:7%;"
                class="buy-btn link-product">
                Back</a>
        </h4>
    </div>
    <div class="card">
        <div class="card-body">
            @foreach ($selectProduct as $productCode => $value)
                {{ $loop->iteration }})
                @if ($itemTable->where('id', $value['item_id'])->first()->product->parentProduct->defaultImage)
                    <img style="width: 15%;margin: 10px 8px;"
                        src="{{ asset('storage/' .$itemTable->where('id', $value['item_id'])->first()->product->parentProduct->images[0]->path .$itemTable->where('id', $value['item_id'])->first()->product->parentProduct->images[0]->filename) }}" />
                @else
                    <img class="responsive-img p-1" src="" alt="Product Image" />
                @endif

                {{ $productAttribute->where('product_code', $productCode)->first()->product2->parentProduct->name }}
                <b> X {{ $value['quantity'] }}</b>
                <div style="text-align: right">
                    RM : {{ number_format(($value['subtotal_price']/100),2) }}
                </div>

                <br>
            @endforeach
        </div>
    </div>



    <p style="text-align: right;font-size:20px"> Total Amount : RM {{ number_format(($totalAmount/100),2) }}</p>

    <div style=" float: right;">
        <form id="bulkactionform" action={{ route('agent.vqs.createDealerPreBuyRedemption') }} method="POST">
            <div class="row mt-2 ml-2">
                <div class="col-md-auto px-1">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="action_ids" id="action_ids" value="{{ implode(',', $arrayID) }}">

                    <div class="input-group mb-1">
                        <div class="input-group-append">
                            <input type="hidden" name="goSummaryPage" value="ohNo">
                            <input type="hidden" name="create" value="yeah">
                            <input type="submit" class="buy-btn link-product" value="Submit" id="btnid">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>



@endsection
