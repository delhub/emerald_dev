@extends('layouts.management.main-agent')

@section('content')
@section('page_title')
    {{ 'Agent - Membership Registration' }}
@endsection
@if (Session::has('successful_message'))
    <div class="alert alert-success">
        {{ Session::get('successful_message') }}
    </div>
@endif
@if (Session::has('error_message'))
    <div class="alert alert-danger">
        {{ Session::get('error_message') }}
    </div>
@endif

<div class="fml-container pb-3">
    <div class="row">
        <div>
            <h4>Virtual Quantity Stock (VQS)</h4>
        </div>
    </div>
</div>
<div class="container-fluid p-0">
    <div class="row mb-4">
        <div class="col-md-12 col-12">
            <div class="row">
                @if ($specialAgent == null || $specialAgent->isEmpty() || $specialAgent == '0')

                            </div>
                            <div class="card-body row">
                                <div class="col-auto mx-auto text-center">
                                    <h5>This page is available for <i>Virtual Quantity Stock</i> participants only.</h5>
                                    <p>

                                        Please contact your respective manager for details of participating in the
                                        <i>Virtual Quantity Stock</i>.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                <div class="col-lg-12 col-md-12 col-auto ml-auto">
                    <div class="row">
                        <div class="col-md-3 col-12 d-flex justify-content-end">
                            {{ $taskLists->links() }}
                        </div>
                        <div class="col-md-7 col-12 d-flex align-items-end">

                            <ul class="nav nav-pills d-sm-flex " id="archimedesTab" role="tablist">
                                <li class="nav-item mr-lg-3 mr-md-3 archimedesNav">
                                    <a class="nav-link active btn vqs-filt-btn" id="orders-tab" data-toggle="tab"
                                        href="#orders" role="tab" aria-controls="home" aria-selected="true">Transaction
                                        Orders</a>
                                </li>
                                <li class="nav-item mr-lg-3 mr-md-3 archimedesNav">
                                    <a class="nav-link btn vqs-filt-btn" id="pending-tab" data-toggle="tab"
                                        href="#pending" role="tab" aria-controls="home" aria-selected="true">Pending
                                        Approval</a>
                                </li>
                                <li class="nav-item mr-lg-3 mr-md-3 archimedesNav">
                                    <a class="nav-link btn vqs-filt-btn" id="summary-tab" data-toggle="tab"
                                        href="#summary" role="tab" aria-controls="profile"
                                        aria-selected="false">Summary</a>
                                </li>
                            </ul>

                        </div>



                    </div>
                </div>
                {{-- Orders --}}
                <div class="tab-content col-lg-12 col-md-12" id="myTabContent">
                    <div class="tab-pane fade row pt-0 show active" id="orders" role="tabpanel"
                        aria-labelledby="orders-tab">
                        @include('agent.vqs-part.all-order-part')
                    </div>
                    {{-- Summary --}}
                    <div class="tab-pane fade row pt-0" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                        @include('agent.vqs-part.pending-approval')
                    </div>
                    {{-- Summary --}}
                    <div class="tab-pane fade row pt-0" id="summary" role="tabpanel" aria-labelledby="summary-tab">
                        @include('agent.vqs-part.summary-part')
                    </div>


                    @endif
                </div>
            </div>
        </div>
        @endsection

        @push('style')
        <style>

            .summary-details {
                background-color: #ececec;
            }

            .table-striped>tbody>tr:nth-child(even)>td,
            .table-striped>tbody>tr:nth-child(even)>th {
                background-color: #fff9e2;
            }

            .table-striped>tbody>tr:nth-child(odd)>td,
            .table-striped>tbody>tr:nth-child(odd)>th {
                background-color: #fff;
            }

            .agent-task::-webkit-scrollbar {
                height: 7px;
            }

            .agent-task::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px #ffcc00;
                border-radius: 10px;
                border: 1px solid #ffcc00;
            }

            .agent-task::-webkit-scrollbar-thumb {
                border-radius: 10px;
                -webkit-box-shadow: inset 0 0 6px#000;
            }

            .agent-task td,
            th {
                border: 1px solid #dee2e6;
                text-align: center;
            }

            .archimedes-header {
                border-bottom: 1px solid #ffcc00;
                background: transparent;
            }

            .bjsh-btn-grey {
                color: #000;
                background: lightgrey;

            }

            .my_order_text {
                padding: 10px 20px;
                background: lightgrey;
            }

            .archimedes-title {
                font-size: 26px;
            }

            .tab-content {
                margin-top: -1px;
                background: transparent;
                border: none;
            }

            .nav-tabs .nav-link {
                color: #000;
                background: #b1b1b1;
            }

            .nav-tabs .nav-link.active,
            .nav-tabs .nav-link.active:focus {
                color: #000;
                background: #ffcc00;
            }

            .nav-tabs .nav-link:hover {
                color: #000;
                background: transparent;
                border: 1px solid #ffcc00;
            }

            .archimedesNav {
                min-width: 150px;
                text-align: center;
                font-weight: 700;
            }

            .largerCheckBox {
                width: 30px;
                height: 30px;
            }

            @media (max-width: 575.98px) {
                .summary-title {
                    font-size: 12px;
                    font-weight: 700;
                }

                .summary-desc {
                    font-size: 12px;
                }

                .nav-tabs .nav-link {
                    color: #000;
                    background: transparent;
                }

                .archimedes-title {
                    font-size: 16px;
                }

                .archimedes-orders {
                    border-radius: 8px;
                    padding: 7px 15px;
                }

                .archimedes-invoice {
                    color: #ffcc00;
                    font-size: 16px;
                    text-align: center;
                    font-weight: 600;
                    padding: 10px 0px;
                }

                .archimedes-header,
                .summaryProductTitle {
                    text-align: center;
                }

                .archimedes-image-title {
                    text-align: center;
                }

                .archimedes-btn {
                    padding: 10px;

                }

                .navigation-icon {
                    font-size: 1.8rem;
                    margin-left: 18px;
                    color: #fbcc34;
                }

                img {
                    vertical-align: middle;
                    border-style: none;
                    margin-left: -7px;
                }

                element.style {
                    max-height: 35px;
                    margin-left: -6px;
                }
            }

            @media only screen and (max-width: 414px) {
                .app-header .navbar-brand {
                    left: 60px;
                    margin-left: 0px;
                }
            }

            @media only screen and (max-width: 320px) {

                .navigation-icon {
                    font-size: 1.8rem;
                    margin-left: 20px;
                    color: #fbcc34;
                }

                img {
                    vertical-align: middle;
                    border-style: none;
                    margin-left: -4px;
                    margin-right: 5px;
                }

                .d-inline-block.align-middle {
                    margin-top: -2px;
                    max-height: 35px;
                    margin-left: -7px;
                }

                .app-header .nav-item .nav-link>.img-avatar,
                .app-header .nav-item .avatar.nav-link>img {
                    height: 35px;
                    margin: 0px -3px;
                    border: 2px solid #ffcc00;
                }

                .fa.fa-caret-down {
                    color: #ffcc00;
                    margin-right: -6px;
                    margin-left: 1px;
                }

                .app-header .nav-item {
                    position: relative;
                    width: auto;
                    margin: -5px;
                    text-align: center;
                    float: right;
                }

                .margin-logo-mobile {
                    margin-left: -10px;
                }

            }

            @media only screen and (max-width: 280px) {
                .navigation-icon {
                    font-size: 1.8rem;
                    margin-left: -4px;
                    color: #fbcc34;
                }

                img {
                    vertical-align: middle;
                    border-style: none;
                    margin-left: -7px;
                    margin-right: -3px;
                }

                .d-inline-block.align-middle {
                    margin-top: -2px;
                    max-height: 35px;
                    margin-left: -7px;
                }

                .app-header .nav-item .nav-link>.img-avatar,
                .app-header .nav-item .avatar.nav-link>img {
                    height: 35px;
                    margin: 0px -3px;
                    border: 2px solid #ffcc00;
                }

                .fa.fa-caret-down {
                    color: #ffcc00;
                    margin-right: -6px;
                    margin-left: 1px;
                }

                .app-header .nav-item {
                    position: relative;
                    width: auto;
                    margin: -5px;
                    text-align: center;
                    float: right;
                }

                .margin-logo-mobile {
                    margin-left: -34px;
                }
            }

            @media only screen and (max-width: 240px) {
                .navigation-icon {
                    font-size: 1.8rem;
                    margin-left: -4px;
                    color: #fbcc34;
                }

                img {
                    vertical-align: middle;
                    border-style: none;
                    margin-left: -8px;
                    margin-right: -29px;
                }

                .d-inline-block.align-middle {
                    margin-top: -2px;
                    max-height: 35px;
                    margin-left: -7px;
                }

                .app-header .nav-item .nav-link>.img-avatar,
                .app-header .nav-item .avatar.nav-link>img {
                    height: 35px;
                    margin: 0px -3px;
                    border: 2px solid #ffcc00;
                }

                .fa.fa-caret-down {
                    color: #ffcc00;
                    margin-right: -6px;
                    margin-left: 1px;
                }

                .app-header .nav-item {
                    position: relative;
                    width: auto;
                    margin: -16px;
                    text-align: center;
                    float: right;
                }

                .margin-logo-mobile {
                    margin-left: -34px;
                }

                .nav-link {
                    margin-left: 24px
                }
            }
        </style>
        @endpush

        @push('script')
        <script>
            $(document).ready(function() {

            //check all checkbox
            $("#checkAll").click(function() {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Get checked id
            $("#bulkactionform").submit(function() {
                var checked = new Array();
                var ids = new Array();

                checked = $('input[name="itemsid[]"]:checked');

                for (i = 0; i < checked.length; i++) {
                    ids.push(checked[i].value);
                }

                var stringids = ids.toString();
                $("#action_ids").val(stringids);
            });
        });

        function checkBalance(checked) {

            console.log($('input[name="itemsid"]:checked').data('productCode'));
            // $('.justify-content-end .here').each(function(index, _selectGroup) {
            //     selectGroup = $(_selectGroup);
            //     var productCode = selectGroup.data('productCode');
            //     console.log('here', productCode);

            //     // var selected = 0;
            //     // selectGroup.find(".boxed.size .qltbox input[type=text]").each(function(index, eachItem) {
            //     //     selected += parseFloat(eachItem.value);
            //     // });

            //     // if (maximum != selected) error = 1;

            // });

        }

        $(".dropdown-menu li a").click(function() {
            $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <i class="fas fa-arrow-down"></i>');
            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });

        function updateStatus(obj) {
            let button = $(obj);
            let invoice = button.data('invoice');
            let loader = button.find('.spinner-border');
            // let label = button.find('.fa-heart');
            //let count = button.find('.liked-btn');
            $.ajax({
                async: true,
                beforeSend: function() {
                    // Show loading spinner.
                    loader.show();

                },
                complete: function() {
                    loader.hide();
                },
                url: '{{ route('agent.special.task.update') }}',
                type: "POST",
                data: {
                    id: invoice,
                },
                success: function(result) {

                    console.log(result);

                    // label.toggleClass('fas');
                    // label.toggleClass('far');
                    // console.log(button.data('count'));
                    // //button.data('test' ,result.data.likes);
                    // button.attr('data-count',result.data.likes);
                },
                error: function(result) {
                    label.html('error');
                }
            });
        }
        </script>
        @endpush
