<?php

namespace App\Models\Users\Dealers;

use Illuminate\Database\Eloquent\Model;

class DealerAddress extends Model
{
    // Set table
    protected $table = 'dealer_addresses';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function stateName()
    {
        return $this->belongsTo('App\Models\Globals\State', 'state_id');
    }
    public function cityName()
    {
        return $this->belongsTo('App\Models\Globals\City', 'city_key','city_key');
    }
    public function getStateLabelAttribute()
    {

        return ($this->stateName) ? $this->stateName->name : '';
    }

    public function getCityLabelAttribute()
    {

        return ($this->cityName) ? $this->cityName->city_name : $this->city;
    }
}
