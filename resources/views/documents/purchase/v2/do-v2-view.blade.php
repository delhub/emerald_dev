<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <title>Formula - Invoice {{ $purchase->purchase_number }}</title> --}}
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/a4-receipt-invoice.css') }}">
 @include('layouts.favicon')
</head>

<body>
    <div class="a4">
        <div class="header-invoice">
            <table width="100%">
                <tr >
                    <td width="30%">
                        <img class="company-width" src="{{asset('images/formula/post-img/logo.png')}}" style="width: 180px;margin: auto;">
                    </td>
                    <td class="company-name" width="50%">
                        <p>Formula Healthcare Sdn. Bhd.<span>(1318429-V)</p>
                        <p class="add1">1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                        <p class="add1">No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                        <table style="margin-top:-10px">
                            <tr class="company-details">
                                <td>
                                    <div class="contact-info">
                                        <p class="mt-contact">
                                        <img class="ci-width" src="{{asset('images/formula/post-img/do-c-icon1.png')}}">
                                        formula-cs@delhubdigital.com</p>
                                    </div>
                                </td>
                                <td>
                                    <div class="contact-info">
                                        <p class="mt-contact">
                                        <img class="ci-width" src="{{asset('images/formula/post-img/do-c-icon3.png')}}">
                                        formula2u.com</p>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td width="20%">
                        <div class="do-qrcode2">
                            <img class="barcode-width2" src="{{asset('images/formula/post-img/qrcode2.png')}}" style="width: 120px;">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="invoice-body">
            <div class="invoice-dt">
                <h1 style="
                padding: 0;
                margin: 0;
                margin-bottom: 10px;
            ">DELIVER ORDER</h1>
            </div>
            <div class="invoice-dt-box">
                <table width="100%">
                    <tr>
                        <td width="50%">
                            <div class="invoice-d-info-center">
                                <table>
                                    <tr>
                                        <td class="invoice-name invoice-d-tb">
                                            DELIVER TO
                                        </td>
                                        <td>:</td>
                                        <td class="invoice-name">
                                            sadasdadsa
                                            {{-- {{ isset($purchase->ship_full_name) ? $purchase->ship_full_name : '' }} --}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="invoice-name invoice-d-tb">
                                            CONTACT NO.
                                        </td>
                                        <td>:</td>
                                        <td class="invoice-name">
                                            2342342343242
                                            {{-- {{ isset($purchase->ship_contact_num) ? $purchase->ship_contact_num : '' }} --}}
                                        </td>
                                    </tr>
                                    <tr>
                                        {{-- @php
                                            $lines = array();
                                            $lines[] = ucwords(rtrim($purchase->ship_address_1,",").',');
                                            if ($purchase->ship_address_2) $lines[] = ucwords(rtrim($purchase->ship_address_2,",").',');
                                            if ($purchase->ship_address_3) $lines[] = ucwords(rtrim($purchase->ship_address_3,",").',');

                                            if ($purchase->ship_postcode) $postcode =  $purchase->ship_postcode;
                                            if ($purchase->cityKey != NULL) {
                                                $city = $purchase->cityKey->city_name;
                                            }else {
                                                $city = $purchase->ship_city ;
                                            }
                                            if ($purchase->cityKey != NULL ||$purchase->cityKey) $lines[]=  $postcode . ' ' . $city . ',';
                                            if ($purchase->state->name) $lines[] = $purchase->state->name . ',';

                                            $address = $lines;
                                        @endphp --}}

                                        <td class="invoice-add2" colspan="3">
                                            {{-- @foreach ($lines as $line)
                                                <p>{{$line}}<p>
                                            @endforeach --}}
                                            <p>Malaysia.</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="50%">
                            <div class="invoice-d-info-right">
                                <div class="invoice-name">
                                    <table>
                                        <tr>
                                            <td>DELIVERY ORDER NO.</td>
                                            <td>:</td>
                                            <td>
                                                {{-- {{ $purchase->getFormattedNumber() }} --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>INVOICE NO.</td>
                                            <td>:</td>
                                            <td>
                                                {{-- {{ $purchase->purchase_date }} --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Purchase Date</td>
                                            <td>:</td>
                                            <td>
                                                {{-- {{ ($purchase->user->userInfo->agentInformation) ? $purchase->user->userInfo->agentInformation->account_id : ''}} --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Method</td>
                                            <td>:</td>
                                            <td>
                                                {{-- {{$purchase->orders->first()->delivery_method}} --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Store Location</td>
                                            <td>:</td>
                                            <td>
                                                {{-- {{$purchase->orders->first()->delivery_outlet}} --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Consignment Note</td>
                                            <td>:</td>
                                            <td>
                                                {{-- {{$purchase->user->userInfo->showUserLevel()}} --}}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                                {{-- </td>
                            </tr>
                        </table>
                    </div> --}}
                </div>
            </div>


            <div class="invoice-description">
                <table class="table-width">
                    <thead class="dark-clr2">
                        <tr>
                            <th width="10%">NO.</th>
                            <th width="70%">Description</th>
                            <th width="20%">QTY</th>
                            {{-- <th width="15%">UNIT PRICE <br> (RM)</th>
                            <th width="15%">AMOUNT<br> (RM)</th> --}}
                        </tr>
                    </thead>
                    <tbody style="margin:0">
                        <tr>
                            <td>99</td>
                            <td>
                                <p style="font-weight: bold;">asddasdas(RM3234.34)</p>
                                <p>dadasdqwqdwqd wqeqwe</p>
                            </td>
                            <td style="border-left: 1px solid #00269A;">
                                99
                            </td>
                        </tr>
                        {{-- @php
                        $no = 1;

                        $subTotal = 0;
                        $grandTotal = 0;

                        $deliveryFee = ($purchase->getFeeAmount('shipping_fee') != NULL) ? $purchase->getFeeAmount('shipping_fee') : NULL;

                        foreach ($purchase->items as $item){

                            $size = NULL;

                            if(array_key_exists('product_size', $item->product_information)){
                                $size ='(Size:'.$item->product_information['product_size'].')';
                            }

                            echo'

                            <tr>
                                <td class="i-fw">'.$no.'</td>
                                <td class="table-line2">
                                    <div class="table-p-bold2">
                                        <h4>'.$item->product_code.'</h4>
                                    </div>
                                    <p>'.$item->product->parentProduct->name.$size.'</p>
                                    ';
                                    if (!empty($item->subtotal_point)) {
                                        echo'
                                            <div class="i-promo-tag">
                                                <p>Point applied: '.$item->subtotal_point.'</p>
                                            </div>
                                        ';
                                    }
                                    echo'

                                </td>
                                <td class="table-line2 i-fw">'.$item->quantity.'</td>
                                <td class="table-line2 i-fw">'.number_format(($item->unit_price / 100), 2).'</td>
                                <td class="table-line2 i-fw">'.number_format(($item->subtotal_price / 100), 2).'</td>
                            </tr>
                            ';

                            $no ++;

                            $subTotal += $item->subtotal_price;

                            $successPayment = isset($purchase->successfulPayment) ? $purchase->successfulPayment->amount : 0;
                        }

                        $subTotal = $subTotal + $deliveryFee;
                        //no tax
                        $grandTotal = $subTotal;

                        @endphp

                        @if (!empty($deliveryFee))
                        <tr>
                            <td class="i-fw">
                                {{$no}}
                            </td>
                            <td class="table-line2">
                                <div class="table-p-bold2">
                                    <h4>Delivery Fee</h4>
                                </div>
                            </td>
                            <td class="table-line2 i-fw">
                                1
                            </td>
                            <td class="table-line2 i-fw">
                                {{number_format(($deliveryFee / 100), 2)}}
                            </td>
                            <td class="i-fw">
                                {{number_format(($deliveryFee / 100), 2)}}
                            </td>
                        </tr>
                        @endif --}}

                        {{-- <tr class="table-line-top">
                            <td class="i-fw" width="50%">
                                <p>The issuance of this invoice shall be deemed as your acceptance of the terms
                                    and conditions of this Disclaimer.</p>
                            </td>
                            <td class="i-fw table-line2" width="20%">
                                Total
                            </td>
                            <td class="i-total" width="30%">
                                1000
                                {{number_format(($subTotal / 100), 2)}}
                            </td>
                        </tr> --}}
                        {{-- <tr height="2mm">
                            <td colspan="2" rowspan="3" class="sub-table">
                                <table width="50%" class="i-table-grey">
                                    <tr>
                                        <td class="less-padding">
                                            <p>Payment Method</p>
                                        </td>
                                        <td class="less-padding">:</td>
                                        <td class="less-padding">
                                            {{$purchase->purchase_type}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="less-padding">
                                            <p>Reference Number</p>
                                        </td>
                                        <td class="less-padding">:</td>
                                        <td class="less-padding">
                                            {{ $purchase->successfulPayment->auth_code }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="less-padding">
                                            <p>Received In</p>
                                        </td>
                                        <td class="less-padding">:</td>
                                        <td class="less-padding">PBB</td>
                                    </tr>
                                    <tr>
                                        <td class="less-padding">
                                            <p>Amount Paid</p>
                                        </td>
                                        <td class="less-padding">:</td>
                                        <td class="less-padding">
                                            {{ number_format(($successPayment / 100), 2) }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="2" class="i-fw table-line2 btable-line-top">
                                GST @ 7%
                            </td>
                            <td class="i-total btable-line-top">
                                0.00
                            </td>
                        </tr> --}}
                        {{-- <tr >
                            <td colspan="2" height="2mm" class="i-fw table-line2 btable-line-top">
                                Grand Total
                            </td>
                            <td class="i-total2 btable-line-top">
                                {{number_format(($grandTotal / 100), 2)}}
                            </td>
                        </tr>
                        <tr class="btable-line-top">
                        </tr>
                        <tr>
                            <td class="i-info-btm" colspan="2">
                                The issuance of this invoice shall be deemed as your acceptance of the terms
                                and conditions of this Disclaimer.
                            </td>
                        </tr> --}}
                    </tbody>

                </table>
                <table style="margin-top:0;">
                    <tbody>
                        <tr class="table-line-top">
                            <td class="i-fw" width="60%">
                                <p>
                                    The issuance of this Delivery Order shall be deemed as your acceptance of the terms and
                                    conditions
                                    of this Disclaimer. The Formula Healthcare Agent Agreement shall only become effective
                                    after
                                    full
                                    payment is received by the Company.
                                </p>
                            </td>
                            <td class="i-fw table-line2" width="20%" style="border-bottom: 1px solid #00269A;">
                                Total
                            </td>
                            <td class="i-total" width="20%" style="border-bottom: 1px solid #00269A;">
                                1000
                                {{-- {{number_format(($subTotal / 100), 2)}} --}}
                            </td>
                        </tr>
                        <tr>
                            <td class="i-fw">
                                <div style="background: #dedede;padding: 12px;border-radius: 10px;font-weight: 500;">
                                    <p>尊敬的客户，<br>
                                        感谢您在Formula2u.com购买心儀之品。收到货物，確認無誤後，请扫描送货订单右上角的QR码，并给于评分或评语。
                                        您寶貴的回饋，深具价值与意义，將讓我們團隊精益求精，力求為客至善。谢谢。<br>
                                        Dear Customer,<br>
                                        Thank you for purchasing at Formula2u.com. Please scan the QR code in the upper right corner of the
                                        delivery order that accompanies the delivery. Your ratings and comments are very valuable to us.
                                    </p>
                                </div>
                            </td>
                            <td class="i-total"></td>
                            <td class="i-total"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="invoice-footer">
            <div class="footer-dp2">
                <p>This is computer generated delivery order, no signature is required.</p>
            </div>
        </div>
    </div>
</body>

</html>
