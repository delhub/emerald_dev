@inject('controller', 'App\Http\Controllers\Warehouse\Bunbundle\BunbundleController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>{{$controller::$header}}</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div class="table-responsive m-2">
            <div class="col-12">
                <form action="{{route($controller::$transferStart)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="col-12 col-md-4">
                        <p>
                            User Name : {{$user->userInfo->full_name}}
                            <input type="hidden" name="user" value="{{$user->id}}">
                        </p>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Location <small class="text-danger"></small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="location_id" class="form-control select2 pc"required>
                            @foreach($locations as $location)
                                <option value="{{ $location->id }}">{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Panel<small class="text-danger"></small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="panel_id" class="form-control select2 pc" required>
                                <option value="">Please choose a panel.</option>
                            @foreach($panels as $panel)
                                <option value="{{ $panel->account_id }}">{{ $panel->company_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Bundle / Unbundle<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="type" class="form-control" required>
                            <option value="">Please choose a type.</option>
                            <option value="bundle"> Bundle</option>
                            <option value="unbundle"> Unbundle</option>
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Product Code (From)<small class="text-danger">* </small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="from_product_code" class="form-control select2 pc" required>
                            <option value="">Please choose a product code.</option>
                            @foreach($products as $product)
                                <option value="{{ $product->product_code }}">{{ $product->product_code }} - {{ $product->product2->parentProduct->name }} ({{ $product->attribute_name }})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Product Code (To)<small class="text-danger">* (After bundle/unbundle process, what product code will be)</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="product_code" class="form-control select2 pc" required>
                            <option value="">Please choose a product code.</option>
                            @foreach($products as $product)
                                <option value="{{ $product->product_code }}">{{ $product->product_code }} - {{ $product->product2->parentProduct->name }} ({{ $product->attribute_name }})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Quantity<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <input type="number" name="quantity" class="form-control" placeholder="Quantity" value="{{old('quantity')}}"required>{{old('quantity')}}
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Expiry Date<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="fa fa-calendar"></i>
                              </div>
                            </div>
                            <input type="date" name="expiry_date" id="expiry_date" class="form-control" placeholder="Stock In Date" autocomplete="off" value="{{old('expiry_date')}}"
                            data-parsley-error-message="This value is required." data-parsley-trigger="change" data-parsley-errors-container="#stockin-error-block" required>
                        </div>
                        <span id="stockin-error-block"></span>
                    </div>

                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Remark<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <textarea name="remark" class="form-control" required></textarea>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-danger" style="float: center;"
                           >Start Bundle/Unbundle</button>
                        </div>
                    </div>

                    {{-- <input type="hidden" name="location_id" value="{{$puchongWarehouse->id}}"> --}}

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit(userId) {
    if (confirm('Are you sure want to adjustment?')) {
        document.getElementById("create-form-id").submit();
    }
}
</script>
@endpush
