<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanelAttributeLimitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panel_attribute_limit', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('attribute_id');
            $table->string('product_code');
            $table->string('duration');
            $table->string('roles');
            $table->integer('amount');
            $table->tinyInteger('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panel_attribute_limit');
    }
}
