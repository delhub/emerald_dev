@extends('layouts.administrator.main')

@section('content')
@php

@endphp

<h1>Courier</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/courier/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Product</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
            @if(count($couriers) > 0)
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>Courier Name</td>
                        <td>SSM Number</td>
                        <td>Courier Email</td>
                        <td>Courier Phone</td>
                        <td>PIC Name</td>
                        <td>PIC email</td>
                        <td>PIC Contact</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>

                    @foreach($couriers as $courier)
                    <tr>
                        <td>{{$courier->id}}</td>

                        <td>
                            {{$courier->courier_name}}
                        </td>

                        <td>
                            {{$courier->ssm_number}}
                        </td>

                        <td>
                            {{$courier->courier_email}}
                        </td>

                        <td>
                            {{$courier->courier_phone}}
                        </td>

                        <td>
                            {{$courier->pic_name}}
                        </td>

                        <td>
                            {{$courier->pic_email}}
                        </td>

                        <td>
                            {{$courier->pic_contact}}
                        </td>

                        <td>
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left"
                            href="/administrator/courier/edit/{{$courier->id}}" class="btn btn-primary shadow-sm">Edit</a>

                            <form action="{{ route('administrator.courier.destroy',['id' => $courier->id]) }}" id="delete-courier-form" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            <!-- @method('DELETE') -->
                            @csrf

                            <!-- <input class="btn btn-danger" type="submit" value="Delete" /> -->

                            <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>

                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$couriers->links()}}
            @else
            <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No courier found</p>
            @endif
        </div>
    </div>
</div>
@endsection
