<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRbsFreqTimesInAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('rbs_total_months');
            $table->text('rbs_frequency')->nullable();
            $table->text('rbs_times_send')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            //
            $table->dropColumn('rbs_frequency');
            $table->dropColumn('rbs_times_send');

        });
    }
}
