@if ($user->hasAnyRole('administrator|management|admin_product|formula_pic'))
    <div class="col-md-2 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Current Price (version{{ ($attribute) ? $attribute->getPriceAttributes('price_key') : '' }})</label>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="fixed_price" class="col-form-label font-weight-bold">Web Fixed Price (RM)<small class="text-danger">*</small></label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="fixed_price[]" id="fixed_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('fixed_price') : '' }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_width" class="col-form-label font-weight-bold">Web offer Price (RM) <small class="text-danger">*</small></label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="web_offer_price[]" id="web_offer_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('web_offer_price') : 0 }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_width" class="col-form-label font-weight-bold">Outlet Fixed Price (RM) <small class="text-danger">*</small></label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="outlet_price[]" id="outlet_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('outlet_price') : 0 }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_width" class="col-form-label font-weight-bold">Outlet Offer Price (RM) <small class="text-danger">*</small></label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="outlet_offer_price[]" id="outlet_offer_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('outlet_offer_price') : 0 }}">
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="product_measurement_depth" class="col-form-label font-weight-bold">Standard Member Price (RM) <small class="text-danger">*</small></label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="standard_price[]" id="standard_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('standard_price') : 0 }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_weight" class="col-form-label font-weight-bold">Advance Member Price (RM)</label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="advance_price[]" id="offer_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('advance_price') : 0 }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_weight" class="col-form-label font-weight-bold">Premier Member Price (RM)</label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="premier_price[]" id="premier_price"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('premier_price') : 0 }}">
            </div>
        </div>
    </div>

    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Product SV <small class="text-danger">*</small></label>
                <input {{ $attribute ? 'readonly' : '' }} type="text" name="product_sv[]" id="product_sv"
                class="form-control input-mask-price" value="{{ ($attribute) ? $attribute->getPriceAttributes('product_sv') : 0 }}">
            </div>


            <div class="col-md-9">
                <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Notes</label>
                <textarea {{ $attribute ? 'readonly' : '' }} type="text" name="notes[]" id="notes"
                class="form-control" value="{{ ($attribute) ? $attribute->getPriceAttributes('notes') : 0 }}">{{ ($attribute) ? $attribute->getPriceAttributes('notes') : 0 }}
                </textarea>
            </div>
        </div>
    </div>
@endif

    <div class="col-11 m-md-4 " style="border-bottom:1px solid lightgrey;">
    </div>

    <div class="col-md-2 d-flex mr-md-5">
        <label for="product_variation_measurement" class="my-auto col-form-label font-weight-bold">Product Point</label>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-3">
                <label for="fixed_price" class="col-form-label ">Point</label>
                <input {{ $attribute ? 'readonly' : '' }} type="number" name="point[]" id="point"
                    class="form-control" value="{{ ($attribute) ? $attribute->getPriceAttributes('point') : 0 }}">
            </div>
            <div class="col-md-3">
                <label for="product_measurement_width" class="col-form-label ">Discount Percentage (%)</label>
                <input {{ $attribute ? 'readonly' : '' }} type="number" id="discount_percentage" name="discount_percentage[]"
                    class="form-control d-inline" value="{{ ($attribute) ? $attribute->getPriceAttributes('discount_percentage') : 0 }}">
            </div>
            @php
                $pointCash = $attribute && $attribute->getPriceAttributes('point_cash') != NULL ? json_decode($attribute->getPriceAttributes('point_cash')) : null;
            @endphp
            <div class="col-md-3">
                <label for="product_measurement_depth" class="col-form-label ">Point + Cash<small class="text-danger">*</small></label>
                <div class="input-group">
                    <input {{ $attribute ? 'readonly' : '' }} type="text" name="point_cash_point[]" id="point_cash_point" class="select2 form-control available-in d-inline" value="{{ $pointCash ? $pointCash->point : ''}}" placeholder="0">
                    <div class="input-group-text">
                        <span class="input-group-prepend">+</span>
                    </div>
                    <input {{ $attribute ? 'readonly' : '' }} type="text" name="point_cash_cash[]" id="point_cash_cash" value="{{$pointCash ? $pointCash->cash : ''}}" placeholder="0" class="form-control input-mask-price">
                </div>
            </div>
        </div>
    </div>
