@extends('layouts.shop.main')

@section('content')
@if ($validationStatus == '200')
<div class="row" style="margin:0;height: 763px;">
    <div class="offset-2 col-8">
        <div class="card text-center">
            <div class="danger card-header">
                Validation result
            </div>
            <div class="card-body">
                <h5 class="card-title">{{ $validationMessage }}</h5>
                <p class="card-text">You can shop with Ruby price now</p>
                <a href="/shop" class="btn bjsh-btn-gradient">Go shopping</a>
            </div>
            <div class="card-footer text-muted">
                Thank you
            </div>
        </div>
    </div>
</div>
@else
<div class="row" style="margin:0;height: 763px;">
    <div class="offset-2 col-8">
        <div class="card text-center">
            <div class="danger card-header">
                Validation result
            </div>
            <div class="card-body">
                <h5 class="card-title">{{ $validationMessage }}</h5>
                <p class="card-text">We will arrange a PIC to contact you, or you can drop us an email</p>
                <a href="mailto:formula2u-cs@delhubdigital.com"
                    class="btn bjsh-btn-gradient">formula2u-cs@delhubdigital.comm</a>
            </div>
            <div class="card-footer text-muted">
                Thank you
            </div>
        </div>
    </div>
</div>
@endif
@endsection