@extends('layouts.administrator.main')

@section('content')
{{-- @php
dd($testbanners->country_id);
@endphp --}}
    
<h1>Banner</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/testbanners/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New Product</a>
                </div>
            </div>
        </div>
        <div class="table-responsive m-2">
        @if(count($testbanners) > 0)
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>Title</td>
                        <td>Categories</td>
                        <td>Web</td>
                        <td>Mobile</td>
                        <td>Link</td>
                        <td>Edit</td>
                    </tr>
                </thead>
                <tbody>
                
                    @foreach($testbanners as $testbanner)
                    <tr>
                        <td style="width: 5%;">{{$testbanner->id}}</td>
                        <td style="width: 15%;">
                        {{$testbanner->title}}
                        <br>
                        <small>Created on : {{$testbanner->created_at}}</small>
                        </td>
                        <td style="width: 7%;">
                            Country: {{$testbanner->country_id}}</br>
                            {{$testbanner->banner_type}}
                            <br>
                            @if($testbanner->banner_show == 1)
                            <small style="color: rgb(255 255 255);padding: 5px 5px;background: #ff4646;border-radius: 5px;text-align: center;">Active</small>
                            @else
                            <small style="color: rgb(255 255 255);padding: 5px 5px;background: #81cec6;border-radius: 5px;text-align: center;">No Active</small>
                            @endif
                        </td>
                        <td style="width: 15%">
                            <img class="mw-100" src="{{asset('/storage/uploads/banner/' . $testbanner->banner_web)}}" alt="">
                        </td>
                        <td style="width: 10%">
                            <img class="mw-100" src="{{asset('/storage/uploads/banner/' . $testbanner->banner_mobile)}}" alt="">
                        </td>
                        <td style="width: 33%;">
                            {{$testbanner->banner_link}}
                        </td>
                        <td style="width: 20%;">
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left" 
                            href="/administrator/testbanners/edit/{{$testbanner->id}}" class="btn btn-primary shadow-sm">Edit</a>

                            <form action="{{ route('administrator.testbanners.destroy',['id' => $testbanner->id]) }}" id="delete-banner-form" method="POST" style="width: auto;float: left;margin-left: 20px;">
                            <!-- @method('DELETE') -->
                            @csrf

                            <!-- <input class="btn btn-danger" type="submit" value="Delete" /> -->

                            <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>

                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$testbanners->links()}}
                    @else
                    <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No posts banner found</p>
            @endif
        </div>
    </div>
</div>
@endsection