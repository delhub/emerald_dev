<?php

namespace App\Http\Controllers\Shop;

use PDF;
use DB;
use App\Models\Users\User;
use App\Models\Purchases\Purchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\Customers\Favorite;
use Illuminate\Support\Facades\Auth;
use App\Models\Globals\Status;
use Illuminate\Support\Carbon;
use App\Models\Sidemenus\Sidemenu;
use App\Models\Discount\Code;
use App\Models\Purchases\Order;
use App\Models\Purchases\RBS as PurchasesRBS;
use App\Traits\Voucher;
use App\Models\Rbs\RbsOptions;
use App\Models\Rbs\RbsPostpaid;

class ValueRecordsController extends Controller
{
    use Voucher;

    static $statuses;
    public function __construct()
    {
        self::$statuses = [3001, 3002, 3003, 3013, 4000, 4001, 4002, 4003, 4006, 4007,4008,4009];
    }

    /********Return Welcome Page Customer****/

    public function homePageCustomer()
    {

        $user = User::find(Auth::user()->id);
        //Get user profile information
        $userProfile = $user->userInfo;
        $userInfo = [
            'customer_id' => $userProfile->account_id,
            'name' => $userProfile->full_name,
            'email' => $user->email,
            'phone' => $userProfile->mobileContact->contact_num,
            'nric' => $userProfile->nric,
            'member_status' => $userProfile->user_level
        ];
        $userInfo = json_encode($userInfo);
        $userInfo = base64_encode($userInfo);
        $userInfo = 'customerqr::' . $userInfo;

        //Get first 3 value records from user
        $purchases = Purchase::where('user_id', $user->id)
            ->whereIn('purchase_status', self::$statuses)
            ->withCount('orders')
            ->where('country_id', country()->country_id)
            ->latest()
            ->take(1)
            ->get();

        //Get first 3 perfect list from user
        //$user = User::find(Auth::user()->id);
        //Get favourite item for user
        $favourite = $user->favorites->take(1);

        // Dynamic menu
        $menus = Sidemenu::where('menu_parent', '=', 0)->get();
        $menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $allMenus = Sidemenu::pluck('menu_title', 'id')->all();

        // Get user.
        $user = User::find(Auth::user()->id);

        $country_code = country()->country_id;

        $voucher = Code::where([
            ['user_id', $user->id],
            ['country_id', $country_code],
            ['coupon_status', 'valid']
        ])->count();

        return view('shop.customer-dashboard.home')
            ->with('allMenus', $allMenus)
            ->with('menus', $menus)
            ->with('voucher', $voucher)
            ->with('country', $country_code)
            ->with('userProfile', $userProfile)
            ->with('purchases', $purchases)
            ->with('favourite', $favourite)
            ->with('userInfo', $userInfo);
    }

    //test user dashboard value page
    public function valuePageCustomer()
    {

        $user = User::find(Auth::user()->id);
        //Get user profile information
        $userProfile = $user->userInfo;

        //Get first 3 value records from user
        $purchases = Purchase::where('user_id', $user->id)
            ->whereIn('purchase_status', self::$statuses)
            ->withCount('orders')
            ->where('country_id', country()->country_id)
            ->take(1)
            ->get();

        //Get first 3 perfect list from user
        //$user = User::find(Auth::user()->id);
        //Get favourite item for user
        $favourite = $user->favorites->take(1);

        // Dynamic menu
        $menus = Sidemenu::where('menu_parent', '=', 0)->get();
        $menus = Sidemenu::orderBy('menu_arrangement', 'ASC')->get();
        $allMenus = Sidemenu::pluck('menu_title', 'id')->all();

        // Get user.
        $user = User::find(Auth::user()->id);

        $country_code = country()->country_id;

        $voucher = Code::where([
            ['user_id', $user->id],
            ['country_id', $country_code],
            ['coupon_status', 'valid']
        ])->count();

        return view('shop.customer-dashboard.value-record')
            ->with('allMenus', $allMenus)
            ->with('menus', $menus)
            ->with('voucher', $voucher)
            ->with('country', $country_code)
            ->with('userProfile', $userProfile)
            ->with('purchases', $purchases)
            ->with('favourite', $favourite);
    }

    /****Return All Orders in customer dashboard  ****/

    public function customerAllOrders(Request $request)
    {
        $user = User::find(Auth::user()->id);


        // Return purchases that user have paid

        // $order_status = [1000];
        // $purchases = $user->purchases->whereIn('purchase_status', $statuses);
        // $totalOrders = $purchases->sum('orders_count');

        $country_id = country()->country_id;

        $purchases = Purchase::where('user_id', $user->id)
            ->whereIn('purchase_status', self::$statuses)
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->withCount('orders')
            ->orderBy('id', 'DESC')
            ->where('country_id', $country_id);

        $ForCount = $purchases->get();

        $purchases = $purchases->paginate(10);
        $purchases->appends(request()->query());

        $statuses = Status::whereIn('id', [3000, 3001, 3002, 3003, 4000, 4001, 4002, 4003, 4004, 4005, 4006, 4007])->get();
        $status = array();
        foreach ($statuses  as $stats) {
            $status[$stats->id] = $stats->name;
        }

        // Get date
        $now = Carbon::now()->toDateString();

        // return $delivery = Purchase::all()->orders;
        return view('shop.customer-dashboard.value-record')
            ->with('purchases', $purchases)
            ->with('ForCount', $ForCount)
            ->with('request', $request)
            ->with('now', $now)
            ->with('status', $status);
    }

    /*******Return Open Orders in customer dashboard******/

    public function openOrders()
    {
        $user = User::find(Auth::user()->id);
        // Return purchases that user have paid
        // $statuses = [3001, 3002, 3003];
        // $purchases = $user->purchases->where('purchase_status', 3000)->withCount('orders')->get();
        // $annualOrders= $user->purchases->orders->whereYear('created_at', '=', 2020)->get();
        $purchases = Purchase::where('user_id', $user->id)->whereIn('purchase_status', [3000, 4000])
            ->withCount('orders')->get();

        return view('shop.customer-dashboard.value-records.open-orders')->with('purchases', $purchases);
    }

    /*******Return Order Status in customer dashboard******/

    public function orderStatus()
    {
        $user = User::find(Auth::user()->id);
        // Return purchases that user have paid

        // $purchases = $user->purchases->whereIn('purchase_status', $statuses);
        $purchases = Purchase::where('user_id', $user->id)->whereIn('purchase_status', self::$statuses)
            ->withCount('orders')->get();

        return view('shop.customer-dashboard.value-records.order-status')->with('purchases', $purchases);
    }


    /** Show invoice of the order (PDF)**/

    public function invoice($purchase_num)
    {
        // Get user.


        $purchase = Purchase::where('purchase_number', $purchase_num)->first();
        $pdf = PDF::loadView('documents.purchase.v2.invoice', compact('purchase'))->setPaper('A4');
        return $pdf->stream('purchase-order.' . $purchase_num . '.pdf');
    }


    /***Show receipt of order (PDF)***/

    public function receipt($purchase_num)
    {


        $purchase = Purchase::where('purchase_number', $purchase_num)->first();
        $pdf = PDF::loadView('documents.purchase.v2.receipt', compact('purchase'))->setPaper('A4');
        return $pdf->stream('receipt-.' . $purchase_num . '.pdf');
    }


    /**Return wishlist for user */
    public function wishlist()
    {
        // Get user.
        $user = User::find(Auth::user()->id);
        //Get favourite item for user
        $favourite = $user->favorites->all();
        // dd($favourite);

        return view('shop.customer-dashboard.value-records.wishlist')->with('favourite', $favourite);
    }

    public function promotion()
    {
        return view('shop.customer-dashboard.promo-offer');
    }

    public function rbsRemind(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $RbsPostpaid = RbsPostpaid::where('user_id', $user->id)->where('active','!=', 3);

        $ForCount = $RbsPostpaid->get();

        $RbsPostpaid = $RbsPostpaid->paginate(10);
        $RbsPostpaid->appends(request()->query());

        $globalRbs =  RbsOptions::where('option_type', 'frequency')->where('option_status','active')->get();

        return view('shop.customer-dashboard.rbs-remind')
            ->with('request', $request)
            ->with('RbsPostpaid', $RbsPostpaid)
            ->with('ForCount', $ForCount)
            ->with('globalRbs', $globalRbs);
    }

    public function rbsProductTracking(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $country_id = country()->country_id;

        $purchases = Purchase::where('user_id', $user->id)
            ->whereIn('purchase_status', self::$statuses)
            ->where('order_type', 'rbs')
            ->searchFilter($request->searchBox, $request->selectSearch)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->withCount('orders')
            ->orderBy('id', 'DESC')
            ->where('country_id', $country_id);

            // return Purchase::with('orders')->where('user_id', $user->id)
            //         ->where('order_type','rbs')
            //         ->get();

        $ForCount = $purchases->get();

        $purchases = $purchases->paginate(2);
        $purchases->appends(request()->query());

        // $today = Carbon::createFromFormat('d/m/Y', Carbon::now())->format('d-m-Y');
        //     // return Carbon::createFromFormat('d/m/Y', Carbon::now());

        return view('shop.customer-dashboard.rbs-producttracking')
            ->with('request', $request)
            ->with('purchases', $purchases)
            // ->with('today', $today)
            ->with('ForCount', $ForCount);
    }

    public function updateRBSMonthReminder(Request $request)
    {
        $rbsPostPaid = RbsPostpaid::where('id', $request->id)->first();

        $returndata = array();
        if ($request->data != '0') {
            $formData = explode("&", $request->data);
            foreach ($formData as $data) {
                $array = explode("=", $data);
                $returndata[$array[0]] = $array[1];
            }
        }
        // dd($rbsPostPaid,$request->request,$returndata);

        switch ($request->process) {
            case 'cancel':
                $rbsPostPaid->active = 0;
                $rbsPostPaid->cancel_date = Carbon::now();
                break;
            case 'updateMonth':
                $rbsPostPaid->months_id = $returndata['rbs_month'];
                break;
            case 'delete':
                $rbsPostPaid->active = 3;
                break;
        }

        $rbsPostPaid->save();
        if ($rbsPostPaid->save()) {
            return 'Updated!';
        } else {
            return 'Something went wrong. Please try again.';
        }
    }

    public function inactiveRBSMonthReminder(Request $request)
    {
        $rbsPostPaid = RbsPostpaid::where('id', $request->postpaid_id)->first();
        $rbsPostPaid->active = 0;
        $rbsPostPaid->save();

        return redirect()->back();
    }

    public function voucher()
    {
        // Get user.
        $user = User::find(Auth::user()->id);

        $code = $this->getUserVoucherList($user->id, country()->country_id);

        /* $code = DB::table('discount_code')
            ->join('discount_rule', 'discount_code.coupon_rule_id', '=', 'discount_rule.id')
            ->where([
                ['discount_code.country_id', $country_code],
                ['discount_code.user_id', $user->id],
                ['discount_code.coupon_status','!=','inactive']
            ])
            ->orderBy('discount_code.created_at', 'desc')
            ->get();

        foreach ($code as $val) {
            $val->coupon_code = implode('-', str_split($val->coupon_code, 4));
            $val->expiry = Carbon::parse($val->expiry)->format('d/m/Y');
        } */

        return view('shop.customer-dashboard.voucher')
            ->with('voucher', $code)
            ->with('country', country()->country_id);
    }
}
