<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bujishu - Purchase Order {{ $order->order_number}}</title>
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

  <style>
    p {
      font-size: 12px;
      font-family: 'Lato', sans-serif;

    }

    mainTable {
      font-family: arial, sans-serif;
      /* table-layout: fixed; */
      height: 100%;
      border-collapse: collapse;
      border: black 1px solid;
      height: auto;

    }
    #invoice-box {
      background-color: #fef5d6;
      border-radius: 20px;
      padding: 20px 0px 20px 20px;
      font-size: 13px;
      width: 100%;
      height: 100%;
    }

    #payment-box {
      background-color: #fef5d6;
      border-radius: 20px;
      padding: 20px 0px 20px 20px;
      font-size: 13px;
      width: 100%;
      height: 100%;
    }

    #head-table {
      background-color: #fef5d6;
      line-height: 30px;
    }

    #col-amount {
      background-color: #fef5d6;
      text-align: center;
      width: auto;
    }

    #col-unit,
    #col-qty {
      text-align: center;
    }

    #col-no {
      width: 5%;
    }

    #last-footer {}

    #main-tab {
      width: 100%;
      position: absolute;
      bottom: 0;
      left: 0;
    }

    #payment-tab {
      width: 100%;
    }
    .vertical {
            border-left: 1px solid gold;
            height: 100px;
            position: fixed;
            left: 220px;
            top: 10px;
        }
  </style>
</head>

<body style="margin: 0;">
  <?php
  // $url = URL::signedRoute('confirm-order', [
  //     'purchase_num' => $purchase->purchase_number,
  // ]);

  $url = URL::signedRoute(
      'guest.order-received',
      ['purchaseNum' => $order->order_number]
  );
  ?>

  <hr style="height:10px;border:none;background-color:#ffcc00; padding:0;margin:0;" />

  <table cellspacing="0" width="100%" height="1410px" style="padding: 0px 50px 0px 50px;">
    <thead>
      <tr style="padding:10px;">
        <th colspan="2" id="logo" style="padding:10px 10px 10px 0;"><img align="left" width="64%" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-logo.jpg') }}"><div class = "vertical"></div></th>
        <th colspan="5" style="text-align:left;">
          <p style="margin: 0px 0px 0px -20px;font-size:17px;line-height: 0px;padding-top: 20px;"><br>DC SIGNATURE LIVING STYLE SDN BHD</p>
          <p style="margin: 15px 0px 0px -20px;line-height:1px;color:#696969;font-weight: 100;"><sup>Company No:202001002917(1859236-K)</sup></p>
          <p style="margin: 0px 0px 0px -20px;font-size:12px;color:#696969;font-weight: 100;">1-26-15 Menara Bangkok Bank, Berjaya Central Park,
            <br>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
          <p style="margin: 0px 0px 0px -20px;font-size:12px;color:#696969;font-weight: 100;"><img style="height:12px;vertical-align:text-bottom;" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-tel.jpg') }}"> 603-2181 8821&nbsp;&nbsp;&nbsp;&nbsp;<img
               style="height:12px;vertical-align:text-bottom;" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-mail.jpg') }}">
            bujishu-cs@delhubdigital.com&nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-web.jpg') }}" style="height:12px;vertical-align:text-bottom;" > www.bujishu.com</p>
          </p>
        </th>
      </tr>
      <tr>
      <th colspan="4">
      </th>
      <th colspan="4">
        <p style="color:gold;font-size:33px; text-align:left;padding-top: 20px;">&nbsp;PURCHASE ORDER</p>
      </th>
      </tr>
      <tr>
        <td colspan="2" width="30%">
          <p style="font-size:16px;color:#696969;">PURCHASE FROM</p>
          <hr style="border-top: 1px solid gold;">
          <p>{{ $order->purchase->user->userInfo->shippingAddress->address_1 }}<br>{{ $order->purchase->user->userInfo->shippingAddress->address_2 }}<br>{{ $order->purchase->user->userInfo->shippingAddress->postcode }},
            {{ $order->purchase->user->userInfo->shippingAddress->city }}<br>{{ $order->purchase->user->userInfo->shippingAddress->state->name }},
            Malaysia<br>Attn : <strong>{{ $order->purchase->user->userInfo->full_name }}</strong><br>Tel : <strong>{{ $order->purchase->user->userInfo->mobileContact->contact_num }}</strong></p>
          <hr style="border-top: 1px solid gold;">
        </td>

        <td colspan="2" width="30%" style="margin-right:20px;">
          <p style="font-size:16px;color:#696969;">DELIVER TO</p>
          <hr style="border-top: 1px solid gold;width: 80%; margin-left: 0;">
          <p>@if ($order->purchase->ship_address_1)
            {{ $order->purchase->ship_address_1 }}
            @else
            {{ $order->purchase->user->userInfo->shippingAddress->address_1 }}
            @endif<br> @if ($order->purchase->ship_address_2)
            {{ $order->purchase->ship_address_2 }}
            @else
            {{ $order->purchase->user->userInfo->shippingAddress->address_2 }}
            @endif<br>@if ($order->purchase->ship_postcode)
            {{ $order->purchase->ship_postcode }},
            @else
            {{ $order->purchase->user->userInfo->shippingAddress->postcode }}
            @endif

            @if ($order->purchase->city)
            {{ $order->purchase->city }}
            @else
            {{ $order->purchase->user->userInfo->shippingAddress->city }}
            @endif<br>@if ($order->purchase->ship_state_id)
            {{ $order->purchase->state->name }},
            @else
            {{ $order->purchase->user->userInfo->shippingAddress->state->name }}
            @endif
            Malaysia<br>Attn : <strong>@if($order->purchase->ship_full_name)
              {{ $order->purchase->ship_full_name }}
              @else
              {{ $order->purchase->user->userInfo->full_name }}
              @endif</strong><br>Tel : <strong>@if ($order->purchase->ship_contact_num)
              {{ $order->purchase->ship_contact_num }}
              @else
              {{ $order->purchase->user->userInfo->mobileContact->contact_num }}
              @endif</strong></p>
          <hr style="border-top: 1px solid gold;width: 80%; margin-left: 0;">
        </td>
        <td colspan="4" width="30%">
          <table id="invoice-box">
            <tr>
              <td>
                <p>PURCHASE ORDER NO. &nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{ $order->getOrderNumberFormatted() }}</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Date&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{ $order->getFormattedDate() }}</p>
              </td>

            </tr>
            <tr>
              <td>
                <p>Agent code&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: -
              </td>
            </tr>
            <tr>
              <td>
                <p>Credit Terms&nbsp;&nbsp;</p>
              </td>
              <td>
                <p style="text-transform:capitalize;">: -
              </td>
              </p>
            </tr>
            <tr>
              <td>
                <p>Page&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: 2 / 2</p>
              </td>
            </tr>
          </table>
        </td>
        </th>
      </tr>
      <tr>
        <td colspan="8" height="20px"></td>
      </tr>
      <tr id="head-table">
        <td style="border-bottom:gold 1px solid; border-top:gold 1px solid;" id="col-no">
          <p style="font-weight:600;">&nbsp;&nbsp;&nbsp;&nbsp;No.</p>
        </td>
        <td style="border-bottom:gold 1px solid; border-top:gold 1px solid;">
          <p style="font-weight:600;">Product ID </p>
        </td>

        <td style="border-bottom:gold 1px solid; border-top:gold 1px solid;" colspan="5">
          <p style="font-weight:600;text-align:center;">Description </p>
        </td>
        <td style="border-bottom:gold 1px solid;width: 10%; border-top:gold 1px solid;" id="col-amount">
          <p style="font-weight:600;">Qty </p>
        </td>

      </tr>
    </thead>

    <tbody id="middle-body" style="display:table-row-group;">

      <?php
    $iterationNo = 0;
    ?>
      @foreach($order->items as $item)

      <tr valign="top">
        <td id="col-no" style="border-bottom:gold 1px solid;">
          <?php
            $iterationNo = $iterationNo + 1;
            ?>
          <p><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $iterationNo }}</p>
        </td>
        <td style="width:15%;border-bottom:gold 1px solid;">
          <p><br>{{ $item->product->parentProduct->product_code }}</p>
        </td>
        <td style="width:14%;border-bottom:gold 1px solid;">
          <img width="60%" src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}" alt="{{ $item->product->parentProduct->name }}"
            style="padding:30px 0px 30px 0px;display:block;">
        </td>
        <td colspan="4" style="border-bottom:gold 1px solid;width:20%;">
          <p><br><strong>{{ $item->product->parentProduct->name }}</strong>
            <br>
            @if($item->product_id != 175 || $item->product_id != 176)
            @if(array_key_exists('product_color_name', $item->product_information))
            Color: {{ $item->product_information['product_color_name'] }}
            @endif
            <br>
            @if(array_key_exists('product_size', $item->product_information))
            Size: {{ $item->product_information['product_size'] }}
            @endif
            <br>
            @if(array_key_exists('product_temperature', $item->product_information))
            Color Temperature: {{ $item->product_information['product_temperature'] }}
            @endif
          </p>
          @endif
        </td>
        <td id="col-amount" style="border-bottom:gold 1px solid;">
          <p><br>{{ $item->quantity }}</p>
        </td>

      </tr>
      @endforeach

    </tbody>
    <tfoot id="last-footer">
      <tr>
        <td colspan="3">
          <p style="color:grey;font-size:15px;padding: 10px 10px 0 30px;">REMARK<br></p>
        </td>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td style="border-bottom:gold 1px solid;white-space:nowrap !important;">
          <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;line-height:20px;">Total&nbsp;</p>
        </td>
        <td style="border-bottom:gold 1px solid; background-color:#ffcc00;" id="col-amount">
          <?php
          $qtyTotal = 0;

          foreach ($order->items as $item) {
              $qtyTotal+= $item->quantity;
          }

          ?>
          <p>{{ number_format($qtyTotal) }}</p>
        </td>
      </tr>
      <tr>
        <td id="payment-box" colspan="2"><br><br><br></td>
      </tr>
      <tr>
      <tr>
        <td colspan="8">
          <center style="font-size:12px;text-align:left;vertical-align: top;">
            <p>The issuance of this invoice shall be deemed as your acceptance of the terms and conditions of this disclaimer. The Bujishu Agent Agreement shall only become effective after full payment is received by the Company.</p>
          </center><br>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <center style="color:grey;font-size:12px;line-height:25px;">
            <p>This is computer generated invoice no signature is required.</p>
          </center>
        </td>
      </tr>
    </table>
      <hr style="height:40px;border:none;background-color:#ffcc00; padding:0;margin:0;" />
  </body>

</html>
