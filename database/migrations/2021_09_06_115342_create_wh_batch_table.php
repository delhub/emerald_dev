<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhBatchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('wh_batch_detail');
        Schema::dropIfExists('wh_batch_item');
        Schema::dropIfExists('wh_batch');
        
		Schema::create('wh_batch_detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('batch_id', 24)->nullable();
			$table->string('product_code', 24)->nullable();
			$table->string('attribute_id', 24)->nullable();
			$table->decimal('cost_price')->nullable();
			$table->dateTime('expiry_date')->nullable();
			$table->timestamps();
		});

        Schema::create('wh_batch_item', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('items_id', 24)->nullable();
			$table->string('batch_id', 24)->nullable();
			$table->integer('detail_id')->nullable();
			$table->integer('location_id')->nullable();
			$table->integer('shelf_id')->nullable();
			$table->bigInteger('picking_order_id')->nullable();
			$table->timestamps();
		});

        Schema::create('wh_batch', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('batch_id', 24)->nullable();
			$table->integer('panel_id')->nullable();
			$table->string('po_number', 24)->nullable();
			$table->string('inv_number', 32)->nullable();
			$table->string('vendor_desc')->nullable();
			$table->string('lot', 24)->nullable();
			$table->dateTime('stock_in_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wh_batch_detail');
        Schema::drop('wh_batch_item');
        Schema::drop('wh_batch');
	}

}
