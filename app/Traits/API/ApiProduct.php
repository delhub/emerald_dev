<?php

namespace App\Traits\API;

use App\Models\User;
use App\Models\Globals\Products\Product;
use App\Models\Globals\Image;
use App\Models\Categories\Category;
use App\Models\Products\ProductBrand;
use App\Models\Products\ProductAttribute;
use App\Models\Products\ProductPrices;
use App\Models\Purchases\Payment;
use App\Models\Purchases\Order;
use App\Models\Purchases\Item;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Gift\Freegift;
use App\Models\Products\ProductAttributeBundle;
use App\Models\Products\WarehouseBatchItem;
// use App\Models\Products\ProductAttributeBundle;
use stdClass;

trait ApiProduct
{
    public function getAllProduct()
    {
        $data = Product::select('panel_products.*', 'panel_products.id as pid', 'global_products.*', 'global_products.id as gid')
            ->join('panel_products', 'global_products.id', '=', 'panel_products.global_product_id')->get();

        if (count($data)) {
            foreach ($data as $k => $v) {
                $v->attributes = ProductAttribute::where('panel_product_id', $v->pid)->get();
                $v->images = Image::where('imageable_id', $v->gid)->get();
                $v->pivCategory = \DB::table('piv_category_product')->where('product_id', $v->gid)->get();
                $datas[] = $v;
            }

            return $datas;
        }
    }

    public function getAllProductAttribute()
    {
        $data = ProductAttribute::all();

        if (count($data)) {
            return $data;
        }
    }

    public function getAllProductPrice()
    {
        $data = ProductPrices::where('active', 1)->get();

        if (count($data)) {
            return $data;
        }
    }

    public function getProductAttributeGift()
    {
        $data = ProductAttribute::all();

        if (count($data)) {
            foreach ($data as $k => $v) {
                if ($v->product2->parentProduct['display_only'] == 0) {
                    $v->product_name = $v->product2->parentProduct['name'];
                    $v->gid = $v->product2->parentProduct['id'];
                    if (isset($v->product_name)) {
                        $product_attribute[] = $v;
                    }

                    unset($v->product);
                }
            }

            usort($product_attribute, function ($v1, $v2) {
                return strcmp($v1['product_name'], $v2['product_name']);
            });

            return json_encode($product_attribute);
        }
    }

    public function getProductAttributeGiftBy($id)
    {
        $data = Freegift::find($id);

        if ($data) {
            return $data;
        }
    }

    public function getAllImages()
    {
        $data = Image::all();

        if (count($data)) {
            return $data;
        }
    }

    public function getAllBrand()
    {
        $data = ProductBrand::all();

        if (count($data)) {
            return $data;
        }
    }

    public function getAllCategories()
    {
        $data = Category::all();

        if (count($data)) {
            return $data;
        }
    }

    public function getAllPivCategoryProduct()
    {
        $data = \DB::table('piv_category_product')->get();

        if (count($data)) {
            return $data;
        }
    }

    public function getproductCatalog($request)
    {
        $slug = isset($request->query) ? $request->input('id') : '';
        $item_id = isset($request->item_id) ? $request->item_id : 0;

        if (isset($slug)) {
            $data = ProductAttribute::where('id', $slug)->first();

            if ($data) {
                $data->imageItem;
                $data->wh_item_id = $item_id;
                $data->cart_item = [$item_id];
                $data->product_name = $data->product->parentProduct->name;
                $data->images = isset($data->imageItem) ? "storage/" . $data->imageItem['path'] . "/" . $data->imageItem['filename'] : '';
                $data->fixed_price = $data->getPriceAttributes('fixed_price') ? number_format(($data->getPriceAttributes('fixed_price') / 100), 2) : 0;
                $data->outlet_price = $data->getPriceAttributes('outlet_price') ? number_format(($data->getPriceAttributes('outlet_price') / 100), 2) : 0;
                $data->offer_price = $data->getPriceAttributes('offer_price') ? number_format(($data->getPriceAttributes('offer_price') / 100), 2) : 0;
                $data->member_price = $data->getPriceAttributes('member_price') ? number_format(($data->getPriceAttributes('member_price') / 100), 2) : 0;
                $data->advance_price = $data->getPriceAttributes('advance_price') ? number_format(($data->getPriceAttributes('advance_price') / 100), 2) : 0;
                $data->premier_price = $data->getPriceAttributes('premier_price') ? number_format(($data->getPriceAttributes('premier_price') / 100), 2) : 0;
                $data->web_offer_price = $data->getPriceAttributes('web_offer_price') ? number_format(($data->getPriceAttributes('web_offer_price') / 100), 2) : 0;
                $data->outlet_offer_price = $data->getPriceAttributes('outlet_offer_price') ? number_format(($data->getPriceAttributes('outlet_offer_price') / 100), 2) : 0;
                $data->quantity = 1;

                unset($data->product);
                unset($data->imageItem);
                return $data;
            }
        } else {
            $data = ProductAttribute::paginate(10);

            if (count($data)) {
                return $data;
            }
        }
    }

    public function getSearchTypeHead($request)
    {
        $slug = $request->input('query');

        if ($slug != NULL) {
            //By global Product
            $products = Product::where('product_status', 1)
                ->where('global_products.name', 'like', '%' . $slug . '%')
                ->get();

            if (count($products)) {
                foreach ($products as $product) {
                    if (isset($product->productSoldByPanels[0]->panel_account_id) != NULL) {
                        $result[] = $product->name;
                    }
                }
            }
        }

        return response()->json($result);
    }

    public function getSearchId($request)
    {
        $slug = $request->input('query');

        if ($slug != NULL) {
            //By global Product
            $products = Product::where('product_status', 1)
                ->where('global_products.name', 'like', '%' . $slug . '%')
                ->get();

            if (count($products)) {
                foreach ($products as $product) {
                    if (isset($product->productSoldByPanels[0]->panel_account_id) != NULL) {
                        $result[] = $product->id;
                    }
                }

                return $result;
            } else {
                return [];
            }
        }
    }

    public function getSP($item)
    {

        if (count($item)) {
            $data = ProductAttribute::whereIn('panel_product_id', $item)->paginate(10);

            if (count($data)) {
                return $data;
            }
        } else {
            $data = ProductAttribute::paginate(10);

            if (count($data)) {
                return $data;
            }
        }
    }

    public function createOrder($data, $payId)
    {

        $user = \Auth::user();
        $operator = \Auth::id();

        $maxId = Order::max('id') + 1;

        $order = new Order;
        $order->order_number = "POS-" .  sprintf('%06d', $maxId);
        $order->operator_id = $operator;
        $order->customer_id = $data["customer"]["customer_id"];
        $order->total = round($data["payments"]['total'] * 100);
        $order->order_status = 1;
        $order->member_status = $data["customer"]["member_status"];
        $order->payment_id = $payId;

        if ($order->save()) {
            // \Log::info($order);
            $order->id = $maxId;
            if (count($data['items'])) {
                foreach ($data['items'] as $k => $v) {
                    $this->createOrderItem($v, $maxId, $order->member_status);
                }
            }

            return $order;
        }
    }

    public function createOrderItem($data, $orderId, $member_status)
    {

        switch ($member_status) {
            case 6001:
                $price = $data['advance_price'];
                break;
            case 6002:
                $price = $data['premier_price'];
                break;
            default:
                $price = $data['fixed_price'];
        }

        $item = new Item;

        $item->order_id = $orderId;
        $item->product_id = $data['panel_product_id'];
        $item->product_code = $data["product_code"];
        $item->quantity = $data['quantity'];
        $item->unit_price = round($price * 100);
        $item->subtotal_price = round($price * 100) * $data['quantity'];
        $item->unit_point = 0;
        $item->subtotal_point = 0;

        $item->save();
    }

    public function createPayment($data)
    {

        $payment = new Payment;

        $summary = new stdClass;
        $summary->subtotal = round($data["payments"]['subtotal'] * 100);
        $summary->total = round($data["payments"]['total'] * 100);
        $summary->discount = round($data["payments"]['discount'] * 100);
        $summary->voucher = $data["payments"]['voucher'];
        $summary->points = $data["payments"]['points'];
        $summary->gateway = $data["payments"]['gateway'];

        $payment->transaction_id = uniqid();
        $payment->payment_gateway = $data["payments"]['gateway'];
        $payment->amount = round($data["payments"]['total'] * 100);
        $payment->reference_number = $data["payments"]['reference_number'];
        $payment->status = 1;
        $payment->summary = json_encode($summary);

        if ($payment->save()) {
            $order = $this->createOrder($data, $payment->id);

            return ['order' => $order, 'payment' => $payment];
        }
    }

    public function getOrderHistory($limit = 10)
    {

        $user = \Auth::user();
        $operator = \Auth::id();

        $order = Order::where([['operator_id', $operator], ['order_status', 1]])->orderBy('id', 'desc')->paginate($limit);

        if (count($order)) {
            return $order;
        } else {
            return [];
        }
    }

    public function getOrderById($id)
    {
        $order = Order::where('id', $id)->first();

        if ($order) {
            return $order;
        }
    }

    public function getScancode($qr)
    {
        $a = explode("/", $qr->data);

        if (count($a) > 1) {
            $data = explode("-", $a[3]);
            $item_id = $a[3];
        } else {
            $data = explode("-", $qr->data);
            $item_id = $qr->data;
        }

        $wh_detail = WarehouseBatchDetail::where('batch_id', $data[0])->first();

        if ($wh_detail) {
            $attribute = ProductAttribute::where('product_code', $wh_detail->product_code)->first();

            if ($attribute) {
                return $attribute->id;
            }
        } else {
            // return "QR data not found";
            return ['id' => 2, 'item_id' => $item_id];
        }
    }

    public function getProductAttributeBundle($request)
    {
        if (isset($request->ppc)) {
            $data = ProductAttributeBundle::where([['primary_product_code', $request->ppc], ['primary_quantity', '<=', $request->qty]])->get();
        } else {
            $data = ProductAttributeBundle::all();

            if ($data->count() > 100 || $request->aspage == 1) {
                $data = ProductAttributeBundle::paginate(15);
            }
        }

        if ($data->count()) {
            // return $data;
            $datas = array();
            foreach ($data as $k => $v) {
                if ($v->parentProductAttribute->active == 1 || in_array($v->parentProductAttribute->product_code, ['FMLIMX0002', 'TPNC500005', 'FMLIMX0009', 'CPPIMX007', 'FMLIMX00010', 'FMLIMX00011', 'FMLENX074'])) {

                    $images = $v->productAttMain->product2->parentProduct->images;
                    $default_image = collect($images)->where('default', 1)->first();

                    $v->fixed_price = $v->productAttMain->getPriceAttributes('fixed_price') / 100;
                    $v->outlet_price = $v->productAttMain->getPriceAttributes('outlet_price') / 100;
                    $v->standard_price = $v->productAttMain->getPriceAttributes('standard_price') / 100;
                    $v->member_price = $v->productAttMain->getPriceAttributes('member_price') / 100;
                    $v->advance_price = $v->productAttMain->getPriceAttributes('advance_price') / 100;
                    $v->premier_price = $v->productAttMain->getPriceAttributes('premier_price') / 100;
                    $v->web_offer_price = $v->productAttMain->getPriceAttributes('web_offer_price') / 100;
                    $v->outlet_offer_price = $v->productAttMain->getPriceAttributes('outlet_offer_price') / 100;

                    $v->product_name = $v->productAttMain->product2->parentProduct->name;
                    $v->attribute_name = $v->productAttMain->attribute_name;
                    $v->quantity = 1;

                    $v->images = isset($default_image) ? "https://www.formula2u.com/storage/" . $default_image['path'] . $default_image['filename'] : '';

                    if (isset($request->ppc)) $v->bundle_product_code = ProductAttributeBundle::where('product_code', $v->product_code)->pluck('primary_quantity', 'primary_product_code');

                    unset($v->productAttMain);

                    $datas[] = $v;
                }
            }


            return $datas;
        } else {
            return [];
        }
    }

    public function getProductAttributeBundleConfirm($request)
    {
        $datas = json_decode($request->data);
        $bundle_errors = [];

        foreach ($datas as $cartId => $data) {
            $inBundle_error = false;
            if (!$data->isBundle) continue;

            $items_bundle = WarehouseBatchItem::whereIn('items_id', $data->cart_item)->with('batchDetail')->get()->map(function ($item) {
                return $item->batchDetail->product_code;
            })->toArray();

            $attribute_bundles = ProductAttributeBundle::where('product_code', $data->product_code)->get();

            if ($attribute_bundles->isEmpty()) continue;

            foreach ($attribute_bundles as $attribute_bundle) {
                $int_item_bundle = array_intersect($items_bundle, [$attribute_bundle->primary_product_code]);
                
                if (count($int_item_bundle) != $attribute_bundle->primary_quantity) $inBundle_error = true;
            }

            if ($inBundle_error) $bundle_errors[] = $cartId;
        }

        return response()->json($bundle_errors);
    }
}
