@extends('layouts.administrator.main')

@section('content')
@if(Session::has('successful_message'))
<div class="alert alert-success">
    {{ Session::get('successful_message') }}
</div>
@endif

@if(Session::has('error_message'))
<div class="alert alert-danger">
    {{ Session::get('error_message') }}
</div>
@endif

<div style="font-size:small;">
    <div class="row">
        <div class="col-12 mb-3">
            <h1 style="display:inline;">Order Tracking (Detail)</h1>
        </div>
    </div>

    {{-- select bulk action  --}}
    {{-- <form id="bulkactionform" action={{route('bulkupdate.ordertracking.detail') }} method="POST">
        <div class="row mt-2 ml-2">
            <div class="col-md-auto px-1">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="action_ids" id="action_ids" value="">

        <div class="input-group mb-1">
            <select name="bulkaction" id="bulkaction" class="col-12 text-left custom-select">
                <option value="">Select action</option>
                <option value="splitPOnDO">Split DO</option>
            </select>

            <div class="input-group-append">
                <input type="submit" class="bjsh-btn-gradient" value="Submit" id="btnid">
            </div>
        </div>
    </div>
</div>
    </form> --}}

    <form action="{{ route('administrator.ordertracking.detail') }}" method="GET">
        <input type="hidden" name="manyID" value="{{ $request->manyID}}">

        <div class="row mt-2 ml-2">
            {{--
            <!-- Status -->
            <div class="col-md-auto px-1">
                <select name="status" class="col-12 text-left custom-select">
                    <option value="all">All Status</option>
                    @foreach ($statuses as $status)
                    <option value="{{ $status->id }}" {{ $request->status == $status->id ? 'selected' : ''}}>
                        {{ str_replace("Order","Item", $status->name ) }}</option>
                    @endforeach
                </select>
            </div>

            <!-- States -->
            <div class="col-md-auto px-1">
                <select name="states" class="col-12 text-left custom-select">
                    <option value="all">All States</option>
                    @foreach ($states as $state)
                    <option value="{{ $state->id }}" {{ $request->states == $state->id ? 'selected' : ''}}>
                        {{ $state->name }}</option>
                    @endforeach
                </select>
            </div>
            --}}
            <!-- Stock Filter -->
            <div class="col-md-auto px-1">
                <select name="stock_filter" class="col-12 text-left custom-select">
                    <option value="all">All</option>
                    <option value="has_stock" {{ $request->stock_filter == 'has_stock' ? 'selected' : ''}}>Stock Ready</option>
                    <option value="no_stock" {{ $request->stock_filter == 'no_stock' ? 'selected' : ''}}>Stock No Ready</option>
                </select>
            </div>
            {{--
             <!-- Date filter from date to date-->
             <div class="col-md-auto px-1">
                <div class="input-group mb-3">
                    <input name="datefrom" type="text" class="form-control col-12 filterDate" placeholder="From Order Date"
                        aria-label="Username" aria-describedby="basic-addon1"
                        value="{{ ($request->datefrom) ? $request->datefrom : '' }}" autocomplete="off">
                </div>
            </div>

            <div class="col-md-auto px-1">
                <div class="input-group mb-3">
                    <input name="dateto" type="text" class="form-control col-12 filterDate" placeholder="To Order Date"
                        aria-label="Username" aria-describedby="basic-addon1"
                        value="{{ ($request->dateto) ? $request->dateto : '' }}" autocomplete="off">
                </div>
            </div>

            <div class="col-md-auto ml-auto">
                <div class="row">
                    <div class="col-md-8 p-0">
                        <select class="custom-select" id="selectSearch" name="selectSearch">
                            <option value="ALL" selected>Choose...</option>
                            <option value="BJN" {{ $request->selectSearch == 'BJN' ? 'selected' : '' }}>Invoice Number</option>
                            <option value="PODO" {{ $request->selectSearch == 'PODO' ? 'selected' : '' }}>PO / DO Number</option>
                            <option value="NAME" {{ $request->selectSearch == 'NAME' ? 'selected' : '' }}>Product Name</option>
                            <option value="CODE" {{ $request->selectSearch == 'CODE' ? 'selected' : '' }}>Product Code</option>
                        </select>

                        <input type="text" class="form-control" id="" name="searchBox"
                            value="{{ ($request->searchBox) ? $request->searchBox : '' }}">
                    </div>
                    <div class="col-md-4 ">
                        <button type="submit" class="btn btn-warning" style="color:black;">Search</button>
                    </div>
                </div>
            </div>
        </div>

        //filter here

        <br>

        <p style="text-align:center;"><span style="font-size:20px;"> TOTAL RESULT (Item) : <b>{{ count($ForCountcustomerOrders)}}</b></span></p>
        <div class="row">
            <div class="col-auto ml-auto">
                {!! $customerOrders->render() !!}
            </div>
        </div>
--}}
        <div style="text-align:center;">
            <div class="col-md-auto px-1">
                <button type="submit" class="btn btn-warning" style="color:black;">Filter</button>
                {{-- <button type="submit" class="btn btn-warning" style="color:black;"><a href={{ route('administrator.ordertracking.detail') }} style="color:black;">Reset Filter</a></button> --}}
                {{-- <button type="submit" class="btn btn-warning" style="color:black;"><a href="{{route('administrator.ordertracking.exportCsv', ['status'=> $request->status, 'states'=> $request->states, 'datefrom' => $request->datefrom, 'dateto' =>  $request->dateto, 'stock_filter' =>  $request->stock_filter, 'selectSearch' => $request->selectSearch , 'searchBox' => $request->searchBox])}}" id="export" style="color:black; ">Download</a></button> --}}
            </div>
        </div>
    </div>


    </form>

    <div class="row">
        <div class="col-12">
            <div class="tableFixHead">
                <table class="table table-bordered text-center table-striped " id="sales-tracking" style="width:100%">
                    <thead style="background-color:	#E8E8E8;">
                        <tr>
                            {{-- <th scope="col"><input type="checkbox" id="checkAll" class="largerCheckBox"/><br>Action</th> --}}
                            <th scope="col">Purchase No. / DO No.</th>
                            <th scope="col">Product Code</th>
                            <th scope="col">Product Name </th>
                            <th scope="col">Order Date</th>
                             <th scope="col">Qty</th>
                            {{--<th scope="col">Warehouse Physical Qty</th>
                            <th scope="col">Warehouse Virtual Qty</th> --}}
                            <th scope="col">Sorted Qty</th>



                            {{-- <th scope="col">Pending Days</th> --}}
                            {{-- <th scope="col">DO Status / Scan Date</th> --}}
                            {{-- <th scope="col">Item Status</th> --}}
                            {{-- <th scope="col">States</th> --}}
                            {{-- <th scope="col">Tracking</th>
                            <th scope="col">Estimate Ship Out Date</th>
                            <th scope="col">Actual Ship Out Date</th>
                            <th scope="col">Item Collected Date</th> --}}
                            {{-- <th scope="col">Remark</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customerOrders as $customerOrder)
                        {{-- @if ($customerOrder->ship_date < now() && $customerOrder->actual_ship_date == 'Pending' && $customerOrder->collected_date == 'Pending' && $customerOrder->item_order_status != 1004)
                            <tr style="background-color:#ffb3b3">
                        @elseif (isset($customerOrder->tracking_number) && $customerOrder->item_order_status == 1001)
                            <tr style="background-color:#b3daff">
                        @elseif ($customerOrder->item_order_status == 1002 || $customerOrder->item_order_status == 1003)
                        <tr style="background-color:#DFEFCB">
                        @else --}}
                            <tr>
                        {{-- @endif --}}
                            {{-- <td>
                                <input type="checkbox" class="cb-element largerCheckBox" name="itemsid[]" value="{{ $customerOrder->id }}" />
                            </td> --}}

                            <td>
                                <a target="_blank"
                                href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->order->purchase->getFormattedNumber().
                                (($customerOrder->order->purchase->invoice_version != 0) ? '/v'.$customerOrder->order->purchase->invoice_version.'/' : '/').$customerOrder->order->purchase->getFormattedNumber().'.pdf')}}"
                                >{{$customerOrder->order->purchase->getFormattedNumber()}}
                                </a>
                                @if (isset($customerOrder->order->purchase->inv_number))
                                    <br><small>({{$customerOrder->order->purchase->inv_number}})</small>
                                @endif
                                <hr>
                                <a target="_blank"
                                href="{{ URL::asset('storage/documents/invoice/'.$customerOrder->order->purchase->getFormattedNumber().(($customerOrder->order->purchase->invoice_version != 0) ? '/v'.$customerOrder->order->purchase->invoice_version.'/' : '/').'delivery-orders/'.$customerOrder->delivery_order.'.pdf')}}"
                                >{{$customerOrder->delivery_order}}
                                </a>
                            </td>

                            <td>
                                {!! ($customerOrder->product_code) ? $customerOrder->product_code : '<center>-</center>' !!}
                            </td>

                            <td>
                                @if(is_object( $customerOrder->product))
                                {{ $customerOrder->product->parentProduct->name }}
                                @endif

                                @if(array_key_exists('product_color_name', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Color:
                                    {{ $customerOrder->product_information['product_color_name'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_size', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Size:
                                    {{ $customerOrder->product_information['product_size'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_curtain_size', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Curtain Model:
                                    {{ $customerOrder->product_information['product_curtain_size'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_miscellaneous', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">
                                    {{ $customerOrder->product_information['product_miscellaneous'] }}
                                </p>
                                @endif
                                @if(array_key_exists('invoice_number', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Invoice Number:
                                    {{ $customerOrder->product_information['invoice_number'] }}
                                </p>
                                @endif
                                @if(array_key_exists('product_temperature', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Color Temperature:
                                    {{ $customerOrder->product_information['product_temperature'] }}</p>
                                @endif
                                @if(array_key_exists('product_preorder_date', $customerOrder->product_information))
                                <p class="text-capitalize text-secondary m-0">Pre Order Delivery:
                                    {{ $customerOrder->product_information['product_preorder_date'] }}</p>
                                @endif
                                @if(array_key_exists('product_order_selfcollect', $customerOrder->product_information) && $customerOrder->product_information['product_order_selfcollect'])
                                <p class="text-capitalize text-secondary m-0">
                                    Self Collection: Yes
                                </p>
                                @endif
                                 <!-- Trade In -->
                                @if(array_key_exists('product_order_tradein', $customerOrder->product_information) &&
                                $customerOrder->product_information['product_order_tradein'])
                                <p class="text-capitalize text-secondary m-0">Rebate: - RM
                                {{ number_format(($customerOrder->product_information['product_order_tradein'] * $customerOrder->quantity)/100),2 }}
                                {{ $customerOrder->quantity > 1 ? '( Trade-in '. $customerOrder->quantity . ' sofa )' : ''}}
                                </p>
                                @endif
                                @if ($customerOrder->bundle_id != 0)
                                <p class="text-capitalize text-secondary m-0">In Bundle :
                                    ({{$customerOrder->getParentName($customerOrder->bundle_id)}})
                                </p>
                                @endif
                            </td>

                            <td>
                                {{ ($customerOrder->order->purchase->created_at->format('d/m/Y')) ? $customerOrder->order->purchase->created_at->format('d/m/Y') : '' }}
                            </td>

                            <td>
                                {{$customerOrder->quantity }}
                                @if (isset($customerOrder->bundleChild) && ($customerOrder->bundleChild->first() != NULL) && $customerOrder->bundleChild->first()->active == 2)
                                    <hr>

                                        Quantity :
                                        @foreach ($customerOrder->bundleChild as $key => $bundle)
                                        <br>
                                            {{($bundle->primary_quantity *$customerOrder->quantity) }}@if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                            </td>
{{--
                            @if ($customerOrder->ProductWarehouseQty()->inventory->physical_stock < $customerOrder->quantity)
                                <td style="background-color:#ffb3b3">
                            @else
                                <td style="background-color:#DFEFCB">
                            @endif
                                {{$customerOrder->ProductWarehouseQty()->inventory->physical_stock }}
                            </td>

                            @if ($customerOrder->ProductWarehouseQty()->inventory->virtual_stock < $customerOrder->quantity)
                                <td style="background-color:#ffb3b3">
                            @else
                                <td style="background-color:#DFEFCB">
                            @endif
                                {{$customerOrder->ProductWarehouseQty()->inventory->virtual_stock }}
                            </td> --}}

                            {{-- @if (($customerOrder->sorted_qty < $customerOrder->quantity) && !empty($customerOrder->sorted_qty)) style="background-color:#ffb3b3" @endif --}}
                            <td>
                                {{$customerOrder->sorted_qty}}
                                @if (isset($customerOrder->bundleChild) && ($customerOrder->bundleChild->first() != NULL) && $customerOrder->bundleChild->first()->active == 2)
                                    <hr>

                                        Sorted Qty :
                                        @foreach ($customerOrder->bundleChild as $key => $bundle)
                                        <br>
                                            {{$bundle->sorted_qty}}@if (!$loop->last),@endif
                                        @endforeach
                                    @endif
                            </td>

                            {{-- <td>
                                {{$customerOrder->getPendingAttribute()}}
                            </td> --}}

                            {{-- <td>
                                @foreach ($statuses as $status)
                                @if ($status->id == $customerOrder->order->order_status)
                                {{ $status->name }}
                                @endif
                                @endforeach
                                &nbsp;/ <br>
                                {{($customerOrder->order->received_date) ? $customerOrder->order->received_date : 'Not Scan'}}
                            </td> --}}

                            {{-- <td>
                                @foreach ($statuses as $status)
                                @if ($status->id == $customerOrder->item_order_status)
                                {{ str_replace("Order","Item", $status->name ) }}
                                    @if ($status->id === 1004)
                                      <p class="text-capitalize text-secondary m-0">({{$customerOrder->order->purchase->status->name}})</p>
                                    @endif
                                @endif
                                @endforeach
                            </td> --}}

                            {{-- <td>
                                {{ $customerOrder->order->purchase->state->name }}
                            </td> --}}



                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-auto ml-auto py-3">
            {!! $customerOrders->render() !!}
        </div>

    </div>

</div>

@push('style')
    <style>
        .text-bold {
            font-weight: bold;
        }

        .table .thead-light th {
            font-weight: bold;
            font-family: 'Nunito', sans-serif;
        }

        .largerCheckBox{
            width: 18px;
            height: 18px;
        }

        @media(max-width:767px) {
            .hidden-sm {
                display: none;
            }

        }

        @media(min-width:767px) {
            .hidden-md {
                display: none;
            }
        }


        .text-font-family {
            font-family: 'Nunito', sans-serif;
        }

        /* freeze header  */
        .tableFixHead          { overflow-y: auto; height: 100vh; }
        .tableFixHead thead th { position: sticky; top: 0; }

            table  { border-collapse: collapse; width: 100%; }
            th     { background:#eee; }
    </style>
@endpush

@push('script')
    <script>
        $(function() {
            $(".date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
                });

                $(".estimatedateonly").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                changeMonth: true,
                changeYear: true
                });

                $(".filterDate").datepicker({
                dateFormat: 'yy-mm-dd',
                maxDate: 0,
                changeMonth: true,
                changeYear: true
            });
        });

        function showButton(itemID) {
            var opt = document.getElementById("courier_name" + itemID).value;
            console.log('opt');
            console.log(opt);
            if(opt == 'POSLAJU'){
                $("#createShipment_" +itemID).show();
            }else{
                $("#createShipment_" +itemID).hide();
            }
        }

        $(document).ready(function () {

            //check all checkbox
            $("#checkAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Get checked id
            $("#bulkactionform").submit(function(){
                var checked = new Array();
                var ids = new Array();

                checked = $('input[name="itemsid[]"]:checked');

                for (i = 0; i < checked.length; i++) {
                    ids.push(checked[i].value);
                    }

                var stringids = ids.toString();
                $("#action_ids").val(stringids);
            });

            //show or hide when selected bulk action
            $("#bulkaction").click(function(){

                if(bulkaction.value == 'combine_do' || bulkaction.value == 'splitPOnDO'){

                document.getElementById("action_date").style.display = "none";
                }else if(bulkaction.value == 'mark_order_date'){

                    document.getElementById("action_date").className = "estimatedateonly form-control";

                    $(".estimatedateonly").datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else if(bulkaction.value == 'mark_actual_ship_date'){

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }else{

                    document.getElementById("action_date").className = "date form-control";

                    $(".date").datepicker({
                    dateFormat: 'yy-mm-dd',
                    maxDate: 0,
                    changeMonth: true,
                    changeYear: true
                    });

                    document.getElementById("action_date").style.display = "block";
                }
            });
        });

        function getShipmentData(itemID) {
            console.log(itemID);
            var element = document.getElementById("spinnerSubmit" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/create_shipment/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("createShipment_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Shipment Create";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        alert(result);


                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuCheckout(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/poslaju_checkout/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        button =  document.getElementById("checkout_" +itemID);
                        button.disabled = true;
                        button.innerHTML = "Checkout";
                        $("#tracking"+itemID).submit();
                    } else {
                        element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function PoslajuShip(itemID) {
            console.log(itemID);
            var element = document.getElementById("checkoutSpinner" + itemID);
            $.ajax({
                async: true,
                beforeSend: function() {
                    // setting a timeout
                    element.classList.remove("d-none");
                },
                complete: function() {
                    element.classList.add("d-none");
                },
                url: "courier/poslaju_ship/" + itemID,
                type: "GET",
                success: function(result) {
                    console.log(result);
                    if (result == 'success') {
                        // button.disabled = true;
                        // button.innerHTML = "Checkout";
                        // $("#actualSubmit").submit();
                    } else {
                        // element.classList.add("d-none");
                        location.reload();
                    }
                },
                error: function(result) {
                    console.log(result.status + ' ' + result.statusText);
                }
            });
        }

        function exportTasks(_this) {
            let _url = $(_this).data('href');
            window.location.href = _url;
        }

    </script>
@endpush

@endsection
