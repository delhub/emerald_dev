<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $pageTitle }}</title>
     @include('layouts.favicon')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
            body {
        width: 100%;
        height: 100%;
        margin: 0 auto;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 210mm;
        min-height: 200mm;
        padding: 4.5mm 10.5mm ;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px; */
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    .my-style{
        background: rgb(255 255 255);
        height: 17mm;
        width: 27mm;
        /* border: 1px dotted #eaeaea;
        border-radius: 20px; */
        float: left;
    }
    .itemid {
        display: block;
        font-size: 7px;
        position: relative;
        top: -12px;
    }
    @page {
        size: A4;
        padding-top: 4.8mm;

    }

    @page :first {
        padding-top: 0mm
    }

    @media print {
        body {
            width: 210mm;
            height: 297mm;
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }

    }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-lg-12 mx-3 my-3">
            <strong>Product Code</strong> : {{$attribute->product_code}} <br>
            <strong>Product Name</strong> : {{$attribute->product2->parentProduct["name"]}} <br>
            <strong>Product Desc</strong>: {{$attribute->product2['product_description']}} <br>
            <strong>Fixed Price</strong>: {{country()->country_currency}} {{ $attribute->getPriceAttributes('fixed_price') ? number_format(($attribute->getPriceAttributes('fixed_price') / 100), 2) : 0 }} <br>
            <strong>Offer Price</strong>: {{country()->country_currency}} {{ $attribute->getPriceAttributes('offer_price') ? number_format(($attribute->getPriceAttributes('offer_price') / 100), 2) : 0 }} <br>
        </div>
    </div>

</body>
</html>
