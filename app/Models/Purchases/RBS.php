<?php

namespace App\Models\Purchases;

use Illuminate\Database\Eloquent\Model;

class RBS extends Model
{
    // Set table
    protected $table = 'purchases_rbs';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function purchase()
    {
        return $this->hasOne('App\Models\Purchases\Purchase', 'purchase_id', 'id');
    }

    public function getRbsExpiredDate($created_at)
    {
        return $created_at->addMonths($this->total_months);
    }
}
