<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Products\ProductAttribute;

class ProductStockImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $rows[0][0] = str_slug($rows[0][0]);
        $rows[0][1] = str_slug($rows[0][1]);
        $rows[0][2] = str_slug($rows[0][2]);
        $rows[0][3] = str_slug($rows[0][3]);

        foreach ($rows as $k => $v) {
            if($k > 0) {
                foreach ($rows[0] as $k1 => $v1) {
                    $b[$v1] = $v[$k1];
                }

                $data[] = $b;
            }
        }

        if(count($data) > 0) {
            foreach ($data as $k => $v) {
                if(isset($v['stock']) && !empty($v['stock']) && $v['stock'] > 0) {
                    $this->updateProductAttribute($v);
                }
            }

            return 'data has been imported successfully...';
        }

    }

    public function updateProductAttribute($data) {
        $product_att = ProductAttribute::where('product_code',$data['product-code'])->first();

        if($product_att) {
            $product_att->stock = $data['stock'];
            $product_att->save();
        }
    }
}
