<?php

namespace App\Models\Registrations\Dealer;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    /**
     * Handle dealer registration application.
     */

    /**
     * Table 2 d
     */
    protected $table = 'dealer_registrations';

     // find dealer info & connect user info, get user info
     public function dealerInfo(){
        
        return $this->belongsTo('App\Models\Users\Dealers\DealerInfo' , 'buyer_id', 'user_id');
    }
}
