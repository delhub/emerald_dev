
<form action="" >
    @if (isset($products) && isset($products->panelProduct))


   <div class="ft-cart-content">
      <div class="item-variation">
         <div class="variation-title">
            <h4>Variation</h4>
         </div>
         <ul class="d-flex">
            @foreach ($products->panelProduct->attributes as $attributes)
                @php
                    $productLimit = null;
                    $roleLimit = false;
                    $totalPurchase = 0;
                    $period = null;
                    $purchaseLeft = 0;

                    if (Auth::check() && isset($attributes) && $attributes->productLimit->isNotEmpty()) {
                        $productLimit = $attributes->productLimit->where('user_id', auth()->user()->id)->first();
                        if(!$productLimit)$productLimit = $attributes->productLimit->where('user_id', -1)->first();
                        if(!$productLimit) continue;
                        $result = $productLimit->checkPurchaseLimit(auth()->user()->id, $productLimit);

                        $totalPurchase = $result['total_purchase'];
                        $period = $result['period'];
                        $roleLimit = $result['purchase_limit_eligible'];
                        $purchase_left = $productLimit->amount - $totalPurchase;
                        if ($purchase_left < 0) $purchase_left = 0;
                    }
                @endphp
            <li>

                <div class="variation-box">
                  <label>
                     <input type="radio" name="attributesPrice" value="vrt-{{ $attributes->id }}"  id="attributesPrice{{ $attributes->id }}"
                        data-offer-price="{{ number_format(($attributes->getPriceAttributes('web_offer_price') /100),2) }}"
                        data-fixed-price="{{ number_format(($attributes->getPriceAttributes('fixed_price')/100),2) }}"
                        data-advance-price="{{ number_format(($attributes->getPriceAttributes('price')/100),2) }}"
                        data-premium-price="{{ number_format(($attributes->getPriceAttributes('member_price')/100),2) }}"
                        data-product-name="{{ $products->name }}"
                        data-variation-name="{{ $attributes->attribute_name }}"
                        data-sales-count="{{ $attributes->sales_count }} "
                        data-variation-id="{{ $attributes->id }}" {{ $loop->first ? 'checked' : '' }}
                        data-rbs-months=""
                        data-eligible-purchase="{{ ($productLimit && $roleLimit) ? $purchase_left : null }}"
                        data-purchase-limit-amount="{{ ($productLimit && $roleLimit) ? $productLimit->amount : null }}"
                        data-purchase-limit-period="{{ $period }}"
                        data-allow-reminder="{{ $attributes->allow_reminder }}">
                     @if ($images->where('id',$attributes->color_images)->first())
                     <img src="{{asset('storage/'.$images->where('id',$attributes->color_images)->first()->path.$images->where('id',$attributes->color_images)->first()->filename)}}">
                        @else
                     <img src="{{asset('/images/product_item/test_productitem.png')}}">
                        @endif
                  </label>
                  <p>{{ $attributes->attribute_name }}</p>
                </div>
                @if (Auth::check() && $productLimit && $roleLimit)
                    <p class="limit-text-variation"> You have a remaining balance of {{ $purchase_left }} unit(s) for purchase {{ $period }} </p>
                @endif
            </li>
            @endforeach
         </ul>
      </div>
   @if (Auth::check())
   {{--Add Recurring Booking System(RBS) start --}}
   @foreach ($products->panelProduct->attributes as $attributes)
   {{-- @if ($products->panelProduct->attributes->count() > 0) --}}
   <div class="rbs-col rbs-col-hide active pointerEvents" id="rbsDrop{{ $attributes->allow_rbs == 1 ? $attributes->id : 'xx' }}" style="{{ $loop->first && $attributes->allow_rbs == 1  ? '' : 'display:none;' }}">
      <div class="accordion" id="accordionExample">
         <div class="accordion-item">
            <div class="accordion-header title" id="headingOne">
             <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $attributes->id }}" aria-expanded="false" aria-controls="collapseOne">
               <div class="tit-group">
                  <div class="icon-rbs1">
                     <img src="{{ asset('/images/icons/rbs-slc.png') }}" alt="" >
                  </div>
                  <h3>Recurring Booking System(RBS)</h3>

                  {{-- <div class="remind-bell" onclick="funcRemind()">
                      <img src="{{ asset('/images/icons/rbsbell.png') }}" alt="" >
                  </div> --}}
               </div>
             </button>
             <span>*RBS have been removed!</span>
            </div>
           <div id="collapse{{ $attributes->id }}" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
             <div class="accordion-body">
               <div class="add-pd">
                  <h5>Delivery frequency</h5>
                  <div class="rbs-time-slc">
                     @foreach ($rbsReminders->whereIn('id', $attributes->rbs_frequency) as $rbsReminder)
                     <div class="list-rbs">
                        <label>
                           <input type="radio" name="month" id="permonth" value="{{ $rbsReminder->rbs_data }}">
                           <div class="dot"></div>
                           {{ $rbsReminder->rbs_label }}
                        </label>
                     </div>
                     @endforeach
                  </div>
               </div>
               <hr>
               <div class="add-pd">
                  <h5>Total times of delivery</h5>
                  <div class="rbs-qlt-slc">
                     @if ($attributes->rbs_times_send)
                     @foreach ($attributes->rbs_times_send as $keys => $rbs_times_send)
                     @php
                         $rbs = $rbsReminders->where('id',$keys)->where('option_status','active')->first();
                     @endphp
                        @if ($rbs)
                        <div class="list-rbs">
                           <label>
                              <input type="radio" name="dlvrtime" id="itme01" value="{{ $rbs->rbs_data }}" data-pricePer="{{ $rbs_times_send['price'] }}"
                              data-shippingCat="{{ array_key_exists('shipping_category',$rbs_times_send) ?  $rbs_times_send['shipping_category'] : 0 }}" data-quantity="{{ $rbs_times_send['qty'] }}">
                              <div class="dot"></div>
                              {{ $rbs->rbs_label }}
                           </label>
                        </div>
                        @endif
                     @endforeach
                     @endif

                  </div>
               </div>
               <hr>
               <div class="add-pd">
                  <div class="rbs-total">
                     <div class="title">
                        <div class="tit-group">
                           <div class="icon-rbs">$</div>
                           <h3>Total RBS Price</h3>
                           <p class="rbs_total">MYR <span>2640.00</span>*</p>
                        </div>
                     </div>
                  </div>
               </div>
             </div>
           </div>
         </div>
       </div>
      
      {{-- <div class="list-box">
         @foreach ($products->panelProduct->attributes as $attributes)
          <div class="price-box">
              <input type="checkbox" name="attributesPrice" id="rbs-price{{ $attributes->id }}"
              class="rbs-price inputRBS" value="rbs-{{ $attributes->id }}" data-variation-id="{{ $attributes->id }}" data-rbs-months="" data-sales-count="{{ $attributes->sales_count }}"
              data-offer-price="{{ number_format(($attributes->getPriceAttributes('web_offer_price') /100),2) }}"
              data-fixed-price="{{ number_format(($attributes->getPriceAttributes('fixed_price')/100),2) }}"
              data-advance-price="{{ number_format(($attributes->getPriceAttributes('price')/100),2) }}"
              data-premium-price="{{ number_format(($attributes->getPriceAttributes('member_price')/100),2) }}"
              data-variation-name="{{ $attributes->attribute_name }}" {{ $loop->first ? 'checked' : '' }}>
              <label for="rbs-price{{ $attributes->id }}"></label>
              <h3><span>MYR </span>{{ number_format(($attributes->getPriceAttributes('price') /100),2) }}</h3>
              <p class="item-note">({{ $attributes->rbs_total_months }} Month)</p>
              <p class="item-note">{{ $attributes->attribute_name }}</p>
          </div>
          @endforeach
      </div> --}}
   </div>
   @endforeach
   @endif
   {{-- @endif --}}
   {{--Add Recurring Booking System(RBS) end --}}

      @if (Auth::check())
      <div class="item-buy-section">
         <div class="item-quality">
            <h6 class="rbsQuantity">Quantity</h6>
            <div class="item-buy-section-quantity" >
               <button class="product-minus-btn " type="button" name="fml-minus-btn">
                  <i class="fa fa-minus" aria-hidden="true"></i>
               </button>
               <input type="text" name="productQuantity" value="" id="varqty" class="varqty">
               <button class="product-add-btn" type="button" name="fml-add-btn">
                  <i class="fa fa-plus" aria-hidden="true"></i>
               </button>
            </div>
         </div>
         <div class="remind-bell" onclick="funcRemind()" @if($products->panelProduct->attributes->first()->allow_reminder != 1) style="display:none;" @endif>
            <img src="{{ asset('/images/icons/rbsbell.png') }}" alt="" >
        </div>
         <div class="clickme-info web" style="{{ $products->show_warning == 1 ? 'display: block':'display: none'}}">
            <a data-bs-toggle="modal" href="#waringInfo" id="warning-modal" data-button="Back" role="button">
               <img src="{{asset('/images/icons/clickme-icon.png')}}" alt="">
            </a>
         </div>
         <div class="@if($products->show_warning == 1) item-buy warning @else item-buy @endif">
                  {{-- <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart()'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a> --}}

                  {{-- <a id="buynowft" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart()'}}" href="{{ $products->display_only == 1 ?  '#' : '/shop/cart'}}" class="link-product buy_now" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}">
                     <div id="showCart">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>TWO<span id="cart-quantity-ft" class="ftitemCount">0</span>ZXCZXC
                     </div>
                     <div class="p-m-add-btn"  >
                        Buy NowASDASDAS
                     </div>
                  </a> --}}

                  @if($products->show_warning == 1 && $products->display_only == 0)

                  <a id="warning-modal" data-button="Proceed" data-bs-toggle="modal" href="#waringInfo-buy-cart" role="button" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  <a class="buy-btn link-product buy_now warning" id="warning-modal" data-button="Proceed" data-bs-toggle="modal" href="#waringInfo-buy-cart" role="button">
                     Buy Now
                  </a>
                     {{-- <a class="buy-btn link-product buy_now" onclick="{{ $products->display_only == 1 ?  '' : "ajaxSaveCart(),setTimeout(function(){window.location.href = '/shop/cart'; }, 500);"}}" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}">
                        Buy Now
                        </a> --}}

                  @elseif($products->show_warning == 0 && $products->display_only == 1)

                  <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart(false)'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  <a href="#" class="buy-btn link-product buy_now one">
                     Buy Now
                  </a>

                  @elseif($products->display_only == 1 && $products->show_warning == 1)
                  <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart(false)'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  <a href="#"  class="buy-btn link-product buy_now one warning">
                     Buy Now
                  </a>

                  @else

                  <a id="DeleteCartItem" onclick="{{ $products->display_only == 1 ?  '' : 'ajaxSaveCart(false)'}}" href="{{ $products->display_only == 1 ?  '#' : ''}}" class="link-product add_cart" >
                     <div class="p-m-add-btn" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}" >
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                     </div>
                  </a>

                  {{-- this is the show cart number start --}}
                  {{-- <div id="showCart">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-quantity-ft" class="ftitemCount">0</span>
                  </div> --}}
                  {{-- this is the show cart number end --}}

                  <a class="buy-btn link-product buy_now" onclick="{{ $products->display_only == 1 ?  '' : "ajaxSaveCart(true)" }}" style="{{ $products->display_only == 1 ?  'background:darkgrey;cursor:pointer;' : ''}}">
                     Buy Now
                  </a>
                  @endif

                  {{-- <div id="showCart">
                     <i class="fa fa-shopping-cart" aria-hidden="true"></i><span id="cart-quantity-ft" class="ftitemCount">0</span>
                  </div>
                  @if($products->show_warning == 1)
                  <a id="buynowft" class="link-product buy_now one" id="warning-modal" data-button="Proceed" data-bs-toggle="modal" href="#waringInfo" role="button">
                     Buy Now
                     </a>
                  @else
                  <a id="buynowft" type="button" class="link-product buy_now" onclick="{{ $products->display_only == 1 ?  '' : "ajaxSaveCart(),setTimeout(function(){window.location.href = '/shop/cart'; }, 500);"}}">
                     Buy Now
                     </a>
                  @endif --}}

         </div>
         {{-- RBS Noted --}}
         <span class="noteRbs-add-mobile"></span>
         <span class="noteRbs-add-cart"></span>
      </div>
      @else
      <div class="view-product-without-login-box">
         Register as member to purchase the product 
         <a href="/" class="buy-btn">Login/Register</a>
      </div>
      @endif
      {{-- add RBS notice --}}
      <div class="rbs-note"></div>

   </div>
   @endif

 </form>

