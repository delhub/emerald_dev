<?php

use App\Http\Controllers\Register\CustomerRegisterController;
use App\Models\Users\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class AddAccountmanagerUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $users = [
            ['email' => 'acc.fhcsb@gmail.com', 'name' => 'Shu Fung'],
            ['email' => 'formulahealthcare.acc@gmail.com ', 'name' => 'Xin Chen'],
            ['email' => 'mags.jeannelim@gmail.com ', 'name' => 'Jeanne']
        ];

        foreach ($users as $user) {
            $currentUser = User::where('email', $user['email'])->first();

            if (!is_null($currentUser)) {
                $currentUser->assignRole('account_manager');
                continue;
            }

            $request = new Request();

            $request->merge([
                'email' => $user['email'],
                'password' => 'account123',
                'full_name' => $user['name'],
                'nric' => random_int(100000000000, 999999999999),
                'address_1' => 'Menara Bangkok Bank',
                'state' => 14,
                'city_key' => 'KUL_AMP',
                'postcode' => '12345',
                'contact_number_mobile' => '0123456789',
                'referrer_id' => '3911000009',
                'referrer_name' => 'Formula',
            ]);

            $account = CustomerRegisterController::createCustomer($request, true, 'MY');

            $account->notes = null;
            $account->save();

            $account->assignRole('account_manager');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
