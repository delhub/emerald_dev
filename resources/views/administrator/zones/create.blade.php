@inject('controller', 'App\Http\Controllers\Administrator\Zones\ZonesController')

@extends('layouts.administrator.main')

@section('content')

    <div class="row">
        <div class="col-12">
            {{-- href="/administrator/faq" --}}
            <a href="{{ route('administrator.zones.index') }}"  class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <form action="{{ route('administrator.zones.store')}}" id="edit-zones-form" method="POST" enctype= "multipart/form-data">

        @csrf
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">

                    <h4>
                        Create Zones
                    </h4>

                    {{-- @include('inc.messages') --}}
                </div>

                <div class="col-12">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Country
                                </p>
                            </div>

                            <div class="col-12 col-md-5 form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                <select class="select2 form-control" name="country_id">
                                    @foreach($countries as $key => $country)
                                        <option value="{{ $country->country_id }}">{{ $country->country_name}}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Name
                                </p>
                            </div>

                            <div class="col-12 col-md-5 form-group{{ $errors->has('zone_name') ? ' has-error' : '' }}">
                                <input type="text" name="zone_name" id="zone_name" class="form-control" value="{{ old('zone_name') }}" required>

                                <small class="text-danger">{{ $errors->first('zone_name') }}</small>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Sequence
                                </p>
                            </div>

                            <div class="col-12 col-md-5 form-group{{ $errors->has('sequence') ? ' has-error' : '' }}">
                                <input type="text" name="sequence" id="sequence" class="form-control"  value="{{ old('sequence') }}" required>

                                <small class="text-danger">{{ $errors->first('sequence') }}</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Zone Locations <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-5 form-group">
                                <select class="select2 form-control productCategory" id="zoneLocations" name="zoneLocations[]" multiple="multiple"  required>
                                    @foreach($locations as $key => $location)
                                    <option value="{{ $key }}">{{ $location}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        {{-- <div class="row">
                            <div class="col-12 col-md-9 text-md-right my-auto">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div> --}}

                </div>
            </div>
        </div>
    </div>

    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>
                        Rate Table
                    </h4>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-12 col-md-12 form-group" id="rateContainer">
                            <div class="row border mx-md-1 py-md-4 px-md-2 default-item" >
                                @include('administrator.shipping-installations.rate-table')
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-auto ml-md-auto mx-md-4">
                            <button type="submit" class="btn btn-primary" value="submit">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $(".select2").select2();
    });

    function addDeliveryRangeElement() {
        let deliveryRangeContainer = $('#deliveryRangeContainer')

        let currentItem = deliveryRangeContainer.find('.default-item');

        // currentItem.find('select.select2').select2('destroy');
        currentItem.find('.btn.btn-danger').removeClass('d-none');
        currentItem.find('.btn.btn-success').addClass('d-none');
        currentItem.removeClass('default-item');

        let cloneItem = currentItem.clone(false).hide();

        // cloneItem.find('select.select2').removeAttr('data-select2-id').removeAttr('id');
        // cloneItem.find('option').removeAttr('data-select2-id');
        cloneItem.addClass('default-item');
        cloneItem.find('.btn.btn-danger').addClass('d-none');
        cloneItem.find('.btn.btn-success').removeClass('d-none');
        cloneItem.appendTo(deliveryRangeContainer);
        cloneItem.slideDown();
    }

    function removeDeliveryRangeElement(element) {
        $(element).parent().parent().slideUp(500, function() {
            $(this).remove();
        });
    }

</script>
@stack('rateJS')
@endpush
