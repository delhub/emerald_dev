<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\UserInfo;
class UserCountry extends Model
{
    // Set table
    protected $table = 'user_country';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        // 'contact_num',
        // 'is_emergency'
    ];

    /**
     * Get the user contact associated with the user.
     */

    public function userInfo()
    {
        return $this->belongsTo(UserInfo::class, 'account_id','account_id');
    }
}
