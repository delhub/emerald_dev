<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Discount\Code;
use App\Models\Discount\Rule;
use Carbon\Carbon;

class VoucherCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check voucher status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $exp = Code::where('expiry', '<' , now())->get();

        if(count($exp)) {
            foreach ($exp as $val) {
                $this->updateVoucherExpiredStatus($val->id);
            }

            echo 'Finish voucher Checked...';
        }
    }

    function updateVoucherExpiredStatus($id) {

        if(isset($id) && !empty($id)){
            $code = Code::find($id);

            if($code) {
                $code->coupon_status ='expired';
                $code->save();
            }
        }

    }
}
