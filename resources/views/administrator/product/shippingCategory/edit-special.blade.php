<div class="form-row">
    <div class="col-12 col-md-4 my-auto text-right">
        <p>Panel Product Name / Attribute Name</p>
    </div>
    <div class="col-12 col-md-6 form-group">
        <p class="form-control" readonly>
            {{$productAttribute->attribute_name}}

        </p>
    </div>
</div>

<div class="form-row">
    <div class="col-12 col-md-4 my-auto text-right">
        <p>Prouct Code</p>
    </div>
    <div class="col-12 col-md-6 form-group">
        <p class="form-control" readonly>
            {{$productAttribute->product_code}}
        </p>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-12 col-md-4 text-md-right my-auto">
        <p>
            Shipping Category Special<small class="text-danger">*</small>
        </p>
    </div>
    <div class="col-12 col-md-8 form-group">
        <select name="markup_category_special[]" id="markup_category_special"
            class="select2 form-control">
            @foreach($shippingCategoriesSpecial as $category)
            <option value="{{ $category->id }}" data-markup-name-special="{{$category->cat_name}}"
                {{ ($productAttribute->shipping_category_special ==  $category->id) ? 'selected' : '' }}
            >Name : {{ $category->cat_name }}, Description : {{ $category->cat_desc}}, Type {{ $category->cat_type}}</option>
            @endforeach

        </select>
    </div>
</div>

<div class="form-row mt-2">
    <div class="col-12 col-md-6 offset-md-4">
        <button id="modalSubmitBtn" name="modalSubmitBtn" class="btn btn-primary submitBttn modalSubmitBtn" data-item-id="{{ $productAttribute->id }}">{{ $productAttribute->id }}
            <i class="fa fa-save"></i>
            <span class="spinner-border spinner-border-sm" style="display: none;" role="status" aria-hidden="true"></span>
            Submit
        </button>

    </div>
</div>
