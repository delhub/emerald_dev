<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveCashPointsToPanelPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->dropColumn('fixed_price');
            $table->dropColumn('price');
            $table->dropColumn('member_price');
            $table->dropColumn('offer_price');
            $table->dropColumn('active_price');
            $table->dropColumn('promo_price');
            $table->dropColumn('default_price');
            $table->dropColumn('point');
            $table->dropColumn('point_cash');
        });

        Schema::table('panel_product_prices', function (Blueprint $table) {
            $table->text('point_cash')->nullable()->after('offer_price');
            $table->bigInteger('point')->default(0)->after('offer_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            //
        });
    }
}
