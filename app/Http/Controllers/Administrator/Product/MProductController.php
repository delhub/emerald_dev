<?php

namespace App\Http\Controllers\Administrator\Product;

use App\Models\Users\Dealers\DealerInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product\Product;
use App\Models\Categories\Category;
// use App\Models\Globals\Products\ProductAttribute;
use Illuminate\Support\Facades\File;
// use App\Models\Categories\Quality;
use App\Models\Globals\Quality;
use App\Models\Globals\Products\Product as GlobalProduct;
use Illuminate\Support\Str;
use App\Models\Products\ProductBrand;



use App\Models\Products\Product as PanelProduct;
// use App\Models\Globals\Products\ProductAttribute;
use App\Models\Products\ProductAttribute;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Users\User;
use App\Models\Globals\State;
use App\Models\Globals\Image;
use App\Models\ShippingInstallations\Ship_category;
use App\Models\Users\Panels\PanelCategories;
use App\Models\Products\ProductMarkupCategory;
use Illuminate\Support\Facades\Auth;
use App\Models\Products\ProductPrices;
use App\Models\Products\ProductBundle;
use App\Models\Rbs\RbsOptions;
use App\Traits\Voucher;
use Illuminate\Support\Facades\Artisan;
use App\Models\Products\ProductAttributeBundle;
use App\Models\Products\ProductAttributeLimit;
use Spatie\Permission\Models\Role;

class MProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    use Voucher;
    public static $category_type;
    public static $rbs_options;

    public function __construct()
    {
        self::$category_type = array(
            'suitable_for' => 'Suitable For',
            'shop' => 'Product Categories',
            'origin_country' => 'Origin Country',
            'product_care' => 'Product Care',
            'agent' => 'Agent Report Categories',
            'accounting' => 'Accounting Categories',
            'exclude_country' => 'Exclude Country(For search purpose)'
        );
        self::$rbs_options = array(
            'frequency' => 'Frequency',
            'times' => 'Times to send'
        );
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = GlobalProduct::orderBy('created_at', 'DESC')
            ->select('global_products.*')
            ->searchFilter($request->searchBox, $request->selectSearch);

        $ForCountproducts = $products->get();

        $products = $products->paginate(25);
        $products->appends(request()->query());

        return view('administrator.product.index')
            ->with('products', $products)
            ->with('ForCountproducts', $ForCountproducts)
            ->with('request', $request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $newGlobalProduct = new GlobalProduct();
        $newGlobalProduct->product_status = 0;
        $newGlobalProduct->can_installment = 0;
        $newGlobalProduct->product_code = $request->productCode;
        $newGlobalProduct->product_type = 'single';
        $newGlobalProduct->save();

        $newPanelProduct = new PanelProduct();
        $newPanelProduct->global_product_id = $newGlobalProduct->id;

        $newPanelProduct->save();

        return redirect(route('administrator.product.editCreate', ['productId' => $newGlobalProduct->id]));
    }

    public function editCreate($id)
    {
        $user = User::find(Auth::user()->id);

        $roles = Role::get();
        $qualities = Quality::get();
        $categories = Category::get();
        $rbsChoices = RbsOptions::get();
        $panels = PanelInfo::get();
        $states = State::get();
        $shippingCategory = Ship_category::get();
        $productBrand = ProductBrand::get();
        $markupCategories = ProductMarkupCategory::get();

        $globalProduct = GlobalProduct::findOrFail($id);
        $panelproduct = PanelProduct::where('global_product_id', $id)->first();

        $images = $globalProduct->images;
        $firstCategory = $globalProduct->categories->first();
        $deliveries = PanelCategories::where('category_id', $firstCategory)->get();


        $productAttributes = ProductAttribute::selectRaw('panel_product_attributes.id, panel_product_attributes.product_code, panel_product_attributes.attribute_name, global_products.name,global_products.product_status,panel_product_attributes.active as attribute_active')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')

            // ->where('panel_product_attributes.active', 1)
            // ->where('global_products.product_status', '=', 1)
            ->where('global_products.no_stockProduct', 0)

            ->orderBy('panel_product_attributes.product_code', 'DESC')
            ->get();

        //correct way (input form) can refer product name and  productQuality

        return view('administrator.product.editCreate')
            ->with('user', $user)
            ->with('roles', $roles)
            ->with('qualities', $qualities)
            ->with('categories', $categories)
            ->with('rbsChoices', $rbsChoices)
            ->with('panels', $panels)
            ->with('states', $states)
            ->with('shippingCategory', $shippingCategory)
            ->with('productBrand', $productBrand)
            ->with('markupCategories', $markupCategories)

            ->with('globalProduct', $globalProduct)
            ->with('panelproduct', $panelproduct)

            ->with('images', $images)
            ->with('firstCategory', $firstCategory)
            ->with('deliveries', $deliveries)

            ->with('productAttributes', $productAttributes);
    }

    public function getAgentList(Request $request){
        $attr_id = $request->attr_id;
        $limit_type = $request->limit_type;
        $attribute = ProductAttribute::find($attr_id);
        $datas = $attribute->productLimit;
        foreach($datas as $data){
            $data->roles = json_decode($data->roles, true);
        }
        return response()->json([
            'datas' => $datas,
        ]);
    }

    public function removeRestrict(Request $request){
        $request->validate([
            'attr_id' => 'required',
        ]);
        $attribute = ProductAttribute::find($request->attr_id);
        $limits = $attribute->productLimit;
        foreach($limits as $limit){
            $limit->active = '0';
            $limit->save();
        }
        $attribute->purchase_limit_type = 'no';
        $attribute->save();
        return redirect()->route('administrator.product.product-restriction')->with('errornotice', 'Restriction Removed');

    }

    public function updateRestrict(Request $request){
        $request->validate([
            'limit_type' => 'required',
            'attr_id' => 'required',
        ]);
        $limit_type = $request->limit_type;
        $attr_id = $request->attr_id;

        $limit_ids = $limit_type == 'agent' ? $request->limit_id : [$request->product_limit_id];

        $attributeslimitCompares = ProductAttributeLimit::where('active', 1)->where('attribute_id', $attr_id)->whereNotIn('id', $limit_ids)->get();
        foreach ($attributeslimitCompares as $attributeslimitCompare) {
            $attributeslimitCompare->active = 0;
            $attributeslimitCompare->save();
        }

        $attribute = ProductAttribute::find($request->attr_id);
        switch ($limit_type) {
            case 'agent':
                $request->validate([
                    'limit_type' => 'required',
                    'agent_limit_duration' => 'required',
                    'agent_limit_id' => 'required',
                    'agent_limit_amt' => 'required',
                ]);
                $durations = $request->agent_limit_duration;
                $agent_ids = $request->agent_limit_id;
                $agent_amt = $request->agent_limit_amt;
                
                if(array_unique($agent_ids) !== $agent_ids) return redirect()->route('administrator.product.product-restriction')->with('errornotice', 'Duplicate Dealer');
                
                foreach($limit_ids as $key => $ids){
                    $limits = ProductAttributeLimit::firstOrNew(
                        [
                            'id' => $ids,
                        ]
                    );
                    $limits->attribute_id = $attr_id;
                    $limits->product_code = $attribute->product_code;
                    $limits->duration = $durations[$key] ?? '';
                    $limits->roles = json_encode(array_map('strval', ["dealer"]));
                    $limits->amount = $agent_amt[$key] ?? '';
                    $limits->user_id = $agent_ids[$key];
                    $limits->active = '1';
                    $limits->save();
                }
                break;
            case 'roles':
                $request->validate([
                    'product_limit_duration' => 'required',
                    'product_limit_roles' => 'required',
                    'purchase_limit_amount' => 'required',
                ]);

                $limit_id = $request->product_limit_id;
                $duration = $request->product_limit_duration;
                $roles = $request->product_limit_roles;
                $amount = $request->purchase_limit_amount;
                
                $limits = ProductAttributeLimit::firstOrNew(
                    [
                        'id' => $limit_id,
                    ]
                );

                $limits->attribute_id = $attr_id;
                $limits->product_code = $attribute->product_code;

                $limitRole = '';
                $limitRoles = $roles;
                if ($limitRoles) {
                    $convertedRoles = array_map('strval', $limitRoles);
                    $limitRole = json_encode($convertedRoles);
                }

                $limits->duration = $duration ?? '';
                $limits->roles = $limitRole;
                $limits->amount = $amount ?? '';
                $limits->user_id = -1;
                $limits->active = '1';

                $limits->save();

                break;
            default:
                # code...
                break;
        }
        $attribute->purchase_limit_type = $request->limit_type;
        $attribute->save();

        return redirect()->route('administrator.product.product-restriction')->with('successnotice', 'Restriction Updated');
    }

    public function productRestriction(Request $request){
        $limitList = ProductAttributeLimit::where('active', 1)
            ->with('productAttribute.product2.parentProduct', 'user.dealerInfo')
            ->searchFilter($request)
            ->get();
            // ->groupBy('product_code')
            // ->toArray();
        // dd($limits);
        $dealers = DealerInfo::where('account_status', 3)->where('full_name', '!=', NULL)->get();

        $limits = [];
        foreach($limitList as $limit){
            if(!array_key_exists($limit->product_code, $limits)){
                $limits[$limit->product_code] = [
                    'attribute_id' => $limit->attribute_id,
                    'limit_type' => $limit->productAttribute->purchase_limit_type,
                    'product_name' => $limit->productAttribute->product2->parentProduct->name . ' (' . $limit->productAttribute->attribute_name . ')',
                ];
            }
            $limits[$limit->product_code]['details'][] = [
                'roles' => implode(', ', json_decode($limit->roles)),
                'duration' => $limit->duration,
                'amount' => $limit->amount,
                'name' => $limit->user_id != -1 ? $limit->user->dealerInfo->full_name : 'Other',
                'account_id' => $limit->user_id != -1 ? $limit->user->dealerInfo->account_id . ' - ' : '',
            ];
        }

        $roles = Role::get();
        $prodAttrNoLimit = ProductAttribute::with(['product2' => function ($query) {
                $query->select('id', 'global_product_id');
            }, 'product2.parentProduct' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where('purchase_limit_type', 'no')->get();
            
        return view('administrator.product.restriction')
                ->with('limits', $limits)
                ->with('roles', $roles)
                ->with('prodAttr', $prodAttrNoLimit)
                ->with('dealers', $dealers)
                ->with('request', $request);
    }
    
    public function updateCreate(Request $request, $id)
    {
        // dd($request->all());
        // will un comment
        $request->validate([
            'product_variation' => ['required', 'max:255'],
            // 'panel_id' => ['required', 'max:255'],
            // 'display_panel_name' => ['required', 'max:255'],
            // 'ship_from' => ['required', 'max:255'],
            // 'shipping_category' => ['required', 'max:255'],
        ]);


        // -------------------------------------- Global Product ---------------------------------------------------//

        $ingredients = $suitable_for = $categories = array();

        foreach ($request->input('ingredients_key') as $key => $key_data) $ingredients[$key_data] = $request->input('ingredients_desc')[$key];
        // foreach ($request->input('suitable_for_key') as $key => $key_data) $suitable_for[$key_data] = $request->input('suitable_for_desc')[$key];

        // foreach (self::$category_type as $key => $type) $categories =  array_merge($categories, $request->input($key . 'categories'));

        foreach (self::$category_type as $key => $type) {

            if ($request->input('product_carecategories') == NULL && $key == 'product_care') continue;
            if ($request->input('suitable_forcategories') == NULL && $key == 'suitable_for') continue;
            if ($request->input('agentcategories') == NULL && $key == 'agent') continue;
            if ($request->input('accountingcategories') == NULL && $key == 'accounting') continue;
            if ($request->input('exclude_countrycategories') == NULL && $key == 'exclude_country') continue;

            if ($request->input('origin_countrycategories') == NULL && $key == 'origin_country') continue;
            if ($request->input('shopcategories') == NULL && $key == 'shop') continue;

            $categories =  array_merge($categories, $request->input($key . 'categories'));
        }


        $product = GlobalProduct::find($id);

        $product->product_type = $request->input('product_type');
        $product->product_code = $request->input('productCode');
        $product->name = $request->input('productName');
        $product->name_slug = $id . '-' . Str::slug($request->input('productName'), '-');
        $product->ingredients = json_encode($ingredients);
        $product->show_warning = ($request->input('show_warning') != 1) ? 0 : 1;
        // $product->warning = $request->input('warning');
        // $product->caution = $request->input('caution');
        $product->quality_id = $request->input('productQuality');
        $product->brand_id = $request->input('brand_id');
        $product->key_selection = $request->input('keySelection') ?? 0;
        $product->hot_selection = $request->input('hotSelection') ?? 0;
        $product->display_only = $request->input('displayOnly') ?? 0;


        if ($request->input('productPublished')) {
            $product->product_status = 1;
        } else {
            $product->product_status = 2;
        }

        if ($request->input('mespayPurchaseable')) {
            $product->mespay_purchaseable = 1;
        } else {
            $product->mespay_purchaseable = 0;
        }

        if ($request->input('eligibleDiscount')) {
            $product->eligible_discount = 0;
        } else {
            $product->eligible_discount = 1;
        }

        if ($request->input('generateDiscount')) {
            $product->generate_discount = 0;
        } else {
            $product->generate_discount = 1;
        }

        if ($request->input('canRemark')) {
            $product->can_remark = 1;
        } else {
            $product->can_remark = 0;
        }

        if ($request->input('canPreorder')) {
            $product->can_preorder = 1;
        } else {
            $product->can_preorder = 0;
        }

        if ($request->input('canSelfcollect')) {
            $product->can_selfcollect = 1;
        } else {
            $product->can_selfcollect = 0;
        }

        $product->tradein_rule = $request->input('tradein_rule');

        if ($request->input('no_stockProduct') || $product->product_type == 'vqs') {
            $product->no_stockProduct = 1;
        } else {
            $product->no_stockProduct = 0;
        }

        $product->save();


        if ($request->file('productImage')) {
            // Get existing default image.
            $defaultImages = $product->images->where('default', 1);

            // Delete if default image exists.
            if ($defaultImages->count() != 0) {
                foreach ($defaultImages as $defaultImage) {
                    $defaultImage->delete();
                }
            }


            // Store new default image.
            $image = $request->file('productImage');
            $imageDestination = public_path('/storage/uploads/images/products/' . $request->input('productCode') . '/');
            $imageName = $image->getClientOriginalName() . '-' . 'default' . '.' . $image->getClientOriginalExtension();
            $image->move($imageDestination, $imageName);

            // Create record in DB.
            $product->images()->create([
                'path' => 'uploads/images/products/' . $request->input('productCode') . '/',
                'filename' => $imageName,
                'default' => 1
            ]);
        }

        // if ($request->file('brandLogo')) {
        //     // Get existing default image.
        //     $brandImages = $product->images->where('brand', 1);

        //     // Delete if default image exists.
        //     if ($brandImages->count() != 0) {
        //         foreach ($brandImages as $brandImage) {
        //             $brandImage->delete();
        //         }
        //     }

        //     // Store new default image.
        //     $image = $request->file('brandLogo');
        //     $imageDestination = public_path('/storage/uploads/images/products/' . $product->id . '/');
        //     $imageName = $product->id . '-' . 'brand' . '.' . $image->getClientOriginalExtension();
        //     $image->move($imageDestination, $imageName);

        //     // Create record in DB.
        //     $product->images()->create([
        //         'path' => 'uploads/images/products/' . $product->id . '/',
        //         'filename' => $imageName,
        //         'brand' => 1
        //     ]);
        // }

        $product->categories()->sync($categories);


        // -------------------------------------- End Globbal Product ---------------------------------------------------//





        // dd($request->input('fixed_showitem'));


        //panel Product
        // -------------------------------------- Panel Product ---------------------------------------------------//


        // dd('second', $request->input('panel_id'));

        // $offerPrice = $request->input('offer_price');
        // return $request;
        $panelProduct = PanelProduct::where('global_product_id', $id)->first();
        $parentProduct = $panelProduct->parentProduct;

        $panelProduct->panel_account_id = $request->input('panel_id');
        $panelProduct->display_panel_name = $request->input('display_panel_name');
        // $panelProduct->multichoices = $request->input('multichoices') ? $request->input('multichoices') : '0';
        // $panelProduct->price = str_replace('.', '', $request->input('price'));
        // $panelProduct->member_price = str_replace('.', '', $request->input('member_price'));
        $panelProduct->has_offer = $request->input('has_offer');
        // $panelProduct->offer_price = $request->input('offer_price') ? str_replace('.', '', $request->input('offer_price')) : '0';
        $panelProduct->origin_state_id = $request->input('ship_from');
        $panelProduct->short_description = $request->input('short_description');
        $panelProduct->product_description = $request->input('product_description');
        $panelProduct->product_content_1 = $request->input('product_content_1');
        $panelProduct->product_content_2 = $request->input('product_content_2');
        $panelProduct->product_content_3 = $request->input('product_content_3');
        $panelProduct->product_content_4 = $request->input('product_content_4');
        $panelProduct->product_content_5 = $request->input('product_content_5');

        // $panelProduct->product_sv = $request->input('product_sv') ?? '0';
        $panelProduct->estimate_ship_out_date = $request->input('estimate_ship_out_date');
        $panelProduct->place_of_origin = $request->input('origin_countrycategories');

        // $panelProduct->installation_category = $request->input('installationCategories') ? json_encode($request->input('installationCategories')) : 0;
        // $panelProduct->fixed_price = str_replace('.', '', $request->input('fixed_price')) != "" ? str_replace('.', '', $request->input('fixed_price')) : 0;
        $panelProduct->fixed_showitem = ($request->input('fixed_showitem') != 1) ? 0 : 1;
        if($request->input('markup_category')) {
            $panelProduct->markup_category = $request->input('markup_category')[0];
        }


        // -------------------------------- Shipping and Installation ---------------------------------------------------//
        // $panelProduct->shipping_category = 0;
        // $panelProduct->installation_category = 0;

        // $installationCategories = array();

        // if ($request->input('installationCategories')) {
        //     foreach ($request->input('installationCategories') as $installationCategory) {
        //         $installationCategories[] = $installationCategory;
        //     }

        //     $panelProduct->installation_category = $installationCategories;
        // }
        // if ($request->input('shippingcategories')) {
        //     $panelProduct->shipping_category = $request->input('shippingcategories');
        // }

        // if ($request->input('shipping_category')) {
        //     $panelProduct->shipping_category = $request->input('shipping_category');
        // }


        // $panelProduct->fixed_price = str_replace('.', '', $request->input('fixed_price')) != "" ? str_replace('.', '', $request->input('fixed_price')) : 0;

        // if (str_replace('.', '', $request->input('fixed_price')) != "") {

        //     $panelProduct->fixed_price = str_replace('.', '', $request->input('fixed_price'));
        // } else {
        //     $panelProduct->fixed_price = 0;
        // }

        $panelProduct->save();

        // return $request->input('product_variation');
        $attributeID = $panelProduct->id;

        $updatedAttributes = $request->input('product_variation_id');

        // dd($attributeID, $updatedAttributes);

        $updatedAttributePrice = $request->input('product_variation_price');


        // dd($attributeID, $updatedAttributes);
        if ($updatedAttributes != null) {
            $attributesCompares = ProductAttribute::where('panel_product_id', $attributeID)->whereNotIn('id', $updatedAttributes)->get();

            foreach ($attributesCompares as $attributesCompare) {
                $attributesCompare->active = 0;

                $attributesCompare->save();

                // $attributesPriceCompares = ProductPrices::where('model_id', $attributesCompare->id)->get();
                // foreach ($attributesPriceCompares as $key => $attributesPriceCompare) {
                //     $attributesPriceCompare->active = 0;
                //     $attributesPriceCompare->save();
                // }
            }
        }

        foreach ($request->input('product_variation') as $key => $attributeType) {

            $findProductAttribute = ProductAttribute::firstOrNew(
                [
                    'id' => $request->input('product_variation_id')[$key],
                ]
            );

            $pointCash = array(
                'point' => $request->input('point_cash_point')[$key],
                'cash' => $request->input('point_cash_cash')[$key],
            );
            //  $findProductAttribute->attribute_name = $request->input('product_variation_name')[$key] ;
            $findProductAttribute->panel_product_id = $panelProduct->id;
            $findProductAttribute->attribute_type = $attributeType;
            $findProductAttribute->attribute_name = $request->input('product_variation_name')[$key];
            // $findProductAttribute->stock = $request->input('stock')[$key];
            // $findProductAttribute->onhand_stock = $request->input('onhand_stock')[$key];
            // $findProductAttribute->sales_count = $request->input('sales_count')[$key];
            $findProductAttribute->shipping_category = $request->input('shipping_category')[$key];
            $findProductAttribute->shipping_category_special = $request->input('shipping_category_special')[$key];
            // $findProductAttribute->repeatable = $request->input('repeatable')[$key];
            // dd($request->input('frequencyoptions'));
            // $findProductAttribute->rbs_frequency = $request->input('frequencyoptions');
            // $findProductAttribute->allow_rbs = $request->input('allowRBS');
            // $findProductAttribute->allow_reminder = $request->input('allowReminder');

            if ($request->input('rbs')) {
                if (array_key_exists('allowRBS', $request->input('rbs'))) {
                    foreach ($request->input('rbs')['allowRBS'] as $allowRBSkey => $allowRBS) {
                        $rbsAttributes = ProductAttribute::where('id', $allowRBSkey)->first();
                        if ($rbsAttributes) {
                            $rbsAttributes->allow_rbs = $allowRBS;
                            $rbsAttributes->save();
                        }
                    }
                }


                if (array_key_exists('allowReminder', $request->input('rbs'))) {
                    foreach ($request->input('rbs')['allowReminder'] as $allowReminderkey => $allowReminder) {
                        $rbsAttributes = ProductAttribute::where('id', $allowReminderkey)->first();
                        if ($rbsAttributes) {
                            $rbsAttributes->allow_reminder = $allowReminder;
                            $rbsAttributes->save();
                        }
                    }
                }

                if (array_key_exists('frequencyoptions', $request->input('rbs'))) {
                    foreach ($request->input('rbs')['frequencyoptions'] as $frequencyoptionskey => $frequencyoptions) {
                        $rbsAttributes = ProductAttribute::where('id', $frequencyoptionskey)->first();
                        if ($rbsAttributes) {
                            $rbsAttributes->rbs_frequency = $frequencyoptions;
                            $rbsAttributes->save();
                        }
                    }
                }

                // dd($request->input('rbs'));


                $rbs_times_send = array();

                foreach ($request->input('rbs')['rbsDetails'] as $rbsDetailskey => $rbsDetails) {

                    foreach ($rbsDetails as $rbsDetailkey => $rbsDetail) {
                        if ($rbsDetail['price'] != null) {
                            $rbs_times_send[$rbsDetailkey]['price'] = $rbsDetail['price'] * 100;
                            $rbs_times_send[$rbsDetailkey]['qty'] = $rbsDetail['qty'];
                            $rbs_times_send[$rbsDetailkey]['shipping_category'] = $rbsDetail['shipping_category'];
                        }
                    }

                    $rbsAttributes = ProductAttribute::where('id', $rbsDetailskey)->first();
                    if ($rbsAttributes) {
                        $rbsAttributes->rbs_times_send = $rbs_times_send;
                        $rbsAttributes->save();
                    }
                }
            }


            // foreach ($request->input('rbs') as $timesoptions) {
            //     $rbs_times_send[$timesoptions] = $request->input('rbsDetails')[$timesoptions];
            //     $rbs_times_send[$timesoptions]['price'] == null ? $rbs_times_send[$timesoptions]['price'] = 0 : $rbs_times_send[$timesoptions]['price'];
            //     $rbs_times_send[$timesoptions]['qty'] == null ? $rbs_times_send[$timesoptions]['qty'] = 0 : $rbs_times_send[$timesoptions]['qty'];
            // }
            // $findProductAttribute->rbs_times_send = $rbs_times_send;

            // If there is no product code in variation, use global product code
            if (isset($request->input('product_variation_code')[$key])) {
                $findProductAttribute->product_code = $request->input('product_variation_code')[$key];
            } else {
                $findProductAttribute->product_code = $parentProduct->product_code;
            }

            // If there is no SV in variation, use main SV
            if (isset($request->input('product_sv')[$key])) {
                $findProductAttribute->product_sv = $request->input('product_sv')[$key] ?? '0';
            } else {
                $findProductAttribute->product_sv = $request->input('product_sv') ?? '0';
            }
            // If there is no color image in variation, use main color main image
            if (isset($request->input('productImage')[$key])) {
                $findProductAttribute->color_images = $request->input('productImage')[$key];
            } elseif (isset($request->input('productImage')[0])) {
                $findProductAttribute->color_images = $request->input('productImage')[0];
            } else {
                $findProductAttribute->color_images = null;
            }

            $findProductAttribute->product_measurement_length = $request->input('product_measurement_length')[$key];
            $findProductAttribute->product_measurement_width = $request->input('product_measurement_width')[$key];
            $findProductAttribute->product_measurement_depth = $request->input('product_measurement_depth')[$key];
            $findProductAttribute->product_measurement_weight = $request->input('product_measurement_weight')[$key];

            $findProductAttribute->save();

            $attributePrice = ProductPrices::where('model_id', $findProductAttribute->id)->where('country_id', 'MY')->where('active', 1)->first();

            if ($findProductAttribute->getPrice->isEmpty()) {
                $PanelProductPricesSG = new ProductPrices;

                $PanelProductPricesSG->model_type = ProductAttribute::class;
                $PanelProductPricesSG->model_id = $findProductAttribute->id;
                $PanelProductPricesSG->product_code = $findProductAttribute->product_code;
                $PanelProductPricesSG->country_id = 'SG';
                $PanelProductPricesSG->fixed_price = 0;
                $PanelProductPricesSG->price = 0;
                $PanelProductPricesSG->member_price = 0;
                $PanelProductPricesSG->offer_price = 0;

                $PanelProductPricesSG->point = 0;
                $PanelProductPricesSG->point_cash = NULL;
                $PanelProductPricesSG->discount_percentage = 0;
                $PanelProductPricesSG->product_sv = 0;

                $PanelProductPricesSG->save();

                $attributePrice = new ProductPrices;
                $attributePrice->model_type = ProductAttribute::class;
                $attributePrice->model_id = $findProductAttribute->id;
                $attributePrice->product_code = $findProductAttribute->product_code;
                $attributePrice->country_id = 'MY';
            }

            // $attributePrice->fixed_showitem = ($request->input('fixed_showitem')[$key] != 1) ? 0 : 1;
            // $attributePrice->fixed_price = str_replace('.', '', $request->input('fixed_price')[$key]);
            // $attributePrice->price = str_replace('.', '', $request->input('price')[$key]);
            // $attributePrice->member_price = str_replace('.', '', $request->input('member_price')[$key]);
            // $attributePrice->offer_price = str_replace('.', '', $request->input('offer_price')[$key]);            $attributePrice->fixed_price = str_replace('.', '', $request->input('fixed_price')[$key]) != "" ? str_replace('.', '', $request->input('fixed_price')[$key]) : 0;
            if($request->input('member_price')) {
                $attributePrice->price = str_replace('.', '', $request->input('price') ? $request->input('price')[$key] : 0);
                $attributePrice->member_price = str_replace('.', '',$request->input('member_price') ? $request->input('member_price')[$key] : 0);
                $attributePrice->offer_price = str_replace('.', '',$request->input('offer_price') ? $request->input('offer_price')[$key]: 0);
                $attributePrice->web_offer_price = str_replace('.', '', $request->input('web_offer_price')[$key]);
                $attributePrice->premier_price = str_replace('.', '', $request->input('premier_price')[$key]);
                $attributePrice->product_sv = $request->input('product_sv')[$key];
                $attributePrice->notes = $request->input('notes')[$key];
            }

            $attributePrice->point = $request->input('point')[$key];
            $attributePrice->point_cash = json_encode($pointCash);
            $attributePrice->discount_percentage = $request->input('discount_percentage')[$key];


            $attributePrice->save();

            unset($pointCash);

            /********************** Attribute Bundle **************************/
            // dd($request->input('product_bundle_yes_no'), $request->input('product_bundle_primary_product_id'), $request->input('product_bundle_primary_quantity'), $request->input('product_bundle_id'));
            if (isset($request->input('product_bundle_id')[$findProductAttribute->id]) && $request->input('product_bundle_yes_no')[$key] >= 1) {
                if ($request->input('product_bundle_id')[$findProductAttribute->id] != null) {
                    $attributesBundleCompares = ProductAttributeBundle::where('attribute_id', $findProductAttribute->id)->whereNotIn('id', $request->input('product_bundle_id')[$findProductAttribute->id])->get();
                    foreach ($attributesBundleCompares as $attributesBundleCompare) {
                        $attributesBundleCompare->active = 0;
                        $attributesBundleCompare->save();
                    }
                }

                foreach ($request->input('product_bundle_primary_product_id')[$findProductAttribute->id] as $k => $value) {

                    if (isset($request->input('product_bundle_primary_product_id')[$findProductAttribute->id][$k]) && $request->input('product_bundle_yes_no')[$key] >= 1) {
                        if (isset($request->input('product_bundle_primary_product_id')[$findProductAttribute->id][$k])) {
                            $attb = ProductAttribute::find($request->input('product_bundle_primary_product_id')[$findProductAttribute->id][$k]);
                        }

                        $findAttributeBundle = ProductAttributeBundle::firstOrNew(
                            [
                                'id' => $request->input('product_bundle_id')[$findProductAttribute->id][$k],
                            ]
                        );

                        $findAttributeBundle->attribute_id = $findProductAttribute->id;
                        $findAttributeBundle->product_code = $findProductAttribute->product_code;

                        $findAttributeBundle->primary_product_code = ($request->input('product_bundle_yes_no')[$key] >= 1) ? $attb->product_code : NULL;
                        $findAttributeBundle->primary_attribute_id = ($request->input('product_bundle_yes_no')[$key] >= 1) ? $attb->id : NULL;
                        $findAttributeBundle->primary_quantity =
                            ($request->input('product_bundle_yes_no')[$key] >= 1) ?
                            $request->input('product_bundle_primary_quantity')[$findProductAttribute->id][$k] : 0;
                        $findAttributeBundle->active = $request->input('product_bundle_yes_no')[$key];

                        $findAttributeBundle->save();
                    }
                }
            } elseif (isset($request->input('product_bundle_id')[$findProductAttribute->id]) && $request->input('product_bundle_id')[$findProductAttribute->id][0] != NULL) {
                $findAttributeBundlee = ProductAttributeBundle::whereIn('id', $request->input('product_bundle_id')[$findProductAttribute->id])->get();

                if ($findAttributeBundlee != NULL) {
                    foreach ($findAttributeBundlee as $ee) {
                        $ee->active = $request->input('product_bundle_yes_no')[$key];
                        $ee->save();
                    }
                }
            }
            // /********************** END Attribute Bundle **************************/

            //  /********************** Purchase Limit **************************/
            //  if(isset($request->input('product_limit_id')[$findProductAttribute->id])){
            //     $selection = $request->input('product_limit_yes_no')[$key];
            //     $findProductAttribute->purchase_limit_type = $selection;
            //     $findProductAttribute->save();
            //     switch ($selection) {
            //         case 'roles':
            //             if ($request->input('product_limit_id')[$findProductAttribute->id] != null) {
            //                 $attributeslimitCompares = ProductAttributeLimit::where('attribute_id', $findProductAttribute->id)->whereNotIn('id', $request->input('product_limit_id')[$findProductAttribute->id])->get();
            //                 foreach ($attributeslimitCompares as $attributeslimitCompare) {
            //                     $attributeslimitCompare->active = 0;
            //                     $attributeslimitCompare->save();
            //                 }
            //             }
        
            //             foreach ($request->input('product_limit_id')[$findProductAttribute->id] as $k => $value) {
            //                 $attb = ProductAttribute::find($findProductAttribute->id);
            //                 $findAttributeLimit = ProductAttributeLimit::firstOrNew(
            //                     [
            //                         'id' => $request->input('product_limit_id')[$findProductAttribute->id][$k],
            //                     ]
            //                 );
    
            //                 $findAttributeLimit->attribute_id = $findProductAttribute->id;
            //                 $findAttributeLimit->product_code = $findProductAttribute->product_code;
    
            //                 $limitRole = '';
            //                 $limitRoles = $request->input('product_limit_roles')[$findProductAttribute->id];
            //                 if ($limitRoles) {
            //                     $convertedRoles = array_map('strval', $limitRoles);
            //                     $limitRole = json_encode($convertedRoles);
            //                 }
    
            //                 $findAttributeLimit->duration = ($request->input('product_limit_duration')[$findProductAttribute->id][$k]) ?? '';
            //                 $findAttributeLimit->roles = $limitRole;
            //                 $findAttributeLimit->amount = ($request->input('purchase_limit_amount')[$findProductAttribute->id][$k]) ?? '';
            //                 $findAttributeLimit->active = '1';
            //                 $findAttributeLimit->save();
            //             }
            //             break;
            //         case 'agent':
            //             // $role = Role::where('name', 'dealer')->first();
            //             if ($request->input('agent_limit_id')[$findProductAttribute->id] != null) {
            //                 $attributeslimitCompares = ProductAttributeLimit::where('attribute_id', $findProductAttribute->id)->whereNotIn('id', $request->input('agent_limit_id')[$findProductAttribute->id])->get();
            //                 foreach ($attributeslimitCompares as $attributeslimitCompare) {
            //                     $attributeslimitCompare->active = 0;
            //                     $attributeslimitCompare->save();
            //                 }
            //             }
            //             foreach ($request->input('agent_limit_id')[$findProductAttribute->id] as $k => $value) { 
            //                 $attb = ProductAttribute::find($findProductAttribute->id);
            //                 $findAttributeLimit = ProductAttributeLimit::firstOrNew(
            //                     [
            //                         'id' => $request->input('agent_limit_id')[$findProductAttribute->id][$k],
            //                     ]
            //                 );
            //                 $findAttributeLimit->attribute_id = $findProductAttribute->id;
            //                 $findAttributeLimit->product_code = $findProductAttribute->product_code;
    
            //                 $limitRole = '';

            //                 $limitRoles = ["dealer"];
            //                 if ($limitRoles) {
            //                     $convertedRoles = array_map('strval', $limitRoles);
            //                     $limitRole = json_encode($convertedRoles);
            //                 }
    
            //                 $findAttributeLimit->duration = ($request->input('agent_limit_duration')[$findProductAttribute->id][$k]) ?? '';
            //                 $findAttributeLimit->roles = $limitRole;
            //                 $findAttributeLimit->amount = ($request->input('agent_limit_qty')[$findProductAttribute->id][$k]) ?? '';
            //                 $findAttributeLimit->user_id = $value;
            //                 $findAttributeLimit->active = '1';
            //                 $findAttributeLimit->save();
            //             }
            //             break;
            //         default:
            //             if($request->input('product_limit_id')[$findProductAttribute->id][0] != NULL){
            //                 $findAttributeLimit = ProductAttributeLimit::where('id', $request->input('product_limit_id')[$findProductAttribute->id])->first();
            //                 $findAttributeLimit->active = '0';
            //                 $findAttributeLimit->save();
            //             }
            //             break;
            //     }
            // }

            // if (isset($request->input('product_limit_id')[$findProductAttribute->id]) && $request->input('product_limit_yes_no')[$key] == '1') {
            //     dd('hi', $request->input('product_limit_id')[$findProductAttribute->id] );
 
            // } elseif (isset($request->input('product_limit_id')[$findProductAttribute->id]) && $request->input('product_limit_id')[$findProductAttribute->id][0] != NULL) {
            //     dd('hi2');
            //     $findAttributeLimit = ProductAttributeLimit::where('id', $request->input('product_limit_id')[$findProductAttribute->id])->first();
            //     $findAttributeLimit->active = '0';
            //     $findAttributeLimit->save();
            // }
             /********************** End Purchase Limit **************************/
        }

        $bundleID = $panelProduct->id;
        $updatedBundle = $request->input('product_bundle_id');

        if ($updatedBundle != null) {
            $attributesCompares = ProductBundle::where('panel_product_id', $bundleID)->whereNotIn('id', $updatedBundle)->get();
            foreach ($attributesCompares as $attributesCompare) {
                $attributesCompare->delete();
            }
        }

        if ($request->input('bundle_type')) {
            $data = array();

            foreach ($request->input('bundle_type') as $key => $bundleType) {
                $findProductBundle = ProductBundle::firstOrNew(
                    [
                        'id' => $request->input('product_bundle_id')[$key]
                    ]
                );
                $findProductBundle->panel_product_id = $panelProduct->id;
                $findProductBundle->bundle_type = $bundleType;
                $findProductBundle->bundle_title = $request->input('bundle_title')[$key];
                $findProductBundle->bundle_id = $request->input('bundle_id')[$key];
                $findProductBundle->bundle_qty = $request->input('bundle_qty')[$key];
                $findProductBundle->product_attribute_id = $request->input('product_attribute_id')[$key];

                $data['selection'] = $request->input('bundle_variation')[$key];
                $data['name'] = $request->input('bundle_variation_selection')[$key];

                $encode_bundleVariation = json_encode($data);
                $findProductBundle->bundle_variation = $encode_bundleVariation;

                $findProductBundle->save();
            }
        }

        // if ($request->get('selectDeliveries')) {

        //     //    return  ($request->input('freeStates') != null) ? $request->input('freeStates') : 'no';

        //     $parentPanelID = $panelProduct->panel->account_id;
        //     $parentID = $panelProduct->parentProduct->categories->first()->id;

        //     $updatedCategory = $request->input('firstCategory');

        //     if ($updatedCategory != null) {
        //         $compares = PanelCategories::where('category_id', $parentID)->whereNotIn('id', $updatedCategory)->get();
        //         foreach ($compares as $compare) {
        //             $compare->delete();
        //         }
        //     }

        //     foreach ($request->get('selectDeliveries') as $key => $selectDelivery) {
        //         $saveDeliveryRange = PanelCategories::firstOrNew(
        //             [
        //                 'id' => $request->input('firstCategory')[$key]
        //             ]
        //         );

        //         $saveDeliveryRange->panel_id  = $parentPanelID;
        //         $saveDeliveryRange->category_id = $parentID;
        //         $saveDeliveryRange->delivery_text = $request->input('delivery_text');
        //         $saveDeliveryRange->free_state = $request->input('freeStates') ? json_encode($request->input('freeStates')) : null;
        //         // 'category_id' => $request->get('selectDeliveries')[$key]
        //         $saveDeliveryRange->min_price = str_replace('.', '', $request->input('min_delivery')[$key]);
        //         $saveDeliveryRange->max_price = str_replace('.', '', $request->input('max_delivery')[$key]);
        //         $saveDeliveryRange->delivery_fee = str_replace('.', '', $request->input('fixed_delivery')[$key]);
        //         $saveDeliveryRange->no_purchase = $request->get('no_purchase')[$key];
        //         $saveDeliveryRange->group_or_individual = $request->get('selectDeliveries')[$key];

        //         $saveDeliveryRange->save();
        //     }
        // }

        // $panelProductDeliveries = $panelProduct->deliveries;

        // foreach ($panelProductDeliveries as $panelProductDelivery) {
        //     $panelProductDelivery->delete();
        // }

        // foreach ($request->input('available_in') as $key => $availableIn) {
        //     $delivery = new ProductDelivery;
        //     $delivery->state_id = $availableIn;
        //     $delivery->panel_product_id = $panelProduct->id;
        //     $delivery->delivery_fee = ($request->input('available_in_price')[$key] != null) ? $request->input('available_in_price')[$key] * 100 : 0;
        //     $delivery->save();
        // }

        if ($request->input('markup_category') && $request->input('markup_category')[0] != 0) {
            $panelProduct->markupCategories()->sync($request->input('markup_category'));
        }

        $panelProduct->save();

        // -------------------------------------- End Panel Product ---------------------------------------------------//

        //validation here
        $request->validate([
            'productName' => ['required', 'max:255'],
            'panel_id' => ['required', 'max:255'],
            // 'display_panel_name' => ['required', 'max:255'],
            // 'ship_from' => ['required', 'max:255'],
            'shipping_category' => ['required', 'max:255'],

            'shopcategories' => ['required'],
            'origin_countrycategories' => ['required'],
        ]);


        // panel_account_id,display_panel_name,origin_state_id,shipping_category

        // dd($request->input('shipping_category'));x

        return redirect(route('administrator.product'))
            ->with('success', 'Product saved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function storeImage(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $image = $request->file('file');
        $imageDestination = public_path('/storage/uploads/images/products/' . $product->id . '/');
        $imageName = $image->getClientOriginalName();
        $image->move($imageDestination, $imageName);

        $product->images()->create([
            'path' => 'uploads/images/products/' . $product->id . '/',
            'filename' => $imageName,
            'default' => 0
        ]);

        return response()->json(['success' => $imageName]);
    }

    public function deleteImage(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $path = public_path('/storage/uploads/images/products/' . $product->id . '/');
        $filename = $request->input('filename');
        if (File::exists($path . $filename)) {
            File::delete($path . $filename);
            $message = 'true';
        } else {
            $message = 'false';
        }
        $product->images()->where('filename', $filename)->delete();
        return $filename;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit(Request $request, $id)
    // {
    //     $product = Product::findOrFail($id);
    //     $categories = Category::topLevelCategory()->sortBy('name');

    //     if ($request->ajax()) {
    //         $result  = array();
    //         $storeFolder = public_path('/storage/uploads/images/products/' . $product->id . '/');

    //         $files = scandir($storeFolder);

    //         if (false !== $files) {
    //             foreach ($files as $file) {
    //                 if ('.' != $file && '..' != $file) {       //2
    //                     $obj['name'] = $file;
    //                     $obj['size'] = filesize($storeFolder . '/' . $file);
    //                     $result[] = $obj;
    //                 }
    //             }
    //         }

    //         return response()->json($result);
    //     }

    //     return view('administrator.products.global.edit')
    //         ->with('product', $product)
    //         ->with('categories', $categories);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $categories =  $request->input('categories');
    //     $postAttributes = $request->input('attribute_id');

    //     $product = Product::findOrFail($id);
    //     $product->name = $request->input('product_name');
    //     $product->name_slug = Str::slug($request->input('product_name'), '-');
    //     $product->product_code = $request->input('product_code');
    //     $product->details = $request->input('product_details');
    //     $product->description = $request->input('product_description');
    //     $product->quality_id = $request->input('product_quality');
    //     $product->product_status = 2;
    //     $product->save();

    //     $product->categories()->sync($categories);

    //     $productAttributes = $product->attributes;

    //     foreach ($productAttributes as $productAttribute) {
    //         $productAttribute->delete();
    //     }

    //     foreach ($request->input('attribute_type') as $key => $attributeType) {
    //         $newAttribute = new ProductAttribute;
    //         $newAttribute->product_id = $product->id;
    //         $newAttribute->attribute_type = $attributeType;
    //         $newAttribute->attribute_name = $request->input('attribute_name')[$key];
    //         $newAttribute->color_hex = $request->input('color_hex')[$key];
    //         $newAttribute->save();
    //     }

    //     return redirect('/administrator/products');
    // }

    public function publishProduct(Request $request, $id)
    {
        $product = GlobalProduct::findOrFail($id);
        $product->product_status = 1;
        $product->save();

        return redirect()->back();
    }

    public function unpublishProduct(Request $request, $id)
    {
        $product = GlobalProduct::findOrFail($id);
        $product->product_status = 2;
        $product->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function enableAttributes(Request $request)
    {
        $attributes = ProductAttribute::where('id', $request->attributeID)->first();
        $attributes->active = 1;
        $attributes->save();

        // $attributesPriceCompares = ProductPrices::where('model_id', $attributes->id)->get();
        // foreach ($attributesPriceCompares as $key => $attributesPriceCompare) {
        //     $attributesPriceCompare->active = 1;
        //     $attributesPriceCompare->save();
        // }
        return $attributes;
    }

    public function activePrice(Request $request)
    {
        $priceTable = ProductPrices::where('id', $request->price_id);
        $model_id = $priceTable->first()->model_id;

        $inActiveThis = ProductPrices::where('model_id', $model_id)->get();
        foreach ($inActiveThis as $key => $inactive) {
            $inactive->active = 0;

            $inactive->save();
        }

        $activeThis = $priceTable->get();
        foreach ($activeThis as $key => $active) {
            $active->active = 1;
            $active->save();
        }

        return ['activated', $activeThis];
    }

    public function updatePreviousNotes(Request $request)
    {
        $priceTable = ProductPrices::where('id', $request->price_id)->first();

        $priceTable->notes = $request->previous_notes;
        $priceTable->save();

        return ['updated Notes', $priceTable];
    }

    public function attributeAddNewPrice(Request $request)
    {
        $findProductAttribute = ProductAttribute::where('id', $request->attributeID)->first();

        $maxPricekey = ProductPrices::where('model_type', ProductAttribute::class)->where('country_id', country()->country_id)->where('model_id', $request->attributeID)->max('price_key');

        $pointCash = array(
            'point' => $request->new_point_cash_point,
            'cash' => $request->new_point_cash_cash,
        );

        $attributePrice = new ProductPrices;
        $attributePrice->model_type = ProductAttribute::class;
        $attributePrice->model_id = $findProductAttribute->id;
        $attributePrice->product_code = $findProductAttribute->product_code;
        $attributePrice->price_key = $maxPricekey + 1;
        $attributePrice->country_id = 'MY';
        $attributePrice->fixed_price = str_replace('.', '', $request->new_fixed_price) != "" ? str_replace('.', '', $request->new_fixed_price) : 0;
        $attributePrice->price = str_replace('.', '', $request->new_price);
        $attributePrice->member_price = str_replace('.', '', $request->new_member_price);
        $attributePrice->standard_price = str_replace('.', '', $request->new_member_price);
        $attributePrice->advance_price = str_replace('.', '', $request->new_offer_price);
        $attributePrice->web_offer_price = str_replace('.', '', $request->new_web_offer_price);
        $attributePrice->outlet_offer_price = str_replace('.', '', $request->new_outlet_offer_price);
        $attributePrice->outlet_price = str_replace('.', '', $request->new_outlet_fixed_price);
        $attributePrice->premier_price = str_replace('.', '', $request->new_premier_price);

        $attributePrice->product_sv = $request->new_product_sv;
        $attributePrice->notes = $request->new_notes;

        $attributePrice->point = $request->new_point;
        $attributePrice->point_cash = json_encode($pointCash);
        $attributePrice->discount_percentage = $request->new_discount_percentage;

        $attributePrice->active = 0;

        $attributePrice->save();

        //active this price
        if (!empty($request->active)) {

            $inactiveThis  = ProductPrices::where('model_id', $request->attributeID)->get();

            foreach ($inactiveThis as $key => $inactive) {
                $inactive->active = 0;
                $inactive->save();
            }

            $activeThis = ProductPrices::where('id', $attributePrice->id)->get();

            foreach ($activeThis as $key => $active) {
                $active->active = 1;
                $active->save();
            }
        }

        return $attributePrice;
    }
}
