<?php

namespace App\Jobs\Emails;

use App\Mail\CustomerRegisterEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendCustomerRegisterEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected $emailAddress;
    public $membership;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emailAddress, $membership)
    {
        $this->emailAddress = $emailAddress;
        $this->membership = $membership;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emailInstance = new CustomerRegisterEmail($this->membership);
        Mail::to($this->emailAddress)->send($emailInstance);
    }
}
