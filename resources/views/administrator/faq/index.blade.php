@extends('layouts.administrator.main')

@section('content')
@php

@endphp 

    <h1>FAQ</h1>

    <div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="/administrator/faq/create" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New FAQ</a>
                </div>
            </div>
        </div>

        <div class="table-responsive m-2">
            <table id="global-products-table" class="table table-striped table-bordered" style="min-width: 1366px;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>FAQ Type</td>
                        <td>Question</td>
                        <td>Answer</td>
                        <td>Edit</td>
                    </tr>
                </thead>
                <tbody>
                    @if(count($faqs) > 0)
                    @foreach($faqs as $faq)
                    
                    <tr>
                        <td style="width: 1%;"> {{$faq->id}} </td>

                        {{-- <td style="width: 5%;"> {{$faq->faq_type}} </td> --}}

                        @if(count($statuses) > 0)
                        @foreach($statuses->where('id', $faq->faq_type) as $status)

                        <td style="width: 2%;"> {{$status->name}} </td>

                        @endforeach
                        @endif
                        
                        <td style="width: 20%;"> {{$faq->question}} </td>

                        <td style="width: 20%;"> {!! $faq->answer !!} 
                        <br>
                        <small>Created on : {{$faq->created_at}}</small>
                        </td>
                       
                        <td style="width: 10%;">
                            <a style="color: white; font-style: normal; border-radius: 5px; float: left;margin-left: 15px;min-width: 0;" 
                            href="/administrator/faq/edit/{{$faq->id}}" class="btn btn-primary shadow-sm">Edit</a>

                            <form action="{{ route('administrator.faq.destroy',['id' => $faq->id]) }}" id="delete-banner-form" method="POST" style="width: auto;float: left;margin-left: 15px; top-margin:10px;">
                            <!-- @method('DELETE') -->
                            @csrf

                            <!-- <input class="btn btn-danger" type="submit" value="Delete" /> -->

                            <button type="submit" value="Delete" class="btn btn-danger shadow-sm">Delete</button>

                            @if($faq->faq_status == 1)
                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;" href="/administrator/faq/faq-unpublish/{{ $faq->id }}" class="btn btn-danger shadow-sm">Disable</a>
                            @endif

                            @if($faq->faq_status == 0)
                            <a style="color: white; font-style: normal; border-radius: 5px; margin-left: 15px; top-margin:10px;" href="/administrator/faq/faq-publish/{{ $faq->id }}" class="btn btn-info shadow-sm">Enable</a>
                            @endif

                            </form>
                        </td>
                    </tr>
                    
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5" class="text-center">No faq found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection