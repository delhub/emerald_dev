<?php

namespace App\Http\Controllers\Administrator\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categories\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str; 

use App\Models\Products\ProductBrand;

class BrandController extends Controller
{

    public function index()
    {
        $brands = ProductBrand::all();

        return view('administrator.product.brand.index')
            ->with('brands', $brands);
    }
 
    // public function create()
    // {
    //     return view('administrator.product.brand.create');
    // }

    public function store(Request $request)
    {
        
// check why brand canot create
// leave clean code, and test

        // $validator = Validator::make($request->all(), [
        //     'category_name' => 'required',
        //     // 'parent_category_id' => 'required',
        //     'category_icon' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        // ]);

        // if ($validator->fails()) {
        //     $data['status'] = 'Bad Request';
        //     $data['message'] = 'Validation failed. Please make sure all inputs are filled in.';
        //     $data['item'] = $validator->errors();

        //     return response()->json($data, 400);
        // }

        $category = new ProductBrand;
        $category->name = $request->input('category_name');
        $category->slug = Str::slug($request->input('category_name'), '-');
        // $category->type = 'type';
        // $category->parent_category_id = $request->input('parent_category_id');
        // if ($request->input('featured_category')) {
        //     $category->featured = 1;
        // } else {
        //     $category->featured = 0;
        // }

        $category->save();

        if ($category->save() && $request->file('category_icon')) {
            $image = $request->file('category_icon');
            $imageDestination = public_path('/storage/uploads/images/brand/' . $category->id . '/');
            $imageName = $category->slug;
            $imageName = $imageName . '.' . $image->getClientOriginalExtension();
            $image->move($imageDestination, $imageName);

            $category->image()->create([
                'path' => 'uploads/images/brand/' . $category->id . '/',
                'filename' => $imageName,
                'default' => 0
            ]);
        }

        $data['status'] = 'OK';
        $data['message'] = 'Category successfully createdaaa.';
        $data['item'] = $category;

        return response()->json($data, 200);
    }

    /**
     * Assume that the request is made using Ajax.
     * Return a view that will be loaded into a modal using JQuery.
     */
    public function edit($id)
    {
        try {
            $category = ProductBrand::findOrFail($id);
            $categories = ProductBrand::where('id', '!=', $id)
                ->get();

            return view('administrator.product.brand.partials.edit-category')
                ->with('category', $category)
                ->with('categories', $categories);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $category = ProductBrand::findOrFail($id);
            $category->name = $request->input('category_name');
            $category->slug = Str::slug($request->input('category_name'), '-');
            // $category->parent_category_id = $request->input('parent_category_id');
            // if ($request->input('featured_category')) {
            //     $category->featured = 1;
            // } else {
            //     $category->featured = 0;
            // }
            $category->save();

            if ($category->save() && $request->file('category_icon')) {
                $image = $request->file('category_icon');
                $imageDestination = public_path('/storage/uploads/images/brand/' . $category->id . '/');
                $imageName = $category->slug;
                $imageName = $imageName . '.' . $image->getClientOriginalExtension();
                $image->move($imageDestination, $imageName);

                $categoryImage = $category->image;

                if ($categoryImage) {
                    $categoryImage->path = 'uploads/images/brand/' . $category->id . '/';
                    $categoryImage->filename = $imageName;
                    $categoryImage->save();
                } else {
                    $category->image()->create([
                        'path' => '/storage/uploads/images/brand/' . $category->id . '/',
                        'filename' => $imageName,
                        'default' => 0
                    ]);
                }
            }

            $data['status'] = 'OK';
            $data['message'] = 'Category successfully updated.';
            $data['item'] = $category;

            return response()->json($data);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    // 
    public function destroy(Request $request, $id)
    {
        try {
            $category = ProductBrand::findOrFail($id);

            // $image = $category->image;
            // $image->delete();
            $category->active = 0;
            $category->save();

            $data['status'] = 'OK';
            $data['message'] = 'Category deleted successfully.';
            $data['item'] = $category;

            return response()->json($data, 200);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }
}
