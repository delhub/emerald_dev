<?php

namespace App\Http\Controllers\Administrator\Ordertracking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categories\Category as CategoriesCategory;
use App\Models\Purchases\Item;
use App\Models\Globals\Countries;
use App\Models\Products\ProductAttribute;
use App\Models\Warehouse\Location\Wh_location;
use App\Models\Warehouse\ViewInventoriesStockBundle;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;



class LogisticReportController extends Controller
{

    public function index(Request $request)
    {
        $locations = wh_location::get();

        $viewProductStocks = ViewInventoriesStockBundle::selectRaw('view_inventories_stock_bundle.*')
            ->leftJoin('panel_product_attributes', 'panel_product_attributes.product_code', '=', 'view_inventories_stock_bundle.product_code')
            ->join('panel_products', 'panel_products.id', '=', 'panel_product_attributes.panel_product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            ->where('global_products.no_stockProduct', 0)
            ->where('global_products.quality_id', 3)
            ->orderBy('view_inventories_stock_bundle.product_code', 'ASC')
            ->productFilter($request->search_box)
            ->locationFilter($request->location)
            ->productQualityFilter($request->quality)
            ->datebetweenFilter($request->datefrom, $request->dateto)
            ->get();

        $lists = array();

        foreach ($viewProductStocks as $key => $result) {


            $placed = 1001;
            $acd_delivered = 1010;

            $shipped = 1002;
            $acd_shipOut = 1013;
            $acd_self_delivered = 1011;

            $delivered = 1003;
            $acd_collected = 1009;

            $cancel = 1004;

            $pending_payment = 1005;


            // $product_info = $result->product_information;
            // if ($result->bundle) {
            //     $product_bundle =  ProductAttributeBundle::where('product_code', $result->product_code)->whereIn('active', [1, 2])->first();
            //     $productnumber = $product_bundle->primary_product_code;
            //     $quantity = $product_bundle->primary_quantity;
            // } else {
            $productnumber = $result->product_code;
            $quantity = 1;
            // }

            $theItem = Item::where('delivery_order', $result->whDO)->where('product_code', $productnumber)->first();

            if (!isset($lists[$productnumber])) {
                $global_products = ProductAttribute::where('product_code', $productnumber)->first();

                $lists[$productnumber]['product_name'] = $global_products ? $global_products->attribute_name : '-';
                $lists[$productnumber]['global_product_name'] = $global_products ? $global_products->product2->parentProduct->name : '-';
                $lists[$productnumber]['quality'] = $global_products ? $global_products->product2->parentProduct->quality_id : '-';

                $lists[$productnumber]['overallPhysical'] = 0;
                $lists[$productnumber]['virtual_stock'] = 0;
                $lists[$productnumber]['total_sold'] = 0;
                $lists[$productnumber]['pos_completed'] = 0;
                $lists[$productnumber]['online_sold'] = 0;

                $lists[$productnumber]['total_stockIn'] = 0;


                $lists[$productnumber]['order_shipped']['qty'] = 0;
                $lists[$productnumber]['order_shipped']['itemsid'] = array();

                $lists[$productnumber]['order_delivered']['qty'] = 0;
                $lists[$productnumber]['order_delivered']['itemsid'] = array();

                $lists[$productnumber]['order_cancel']['qty'] = 0;
                $lists[$productnumber]['order_cancel']['itemsid'] = array();

                $lists[$productnumber]['order_pending_payment']['qty'] = 0;
                $lists[$productnumber]['order_pending_payment']['itemsid'] = array();

                $lists[$productnumber]['order_pending_payment_30days']['qty'] = 0;
                $lists[$productnumber]['order_pending_payment_30days']['itemsid'] = array();

                $lists[$productnumber]['order_pending_payment_60days']['qty'] = 0;
                $lists[$productnumber]['order_pending_payment_60days']['itemsid'] = array();

                $lists[$productnumber]['order_pending_payment_90days']['qty'] = 0;
                $lists[$productnumber]['order_pending_payment_90days']['itemsid'] = array();

                $lists[$productnumber]['order_pending_payment_120days']['qty'] = 0;
                $lists[$productnumber]['order_pending_payment_120days']['itemsid'] = array();

                $lists[$productnumber]['total']['qty'] = 0;
                $lists[$productnumber]['total']['itemsid'] = array();
            }


            if (isset($theItem)) {
                $item_status = $theItem->order->order_status;
                $items_id = $theItem->id;


                if ($item_status ==  $shipped || $item_status == $acd_shipOut) {

                    // Order Shipped
                    $lists[$productnumber]['order_shipped']['qty'] += $result->online_sold * $quantity;
                    $lists[$productnumber]['order_shipped']['itemsid'][] = $items_id;
                } elseif ($item_status ==  $delivered || $item_status == $acd_collected) {

                    // Order Delivered
                    $lists[$productnumber]['order_delivered']['qty'] += $result->online_sold * $quantity;
                    $lists[$productnumber]['order_delivered']['itemsid'][] = $items_id;
                } elseif ($item_status ==  $cancel) {

                    // Order Cancel
                    $lists[$productnumber]['order_cancel']['qty'] += $result->online_sold * $quantity;
                    $lists[$productnumber]['order_cancel']['itemsid'][] = $items_id;
                } elseif ($item_status ==  $placed || $item_status ==  $acd_delivered || $item_status ==  $pending_payment || $item_status == $acd_delivered) {

                    // Order Placed
                    $lists[$productnumber]['order_pending_payment']['qty'] += $result->virtual_stock_reserved * $quantity;
                    $lists[$productnumber]['order_pending_payment']['itemsid'][] = $items_id;

                    //Pending days
                    $pendingDays = $theItem->getPendingAttribute($theItem->order->purchase->created_at);

                    if ($pendingDays < 30) {

                        $lists[$productnumber]['order_pending_payment_30days']['qty'] += $result->virtual_stock_reserved * $quantity;
                        $lists[$productnumber]['order_pending_payment_30days']['itemsid'][] = $items_id;
                    } elseif ($pendingDays > 30 && $pendingDays < 60) {

                        $lists[$productnumber]['order_pending_payment_60days']['qty'] += $result->virtual_stock_reserved * $quantity;
                        $lists[$productnumber]['order_pending_payment_60days']['itemsid'][] = $items_id;
                    } elseif ($pendingDays > 60 && $pendingDays < 90) {

                        $lists[$productnumber]['order_pending_payment_90days']['qty'] += $result->virtual_stock_reserved * $quantity;
                        $lists[$productnumber]['order_pending_payment_90days']['itemsid'][] = $items_id;
                    } elseif ($pendingDays > 90) {

                        $lists[$productnumber]['order_pending_payment_120days']['qty'] += $result->virtual_stock_reserved * $quantity;
                        $lists[$productnumber]['order_pending_payment_120days']['itemsid'][] = $items_id;
                    }
                }
                $lists[$productnumber]['total']['qty'] += ($result->online_sold * $quantity);
                $lists[$productnumber]['total']['itemsid'][] = $items_id;
            }

            $lists[$productnumber]['total_sold'] += $result->total_sold * $quantity;
            $lists[$productnumber]['pos_completed'] += $result->pos_completed * $quantity;
            $lists[$productnumber]['online_sold'] += $result->online_sold * $quantity;
        }

        $lists = $this->paginate($lists, 200);
        $lists->setPath('');
        $lists->appends(request()->query());

        return view('administrator.ordertracking.management-report', compact(['request', 'lists']))
            ->with('locations', $locations);
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function GG(Request $request)
    {
        $list = array();
        $countResult = 0;
        $countries = Countries::get();

        $mangementReportCategories = CategoriesCategory::where('type', 'management')->get('id');

        $items = Item::selectRaw('items.id, items.product_code, items.product_id, items.product_information, items.item_order_status, items.quantity,
                                    global_products.name as GPName,
                                    purchases.created_at AS purchases_createdAt')
            ->join('orders', 'orders.order_number', '=', 'items.order_number')
            // ->join('panel_infos', 'panel_infos.account_id', '=', 'orders.panel_id')
            ->join('purchases', 'purchases.id', '=', 'orders.purchase_id')

            ->join('panel_products', 'panel_products.id', '=', 'items.product_id')
            ->join('global_products', 'global_products.id', '=', 'panel_products.global_product_id')
            // ->join('piv_category_product', 'piv_category_product.product_id', '=', 'global_products.id')
            // ->join('categories', 'categories.id', '=', 'piv_category_product.category_id')
            // ->whereIn('categories.id', $mangementReportCategories)
            ->where('global_products.quality_id', 3)



            ->whereNotIn('items.item_order_status', ['1000']) // remove status record created
            // ->countryFilter($request->country)
            // ->datebetweenFilter($request->datefrom, $request->dateto)
            ->orderBy('global_products.name', 'ASC');

        // $ForCountcustomerOrders = $items->get();

        $items = $items->paginate(999999999);
        $items->appends(request()->query());

        foreach ($items as $key => $result) {
            $placed = 1001;
            $acd_delivered = 1010;

            $shipped = 1002;
            // $acd_shipOut = 1010;
            // $acd_self_delivered = 1011;

            $delivered = 1003;
            $acd_collected = 1009;

            $cancel = 1004;

            $pending_payment = 1005;

            $productnumber = $result->product_code;

            // $product_info = $result->product_information;
            $item_status = $result->item_order_status;

            // $placed = 1001;
            // $acd_delivered = 1008;

            // $shipped = 1002;
            // $acd_shipOut = 1010;
            // $acd_self_delivered = 1011;

            // $delivered = 1003;
            // $acd_collected = 1009;

            // $cancel = 1004;

            // $pending_payment = 1005;

            if (!isset($list[$productnumber])) {

                // $list[$productnumber]['order_placed']['case'] = 0;
                // $list[$productnumber]['order_placed']['itemsid'] = array();

                // $list[$productnumber]['order_shipped']['case'] = 0;
                $list[$productnumber]['order_shipped']['qty'] = 0;
                $list[$productnumber]['order_shipped']['itemsid'] = array();

                // $list[$productnumber]['order_delivered']['case'] = 0;
                $list[$productnumber]['order_delivered']['qty'] = 0;
                $list[$productnumber]['order_delivered']['itemsid'] = array();

                // $list[$productnumber]['order_cancel']['case'] = 0;
                $list[$productnumber]['order_cancel']['qty'] = 0;
                $list[$productnumber]['order_cancel']['itemsid'] = array();

                // $list[$productnumber]['order_pending_payment']['case'] = 0;
                $list[$productnumber]['order_pending_payment']['qty'] = 0;
                $list[$productnumber]['order_pending_payment']['itemsid'] = array();

                // $list[$productnumber]['order_pending_payment_30days']['case'] = 0;
                $list[$productnumber]['order_pending_payment_30days']['qty'] = 0;
                $list[$productnumber]['order_pending_payment_30days']['itemsid'] = array();

                // $list[$productnumber]['order_pending_payment_60days']['case'] = 0;
                $list[$productnumber]['order_pending_payment_60days']['qty'] = 0;
                $list[$productnumber]['order_pending_payment_60days']['itemsid'] = array();

                // $list[$productnumber]['order_pending_payment_90days']['case'] = 0;
                $list[$productnumber]['order_pending_payment_90days']['qty'] = 0;
                $list[$productnumber]['order_pending_payment_90days']['itemsid'] = array();

                // $list[$productnumber]['order_pending_payment_120days']['case'] = 0;
                $list[$productnumber]['order_pending_payment_120days']['qty'] = 0;
                $list[$productnumber]['order_pending_payment_120days']['itemsid'] = array();

                // $list[$productnumber]['total']['case'] = 0;
                $list[$productnumber]['total']['qty'] = 0;
                $list[$productnumber]['total']['itemsid'] = array();
            }

            if ($item_status ==  $shipped || $item_status == $acd_shipOut || $item_status ==  $acd_self_delivered) {

                // Order Shipped
                // $list[$productnumber]['order_shipped']['case']++;
                $list[$productnumber]['order_shipped']['qty'] += $result->quantity;
                $list[$productnumber]['order_shipped']['itemsid'][] = $result->id;
            } elseif ($item_status ==  $delivered || $item_status == $acd_collected) {

                // Order Delivered
                // $list[$productnumber]['order_delivered']['case']++;
                $list[$productnumber]['order_delivered']['qty'] += $result->quantity;
                $list[$productnumber]['order_delivered']['itemsid'][] = $result->id;
            } elseif ($item_status ==  $cancel) {

                // Order Cancel
                // $list[$productnumber]['order_cancel']['case']++;
                $list[$productnumber]['order_cancel']['qty'] += $result->quantity;
                $list[$productnumber]['order_cancel']['itemsid'][] = $result->id;
            } elseif ($item_status ==  $placed || $item_status ==  $acd_delivered || $item_status ==  $pending_payment) {

                // Order Placed
                // $list[$productnumber]['order_pending_payment']['case']++;
                $list[$productnumber]['order_pending_payment']['qty'] += $result->quantity;
                $list[$productnumber]['order_pending_payment']['itemsid'][] = $result->id;

                //Pending days
                $pendingDays = $result->getPendingAttribute($result->purchases_createdAt);

                if ($pendingDays < 30) {
                    // $list[$productnumber]['order_pending_payment_30days']['case']++;
                    $list[$productnumber]['order_pending_payment_30days']['qty'] += $result->quantity;
                    $list[$productnumber]['order_pending_payment_30days']['itemsid'][] = $result->id;
                } elseif ($pendingDays > 30 && $pendingDays < 60) {
                    // $list[$productnumber]['order_pending_payment_60days']['case']++;
                    $list[$productnumber]['order_pending_payment_60days']['qty'] += $result->quantity;
                    $list[$productnumber]['order_pending_payment_60days']['itemsid'][] = $result->id;
                } elseif ($pendingDays > 60 && $pendingDays < 90) {
                    // $list[$productnumber]['order_pending_payment_90days']['case']++;
                    $list[$productnumber]['order_pending_payment_90days']['qty'] += $result->quantity;
                    $list[$productnumber]['order_pending_payment_90days']['itemsid'][] = $result->id;
                } elseif ($pendingDays > 90) {
                    // $list[$productnumber]['order_pending_payment_120days']['case']++;
                    $list[$productnumber]['order_pending_payment_120days']['qty'] += $result->quantity;
                    $list[$productnumber]['order_pending_payment_120days']['itemsid'][] = $result->id;
                }
            }

            // $list[$productnumber]['total']['case']++;
            $list[$productnumber]['total']['qty'] += $result->quantity;
            $list[$productnumber]['total']['itemsid'][] = $result->id;


            $countResult += $result->quantity;;

            // if ($result->product_code != NULL) $list[$productnumber]['productCode'][$result->product_id] = $result->product_code;
            // $list[$productnumber]['productName'][$result->product_id] = $result->product->parentProduct->name;

            // $list[$productnumber]['panel_companyName'] = $result->company_name;

            // $list[$productnumber]['information'] = $product_info;
            // $list[$productnumber]['item'] = $result;
        }

        return view('administrator.ordertracking.management-report')
            // ->with('panels', $panels)
            ->with('request', $request)
            ->with('countries', $countries)
            ->with('items', $items)
            ->with('list', $list)
            ->with('countResult', $countResult);
    }
}
