<?php

namespace App\Imports;

use App\Models\Tax\Tax;
use Maatwebsite\Excel\Concerns\ToModel;

class TaxImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Tax([
            'country_id'     => $row[0],
            'tax_name'    => $row[1], 
            'tax_percentage' => $row[2],
            'tax_reg_number' => $row[3],
        ]);
    }
}
