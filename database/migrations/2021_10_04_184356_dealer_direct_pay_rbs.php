<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DealerDirectPayRbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            $table->string('purchase_product_type')->default('normal')->after('outlet_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_direct_pay', function (Blueprint $table) {
            $table->removeColumn('purchase_product_type');
        });
    }
}
