<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Products\ProductAttribute;
use App\Models\Products\ProductPrices;
use App\Models\Globals\Products\Product;
// use App\Models\Products\Product as panelProduct;
use App\Models\Categories\Category;
use App\Models\Categories\PivCategory;
use App\Models\Globals\Countries;

class ProductPriceImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows[0] as $c => $d) {
            $rows[0][$c] = str_slug($d,'_');
        }

        foreach ($rows as $k => $v) {
            if($k > 0) {
                foreach ($rows[0] as $k1 => $v1) {
                    $rb[$v1] = $v[$k1];
                }

                $data[] = $rb;
            }
        }

        if(count($data) > 0) {
            foreach ($data as $a => $b) {
                $this->InsertPanelProductPrice($b);
            }

            return 'data has been imported successfully...';
        }

    }

    public function InsertPanelProductPrice($data)
    {
        $country = Countries::whereIn('country_id', ['MY', 'SG'])->get('country_id');

        $model = ProductAttribute::where('product_code', $data['product_code'])->first('id');

        $model_type = 'App\Models\Products\ProductAttribute';
        if ((count($country) > 0) && (isset($model->id) && !empty($model->id))) {

            foreach ($country as $val) {
                $fixed_price = ($val['country_id'] == 'MY') ? $data['web_fixed_price'] * 100 : 0;
                $outlet_price = ($val['country_id'] == 'MY') ? $data['outlet_fixed_price'] * 100 : 0;
                $standard_price = ($val['country_id'] == 'MY') ? $data['standard_price'] * 100 : 0;
                $advance_price = ($val['country_id'] == 'MY') ? $data['advance_price'] * 100 : 0;
                $premier_price = ($val['country_id'] == 'MY') ? $data['premier_price'] * 100 : 0;
                $web_offer_price = ($val['country_id'] == 'MY') ? $data['web_offer_price'] * 100 : 0;
                $outlet_offer_price = ($val['country_id'] == 'MY') ? $data['outlet_offer_price'] * 100 : 0;

                $pPrice = ProductPrices::where([
                    ['model_id', $model->id], ['model_type', $model_type], ['country_id', $val['country_id']],
                    ['fixed_price', $fixed_price], ['outlet_price', $outlet_price], ['standard_price', $standard_price],
                    ['advance_price', $advance_price], ['premier_price', $premier_price], ['web_offer_price', $web_offer_price],
                    ['outlet_offer_price', $outlet_offer_price]
                ])->first();

                if (!$pPrice) {
                    $inactiveThis  = ProductPrices::where([['model_id', $model->id], ['country_id', $val['country_id']]])->get();

                    if (count($inactiveThis)) {
                        foreach ($inactiveThis as $key => $inactive) {
                            $inactive->active = 0;
                            $inactive->save();
                        }
                    }

                    $maxPricekey = ProductPrices::where('model_type', $model_type)->where('country_id', $val['country_id'])->where('model_id', $model->id)->max('price_key');

                    $pPrice = new ProductPrices;
                    $pPrice->price_key = $maxPricekey + 1;
                }

                $pPrice->model_type = $model_type;
                $pPrice->model_id = $model->id;
                $pPrice->product_code = $data['product_code'];
                $pPrice->country_id = $val['country_id'];
                $pPrice->product_sv = isset($data['product_sv']) ? $data['product_sv'] : 0;
                $pPrice->fixed_price = $fixed_price;
                $pPrice->outlet_price = $outlet_price;
                $pPrice->standard_price = $standard_price;
                $pPrice->advance_price = $advance_price;
                $pPrice->premier_price = $premier_price;
                $pPrice->web_offer_price = $web_offer_price;
                $pPrice->outlet_offer_price = $outlet_offer_price;
                $pPrice->is_offer = $data['is_offer'];
                $pPrice->is_free = $data['is_free'];

                $pPrice->save();
            }
        }
    }



}
