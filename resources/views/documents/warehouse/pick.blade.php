<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formula - Pick_Itemize({{ $batch->batch_number }})</title>
    <link href="https://fonts.googleapis.com/css2?family=Galada&family=Roboto:wght@300;400;500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/a4-receipt-invoice.css') }}">
    @include('layouts.favicon')
</head>

<body>
    {{-- @php
        $url = URL::signedRoute(
            'administrator.pickBin.qr-code',
            ['bin_number' => $batch->batch_number]
        );
    @endphp --}}

    <div class="a4">
        <div class="header-invoice">
            <table>
                <tr width="100%">
                    <td width="30%">
                        <img style="margin-left:20px;width:200px;" src="{{asset('images/formula/post-img/logo.png')}}">
                    </td>
                    <td class="company-name" width="50%">

                        <table width="100%">
                            <tr>
                                <td>
                                    <p style="font-weight: bold;font-size:18px;line-height:20px;">Formula Healthcare Sdn. Bhd.<span>(1318429-V)</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="font-size:16px;margin:0;line-height:16px;font-weight:normal;">1-26-05 Menara Bangkok Bank, Berjaya Central Park,</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="font-size:16px;margin:0;line-height:16px;font-weight:normal;">No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
                                </td>
                            </tr>
                            <tr class="company-details">
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <div class="contact-info">
                                                    <p style="font-size:16px;margin:0;line-height:16px;font-weight:normal;"><img class="ci-width" src="{{asset('images/formula/post-img/do-c-icon1.png')}}" style="margin-right:10px;">formula-cs@delhubdigital.com</p>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="contact-info">
                                                    <p style="font-size:16px;margin:0;line-height:16px;font-weight:normal;"><img class="ci-width" src="{{asset('images/formula/post-img/do-c-icon3.png')}}" style="margin-right:10px">formula2u.com</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                </td>

                                {{-- <div class="do-qrcode2">
                                    <img style="position: absolute;width: 120px;top: 0px;right:0;" src="data:images/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($url)) }} ">
                                </div> --}}

                            </tr>
                        </table>
                    </td>
                    <td width="20%"></td>
                </tr>
            </table>
        </div>

        <div class="invoice-body">
            <div class="invoice-dt">
                <h1>Pick Itemize({{$batch->batch_number}})</h1>
            </div>

            <div class="invoice-dt-box">
                <table width="100%">
                    <tr>
                        <td width="33%">
                            <div width="100%">
                                <table width="100%">
                                    <td style="font-weight: bold;" >

                                    </td>

                                    <tr>
                                        <td style="font-weight: bold;" width="33%">
                                            Batch Number
                                        </td>
                                        <td>
                                            : {{$batch->batch_number}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" width="33%">
                                            Bin Number
                                        </td>
                                        <td>
                                            : {{$batch->bin->bin_number}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" width="33%">
                                            Picker
                                        </td>
                                        <td>
                                            : {{ $batch->pick_user->userInfo->full_name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" width="33%">
                                            Created Date
                                        </td>
                                        <td>
                                            : {{ $batch->created_at }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="font-weight: bold;" width="33%">
                                            Delivery Orders Number ({{count($batch->batchDetail)}})
                                        </td>
                                        <td>
                                            :
                                            @foreach ($batch->batchDetail as $detail)
                                                {{$detail->delivery_order}}
                                                @if(!$loop->last), @endif


                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="font-weight: bold;" width="33%">
                                            Order Type :
                                        </td>
                                        <td>
                                            @if ($batch->batchDetail[0]->items[0]->order->order_status == 1010)
                                                {{$batch->batchDetail[0]->items[0]->order->delivery_method}}
                                            @else
                                                Delivery By Company
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>


            <div class="invoice-description">
                <table class="table-width">
                    <thead class="dark-clr2">
                        <tr>
                            <th width="10%" style="font-size:15px;line-height: 20px;font-weight:normal;">NO.</th>
                            <th width="50%" style="font-size:15px;line-height: 20px;font-weight:normal;">Descripton</th>
                            <th width="10%" style="font-size:15px;line-height: 20px;font-weight:normal;">QTY</th>
                        </tr>
                    </thead>

                    <tbody>

                        @php
                        $no = 0;
                        $sumItemQty = 0;

                        $detailInOrder = array();

                        foreach ($batch->batchDetail as $detail){
                            foreach ($detail->items as $key => $item) {
                                if ($item->item_order_status != 1011 && $item->attributes->product2->parentProduct->no_stockProduct == 0) {

                                    if (isset($item->attributes->productBundle) && ($item->attributes->productBundle->first() != NULL) && $item->attributes->productBundle->first()->active == 2) {
                                        foreach ($item->attributes->productBundle as $key => $bundle) {
                                            if (!isset($detailInOrder[$bundle->primary_product_code])) {

                                                $detailInOrder[$bundle->primary_product_code]['item_atrribute_name'] = array();
                                                $detailInOrder[$bundle->primary_product_code]['item_name'] = array();
                                                $detailInOrder[$bundle->primary_product_code]['qty'] = 0;
                                            }
                                            $detailInOrder[$bundle->primary_product_code]['item_atrribute_name'] = $bundle->productAttribute->attribute_name;
                                            $detailInOrder[$bundle->primary_product_code]['item_name'] = $bundle->productAttribute->product2->parentProduct->name;
                                            $detailInOrder[$bundle->primary_product_code]['qty'] += ($bundle->primary_quantity*$item->quantity);
                                        }

                                    }else{

                                        if (!isset($detailInOrder[$item->product_code])) {

                                            $detailInOrder[$item->product_code]['item'] = array();
                                            $detailInOrder[$item->product_code]['item_name'] = array();
                                            $detailInOrder[$item->product_code]['qty'] = 0;
                                        }

                                        $detailInOrder[$item->product_code]['item'] = $item;
                                        $detailInOrder[$item->product_code]['item_name'] = $item->product->parentProduct->name;
                                        $detailInOrder[$item->product_code]['qty'] += $item->quantity;
                                    }

                                }
                            }
                        }

                        foreach ($detailInOrder as $product_code => $detail) {

                            $no ++;

                            if(isset($detail['item']) && array_key_exists('product_size', $detail['item']->product_information)){
                                $size ='<br>(Size:'.$detail['item']->product_information['product_size'].')';
                            }else{
                                $size ='<br>(Size:'.$detail['item_atrribute_name'].')';
                            }

                            echo'
                                <tr>
                                    <td class="i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">'.$no.'</td>
                                    <td class="table-line2">
                                        <div class="table-p-bold2">
                                            <h4 style="font-size:18px;line-height: 20px;font-weight:normal;">'.$product_code.'</h4>
                                        </div>
                                        <p style="font-size: 15px;">'.$detail['item_name'].$size.'</p>
                                        ';
                                            // echo'
                                            //     <p style="font-size:14px;line-height: 18px;font-weight:normal;">'.$item->product_code.' </p>
                                            // ';

                                        // if (isset($detail['items']->attributes->productBundle) && ($detail['items']->attributes->productBundle->first() != NULL) && $detail['items']->attributes->productBundle->first()->active == 2) {
                                        //     foreach ($detail['items']->attributes->productBundle as $key => $bundle) {
                                        //         echo '
                                        //             <br>
                                        //             <div class="table-p-bold2">
                                        //                     <h4 style="font-size:18px;line-height: 20px;font-weight:normal;">
                                        //                         '.$bundle->primary_product_code.'x '.($bundle->primary_quantity*$detail['qty']).'</h4>
                                        //                 <p style="font-size: 15px;">'.$bundle->productAttribute->product2->parentProduct->name.'<br>('.$bundle->productAttribute->attribute_name.')</p>
                                        //             </div>
                                        //         ';
                                        //     }
                                        // }

                                        echo'
                                    </td>
                                    <td class="table-line2 i-fw" style="font-size:18px;line-height: 20px;font-weight:normal;">'.$detail['qty'].'</td>
                                </tr>
                            ';
                            unset($size);
                        }

                        @endphp

                    </tbody>

                </table>
            </div>
        </div>
        <div class="invoice-footer">
            <div class="footer-dp2">
                <p style="font-size:14px;line-height: 18px;font-weight:normal;">This is Pick Itemize.</p>
            </div>
        </div>
    </div>
</body>

</html>
