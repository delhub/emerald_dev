<?php

namespace App\Http\Controllers\Purchase;

use PDF;
use Auth;
use Carbon\Carbon;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\Purchases\Item;
use App\Models\Purchases\Order;
use App\Mail\Orders\CheckoutOrder;
use App\Models\Purchases\Purchase;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use App\Models\Users\Customers\Cart;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use App\Mail\Orders\InvoiceEmailCustomer;
use App\Models\Globals\State;
use App\Models\Purchases\Rating;
use App\Models\Users\Panels\PanelInfo;
use App\Models\Products\Product as PanelProduct;
use App\Models\Users\Dealers\DealerInfo;
use App\Models\Users\Dealers\DealerSales;
use App\Http\Controllers\BujishuAPI;
use App\Http\Controllers\Shop\CartController as ShopCartController;
use App\Models\Dealers\DealerStockLedger;
use App\Http\Controllers\WEB\Shop\v1\CartController;
use App\Models\Dealers\DealerDirectPay;
use App\Models\Products\ProductBundle;
use File;
use Cookie;
use Session;
use App\Models\Discount\Code;
use App\Models\Globals\Cards\Issuer;
use App\Models\Globals\City;
use App\Models\Tax\Tax;
use App\Models\Globals\Countries;
use Illuminate\Support\Facades\File as FacadesFile;
use App\Models\Pick\PickBatch;
use App\Models\Pick\PickBatchDetail;
use App\Models\Products\ProductAttribute;
use App\Traits\FreeGiftraits;
use App\Models\Users\UserConnectedPlatform;

class PurchaseController extends Controller
{
    use FreeGiftraits;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // Check if user is authenticated or not.
            if (Auth::check()) {
                // If authenticated, then get their cart.
                $this->cart = Auth::user()->carts->where('status', 2001);
            }
            // Get all categories, with subcategories and its images.
            $categories = Category::topLevelCategory();

            // Share the above variable with all views in this controller.
            View::share('categories', $categories);
            // TODO: Check why is it causing an error on the server.
            //View::share('cart', $this->cart);

            // Return the request.
            return $next($request);
        });
    }

    /** NOT USED! */
    public function buyNow(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $product = PanelProduct::find($request->input('product_id'));

        $po_numbers = array();

        $invoiceSequence = Purchase::all()->count() + 1;

        $purchase = new Purchase;

        $purchase->user_id = $user->id;

        $purchase->purchase_number =
            '0000000BSN' . Carbon::now()->format('Y') . str_pad($invoiceSequence, 6, "0", STR_PAD_LEFT);

        $purchase->purchase_status = 4000;

        $purchase->purchase_date = Carbon::now()->format('d/m/Y');

        $purchase->purchase_amount = $product->price;

        $purchase->save();

        $poSequence = Order::all()->count() + 1;

        $order = new Order;
        // Assign PO Number to the order.
        $order->order_number =
            'PO' . Carbon::now()->format('Y') . Carbon::now()->format('m') . '-' . str_pad($poSequence, 6, "0", STR_PAD_LEFT);
        // Assign purchase id to the order
        $order->purchase_id = $purchase->id;
        // Assign the panel id to the order record
        $order->panel_id = $product->panel_account_id;
        // Assign a status for the order. Placed, Shipped, Delivered.
        $order->order_status = 1000;
        // Assign empty value for order amount first.
        $order->order_amount = ($product->price * $request->input('productQuantity')) + $product->delivery_fee + $product->installation_fee;

        $order->save();

        // Variables initiliazation.
        $color = null;
        $size = null;
        $temperature = null;

        // If the post request has product color id value in it..
        if ($request->input('product_attribute_color') != null) {
            // Get selected product color.
            $color = $product->colorAttributes->where('id', $request->input('product_attribute_color'))->first();

            // Set color id for checking purposes.
            $colorId = $color->id;
        }

        // If the post request has product dimension id value in it..
        if ($request->input('product_attribute_size') != null) {
            // Get selected product dimension.
            $size = $product->sizeAttributes->where('id', $request->input('product_attribute_size'))->first();

            // Set dimension id for checking purposes.
            $sizeId = $size->id;
        }

        // If the post request has product length id value in it..
        if ($request->input('product_attribute_temperature') != null) {
            // Get selected product length.
            $temperature = $product->lightTemperatureAttributes
                ->where('id', $request->input('product_attribute_temperature'))->first();

            // Set length id for checking purposes.
            $temperatureId = $temperature->id;
        }

        // Check if the post request has product color id in it..
        if ($color != null) {
            // If yes, assign the color id and name
            $productInformation['product_color_id'] = $color->id;
            $productInformation['product_color_name'] = $color->attribute_name;
        }
        // Check if the post request has product dimension id in it..
        if ($size != null) {
            // If yes, assign the dimension id and concate the width, height, depth and measurement unit.
            $productInformation['product_size_id'] = $size->id;
            $productInformation['product_size'] = $size->attribute_name;
        }
        // Check if the post request has product length id in it..
        if ($temperature != null) {
            // If yes, assign the length id and concate the length and measurement unit.
            $productInformation['product_temperature_id'] = $temperature->id;
            $productInformation['product_temperature'] = $temperature->attribute_name;
        }

        // Create a new item record.
        $item = new Item;
        // Assign an order number to the item
        $item->order_number = $order->order_number;
        // Assign a product id to the item
        $item->product_id = $product->id;
        // Get the cart product's information. Color, dimension or length..
        // Store it in an array, easier to access later and avoid creating another column just for an attribute of a product
        $item->product_information = $productInformation;
        // Assign item quantity.
        $item->quantity = $request->input('productQuantity');
        // Assign subtotal price.
        $item->subtotal_price = $product->price * $request->input('productQuantity');
        // Assign delivery fee.
        $item->delivery_fee = $product->delivery_fee;
        // Assign installation fee.
        $item->installation_fee = $product->installation_fee;
        // Assign status of the item. Placed, shipped, delivered.
        $item->status_id = 1;
        $item->save();

        return redirect('/payment/cashier?orderId=' . $purchase->purchase_number);
    }

    /**
     * Handle what happens after user clicks checkout
     */
    public function checkoutItems(Request $request)
    {
        return redirect('/payment/cashier');
    }


    public function checkoutItemsOld(Request $request)
    {
        // Get user.
        $user = User::find(Auth::user()->id);
        // Get the items in the cart of user.
        $cartIds = $request->input('cartItemId');
        $cartItems = Cart::whereIn('id', $cartIds)->get();
        $purchase_amount = 0;
        $valid = 1;
        // Check if the product meets any minimum price set.
        $error =  $this->promotionRules($cartItems, $cartIds);

        if ($error) return redirect()->back()->with('error', $error);


        $invoiceSequence = Purchase::largestPurchaseNumber();
        $invoiceSequence++;

        // Create a new purchase record.
        $purchase = new Purchase;
        // Assign user to the purchase record.
        $purchase->user_id = $user->id;
        // Generate a unique number used to identify the purchase.
        $purchase->purchase_number = $invoiceSequence;

        // Assign a status to the purchase. Unpaid, paid.
        $purchase->purchase_status = 3000;
        // Assign the current date to the purchase in the form of DD/MM/YYYY.
        $purchase->purchase_date = Carbon::now()->format('d/m/Y');
        // Calculate total price of items in cart.

        foreach ($cartItems as $cartItem) {
            $purchase_amount =
                $purchase_amount +
                $cartItem->subtotal_price +
                $cartItem->delivery_fee
                // + $cartItem->installation_fee
            ;
        }


        $purchase->purchase_amount = $purchase_amount;
        // $purchase->dealer_id =  $user->dealerInfo->account_id;

        $receiptSequence = Purchase::largestReceiptNumber();
        $receiptSequence++;

        // $purchase->receipt_number = 'BOR20 ' . str_pad($invoiceSequence, 7, "0", STR_PAD_LEFT);
        $purchase->receipt_number = $receiptSequence;
        $purchase->ship_full_name = $user->userInfo->full_name;
        $purchase->ship_contact_num = $user->userInfo->mobileContact->contact_num;
        $purchase->ship_address_1 = $user->userInfo->shippingAddress->address_1;
        $purchase->ship_address_2 = $user->userInfo->shippingAddress->address_2;
        $purchase->ship_address_3 = $user->userInfo->shippingAddress->address_3;
        $purchase->ship_postcode = $user->userInfo->shippingAddress->postcode;
        $purchase->ship_city = $user->userInfo->shippingAddress->city;
        $purchase->ship_state_id = $user->userInfo->shippingAddress->state_id;
        $purchase->save();

        $price = 0;
        $poSequence = Order::all()->count() + 1;


        // Create order record.
        // Foreach item in the cart..
        foreach ($cartItems as $cartItem) {

            $panels[$cartItem->product->panel_account_id][] =  $cartItem;
        }
        $panels = array();
        // Initialize an empty variable to store panel's id.
        $panelId = null;
        // Foreach PO Number..
        foreach ($panels as $key => $panelProducts) {
            // Create a new order record.
            $order = new Order;
            // Create a new PO Number for each different panel belonging to an item.
            $po_number = 'PO' . Carbon::now()->format('Y') . Carbon::now()->format('m') . '-' . str_pad($poSequence, 6, "0", STR_PAD_LEFT);
            $poSequence = $poSequence + 1;

            // Assign PO Number to the order.
            $order->order_number = $po_number;
            // Assign purchase id to the order
            $order->purchase_id = $purchase->id;
            // Assign the panel id to the order record
            $order->panel_id = $key;
            // Assign a status for the order. Placed, Shipped, Delivered.
            $order->order_status = 1000;
            // Assign empty value for order amount first.
            $orderAmount = 0;
            $order->order_amount = 0;
            $order->delivery_date = "Pending";
            $order->received_date = "";
            $order->claim_status = "";

            $order->save();

            //Create new record for dealer sales tracking
            $dealer_sales = new DealerSales;

            //Assign PO number to order
            $dealer_sales->order_number = $po_number;

            //Assign purchase id to dealer sales
            $dealer_sales->purchase_id = $purchase->id;

            //Assign dealer id to the order record
            // FIXME: What if the customer is not a dealer?
            // Wan replaced with refferer_id instead of using dealer ID from dealer account.
            $dealer_sales->account_id = $user->userInfo->referrer_id;

            //Assign empty value for order amount first
            $dealer_sales->order_amount = $orderAmount;

            // Assign a status for the order. Placed, Shipped, Delivered.
            $dealer_sales->order_status = 1000;

            $panelId = $key;

            // Foreach item in the cart..
            foreach ($cartItems as $cartItem) {
                // If the cart item product's panel id matches with the current panel id..
                if ($cartItem->product->panel->account_id == $panelId) {
                    // Create a new item record.
                    $item = new Item;
                    // Assign an order number to the item
                    $item->order_number = $po_number;
                    // Assign a product id to the item
                    $item->product_id = $cartItem->product->id;
                    // Get the cart product's information. Color, dimension or length..
                    // Store it in an array, easier to access later and avoid creating another column just for an attribute of a product
                    $item->product_information = $cartItem->product_information;
                    // Assign item quantity.
                    $item->quantity = $cartItem->quantity;
                    // Assign unit price.
                    $item->unit_price = $cartItem->unit_price;
                    // Assign subtotal price.
                    $item->subtotal_price = $cartItem->subtotal_price;
                    // Assign delivery fee.
                    $item->delivery_fee = $cartItem->delivery_fee;
                    // Assign installation fee.
                    $item->installation_fee = $cartItem->installation_fee;
                    // Assign status of the item. Placed, shipped, delivered.
                    $item->status_id = 1;
                    // After checkout, cart items should be removed from cart page
                    // TODO: Change status after payment successful.
                    // $cartItem->status = 2001;
                    // $cartItem->save();

                    $item->save();

                    $orderAmount = $orderAmount +
                        $cartItem->subtotal_price +
                        $cartItem->delivery_fee
                        // + $cartItem->installation_fee
                    ;
                }
            }

            $order->order_amount = $orderAmount;
            $order->save();

            $dealer_sales->order_amount = $orderAmount;
            $dealer_sales->save();

            // //Send the email to panel after placing order (attach with PO)
            // Mail::to($order->panel->company_email)->send(new CheckoutOrder($order));

            // $pdf = PDF::loadView('documents.invoice', compact('purchase'))->setPaper('a4');

            // // Make a copy of the PDF invoice and store in public/storage/....
            // $content = $pdf->download()->getOriginalContent();
            // $pdfDestination = public_path('/storage/documents/invoice/' . $purchase->purchase_number . '/');
            // $pdfName = $purchase->purchase_number;
            // if (!File::isDirectory($pdfDestination)) {
            //     File::makeDirectory($pdfDestination, 0777, true);
            // }
            // File::put($pdfDestination . $pdfName . '.pdf', $content);

            // //Send email to customer after placing order( attach with invoice)
            // $message = new InvoiceEmailCustomer($purchase);
            // $message->attachData($pdf->output(), "invoice.pdf");
            // Mail::to($purchase->user->email)->send($message);
        }

        return redirect('/payment/cashier?orderId=' . $purchase->purchase_number);
    }

    //function for rules detection
    private function promotionRules($cartItems, $cartId)
    {

        foreach ($cartItems as $cartItem) {
            $product = $cartItem->product;

            $panel = $product->panel;

            $productCategories = $product->parentProduct->categories;

            $productCategoriesArray = [];

            foreach ($productCategories as $key => $productCategory) {
                $productCategoriesArray[$key] = $productCategory->id;
            }

            $freeDeliveryMinPrice = $product
                ->panel
                ->categoriesWithMinPrice
                ->whereIn('category_id', $productCategoriesArray)
                ->first();

            $totalSubtotalPrice = 0;

            $activeCartItems = Cart::whereIn('id', $cartId)
                ->whereHas('product.panel', function ($q) use ($panel) {
                    $q->where('account_id', $panel->account_id);
                })
                ->whereHas('product.parentProduct', function ($q) use ($productCategoriesArray) {
                    $q->whereHas('categories', function ($q) use ($productCategoriesArray) {
                        $q->whereIn('categories.id', $productCategoriesArray);
                    });
                })
                ->get();

            foreach ($activeCartItems as $activeCartItem) {
                $totalSubtotalPrice = $totalSubtotalPrice + $activeCartItem->subtotal_price;
            }

            $isDeliveryFree = 0;

            if ($freeDeliveryMinPrice) {
                if ($totalSubtotalPrice < $freeDeliveryMinPrice->free_delivery_min_price && $freeDeliveryMinPrice->delivery_fee_no_purchase == 1) {
                    $isDeliveryFree = 2;
                } elseif ($totalSubtotalPrice >= $freeDeliveryMinPrice->free_delivery_min_price) {
                    $isDeliveryFree = 1;
                } else {
                    $isDeliveryFree = 0;
                }
            }

            if ($isDeliveryFree == 2) {
                return 'Please add more item to your purchase.';
            }
        }
    }

    /**
     * Show payment options to customer.
     */
    public function paymentOption(Request $request, $directpayData = false)
    {
        $expiredLink = $isdirectPay = $disableFpx = FALSE;
        $paymentLink = $savedCards = null;

        // if direct pay
        // data from DealerRegisterController/v1
        if ($request->directID) {

            $directPayTables = DealerDirectPay::where('uuid_link', $request->directID)->first();
            $user = User::find($directPayTables->user_id);

            $cookie = $directpayData['cookie'];
            $shippingAddress = $directpayData['shippingAddress'];
            $isdirectPay = TRUE;

            $carts = Cart::whereIn('id', json_decode($directPayTables->cart_id))->whereIn('selected', [0, 1])->get();

            if (in_array(2003, $carts->pluck('status')->toArray()) || in_array(2002, $carts->pluck('status')->toArray())) {
                $expiredLink = true;
            } else {
                $expiredLink = false;
            }

            $paymentLink = "/direct-pay/payment/" . $request->directID;
            $isfreegift = 0;
            $freegift = array();
        } else {
            // Get user
            $user = User::find(Auth::user()->id);
            $customer = $user->userInfo;
            $shippingAddress = $customer->shippingAddress;

            $states = State::all();

            $carts = $user->carts->where('status', 2001)->where('selected', 1)->whereIn('disabled', [0, 8, 9]);

            $cookie = json_decode(Cookie::get('addressCookie'));

            $freegift = FreeGiftraits::freeGift_AutoApply($carts, 1);

            $isfreegift = collect($freegift)->where('status', true)->count();

            if (isset($_REQUEST['freegift']) && $_REQUEST['freegift'] == 1) {
                dd($freegift);
            }

            $savedCards = $user->paymentToken;
        }

        $mespay_option = [];

        if (count($carts)) {
            foreach ($carts as $k => $v) {
                $mespay_option[]['mespay'] = $v->product->parentProduct->mespay_purchaseable;
            }
        }

        $mespay_purchaseable = collect($mespay_option)->where('mespay', 0)->count() > 0 ? 1 : 0;

        $customer = $user->userInfo;
        $cartsItems = [];
        $cartsItems = ShopCartController::arrayProduct(compact('carts', 'shippingAddress', 'cartsItems', 'customer', 'isdirectPay'));

        // fpx bank request
        $fpxController = new FpxGatewayController;
        $B2Clists = $fpxController->getBankLists('01');
        $B2B1lists = $fpxController->getBankLists('02');

        if (!$B2Clists ||  !$B2B1lists) $disableFpx = true;

        $freegift = FreeGiftraits::freeGift_AutoApply($carts, 1);
        $isfreegift = collect($freegift)->where('status', true)->count();

        if (isset($_REQUEST['freegift']) && $_REQUEST['freegift'] == 1) {
            dd($freegift);
        }

        $cartsItems['issuer'] = Issuer::all();
        $cartsItems['bank']['b2c'] = $B2Clists;
        $cartsItems['bank']['b2b1'] = $B2B1lists;

        $availableBanksImages = FacadesFile::files(public_path('storage/banks'));
        foreach ($availableBanksImages as $availableBanksImage) {
            $availableBanks[] = $availableBanksImage->getRelativePathname();
        }

        $launchingBanksImages = FacadesFile::files(public_path('storage/launching-banks'));

        foreach ($launchingBanksImages as $launchingBanksImage) {
            $launchingBanks[] = $launchingBanksImage->getRelativePathname();
        }


        $agent = isset($user->dealerInfo) ? $user->dealerInfo : null;

        //metapoint
        $client = new \GuzzleHttp\Client();

        $userPlatform = UserConnectedPlatform::where('user_id', $user->id)->where('platform_key', 'META')->first();

        if ($userPlatform && $userPlatform->updated_at->lte(Carbon::now()->startOfMonth()->subMonth(11))) {
            $user_expired = true;
        } else {
            $user_expired = false;
        }

        if (isset($userPlatform) && !$isdirectPay && !$user_expired) {
            $canMetaPoint = true;
            $userSecretKey = $userPlatform->secret_key;

            $url = config('app.metapoint_baseurl') . '/api/point/balance/' . config('app.metapoint_platform') . '/';
            $requestMp = $client->get(
                $url,
                [
                    'http_errors' => false,
                    'verify' => false,
                    'headers' => [
                        'Accept'        => 'application/json',
                        'Authorization' => "Bearer " . $userSecretKey,
                    ]
                ]
            );
            $responseStatusMp = $requestMp->getStatusCode();
            if ($responseStatusMp == 200) {
                $responseMp = json_decode($requestMp->getBody(), true);

                $mpAmount['total'] =  $responseMp['balance'];
                $mpAmount['breakdown'] =  $responseMp['breakdown'];
                $mpAmount['meta_user_id'] =  $responseMp['meta_user_id'];
            } else {
                $responseMp = json_decode($requestMp->getBody(), true);
                $mpAmount['usable'] =  0;
                $mpAmount['total'] =  0;
            }
        } else {
            $mpAmount['usable'] =  0;
            $mpAmount['total'] =  0;
            $canMetaPoint = false;
        }

        return view('shop.payment.index')
            ->with('mespay_purchaseable', $mespay_purchaseable)
            ->with('cartsItems', $cartsItems)
            ->with('freegift', $freegift)
            ->with('isfreegift', $isfreegift)
            ->with('cookie', $cookie)
            ->with('user', $user)
            ->with('agent', $agent)
            ->with('savedCards', $savedCards)
            ->with('availableBanks', $availableBanks)
            ->with('launchingBanks', $launchingBanks)
            ->with('paymentLink', $paymentLink)
            ->with('expiredLink', $expiredLink)
            ->with('mpAmount', $mpAmount)
            ->with('canMetaPoint', $canMetaPoint)
            ->with('disableFpx', $disableFpx)
            ->with('user_expired', $user_expired)
            // ->with('bpAmount', $bpAmount)
            // ->with('bluepointPurchase', $bluepointPurchase)
            // ->with('states', $states)
            // ->with('canInstallment', $canInstallment)
            // ->with('installmentDetails', $installmentDetails)
            // ->with('rebateAllowed', $rebateAllowed)
            // ->with('country_code', $country_code);
        ;
    }

    /**
     * Show payment options to customer.
     */
    public function paymentOptionOld(Request $request)
    {
        $purchase = Purchase::where('purchase_number', $request->query('orderId'))
            ->firstOrFail();
        $user = User::find(Auth::user()->id);
        $states = State::all();

        // Update user's bluepoint.
        $client = new \GuzzleHttp\Client();

        $orders = $purchase->orders;
        $bluepointPurchase = true;

        foreach ($orders as $order) {
            foreach ($order->items as $item) {
                if ($item->product->parentProduct->bluepoint_purchaseable == 0) {
                    $bluepointPurchase = false;
                }
            }
        }


        $bpAmount['usable'] =  0;
        $bpAmount['total'] =  0;




        return view('shop.payment.index')
            ->with('purchase', $purchase)
            ->with('user', $user)
            ->with('bpAmount', $bpAmount)
            ->with('bluepointPurchase', $bluepointPurchase)
            ->with('states', $states);
    }

    public function updateCustomerPaymentDetail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $cookie = json_decode(Cookie::get('bjs_cart'));
        $purchase = $cookie->purchase;
        $cartIds = $cookie->cartIds;
        $bluepointPurchase = $cookie->bluepointPurchase;

        // Address Cookie
        $userShipping = $user->userInfo->shippingAddress;
        $states = State::all();

        $shipCookie = json_decode(Cookie::get('addressCookie'));
        // $purchase->ship_full_name = $request->input('attention_to');
        // $purchase->ship_contact_num = $request->input('attention_contact');
        // $purchase->ship_address_1 = $request->input('attention_address_1');
        // $purchase->ship_address_2 = $request->input('attention_address_2');
        // $purchase->ship_address_3 = $request->input('attention_address_3');
        // $purchase->ship_postcode = $request->input('attention_postcode');
        // $purchase->ship_city = $request->input('attention_city');
        // $purchase->ship_state_id = $request->input('state');
        // $purchase->state_name =  ($state = State::find($purchase->ship_state_id)) ? $state->name : 14;

        $purchase->ship_full_name = isset($shipCookie->name) ? $shipCookie->name : $user->userInfo->full_name;
        $purchase->ship_contact_num = isset($shipCookie->mobile) ? $shipCookie->mobile : $user->userInfo->mobileContact->contact_num;
        $purchase->ship_address_1 = isset($shipCookie->address_1) ? $shipCookie->address_1 : $userShipping->address_1;
        $purchase->ship_address_2 = isset($shipCookie->address_1) ? $shipCookie->address_2 : $userShipping->address_2;
        $purchase->ship_address_3 = isset($shipCookie->address_1) ? $shipCookie->address_3 : $userShipping->address_3;
        $purchase->ship_postcode = isset($shipCookie->address_1) ? $shipCookie->postcode : $userShipping->postcode;
        $purchase->ship_city = isset($shipCookie->address_1) ? $shipCookie->city : $userShipping->city;
        $purchase->ship_state_id = isset($shipCookie->address_1) ? $shipCookie->state_id : $userShipping->state_id;

        $cookie = json_encode(array('cartIds' => $cartIds, 'purchase' => $purchase, 'bluepointPurchase' => $bluepointPurchase));
        return redirect('/payment/cashier')->withCookie(Cookie::make('bjs_cart', $cookie, 3600));;
    }

    /**
     * Handle QR Code scans.
     */
    public function qrScanned(Request $request, $orderNum)
    {
        if (Auth::user() && User::find(Auth::user()->id)->hasRole('wh_sorter')) {
            //sort flow
            $pickBatchDetail = PickBatch::join('wh_picking_order', 'wh_picking_order.key', '=', 'wh_picking.id')
                ->where('wh_picking_order.delivery_order', $orderNum)
                ->where('wh_picking.pick_pack_status', 3)
                ->first();

            $user = Auth::user();
            $userLocationId = $user->user_warehouse_location;

            if ($pickBatchDetail != NULL) {
                if (in_array($pickBatchDetail->bin->location->id, $userLocationId)) {
                    $detailDO = PickBatchDetail::where('wh_picking_order.delivery_order', $orderNum)
                        ->first();

                    foreach ($detailDO->items as $key2 => $doItem) {
                        if (($doItem->sorted_qty != $doItem->quantity) || $doItem->item_order_status != 1011) {
                            return redirect()
                                ->route('administrator.pickBatch.sortEnd-showPickingOrder', ['pickingOrderId' => $pickBatchDetail->id])
                                ->with('success', 'Correct Do, start scan product now');
                        }
                    }

                    return back()->with('error', 'This Do completed, scan next DO');
                } else {
                    return back()->with('error', 'Wrong Location DO, please contact admin. DO Number: ' . $orderNum);
                }
            } else {
                return back()->with('error', 'Wrong, no this batch order / DO scanned / Sort Done. DO Number:' . $orderNum);
            }
        } else {
            //user/order recevied flow
            // if (!$request->hasValidSignature()) {
            //     abort(401);
            // }

            // $order = Order::where('delivery_order', $orderNum)->first();
            $order = Order::where('delivery_order', $orderNum)->first();
            $order->order_status = 1003;

            if (!$order->received_date) {
                $order->received_date = Carbon::now()->toDateString();
                $message = '';
            } else {
                $message = 'QR code can only be scanned once';
            }



            $order->save();

            $items = Item::where('delivery_order', $orderNum)->get();
            foreach ($items as $item) {
                if (in_array($item->item_order_status, [1008, 1009, 1010])) {
                    $item->item_order_status = 1009;
                    // $acd = DealerStockLedger::where('item_id',$item->id)->first();
                    // $acd->type = 5003;
                    // $acd->save();
                } else {
                    $item->item_order_status = 1003;
                }

                $item->collected_date = Carbon::now()->toDateString();
                $item->save();
            }

            return view('shop.payment.deliveries.qr-scanned')
                ->with('order', $order)->with('message', $message);
        }
    }

    /**
     * Handle QR Code submit.
     */
    public function qrSubmit(Request $request, $order_num)
    {
        $order = Order::where('delivery_order', $order_num)->firstOrFail();
        $order->received_date = Carbon::now()->format('d/m/Y');
        $order->order_status = 1003;
        $order->save();

        $rating = new Rating();
        $rating->customer_id = $order->purchase->user->userInfo->account_id;
        $rating->order_number = $order->order_number;
        $rating->panel_id = $order->panel_id;
        $rating->rating = $request->input('stars');
        $rating->comment = $request->input('rating_comment');
        $rating->save();
        // return $rating;

        $panel = PanelInfo::where('account_id', $order->panel_id)->firstOrFail();
        // return $panel;

        $allRatings = Rating::where('panel_id', $panel->account_id)->get();

        $averageRating = 0;

        if ($allRatings->count() == 0) {
            $allRatings = 1;

            $averageRating = $averageRating / $allRatings;
        } else {
            foreach ($allRatings as $rating) {
                $averageRating = $averageRating + $rating->rating;
            }

            $averageRating = $averageRating / $allRatings->count();
        }

        $panel->panel_rating = $averageRating;
        $panel->save();

        return view('shop.payment.deliveries.qr-submitted');
    }

    public static function purchaseLimitCheck(Request $request)
    {

        $products = json_decode($request->product);
        $popup_data = [];

        foreach ($products as $product_id) {
            $cart = Cart::where('id', $product_id)->first();
            $cart_quantity = $cart->quantity;
            $over_purchase_limit = false;

            $product_code = $cart->product_code;
            $product_attribute = ProductAttribute::where('product_code', $cart->product_code)->first();

            $product_limit = null;
            $period = null;
            $purchase_limit_eligible = false;
            $purchase_limit_left = 100;

            if ($product_attribute->productLimit->isNotEmpty()) {
                $product_limit = $product_attribute->productLimit->where('user_id', auth()->user()->id)->first();
                if(!$product_limit)$product_limit = $product_attribute->productLimit->where('user_id', -1)->first();
                if(!$product_limit) continue;
                $result = $product_limit->checkPurchaseLimit($cart->user_id, $product_limit);

                $purchase_limit_eligible = $result['purchase_limit_eligible'];
                $period = $result['period'];
                $purchase_limit_left = $result['purchase_limit_left'];

                if ($purchase_limit_eligible && $cart_quantity > $purchase_limit_left) {
                    $over_purchase_limit = true;
                    $popup_data[]  = [
                        'cart_quantity' => $cart_quantity,
                        'product_name' => $cart->product->parentProduct->name,
                        'product_attribute' => $cart->product_information['product_size'],
                        'purchase_limit' => $product_limit->amount,
                        'purchase_period' => $period,
                        'purchase_limit_left' => $purchase_limit_left
                    ];

                }

            }
        }

    return $popup_data;

    }
}
