@inject('controller', 'App\Http\Controllers\Warehouse\StockTransfer\StockTransferController')

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>Stock Receive (IN)</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            <div class="col-12 col-md-4 text-md-left my-auto">
                Choose transfer ID or scan box QR code<small class="text-danger">**</small>
                {{-- <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div> --}}
            </div>
        </div>
        <hr>
        <div class="table-responsive m-2">

            <div class="col-12">
                <form action="{{route('warehouse.StockTransfer.receiveStart')}}" id="create-form-id" method="GET" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12 col-md-4 text-md-left my-auto">
                        <p>
                            Select Tranfer<small class="text-danger">*</small>
                        </p>
                    </div>
                    <div class="col-12 col-md-8 form-group">
                        <select name="stockTransferId" class="form-control">
                            <option value="" >Please Select...</option>
                            @foreach($tables as $transfer)
                                {{-- <option value="{{ $location->id }}" >{{ $location->location_key}} - {{$location->location_name}} ({{$location->default_address}})</option> --}}
                                <option value="{{ $transfer->id }}" >
                                    ID :{{$transfer->id}} {{($transfer->transferBox) ? ', Box : ('.$transfer->transferBox->box_name.')' : ''}} - From : {{ $transfer->fromLocation->location_name}} -  To :{{$transfer->toLocation->location_name}} ( Date :{{$transfer->transfer_datetime}})

                                    {{-- ---Product Code : {{$transfer->stockTransferItems[0]->batchItem->batchDetail->product_code}},
                                    {{$transfer->stockTransferItems[0]->batchItem->batchDetail->productAttribute->product2->parentProduct->name}},
                                    {{$transfer->stockTransferItems[0]->batchItem->batchDetail->productAttribute->attribute_name}} --}}
                                </option>
                            @endforeach
                        </select>
                    </div>


                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn btn-danger" style="float: center;"
                            onclick="clickSubmit({{ $user->id }});">Start Receive</button>
                        </div>
                    </div>
                </form>
            </div>

           {{-- <p style="text-align: center"> Please selct Bin</p>
           <select name="bin_select" class="form-control">
               @foreach ($pickBins as $pickBin)
                <option value="{{$pickBin->id}}">{{$pickBin->pick_number}}<option>
               @endforeach
           </select> --}}
            {{-- <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No {{$controller::$header}} Found</p> --}}
        </div>

        <hr>
        <div class="my-input-warehouse input-group input-group-lg mb-3">
            <input type="text" ref="scancode" v-model="scancode" name="scancode" class="bg-form2 form-control" id="my-input-warehouse"
                placeholder="Use Scanner Here" autofocus autocomplete="off" @@change="qrProduct(scancode)">
            <div class="pop-btn">
                <button type="button" class="qrbtn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit(userId) {
    if (confirm('Are you sure start receive?')) {
        document.getElementById("create-form-id").submit();
    }
}

// $('#stock_in_date').datepicker({
//     // uiLibrary: 'bootstrap4',
//     format: 'yyyy-mm-dd',
//     showOtherMonths: true,
//     selectOtherMonths: true
// });
</script>
@endpush
