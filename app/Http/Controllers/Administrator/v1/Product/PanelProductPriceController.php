<?php

namespace App\Http\Controllers\Administrator\v1\Product;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Models\Users\User;
use App\Models\Products\Product;
use App\Models\Products\ProductPrices;
use App\Models\Products\ProductMarkupCategory;
use App\Models\Categories\Category;
use App\Traits\AttributeImage;

class PanelProductPriceController extends Controller
{
    use AttributeImage;
    public function index(Request $request)
    {
        $user = User::find(Auth::user()->id);

        $productPrices = ProductPrices::orderBy('panel_product_prices.product_code', 'DESC')
            ->orderBy('model_type', 'ASC')
            ->searchFilter($request->searchBox, $request->selectSearch, $request->categories);

        $count = $productPrices->get();

        $productPrices = $productPrices->paginate(100);
        $productPrices->appends(request()->query());

        $lists = array();

        foreach ($productPrices as $product) {

            // return $product;

            $lists[$product->model_id]['panelProductID'] = (isset($product->model['id'])) ? $product->model['id'] : '';
            $lists[$product->model_id]['model_type'] = $product->model_type;
            $lists[$product->model_id]['product_code'] = (isset($product->model->parentProduct->product_code)) ? $product->model->parentProduct->product_code : $product->model['product_code'];
            $lists[$product->model_id]['product_name'] = (isset($product->model->parentProduct->name)) ? $product->model->parentProduct->name : $product->model['attribute_name'];

            if (isset($product->model->parentProduct->id)) {
                // $lists[$product->model_id]['product_image'] = $product->model->parentProduct->id;
                $lists[$product->model_id]['product_image'] = $this->getImageUrlFromID($product->model->parentProduct->id);
            } else {
                $lists[$product->model_id]['product_image'] = 0;
            }

            $lists[$product->model_id]['cPrice'] = ProductPrices::where('model_id', $product->model_id)->get();

            if (isset($product->model['markup_category'])) {
                if (!empty($product->model['markup_category'])) {
                    $lists[$product->model_id]['markup_category'] = $product->model->markupCategories[0]->cat_name;
                } else {
                    $lists[$product->model_id]['markup_category'] = 'NONE';
                }
            } else {
                $lists[$product->model_id]['markup_category'] = 'AtrributePrice';
            }

            $lists[$product->model_id]['countryPrice'][] = $product;
        }

        if ($request->categories || $request->searchBox) {
            //return $lists;
        }

        $markupCategories = ProductMarkupCategory::get();
        $categories = Category::select('id', 'name')->orderBy('name', 'ASC')->get();

        return view('administrator.products.v1.panel.priceIndex')
            ->with('user', $user)
            ->with('lists', $lists)
            ->with('request', $request)
            ->with('productPrices', $productPrices)
            ->with('categories', $categories)
            ->with('markupCategories', $markupCategories)
            ->with('count', $count);
    }

    public function edit($id)
    {
        try {
            $productPrice = ProductPrices::find($id);

            return view('administrator.products.v1.panel.edit-price')
                ->with('productPrice', $productPrice);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $price = ProductPrices::find($id);
        $price->fixed_price = str_replace('.', '', $request->input('fixed_price')) != "" ? str_replace('.', '', $request->input('fixed_price')) : 0;
        $price->price = str_replace('.', '', $request->input('price'));
        $price->member_price = str_replace('.', '', $request->input('member_price'));
        $price->web_offer_price = str_replace('.', '', $request->input('web_offer_price'));
        $price->offer_price = str_replace('.', '', $request->input('offer_price'));
        $price->product_sv = $request->input('product_sv');

        $price->save();

        if (!empty($request->input('fixed_showitem'))) {
            $product = Product::find($price->model_id);
            $product->fixed_showitem = ($request->input('fixed_showitem') != 1) ? 0 : 1;
            $product->save();
        }

        return redirect()->back()->with('success', 'Success update : ' . $price->product_code . ' .');
    }

    public function editMarkup($id)
    {
        try {
            $panelProduct = Product::find($id);

            $markupCategories = ProductMarkupCategory::get();

            return view('administrator.products.v1.panel.edit-markup')
                ->with('panelProduct', $panelProduct)
                ->with('markupCategories', $markupCategories);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function updateMarkup(Request $request, $id)
    {
        $panelProduct = Product::find($id);

        $panelProduct->markup_category = $request->input('category_id');

        $panelProduct->save();

        $panelProduct->markupCategories()->sync($request->input('category_id'));

        // return redirect()->back()->with('success', 'Success update : ' . $panelProduct->parentProduct->product_code . ' .');

    }
}
