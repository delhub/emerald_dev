<?php

namespace App\Http\Controllers\Administrator\faq;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq\Faq;
use App\Models\Globals\Status;
use Illuminate\Support\Facades\Validator;
use DB;

class FaqController extends Controller
{
    public function index() {
        $faqs = Faq::orderBy('created_at','desc')->paginate(10);

        $statuses = Status::where('status_type', 'faq_type')->get();

        return view('administrator.faq.index', compact('faqs','statuses'));

        //return view('administrator.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create() {
        $faqs = Faq::all();

        $statuses = Status::where('status_type', 'faq_type')->get();

        return view('administrator.faq.create',compact(['faqs','statuses']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $this->validate($request, [
            'id' => 'required',
            'id_status' => 'required',
            'question' => 'required',
            'answer' => 'required'
        ]);

        $statuses = Status::where('status_type', 'faq_type')->get();

        $faq = new Faq;

        $faq->id = $request->input('id');
        // $faq->faq_type = $request->input('faq_type');
        $faq->faq_type = $request->input('id_status');
        $faq->question = $request->input('question');
        $faq->answer = $request->input('answer');
        $faq->faq_status = ($request->input('faq_status') != 1) ? 0 : 1;

        $faq->save();

        return redirect('/administrator/faq/')->with('success', 'submit done.');
    }

     /**
     * Store Image a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function show()
    // {
    //     return view('administrator.faq.show');
    // }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $faq = Faq::find($id);

        $statuses = Status::where('status_type', 'faq_type')->get();

        return view('administrator.faq.edit')->with('faq', $faq)->with('statuses', $statuses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'id' => 'required',
            'id_status' => 'required',
            'question' => 'required',
            'answer' => 'required'
        ]);

        $faq = Faq::find($id);
        $faq->id = $request->input('id');
        // $faq->faq_type = $request->input('faq_type');
        $faq->faq_type = $request->input('id_status');
        $faq->question = $request->input('question');
        $faq->answer = $request->input('answer');
        $faq->faq_status = ($request->input('faq_status') != 1) ? 0 : 1;

        $faq->save();

        return redirect('/administrator/faq/')->with('success', 'Faq Updates');
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);
        $faq->delete();
        return redirect('/administrator/faq/')->with('success', 'Faq Remove');
    }

    public function publishFaq(Request $request, $id)
    {
        $faq = Faq::findOrFail($id);
        $faq->faq_status = 1;
        $faq->save();

        return redirect('/administrator/faq');
    }

    public function unpublishFaq(Request $request, $id)
    {
        $faq = Faq::findOrFail($id);
        $faq->faq_status = 0;
        $faq->save();

        return redirect('/administrator/faq');
    }

}


