<?php

namespace App\Models\Purchases;

use App\Http\Controllers\Administrator\Archimedes\ArchimedesController;
use App\Http\Controllers\Warehouse\Order\OrderController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Products\ViewInventoriesStock;
use App\Models\Globals\Outlet;
use App\Models\Warehouse\Order\WarehouseOrder;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    // Set table
    protected $table = 'orders';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'order_number';

    // Set incremeneting to false.
    public $incrementing = false;

    // Set mass assignable columns
    protected $fillable = [
        'id', 'order_number', 'delivery_order', 'po_do_version', 'purchase_id', 'panel_id', 'order_status', 'order_amount', 'point_amount', 'order_type', 'delivery_method', 'delivery_outlet', 'delivery_info', 'delivery_date', 'received_date', 'claim_status', 'courier_name', 'tracking_number', 'shipment_key', 'order_date', 'est_shipping_date', 'shipping_date', 'collected_date', 'admin_remarks', 'country_id', 'payment_type', 'pick_pack_status', 'purchases_rbs_id'
    ];

    public static function boot()
    {
        parent::boot();

        self::updated(function ($order) {

            // check if previous statius == 1000 and next status  == 1001 or 1010 or 1005
            $old_status = $order->getOriginal('order_status');
            $new_status = $order->order_status;

            if (in_array($new_status, [1010, 1005, 1001]) && $old_status == 1000) {

                // if not pre purchase product, create warehouse order - if it is pre purchase product, only affect warehouse when user redeemed
                $pre_purchase = ArchimedesController::checkPrePurchaseProduct($order);


                Log::channel('debugg')->info('before created warehouse order  new_status' . $new_status . ', pre_purchase - ' . json_encode($pre_purchase));

                $purchase = $order->purchase;
                $notExistingWarehouseOrder = true;
                if (!is_null($purchase)) {
                    $notExistingWarehouseOrder = WarehouseOrder::where('invoice_number', $purchase->purchase_number)->doesntExist();
                }

                if (!empty($pre_purchase['normal']) && $notExistingWarehouseOrder) {
                    $data = self::create_new_warehouse_order($order, $pre_purchase['normal']);
                    OrderController::createNew($data);
                    Log::channel('debugg')->info('Order Model data - ' . json_encode($data));
                }

                if ($new_status != 1005) {
                    // agent buy
                    if (!empty($pre_purchase['vqs'])) {
                        ArchimedesController::addDealerData($pre_purchase['vqs'], 5002);
                    }
                    // // user buy
                    // if (!empty($pre_purchase['buyVqs'])) {
                    //     ArchimedesController::addDealerData($pre_purchase['buyVqs']);
                    // }
                }
            }
        });
    }

    public static function create_new_warehouse_order($order)
    {
        $order_data = array();
        $type = 'delivery';
        $locationId = 2;

        //warehouse order
        if (!empty($order->delivery_outlet) && $order->delivery_method == 'Self-pickup') {
            $type = 'selfpickup';

            $outlet = Outlet::where('id', $order->delivery_outlet)->first();
            $locationId = $outlet->warehouseLocation->id;
        }

        $order_data['location_id'] = $locationId;
        $order_data['inv_type'] = ($order->order_status == 1001) ? 8001 : 8011;
        $order_data['type']  = $type;
        $order_data['date'] = Carbon::now();
        $order_data['invoice_number'] = $order->purchase->purchase_number;
        $order_data['delivery_order'] = $order->delivery_order;

        $warehouseOrderId = OrderController::process_WarehouseOrder($order_data);

        //inventory record
        foreach ($order->items as  $item) {
            $inventoryRecords[$item->product_code]['outlet_id'] = $locationId;
            $inventoryRecords[$item->product_code]['inv_type'] = ($order->order_status == 1001) ? 8001 : 8011;
            $inventoryRecords[$item->product_code]['inv_amount'] = - ($item->quantity);
            $inventoryRecords[$item->product_code]['inv_user'] = $order->purchase_id;
            $inventoryRecords[$item->product_code]['model_type'] = 'App\Models\Warehouse\Order\WarehouseOrder';
            $inventoryRecords[$item->product_code]['model_id'] = $warehouseOrderId;
            $inventoryRecords[$item->product_code]['product_code'] = $item->product_code;

            $arryaData[] = [
                'product_name' => $item->product->parentProduct->name,
                'product_code' => $item->product_code,
                'quantity' => - ($item->quantity)
            ];
        }

        $WarehouseOrder = WarehouseOrder::where('id', $warehouseOrderId)->first();
        $WarehouseOrder->order_data = json_encode($arryaData);

        $WarehouseOrder->save();

        return array('order_data' => $order_data, 'inventoryRecords' => $inventoryRecords);
    }

    /**
     * Get all items belongin to the order.
     */
    public function items()
    {
        return $this->hasMany('App\Models\Purchases\Item', 'order_number', 'order_number');
    }

    /**
     * Get purchase of the order.
     */
    public function purchase()
    {
        return $this->belongsTo('App\Models\Purchases\Purchase', 'purchase_id');
    }

    /* Get panel of the order */
    public function panel()
    {
        return $this->belongsTo('App\Models\Users\Panels\PanelInfo', 'panel_id', 'account_id');
    }

    /*Get orders belonging to dealer sales**/
    public function dealerSales()
    {
        return $this->hasMany('App\Models\Users\Dealers\DealerSales', 'order_number', 'order_number');
    }

    /**
     * Get order status.
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Globals\Status', 'order_status', 'id');
    }

    public function outlet()
    {
        return $this->hasOne('App\Models\Globals\Outlet', 'id', 'delivery_outlet');
    }

    /**Get Order Date */
    public function getFormattedDate()
    {
        return $this->created_at->format('d/m/Y');
    }

    /****Get order number of purchase*** */
    public function getOrderNumberFormatted()
    {
        return substr($this->order_number, 0);
    }
    public function getDeliveryOrderNumberFormatted()
    {
        return substr($this->delivery_order, 0);
    }

    // Filter
    // Panel ID
    public function scopepanelFilter($query, $panelFilter)
    {
        if (!is_null($panelFilter)) {
            return $query->where('panel_id', $panelFilter);
        }
        return $query;
    }

    // status
    public function scopestatusFilter($query, $statusFilter)
    {
        if (!is_null($statusFilter)) {
            if ($statusFilter == 'all') {
                // return $query->whereIn('order_status', [1000, 1001, 1002, 1003, 1004,]);
            } else {
                return $query->where('orders.order_status', $statusFilter);
            }
        }
        return $query;
    }

    /***Get pending days of the order ***/
    public function getPendingAttribute()
    {
        if (in_array($this->order_status, [1001, 1002, 1005, 1010])) {
            $now = Carbon::now();
            $now->endOfDay();
            $date = Carbon::parse($this->created_at);
            $date->startOfDay();
            return  $date->diffInDays($now);
        } else {
            return 0;
        }
    }

    //States
    public function scopestatesFilter($query, $statesFilter)
    {
        if (!is_null($statesFilter)) {
            if ($statesFilter == 'all') {

                return $query->whereRaw('1 = 1');
            } else {

                $purchases = DB::table('purchases')
                    ->where('ship_state_id', $statesFilter)->get();

                $purchases_array = [];
                foreach ($purchases as $purchase) {
                    $purchases_array[] =  $purchase->id;
                }

                // return $query->whereIn('id', $purchases_array);

                return $query->whereIn('orders.purchase_id', $purchases_array);
            }
        }

        return $query;
    }

    //date filter from date to date
    public function scopedatebetweenFilter($query, $datefrom, $dateto)
    {
        $from = Carbon::parse($datefrom);
        $to = Carbon::parse($dateto);

        $from->startOfDay();
        $to->endOfDay();

        if (!is_null($datefrom) && !is_null($dateto)) { //when user entry datefrom & dateto

            $query
                ->whereBetween('orders.created_at', [$from, $to]);
        } elseif (isset($datefrom)) { //when user entry datefrom only

            $endOfDatefrom = Carbon::parse(' ' . $datefrom . ' 23:55:59');

            return $query
                ->whereBetween('orders.created_at', [$from, $endOfDatefrom]);
        } elseif (isset($dateto)) { //when user entry dateto only

            $startOfDateto = Carbon::parse(' ' . $dateto . ' 00:00:00');

            return $query
                ->whereBetween('orders.created_at', [$startOfDateto, $to]);
        }
    }

    // Search Bar
    public function scopesearchFilter($query,  $searchFilter, $selectSearch)
    {
        if (!is_null($searchFilter)) {

            if (($selectSearch == 'PODO')) {
                return $query
                    ->where('orders.order_number', 'like', '%' . $searchFilter . '%')
                    ->orwhere('orders.delivery_order', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'INV') {
                return $query
                    ->join('purchases', 'purchases.id', '=', 'orders.purchase_id')
                    ->where('purchases.inv_number', 'like', '%' . $searchFilter . '%');
            } elseif ($selectSearch == 'BJN') {
                return $query
                    ->join('purchases', 'purchases.id', '=', 'orders.purchase_id')
                    ->where('purchases.purchase_number', 'like', '%' . $searchFilter . '%');
            }
        }

        return $query;
    }

    //Stock Filter
    public function scopestockFilter($query, $stock_filter)
    {
        if (!is_null($stock_filter)) {
            if ($stock_filter == 'all') {

                return $query->whereRaw('1 = 1');
            } else {

                $inventory = ViewInventoriesStock::where('physical_stock', 0)->get();

                $productCode = array();
                foreach ($inventory as $key => $attribute) {
                    $productCode[] = $attribute->product_code;
                }

                $order_numbers = $this
                    ->join('items', 'orders.order_number', '=', 'items.order_number')
                    ->whereIn('product_code', $productCode)
                    ->get('orders.order_number');

                if ($stock_filter == 'has_stock') {
                    return $query->whereNotIn('orders.order_number', $order_numbers);
                } elseif ($stock_filter == 'no_stock') {
                    return $query->whereIn('orders.order_number', $order_numbers);
                }
            }
        }

        return $query;
    }

    // find items.id
    public function scopefindID($query, $ids)
    {
        $findid = explode(",", $ids);

        if (!is_null($ids)) {

            return $query
                ->whereIn('items.id', $findid);
        }
    }
}
