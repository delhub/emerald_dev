<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Register\CustomerRegisterController;
use Illuminate\Http\Request;

use App\Models\Users\User;
use App\Models\Users\UserConnectedPlatform;
use App\Models\Users\UserInfo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Passport\ClientRepository;

class ApiAuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            /* 'name' => 'required|string|max:255', */
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            /* 'type' => 'integer', */
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        // $request['type'] = $request['type'] ? $request['type']  : 0;
        $user = User::create($request->toArray());

        // (new ClientRepository)->createPasswordGrantClient($user->id, 'Platform', 'http://test.com/callback');

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token];
        return response($response, 200);
    }

    public function registerNew(Request $request)
    {

        $userNric = UserInfo::where('nric', $request->nric)->exists();
        $userEmail = User::where('email', $request->email)->exists();

        if ($userEmail || $userNric) {
            $response = ["message" => 'User already exist'];
            return response($response, 200);
        } else {
            $user = CustomerRegisterController::createCustomer($request, true, 'MY');

            if ($user) {

                $user->notes = NULL;
                $user->save();

                if (Hash::check($request->password, $user->password) || isset($request->hashPass)) {

                    $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                    $response = ['token' => $token];
                    if ($request->access_token) {
                        $connPlatform = UserConnectedPlatform::where('user_id', $user->id)->first();
                        $connPlatform = ($connPlatform) ? $connPlatform : new UserConnectedPlatform;
                        $connPlatform->user_id = $user->id;
                        $connPlatform->platform_key = 'META';
                        $connPlatform->secret_key = $request->access_token;
                        $connPlatform->account_id = $request['user']['account_id'];
                        $connPlatform->email = $request['user']['email'];
                        $connPlatform->save();

                        $user->account_id = $user->userInfo->account_id;
                        $user->agent_id = optional($user->dealerInfo)->account_id ?? null;
                        unset($user->userInfo);
                        $response = array_merge($response, ['user' => $user->only('email', 'account_id', 'agent_id')]);
                    }

                    return response($response, 200);
                }
            }
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|exists:users,email',
            'password' => 'required|string|min:6',
        ], [
            'email.exists' => 'User does not exist'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()], 422);
        }

        $user = User::where('email', $request->email)->first();
        $passwordCheck = Hash::check($request->password, $user->password);
        return $this->platformConnect($request, $user, $passwordCheck);
    }

    public function platformConnect(Request $request, $user, $passwordCheck){
        $platform_key = $request->origin_platform ?? 'META';

        $exstAcc = UserConnectedPlatform::where(['user_id' => $user->id, 'platform_key' => $platform_key])->first();
        if ($exstAcc && (strtolower($exstAcc->email) != strtolower($request['user']['email']))) return ['errors' => ['email' => "The account has been tied to another {$platform_key} email"]];
        if ($user) {

            if ($passwordCheck) {
                if ($request->user_expired) {
                    UserConnectedPlatform::where(['user_id' => $user->id, 'platform_key' => $platform_key])->delete();
                    $exstAcc = null;
                }
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token];
                if ($request->access_token) {
                    $connPlatform = ($exstAcc) ? $exstAcc : new UserConnectedPlatform;
                    $connPlatform->user_id = $user->id;
                    $connPlatform->platform_key = $platform_key;
                    $connPlatform->secret_key = $request->access_token;
                    $connPlatform->account_id = $request['user']['account_id'];
                    $connPlatform->email = $request['user']['email'];
                    $connPlatform->save();

                    $user->account_id = $user->userInfo->account_id;
                    $user->agent_id = optional($user->dealerInfo)->account_id ?? null;
                    unset($user->userInfo);
                    $response = array_merge($response, ['user' => $user->only('email', 'account_id', 'agent_id')]);
                }

                return response($response, 200);
            } else {
                $response = ["errors" => ["password" => "Password mismatch"]];
                return response($response, 422);
            }
        } else {
            $response = ["message" => 'User does not exist'];
            return response($response, 422);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }
}
