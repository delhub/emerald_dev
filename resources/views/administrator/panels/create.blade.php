@inject('controller', 'App\Http\Controllers\Administrator\Panels\PanelsController')

@extends('layouts.administrator.main')

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{route($controller::$home)}}" class="btn btn-dark" style="border-radius: 0.25rem; color: #ffffff;">Go Back Home</a>
        </div>
    </div>
    <div class="card shadow-sm mt-3">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>
                        Create {{$controller::$header}}
                    </h4>
                    @include('inc.messages')
                </div>
                <div class="col-12">
                    <form action="{{route($controller::$store)}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Company Name <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="company_name" name="company_name" class="form-control" value="{{ old('company_name') }}">
                            </div>
                        </div>

                        {{-- <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Company Name for display <small class="text-danger">*</small>
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="name_for_display" name="name_for_display" class="form-control" value="{{ old('name_for_display') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    SSM Number
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="ssm_number" name="ssm_number" class="form-control" value="{{ old('ssm_number') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Company Email
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="company_email" name="company_email" class="form-control" value="{{ old('company_email') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    Company Phone
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="company_phone" name="company_phone" class="form-control" value="{{ old('company_phone') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    PIC Name
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="pic_name" name="pic_name" class="form-control" value="{{ old('pic_name') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    PIC NRIC
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="pic_nric" name="pic_nric" class="form-control" value="{{ old('pic_nric') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    pic_contact
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="pic_contact" name="pic_contact" class="form-control" value="{{ old('pic_contact') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 text-md-right my-auto">
                                <p>
                                    pic_email
                                </p>
                            </div>
                            <div class="col-12 col-md-8 form-group">
                                <input type="text" id="pic_email" name="pic_email" class="form-control" value="{{ old('pic_email') }}">
                            </div>
                        </div> --}}

                        <div class="row">
                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary" value="submit">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


