@php  use App\Http\Controllers\Agent\AgentController; @endphp
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <title>CP58 Form - {{$data['biz_year']}}</title>
</head>

<style>
    /*
	 CSS-Tricks Example
	 by Chris Coyier
	 http://css-tricks.com
    */

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font: 13px/1.2 Arial, Georgia, serif;
    }

    #page-wrap {
        width: 950px;
        margin: 0 auto;
    }

    textarea {
        border: 0;
        font: 15px/1.2 Arial, Georgia, Serif;
        overflow: hidden;
        resize: none;
    }

    table {
        border-collapse: collapse;
    }

    table td,
    table th {
        border: 1px solid black;
        padding: 5px;
    }

    #header {
        height: 15px;
        width: 100%;
        margin: 20px 0;
        background: #222;
        text-align: center;
        color: white;
        font: bold 15px Helvetica, Sans-Serif;
        text-decoration: uppercase;
        letter-spacing: 20px;
        padding: 8px 0px;
    }

    #header-year {
        height: 20px;
        width: 100%;
        margin: 20px 0;
        background: #222;
        text-align: center;
        color: white;
        font: bold 20px Helvetica, Sans-Serif;
        text-decoration: uppercase;
        letter-spacing: 20px;
        padding: 8px 0px;
    }

    #header-part {
        height: 15px;
        width: 100%;
        margin: 10px 0;
        background: #222;
        text-align: left;
        color: white;
        font: bold 13px Helvetica, Sans-Serif;
        text-decoration: uppercase;
        letter-spacing: 1px;
        padding: 2px 6px;
    }

    #address {
        width: 250px;
        height: 150px;
        float: left;
    }

    #customer {
        overflow: hidden;
    }

    #logo {
        text-align: right;
        float: right;
        position: relative;
        margin-top: 25px;
        border: 1px solid #fff;
        max-width: 540px;
        max-height: 100px;
        overflow: hidden;
    }

    #logo:hover,
    #logo.edit {
        border: 1px solid #000;
        margin-top: 0px;
        max-height: 125px;
    }

    #logoctr {
        display: none;
    }

    #logo:hover #logoctr,
    #logo.edit #logoctr {
        display: block;
        text-align: right;
        line-height: 25px;
        background: #eee;
        padding: 0 5px;
    }

    #logohelp {
        text-align: left;
        display: none;
        font-style: italic;
        padding: 10px 5px;
    }

    #logohelp input {
        margin-bottom: 5px;
    }

    .edit #logohelp {
        display: block;
    }

    .edit #save-logo,
    .edit #cancel-logo {
        display: inline;
    }

    .edit #image,
    #save-logo,
    #cancel-logo,
    .edit #change-logo,
    .edit #delete-logo {
        display: none;
    }

    #customer-title {
        font-size: 20px;
        font-weight: bold;
        float: left;
    }

    #meta01 {
        margin-top: 1px;
        width: 650px;
        float: right;
    }

    #meta01 td {
        text-align: right;
        padding: 5px;
    }

    #meta01 td.meta-head {
        text-align: left;
        background: #eee;
    }

    #meta01 td textarea {
        width: 100%;
        height: 30px;
        text-align: right;
    }


    #meta02 {
        margin-top: 1px;
        width: 720px;
        float: right;
    }

    #meta02 td {
        text-align: right;
        padding: 5px;
    }

    #meta02 td.meta-head {
        text-align: left;
        background: #eee;
    }

    #meta02 td textarea {
        width: 100%;
        height: 30px;
        text-align: right;
    }


    #meta03 {
        margin-top: 1px;
        width: 650px;
        float: right;
    }

    #meta03 td {
        text-align: right;
        padding: 2px;
    }

    #meta03 td.meta-head {
        text-align: left;
        background: #eee;
    }

    #meta03 td textarea {
        width: 100%;
        height: 15px;
        text-align: right;
    }

    #metaOthers {
        margin-top: 0px;
        width: 500px;
        float: right;
        margin-right: 50px;
    }

    #metaOthers td {
        text-align: right;
        padding: 0px;
    }

    #metaOthers td.meta-head {
        text-align: left;
        background: #eee;
    }

    #metaOthers td textarea {
        width: 100%;
        height: 13px;
        text-align: right;
    }

    #meta04 {
        margin-top: 1px;
        width: 500px;
        float: right;
    }

    #meta04 td {
        text-align: right;
        padding: 2px;
    }

    #meta04 td.meta-head {
        text-align: left;
        background: #eee;
    }

    #meta04 td textarea {
        width: 100%;
        height: 15px;
        text-align: right;
    }

    #meta05 {
        margin-top: 1px;
        width: 180px;
        float: right;
    }

    #meta05 td {
        text-align: right;
        padding: 0px;
    }

    #meta05 td.meta-head {
        text-align: right;
        background: #eee;
    }

    #meta05 td textarea {
        width: 100%;
        height: 12px;
        text-align: right;
    }

    #meta05-1 {
        width: 180px;
        float: right;
        height: 20px;
    }

    #meta05-1 td {
        text-align: right;
        padding: 0px;
    }

    #meta05-1 td.meta-head {
        text-align: right;
        background: #eee;
    }

    #meta05-1 td textarea {
        width: 100%;
        height: 12px;
        text-align: right;
    }

    #meta06 {
        margin-top: 1px;
        width: 320px;
        float: right;
    }

    #meta06 td {
        text-align: right;
        padding: 0px;
    }

    #meta06 td.meta-head {
        text-align: center;
        background: #eee;
        font-weight: bold;
        width: 15px
    }

    #meta06 td textarea {
        width: 100%;
        height: 12px;
        text-align: right;
    }

    #meta07 {
        margin-top: 1px;
        width: 400px;
    }

    #meta07 td {
        text-align: right;
        padding: 0px;
        border: 1px solid black;
    }

    #meta07 td.meta-head {
        text-align: center;
        background: #eee;
        font-weight: bold;
        width: 15px;
        border: 1px solid black
    }

    #meta07 td textarea {
        width: 100%;
        height: 12px;
        text-align: right;
    }


    #meta {
        margin-top: 1px;
        width: 300px;
        float: right;
    }

    #meta td {
        text-align: right;
    }

    #meta td.meta-head {
        text-align: left;
        background: #eee;
    }

    #meta td textarea {
        width: 100%;
        height: 20px;
        text-align: right;
    }

    #items {
        clear: both;
        width: 100%;
        margin: 30px 0 0 0;
        border: 0px solid black;
    }

    #items th {
        background: #eee;
    }

    #items textarea {
        width: 80px;
        height: 20px;
    }

    #items tr.item-row td {
        border: 0;
        vertical-align: top;
    }

    #items td.description {
        width: 300px;
    }

    #items td.description2 {
        width: 120px;
    }

    #items td.date {
        width: 100px;
    }

    #items td.qty {
        width: 80px;
    }

    #items td.item-name {
        width: 175px;
    }

    #items td.date textarea,
    #items td.item-name textarea {
        width: 100%;
    }

    #items td.description textarea,
    #items td.item-name textarea {
        width: 100%;
    }

    #items td.description2 textarea,
    #items td.item-name textarea {
        width: 100%;
    }

    #items td.total-line {
        border-right: 0;
        text-align: right;
    }

    #items td.total-value {
        border-left: 0;
        padding: 10px;
    }

    #items td.total-value textarea {
        height: 20px;
        background: none;
    }

    #items td.balance {
        background: #eee;
    }

    #items td.blank {
        border: 0;
    }

    #terms {
        text-align: left;
        margin: 10px 0 0 0;
        font: 9px Helvetica;
    }

    #terms h5 {
        text-transform: uppercase;
        font: 13px Helvetica, Sans-Serif;
        letter-spacing: 10px;
        border-bottom: 1px solid black;
        padding: 0 0 8px 0;
        margin: 0 0 8px 0;
    }

    #terms textarea {
        width: 100%;
        text-align: left;
    }

    #notes {
        text-align: center;
        margin: 0 0 0 0;
        font: 11px Helvetica;
    }

    #notes h5 {
        text-transform: uppercase;
        font: 13px Helvetica, Sans-Serif;
        letter-spacing: 10px;
        border-bottom: 1px solid black;
        padding: 0 0 8px 0;
        margin: 0 0 8px 0;
    }

    #notes textarea {
        width: 100%;
        text-align: center;
    }

    #right {
        float: right;
    }

    #number {
        letter-spacing: 5px;
        margin: 0 auto;
    }

    textarea:hover,
    textarea:focus,
    #items td.total-value textarea:hover,
    #items td.total-value textarea:focus,
    .delete:hover {
        background-color: #EEFF88;
    }

    .delete-wpr {
        position: relative;
    }

    .delete {
        display: block;
        color: #000;
        text-decoration: none;
        position: absolute;
        background: #EEEEEE;
        font-weight: bold;
        padding: 0px 3px;
        border: 1px solid;
        top: -6px;
        left: -22px;
        font-family: Verdana;
        font-size: 12px;
    }

    /* print.css */
    #hiderow,
    .delete {
        display: none;
    }

    .ml-15 { margin-left: 15px }
    .ml-2 { margin-left: 2px }
    .meta-grey { text-align: center; background: #eee; font-weight: bold; width: 22px; display: inline-block; border: 1px solid black;}
    .fs-13 { font-size: 13px }
    .d-flex { display: -webkit-box; /* wkhtmltopdf uses this one */ display: flex }
    .mt-3 {margin-top: 3px }
    .mt-1 {margin-top: 1px }
    .mb-3 {margin-bottom: 3px }
    #meta07 td.meta-head.meta-empty { background: #ffffff; border: 0px; }
    .triangle-up { width: 0; height: 0; border-left: 8px solid transparent; border-right: 8px solid transparent; border-bottom: 13px solid #000000; margin-left: 34px}
    #metaOthers td.td-specify { text-align: center; padding: 0px; border: 0px }
    .ta-r { text-align: right }
    .flex-end { justify-content: flex-end }
    .flex-middle { align-items: center }
    .flex-between { -webkit-box-pack: justify; justify-content: space-between; }
    .mr-25 { margin-right: 25px; }
    .mt-10 { margin-top: 10px }
    .mtb-10 { margin: 10px 0px }
    .mt-5 { margin-top: 5px }
    .text-center { text-align: center }
    .jumlah { text-align: right; width: 740px; display: inline-block; }
    #taxDeduction > table { width: 100% }
    #taxDeduction table td { padding: 0px; border: 0px }
    #taxDeduction table td.vtop { vertical-align: top }
    #taxDeduction table td.vbottom { vertical-align: bottom }
    #taxDeduction table td.border1 { border: 1px solid black }
    .smaller-header { font-size: 14px }
    .info-small { font-size : 12px}
    table#metaCukai { margin: 5px 0px; width: 80% }
    table#metaCukai td { border: 1px solid rgb(255, 255, 255); padding: 0px; vertical-align: top }
    #rightFooter { text-align: right; }

</style>

<body style="padding-top:50px;">
    <div id="page-wrap">

        <!--<div id="logo">
              <img id="image" src="dc-logo-s.png" alt="logo" />
            </div>-->
        <div id="right"><b>CP58 <font size=-1>[Pin. 1/2022]</font></b></div>
        <br>
        <center>
            <span class="smaller-header">
            <b>PENYATA BAYARAN INSENTIF BERBENTUK WANG TUNAI DAN BUKAN WANG TUNAI KEPADA EJEN,
                <br>PENGEDAR ATAU PENGAGIH TERTAKLUK KEPADA SEKSYEN 83A AKTA CUKAI PENDAPATAN 1967</b>
            <i>
                <br> STATEMENT OF MONETARY AND NON-MONETARY INCENTIVE PAYMENT TO AN AGENT,
                <br> DEALER OR DISTRIBUTOR PURSUANT TO SECTION 83 A OF THE INCOME TAX ACT 1967</i>
            </span>
            <div id="notes">
                Borang ini ditetapkan di bawah seksyen 152 Akta Cukai Pendapatan 1967
                <br><i>This form is prescribed under section 152 of the Income Tax Act 1967</i>
                <h5>
                    <!--Terms-->
                </h5>
            </div>
            <table style="border:0px;padding: 0px;">
                <tr>
                    <td style="border:0px;padding: 0px;">
                        <div id="notes">
                            BAGI TAHUN BERAKHIR 31 DISEMBER</div>
                    </td>
                    <td rowspan=2 style="border:2px;">
                        <h3><b>{{$data['biz_year']}}</b></h3>
                    </td>

                </tr>
                <tr>
                    <td style="border:0px;padding: 0px;">
                        <div id="notes"><i>FOR THE YEAR ENDED 31 DECEMBER</i></div>
                    </td>
                </tr>
            </table>


        </center>

        <textarea id="header-part">BAHAGIAN A / PART A : MAKLUMAT SYARIKAT PEMBAYAR / PAYER COMPANY'S PARTICULARS</textarea>
        <br>
        1. Nama / <i>Name</i>
        <table id="meta01">
            <tr>
                <td class="meta-head"><b>{{strtoupper(country()->company_name)}}</b><br>{!! AgentController::giveSpace(1) !!}</td>
            </tr>
        </table>
        <br><br><br>

        2. Alamat / <i>Address</i>
        <table id="meta01">
            <tr>
                <td class="meta-head"><b>{{strtoupper(strip_tags(country()->company_address))}} </b>
                </td>
            </tr>
        </table>
        <br><br><br>

        3. No. Rujukan (No. Pendaftaran) / <i>Reference No. (Registration No.)</i> :


        <table id="meta06">
            <tr>

               {!! AgentController::metaHead('1318429-V ') !!}
            </tr>
            <tr>
                 {!! AgentController::metaHead('2588004503') !!}
            </tr>
        </table>

        <br>
        4. No. Cukai Pendapatan / <i>Income Tax No.</i> :
        {!! AgentController::giveSpace(42) !!}<b>C</b>

        <textarea id="header-part">BAHAGIAN B / PART B : MAKLUMAT PENERIMA / RECIPIENT'S PARTICULARS</textarea>
        <br>

        1. Nama / <i>Name</i>
        <table id="meta01">
            <tr>
                <td class="meta-head"><b>{{strtoupper($dealer->full_name)}}</b></td>
            </tr>
        </table>
        <br><br><br>

        2. Alamat / <i>Address</i>
        <table id="meta01">
            <tr>
                <td class="meta-head" style="line-height:"><b>{{strtoupper($address)}}</b></td>
            </tr>
        </table>
        <br><br><br>

        3. No. Pendaftaran / Kad Pengenalan / Polis / Tentera / Pasport * :

        <table id="meta06">
            <tr>
                 {!! AgentController::metaHead($dealer->nric) !!}
            </tr>
        </table>
        <br>
        <i class="ml-15">Registration / Identity Card / Police / Army / Passport No. *</i>
        <br>
        <span class="fs-13 ml-15">(* Potong yang tidak berkenaan /<i> Delete whichever is not applicable</i>)</span>
        <br>
        <table id="metaCukai">
            <tr>
                <td>4. No. Cukai Pendapatan / <i>Income Tax No.</i> :</td>
                <td>
                    <table id="meta07" class="ml-15">
                        <tr>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head meta-empty">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                            <td class="meta-head">{!! AgentController::giveSpace(1) !!}</td>
                        </tr>
                        <table>
                            <tr><div class="triangle-up"></div></tr>
                            <tr><div class="fs-13 ml-15"> Isi / Enter : C / C1 / TN / PT / SG / OG / D / TP / TJ / TF </div></tr>
                        </table>
                    </table>
                </td>
            </tr>
        </table>
        5. Mastautin di Malaysia / <i>Resident in Malaysia</i> : <span class="meta-grey" ><b>  1</b></span>
        <font size=-1>{!! AgentController::giveSpace(3) !!}1 = Ya / <i>Yes</i>{!! AgentController::giveSpace(4) !!} 2 = Tidak / <i> No</i></font>

        <textarea
            id="header-part">BAHAGIAN C / PART C : MAKLUMAT BAYARAN INSENTIF / PARTICULARS OF INCENTIVE PAYMENT</textarea>
        <br>

        <b>1. Nilai insentif berbentuk wang tunai / <i>Value of monetary incentive</i> </b>
        <br>
        {!! AgentController::giveSpace(2) !!}(a) Komisen / bonus / <i>Commission / bonus</i>

        <table id="meta05">
            <tr>
                <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}RM {{number_format($data['comm_amount'],2)}}{!! AgentController::giveSpace(1) !!}</b></td>
            </tr>
            <tr>
                <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}</b></td>
            </tr>
        </table>
        <br>
        {!! AgentController::giveSpace(2) !!}(b) Lain-lain / <i>Others</i>
        <table id="metaOthers">
            <tr><td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}</b></td></tr>
            <tr><td class="td-specify fs-13">(Sila nyatakan / <i>Please specify</i>  )</td></tr>
        </table>
        <br><br>
        <div class="mtb-10">
            <span class="jumlah mr-25"><b> JUMLAH A / TOTAL A :</b></span>
            <table id="meta05">
                <tr>
                    <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}RM {{number_format($data['comm_amount'],2)}}{!! AgentController::giveSpace(1) !!}</b></td>
                </tr>
            </table>
        </div>

        <b>2. Nilai insentif berbentuk bukan wang tunai / <i>Value of non-monetary incentive</i></b>
        <br>
        {!! AgentController::giveSpace(2) !!}(a) Kenderaan / <i>Vehicle</i>
        <table id="meta05">
            <tr>
                <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}</b></td>
            </tr>
            <tr>
                <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}</b></td>
            </tr>
            <tr>
                <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}</b></td>
            </tr>
            <tr>
                <td class="meta-head"><b> {!! AgentController::giveSpace(1) !!} </b></td>
            </tr>
        </table>
        <br>
        {!! AgentController::giveSpace(2) !!}(b) Rumah / <i>House</i>
        <br>
        {!! AgentController::giveSpace(2) !!}(c) Pakej pelancongan / perjalanan / <i>Tour / travel package</i>
        <br>
        {!! AgentController::giveSpace(2) !!}(d) Lain-lain / <i>Others</i>
        <table id="metaOthers">
            <tr>
                <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}</b></td>
            </tr>
            <tr><td class="td-specify fs-13">(Sila nyatakan / <i>Please specify</i>  )</td></tr>
        </table>
        <br><br>
        <div class="mtb-10">
            <span class="jumlah mr-25"><b> JUMLAH B / TOTAL B :</b></span>
            <table id="meta05">
                <tr>
                    <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!} RM 0.00{!! AgentController::giveSpace(1) !!}</b></td>
                </tr>
            </table>
        </div>
        <div class="mtb-10">
            <span class="jumlah mr-25"><b> JUMLAH KESELURUHAN / TOTAL AMOUNT :</b></span>
            <table id="meta05">
                <tr>
                    <td class="meta-head"><b>{!! AgentController::giveSpace(1) !!}RM {{number_format($data['total_amount'],2)}}{!! AgentController::giveSpace(1) !!}</b></td>
                </tr>
            </table>
        </div>

        <div id="taxDeduction" class="mt-10">
            <table>
                <tr>
                    <td class="vtop"><b>3. </b></td>
                    <td class="ml-2 text-center"> <b>Jumlah potongan cukai 2% di bawah seksyen 107D ACP / <i>An amount of 2% tax deduction under <br> section 107D ITA </i><br>
                        2% X Jumlah A / <i>2% X Total A </i></b></td>
                    <td class="vbottom"><table id="meta05">
                        <tr>
                            <td class="meta-head border1"><b>{!! AgentController::giveSpace(1) !!}</b></td>
                        </tr>
                    </table></td>
                </tr>
            </table>

        </div>

        <br>

        <textarea id="header-part">BAHAGIAN D / PART D : AKUAN PEMBAYAR / PAYER'S DECLARATION</textarea>
        <br>
        Saya / <i>I</i>
        <table id="meta02">
            <tr>
                <td class="meta-head"><b>LEE MAY LAN</b><br>{!! AgentController::giveSpace(1) !!}</td>
            </tr>
        </table>
        <br><br><br>

        No. Pendaftaran / Kad Pengenalan / Polis / Tentera / Pasport * :

        <table id="meta06">
            <tr>
                {!! AgentController::metaHead('800307045082') !!}
            </tr>
        </table>
        <br>
        <i>Registration / Identity Card / Police / Army / Passport No. *</i>
        <br><br>
        Jawatan / <i>Designation</i>
        <table id="meta03">
            <tr>
                <td class="meta-head"><b>MANAGER</b></td>
            </tr>
        </table>

        <br><br>
        <span class="info-small">
        dengan ini mengakui bahawa maklumat yang diberikan dalam penyata ini adalah benar, betul dan lengkap seperti
        mana yang dikehendaki di bawahseksyen 83A Akta Cukai Pendapatan 1967. Saya juga sedia maklum bahawa kegagalan
        menyedia dan menyerahkan salinan penyata ini kepada ejen,pengedar atau pengagih berkenaan dalam tempoh yang
        ditetapkan oleh Akta tersebut adalah merupakan suatu kesalahan, dan jika disabitkan kesalahan, boleh dikenakan
        denda tidak kurang daripada dua ratus ringgit (RM200) dan tidak melebihi dua puluh ribu ringgit (RM20,000) atau
        hukuman penjara untuk satu tempoh tidak melebihi enam (6) bulan atau kedua-duanya sekali di bawah subseksyen
        120(1) Akta yang sama.
        <br><br>
        <i>hereby declare that the information given in this
            statement is true, correct and complete as required under section 83A of the Income Tax Act 1967.
            I am also aware that failure to prepare and render a copy of this statement to the relevant agent,
            dealer or distributor within the period stipulated by the Act is an offence and shall, on conviction,
            beliable to a fine of not less than two hundred ringgit (RM200) and not more than twenty thousand ringgit
            (RM20,000) or to imprisonment for a term not exceeding six (6) months or toboth under subsection 120(1) of
            the same Act.</i>
        </span>

        <br>
        <div id="rightFooter">Tarikh / <i>Date</i> : <b>{{$data['date_generated']}}</b></div>



        <div id="terms">
            <h5>
                <!--Terms-->
            </h5>
            ** Nota: Pengakuan ini hendaklah dibuat oleh pemegang jawatan dalam syarikat selaras dengan peruntukan Akta
            Cukai Pendapatan 1967
            <br><i>** Note: This declaration must be made by a designated officer of the company pursuant to the Income
                Tax Act 1967</i>
        </textarea>
    </div>

    </div>

</body>

</html>
