<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Users\Dealers\DealerInfo;
use App\Http\Controllers\Administrator\Archimedes\ArchimedesController;

class FixData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:data {function?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Test Email ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $function =  $this->argument('function');
            $is_function = false;

            while (!$is_function) {
                $function = str_replace('-', '_', $function);
                if (method_exists(__CLASS__, $function)) break;
                if ($function != '') $this->error("Function {$function} not found.");
                $function =  $this->choice(
                    'What to fix?',
                    ['dealer_package_add_dealer_data']
                );
            }

            $this->$function();
        } catch (\Throwable $th) {
            $this->error('Execution Error: ' . $th->getMessage());
        }
    }


    public function dealer_package_add_dealer_data()
    {
        $dealers = DealerInfo::where('account_status', 3)->get();

        foreach ($dealers as $dealerInfo) {
            if ($dealerInfo->dealerSales->isNotEmpty()) {
                foreach ($dealerInfo->dealerSales as $key => $dSales) {
                    $theOrder = $dSales->orders->where('purchase_id', $dSales->purchase_id)->first();
                    if ($theOrder != NULL && in_array($theOrder->purchase->purchase_status, [3001, 3002, 3003, 3013, 4002, 4007])) {
                        $pre_purchase = ArchimedesController::checkPrePurchaseProduct($theOrder);
                        // agent buy
                        if (!empty($pre_purchase['vqs'])) {
                            ArchimedesController::addDealerData($pre_purchase['vqs'], 5002);
                        }
                    }
                }
            }
        }

        $this->info("Success : updated all");
    }
}
