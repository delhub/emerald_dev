<?php

use App\Models\Globals\Status;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddRefundedGlobalStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $oldRefund = Status::where('id',4005)->first();
        $oldRefund->name = 'Refunded Cash';
        $oldRefund->status_type = 'payment_status';
        $oldRefund->save();

        DB::table('global_statuses')->insert([
            [
                'id'=>4008,
                'name'=>'Refunded Voucher (Full)',
                'description'=>'Status for purchase status.',
                'status_type'=>'payment_status',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'id'=>4009,
                'name'=>'Refunded Voucher (Partial)',
                'description'=>'Status for purchase status.',
                'status_type'=>'',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'id'=>1011,
                'name'=>'Order Refunded',
                'description'=>'Status for order status.',
                'status_type'=>'',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'id'=>1012,
                'name'=>'Order Partial Refunded',
                'description'=>'Status for order status.',
                'status_type'=>'',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'id'=>8015,
                'name'=>'Purchase Order - Full Refunded',
                'description'=>'Refund whole item in order.',
                'status_type'=>'inventory_status',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'id'=>8016,
                'name'=>'Purchase Order - Partial Refunded',
                'description'=>'Refund partial item in order.',
                'status_type'=>'inventory_status',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
        ]);

        Schema::table('purchases', function (Blueprint $table) {
			$table->string('purchases_notes')->after('voucher_data')->nullable();
			$table->integer('invoice_version')->after('purchase_number')->default(0);
        });

        Schema::table('orders', function (Blueprint $table) {
			$table->integer('po_do_version')->after('delivery_order')->default(0);
        });

        Schema::table('items', function (Blueprint $table) {
			$table->integer('po_do_version')->after('delivery_order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $newRefund = Status::where('id',4005)->first();
        $newRefund->name = 'Refunded';
        $newRefund->status_type = 'payment_status';
        $newRefund->save();

        Status::where('id',4008)->delete();
        Status::where('id',4009)->delete();
        Status::where('id',1011)->delete();
        Status::where('id',8015)->delete();
        Status::where('id',8016)->delete();

        Schema::table('purchases', function (Blueprint $table) {
			$table->dropColumn('purchases_notes');
			$table->dropColumn('invoice_version');
        });

        Schema::table('orders', function (Blueprint $table) {
			$table->dropColumn('po_do_version');
        });

        Schema::table('items', function (Blueprint $table) {
			$table->dropColumn('po_do_version');
        });
    }
}
