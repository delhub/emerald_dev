<?php

namespace App\Models\Globals;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    // Set table
    protected $table = 'global_settings';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'id',
        'setting_name',
        'setting_title',
        'setting_value',
        'setting_type'
    ];

    public static function getData($name, $type, $default = null)
    {
        $item = Settings::where('name', $name)->where('type', $type)->first();

        return $item->value ?? $default ?? null;
    }
}
