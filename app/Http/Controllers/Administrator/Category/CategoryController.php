<?php

namespace App\Http\Controllers\Administrator\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categories\Category;
use App\Models\Categories\CategoryData;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        $category_data = CategoryData::get();

        return view('administrator.categories.index')
            ->with('categories', $categories)
            ->with('category_data', $category_data);
    }

    public function create()
    {
        return view('administrator.categories.create');
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'category_name' => 'required',
            'parent_category_id' => 'required',
            'category_icon' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($validator->fails()) {
            $data['status'] = 'Bad Request';
            $data['message'] = 'Validation failed. Please make sure all inputs are filled in.';
            $data['item'] = $validator->errors();

            return response()->json($data, 400);
        }

        $category = new Category;
        $category->name = $request->input('category_name');
        $category->slug = Str::slug($request->input('category_name'), '-');
        $category->parent_category_id = $request->input('parent_category_id');
        if ($request->input('featured_category')) {
            $category->featured = 1;
        } else {
            $category->featured = 0;
        }
        $category->save();

        if ($request->file('category_icon')) {
            $image = $request->file('category_icon');
            $imageDestination = public_path('/images/icons/category/');
            $imageName = $category->slug;
            $imageName = $category->id . '-' . $imageName . '.' . $image->getClientOriginalExtension();
            $image->move($imageDestination, $imageName);

            $categoryImage = $category->image;

            $category->image()->create([
                'path' => 'images/icons/category/',
                'filename' => $imageName,
                'default' => 0
            ]);
        }

        if ($category->save()) {
            Cache::flush();
        }

        $data['status'] = 'OK';
        $data['message'] = 'Category successfully created.';
        $data['item'] = $category;

        return response()->json($data, 200);
    }

    /**
     * Assume that the request is made using Ajax.
     * Return a view that will be loaded into a modal using JQuery.
     */
    public function edit($id)
    {
        try {
            $category = Category::findOrFail($id);
            $categories = Category::where('id', '!=', $id)->get();
            $roles = Role::orderBy('id')->pluck('name', 'name')->all();

            $excludeCountry['my'] = 'my';
            $excludeCountry['sg'] = 'sg';

            $categoryData = Category::select('categories_data.*')
                ->where('categories_data.category_id', '=', $id)
                ->join('categories_data', 'categories_data.category_id', '=', 'categories.id')
                ->where('categories_data.type', 'search')
                ->get();

            $dataArray = array();
            foreach ($categoryData as $key => $data) {
                $dataArray[$data->country] = $data->country;
            };

            return view('administrator.categories.partials.edit-category')
                ->with('roles', $roles)
                ->with('category', $category)
                ->with('categories', $categories)
                ->with('excludeCountry', $excludeCountry)
                ->with('dataArray', $dataArray);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        // dd($request->input(), $id);
        try {
            $category = Category::findOrFail($id);
            $category->name = $request->input('category_name');
            $category->position = $request->input('category_position');
            $category->public_view = $request->input('public_view', 1);
            $category->slug = Str::slug($request->input('category_name'), '-');
            $category->parent_category_id = $request->input('parent_category_id');

            $validated = $request->validate([
                'rolesCategory' => 'array',
                'rolesCategory.*' => 'string',
            ]);
            $category->roles = $validated['rolesCategory'] ?? null;

            if ($request->input('featured_category')) {
                $category->featured = $request->input('featured_category');
            } else {
                $category->featured = 0;
            }
            if ($request->input('featured_mobile')) {
                $category->featured_mobile = $request->input('featured_mobile');
            } else {
                $category->featured_mobile = 0;
            }

            if ($request->file('category_icon')) {
                $image = $request->file('category_icon');
                $imageDestination = public_path('/images/icons/category/');
                $imageName = $category->slug;
                $imageName = $category->id . '-' . $imageName . '.' . $image->getClientOriginalExtension();
                $image->move($imageDestination, $imageName);

                $categoryImage = $category->image;

                if ($categoryImage) {
                    $categoryImage->path = 'images/icons/category/';
                    $categoryImage->filename = $imageName;
                    $categoryImage->save();
                } else {
                    $category->image()->create([
                        'path' => 'images/icons/category/',
                        'filename' => $imageName,
                        'default' => 0
                    ]);
                }
            }
            $category->save();

            if ($category->save()) {
                Cache::flush();
            }



            $data['status'] = 'OK';
            $data['message'] = 'Category successfully updated.';
            $data['item'] = $category;

            $oldData = categoryData::where('category_id', $category->id)->delete();
            if ($request->input('excludeCountryCategory') != NULL) {
                foreach ($request->input('excludeCountryCategory') as $key => $country) {

                    $categoryData = new categoryData;

                    $categoryData->category_id = $category->id;
                    $categoryData->type = $request->input('dataType');
                    $categoryData->value = $request->input('dataValue');
                    $categoryData->country = $country;

                    $categoryData->save();
                }
            }

            return response()->json($data);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    //
    public function destroy(Request $request, $id)
    {
        try {
            $category = Category::findOrFail($id);

            $image = $category->image;
            $image->delete();

            $category->delete();

            $data['status'] = 'OK';
            $data['message'] = 'Category deleted successfully.';
            $data['item'] = $category;

            return response()->json($data, 200);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Model not found.'], 404);
        }
    }

    public function updateCategory(Request $request)
    {
        $category = Category::findOrFail($request->category_id);
        $newStatus = $request->status;

        if ($request->category_type == 'status') {
            $category->active = $newStatus;
            $category->save();

            $statusText = $newStatus == 1 ? 'Enabled' : 'Disabled';
            $color = $newStatus == 1 ? 'green' : 'red';
            $message = '<strong style="color: black;">' . $category->name . '</strong> - <span style="color: ' . $color . ';">' . $statusText . '</span>!';

            return response()->json(['status' => 'success', 'message' => $message]);
        }

        return response()->json(['status' => 'error', 'message' => 'Invalid request'], 400);
    }
}
