<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePanelProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panel_product_attributes', function (Blueprint $table) {
            $table->bigInteger('shipping_category')->nullable(false)->after('size_icon');
        });

        Schema::table('panel_products', function (Blueprint $table) {
            $table->dropColumn('shipping_category');
            $table->dropColumn('installation_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
