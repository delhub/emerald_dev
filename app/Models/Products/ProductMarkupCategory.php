<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductMarkupCategory extends Model
{ 
    // Set table
    protected $table = 'panel_product_markup_category';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function products()
    {
        return $this->belongsToMany(
            'App\Models\Products\Product',
            'piv_product_markup',
            'category_id',
            'product_id'
        );
    }
}
