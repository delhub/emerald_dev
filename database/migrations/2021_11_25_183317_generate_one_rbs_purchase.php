<?php

use App\Models\Purchases\Item;
use App\Models\Purchases\Order;
use App\Models\Purchases\Purchase;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenerateOneRbsPurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $purchases = Purchase::where('purchase_number','FWS21-003925')->first();

        if ($purchases) {
            # code...
        }

        $oldOrder = Order::where('purchase_id',$purchases->id)->first();
        $oldOrder->order_type = 'rbs';
        $oldOrder->save();
        $items = Item::where('delivery_order',$oldOrder->delivery_order)->where('product_code','LIKE','RBS%')->get();
        foreach ($items as $key => $item) {
            $item->order_type = 'rbs';
            $item->save();
        }

        $itemData = array();

        foreach ($items as $key => $item) {
            // $newOrder = new stdClass();

            for ($i=2; $i <= ($item->product_information['rbs_total_months']); $i++) {

                $itemData['orders'][$oldOrder->order_number.'-'.$i]['id'] = 'new';
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['order_number'] = $oldOrder->order_number.'-'.$i;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['delivery_order'] = $oldOrder->delivery_order.'-'.$i;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['order_status'] = 1001;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['delivery_info'] = null;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['delivery_date'] = 'Pending';
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['courier_name'] = null;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['tracking_number'] = null;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['shipment_key'] = null;
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['order_date'] = Carbon::parse($oldOrder->est_shipping_date)->addMonth($oldOrder->reminder_interval_months+($i-1))->format('d/m/Y');
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['est_shipping_date'] = 'Pending';
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['shipping_date'] = 'Pending';
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['collected_date'] = 'Pending';
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['created_at'] = Carbon::now();
                $itemData['orders'][$oldOrder->order_number.'-'.$i]['updated_at'] = Carbon::now();

                $itemData['items'][$key][$i]['id'] = 'new';
                $itemData['items'][$key][$i]['old_product_code'] = $item->product_code;
                $itemData['items'][$key][$i]['bundle_id'] = $item->id;
                $itemData['items'][$key][$i]['order_number'] = $item->order_number.'-'.$i;
                $itemData['items'][$key][$i]['delivery_order'] = $item->delivery_order.'-'.$i;
                $itemData['items'][$key][$i]['item_order_status'] = 1001;
                $itemData['items'][$key][$i]['ship_date'] = 'Pending';
                $itemData['items'][$key][$i]['actual_ship_date'] = 'Pending';
                $itemData['items'][$key][$i]['collected_date'] = 'Pending';
                $itemData['items'][$key][$i]['created_at'] = Carbon::now();
                $itemData['items'][$key][$i]['updated_at'] = Carbon::now();
            }
        }

        foreach ($itemData['orders'] as $datakey => $data) {

            $copyOrder = $oldOrder->replicate()->fill(
                [
                    'id' => $data['id'],
                    'order_number' => $data['order_number'],
                    'delivery_order' => $data['delivery_order'],
                    'order_status' => $data['order_status'],
                    'delivery_info' => $data['delivery_info'],
                    'delivery_date' => $data['delivery_date'],
                    'courier_name' => $data['courier_name'],
                    'tracking_number' => $data['tracking_number'],
                    'shipment_key' => $data['shipment_key'],
                    'order_date' => $data['order_date'],
                    'est_shipping_date' => $data['est_shipping_date'],
                    'shipping_date' => $data['shipping_date'],
                    'collected_date' => $data['collected_date'],
                    'created_at' => $data['created_at'],
                    'updated_at' => $data['updated_at'],
                ]
            );
            $copyOrder->save();
        }

        foreach ($itemData['items'] as $itemskey => $newItems) {
            foreach ($newItems as $newItemskey => $newItem) {
                $itemForDuplicate = Item::where('product_code',$newItem['old_product_code'])->first();
                $copyItems = $itemForDuplicate->replicate()->fill(
                    [
                        'id' => $newItem['id'],
                        'order_number' => $newItem['order_number'],
                        'delivery_order' => $newItem['delivery_order'],
                        'bundle_id' => $newItem['bundle_id'],
                        'item_order_status' => $newItem['item_order_status'],
                        'ship_date' => $newItem['ship_date'],
                        'actual_ship_date' => $newItem['actual_ship_date'],
                        'collected_date' => $newItem['collected_date'],
                        'created_at' => $newItem['created_at'],
                        'updated_at' => $newItem['updated_at'],

                    ]
                );
                $copyItems->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
