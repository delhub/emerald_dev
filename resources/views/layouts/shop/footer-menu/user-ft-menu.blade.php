<div class="mobile-user-box">
      @if (!Auth::check())
      <a class="login-button" href="/">
            <img src="{{ asset('/images/icons/logout-icon.png') }}" alt="">
            Login/Register
      </a>
      @else
      <a class="profile-menu" href="{{ route('shop.dashboard.customer.profile') }}">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i> --}}
            <img src="{{asset('/images/icons/prodile-icon.png')}}" alt="">
            Profile
      </a>

      <a class="myDashboard-menu" href="{{ route('shop.dashboard.customer.home') }}">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i> --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            My Dashboard
      </a>

      @dealershipcheck
      <a class="agentDashboard-menu" href="{{ route('agent.overview') }}">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/agentdo-icon.png')}}" alt="">
            Agent Dashboard
      </a>

      <a href="{{ route('sg.launch.index') }}">
            <img src="{{asset('/images/icons/ag-package-icon.png')}}" alt=""> Agent Packages
      </a>
      @enddealershipcheck

      {{-- @hasrole('customer')
      <a class="agentDashboard-menu" href="{{ route('shop.category.first',['topLevelCategorySlug'=>'double-ong-packages']) }}">
            <img src="{{asset('/images/icons/ag-package-icon.png')}}" alt="">
            Double ONG Packages
      </a>
      @endhasrole --}}

      @dealershipcheck
      <a href="{{ route('shop.category.first',['topLevelCategorySlug'=>'agent-package-product']) }}">
            <img src="{{asset('/images/icons/agent-purchase.png')}}" alt=""> Agent Package Product
      </a>
      @enddealershipcheck

      @hasrole('panel')
      <a class="panelDashboard-menu" href="{{ route('management.panel.home') }}">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/paneldo-icon.png')}}" alt="">
            Panel Dashboard
      </a>
      @endhasrole

      @hasrole('formula_pic')
        <a class="panelDashboard-menu" href="/administrator">
        {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
        <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
        PIC
        </a>
    @endhasrole

      @hasrole('administrator')
      <a href="/administrator" class="adminDashboard-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Administrator</a>
      @endhasrole

      @hasrole('export_manager')
      <a href="{{ route('csv.main.list') }}" class="adminDashboard-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/paneldo-icon.png')}}" alt="">
            Export</a>
      @endhasrole


      @hasrole('admin_product')
      <a href="/administrator" class="adminDashboard-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Administrator</a>
      @endhasrole

      @hasrole('management')
      <a href="{{route('management.sale')}}" class="saleSummary-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Sale Dashboard
      </a>

      <a href="{{route('administrator.products.reports')}}" class="saleSummary-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Brands / Product Report
      </a>

        <a href="{{route('administrator.warehouse.inventory.overall-management')}}" class="saleSummary-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            FML Inventory Summary
        </a>

        <a href="{{route('administrator.warehouse.inventory.overall-management-v2')}}" class="saleSummary-menu">
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            FML Actual Stock
        </a>

        <a href="{{route('administrator.ordertracking.logisticReport')}}" class="saleSummary-menu"><img
            src="{{asset('/images/icons/mydo-icon.png')}}" alt=""
           > Logistic Report</a>
      @endhasrole

      @hasrole('agent_manager')
      <a href="{{route('management.manager.groupsale')}}" class="saleSummary-menu">
            {{-- <i class="fa fa-file-text-o" aria-hidden="true"></i> --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Sales Dashboard
      </a>
      @endhasrole
      @hasrole('redemption')
      <a href="{{ route('administrator.redemption') }}" class="redemptionDashboard-menu">
            {{-- <i class="fa fa-folder-o" aria-hidden="true"></i>  --}}
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Redemption
      </a>
      @endhasrole
      @hasrole('voucher_summary')
      {{-- <a href="{{ route('administrator.discount.summary') }}" class="redemptionDashboard-menu">
            <i class="fa fa-folder-o" aria-hidden="true"></i>
            <img src="{{asset('/images/icons/mydo-icon.png')}}" alt="">
            Voucher Summary
      </a> --}}
      @endhasrole

      <a class="logout-menu" href="{{ route('logout') }}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
            {{-- <i class="fa fa-sign-out" aria-hidden="true"></i> --}}
            <img src="{{asset('/images/icons/logout-icon.png')}}" alt="">
            {{ __('Logout') }}
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
      </form>
      @endif
</div>
