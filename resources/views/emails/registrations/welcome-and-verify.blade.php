@component('mail::message')

Dear {{ $user->userInfo->full_name }},

Your account ID is: {{$user->userInfo->account_id}}.

Thank you for registering with us!

Verify your account so that you may start shopping at <a href="https://www.formula2u.com/">formula2u.com</a>

@component('mail::button', ['url' => $url])
Verify Email
@endcomponent


Regards,<br>
{{ config('app.name') }}

@endcomponent
