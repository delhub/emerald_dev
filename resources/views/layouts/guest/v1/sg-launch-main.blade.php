<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!--Validate register fields -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?v=2.4" rel="stylesheet">
    @stack('style')

    <style>
        /* @media (max-width: 576px) {
            .bg-sm {
                background-image: url(/images/Bujishu_Landing_Page_Desktop_{{$country}}.jpg);
                background-size: cover;
            }
        }

        @media (min-width: 768px) {
            .bg-md {

                background-image: url(/images/Bujishu_Landing_Page_Desktop_{{ $country }}.jpg);
                background-size: cover;

            }
        } */

    </style>
</head>

<body>
    {{-- <div class="middleBar pt-1 pb-1">
        <div class="container-90">
            <div class="row d-flex">
                <div class="col-12 col-md-4 offset-md-8 vertical-align my-2">
                    <ul class="nav float-right font-size-125">
                        <li class="nav-item m-1">
                            <a class="btn  grad2 bjsh-btn-gradient btn-small-screen font-weight-bold" style="border-radius: 42px;
                            border: 3px solid #ababab;
                            height: 49px;
                            line-height: 31px;
                            width: 50px;" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" style="border-radius: 0 0 30px 30px;">
                                <img src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout"
                                    style="height:23px; padding:0 10px 0 0px !important;">
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="container-left" style="padding: 10px 10px;">
        {{-- <img class="navigation-logo" style="margin-right: 10px;" src="{{ asset("storage/logo/bujishu-logo-$country.png") }}" alt="Bujishu"> --}}
        <div class="ri-btn">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="btn gbtn r-color-btn "><img
                    src="{{ asset('assets/images/icons/logout_b.png') }}" alt="Logout"
                    style="height:23px; padding:0 10px 0 0px !important;"></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>


    {{-- <main class="h-100">

        @yield('content')
    </main>
    @stack('script')
</body>

</html> --}}
