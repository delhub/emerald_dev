<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Models\Users\UserInfo;
use Carbon\Carbon;
use App\Http\Controllers\Register\CustomerRegisterController;
use App\Models\Users\UserContact;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Hash;
use stdClass;

use function GuzzleHttp\json_decode;

class ApiUsersController extends Controller
{
    public function all()
    {
        $user = User::get();
        $data = array();
        if (count($user)) {
            foreach ($user as $k => $v) {
                $v->userInfo;
                $v->dealer_id = isset($v->dealerInfo) ? $v->dealerInfo->account_id : null;
                isset($v->userInfo->mobileContact) ? $v->userInfo->mobileContact : '';
                $data[] = $v;

                unset($v->dealerInfo);
            }

            return $data;
        }
    }

    public function updated(Request $request)
    {
        $day = isset($request->day) ? $request->day : 7;
        $user = User::whereDate('created_at', '>', Carbon::now()->subDays($day))->get();
        $data = array();
        if (count($user)) {
            foreach ($user as $k => $v) {
                $v->userInfo;
                $v->dealerInfo;
                isset($v->userInfo->mobileContact) ? $v->userInfo->mobileContact : '';
                $data[] = $v;
            }

            return $data;
        }
    }

    public function updateUserLevel(Request $request)
    {
        $userInfo = UserInfo::where('account_id', $request->customer_id)->first();

        if ($userInfo) {
            $userInfo->user_level = $request->member_status;
            if ($userInfo->save()) {
                if ($user = User::find($userInfo->user->id)) {
                    CustomerRegisterController::createMembershipLogs($user);
                }
            }
        }
    }

    public function register(Request $request)
    {
        try {
            $requestdata = $request->getContent();
            $requestdata = explode("::", $requestdata);
            $requestdata = base64_decode($requestdata[1]);
            $requestdata = json_decode($requestdata, true);

            \Log::info($requestdata['password']);
            \Log::info(env('APP_KEY'));
            $requestdata['password'] = decrypt($requestdata['password']);
            $userinfos = UserInfo::where('nric', $requestdata['nric'])->first();
            $users = User::where('email', $requestdata['email'])->first();
            if ($userinfos || $users) {
                $error = 'User has been registered';
                $response = [
                    'error' => $error,
                ];
                return response()->json($response, 500);
            }

            $requestdata = array_merge($requestdata, [
                'city' => null,
                'date_of_birth' => null,
                'passport_no' => null,
                'country' => null,
                'invoice_number' => 'walk-in',
                'year_joined' => null,
                'product_used' => null,
                'address_1' => null,
                'address_2' => null,
                'address_3' => null,
                'postcode' => null,
                'city_key' => 0,
                'contact_number_home' => null
            ]);
            $request->merge($requestdata);
            $user = CustomerRegisterController::createCustomer($request, false, country()->country_id);
            $user->notes = null;
            $user->save();
            $userInfo = UserInfo::where('user_id', $user->id)->first();
            $response = [
                'account_id' => $userInfo->account_id,
                'full_name' => $userInfo->full_name,
                'email' => $user->email,
                'phone' => $userInfo->mobileContact->contact_num,
                'nric' => $userInfo->nric,
                'member_status' => $userInfo->user_level,
                'upline_id' => $userInfo->referrer_id
            ];
            return response()->json($response, 200);
        } catch (\Exception $error) {
            $response = [
                'error' => $error->getMessage(),
            ];
            return response()->json($response, 404);
        }
    }

    public function checkUser(Request $request)
    {
        try {
            $enc = new Encrypter(config('app.crypter_key'), config('app.cipher'));

            $token = null;
            if ($request->token) $token = $enc->decryptString(base64_decode($request->token));
            if (!$request->token or ($token !== config('app.crypter_key'))) return response()->json(['error' => 'Unauthorized'], 404);

            $user = User::where('email', $request->input('email'))->first();

            if (!$user) return response()->json(['error' => ['email' => 'Email not found']], 404);

            return response()->json(true, 200);
        } catch (\Exception $error) {
            $response = ['error' => $error->getMessage()];
            return response()->json($response, 404);
        }
    }

    public function getUser(Request $request)
    {
        try {
            $enc = new Encrypter(config('app.crypter_key'), config('app.cipher'));

            $token = null;
            if ($request->token) $token = $enc->decryptString(base64_decode($request->token));
            if (!$request->token or ($token !== config('app.crypter_key'))) return response()->json(['error' => 'Unauthorized'], 404);
            if ($request->password) $request->password = $enc->decryptString(base64_decode($request->password));

            $user = User::where('email', $request->input('email'))->first();
            if (!$user) return response()->json(['error' => ['email' => 'Email not found']], 404);
            if (!Hash::check($request->password, $user->password)) return response()->json(['error' => ['password' => 'Password not match']], 404);

            $user->userInfo;
            $user->userInfo->mailingAddress;
            $user->userInfo->mobileContact;
            $user->userInfo->homeContact;
            $response = ['user' => $user];
            return response()->json($response, 200);
        } catch (\Exception $error) {
            $response = ['error' => $error->getMessage()];
            return response()->json($response, 404);
        }
    }

    public function getAgent(Request $request)
    {
        try {
            $enc = new Encrypter(config('app.crypter_key'), config('app.cipher'));

            $token = null;
            if ($request->token) $token = $enc->decryptString(base64_decode($request->token));
            if (!$request->token or ($token !== config('app.crypter_key'))) return response()->json(['error' => 'Unauthorized'], 404);
            if ($request->password) $request->password = $enc->decryptString(base64_decode($request->password));

            $agent = User::where('email', $request->input('email'))->first();
            if (!$agent or !$agent->hasRole('dealer') or !$agent->dealerInfo) return response()->json(['error' => ['email' => 'You are not an agent on Formula platform']], 404);
            if (!Hash::check($request->password, $agent->password)) return response()->json(['error' => ['password' => 'Password not match']], 404);

            $user = new stdClass;
            $user->dealerInfo = new stdClass;
            $user->userInfo = new stdClass;
            $user->userInfo->homeContact = new stdClass;

            $user->userInfo = (object) $agent->userInfo->only('full_name');
            $user->dealerInfo = (object) $agent->dealerInfo->only('nric', 'date_of_birth', 'gender_id', 'race_id', 'marital_id', 'group_id');
            $user->userInfo->shippingAddress = (object) $agent->dealerInfo->billingAddress->makeHidden(['id', 'account_id', 'created_at', 'updated_at']);
            $user->userInfo->mobileContact = (object) $agent->dealerInfo->dealerMobileContact->only('contact_num');
            $user->userInfo->homeContact = (object) $agent->dealerInfo->dealerHomeContact->only('contact_num');
            $user->dealerInfo->employmentAddress = (object) $agent->dealerInfo->employmentAddress->makeHidden(['id', 'account_id', 'created_at', 'updated_at']);
            $user->dealerInfo->dealerSpouse = (object) $agent->dealerInfo->dealerSpouse->makeHidden(['id', 'account_id', 'created_at', 'updated_at']);
            $user->dealerInfo->dealerBankDetails = (object) $agent->dealerInfo->dealerBankDetails->makeHidden('account_id');

            $response = ['user' => $user];
            return response()->json($response, 200);
        } catch (\Exception $error) {
            $response = ['error' => $error->getMessage()];
            return response()->json($response, 404);
        }
    }
}
