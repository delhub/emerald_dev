@php
$shipCookie = json_decode(Cookie::get('addressCookie'));
use App\Models\Globals\City;

$City = '';

if(!isset($shipCookie->city) && empty($shipCookie->city)) $shipCookie->city = '';

if (isset($shipCookie) && $shipCookie->city_key != '0') {
    $City = $shipCookie->city_name->city_name;
}else {
    $City = $shipCookie->city;
}
@endphp
<input type="hidden" name="purchase_name"
    value="{{ isset($shipCookie->name) ? $shipCookie->name : $user->userInfo->full_name}}">
<input type="hidden" name="purchase_mobile"
    value="{{ isset($shipCookie->mobile) ? $shipCookie->mobile : $user->userInfo->mobileContact->contact_num}}">
<input type="hidden" name="purchase_address_1"
    value="{{ isset($shipCookie->address_1) ? $shipCookie->address_1 : $user->userInfo->shippingAddress->address_1 }}">
<input type="hidden" name="purchase_address_2"
    value="{{ isset($shipCookie->address_1) ? $shipCookie->address_2 : $user->userInfo->shippingAddress->address_2 }}">
    @if ( country()->country_id == 'MY')
    <input type="hidden" name="purchase_address_3"
    value="{{ isset($shipCookie->address_1) ? $shipCookie->address_3 : $user->userInfo->shippingAddress->address_3 }}">
    @endif
<input type="hidden" name="purchase_postcode"
    value="{{ isset($shipCookie->address_1) ? $shipCookie->postcode : $user->userInfo->shippingAddress->postcode }}">
<input type="hidden" name="purchase_city"
    value="{{$City}}">
<input type="hidden" name="purchase_state_id"
    value="{{ isset($shipCookie->address_1) ? $shipCookie->state_id : $user->userInfo->shippingAddress->state_id }}">

{{-- { isset($shipCookie->address_1) ?

    ($shipCookie->city_key != '0' ?
    (isset($shipCookie->city_name) ? $shipCookie->city_name->city_name : 'OTH') : $shipCookie->city_name) : ($user->userInfo->shippingAddress->city ?? ($thisCity = City::where('city_key',$user->userInfo->shippingAddress->city_key)->first() == null  ? $thisCity->city_name : 'OTH')) }} --}}
