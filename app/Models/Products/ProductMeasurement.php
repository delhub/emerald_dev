<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductMeasurement extends Model
{
    // Set table
    protected $table = 'panel_product_measurement';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [
        'product_code',
        'length',
        'width',
        'depth',
        'weight',
        'cubic_weight'
    ];

}
