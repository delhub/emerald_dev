<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bujishu - Invoice {{ $purchase->purchase_number }}</title>
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
  <style>
    p {
      font-size: 12px;
      font-family: 'Lato', sans-serif;
    }
    #invoice-box {
      background-color: #fef5d6;
      border-radius: 20px;
      padding: 20px 0px 20px 20px;
      font-size: 13px;
      width: 100%;
      height: 100%;
    }

    #payment-box {
      background-color: #fef5d6;
      border-radius: 20px;
      padding: 20px 120px 20px 20px;
      margin-right: 20px;
      font-size: 13px;
      width: 100%;
      height: 100%;
    }

    #head-table {
      background-color: #fef5d6;
      line-height: 30px;
      border-bottom: gold 1px solid;
    }

    #col-amount {
      background-color: #fef5d6;
      text-align: right;
      padding-right: 20px;
      width: auto;

    }

    #col-unit,
    #col-qty {
      text-align: center;
    }

    #col-no {
      width: 5%;
    }

    #last-footer {}

    #main-tab {
      width: 100%;
      position: absolute;
      bottom: 0;
      left: 0;
    }

    #payment-tab {
      width: 100%;
    }

    thead {
      display: table-header-group;
    }

    tfoot {
      display: table-row-group;
    }

    tr {
      page-break-inside: avoid;
    }
    .vertical {
            border-left: 1px solid gold;
            height: 100px;
            position: fixed;
            left: 220px;
            top: 10px;
        }
  </style>
</head>

<body style="margin: 0;">
  <hr style="height:10px;border:none;background-color:#ffcc00; padding:0;margin:0;" />

  <table cellspacing="0" width="100%" height="1410px" style="padding: 0px 50px 0px 50px;">
    <thead>
      <tr style="padding:10px;">
        <th colspan="2" id="logo" style="padding:10px 10px 10px 0;"><img align="left" width="60%" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-logo.jpg') }}"><div class = "vertical"></div></th>
        <th colspan="5" style="text-align:left;">
          <p style="margin: 0px 0px 0px -33px;font-size:17px;line-height: 0px;padding-top: 20px;"><br>DC SIGNATURE LIVING STYLE SDN BHD</p>
          <p style="margin: 15px 0px 0px -33px;line-height:1px;color:#696969;font-weight: 100;"><sup>Company No:202001002917(1859236-K)</sup></p>
          <p style="margin: 0px 0px 0px -33px;font-size:12px;color:#696969;font-weight: 100;">1-26-15 Menara Bangkok Bank, Berjaya Central Park,
            <br>No 105, Jalan Ampang, 50450 Kuala Lumpur, Malaysia.</p>
          <p style="margin: 0px 0px 0px -33px;font-size:12px;color:#696969;font-weight: 100;"><img style="height:12px;vertical-align:text-bottom;" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-tel.jpg') }}"> 603-2181 8821&nbsp;&nbsp;&nbsp;&nbsp;<img
               style="height:12px;vertical-align:text-bottom;" src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-mail.jpg') }}">
            bujishu-cs@delhubdigital.com&nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ asset('assets/images/miscellaneous/letterhead/letterhead-web.jpg') }}" style="height:12px;vertical-align:text-bottom;" > www.bujishu.com</p>
          </p>
        </th>
      </tr>
      <th colspan="4">
      </th>
      <th colspan="3">
        <p style="color:gold;font-size:35px; text-align:left;padding-top: 20px;">&nbsp;&nbsp;INVOICE</p>
      </th>
      </tr>
      <tr>
        <td colspan="2" width="30%">
          <p style="font-size:16px;color:#696969;">RECEIVER</p>
          <hr style="border-top: 1px solid gold;">
          <p>{{ $purchase->user->userInfo->shippingAddress->address_1 }}<br>{{ $purchase->user->userInfo->shippingAddress->address_2 }}<br>{{ $purchase->user->userInfo->shippingAddress->postcode }},
            {{ $purchase->user->userInfo->shippingAddress->city }}<br>@if ($purchase->ship_state_id)
            {{$purchase->state->name }},
            @else
            {{$purchase->user->userInfo->shippingAddress->state->name }}
            @endif
            Malaysia<br>Attn : <strong>{{ $purchase->user->userInfo->full_name }}</strong><br>Tel : <strong>{{ $purchase->user->userInfo->mobileContact->contact_num }}</strong></p>
          <hr style="border-top: 1px solid gold;">
        </td>

        <td colspan="2" width="30%" style="margin-right:20px;">
          <p style="font-size:16px;color:#696969;">DELIVER TO</p>
          <hr style="border-top: 1px solid gold;width: 80%; margin-left: 0;">
          <p>@if ($purchase->ship_address_1)
            {{ $purchase->ship_address_1 }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->address_1 }}
            @endif<br>@if ($purchase->ship_address_2)
            {{ $purchase->ship_address_2 }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->address_2 }}
            @endif<br>@if ($purchase->ship_postcode)
            {{ $purchase->ship_postcode }},
            @else
            {{ $purchase->user->userInfo->shippingAddress->postcode }}
            @endif
            @if ($purchase->ship_city)
            {{ $purchase->ship_city }}
            @else
            {{ $purchase->user->userInfo->shippingAddress->city }}
            @endif<br>@if ($purchase->ship_state_id)
            {{$purchase->state->name }},
            @else
            {{$purchase->user->userInfo->shippingAddress->state->name }}
            @endif
            Malaysia<br>Attn : <strong>@if ($purchase->ship_full_name)
              {{ $purchase->ship_full_name }}
              @else
              {{ $purchase->user->userInfo->full_name }}
              @endif</strong><br>Tel : <strong>@if ($purchase->ship_contact_num)
              {{ $purchase->ship_contact_num }}
              @else
              {{ $purchase->user->userInfo->mobileContact->contact_num }}
              @endif</strong></p>
          <hr style="border-top: 1px solid gold;width: 80%; margin-left: 0;">
        </td>
        <td colspan="3" width="30%">
          <table id="invoice-box">
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p>INVOICE NO &nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{ $purchase->getFormattedNumber() }}</p>
              </td>
            </tr>
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p>Date&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: {{ $purchase->getFormattedDate() }}</p>
              </td>

            </tr>
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p>Agent code&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: 10000000000</p>
              </td>
            </tr>
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p>Credit Terms&nbsp;&nbsp;</p>
              </td>
              <td>
                <p style="text-transform:capitalize;">: {{ $purchase->purchase_type }}</p>
              </td></span>
            </tr>
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p>Page&nbsp;&nbsp;</p>
              </td>
              <td>
                <p>: 2 / 2</p>
              </td>
            </tr>
          </table>
        </td>
        </th>
      </tr>
      <tr style="padding: 0px 10px 0px 10px;">
        <td height="20px"></td>
      </tr>
      <tr id="head-table">
        <td id="col-no" style="border-bottom:gold 1px solid; border-top:gold 1px solid;">
          <p style="font-weight:600;">&nbsp;&nbsp;&nbsp;&nbsp;No.</p>
        </td>
        <td style="border-bottom:gold 1px solid;border-top:gold 1px solid;">
          <p style="font-weight:600;">Product ID </p>
        </td>
        <td style="border-bottom:gold 1px solid;border-top:gold 1px solid;" colspan="2">
          <p style="font-weight:600;text-align:center;">Description </p>
        </td>
        <td style="border-bottom:gold 1px solid;border-top:gold 1px solid;" id="col-qty">
          <p style="font-weight:600;">Qty </p>
        </td>
        <td style="border-bottom:gold 1px solid;border-top:gold 1px solid;" id="col-unit">
          <p style="font-weight:600;">Unit Price (RM)</p>
        </td>
        <td style="border-top:gold 1px solid;height: 40px;" id="col-amount">
          <p style="font-weight:600;">Amount (RM)</p>
        </td>
      </tr>
    </thead>

    <tbody id="middle-body" style="display:table-row-group;">

      <?php
    $iterationNo = 0;
    ?>
      @foreach($purchase->orders as $order)
      @foreach($order->items as $item)
      <tr valign="top">
        <td id="col-no" style="border-bottom:gold 1px solid;">
          <?php
            $iterationNo = $iterationNo + 1;
            ?>
          <p><br>&nbsp;&nbsp;&nbsp;&nbsp;{{ $iterationNo }}</p>
        </td>
        <td style="width:10%;border-bottom:gold 1px solid;">
          <p><br>{{ $item->product->parentProduct->product_code }}</p>
        </td>
        <td style="width:10%;border-bottom:gold 1px solid;">
          <img width="80%" src="{{ asset('storage/' . $item->product->parentProduct->images[0]->path . '/' . $item->product->parentProduct->images[0]->filename) }}" alt="{{ $item->product->parentProduct->name }}"
            style="padding:30px 0px 30px 0px;display:block;">
        </td>
        <td style="border-bottom:gold 1px solid;width:20%; padding-right:10px;">
          <p><br><strong>{{ $item->product->parentProduct->name }}</strong>
            <br>
            @if($item->product_id != 175 || $item->product_id != 176)
            @if(array_key_exists('product_color_name', $item->product_information))
            Color: {{ $item->product_information['product_color_name'] }}
            @endif
            <br>
            @if(array_key_exists('product_size', $item->product_information))
            Size: {{ $item->product_information['product_size'] }}
            @endif
            <br>
            @if(array_key_exists('product_temperature', $item->product_information))
            Color Temperature: {{ $item->product_information['product_temperature'] }}
            @endif
          </p>
          @endif
        </td>
        <td id="col-qty" style="border-bottom:gold 1px solid;">
          <p><br>{{ $item->quantity }}</p>
        </td>
        <td id="col-unit" style="border-bottom:gold 1px solid;">
          <p><br>{{ number_format(($item->product->member_price / 100), 2) }}</p>
        </td>
        <td id="col-amount" style="border-bottom:gold 1px solid;border-top:gold 1px solid;">
          <p><br>
            {{ number_format(($item->subtotal_price / 100), 2) }}
          </p>
        </td>
      </tr>
      @endforeach
      @endforeach
    </tbody>
    <tfoot id="last-footer">
      <tr style="height: 40px;">
        <td colspan="3">
          <p style="color:grey;font-size:15px;padding: 10px 10px 0 30px;">PAYMENT RECEIVED<br></p>
        </td>
        <td></td>
        <td></td>
        <td style="border-bottom:gold 1px solid;">
          <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;">Sub Total&nbsp;</p>
        </td>
        <td style="border-bottom:gold 1px solid !important;" id="col-amount">
          <p><?php
          $subtotal = 0;
          foreach ($purchase->orders as $order) {
              foreach ($order->items as $item) {
                  $subtotal = $subtotal + $item->subtotal_price;
              }
          }
          ?>
             {{ number_format(($subtotal / 100), 2)}}
          </p>
        </td>
      </tr>
      <tr style="height: 40px;">
        <td rowspan="3" colspan="3">
          <table id="payment-box">
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p><strong>Payment Method</strong></p>
              </td>
              <td>
                <p><span style="text-transform:capitalize;">: {{ $purchase->purchase_type }}</span></p>
              </td>
            </tr>
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p><strong>Reference Number</strong></p>
              </td>
              <td>
                <p>: {{ $purchase->successfulPayment->auth_code }}</p>
              </td>

            </tr>
            <tr style="padding: 0px 10px 0px 10px;">
              <td>
                <p><strong>Amount Paid</strong></p>
              </td>
              <td>
                <p>: RM  {{ number_format(($purchase->successfulPayment->amount/100), 2)}}</p>
              </td>
            </tr>
          </table>
        </td>
        <td></td>
        <td></td>
        <td style="border-bottom:gold 1px solid;">
          <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;line-height:20px;">Delivery Fee</p>
        </td>
        <td style="border-bottom:gold 1px solid;" id="col-amount">
          <p><?php
          $deliveryFee = 0;
          foreach ($purchase->orders as $order) {
              foreach ($order->items as $item) {
                  $deliveryFee = $deliveryFee + $item->delivery_fee;
              }
          }
          ?>
             {{ number_format(($deliveryFee / 100), 2) }}</p>
        </td>
      </tr>
      <tr style="height: 40px;">
        <td></td>
        <td></td>
        <td style="border-bottom:gold 1px solid;">
          <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;line-height:20px;">Installation Fee</p>
        </td>
        <td style="border-bottom:gold 1px solid;" id="col-amount">
          <p><?php
          $installationFee = 0;
          foreach ($purchase->orders as $order) {
              foreach ($order->items as $item) {
                  $installationFee = $installationFee + $item->installation_fee;
              }
          }
          ?>
             {{ number_format(($installationFee / 100), 2) }}</p>
        </td>
      </tr>
      <tr style="height: 40px;">
        <td></td>
        <td></td>
        <td style="border-bottom:gold 1px solid;">
          <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;line-height:20px; color:#ffcc00">Grand Total</p>
        </td>
        <td style="border-bottom:gold 1px solid; background-color:#ffcc00;" id="col-amount">
          <?php
          $grandTotal = 0;
          foreach ($purchase->orders as $order) {
              foreach ($order->items as $item) {
                  $grandTotal = $grandTotal + $item->subtotal_price + $item->delivery_fee + $item->installation_fee;
              }
          }
          ?>
          <p> {{ number_format(($grandTotal / 100), 2) }}</p>
        </td>
      </tr>
      <tr style="height: 40px;">
        <td colspan="4">
          <p style="font-size:12px;text-align:left;vertical-align: top;">The issuance of this invoice shall be deemed as your acceptance of the terms and conditions of this disclaimer. The Bujishu Agent Agreement shall only become effective after full payment is received by the Company.</p>
        </td>
        <td>
          <td style="border-bottom:gold 1px solid;">
            <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;line-height:20px;">Amount Paid</p>
          </td>
          <td style="border-bottom:gold 1px solid;" id="col-amount">

            <p>{{ number_format(($purchase->successfulPayment->amount / 100), 2)}}</p>
          </td>
        </td>
      </tr>
      <tr style="height: 40px;">
        <td colspan="4" style="line-height:20px;">
          <P style="font-size:12px;text-align:left;vertical-align: top;line-height:20px;">
          </P><br>
        </td>
        <td>
          <td style="border-bottom:gold 1px solid;">
            <p style="font-size:12px; font-weight:600;text-align:right;padding-right: 10px;line-height:20px;">Balance Due</p>
          </td>
          <td style="border-bottom:gold 1px solid;line-height:20px;" id="col-amount">
<!-- balance due -->
<?php
$balancedue = 0;
// $paidamount = (($purchase->successfulPayment->amount/100), 2);
foreach ($purchase->orders as $order) {
    foreach ($order->items as $item) {
        $balancedue = $purchase->successfulPayment->amount - $grandTotal;
    }
}
?>
            <p> {{ number_format(($balancedue / 100), 2) }}</p>
          </td>
        </td>
      </tr>
      <tr style="padding: 0px 10px 0px 10px;">
        <td colspan="7">
          <P style="line-height:40px;">

          </P>
        </td>
      </tr>
      <tr style="padding: 0px 10px 0px 10px;">
      <tr style="padding: 0px 10px 0px 10px;">
        <td colspan="7">
          <center style="color:grey;font-size:12px;line-height:25px;">
            <p>This is computer generated invoice no signature is required.</p>
          </center>
        </td>
      </tr>

    </tfoot>
    </tr>
  </table>
    <hr style="height:40px;border:none;background-color:#ffcc00; padding:0;margin:0;" />
</body>

</html>
