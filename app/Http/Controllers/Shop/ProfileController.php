<?php

namespace App\Http\Controllers\Shop;

use Auth;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Models\Globals\State;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Models\Globals\Gender;
use App\Models\Globals\Race;
use App\Models\Globals\Marital;
use App\Models\Globals\Employment;
use App\Jobs\Emails\SendCustomerRegisterEmail;
use App\Models\Users\UserMembership;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use App\Models\Sidemenus\Sidemenu;
use App\Models\Globals\City;
use App\Models\Users\UserAddress;




class ProfileController extends Controller
{
    protected $cart = null;
    protected $categories = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // Check if user is authenticated or not.
            if (Auth::check()) {
                // If authenticated, then get their cart.
                $this->cart = Auth::user()->carts->where('status', 2001);
            }
            // Get all categories, with subcategories and its images.
            $categories = Category::topLevelCategory();

            // Share the above variable with all views in this controller.
            View::share('categories', $categories);
            View::share('cart', $this->cart);

            // Return the request.
            return $next($request);
        });
    }

    /***Handles shop->customer profile ***/

    public function index()
    {
        $customerInfo = User::find(Auth::user()->id);
        //shipping city
        $shippingCity = '';
        if (isset($customerInfo->userInfo->cartShippingAddress->city_key)) {
            if (!empty($customerInfo->userInfo->cartShippingAddress->city_key)) {
                $shippingCity = $customerInfo->userInfo->cartShippingAddress->cityKey->city_name;
            } else {

                $shippingCity = $customerInfo->userInfo->cartShippingAddress->city;
            }
        }


        //billing city
        $billingCity = '';
        if (isset($customerInfo->userInfo->mailingAddress->city_key)) {
            if (!empty($customerInfo->userInfo->mailingAddress->city_key)) {
                $billingCity = $customerInfo->userInfo->mailingAddress->cityKey->city_name;
            } else {

                $billingCity = $customerInfo->userInfo->mailingAddress->city;
            }
        }

        return view('shop.customer-dashboard.profile.index')->with('customerInfo', $customerInfo)->with('shippingCity', $shippingCity)->with('billingCity', $billingCity);
    }



    // Returns edit page for user profile

    public function edit()
    {
        $customerInfo = User::find(Auth::user()->id);
        
        $stateId = 0;
        $shipping_city_key = 0;
        $billing_stateId = 0;
        $billing_city_key = 0;

        //shipping
        if (isset($customerInfo->userInfo->cartShippingAddress->state_id)) $stateId = $customerInfo->userInfo->cartShippingAddress->state_id;
        if (isset($customerInfo->userInfo->cartShippingAddress->city_key)) $shipping_city_key = $customerInfo->userInfo->cartShippingAddress->city_key;

        //billing
        if (isset($customerInfo->userInfo->mailingAddress->state_id)) $billing_stateId = $customerInfo->userInfo->mailingAddress->state_id;
        if (isset($customerInfo->userInfo->mailingAddress->city_key)) $billing_city_key = $customerInfo->userInfo->mailingAddress->city_key;

        $states = State::where('country_id', country()->country_id)->get();

        //shipping
        if ($stateId == 0) {
            $cities = City::where('country_id', country()->country_id)->orderBy('city_name', 'asc')->get();
        } else {
            $cities = City::where('country_id', country()->country_id)->where('state_id', $stateId)->orderBy('city_name', 'asc')->get();
        }

        //billing
        if ($billing_stateId == 0) {
            $billcities = City::where('country_id', country()->country_id)->orderBy('city_name', 'asc')->get();
        } else {
            $billcities = City::where('country_id', country()->country_id)->where('state_id', $billing_stateId)->orderBy('city_name', 'asc')->get();
        }
        $emailChange = false;
        return view('shop.customer-dashboard.profile.edit')
            ->with('customerInfo', $customerInfo)
            ->with('states', $states)
            ->with('stateId', $stateId)
            ->with('cities', $cities)
            ->with('billcities', $billcities)
            ->with('billing_stateId', $billing_stateId)
            ->with('shipping_city_key', $shipping_city_key)
            ->with('billing_city_key', $billing_city_key)
            ->with('emailChange', $emailChange);
    }

    // Update user profile information
    public function updateProfile(Request $request, $id)
    {
        $this->validate($request, array(

            'billing_address_1' => 'required',
            'billing_postcode' => 'required|digits:5',
            // 'billing_city' => 'required',
            'shipping_address_1' => 'required',
            'shipping_postcode' => 'required|digits_between:5,6',
            //'shipping_city_key' => 'required',
            'mobile_phone' => 'required|digits_between:10,11',
            'email' => 'required|unique:users,email,'. auth()->user()->id .'|email'
        ));

        //Get user info
        $customerInfo = User::findOrFail($id);

        $name = $customerInfo->userInfo;
        $billTo = $customerInfo->userInfo->mailingAddress;
        $shipTo = $customerInfo->userInfo->cartShippingAddress;
        $contact = $customerInfo->userInfo->mobileContact;
        $birth_date = $customerInfo->userInfo;
        $email = null;
        /*$name->full_name = $request->input('full_name');*/
        /*$name->date_of_birth = $request->input('date_of_birth');
        /*$name->save();*/

        $birth_date->nric = $request->input('birth_date');
        $birth_date->save();

        if ($customerInfo->email !== $request->input('email')) {
            $email = 'true';
            $customerInfo->email = $request->input('email');
            $customerInfo->save();
        }

        if ($billTo != NULL) {
            $billTo->address_1 = $request->input('billing_address_1');
            $billTo->address_2 = $request->input('billing_address_2');
            $billTo->address_3 = $request->input('billing_address_3');
            $billTo->postcode = $request->input('billing_postcode');
            $billTo->city_key = $request->input('billing_city_key');
            if ($request->input('billing_city_key') == 0) $billTo->city = $request->input('billing_city');
            $billTo->country_id = country()->country_id;
            $billTo->state_id = $request->input('billing_state');
            $billTo->save();
        } else {
            $billTo = new UserAddress;
            $billTo->account_id = $name->account_id;
            $billTo->address_1 = $request->input('billing_address_1');
            $billTo->address_2 = $request->input('billing_address_2');
            $billTo->address_3 = $request->input('billing_address_3');
            $billTo->postcode = $request->input('billing_postcode');
            $billTo->city_key = $request->input('billing_city_key');
            if ($request->input('billing_city_key') == 0) $billTo->city = $request->input('billing_city');
            $billTo->country_id = country()->country_id;
            $billTo->state_id = $request->input('billing_state');
            $billTo->is_billing_address = 1;
            $billTo->save();
        }

        if ($shipTo != NULL) {
            $shipTo->address_1 = $request->input('shipping_address_1');
            $shipTo->address_2 = $request->input('shipping_address_2');
            $shipTo->address_3 = $request->input('shipping_address_3');
            $shipTo->postcode = $request->input('shipping_postcode');
            $shipTo->city_key = $request->input('shipping_city_key');
            if ($request->input('shipping_city_key') == 0) $shipTo->city = $request->input('shipping_city');
            $shipTo->country_id = country()->country_id;
            $shipTo->state_id = $request->input('shipping_state');
            $shipTo->save();
        } else {
            $shipTo = new UserAddress;
            $shipTo->account_id = $name->account_id;
            $shipTo->address_1 = $request->input('shipping_address_1');
            $shipTo->address_2 = $request->input('shipping_address_2');
            $shipTo->address_3 = $request->input('shipping_address_3');
            $shipTo->postcode = $request->input('shipping_postcode');
            $shipTo->city_key = $request->input('shipping_city_key');
            if ($request->input('shipping_city_key') == 0) $shipTo->city = $request->input('shipping_city');
            $shipTo->country_id = country()->country_id;
            $shipTo->state_id = $request->input('shipping_state');
            $shipTo->is_shipping_address = 1;
            $shipTo->save();
        }
        $contact->contact_num = $request->input('mobile_phone');
        $contact->save();

        if ($email == 'true') {
            $customerInfo = User::find(Auth::user()->id);
            $stateId = 0;
            $shipping_city_key = 0;
            $billing_stateId = 0;
            $billing_city_key = 0;

            //shipping
            if (isset($customerInfo->userInfo->cartShippingAddress->state_id)) $stateId = $customerInfo->userInfo->cartShippingAddress->state_id;
            if (isset($customerInfo->userInfo->cartShippingAddress->city_key)) $shipping_city_key = $customerInfo->userInfo->cartShippingAddress->city_key;

            //billing
            if (isset($customerInfo->userInfo->mailingAddress->state_id)) $billing_stateId = $customerInfo->userInfo->mailingAddress->state_id;
            if (isset($customerInfo->userInfo->mailingAddress->city_key)) $billing_city_key = $customerInfo->userInfo->mailingAddress->city_key;

            $states = State::where('country_id', country()->country_id)->get();

            //shipping
            if ($stateId == 0) {
                $cities = City::where('country_id', country()->country_id)->orderBy('city_name', 'asc')->get();
            } else {
                $cities = City::where('country_id', country()->country_id)->where('state_id', $stateId)->orderBy('city_name', 'asc')->get();
            }

            //billing
            if ($billing_stateId == 0) {
                $billcities = City::where('country_id', country()->country_id)->orderBy('city_name', 'asc')->get();
            } else {
                $billcities = City::where('country_id', country()->country_id)->where('state_id', $billing_stateId)->orderBy('city_name', 'asc')->get();
            }
            return view('shop.customer-dashboard.profile.edit')
                ->with('customerInfo', $customerInfo)
                ->with('states', $states)
                ->with('stateId', $stateId)
                ->with('cities', $cities)
                ->with('billcities', $billcities)
                ->with('billing_stateId', $billing_stateId)
                ->with('shipping_city_key', $shipping_city_key)
                ->with('billing_city_key', $billing_city_key)
                ->with('emailChange', true);
        }
        if ($name && $billTo && $shipTo && $contact && $birth_date) {
            return redirect()->route('shop.dashboard.customer.profile')->with(['successful_message' => 'Profile updated successfully'])->with('customerInfo', $customerInfo);
        } else {
            return redirect()->route('shop.dashboard.customer.profile')->with(['error_message' => 'Failed to update profile'])->with('customerInfo', $customerInfo);
        }
    }

    // Membership main page
    public function membership()
    {
        // Get user
        $user = User::find(Auth::user()->id);
        //
        $genders = Gender::all();
        $races = Race::all();
        $states = State::all();
        $maritals = Marital::all();
        $employments = Employment::all();

        return view('shop.membership.index')
            ->with('genders', $genders)
            ->with('races', $races)
            ->with('states', $states)
            ->with('maritals', $maritals)
            ->with('user', $user)
            ->with('employments', $employments);
    }

    // Membership validation
    public function checkMembership(Request $request)
    {


        // $user = User::find(Auth::user()->id);
        // $userDetails = $user->userInfo;
        //  $data =  $request->all();
        // dd($data);

        // Save to database
        $membership = new UserMembership;
        $membership->new_or_existing = $request->input('existingOrNew');
        $membership->user_id = $request->input('user_id');
        $membership->email = $request->input('email');
        $membership->full_name = $request->input('full_name');
        $membership->mobile_number = $request->input('contact_number_mobile');
        $membership->years_joined = $request->input('year');
        $membership->product_used = $request->input('whatProduct');
        $membership->agent_name = $request->input('referrer_name');
        $membership->agent_id = $request->input('referrer_id');

        if ($request->file('DCProductInvoice')) {

            $file = $request->file('DCProductInvoice');
            $fileExtension = $file->getClientOriginalExtension();
            $fileName = $membership->product_used . '-product-invoice' . '.' . $fileExtension;
            $destinationPath =

                public_path('/storage/uploads/images/users/' . $membership->user_id . '/memberships');

            if (!File::isDirectory($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true);
            }
            $file->move($destinationPath, $fileName);

            $membership->product_invoice =
                '/storage/uploads/images/users/' . $membership->user_id . '/memberships' . '/' . $fileName;
        }

        $membership->save();

        // // details for API
        // // Get name from database
        // $name = $userDetails->full_name;
        // // Get name from request
        // $nric = $request->last_nric;
        // // Get dob from database
        // $date_of_birth = $userDetails->date_of_birth;

        // // Save last 4 digits NRIC
        // $userDetails->nric = $nric;
        // $userDetails->save();

        // // Start API request
        // $client = new Client();
        // $url =
        //     "https://beta.dcvital.com/wp-json/creator/v1/customers/validate-status/";

        // $requestAPI = $client->post(
        //     $url,
        //     [
        //         'json' => [
        //             'name' => $name,
        //             'identity' => $nric,
        //             'birthday' => $date_of_birth,
        //         ],
        //         'http_errors' => false,
        //     ]
        // );
        // // Decode API request
        // $response = json_decode($requestAPI->getBody(), true);
        // $rubyCode = "101";
        // $topazCode = "102";
        // dd($data);
        //  return    view('emails.customer-register')
        // ->with('data',  $data)
        // ->with('membership',  $membership);

        $email = env('BUJISHU_EMAIL', 'dc@delhubdigital.com');
        SendCustomerRegisterEmail::dispatch($email, $membership);

        // if ($response['code'] == 200) {
        //         $userDetails->account_status = $rubyCode;
        //         $userDetails->save();
        //         $validationMessage = $response['message'];
        //         $validationStatus = $response['code'];
        // return view('shop.membership.validation', compact(['validationMessage','validationStatus']));
        return view('shop.membership.validation');
        // } else {
        //         $userDetails->account_status = $topazCode;
        //         $userDetails->save();
        //         $validationMessage = $response['message'];
        //         $validationStatus = 'false';
        //         return redirect()->back()->with('validationStatus');
        // }
    }
}
