{{-- @inject('controller', 'App\Http\Controllers\Administrator\Pick\PickBatchController') --}}

@extends('layouts.administrator.main')

@section('content')

@include('inc.messages')

<h1>Stock Receive by batch - ID {{$stockTransferCheck->id}}, Box Name({{$stockTransferCheck->transferBox->box_name}})</h1>
<div class="card shadow-sm">
    <div class="card-body">
        <div>
            {{-- <div class="row">
                <div class="col-12 text-right p-2">
                    <a href="{{route($controller::$create)}}" style="color: white; font-style: normal; border-radius: 5px;" class="btn btn-dark">Create New {{$controller::$header}}</a>
                </div>
            </div> --}}
        </div>
        <div class="table-responsive m-2">

            @if($stockTransferCheck)
                <p> <b>ID</b> :{{$stockTransferCheck->id}}</p>
                <p> <b>Transfer Type </b>: {{showWarehouseTransferType($stockTransferCheck->type)}} - {{($stockTransferCheck->transferBox) ? 'Box : ('.$stockTransferCheck->transferBox->box_name.')' : ''}} </p>
                <p> <b>From Location</b> : {{ $stockTransferCheck->fromLocation->location_key}} - <b>{{$stockTransferCheck->fromLocation->location_name}}</b>({{$stockTransferCheck->fromLocation->default_address}})</p>
                <p> <b>To Location</b> : {{ $stockTransferCheck->toLocation->location_key}} - <b>{{$stockTransferCheck->toLocation->location_name}}</b> ({{$stockTransferCheck->toLocation->default_address}})</p>

                <div class="col-8">
                    Notes:
                    <textarea type="text" name="notes" id="notes{{$stockTransferCheck->id}}" class="form-control" value="{{$stockTransferCheck->notes}}">{{$stockTransferCheck->notes}}</textarea>
                    <div class="ml-auto col-2 mr-4 mt-2">
                        <button type="button" class="btn btn-danger"
                            onclick="editNotes({{ $stockTransferCheck->id }});">
                            Edit
                            <div class="spinner-border spinner-border-sm text-dark variationLoader{{ $stockTransferCheck->id }}" role="status" style="display:none;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </button>
                    </div>
                    <p class="variationThisEmpty{{ $stockTransferCheck->id }}" style="color:red;display: none;">Please enter something to add!</p>
                    <p class="variationRefreshPrice{{ $stockTransferCheck->id }}" style="color:red;display: none;">Refresh to edit this notes</p>
                </div>

                <br>
                <hr>
                <br>
                <h3>Product Count</h3>

                <form action="{{route('warehouse.StockTransfer.receiveBatchChecking', ['stockTransferId'=>$stockTransferCheck->id])}}" id="create-form-id" method="POST" enctype="multipart/form-data">
                    @csrf

                    <table id="table-id"  class="table table-striped table-bordered" style="min-width: 1366px;">
                        <thead>
                            <tr>
                                {{-- <th> Received</th> --}}
                                {{-- <th>Batch Item Id</th> --}}
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th>Product Attribute</th>
                                <th>Received Quantity</th>
                            </tr>
                        </thead>
                        <tbody>

                            @php
                                $array =array();
                            @endphp
                                @foreach($stockTransferCheck->stockTransferItems as $row)
                                @php
                                    if (!isset($array[$row->batchItem->batchDetail->product_code])) {
                                        $array[$row->batchItem->batchDetail->product_code]['qty'] = 0;
                                        $array[$row->batchItem->batchDetail->product_code]['received_qty'] = 0;
                                        $array[$row->batchItem->batchDetail->product_code]['product_code'] = $row->batchItem->batchDetail->product_code;
                                        $array[$row->batchItem->batchDetail->product_code]['product_name'] = $row->batchItem->batchDetail->productAttribute->product2->parentProduct->name;
                                        $array[$row->batchItem->batchDetail->product_code]['attribute_name'] = $row->batchItem->batchDetail->productAttribute->attribute_name;
                                    }
                                    $array[$row->batchItem->batchDetail->product_code]['qty'] ++;
                                    $array[$row->batchItem->batchDetail->product_code]['received_qty'] += $row->received;
                                @endphp
                                @endforeach

                                @foreach($array as $productCode => $item)

                                <tr>
                                    <td>{{$productCode}}</td>
                                    <td>{{$item['product_name']}}</td>
                                    <td>{{$item['attribute_name']}}</td>
                                    <td><input type="number" name="received_quantity[{{$item['product_code']}}]" required></td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>

                    <div style="text-align: center;">
                        <button type="submit" class="btn btn-success"> Complete Transfer In</button>
                    </div>
                </form>

               {{-- <hr>

                <h3>Summary</h3>
                <table class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Received Qty</th>
                            <th>Total Quantity</th>
                        </tr>

                @if ($array)
                    @foreach ($array as $productCode => $total)
                        <tbody>
                            <td> {{$productCode}} </td>
                            <td> {{$total['received_qty']}}</td>
                            <td> {{$total['qty']}}</td>
                        </tbody>
                    @endforeach
                @endif

                </table> --}}


                <div class="row">
                    <div class="col-12 text-center">


                    </div>
                </div>
            @else
                <p style="color: rgb(148, 148, 148);text-align: center;font-size: 20px;">No Found</p>
            @endif

            {{-- <div class="my-input-warehouse input-group input-group-lg mb-3">
                <input type="text" ref="scancode" v-model="scancode" name="scancode" class="bg-form2 form-control" id="my-input-warehouse"
                    placeholder="Use Scanner Here" autofocus autocomplete="off" @@change="qrProduct(scancode)">
                <div class="pop-btn">
                    <button type="button" class="qrbtn btn-primary">Submit</button>
                </div>
            </div> --}}
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
function clickSubmit() {
    if (confirm('Are you sure complete stock transfer?')) {
        document.getElementById("create-form-id").submit();
    }
}

function editNotes(batchID) {
    const notes =$("#notes"+batchID).val();

    if (confirm('Are you sure want to edit this notes? Any unsaved changes will be lost')) {
        if (notes=='' ) {
            $(".variationThisEmpty"+batchID).show();
        } else {
            $.ajax({
                // url: '/administrator/warehouse/inventory/edit-notes/'+batchID,
                url:"{{ route('warehouse.StockTransfer.stockTransferList.edit-notes', ['batchID' => "+batchID+"]) }}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "batchID": batchID,
                    "notes": notes,
                },
                beforeSend: function () {
                    $(".variationLoader"+batchID).show();
                },
                success: function (success) {
                    $(".variationLoader"+batchID).hide();
                        $(".variationRefreshPrice"+batchID).show();
                },
                error: function () {
                    alert("error in loading");
                }
            });
        }
    }
}
</script>
@endpush
