<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Formula') }}</title>
    @include('layouts.google-tags')
    @include('layouts.favicon')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.0.1/css/bootstrap.min.css" integrity="sha512-Ez0cGzNzHR1tYAv56860NLspgUGuQw16GiOOp/I2LuTmpSK9xDXlgJz3XN4cnpXWDmkNBKXR/VDMTCnAaEooxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400;500&family=Yellowtail&display=swap"
        rel="stylesheet">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet'>


    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}?v=3.6">
    <link rel="stylesheet" href="{{ asset('css/style2.css') }}?v=3.7">
    <link rel="stylesheet" href="{{ asset('css/style3.css') }}?v=3.3">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}?v=3.9">
    <link rel="stylesheet" href="{{ asset('css/responsive2.css') }}?v=3.3">
    <link href="{{ asset('css/app.css') }}?v=1.7" rel="stylesheet">
    <!-- Custom Scrollbar CDN -->

    <!-- TODO: Import using mix. -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    @stack('style')
</head>

<body class="promo-page-background-color">

    <!-- Navigation bar -->
    {{-- @include('layouts.shop.navigation.navigation') --}}
    @include('layouts.management.navigations.navigation-bar-customer')

    <!-- Side bar -->
    {{-- @include('layouts.shop.navigation.sidebar') --}}

    <main id="body-content-collapse-sidebar">
        <div id="border app">
            @yield('content')
        </div>
    </main>

    <!-- Footer -->
    @include('layouts.shop.footer.footer-about-us')

    <!-- Custom Scrollbar CDN -->
    <!-- TODO: Import using mix. -->
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js">
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function() {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function() {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });


            $('#body-content-collapse-sidebar,#footer-content-collapse-sidebar,.nav-content-sidebar-collapse')
                .click(function(event) {
                    if ($(event.target).attr('id') !== "sidebar" && $(event.target).attr('id') !==
                        "sidebarCollapse") {
                        $('#sidebar').removeClass('active');
                        $('.overlay').removeClass('active');
                    }
                });
        });
    </script>
    <script type="text/javascript">
        function onPageLoadGetQuantity() {
            let getQuantityURL = "{{ route('web.shop.cart.quantity', ['id' => Auth::user()->id]) }}";
            const cartQuantityElement = $('#cart-quantity');
            const cartQuantityElementMobile = $('#cart-quantity-mobile');

            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    cartQuantityElement.text(response.data);
                    cartQuantityElementMobile.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });
        }

        // function to get perfect list quantity
        function onPageLoadGetPerfectListQuantity() {
            let getQuantityURL = "{{ route('shop.perfect-list.quantity', ['id' => Auth::user()->id]) }}";
            const perfectListQuantityElement = $('#perfect-list-quantity');
            const perfectListQuantityMobileElement = $('#perfect-list-quantity-mobile');
            $.ajax({
                async: true,
                beforeSend: function() {
                    console.log('Request starting.');
                },
                complete: function() {
                    console.log('Request complete.');
                },
                url: getQuantityURL,
                type: 'GET',
                success: function(response) {
                    perfectListQuantityElement.text(response.data);
                    perfectListQuantityMobileElement.text(response.data);
                },
                error: function(response) {
                    console.log('Error');
                    console.log(response);
                }
            });


        }

        // $(document).ready(function() {
        //     onPageLoadGetQuantity();
        //     onPageLoadGetPerfectListQuantity();
        // });
    </script>
    @stack('script')
</body>
<!-- 1920 -->

</html>
