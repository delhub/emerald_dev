<?php

namespace App\Models\File;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //use HasFactory;

    // Table Name
    protected $table = 'files';
    // Primary Key
    public $primaryKey = 'id';
    protected $fillable = [
        'name',
        'file_path'
    ];
}
