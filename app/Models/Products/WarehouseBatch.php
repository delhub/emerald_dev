<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;
use App\Models\Products\WarehouseBatchDetail;
use App\Models\Warehouse\Location\Wh_location;

class WarehouseBatch extends Model
{
    // Set table
    protected $table = 'wh_batch';

    // Set timestamps
    public $timestamps = true;

    // Set primary key
    protected $primaryKey = 'id';

    // Set mass assignable columns
    protected $fillable = [];

    public function details()
    {
        return $this->hasMany('App\Models\Products\WarehouseBatchDetail', 'batch_id', 'batch_id');
    }

    public function panel()
    {
        return $this->hasOne('App\Models\Users\Panels\PanelInfo', 'account_id', 'panel_id');
    }

    public function location()
    {
        return $this->hasOne(Wh_location::class, 'id', 'location_id');
    }
}
