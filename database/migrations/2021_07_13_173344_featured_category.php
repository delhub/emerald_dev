<?php

use App\Models\Categories\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class FeaturedCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $categories = Category::all();

        $slug1 = [
            'energy-multi-vitamins-and-nutrition',
            'immunity-care',
            'vision-care',
            'skin-care',
            'stomach-and-guts-care',
            'children-care',
            'infant-care',
            'hair',
            'body-wash',
            'oral-care',
        ];

        $slug2 = [
            'facial',
            'body-protection',
            'intimate-wash',
            'men-care',
            'women-care',
            'cholesterol',
            'blood-pressure',
            'blood-sugar',
            'mouth-and-throat-care',
            'brain-care',
        ];

        $slug3 = [
            'heart-care',
            'lung-and-respiratory-care',
            'bone-and-joint-care',
            'liver-care',
            'kidney-care',
            'muscle-care',
            'anti-oxidant-anti-inflammation',
            'relaxing-sleeping',
            'weight-management',
            'cosmetic'
        ];

        foreach ($categories as $category) {
            if (in_array($category->slug,$slug1)) {
                $category->featured = 1;
                $category->save();
            }
            if (in_array($category->slug,$slug2)) {
                $category->featured = 2;
                $category->save();
            }
            if (in_array($category->slug,$slug3)) {
                $category->featured = 3;
                $category->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
